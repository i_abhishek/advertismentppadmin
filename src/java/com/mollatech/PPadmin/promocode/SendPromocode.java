package com.mollatech.PPadmin.promocode;

import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.connector.communication.SGStatus;
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.commons.UtilityFunctions;
import com.mollatech.serviceguard.nucleus.db.Operators;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgPromocode;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PromocodeManagement;
import com.mollatech.serviceguard.nucleus.settings.SendNotification;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "SendPromocode", urlPatterns = {"/SendPromocode"})
public class SendPromocode extends HttpServlet {

    static final Logger logger = Logger.getLogger(SendPromocode.class);

    public static final int PENDING = 2;

    public static final int SENT = 0;

    public static final int SEND = 0;

    public static final int RESET = 1;

    private String partName;

    private String channelName = "ServiceGuard Portal";

    private String code;

    private Date validTill;

    private String emailId;

    private String channelId;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Request servlet is #SendPromocode from #PPAdmin at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        String channelid = (String) request.getSession().getAttribute("_channelId");
        String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        Operators opObj = (Operators) request.getSession().getAttribute("_apOprDetail");
        String _promocodeId = request.getParameter("promoCodeId");
        String result = "success";
        String message = "Notification send successfully";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        int ipromocode = -1;
        if (_promocodeId != null) {
            ipromocode = Integer.parseInt(_promocodeId);
        }

        SgPromocode promoObj = new PromocodeManagement().getPromocodeById(sessionId, channelid, ipromocode);
        if (promoObj != null) {
            if (promoObj.getStatus() == GlobalStatus.ACTIVE) {
                String partnerId = promoObj.getPartnerId();
                String[] emailToSend = null;
                if (partnerId != null && !partnerId.equals("") && partnerId.contains("all")) {
                    PartnerDetails[] partArray = new PartnerManagement().getAllPartnerDetails(sessionId, channelid);
                    if (partArray != null) {
                        emailToSend = new String[partArray.length];
                        for (int i = 0; i < partArray.length; i++) {
                            emailToSend[i] = partArray[i].getPartnerEmailid();
                        }
                    }
                } else if (partnerId != null && !partnerId.equals("")) {
                    String[] parDet = partnerId.split(",");
                    emailToSend = new String[parDet.length];
                    for (int j = 0; j < parDet.length; j++) {
                        int parId = Integer.parseInt(parDet[j]);
                        PartnerDetails parObj = new PartnerManagement().getPartnerDetails(parId);
                        if (parObj != null) {
                            emailToSend[j] = parObj.getPartnerEmailid();
                        }
                    }
                }
                SendMail mail = new SendMail(channelid, "Partner", channelName, promoObj.getCode(), promoObj.getExpireOn(), opObj.getEmailid(), emailToSend);
                Thread notifyThread = new Thread(mail);
                notifyThread.start();
            } else {
                result = "error";
                message = "Promo code is not active yet!!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            result = "error";
            message = "Failed to send share promo code!!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }

    class SendMail implements Runnable {

        String partName, channelName, code, emailId, channelid;
        String[] partnerList;
        Date validTill;

        public SendMail(String channelid, String partName, String channelName, String code, Date validTill, String emailId, String[] partnerList) {
            this.channelName = channelName;
            this.partName = partName;
            this.validTill = validTill;
            this.code = code;
            this.emailId = emailId;
            this.channelid = channelid;
            this.partnerList = partnerList;
        }

        @Override
        public void run() {
            String emailMsg = LoadSettings.g_templateSettings.getProperty("email.promocode.notification");
            if (emailMsg != null) {
                Date d = new Date();
                emailMsg = emailMsg.replaceAll("#name#", partName);
                emailMsg = emailMsg.replaceAll("#channel#", channelName);
                emailMsg = emailMsg.replaceAll("#code#", code);
                emailMsg = emailMsg.replaceAll("#Validity#", UtilityFunctions.getTMReqDate(validTill));
                emailMsg = emailMsg.replaceAll("#datetime#", UtilityFunctions.getTMReqDate(d));
//                emailMsg = emailMsg.replaceAll("#email#", emailId);
            }
            int productType = 3;
            SendNotification notification = new SendNotification();
            SGStatus statusSG = notification.SendEmail(channelid, emailId, "Promo code", emailMsg, null, partnerList, null, null, productType);
            logger.info("PromocodeEmailStatus : " + statusSG.regcode + " ," + statusSG.regcodeMsg + " ,emailId : " + emailId + " ,partName" + partName);
            //System.out.println("PromocodeEmailStatus : "+statusSG.regcode+" ,"+statusSG.regcodeMsg+" ,emailId : "+emailId+" ,partName"+partName);
        }
    }
}
