<%@page import="java.util.Iterator"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement"%>

<%@page import="com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MethodName"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Classes"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Methods"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        String apName = request.getParameter("apName");
        String packageName = request.getParameter("packageName");
        String resourceName = request.getParameter("_resourceName");
        String versionData = request.getParameter("versionData");
        String apiName = request.getParameter("apiName");
        if (!apName.equals("-1")) {
            SgReqbucketdetails reqObj = new RequestPackageManagement().getReqPackageByName(SessionId, channelId, packageName);
            String apDetails = reqObj.getSlabApRateDetails();
            if (apDetails != null) {
                JSONArray jsOld = new JSONArray(apDetails);
                String keyValue = apName + ":" + resourceName + ":" + versionData + ":" + apiName;
                JSONObject reqJsonObj = null;
                for (int j = 0; j < jsOld.length(); j++) {
                    JSONObject jsonexists1 = jsOld.getJSONObject(j);
                    if (!jsonexists1.isNull(keyValue)) {
                        reqJsonObj = jsonexists1.getJSONObject(keyValue);
                        break;
                    }
                }
                if (reqJsonObj != null) {
    %>
    <div class="form-group">   
        <label class="col-lg-2 control-label" style="">Range</label>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon">From</span>
                <input type="text" id="rangeFrom1" name="rangeFrom1" disabled class="form-control" placeholder="From " value="<%=reqJsonObj.getString("rangeFrom1")%>">                                    
            </div>
        </div>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon">To</span>
                <input type="text" id="rangeTo1" name="rangeTo1" disabled class="form-control" placeholder="To" value="<%=reqJsonObj.getString("rangeTo1")%>">                                    
            </div>
        </div>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon">Price</span>
                <input type="text" id="price1" name="price1" disabled class="form-control" placeholder="Price" value="<%=reqJsonObj.getString("price1")%>">                                    
            </div>
        </div>
    </div>

    <div class="form-group">   
        <label class="col-lg-2 control-label" style="">Range</label>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon">From</span>
                <input type="text" id="rangeFrom2" name="rangeFrom2" disabled class="form-control" placeholder="From " value="<%=reqJsonObj.getString("rangeFrom2")%>">                                    
            </div>
        </div>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon">To</span>
                <input type="text" id="rangeTo2" name="rangeTo2" disabled class="form-control" placeholder="To" value="<%=reqJsonObj.getString("rangeTo2")%>">                                    
            </div>
        </div>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon">Price</span>
                <input type="text" id="price2" name="price2" disabled class="form-control" placeholder="Price" value="<%=reqJsonObj.getString("price2")%>">                                    
            </div>
        </div>
    </div>

    <div class="form-group">   
        <label class="col-lg-2 control-label" style="">Range</label>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon">From</span>
                <input id="rangeFrom3" name="rangeFrom3" disabled class="form-control" placeholder="From " value="<%=reqJsonObj.getString("rangeFrom3")%>">                                    
            </div>
        </div>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon">To</span>
                <input id="rangeTo3" name="rangeTo3" disabled type="text" class="form-control" placeholder="To" value="<%=reqJsonObj.getString("rangeTo3")%>">                                    
            </div>
        </div>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon">Price</span>
                <input type="text" id="price3" name="price3" disabled class="form-control" placeholder="Price" value="<%=reqJsonObj.getString("price3")%>">                                    
            </div>
        </div>
    </div>

    <div class="form-group">   
        <label class="col-lg-2 control-label" style="">Range</label>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon">From</span>
                <input id="rangeFrom4" name="rangeFrom4" type="text" disabled class="form-control" placeholder="From " value="<%=reqJsonObj.getString("rangeFrom4")%>">                                    
            </div>
        </div>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon">To</span>
                <input type="text" id="rangeTo4" name="rangeTo4" class="form-control" disabled placeholder="To" value="<%=reqJsonObj.getString("rangeTo4")%>">                                    
            </div>
        </div>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon">Price</span>
                <input type="text" id="price4" name="price4" class="form-control" disabled placeholder="Price" value="<%=reqJsonObj.getString("price4")%>">                                    
            </div>
        </div>
    </div>

    <div class="form-group">   
        <label class="col-lg-2 control-label" style="">Range</label>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon">From</span>
                <input type="text" id="rangeFrom5" name="rangeFrom5" class="form-control" disabled placeholder="From " value="<%=reqJsonObj.getString("rangeFrom5")%>">                                    
            </div>
        </div>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon">To</span>
                <input type="text" id="rangeTo5" name="rangeTo5" class="form-control" disabled placeholder="To" value="<%=reqJsonObj.getString("rangeTo5")%>">                                    
            </div>
        </div>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon">Price</span>
                <input type="text" id="price5" name="price5" class="form-control" disabled placeholder="Price" value="<%=reqJsonObj.getString("price5")%>">                                    
            </div>
        </div>
    </div>                  
    <%}
                        }

                    }%>
