<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgProductionAccess"%>
<%@include file="header.jsp" %>
<script src="js/productionAccess.js" type="text/javascript"></script>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header text-uppercase">Production Access Management</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47; <i class="fa fa-list-ol"></i><b> List of Access</b>
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover display nowrap" id="dataTables-example">
                        <thead>
                            <tr>
                                <td/>
                                <th>No.</th>
                                <th>Developer Name</th>
                                <th>Status</th>
                                <th>Access Point</th>
                                <th>Res. Name</th>
                                <th>Version</th>
                                <th>Manage</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                Map<Integer, String> partnerMap = new HashMap();
                                Map<Integer, String> apMap = new HashMap();
                                Map<Integer, String> resMap = new HashMap();
                                SgProductionAccess[] access = new ProductionAccessManagement().getAllDetails(SessionId, channelId);
                                if (access != null) {
                                    for (int i = 0; i < access.length; i++) {
                                        String PartnerName, accessPointName, resourceName, status = "PENDING";
                                        if (access[i].getStatus() == GlobalStatus.SUCCESS) {
                                            status = "APPROVED";
                                        } else if (access[i].getStatus() == GlobalStatus.REJECTED) {
                                            status = "REJECTED";
                                        } else if (access[i].getStatus() == GlobalStatus.SUSPEND){
                                            status = "SUSPENDED";
                                        }else if (access[i].getStatus() == GlobalStatus.TERMINATED){
                                            status = "TERMINATED";
                                        }
                                        if (partnerMap.get(access[i].getPartnerid()) == null) {
                                            PartnerName = new PartnerManagement().getPartnerDetails(access[i].getPartnerid()).getPartnerName();
                                            partnerMap.put(access[i].getPartnerid(), PartnerName);
                                        } else {
                                            PartnerName = partnerMap.get(access[i].getPartnerid());
                                        }
                                        if (apMap.get(access[i].getAccesPointId()) == null) {
                                            accessPointName = new AccessPointManagement().getAccessPointById(access[i].getChannelId(), access[i].getAccesPointId()).getName();
                                            apMap.put(access[i].getAccesPointId(), accessPointName);
                                        } else {
                                            accessPointName = apMap.get(access[i].getAccesPointId());
                                        }
                                        if (resMap.get(access[i].getResourceId()) == null) {
                                            resourceName = new ResourceManagement().getResourceById(access[i].getResourceId()).getName();
                                            resMap.put(access[i].getResourceId(), resourceName);
                                        } else {
                                            resourceName = resMap.get(access[i].getResourceId());
                                        }

                            %>
                            <tr>
                                <td/>
                                <td><%=i + 1%></td>
                                <td><a href="./viewDetails.jsp?pid=<%=access[i].getPartnerid()%>&apId=<%=access[i].getAccesPointId()%>&resId=<%=access[i].getResourceId()%>&version=<%=access[i].getVersion()%>"><%=PartnerName%></a></td>
                                <td>
                                    <%if(access[i].getStatus() == GlobalStatus.SUCCESS){%>
                                    <span class="label label-success"><%=status%></span>
                                    <%}if(access[i].getStatus() == GlobalStatus.PENDING){%>
                                    <span class="label label-info"><%=status%></span>
                                    <%}if(access[i].getStatus() == GlobalStatus.REJECTED){%>
                                    <span class="label label-danger"><%=status%></span>
                                    <%}if(access[i].getStatus() == GlobalStatus.SUSPEND){%>
                                    <span class="label label-warning"><%=status%></span>
                                    <%}if(access[i].getStatus() == GlobalStatus.TERMINATED){%>
                                    <span class="label label-primary"><%=status%></span>
                                    <%}%>
                                </td>
                                <td><%=accessPointName%></td>
                                <td><%=resourceName%></td>
                                <td><%=access[i].getVersion()%></td>
                                <td>
                                    <div class="btn btn-group btn-xs">
                                        <button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown"><%if (access[i].getStatus() == GlobalStatus.SUCCESS) {%> <font style="font-size: 11px;">Approved</font> <%} else if(access[i].getStatus() == GlobalStatus.PENDING){%><font style="font-size: 11px;"> Pending</font> <%}else if(access[i].getStatus() == GlobalStatus.REJECTED){%><font style="font-size: 11px;"> Rejected</font> <%}else if(access[i].getStatus() == GlobalStatus.SUSPEND){%><font style="font-size: 11px;"> Suspend</font><%}else if(access[i].getStatus() == GlobalStatus.TERMINATED){%><font style="font-size: 11px;"> Terminated</font><%}else{%><font style="font-size: 11px;">New</font><%}%><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" onclick="changeProductionAccessStatus('<%=access[i].getId()%>', '<%=GlobalStatus.SUCCESS%>')">Mark as Approve?</a></li>
                                            <li><a href="#" onclick="changeProductionAccessStatus('<%=access[i].getId()%>', '<%=GlobalStatus.SUSPEND%>')">Mark as Suspended?</a></li>
                                            <li><a href="#" onclick="changeProductionAccessStatus('<%=access[i].getId()%>', '<%=GlobalStatus.TERMINATED%>')">Mark as Terminate?</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            <% }
                            }
                                session.setAttribute("partnerMap", partnerMap);
                                session.setAttribute("apMap", apMap);
                                session.setAttribute("resMap", resMap);
                            %>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
                        
</div>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="dist/js/sb-admin-2.js"></script>
   
</body>
</html>
<%@include file="footer.jsp" %>