/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.partner.account;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.SgProductionAccess;
import com.mollatech.serviceguard.nucleus.db.SgRequestForProduction;
import com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestProductionManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author Ashu
 */
@WebServlet(name = "ChangeStatusForAPI", urlPatterns = {"/ChangeStatusForAPI"})
public class ChangeStatusForAPI extends HttpServlet {

    static final Logger logger = Logger.getLogger(SetProductionAccess.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        logger.info("Requested Servlet is ChangeStatusForAPI from PPAdmin at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        String result, message;
        JSONObject json = new JSONObject();
        Map<String, SgRequestForProduction> productionMap = (Map) request.getSession().getAttribute("RequestForProductionMap");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        try {
            String apiName = request.getParameter("_APIForSlabPricing2");
            String status = request.getParameter("status");
            String rejectReason = request.getParameter("rejectReason");
            SgRequestForProduction forProduction = productionMap.get(apiName);
            forProduction.setStatus(Integer.parseInt(status));
            forProduction.setRejectReason(rejectReason);
            int res = new RequestProductionManagement().updateRequest(SessionId, forProduction);
            if (res == 0) {
                result = "success";
                if (Integer.parseInt(status) == GlobalStatus.APPROVED) {
                    message = "Request Approved Successfully. ";
                } else {
                    message = "Request Rejected Successfully. ";
                }
            } else {
                result = "error";
                if (Integer.parseInt(status) == GlobalStatus.APPROVED) {
                    message = "Error in  Approving Request. ";
                } else {
                    message = "Error in  Rejecting Request. ";
                }
            }
            productionMap.put(apiName, forProduction);
            boolean allDone = false;
            for (String key : productionMap.keySet()) {
                forProduction = productionMap.get(key);
                if (forProduction.getStatus() == GlobalStatus.APPROVED) {
                    allDone = true;
                } else {
                    allDone = false;
                    break;
                }
            }
            if (allDone) {
                SgProductionAccess access = new ProductionAccessManagement().getDetails(SessionId, channelId, forProduction.getAccesPointId(), forProduction.getResourceId(), forProduction.getVersion(), forProduction.getPartnerid());
                access.setStatus(GlobalStatus.SUCCESS);
                new ProductionAccessManagement().updateRequest(SessionId, access);
            }else{
                SgProductionAccess access = new ProductionAccessManagement().getDetails(SessionId, channelId, forProduction.getAccesPointId(), forProduction.getResourceId(), forProduction.getVersion(), forProduction.getPartnerid());
                access.setStatus(GlobalStatus.REJECTED);
                new ProductionAccessManagement().updateRequest(SessionId, access);
            }
            json.put("result", result);
            json.put("message", message);
        } catch (Exception e) {
            logger.error("Exception at ChangeStatusForAPI ", e);
            result = "error";
            message = "Error in Checking Availability";
            json.put("result", result);
            logger.debug("Response of ChangeStatusForAPI Servlet's from PPAdmin Parameter   result is " + result);
            json.put("message", message);
            logger.debug("Response of ChangeStatusForAPI Servlet's from PPAdmin Parameter   message is " + message);
        } finally {
            logger.info("Response of ChangeStatusForAPI from PPAdmin" + json.toString());
            logger.info("Response of ChangeStatusForAPI Servlet from PPAdmin at " + new Date());
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
