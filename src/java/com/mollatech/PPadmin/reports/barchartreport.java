package com.mollatech.PPadmin.reports;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.serviceguard.nucleus.db.RequestTracking;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestTrackingManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class barchartreport extends HttpServlet {

    final int ALLOWED = 0;

    final int BLOCK_TIME = -1;

    final int BLOCK_DAY = -2;

    final int BLOCK_IP = -3;

    final int BLOCK_TPS = -4;

    final int BLOCK_TPD = -5;

    final int BLOCK_SSL = -6;

    final int BLOCK_TOKEN = -7;

    final int SERVICE_UNAVAILABLE = -8;

    final int BLOCK_BLACK_IP = -9;

    final int PACKAGE_NOT_SUBSCRIBED = -10;

    final int API_LEVEL_TOKEN = -11;

    final int BILLING = -12;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        int apid = -1, resid = -1, groupid = -1, partnerid = -1, sdate = -1, edate = -1, stime = -1, etime = -1;
        String channelId = null, sessionId = null;
        int OperatorType = -1;
        String _OperatorType = request.getParameter("_opType");
        if (_OperatorType != null) {
            OperatorType = Integer.parseInt(_OperatorType);
        }
        sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String _accesspoint = request.getParameter("_accesspoint");
        if (_accesspoint != null) {
            apid = Integer.parseInt(_accesspoint);
        }
        String _resourceid = request.getParameter("_resource");
        if (_resourceid != null) {
            resid = Integer.parseInt(_resourceid);
        }
        String _groupid = request.getParameter("_group");
        if (_groupid != null) {
            groupid = Integer.parseInt(_groupid);
        }
        String _partner = request.getParameter("_partner");
        if (_partner != null) {
            partnerid = Integer.parseInt(_partner);
        }
        String _sdate = request.getParameter("_sdate");
        String _edate = request.getParameter("_edate");
        String _stime = request.getParameter("_stime");
        String _etime = request.getParameter("_etime");
        RequestTrackingManagement ppw = new RequestTrackingManagement();
        try {
            int iAllowedCount = 0;
            int iBTimeCount = 0;
            int iBDayCount = 0;
            int iBIPCount = 0;
            int iBTPSCount = 0;
            int iBTPDCount = 0;
            int iBSSLCount = 0;
            int iTOKENCount = 0;
            int BLACK_IP = 0;
            int PSUBSCRIBED = 0;
            int API_TOKEN = 0;
            int iBILLING = 0;
            RequestTrackingManagement rtMngt = new RequestTrackingManagement();
            RequestTracking[] requestTracking = rtMngt.getDetails(channelId, apid, resid, groupid, partnerid, _sdate, _edate, _stime, _etime, "Select");

//            int iAllowedCount = ppw.getCountOfPartnerByStatus(sessionId, channelId, apid, resid, groupid, partnerid, _sdate, _edate, _stime, _etime, ALLOWED);
//            int iBTimeCount = ppw.getCountOfPartnerByStatus(sessionId, channelId, apid, resid, groupid, partnerid, _sdate, _edate, _stime, _etime, BLOCK_TIME);
//            int iBDayCount = ppw.getCountOfPartnerByStatus(sessionId, channelId, apid, resid, groupid, partnerid, _sdate, _edate, _stime, _etime, BLOCK_DAY);
//            int iBIPCount = ppw.getCountOfPartnerByStatus(sessionId, channelId, apid, resid, groupid, partnerid, _sdate, _edate, _stime, _etime, BLOCK_IP);
//            int iBTPSCount = ppw.getCountOfPartnerByStatus(sessionId, channelId, apid, resid, groupid, partnerid, _sdate, _edate, _stime, _etime, BLOCK_TPS);
//            int iBTPDCount = ppw.getCountOfPartnerByStatus(sessionId, channelId, apid, resid, groupid, partnerid, _sdate, _edate, _stime, _etime, BLOCK_TPD);
//            int iBSSLCount = ppw.getCountOfPartnerByStatus(sessionId, channelId, apid, resid, groupid, partnerid, _sdate, _edate, _stime, _etime, BLOCK_SSL);
//            int iTOKENCount = ppw.getCountOfPartnerByStatus(sessionId, channelId, apid, resid, groupid, partnerid, _sdate, _edate, _stime, _etime, BLOCK_TOKEN);
//            int iUnavailableCount = ppw.getCountOfPartnerByStatus(sessionId, channelId, apid, resid, groupid, partnerid, _sdate, _edate, _stime, _etime, SERVICE_UNAVAILABLE);
            if (requestTracking != null) {
                for (RequestTracking requestTracking1 : requestTracking) {
                    if (requestTracking1.getStatus() == ALLOWED) {
                        iAllowedCount++;
                    } else if (requestTracking1.getStatus() == BLOCK_TIME) {
                        iBTimeCount++;
                    } else if (requestTracking1.getStatus() == BLOCK_DAY) {
                        iBDayCount++;
                    } else if (requestTracking1.getStatus() == BLOCK_IP) {
                        iBIPCount++;
                    } else if (requestTracking1.getStatus() == BLOCK_TPS) {
                        iBTPSCount++;
                    } else if (requestTracking1.getStatus() == BLOCK_TPD) {
                        iBTPDCount++;
                    } else if (requestTracking1.getStatus() == BLOCK_SSL) {
                        iBSSLCount++;
                    } else if (requestTracking1.getStatus() == BLOCK_TOKEN) {
                        iTOKENCount++;
                    } else if (requestTracking1.getStatus() == BLOCK_BLACK_IP) {
                        BLACK_IP++;
                    } else if (requestTracking1.getStatus() == PACKAGE_NOT_SUBSCRIBED) {
                        PSUBSCRIBED++;
                    } else if (requestTracking1.getStatus() == API_LEVEL_TOKEN) {
                        API_TOKEN++;
                    } else if (requestTracking1.getStatus() == BILLING) {
                        iBILLING++;
                    }
                }
            }
            ArrayList<bar> sample = new ArrayList<bar>();
            sample.add(new bar(iAllowedCount, "ALLOWED"));
            sample.add(new bar(iBTimeCount, "TIME"));
            sample.add(new bar(iBDayCount, "DAY"));
            sample.add(new bar(iBIPCount, "IP"));
            sample.add(new bar(iBTPSCount, "TPS"));
            sample.add(new bar(iBTPDCount, "TPD"));
            sample.add(new bar(iBSSLCount, "SSL"));
            sample.add(new bar(iTOKENCount, "TOKEN"));
            sample.add(new bar(BLACK_IP, "BLACK_IP"));
            sample.add(new bar(PSUBSCRIBED, "SUBSCRIPTION"));
            sample.add(new bar(API_TOKEN, "APITOKEN"));
            sample.add(new bar(iBILLING, "BILLING"));
            Gson gson = new Gson();
            JsonElement element = gson.toJsonTree(sample, new TypeToken<List<bar>>() {
            }.getType());
            JsonArray jsonArray = element.getAsJsonArray();
            out.print(jsonArray);
        } finally {
            out.close();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
