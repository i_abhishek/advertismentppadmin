package com.mollatech.PPadmin.billing;

import com.mollatech.serviceguard.nucleus.db.SgBucketdetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "CheckAvailability", urlPatterns = { "/CheckAvailability" })
public class CheckAvailability extends HttpServlet {

    static final Logger logger = Logger.getLogger(CheckAvailability.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Requested Servlet is CheckAvailability from PPAdmin at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Access Point Added Successfully";
        String channels = (String) request.getSession().getAttribute("_channelId");
        String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String __packageName = request.getParameter("__packageName");
        String packageName = __packageName.trim();
        Pattern p1 = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher m1 = p1.matcher(packageName);
        boolean b1 = m1.find();
        Pattern p2 = Pattern.compile("[0-9]", Pattern.CASE_INSENSITIVE);
        Matcher m2 = p2.matcher(packageName);
        boolean b2 = m2.find();
        boolean spaces = false;
        if (packageName != null) {
            if (__packageName.contains(" ")) {
                spaces = true;
            }
        }
        try {
            if (packageName.trim().length() == 0) {
                result = "error";
                message = "Please Enter Package Name.";
                json.put("result", result);
                logger.debug("Response of CheckAvailability Servlet's from PPAdmin Parameter   result is " + result);
                json.put("message", message);
                logger.debug("Response of CheckAvailability Servlet's from PPAdmin Parameter   message is " + message);
                return;
            } else if (packageName.length() < 3) {
                result = "error";
                message = "Name should be more than 3 characters.";
                json.put("result", result);
                logger.debug("Response of CheckAvailability Servlet's from PPAdmin Parameter   result is " + result);
                json.put("message", message);
                logger.debug("Response of CheckAvailability Servlet's from PPAdmin Parameter   message is " + message);
                return;
            } else if (b1) {
                result = "error";
                message = "Name should not contain any symbols.";
                json.put("result", result);
                logger.debug("Response of CheckAvailability Servlet's from PPAdmin Parameter   result is " + result);
                json.put("message", message);
                logger.debug("Response of CheckAvailability Servlet's from PPAdmin Parameter   message is " + message);
                return;
            } else if (b2) {
                result = "error";
                message = "Name should not contain digits.";
                json.put("result", result);
                logger.debug("Response of CheckAvailability Servlet's from PPAdmin Parameter   result is " + result);
                json.put("message", message);
                logger.debug("Response of CheckAvailability Servlet's from PPAdmin Parameter   message is " + message);
                return;
            }
            SgBucketdetails bucketObj = new PackageManagement().getPackageByName(sessionId, channels, packageName);
            if (bucketObj != null) {
                result = "error";
                message = "This Package name Is Not Available";
                json.put("result", result);
                logger.debug("Response of CheckAvailability Servlet's from PPAdmin Parameter   result is " + result);
                json.put("message", message);
                logger.debug("Response of CheckAvailability Servlet's from PPAdmin Parameter   message is " + message);
                return;
            } else {
                result = "success";
                message = "This Package name Is Available";
                json.put("result", result);
                logger.debug("Response of CheckAvailability Servlet's from PPAdmin Parameter   result is " + result);
                json.put("message", message);
                logger.debug("Response of CheckAvailability Servlet's from PPAdmin Parameter   message is " + message);
                return;
            }
        } catch (Exception e) {
            logger.error("Exception at CheckAvailability ", e);
            result = "error";
            message = "Error in Checking Availability";
            json.put("result", result);
            logger.debug("Response of CheckAvailability Servlet's from PPAdmin Parameter   result is " + result);
            json.put("message", message);
            logger.debug("Response of CheckAvailability Servlet's from PPAdmin Parameter   message is " + message);
        } finally {
            logger.info("Response of CheckAvailability from PPAdmin" + json.toString());
            logger.info("Response of CheckAvailability Servlet from PPAdmin at " + new Date());
            out.print(json);
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
