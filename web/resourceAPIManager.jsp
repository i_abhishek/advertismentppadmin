<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourcePriceMgmt"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgResourceprice"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@include file="header.jsp" %>
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header text-uppercase">Resource API Price Management</h3>
            </div>
            <!-- /.col-lg-10 -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47;  <i class="fa fa-table"></i><b> List of Resources</b>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal " method="get">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover display nowrap" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <td/>
                                            <th>No.</th>
                                            <th>Name</th>
                                            <th>Get API(s)</th>
                                            <th>Edit API Price</th>
                                            <th>Created On</th>
                                            <th>Updated On</th>
                                        </tr>
                                    </thead>   
                                    <tbody>
                                    <%
                                        ResourceDetails[] detailses = new ResourceManagement().getAllResources();
                                        int count = 0;
                                        if (detailses != null) {
                                            for (ResourceDetails details : detailses) {
                                                TransformDetails transformDetails = new TransformManagement().getTransformDetailsbyResourseId(SessionId, channelId, details.getResourceId());
                                                if (transformDetails != null) {
                                                    SgResourceprice resourceprice = new ResourcePriceMgmt().getDetailsFromResId(details.getResourceId());
                                                    count++;
                                    %>
                                        
                                        <tr>
                                            <td/>                                            <td><%=count%></td>
                                            <td><%=details.getName()%></td>
                                            <td><font style="font-size: 11px"><a class="btn btn-info btn-xs" href="getAPIFromResource.jsp?resId=<%=details.getResourceId()%>"><i class="fa fa-eye"></i> Get API </a> </font></td>
                                            <td><font style="font-size: 11px"><a class="btn btn-info btn-xs" <%if (resourceprice == null) { %>disabled<%} else {%> href="editAPIPrice.jsp?resId=<%=details.getResourceId()%>"<%}%>><i class="fa fa-eye"></i> Edit Price </a> </font></td>
                                            <td><%if (resourceprice == null) { %>NA<%} else {%><%=resourceprice.getCreatedOn()%> <%}%></td>
                                            <td><%if (resourceprice == null) { %>NA<%} else {%><%=resourceprice.getUpdatedOn()%> <%}%></td>
                                        </tr>
                                        <%}
                                            }
                                        } %>
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="dist/js/sb-admin-2.js"></script>
<%@include file="footer.jsp" %>
