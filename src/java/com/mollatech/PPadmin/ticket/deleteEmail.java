/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.ticket;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.SgEmailticket;
import com.mollatech.serviceguard.nucleus.db.connector.management.EmailManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
@WebServlet(name = "deleteEmail", urlPatterns = {"/deleteEmail"})
public class deleteEmail extends HttpServlet {

    static final Logger logger = Logger.getLogger(deleteEmail.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Request servlet is #deleteEmail from #PPAdmin at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        String result = "success";
        String message = "Email Deleted successfully";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        String sessionid = (String) request.getSession().getAttribute("_partnerSessionId");

        logger.debug("value of session id : " + sessionid);
        String emailNo = request.getParameter("id");
        logger.debug("value of emailNo : " + emailNo);
//        int emailId = Integer.parseInt(emailNo);

        SgEmailticket updateStatus = new EmailManagement().getEmailDetailsById(sessionid, emailNo);

        updateStatus.setDeletedByOperator(GlobalStatus.DELETED);
        int res = new EmailManagement().deleteEmailById(sessionid, updateStatus);
        if (res == 0) {
            result = "success";
            logger.debug("Response of #deleteEmail from #PPAdmin Servlet's Parameter  result is " + result);
            message = "Email Deleted Succefully !";
            logger.debug("Response of #deleteEmail from #PPAdmin Servlet's Parameter  message is " + message);

        } else {
            result = "error";
            logger.debug("Response of #deleteEmail from #PPAdmin Servlet's Parameter  result is " + result);
            message = "Email deletion failed !";
            logger.debug("Response of #deleteEmail from #PPAdmin Servlet's Parameter  message is " + message);
        }
        try {
            json.put("_result", result);
            logger.debug("Response of #deleteEmail from #PPAdmin Servlet's Parameter result is " + result);
            json.put("_message", message);
            logger.debug("Response of #deleteEmail from #PPAdmin Servlet's Parameter message is " + message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
