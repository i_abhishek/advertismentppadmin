/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.billing;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.commons.MakerChaker;
import com.mollatech.serviceguard.nucleus.db.SgBucketdetails;
import com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "EditFlatPrice", urlPatterns = {"/EditFlatPrice"})
public class EditFlatPrice extends HttpServlet {
    static final Logger logger = Logger.getLogger(EditFlatPrice.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Request servlet is #EditFlatPrice from #PPAdmin at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Flat price have been updated successfully";
        String channelId = (String) request.getSession().getAttribute("_channelId");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String packageID = request.getParameter("packageId");
        String flatPriceSetting = request.getParameter("flatPriceSetting");
        logger.debug("Value of packageID  = " + packageID);
        int pId = 0;
        if (packageID != null) {
            pId = Integer.parseInt(packageID);
        }
        SgBucketdetails packObj = new PackageManagement().getPackageDetails(SessionId, channelId, pId);
        PackageManagement pkMangObj = new PackageManagement();
        try {
            if (packObj == null) {
                result = "error";
                message = "Can't edit the package details.";
                json.put("result", result);
                json.put("message", message);
                return;
            }
        } catch (Exception e) {
        }
        
        MakerChaker makerChakerObj = (MakerChaker) request.getSession().getAttribute("makerChacker");
        SgReqbucketdetails existsObj = new RequestPackageManagement().getReqPackageByName(SessionId, channelId, packObj.getBucketName());
        boolean makerChackerFlag = true;
        if (makerChakerObj == null) {
            makerChackerFlag = false;
        } else if (makerChakerObj.status == GlobalStatus.SUSPEND) {
            makerChackerFlag = false;
        }
        if (flatPriceSetting.equalsIgnoreCase("Disable")) {
            packObj.setTabShowFlag(GlobalStatus.APIThrottling);
            packObj.setFlatPrice(null);
            pkMangObj.editPackage(SessionId, packObj);
            if (makerChackerFlag == false && existsObj != null) {
                existsObj.setPartnerVisibility(packObj.getPartnerVisibility());
                existsObj.setTabShowFlag(packObj.getTabShowFlag());
                existsObj.setFlatPrice(null);
                new RequestPackageManagement().editReqPackage(SessionId, channelId, existsObj);
            }
            try {
                json.put("result", result);
                json.put("message", message);
                return;
            } finally {
                logger.info("Response of #EditFlatPrice from #PPAdmin " + json.toString());
                logger.info("Response of #EditFlatPrice from #PPAdmin Servlet at " + new Date());
                out.print(json);
                out.flush();
            }            
        }
        String accessPoint = request.getParameter("_accesspointFlatPrice");
        logger.debug("Value of AP Name  = " + accessPoint);
        String resourceName = request.getParameter("_resourceForFlatPrice");
        logger.debug("Value of resource Name  = " + resourceName);
        String version = request.getParameter("_versionForFlatPrice");
        logger.debug("Value of version Name  = " + version);
        String _flatPriceUnlimited = request.getParameter("_flatPriceUnlimited");
        logger.debug("Value of _flatPriceUnlimited Name  = " + _flatPriceUnlimited);
        String PartnerVisibility[] = request.getParameterValues("visibilityPartner");
        String apiFlatPrice = request.getParameter("apiFlatPrice");
        logger.debug("Value of apiFlatPrice Name  = " + apiFlatPrice);
        JSONArray apRateArray = new JSONArray();
        
        int retValue = 0;
        JSONObject aps = new JSONObject();        
        try {           
            String key   = accessPoint + ":" + resourceName + ":" + version;
            String value = _flatPriceUnlimited + ":"  + apiFlatPrice;
            aps.put(key, value);            
           
            String partnerId = "";
            if (PartnerVisibility != null) {
                for(int i = 0; i < PartnerVisibility.length; i++) {
                    partnerId += PartnerVisibility[i] + ",";
                }
            }
            packObj.setPartnerVisibility(partnerId);
            if (packObj.getFlatPrice() == null || packObj.getFlatPrice().equals("")) {
                apRateArray.put(aps);
                packObj.setTabShowFlag(GlobalStatus.APIThrottling);
                packObj.setFlatPrice(apRateArray.toString());
                retValue = pkMangObj.editPackage(SessionId, packObj);
            } else {
                boolean flag = true;
                String jsonString = packObj.getFlatPrice();
                JSONArray jsOld = new JSONArray(jsonString);
                for (int i = 0; i < jsOld.length(); i++) {
                    JSONObject jsonexists1 = jsOld.getJSONObject(i);
                    if (jsonexists1.has(key)) {
                        jsonexists1.remove(key);
                        jsOld.put(i, aps);
                        packObj.setFlatPrice(jsOld.toString());
                        retValue = pkMangObj.editPackage(SessionId, packObj);
                        flag = false;
                        break;
                    }
                }
                if (flag) {                    
                    jsOld.put(aps);
                    packObj.setFlatPrice(jsOld.toString());
                    retValue = pkMangObj.editPackage(SessionId, packObj);
                }
            }            
            if (retValue == -1) {
                result = "error";
                message = "Flat Price does not saved.";
                json.put("result", result);
                logger.debug("Response of #EditFlatPrice from #PPAdmin Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of #EditFlatPrice from #PPAdmin Servlet's Parameter  message is " + message);
            } else {
                if (makerChackerFlag == false && existsObj != null) {
                    existsObj.setPartnerVisibility(packObj.getPartnerVisibility());
                    existsObj.setTabShowFlag(packObj.getTabShowFlag());
                    existsObj.setFlatPrice(packObj.getFlatPrice());
                    new RequestPackageManagement().editReqPackage(SessionId, channelId, existsObj);
                }
                json.put("result", result);
                json.put("message", message);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Exception at #EditFlatPrice from #PPAdmin " + e);
        } finally {
            logger.info("Response of #EditFlatPrice from #PPAdmin " + json.toString());
            logger.info("Response of #EditFlatPrice from #PPAdmin Servlet at " + new Date());
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
