<%@include file="header.jsp" %>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgBucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement"%>

<script src="js/packageOperation.js" type="text/javascript"></script>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Package Details</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-home"></i><a href="home.jsp"> Home</a> &#47;
                    <i class="fa fa-shopping-cart"></i> Package Details &#47;                                                          
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover display nowrap">
                        <thead>
                            <tr>
                                <td/>
                                <th>No.</th>
                                <th>Status</th>
                                <th>Package</th>
                                <th>Price</th>
                                <th>Duration</th>
                                <th>Pay</th>
                                <!--<th>Manage</th>-->
                                <th>Created On</th>
                                <th>Updated On</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                int count = 0;
                                String resultInfo = "No Record Found";
                                String updationDate = "Not Updated yet";
                                PackageManagement um = new PackageManagement();
                                SgBucketdetails[] rDetails = null;
                                int pId = -1;
                                rDetails = um.listpackage(SessionId);
                                if (rDetails != null) {
                                    if (rDetails.length == 0) {
                                        rDetails = null;
                                    }
                                }
                                if (rDetails != null) {
                                    for (int i = 0; i < rDetails.length; i++) {
                                        count++;
                                        String userStatus = "user-status-value-" + i;

                            %>
                            <tr style="text-align: center">
                                <td/>
                                <td><%=count%></td>
                                <%if (rDetails[i].getStatus() == GlobalStatus.SUSPEND || rDetails[i].getStatus() == GlobalStatus.UPDATED || rDetails[i].getStatus() == GlobalStatus.ACTIVE) {%>
                                <td>
                                    <a href="#" class="btn btn-warning btn-xs btn-circle" data-toggle="tooltip" data-placement="right" title="Send request to maker"  ><i class="fa fa-thumbs-o-down"></i></a>
                                </td>
                                <% } else if (rDetails[i].getStatus() == GlobalStatus.REJECTED) { %>
                                <td>
                                    <a href="#" class="btn btn-danger btn-xs btn-circle" data-toggle="tooltip" data-placement="right" title="Request rejected"><i class="fa fa-thumbs-o-down"></i></a>
                                </td>
                                <%} else {%>
                                <td>
                                    <a href="#" class="btn btn-success btn-xs btn-circle" data-toggle="tooltip" data-placement="right" title="Request approved"><i class="fa fa-thumbs-o-up"></i></a>
                                </td>
                                <%}%>
                                <!--                                <td><span class="text-info">Not requested</span></td>-->
                                <td><%=rDetails[i].getBucketName()%></td>                                
                                <td><%=rDetails[i].getPlanAmount()%></td>
                                <td><%=rDetails[i].getBucketDuration()%></td>
                                <td><%=rDetails[i].getPaymentMode()%></td>

                                <td><%=rDetails[i].getCreationDate()%></td>
                                <%if (rDetails[i].getUpdationDate() == null) {%>
                                <td><%=updationDate%></td>
                                <%} else {%>
                                <td><%=rDetails[i].getUpdationDate()%></td>
                                <%}%>
                            </tr>
                            <%}
                                }%>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="viewRejection" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="rejectPackageModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 id="rejectPackageModal"><b>Rejection details</b></h4>
            </div>          
            <div class="modal-body">
                <div class="row-fluid">
                    <form class="form-horizontal" id="viewrejectPackageForm">
                        <fieldset>
                            <div class="control-group">
                                <label class="control-label col-lg-2"  for="partnername">Reason</label>
                                <div class="controls col-lg-10">
                                    <!--                                    <input type="hidden" readonly id="_packageName" name="_packageName" >
                                                                        <input type="hidden" readonly id="_packagestatus" name="_packagestatus" >-->
                                    <textarea id="_rejectionDetails" name="_rejectionDetails" class="form-control" rows="4" readonly></textarea>
                                </div>
                            </div>                            
                        </fieldset>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div id="edit-partner-result"></div>
                <button class="btn btn-info btn-xs" data-dismiss="modal" ><i class="fa fa-info-circle"></i> OK</button>
                <!--                <button class="btn btn-success btn-xs" onclick="rejectPackageRequest()" id="addPartnerButtonE">Reject request</button>-->
            </div>
        </div>
    </div>
</div>


<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->

</body>
</html>

<%@include file="footer.jsp" %>