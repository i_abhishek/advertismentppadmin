package com.mollatech.PPadmin.promocode;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.SgPromocode;
import com.mollatech.serviceguard.nucleus.db.connector.management.PromocodeManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "SendPromocodeToChecker", urlPatterns = {"/SendPromocodeToChecker"})
public class SendPromocodeToChecker extends HttpServlet {

    static final Logger logger = Logger.getLogger(SendPromocodeToChecker.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Request forward to Checker sucessfully!!!";
        PrintWriter out = response.getWriter();
        String promoCodeId = request.getParameter("promoCodeId");
        String promocodeReqFlag = request.getParameter("promocodeReqFlag");
        int ireqStatus = 0;
        int ipromoId = 0;
        if (promocodeReqFlag != null) {
            ireqStatus = Integer.parseInt(promocodeReqFlag);
        }
        if (promoCodeId != null) {
            ipromoId = Integer.parseInt(promoCodeId);
        }
        int a = -1;
        logger.debug("Value of promoCodeId = " + promoCodeId);

        PromocodeManagement pm = new PromocodeManagement();
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        SgPromocode promoCodeObj = pm.getPromocodeById(SessionId, channelId, ipromoId);
        if (promoCodeObj != null) {
            if (promoCodeObj.getStatus() == GlobalStatus.ACTIVE) {
                result = "error";
                message = "Promocode already approved by the checker!!!";
                json.put("_result", result);
                logger.debug("Response of #SendPromocodeToChecker from #PPAdmin Servlet's Parameter  result is " + result);
                json.put("_message", message);
                logger.debug("Response of #SendPromocodeToChecker from #PPAdmin Servlet's Parameter  message is " + message);
                out.print(json);
                out.flush();
                return;
            }
        }
        a = pm.ChangeRequestStatus(SessionId, channelId, ipromoId, ireqStatus);
        if (a == 0) {
            result = "success";
            message = "Request forward to Checker sucessfully!!!";
        } else if (a == 3) {
            result = "error";
            message = "Requet already forward to Checker  !!!";
        } else {
            result = "error";
            message = "Request not forward to checker !!!";
        }
        try {
            json.put("_result", result);
            logger.debug("Response of #SendPromocodeToChecker from #PPAdmin Servlet's Parameter  result is " + result);
            json.put("_message", message);
            logger.debug("Response of #SendPromocodeToChecker from #PPAdmin Servlet's Parameter  message is " + message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            logger.info("Response of #SendPromocodeToChecker from #PPAdmin " + json.toString());
            logger.info("Response of #SendPromocodeToChecker from #PPAdmin Servlet at " + new Date());
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
