package com.mollatech.PPadmin.reports;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.serviceguard.nucleus.db.RequestTracking;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestTrackingManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class linechartresponsereport extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        String Channel = null, sessionId = null;
        sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        int partnerid = -1, apid = -1, groupid = -1, resid = -1;
        String _accesspoint = request.getParameter("_accesspoint");
        if (_accesspoint != null) {
            apid = Integer.parseInt(_accesspoint);
        }
        String _group = request.getParameter("_group");
        if (_group != null) {
            groupid = Integer.parseInt(_group);
        }
        String _resources = request.getParameter("_resources");
        if (_resources != null) {
            resid = Integer.parseInt(_resources);
        }
        String _partner = request.getParameter("_partner");
        if (_partner != null) {
            partnerid = Integer.parseInt(_partner);
        }
        String _sdate = request.getParameter("_sdate");
        String _edate = request.getParameter("_edate");
        String _stime = request.getParameter("_stime");
        String _etime = request.getParameter("_etime");
        RequestTracking[] res = null;
        String dates1 = null;
        long responsetime = 0;
        String datess = null;
        DateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
            ArrayList<line> sample = new ArrayList<line>();
            res = new RequestTrackingManagement().getResponseTimebyDuration(sessionId, Channel, apid, resid, groupid, partnerid, _sdate, _edate, _stime, _etime);
            if (res != null && res.length != 0) {
                for (int i = 0; i < res.length; i++) {
                    responsetime = Integer.parseInt(res[i].getResponsetime());
                    datess = formatter1.format(res[i].getCreatedOn());
                    if (!datess.equals(dates1)) {
                        sample.add(new line(responsetime, datess));
                    }
                    dates1 = datess;
                }
            } else {
                Date d = new Date();
                datess = formatter1.format(d);
                sample.add(new line(responsetime, datess));
            }
            Gson gson = new Gson();
            JsonElement element = gson.toJsonTree(sample, new TypeToken<List<line>>() {
            }.getType());
            JsonArray jsonArray = element.getAsJsonArray();
            out.print(jsonArray);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
