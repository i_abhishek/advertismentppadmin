<%--<%@page import="com.partner.web.NucleusSetting"%>--%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Monitortracking"%>
<%@page import="com.mollatech.serviceguard.nucleus.settings.NucleusMonitorSettings"%>
<%@page import="com.mollatech.service.nucleus.crypto.AxiomProtect"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Monitorsettings"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.MonitorSettingsManagement"%>
<%@page import="javax.xml.datatype.DatatypeFactory"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="javax.xml.datatype.XMLGregorianCalendar"%>
<%--<%@page import="portalconnect.PartnerPortalWrapper"%>
<%@page import="com.partner.web.Monitortracking"%>
<%@page import="com.partner.web.Monitorsettings"%>--%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<script src="./assets/js/addSettings.js"></script>
<div class="tab-content">
    <div class="tab-pane active" id="primary">
        <%!
            public String ChechFor = "";
        %> 
        <%
           // PartnerPortalWrapper ppw = new PartnerPortalWrapper();
            MonitorSettingsManagement ppw = new MonitorSettingsManagement();
            int types = 1;
            String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
            String channelId = (String) request.getSession().getAttribute("_channelId");
            String _settingId = request.getParameter("_settingId");
            int settingId = Integer.parseInt(_settingId);
            Monitorsettings ms = ppw.getMonitorSettingbyId(sessionId, channelId, settingId);
            int type = ms.getMonitorType();
            String settingName = ms.getMonitorName();
            String start = request.getParameter("_startdate");
            String end = request.getParameter("_enddate");
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date startdate = sdf.parse(start);
            GregorianCalendar st = new GregorianCalendar();
            st.setTime(startdate);
            XMLGregorianCalendar XMLsDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(st);
            Date enddate = sdf.parse(end);
            GregorianCalendar et = new GregorianCalendar();
            st.setTime(enddate);
            XMLGregorianCalendar XMLeDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(et);
            Monitorsettings msObj = ppw.getMonitorSettingbyId(sessionId, channelId, settingId);
            //NucleusSetting nms
            byte[] obj = msObj.getMonitorSettingEntry();
            byte[] f = AxiomProtect.AccessDataBytes(obj);
            ByteArrayInputStream bais = new ByteArrayInputStream(f);
            Object object = ppw.deserializeFromObject(bais);
            NucleusMonitorSettings nms = null;
            if (object instanceof NucleusMonitorSettings) {
                nms = (NucleusMonitorSettings) object;
            }
            Monitortracking[] monitortrackings = ppw.getMonitorTrackByNameDuration(sessionId, channelId, settingName, startdate, enddate, types);
            session.setAttribute("monitortrackings", monitortrackings);
        %>
        <div class="container-fluid">           
            <div class="row-fluid">
                <div id="licenses_data_table">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                        <tr>
                            <th>No.</th>
                            <th>Monitor Name</th>
                            <th>Monitor Type</th>
                            <th>Check For</th>
                            <th>Performance</th>
                            <th>Executed On</th>
                        </tr>
                            </thead>
                        <% int l = 25;
                            if (monitortrackings != null) {
                                if (monitortrackings.length < 25) {
                                    l = monitortrackings.length;
                                }
                                for (int i = 0; i < l; i++) {
                                    String performance = null;
                                    String p = monitortrackings[i].getPerformance();
                                    if (p.equals("0ms")) {
                                        performance = "Not Reachable";
                                    } else {
                                        performance = monitortrackings[i].getPerformance();
                                    }
                        %>
                        <tr>
                            <td><%=i + 1%></td>
                            <td><%= ms.getMonitorName()%></td>
                            <td><%= getStringType(type, nms)%></td>
                            <td><%= ChechFor%></td>
                            <td><%= performance%></td>
                            <td><%= monitortrackings[i].getExecutionStartOn()%></td>
                        </tr>
                        <% }
                        } else {%>
                        <tr>
                            <td>1</td>
                            <td>No Record Found</td>
                            <td>No Record Found</td>
                            <td>No Record Found</td>
                            <td>No Record Found</td>
                            <td>No Record Found</td>
                        </tr>
                        <% }
                        %>
                    </table>
                </div>
            </div>
        </div>
        <table style="border-spacing: 50%;">
            <tr>
                <td style="padding-right: 10px;"> 
                    <a href="#" class="btn btn-success" onclick="ReportCSV('<%= ms.getMonitorName()%>',<%= type%>, '<%= ChechFor%>')" >
                        <i class="fa fa-file-excel-o"></i> Download CSV</a>
                </td>
                
                <td style="padding-right: 10px;">
                    <a href="#" class="btn btn-info" onclick="ReportPDF('<%= ms.getMonitorName()%>',<%= type%>, '<%= ChechFor%>')" >
                        <i class="fa fa-file-pdf-o"></i> Download PDF</a>
                </td>
                
<!--                <td >
                    <a href="#" class="btn btn-warning" onclick="ReportTXT('<%= ms.getMonitorName()%>',<%= type%>, '<%= ChechFor%>')" >
                        <i class="fa fa-file-text-o"></i> Download TXT</a>
                </td>-->
            </tr>
        </table>
        <%! public String getStringType(int type, NucleusMonitorSettings nms) {

                String types = "";
                if (type == 1) {
                    types = "Web Site";
                    ChechFor = nms.getWebadd();
                } else if (type == 3) {
                    types = "Ping Monitor";
                    ChechFor = nms.getPinghost();
                }
                return types;
            }
        %>
    </div>
</div>
</div>
</div>
<script src="bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript 
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.min.js"></script>
<script src="js/morris-data.js"></script>-->

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>
<script src="js/bootbox.min.js" type="text/javascript"></script>
<script>
                                            $(document).ready(function () {
                                                $('#dataTables-example').DataTable({
                                                    responsive: true
                                                });
                                            });
</script>                         