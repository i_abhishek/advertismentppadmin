<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@include file="header.jsp" %>
<script src="js/partyManager.js" type="text/javascript"></script>

<div id="page-wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Add Party</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-home"></i><a href="home.jsp"> Home</a> &#47;
                <i class="fa fa-group"></i><a href="partyDetails.jsp"> Party Details</a> &#47;
                <i class="fa fa-edit"></i> Party Details
            </div>
            
            <div class="panel-body">
                <h4>Party details</h4>
                <hr>                
                <form class="form-horizontal" id="party_form" name="party_form" role="form" method="POST">
                    <div class="form-group">
                        <label class="control-label col-lg-2">Name <span style="color: red">*</span></label>
                        <div class="col-lg-3">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" id="_partyName" name="_partyName" class="form-control" placeholder="Party name" onblur="checkPartyAvailability()">
                            </div>
                            <div id="checkAvailability-result">
                            </div>
                        </div>
                        <label class="control-label col-lg-2">Company Name <span style="color: red">*</span></label>
                        <div class="col-lg-3">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-university"></i></span>
                                <input type="text" id="_companyName" name="_companyName" class="form-control" placeholder="Company name">                                    
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-2">Phone Number <span style="color: red">*</span></label>
                        <div class="col-lg-3">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                <input type="text" id="_phoneNumber" name="_phoneNumber" class="form-control" placeholder="Phone Number">                                    
                            </div>
                        </div>
                        <label class="control-label col-lg-2">Email Id <span style="color: red">*</span></label>
                        <div class="col-lg-3">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                                <input type="text" id="_emailId" name="_emailId" class="form-control" placeholder="Email Id">                                    
                            </div>
                        </div>                       
                    </div>
                    <a  class="btn btn-success btn-xs" onclick="createParty()"><i class="fa fa-plus-circle"></i> Add details</a>
                </form>
            </div>
        </div>
    </div>
</div>  

<script>


</script>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="dist/js/sb-admin-2.js"></script>
<script src="select2/select2.js" type="text/javascript"></script>
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>
<script src="js/bootbox.min.js" type="text/javascript"></script>  
</body>
</html>       
<%@include file="footer.jsp" %>