package com.mollatech.PPadmin.resource;

import com.mollatech.serviceguard.nucleus.db.SgResourceowner;
import com.mollatech.serviceguard.nucleus.db.connector.management.ResourceOwnerManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.json.simple.JSONArray;

/**
 *
 * @author Ash
 */
@WebServlet(name = "UpdateOwnerInfo", urlPatterns = { "/UpdateOwnerInfo" })
public class UpdateOwnerInfo extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    static final Logger logger = Logger.getLogger(UpdateOwnerInfo.class);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        logger.info("Requested Servlet is UpdateOwnerInfo at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        JSONObject json = new JSONObject();
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        try {
            int resId = Integer.parseInt(request.getParameter("resId"));
            logger.debug("value of resId : " + resId);
            String[] allResourceDe = request.getParameterValues("resourceDe");
            logger.debug("value of resourceDe : " + allResourceDe);
            String resourceDe = request.getParameter("assignedElement");
            logger.debug("value of resourceDe : " + resourceDe);
            JSONObject jsono = new JSONObject();
            float owner = 0;
            float partyA = 0;
            float partyB = 0;
            float partyC = 0;
            float partyD = 0;
            if (resourceDe == null) {
                json.put("result", "error");
                json.put("message", "Please Select Minimum One Resource.");
                return;
            }
            String res = ",";
            for (int i = 0; i < allResourceDe.length; i++) {
                res += allResourceDe[i] + ",";
            }
            String ownerS = request.getParameter("_ownerRev");
            logger.debug("value of _ownerRev : " + ownerS);
            String pA = request.getParameter("_aPartyRev");
            logger.debug("value of _aPartyRev : " + pA);
            String pB = request.getParameter("_bPartyRev");
            logger.debug("value of _bPartyRev : " + pB);
            String pC = request.getParameter("_cPartyRev");
            logger.debug("value of _cPartyRev : " + pC);
            String pD = request.getParameter("_dPartyRev");
            logger.debug("value of _dPartyRev : " + pD);
            
            String firstPartyId = request.getParameter("_selectFirstParties");
            logger.debug("value of firstPartyId : " + firstPartyId);
            String secondPartyId = request.getParameter("_selectSecondParties");
            logger.debug("value of secondParty : " + secondPartyId);
            String thirdPartyId = request.getParameter("_selectThirdParties");
            logger.debug("value of thirdPartyId : " + thirdPartyId);
            String fourthPartyId = request.getParameter("_selectFourthParties");
            logger.debug("value of fourthPartyId : " + fourthPartyId);
            
            if (ownerS.equals("")) {
                json.put("result", "error");
                json.put("message", "Enter Owner Revenue Details");
                return;
            }
            owner = Float.parseFloat(ownerS);
            if (!pA.equals("")) {
                partyA = Float.parseFloat(pA);
            }
            if (!pB.equals("")) {
                partyB = Float.parseFloat(pB);
            }
            if (!pC.equals("")) {
                partyC = Float.parseFloat(pC);
            }
            if (!pD.equals("")) {
                partyD = Float.parseFloat(pD);
            }
            if ((owner + partyA + partyB + partyC + partyD) != 100) {
                json.put("result", "error");
                json.put("message", "Revenue Total is not 100.");
                return;
            }
            jsono.put("_dPartyRev", "" + partyD);
            jsono.put("_fourthParty",""+ fourthPartyId);
            jsono.put("_cPartyRev", "" + partyC);
            jsono.put("_thirdParty", ""+thirdPartyId);
            jsono.put("_bPartyRev", "" + partyB);
            jsono.put("_secondParty",""+ secondPartyId);
            jsono.put("_aPartyRev", "" + partyA );
            jsono.put("_firstParty", ""+firstPartyId);
            jsono.put("_ownerRev", "" + owner);
            JSONObject revenueListJson = new JSONObject();
            SgResourceowner SgResourceowner = new ResourceOwnerManagement().getResourceownerbyId(SessionId, resId);

            revenueListJson.put(resourceDe, jsono);
            JSONArray revenueArray = new JSONArray();
            if (SgResourceowner.getData() == null || SgResourceowner.getData().equals("")) {
                revenueArray.add(revenueListJson);
                SgResourceowner.setData(revenueArray.toString());
            }else{
                 boolean flag = true;
                String jsonString = SgResourceowner.getData();
                org.json.JSONArray jsOld = new org.json.JSONArray(jsonString);                
                for (int i = 0; i < jsOld.length(); i++) {
                    org.json.JSONObject jsonexists1 = jsOld.getJSONObject(i);
                    if (jsonexists1.has(resourceDe)) {
                        jsonexists1.remove(resourceDe);
                        jsonexists1.put(resourceDe, jsono);
                        jsOld.put(i, jsonexists1);
                        SgResourceowner.setData(jsOld.toString());
                        flag = false;
                        break;
                    }
                }
                if (flag) {
                    org.json.JSONObject jsObject = new org.json.JSONObject();
                    jsObject.put(resourceDe, jsono);
                    org.json.JSONArray jsonArrayObject = new org.json.JSONArray(jsonString);
                    jsonArrayObject.put(jsObject);
                    SgResourceowner.setData(jsonArrayObject.toString());
                }                                      
            }            
            SgResourceowner.setResourceName(res);
            int result = new ResourceOwnerManagement().updateDetails(SgResourceowner);
            if (result == 0) {
                json.put("result", "success");
                json.put("message", "Revenue Sharing Details Updated Successfully.");
                return;
            } else {
                json.put("result", "error");
                json.put("message", "Error in Saving Revenue Sharing Details.");
                return;
            }
        } catch (Exception ex) {
            logger.error("Exception at UpdateOwnerInfo ", ex);
            json.put("result", "error");
            json.put("message", "Error in Saving API Price.");
        } finally {
            logger.info("Response of UpdateOwnerInfo " + json.toString());
            logger.info("Response of UpdateOwnerInfo Servlet at " + new Date());
            out.print(json);
            out.flush();
            out.close();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
