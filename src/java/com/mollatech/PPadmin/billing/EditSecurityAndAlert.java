package com.mollatech.PPadmin.billing;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.commons.MakerChaker;
import com.mollatech.serviceguard.nucleus.db.Operators;
import com.mollatech.serviceguard.nucleus.db.SgBucketdetails;
import com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "EditSecurityAndAlert", urlPatterns = { "/EditSecurityAndAlert" })
public class EditSecurityAndAlert extends HttpServlet {

    static final Logger logger = Logger.getLogger(EditSecurityAndAlert.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Request servlet is #EditSecurityAndAlert from #PPAdmin at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Security and alert are saved successfully";
        Operators operators = (Operators) request.getSession().getAttribute("_apOprDetail");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String packageID = request.getParameter("packageId");
        logger.debug("Value of packageID  = " + packageID);
        int pId = 0;
        if (packageID != null) {
            pId = Integer.parseInt(packageID);
        }
        SgBucketdetails packObj = new PackageManagement().getPackageDetails(SessionId, channelId, pId);
        try {
            if (packObj == null) {
                result = "error";
                message = "Can't edit the package details.";
                json.put("result", result);
                json.put("message", message);
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String signUpAlertMesssage = request.getParameter("signUpAlertMesssage");
        String trialPackageAlertMesssage = request.getParameter("trialPackageAlertMesssage");
        String changeOnPackageAlertMesssage = request.getParameter("changeOnPackageAlertMesssage");
        String latePenaltiesAlertMesssage = request.getParameter("latePenaltiesAlertMesssage");
        String terOfServiceAlertMesssage = request.getParameter("terOfServiceAlertMesssage");
        String reActivationAlertMesssage = request.getParameter("reActivationAlertMesssage");
        String lowBalanceMesssage = request.getParameter("lowBalanceMesssage");
//        String encryptedPDFCheck = request.getParameter("encryptedPDFCheck");
//        String pdfSigningCheck = request.getParameter("pdfSigningCheck");
        String pdfFeature = request.getParameter("pdfFeature");
        logger.debug("Value of signUpAlertMesssage = " + signUpAlertMesssage);
        logger.debug("Value of trialPackageAlertMesssage = " + trialPackageAlertMesssage);
        logger.debug("Value of changeOnPackageAlertMesssage = " + changeOnPackageAlertMesssage);
        logger.debug("Value of latePenaltiesAlertMesssage = " + latePenaltiesAlertMesssage);
        logger.debug("Value of terOfServiceAlertMesssage = " + terOfServiceAlertMesssage);
        logger.debug("Value of reActivationAlertMesssage = " + reActivationAlertMesssage);
        logger.debug("Value of lowBalanceMesssage = " + lowBalanceMesssage);
//        logger.debug("Value of encryptedPDFCheck = " + encryptedPDFCheck);
//        logger.debug("Value of pdfSigningCheck = " + pdfSigningCheck);
        logger.debug("Value of pdfFeature = " + pdfFeature);
        JSONObject alertMessageJSON = new JSONObject();
        alertMessageJSON.put("encryptedPDF", "disable");
        alertMessageJSON.put("pdfSigning", "disable");
        if (pdfFeature != null && pdfFeature.equalsIgnoreCase("encryptedPDFEnable")) {
            alertMessageJSON.put("encryptedPDF", "enable");
        }
        if (pdfFeature != null && pdfFeature.equalsIgnoreCase("pdfSigningEnable")) {
            alertMessageJSON.put("pdfSigning", "enable");
        }
        alertMessageJSON.put("signUpAlertMesssage", signUpAlertMesssage);
        alertMessageJSON.put("trialPackageAlertMesssage", trialPackageAlertMesssage);
        alertMessageJSON.put("changeOnPackageAlertMesssage", changeOnPackageAlertMesssage);
        alertMessageJSON.put("latePenaltiesAlertMesssage", latePenaltiesAlertMesssage);
        alertMessageJSON.put("terOfServiceAlertMesssage", terOfServiceAlertMesssage);
        alertMessageJSON.put("reActivationAlertMesssage", reActivationAlertMesssage);
        alertMessageJSON.put("lowBalanceMesssage", lowBalanceMesssage);
        JSONObject singleMessageJson = new JSONObject();
        singleMessageJson.put(packObj.getBucketName(), alertMessageJSON);
        JSONArray securityAlertArray = new JSONArray();
        int retValue = 0;
        PackageManagement pkMangObj = new PackageManagement();
        try {
            //if (packObj.getTabShowFlag() != GlobalStatus.FEATURE) {
                packObj.setTabShowFlag(GlobalStatus.FEATURE);
           // }
            String PartnerVisibility[] = request.getParameterValues("visibilityPartner");
            String partnerId = "";
            if (PartnerVisibility != null) {
                for (int i = 0; i < PartnerVisibility.length; i++) {
                    partnerId += PartnerVisibility[i] + ",";
                }
            }
            packObj.setPartnerVisibility(partnerId);
            if (packObj.getSecurityAndAlertDetails() == null || packObj.getSecurityAndAlertDetails().equals("")) {
                securityAlertArray.put(singleMessageJson);
                packObj.setSecurityAndAlertDetails(securityAlertArray.toString());
                retValue = pkMangObj.editPackage(SessionId, packObj);
            } else {
                boolean flag = true;
                String jsonString = packObj.getSecurityAndAlertDetails();
                JSONArray jsOld = new JSONArray(jsonString);
                for (int i = 0; i < jsOld.length(); i++) {
                    JSONObject jsonexists1 = jsOld.getJSONObject(i);
                    if (jsonexists1.has(packageID)) {
                        jsonexists1.remove(packageID);
                        jsonexists1.put(packageID, alertMessageJSON);
                        jsOld.put(i, jsonexists1);
                        packObj.setSecurityAndAlertDetails(jsOld.toString());
                        retValue = pkMangObj.editPackage(SessionId, packObj);
                        flag = false;
                        break;
                    }
                }
                if (flag) {
                    JSONObject jsObject = new JSONObject();
                    jsObject.put(packageID, alertMessageJSON);
                    JSONArray jsonArrayObject = new JSONArray(jsonString);
                    jsonArrayObject.put(jsObject);
                    packObj.setSecurityAndAlertDetails(jsonArrayObject.toString());
                    retValue = pkMangObj.editPackage(SessionId, packObj);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            MakerChaker makerChakerObj = (MakerChaker) request.getSession().getAttribute("makerChacker");
            SgReqbucketdetails existsObj = new RequestPackageManagement().getReqPackageByName(SessionId, channelId, packObj.getBucketName());
            boolean makerChackerFlag = true;
            if (makerChakerObj == null) {
                makerChackerFlag = false;
            } else if (makerChakerObj.status == GlobalStatus.SUSPEND) {
                makerChackerFlag = false;
            }
            if (retValue != 0) {
                result = "error";
                message = "Security and alert details does not saved.";
                json.put("result", result);
                logger.debug("Response of #EditSecurityAndAlert from #PPAdmin Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of #EditSecurityAndAlert from #PPAdmin Servlet's Parameter  message is " + message);
            } else {
                if (makerChackerFlag == false && existsObj != null) {
                    existsObj.setPartnerVisibility(packObj.getPartnerVisibility());
                    existsObj.setTabShowFlag(packObj.getTabShowFlag());
                    existsObj.setSecurityAndAlertDetails(packObj.getSecurityAndAlertDetails());
                    new RequestPackageManagement().editReqPackage(SessionId, channelId, existsObj);
                }
                json.put("result", result);
                json.put("message", message);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Exception at #EditSecurityAndAlert from #PPAdmin " + e);
        } finally {
            logger.info("Response of #EditSecurityAndAlert from #PPAdmin " + json.toString());
            logger.info("Response of #EditSecurityAndAlert from #PPAdmin Servlet at " + new Date());
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
