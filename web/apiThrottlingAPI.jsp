<%@page import="java.math.BigDecimal"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgBucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Warfiles"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.AccessPolicy"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MethodName"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Classes"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Methods"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        String apId = request.getParameter("_apId");
        String versionData = request.getParameter("versionData");
        String _resourceId = request.getParameter("_resourceId");
        String packageId = request.getParameter("packageIDThro");
        int pId = 0;
        if (packageId != null) {
            pId = Integer.parseInt(packageId);
        }
        SgBucketdetails pacObject = new PackageManagement().getPackageDetails(SessionId, channelId, pId);
        if (apId != null && !apId.equals("-1") && !_resourceId.equals("-1")) {
            Accesspoint accesspoint = new AccessPointManagement().getAccessPointByName(SessionId, channelId, apId);
            String apName = accesspoint.getName();
            Map classMap = null;
            Map methodMap = null;
            String value = apName;

//            byte[] accessPolicy = accesspoint.getLaccessPolicyEntry();
//            AccessPolicy ap = (AccessPolicy) Serializer.deserialize(accessPolicy);
            int limitForApi = 0;
//            if (ap != null) {
//                amountOfApi = ap.Amount;
//            }
            //new changee
            ResourceDetails rd = new ResourceManagement().getResourceByName(SessionId, channelId, _resourceId);
            
            TransformDetails tf = new TransformManagement().getTransformDetails(SessionId, channelId, accesspoint.getApId(), rd.getResourceId());
            Warfiles warfiles = new WarFileManagement().getWarFile(SessionId, channelId, accesspoint.getApId());
            int version = 1;
            if (warfiles != null) {
                Map map = (Map) Serializer.deserialize(warfiles.getWfile());
                version = map.size() / 2;
            }
            byte[] meBy = tf.getMethods();
            Object obj = Serializer.deserialize(meBy);
            Map tmpMethods = (Map) obj;
            Methods methods = (Methods) tmpMethods.get("" + versionData);

            if (methodMap == null) {
                methodMap = new HashMap();
            }

            methodMap.put(value, methods);
            String mName = "";
            List list = methods.methodClassName.methodNames;
            int count = 0;
            JSONObject reqJSONObj = null;
            //new change
            if (pacObject != null && pacObject.getApiThrottling() != null) {
                String apRateDetails = pacObject.getApiThrottling();
                String key = apName + ":" + _resourceId + ":" + versionData;

                JSONArray jsOld = new JSONArray(apRateDetails);
                for (int j = 0; j < jsOld.length(); j++) {
                    JSONObject jsonexists1 = jsOld.getJSONObject(j);
                    if (jsonexists1.has(key)) {
                        reqJSONObj = jsonexists1.getJSONObject(key);
                        if (reqJSONObj != null) {
                            break;
                        }
                    }
                }
            }
    %>

    <%

        for (int i = 0; i < list.size(); i++) {

            MethodName methodName = (MethodName) list.get(i);
            if (methodName.visibility.equalsIgnoreCase("yes")) {
                count++;
                mName = methodName.methodname.split("::")[0];
                if (!methodName.transformedname.equals("")) {
                    mName = methodName.transformedname.split("::")[0];
                }
                String methodNameStr = mName;
                if (reqJSONObj != null) {
                    String methodN = reqJSONObj.getString(methodNameStr);
                    limitForApi = Integer.parseInt(methodN);
                }

    %>

    <div class="form-group">
        <label class=" control-label col-lg-5"><%=methodNameStr%></label>
        <div class="col-lg-3" >
            <div class="input-group">
                <span class="input-group-addon">$</span>
                <input onkeypress="return isNumericKey(event)"  type="text" class="form-control" placeholder="apiPrice" id="apiPrice<%=count-1%>" name="apiPrice<%=count-1%>" value="<%=limitForApi%>">                                    
                <input type="hidden" name="apiName<%=count-1%>" id="apiName<%=count-1%>" value="<%=methodNameStr%>">                                
            </div>
        </div>
    </div>

    <%
            }
        }

    %>
    <input type="hidden" name="apNameRate" id="apNameRate" value="<%=apName%>">
    <input type="hidden" name="paramcountAPR" id="paramcountAPR" value="<%=count%>">
    <%} else {%>
    <p style="display: none"> NO Record Found</p>  
    <% }%>



