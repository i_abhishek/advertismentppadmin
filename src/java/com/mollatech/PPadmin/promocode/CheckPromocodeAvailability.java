/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.promocode;

import com.mollatech.serviceguard.nucleus.db.SgPromocode;
import com.mollatech.serviceguard.nucleus.db.connector.management.PromocodeManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "CheckPromocodeAvailability", urlPatterns = {"/CheckPromocodeAvailability"})
public class CheckPromocodeAvailability extends HttpServlet {

    static final Logger logger = Logger.getLogger(CheckPromocodeAvailability.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Requested Servlet is CheckPromocodeAvailability from PPAdmin at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Promocode available";
        String channels = (String) request.getSession().getAttribute("_channelId");
        String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String _promocode = request.getParameter("_promocode");

        try {
            if (_promocode == null || _promocode.isEmpty()) {
                result = "error";
                message = "Please enter promocode";
                json.put("result", result);
                logger.debug("Response of CheckPromocodeAvailability Servlet's from PPAdmin Parameter   result is " + result);
                json.put("message", message);
                logger.debug("Response of CheckPromocodeAvailability Servlet's from PPAdmin Parameter   message is " + message);
                return;
            }
            SgPromocode promoObj = new PromocodeManagement().getPromocodeByPromocode(sessionId, channels, _promocode);
            if (promoObj != null) {
                result = "error";
                message = "This Promocode Is Not Available";
                json.put("result", result);
                logger.debug("Response of CheckPromocodeAvailability Servlet's from PPAdmin Parameter   result is " + result);
                json.put("message", message);
                logger.debug("Response of CheckPromocodeAvailability Servlet's from PPAdmin Parameter   message is " + message);
                return;
            } else {
                result = "success";
                message = "This Promocode Is Available";
                json.put("result", result);
                logger.debug("Response of CheckPromocodeAvailability Servlet's from PPAdmin Parameter   result is " + result);
                json.put("message", message);
                logger.debug("Response of CheckPromocodeAvailability Servlet's from PPAdmin Parameter   message is " + message);
                return;
            }
        } catch (Exception e) {
            logger.error("Exception at CheckPromocodeAvailability ", e);
            result = "error";
            message = "Error in Checking Availability";
            json.put("result", result);
            logger.debug("Response of CheckPromocodeAvailability Servlet's from PPAdmin Parameter   result is " + result);
            json.put("message", message);
            logger.debug("Response of CheckPromocodeAvailability Servlet's from PPAdmin Parameter   message is " + message);
        } finally {
            logger.info("Response of CheckPromocodeAvailability from PPAdmin" + json.toString());
            logger.info("Response of CheckPromocodeAvailability Servlet from PPAdmin at " + new Date());
            out.print(json);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
