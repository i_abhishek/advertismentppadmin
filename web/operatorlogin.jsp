<%@page import="java.text.SimpleDateFormat"%>
<html lang="en">
    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Developer Admin</title>

        <!-- Bootstrap Core CSS -->
        <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <!-- MetisMenu CSS -->
        <link href="bower_components/metisMenu/dist/metisMenu.css" rel="stylesheet" type="text/css"/>
        <!-- Timeline CSS -->
        <link href="dist/css/timeline.css" rel="stylesheet" type="text/css"/>
        <!-- Custom CSS -->
        <link href="dist/css/sb-admin-2.css" rel="stylesheet" type="text/css"/>
        <!-- Morris Charts CSS -->
        <link href="bower_components/morrisjs/morris.css" rel="stylesheet" type="text/css"/>
        <!-- Custom Fonts -->
        <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="js/jquery.js"></script>
        <script src="js/jquery-ui-1.10.4.min.js"></script>
        <script src="js/jquery-1.8.3.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
        <script src="js/bootbox.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js"></script> 
        <!--        <script src="./js/operators.js"></script>-->
        <script src="./js/operatorsOLD2.js"></script>
        <script src="./js/modal.js"></script>
    </head>
    <body>
        <div class="container">
            <div id = "alert_placeholder"></div>
            <div class="login-panel-show">
                <div class="col-md-6">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <b> ADMIN LOGIN</b>
                        </div>
                        <div class="panel-body">
                            <form class="login-form" id="login_form" action="home.jsp">
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input type="text" class="form-control" id="_name" name="_name" placeholder="Username" tabindex="0" autofocus onblur="showImage()">
                                </div>
                                <div class="form-group input-group">
                                    <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                    <input type="password" class="form-control" id="_passwd" name="_passwd" tabindex="0" placeholder="Password">  
                                </div>
                                <a href="#/"class="btn btn-success" id ="loginData" onclick="opratorlogin()" tabindex="0"><i class="fa fa-check"></i> Login</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--        New ly added-->


        <script>
            $(document).ready(function () {
                checkCookie();
            });
            function getCookie(cname) {
                var name = cname + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) === ' ')
                        c = c.substring(1);
                    if (c.indexOf(name) === 0)
                        return c.substring(name.length, c.length);
                }
                return "";
            }
            function checkCookie() {
                var checked = getCookie("OperatorChecked");
                if (checked === "1") {
                    document.getElementById("rememberpasswordoperator").checked = true;
                    var username = getCookie("Operatorusername");
                    var password = getCookie("Operatorpassword");
                    document.getElementById("_name").value = username;
                    document.getElementById("_passwd").value = password;
                }
            }
            function bootboxmodel(content) {
                //alert("Hi ");
                // showAlert(content, "info", 3000);
                var popup = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
                        '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' +
                        '<button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button><br>' +
                        content +
                        '<br><a class="btn btn-primary" data-dismiss="modal" class="close"> &nbsp; OK </a></div></div></div></div>');
                //popup.modal();
            }
            //    function pleasewait() {
            //        var waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            //                '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h2>Please Wait</h2>' +
            //                '</div></div></div></div>');
            //        waiting.modal();
            //    }
            function showAlert(message, type, closeDelay) {
                if ($("#alerts-container").length == 0) {
                    // alerts-container does not exist, create it
                    $("body")
                            .append($('<div id="alerts-container" style="position: fixed;' +
                                    'width: 50%; left: 25%; top: 10%;">'));
                }
                // default to alert-info; other options include success, warning, danger
                type = type || "info";

                // create the alert div
                var alert = $('<div class="alert alert-' + type + ' fade in">')
                        .append(
                                $('<button type="button" class="close" data-dismiss="alert">')
                                .append("&times;")
                                )
                        .append(message);

                // add the alert div to top of alerts-container, use append() to add to bottom
                $("#alerts-container").prepend(alert);

                // if closeDelay was passed - set a timeout to close the alert
                if (closeDelay)
                    window.setTimeout(function () {
                        alert.alert("close")
                    }, closeDelay);

            }

        </script>
        <%
            java.util.Date dFooter = new java.util.Date();
            SimpleDateFormat sdfFooter = new SimpleDateFormat("yyyy");
            String strYYYY = sdfFooter.format(dFooter);
            SimpleDateFormat tz = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
            String completeTimewithLocalTZ = tz.format(dFooter);
            long LongTime = dFooter.getTime() / 1000;

        %>
        <footer>
            <div align="center" style="margin-top: 10%">
                <p>&copy; Molla Technologies 2009-<%=strYYYY%> <a href="http://www.mollatech.com" target="_blank"> (www.mollatech.com)</a></p>
                <p>Local Date and Time::<%=completeTimewithLocalTZ%> (<%=LongTime%>)</p>                
            </div>
        </footer>
    </body>
</html>