<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.CaaSManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgCassNumbers"%>
<%@page import="java.util.List"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgNumbers"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.NumbersManagement"%>
<!DOCTYPE html>
<%@include file="header.jsp" %>
<html>
    <head>
        <link href="css/select2.css" rel="stylesheet" type="text/css"/>
        <link href="./select2/select2.css" rel="stylesheet"/>
        <script src="js/caasNumbers.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin Portal</title>

    </head>

    <%
        String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelid = (String) request.getSession().getAttribute("_channelId");
        SgCassNumbers[] numberDetails = new CaaSManagement().getAllNumbers();
        PartnerDetails[] partnerDetails = new PartnerManagement().getAllPartnerDetails(sessionId, channelid);
        List list = new CaaSManagement().getEnvTypes();
    %>
    <body>
        <div id="wrapper">
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="page-header text-uppercase">Caas Number Management</h3>
                    </div>
                    <!-- /.col-lg-10 -->
                </div>
                <div class="row">
                    <div id="alerts-container" style="width: 50%; left: 25%; top: 2%;margin-left: 25%"></div>
                    <div class="col-lg-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47;  <i class="fa fa-table"></i><b> Assign Numbers</b>
                            </div>
                            <div class="panel-body">
                                <div class="col-lg-12">
                                    <form class="form-horizontal" id="partnerInfoForm" name="partnerInfoForm" role="form" method="POST">
                                        <br>
                                        <div class="form-group">
                                            <div class="col-lg-offset-0">
                                                <label class="control-label col-lg-2">Developer Name</label>
                                            </div>
                                            <div class="col-lg-2">
                                                <select class="form-control" id="partnerde" name="partnerde" onchange="acesspointTabResource()"> 
                                                    <option value="-1" selected>Select</option>
                                                    <%                                        if (partnerDetails != null) {
                                                            for (PartnerDetails details : partnerDetails) {
                                                    %>
                                                    <option value="<%=details.getPartnerName()%>"><%=details.getPartnerName()%></option>
                                                    <%}
                                                        }%>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-offset-0">
                                                <label class="control-label col-lg-2">Number Type</label>
                                            </div>
                                            <div class="col-lg-2">
                                                <select class="form-control" id="numberType" name="numberType" onchange="acesspointTabResource()"> 
                                                    <option value="-1" selected>Select</option>
                                                    <%                                        if (list != null) {
                                                            for (Object details : list) {
                                                    %>
                                                    <option value="<%=details%>"><%=details%></option>
                                                    <%}
                                                        }%>

                                                </select>
                                            </div>
                                        </div>
                                        <br>    
                                        <div class="form-group">
                                            <div class="col-lg-offset-0">
                                                <label id="assignLabel" class="control-label col-lg-2" style="display: none">Assign Number (s)</label>
                                            </div>
                                            <div class="col-lg-8">
                                                <select  id="NumberForPartners" name="NumberForPartners" multiple="multiple" style="display: none">
                                                </select>
                                                <br>
                                            </div>
                                            <div class="col-lg-2 col-lg-offset-2">
                                                <font style="font-size: 11px"><a style="display: none" class="btn btn-success btn-xs" id="register" onclick="updateCaasNo()"><i class="fa fa-dropbox"></i> Save</a> </font>
                                            </div>
                                            <br>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function acesspointTabResource() {
                var value = document.getElementById('partnerde').value;
                var value1 = document.getElementById('numberType').value;
                if (value === '-1' || value1 === '-1') {
                    document.getElementById('register').style.display = 'none';
                    $('#NumberForPartners').html("");
                    $('#NumberForPartners').select2("close").parent().hide();
                    document.getElementById('NumberForPartners').style.display = 'none';
                    document.getElementById('assignLabel').style.display = 'none';
                    return;
                } else {
                    document.getElementById('register').style.display = 'block';
                    document.getElementById('NumberForPartners').style.display = 'block';
                    document.getElementById('assignLabel').style.display = 'block';
                }
                var s = './caasNumbers.jsp?partnerName=' + value + "&type=" + value1;
                $.ajax({
                    type: 'GET',
                    url: s,
                    success: function (data) {
                        $('#NumberForPartners').html(data);
                        $('#NumberForPartners').select2().parent().show();

                    }
                });
            }
        </script>

        <script src="bower_components/jquery/dist/jquery.min.js"></script>
        <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="js/resourcePrice.js"></script>
        <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
        <script src="dist/js/sb-admin-2.js"></script>
        <script src="./select2/select2.js"></script>
        <script src="js/numberManagement.js" type="text/javascript"></script>

        <script>
            $('#NumberForPartners').select2();
        </script>
    </body>
</html>
<%@include file="footer.jsp" %>