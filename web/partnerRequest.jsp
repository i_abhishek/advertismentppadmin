
<%@page import="com.mollatech.serviceguard.nucleus.commons.UtilityFunctions"%>
<%@page import="java.util.Enumeration"%>
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="java.util.Properties"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.GroupDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.GroupManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.AccessPolicy"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="javax.xml.datatype.XMLGregorianCalendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@page import="java.nio.channels.Channels"%>
<%@include file="header.jsp" %>
<script src="./js/sgservices.js"></script>
<script src="./js/modal.js"></script>

<%
    Properties partnerRejectionProperties = null;
    partnerRejectionProperties = LoadSettings.g_partnerRejectionSettings;

    String options = "<option value='Select' selected>Select Reason for Rejection </option>\n";

    if (partnerRejectionProperties != null) {
        Enumeration enuKeys = partnerRejectionProperties.keys();
        while (enuKeys.hasMoreElements()) {
            String key = (String) enuKeys.nextElement();
            String value = partnerRejectionProperties.getProperty(key);
            options += "<option value='" + value + "'>" + value + "</option>\n";
        }
    }
%>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">DEVELOPER REQUESTS</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47; <i class="fa fa-users"></i><b> All Pending Requests</b>
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <form class="form-horizontal " method="get">                    
                            <table class="table table-striped table-bordered table-hover display nowrap">
                                <thead>
                                    <tr>
                                        <td/>
                                        <th>No.</th>
                                        <th>Name</th>
                                        <th>Company</th>
                                        <th>Mobile</th>
                                        <th>Email</th>
                                        <th>IP</th>
                                        <th>Manage</th>                                    
                                        <th>Created</th>                                    
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        SgPartnerrequest[] SgPartnerRequest = new PartnerRequestManagement().listPartnerRequestsbystatus(SessionId, GlobalStatus.PENDING); //- 2  pending
                                        if (SgPartnerRequest != null && SgPartnerRequest.length > 0) {
                                            for (int i = 0; i < SgPartnerRequest.length; i++) {
                                                Date date = SgPartnerRequest[i].getCreatedOn();

                                    %>

                                    <tr>
                                        <td/>
                                        <td ><%=i + 1%></td>
                                        <td ><%=SgPartnerRequest[i].getName()%></td>
                                        <td ><a href="./CompInfo.jsp?_requestId=<%= SgPartnerRequest[i].getId()%>"><u><%=SgPartnerRequest[i].getComapanyRegName()%></u></a></td>
                                        <td ><%=SgPartnerRequest[i].getPhone()%></td>
                                        <td ><%=SgPartnerRequest[i].getEmail()%></td>
                                        <td ><%=SgPartnerRequest[i].getIpAddress()%></td>
                                        <td>
                                            <div class="btn-group">                                                
                                                <button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">Pending &nbsp;&nbsp;<span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" onclick="loadRequestDetails1('<%=SgPartnerRequest[i].getId()%>', '<%=GlobalStatus.ACTIVE%>')">Approve</a></li>
                                                    <li><a href="#" onclick="rejectPartner('<%=SgPartnerRequest[i].getId()%>', '<%=GlobalStatus.REJECTED%>')">Reject</a></li>
                                                    <li><a href="#" onclick="validateDocments('<%=SgPartnerRequest[i].getId()%>')"><i class="fa fa-download"></i>  Check Documents</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td><%= UtilityFunctions.getTMReqDate(date)%></td>
                                    </tr>
                                    <%}
                                        } %>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>                            
    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="approvePartnerRequest" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>
                    <h4 class="modal-title">Assign Group to Developer</h4>
                </div>
                <div class="modal-body">
                    <form class="form-inline">
                        <input type="hidden" id="_partnerRequestId" name="_partnerRequestId"/>
                        <input type="hidden" id="_partnerRequestStatus" name="_partnerRequestStatus"/>
                        <div class="control-group">
                            <!--<label class="control-label"  for="username">Assign Group</label>-->
                            <div class="controls">
                                <select class="span4 form-control" name="_groupId" id="_groupId" >
                                    <%
                                        GroupDetails[] grp = new GroupManagement().getAllGroupDetails(SessionId, channelId);
                                        if (grp != null) {
                                            for (int i = 0; i < grp.length; i++) {
                                                if (grp[i].getStatus() != GlobalStatus.DELETED) {%>                                   
                                    <option  value ="<%=grp[i].getGroupId()%>"><%=grp[i].getGroupName()%></option>
                                    <%}
                                        }
                                    } else {
                                    %>
                                    <option value="null">No Group added</option>
                                    <%}%>
                                </select>
                                <a class="btn btn-success btn-sm"  onclick="AddPartner()"><span class="icon_profile"></span> <i class="fa fa-plus-circle"></i> Add Developer</a>
                            </div>
                        </div>
                    </form>
                    <!--<font style="color: dodgerblue"></font>-->

                </div>
            </div>
        </div>
    </div>

    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="abcd" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">                                           
                <div class="modal-body" >
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>
                    <h3 class="text-primary">Requested Throughput Details</h3>
                    <br>
                    <div class="row">
                        <label  for="username" class="control-label col-lg-2">TPS</label>
                        <div class="col-lg-4">
                            <input class="form-control" type="text" id='tps' name='tps'>                                                                                                  
                        </div>
                        <label  for="username" class="control-label col-lg-2">TPD</label>
                        <div class="col-lg-4">
                            <input class="form-control" type="text" id='tpd' name='tpd' >
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <label class="control-label col-lg-2"  for="username">Start Date</label>                              
                        <div class="col-lg-4">                                            
                            <select class="form-control" id="sdate" name="sdate" disabled>
                                <option value="sunday">Sunday</option>
                                <option value="monday">Monday</option>
                                <option value="tuesday">Tuesday</option>
                                <option value="wednesday">Wednesday</option>
                                <option value="thursday">Thursday</option>
                                <option value="friday">Friday</option>
                                <option value="saturday">Saturday</option>
                            </select>
                        </div>
                        <label class="control-label col-lg-2"  for="username">Start Time</label>
                        <div class="col-lg-4">                                    
                            <input class="form-control" type="time" id='stime' name='stime' readonly>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <label class="control-label col-lg-2"  for="username">End Date</label>
                        <div class="col-lg-4">                                            
                            <select class="form-control" id="edate" name="edate" disabled>
                                <option value="sunday">Sunday</option>
                                <option value="monday">Monday</option>
                                <option value="tuesday">Tuesday</option>
                                <option value="wednesday">Wednesday</option>
                                <option value="thursday">Thursday</option>
                                <option value="friday">Friday</option>
                                <option value="saturday">Saturday</option>
                            </select>
                        </div>                          
                        <label class="control-label col-lg-2"  for="username">Start Time</label>
                        <div class="col-lg-4">
                            <input class="form-control" type="time" id='etime' name='etime' readonly>
                        </div>
                    </div>
                    <br>
                    <br>
                </div>
            </div>
        </div>
    </div>

    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="rejectionPartnerModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>
                    <h4 class="modal-title">Reject Developer</h4>
                </div>
                <div class="modal-body">
                    <form class="form-inline" method="post">
                        <fieldset>
                            <div class="control-group">
                                <label class="control-label col-lg-2"  for="partnername">Reason</label>
                                <div class="controls col-lg-10">
                                    <select id="reason" name="reason" class="form-control" >
                                        <%=options%>
                                    </select>
                                    <input type="hidden"  id="_rejectionPartnerRequestId" name="_rejectionPartnerRequestId"/> 
                                    <input type="hidden"  id="_rejectionPartnerRequestStatus" name="_rejectionPartnerRequestStatus"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="col-lg-12">
                                    <a class="btn btn-success btn-sm" onclick="ChangePartnerStatus()" style="float: right; margin: 15px; "><span class="icon_profile"></span> <i class="fa fa-check-circle"></i> Reject Developer</a>
                                    <button class="btn btn-danger btn-sm" style="float: right; margin: 15px;" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>                
                </div>
            </div>            
        </div>
    </div>
</div>



<script src="bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="js/bootbox.min(1).js" type="text/javascript"></script>
<!-- Morris Charts JavaScript 
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.min.js"></script>
<script src="js/morris-data.js"></script>-->

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->

</body>
</html>
<%@include file="footer.jsp" %>
