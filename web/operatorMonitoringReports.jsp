<%@page import="com.mollatech.serviceguard.nucleus.db.Monitorsettings"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.MonitorSettingsManagement"%>
<%@include file="header.jsp"%>
<link href="./css/select2.css" rel="stylesheet"/>
<script src="./js/select2.js"></script>
<script src="./js/json3.min.js"></script>
<link href="./css/bootstrap-timepicker.css" rel="stylesheet"/>
<link rel="stylesheet" href="./css/datepicker.css">
<script src="./js/reports.js"></script>
<%
    int opType = -1;
    String _opType = request.getParameter("_opType");
    if (_opType != null) {
        opType = Integer.parseInt(_opType);
    }
    String type = request.getParameter("_type");
    int Type = -1;
    if (type != null) {
        Type = Integer.parseInt(type);
    }
%>
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">                
                <h3 class="page-header text-uppercase">Monitoring Report</h3>
            </div>
            <!-- /.col-lg-10 -->
        </div>
        <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47;
                        <i class="fa fa-pie-chart"></i> Graphical Reports                        
                        &#47; <i class="fa fa-desktop"></i> Monitoring Report                                     
                    </div>
                    <div class="panel-body">
                        <table border="0">
                            <tr>  
                                <td>                            
                                    Resource Name &nbsp;
                                </td>
                                <td>
                                    <select id="_Ressource" name="_Ressource" class="form-control span2" style="width: 120px">
<!--                                        <option value="-1" selected>Select All</option>     -->
                                        <%                                            
                                            Monitorsettings[] rd = new MonitorSettingsManagement().listAllsettings(SessionId, channelId);
                                            if (rd != null) {
                                                for (int i = 0; i < rd.length; i++) {
                                        %>
                                        <option value="<%= rd[i].getMonitorId()%>"><%=rd[i].getMonitorName()%></option>
                                        <%}
                                            }
                                        %>                                       
                                    </select>
                                </td>                     
                                <td>                            
                                    &nbsp;  From &nbsp;
                                </td>
                                <td>
                                    <div id="datetimepicker1" class="date">
                                        <input id="_startdate" class="datepicker" name="_startdate" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth" style="width: 120px" />
                                        <span class="add-on" hidden>
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar" ></i>
                                        </span>
                                    </div>
                                </td>
                                <td>                                                
                                <td>
                                    &nbsp; Till&nbsp;
                                </td>
                                <td>
                                    <div id="datetimepicker2" class="date">
                                        <input id="_enddate" class="datepicker " name="_enddate" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth" style="width: 120px"/>
                                        <span class="add-on" hidden>
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar" ></i>
                                        </span>
                                    </div>  
                                </td>
                                <td>                
                                <td>
                                    &nbsp; 
                                    <button class="btn btn-success" id="Button" onclick="chart()"><i class="fa fa-line-chart"></i> Generate Report</button>                        
                                    &nbsp;   <button class="btn btn-info" id="Refresh" onclick="pageRefreshoMonitoringReports()"><i class="fa fa-refresh"></i> Refresh</button>                        
                                </td>
                            </tr>                    
                        </table>        
                        <br>

                        <div id="tabchart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="dist/js/moment.min.js" type="text/javascript"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript 
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.min.js"></script>
<script src="js/morris-data.js"></script>-->

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->

<script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>
<script src="js/bootbox.min.js" type="text/javascript"></script>
<script src="./js/bootstrap-timepicker.js"></script>
<script src="./js/bootstrap-datepicker.js"></script>

<script>
                                        $(function () {
                                            $('#datetimepicker1').datepicker({
                                                language: 'pt-BR'
                                            });
                                        });
                                        $(function () {
                                            $('#datetimepicker2').datepicker({
                                                language: 'pt-BR'
                                            });
                                        });
                                        $('#_ApStartTime').timepicker({
                                            minuteStep: 1,
                                            showInputs: false,
                                            disableFocus: true

                                        });
                                        $('#_ApEndTime').timepicker({
                                            minuteStep: 1,
                                            showInputs: false,
                                            disableFocus: true

                                        });
                                        $('#datetimepicker1').on('changeDate', function (ev) {
                                            $(this).datepicker('hide');
                                        });
                                        $('#datetimepicker2').on('changeDate', function (ev) {
                                            $(this).datepicker('hide');
                                        });
</script>
<%@include file="footer.jsp" %>