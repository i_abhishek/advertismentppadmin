/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.reports;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.serviceguard.nucleus.db.SgResourcecount;
import com.mollatech.serviceguard.nucleus.db.SgResourceowner;
import com.mollatech.serviceguard.nucleus.db.connector.management.ResourceCountMgmt;
import com.mollatech.serviceguard.nucleus.db.connector.management.ResourceOwnerManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author urmila
 */
@WebServlet(name = "BarChart", urlPatterns = {"/BarChart"})
public class BarChart extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");

        int resOwnerId = Integer.parseInt(request.getParameter("resownname"));
        int ResourceId = Integer.parseInt(request.getParameter("resId"));
        String month = request.getParameter("month");
        String year = request.getParameter("year");

        DateFormat sdf = new SimpleDateFormat("yyyy-MMM-dd");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {

            SgResourceowner resourceowner = new ResourceOwnerManagement().getResourceownerbyId(SessionId, resOwnerId);
            ArrayList<bar> sample = new ArrayList<bar>();
            Calendar calendar = Calendar.getInstance();
            Date today = sdf.parse(year + "-" + month + "-01");
            for (int i = 0; i < 4; i++) {
                calendar.setTime(today);
                calendar.add(Calendar.WEEK_OF_MONTH, i);
                Date startDates = calendar.getTime();
                startDates.setHours(0);
                startDates.setMinutes(0);
                startDates.setSeconds(0);
                calendar.setTime(today);
                calendar.add(Calendar.WEEK_OF_MONTH, i + 1);
                Date endDate = calendar.getTime();
                endDate.setHours(23);
                endDate.setMinutes(59);
                endDate.setSeconds(59);
                String tDate = formatter.format(startDates);
                if (resourceowner != null) {
                    SgResourcecount[] resourcecount = new ResourceCountMgmt().getDetailsByDate(ResourceId, startDates, endDate);                    
                    if (resourcecount != null) {
                        float totalAmout = 0;
                        for (SgResourcecount sgResourcecount : resourcecount) {
                            JSONObject data = new JSONObject(sgResourcecount.getCountI());
                            Iterator iterator = data.keys();
                            while (iterator.hasNext()) {
                                String key = (String) iterator.next();
                                totalAmout += Float.parseFloat(data.getString(key).split(":")[0]) * Float.parseFloat(data.getString(key).split(":")[1]);
                            }

                        }
                        int iTotal = Math.round(totalAmout);
                        sample.add(new bar(iTotal, tDate));
                    } else {
                        sample.add(new bar(0, tDate));
                    }
                }else {
                        sample.add(new bar(0, tDate));
                    }
            }
            Gson gson = new Gson();
            JsonElement element = gson.toJsonTree(sample, new TypeToken<List<bar>>() {
            }.getType());
            JsonArray jsonArray = element.getAsJsonArray();
            out.print(jsonArray);
            out.flush();
            out.close();
            return;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.close();
            out.flush();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
