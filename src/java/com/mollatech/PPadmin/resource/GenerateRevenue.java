package com.mollatech.PPadmin.resource;

import com.mollatech.serviceguard.nucleus.db.SgPartydetails;
import com.mollatech.serviceguard.nucleus.db.SgResourcecount;
import com.mollatech.serviceguard.nucleus.db.SgResourceowner;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartyManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.ResourceCountMgmt;
import com.mollatech.serviceguard.nucleus.db.connector.management.ResourceOwnerManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Ash
 */
@WebServlet(name = "GenerateRevenue", urlPatterns = { "/GenerateRevenue" })
public class GenerateRevenue extends HttpServlet {

    static final Logger logger = Logger.getLogger(GenerateRevenue.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        logger.info("Requested Servlet is GenerateRevenue at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        JSONObject json = new JSONObject();
        JSONArray array = new JSONArray();
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        try {
            String resId = request.getParameter("resId");
            String month = request.getParameter("month");
            String year = request.getParameter("year");
            DateFormat sdf = new SimpleDateFormat("yyyy-MMM-dd");
            Date today = sdf.parse(year + "-" + month + "-01");
            today.setHours(0);
            today.setMinutes(0);
            today.setSeconds(0);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(today);
            calendar.add(Calendar.MONTH, 1);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.add(Calendar.DATE, -1);
            Date lastDayOfMonth = calendar.getTime();
            lastDayOfMonth.setHours(23);
            lastDayOfMonth.setMinutes(59);
            lastDayOfMonth.setSeconds(59);
            SgResourceowner resourceowner = new ResourceOwnerManagement().getResourceownerbyResId(SessionId, resId);
            if (resourceowner != null) {
                SgResourcecount[] resourcecount = new ResourceCountMgmt().getDetails(Integer.parseInt(resId), today, lastDayOfMonth);
                if (resourcecount == null) {
                    json.put("label", "Owner");
                    json.put("value", 0);
                    array.put(json);
                    return;
                } else {
                    float totalAmout = 0;
                    for (SgResourcecount sgResourcecount : resourcecount) {
                        JSONObject data = new JSONObject(sgResourcecount.getCountI());
                        Iterator iterator = data.keys();
                        while (iterator.hasNext()) {
                            String key = (String) iterator.next();
                            totalAmout += Float.parseFloat(data.getString(key).split(":")[0]) * Float.parseFloat(data.getString(key).split(":")[1]);
                        }
                    }
                    //JSONObject data = new JSONObject(resourceowner.getData());
                    JSONObject data = null;
                    JSONObject requiredData = null;
                    org.json.JSONArray jsResourceOwner = new org.json.JSONArray(resourceowner.getData());  
                    for (int i = 0; i < jsResourceOwner.length(); i++) {
                        org.json.JSONObject jsonexists = jsResourceOwner.getJSONObject(i);
                        if (jsonexists.has(resId)) {
                            requiredData = jsonexists;
                            break;
                        }
                    }
                    if(requiredData != null){
                        data = requiredData.getJSONObject(resId);
                    }
                    json.put("label", "Owner");
                    float roundOff = Math.round(((totalAmout * Float.parseFloat(data.getString("_ownerRev"))) / 100) * 100) / 100.00f;
                    json.put("value", "" + roundOff);
                    array.put(json);
                    if (data != null && !data.getString("_aPartyRev").equals("")) {
                        JSONObject json1 = new JSONObject();
                        SgPartydetails partyObj = new PartyManagement().getPartyById(channelId, Integer.parseInt(data.getString("_firstParty")));
                        if(partyObj!=null){
                            json1.put("label", partyObj.getPartyName());
                        }else{
                            json1.put("label", "NA");
                        }
                        float rev = Float.parseFloat(data.getString("_aPartyRev"));
                        roundOff = Math.round(((totalAmout * rev) / 100) * 100) / 100.00f;
                        json1.put("value", "" + roundOff);
                        array.put(json1);
                    }
                    if (data != null && !data.getString("_bPartyRev").equals("")) {
                        JSONObject json1 = new JSONObject();
                        SgPartydetails partyObj = new PartyManagement().getPartyById(channelId, Integer.parseInt(data.getString("_secondParty")));
                        if(partyObj!=null){
                            json1.put("label", partyObj.getPartyName());
                        }else{
                            json1.put("label", "NA");
                        }
                        
                        float rev = Float.parseFloat(data.getString("_bPartyRev"));
                        roundOff = Math.round(((totalAmout * rev) / 100) * 100) / 100.00f;
                        json1.put("value", "" + roundOff);
                        array.put(json1);
                    }
                    if (data != null && !data.getString("_cPartyRev").equals("")) {
                        JSONObject json1 = new JSONObject();
                        json1.put("label", "Party C");
                        SgPartydetails partyObj = new PartyManagement().getPartyById(channelId, Integer.parseInt(data.getString("_thirdParty")));
                        if(partyObj!=null){
                            json1.put("label", partyObj.getPartyName());
                        }else{
                            json1.put("label", "NA");
                        }
                        float rev = Float.parseFloat(data.getString("_cPartyRev"));
                        roundOff = Math.round(((totalAmout * rev) / 100) * 100) / 100.00f;
                        json1.put("value", "" + roundOff);
                        array.put(json1);
                    }
                    if (data != null && !data.getString("_dPartyRev").equals("")) {
                        JSONObject json1 = new JSONObject();
                        SgPartydetails partyObj = new PartyManagement().getPartyById(channelId, Integer.parseInt(data.getString("_fourthParty")));
                        if(partyObj!=null){
                            json1.put("label", partyObj.getPartyName());
                        }else{
                            json1.put("label", "NA");
                        }
                        float rev = Float.parseFloat(data.getString("_dPartyRev"));
                        roundOff = Math.round(((totalAmout * rev) / 100) * 100) / 100.00f;
                        json1.put("value", "" + roundOff);
                        array.put(json1);
                    }
                }
            } else {
                json.put("label", "Owner");
                json.put("value", 0);
                array.put(json);
                return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (array.length() == 0) {
                json.put("label", "Owner");
                json.put("value", 0);
                array.put(json);
            }
            logger.info("Response of GenerateRevenue " + array.toString());
            logger.info("Response of GenerateRevenue Servlet at " + new Date());
            out.print(array);
            out.flush();
            out.close();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
