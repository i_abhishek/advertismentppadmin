<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.GroupDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.GroupManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MSConfig"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="java.util.Date"%>
<%@page import="javax.xml.datatype.XMLGregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@include file="header.jsp" %>
<script src="./js/jquery-1.8.3.min.js"></script>
<script src="./js/sgservices.js"></script>
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <%                    String _groupId = request.getParameter("_groupId");
                    int groupId = Integer.parseInt(_groupId);
                    GroupDetails gdetails = new GroupManagement().getGroupDetails(SessionId, channelId, groupId);
                %>
                <h3 class="page-header text-uppercase">Services of <%= gdetails.getGroupName()%> Group</h3>
            </div>
            <!-- /.col-lg-10 -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47;  <i class="fa fa-user"></i><a href="partners.jsp"> Developer Manager</a>&#47; Group Services                
                    </div>
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <%SimpleDateFormat sdf = new SimpleDateFormat("MMM d,yyyy HH:mm");%>
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <th><font style="font-size: 14px">No.</font></th>
                                <th><font style="font-size: 14px">Access Point</font></th>
                                <th><font style="font-size: 14px">Version</font></th>
                                <th><font style="font-size: 14px">Description</font></th>
                                <th><font style="font-size: 14px">Web Service</font></th>                                                
                                <th><font style="font-size: 14px">WSDL</font></th>       
                                <th><font style="font-size: 14px">WADL</font></th> 
                                <th><font style="font-size: 14px">Created On</font></th>
                                <th><font style="font-size: 14px">Updated On</font></th>
                                </thead>
                                <tbody>
                                    <%
                                        int count = 0;
                                        Object sobject = null;
                                        sobject = new SettingsManagement().getSetting(SessionId, channelId, SettingsManagement.MATERSLAVE, SettingsManagement.PREFERENCE_ONE);
                                        MSConfig msConfig = (MSConfig) sobject;
                                        Accesspoint Res[] = null;
                                        Res = new AccessPointManagement().getAllAccessPoint(SessionId, channelId);
                                        //                                    if (list != null && !list.isEmpty()) {
                                        //                                        Res = new Accesspoint[list.size()];
                                        //                                        for (int i = 0; i < (list.size() - 1); i++) {
                                        //                                            Res[i] = (Accesspoint) list.get(i);
                                        //                                        }
                                        //                                    }
                                        if (Res != null) {

                                            for (int i = 0; i < (Res.length - 1); i++) {
                                                if (Res[i].getStatus() != -99) {
                                                    int ap = Res[i].getApId();
                                                    //GroupDetails gedtails = gmngt.getGroupDetails(sessionId, channel.getChannelid(), groupid);
                                                    Accesspoint apdetails = Res[i];
                                                    //rmObj.getResourceById(sessionId, _channelId, id);
                                                    if (apdetails != null) {
                                                        int resID = -1;
                                                        String userStatus = "user-status-value-" + i;
                                                        String resourceList = apdetails.getResources();
                                                        String[] resids = resourceList.split(",");
                                                        for (int j = 0; j < resids.length; j++) {
                                                            if (!resids[j].isEmpty() && resids[j] != null) {
                                                                resID = Integer.parseInt(resids[j]);
                                                                ResourceDetails rsName = new ResourceManagement().getResourceById(SessionId, channelId, resID);
                                                                TransformDetails tmdetail = new TransformManagement().getTransformDetails(SessionId, channelId, apdetails.getApId(), resID);
                                                                if (rsName != null && tmdetail != null) {
                                                                    Map tmpMethods = (Map) Serializer.deserialize(tmdetail.getMethods());
                                                                    if (groupId == apdetails.getGroupid()) {
                                                                        count++;
                                                                        for (int ver = 1; ver <= tmpMethods.size(); ver++) {
                                    %>
                                    <tr>
                                        <td><%=count%></td>
                                        <td><%=apdetails.getName()%></td>
                                        <td><%=ver%></td>
                                        <td><a class="btn btn-success btn-xs" href = './transformDetails.jsp?_apid=<%=apdetails.getApId()%>&_resId=<%=resID%>&ver=<%=ver%>'><i class="fa fa-info-circle"></i> View</a></td>
                                        <td><a href="#" class="btn btn-warning btn-xs" onclick="viewWSURL('<%=rsName.getWsdlUrl()%>')" ><i class="fa fa-eye"></i> View</a></td>
                                        <%if (msConfig != null) {
                                                //                        MSConfig msConfig = (MSConfig) settingsObj;
                                                String ssl = "http";
                                                if (msConfig.ssl.equalsIgnoreCase("yes")) {
                                                    ssl = "https";
                                                }
                                                String implName = (new JSONObject(tmdetail.getImplClassName()).getString("" + ver));
                                        %>
                                        <td ><a href="#" class="btn btn-success btn-xs" onclick="viewWSURL('<%=ssl%>://<%=msConfig.aphostIp%>:<%=msConfig.aphostPort%>/<%=apdetails.getName().replaceAll(" ", "")%>/<%=implName%>?wsdl')" ><font style="font-size: 14px"><i class="fa fa-eye"></i> View</font></a></td>
                                        <td><a href="#" class="btn btn-warning btn-xs" onclick="viewWSURL('<%=ssl%>://<%=msConfig.aphostIp%>:<%=msConfig.aphostPort%>/<%=apdetails.getName().replaceAll(" ", "")%>/<%=implName%>/application.wadl')" ><i class="fa fa-eye"></i> View</a></td>
                                        <%} else {%>
                                        <td><button  class="btn btn-success btn-xs" onclick="viewWSURL('Please do Global Configuration')" ><i class="fa fa-eye"></i> View</button></td>
                                        <td><button  class="btn btn-warning btn-xs" onclick="viewWSURL('Please do Global Configuration')" ><i class="fa fa-eye"></i> View</button></td>
                                        <%}%>
                                        <%

                                            Date date = tmdetail.getCreatedOn();
                                        %>
                                        <td><%= sdf.format(date)%></td>
                                        <%

                                            date = tmdetail.getLastupOn();
                                        %>
                                        <td><%= sdf.format(date)%></td>
                                    </tr>

                                    <%}
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        if (count == 0) {%>
                                    <tr><td>No record found</td><td>No record found</td><td>No record found</td><td>No record found</td><td>No record found</td><td>No record found</td><td>No record found</td><td>No record found</td></tr>
                                    <%}%>
                                </tbody>
                            </table>                            
                        </div>
                        <div id="resource_table_main">                              
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript 
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.min.js"></script>
<script src="js/morris-data.js"></script>-->

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>
<script src="js/bootbox.min.js" type="text/javascript"></script>
<script>
                                            $(document).ready(function () {
                                                $('#dataTables-example').DataTable({
                                                    responsive: true
                                                });
                                            });
</script>                         
<%@include file="footer.jsp" %>