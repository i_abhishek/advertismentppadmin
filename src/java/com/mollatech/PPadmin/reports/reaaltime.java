package com.mollatech.PPadmin.reports;

import com.mollatech.service.nucleus.crypto.AxiomProtect;
import com.mollatech.serviceguard.nucleus.db.Monitorsettings;
import com.mollatech.serviceguard.nucleus.db.connector.management.MonitorSettingsManagement;
import com.mollatech.serviceguard.nucleus.settings.NucleusMonitorSettings;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.json.JSONObject;

public class reaaltime extends HttpServlet {

    private static int counts = 0;

    private Date date = null;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        MonitorSettingsManagement ppw = new MonitorSettingsManagement();
        String _settingId = request.getParameter("_settingId");
        int settingId = Integer.parseInt(_settingId);
        String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        String _settingName = ppw.getMonitorSettingbyId(sessionId, channelId, settingId).getMonitorName();
        Monitorsettings ms = ppw.getMonitorSettingbyId(sessionId, channelId, settingId);
        byte[] obj = ms.getMonitorSettingEntry();
        byte[] f1 = AxiomProtect.AccessDataBytes(obj);
        ByteArrayInputStream bais = new ByteArrayInputStream(f1);
        Object object = new MonitorSettingsManagement().deserializeFromObject(bais);
        NucleusMonitorSettings nms = null;
        if (object instanceof NucleusMonitorSettings) {
            nms = (NucleusMonitorSettings) object;
        }
        Calendar endDate = Calendar.getInstance();
        Date eDate = endDate.getTime();
        String frequency = nms.getFrequency();
        String f = frequency.replaceAll("min", "");
        int freqmin = Integer.parseInt(f);
        int frequensec = freqmin * 60;
        endDate.add(Calendar.SECOND, -frequensec);
        Date sDate = endDate.getTime();
        if (date == null) {
            XMLGregorianCalendar datestart = null;
            XMLGregorianCalendar dateend = null;
            GregorianCalendar s = new GregorianCalendar();
            s.setTime(sDate);
            try {
                datestart = DatatypeFactory.newInstance().newXMLGregorianCalendar(s);
                sDate = datestart.toGregorianCalendar().getTime();
                GregorianCalendar e = new GregorianCalendar();
                e.setTime(sDate);
                dateend = DatatypeFactory.newInstance().newXMLGregorianCalendar(e);
                eDate = dateend.toGregorianCalendar().getTime();
            } catch (Exception e) {
                e.printStackTrace();
            }
            counts = ppw.getMonitorTrackingByNameDuration(sessionId, channelId, _settingName, sDate, eDate);
            date = new Date();
        } else {
            long tinter = System.currentTimeMillis() - date.getTime();
            tinter = (tinter / 1000) % 60;
            if (tinter > 3) {
                XMLGregorianCalendar datestart = null;
                XMLGregorianCalendar dateend = null;
                GregorianCalendar s = new GregorianCalendar();
                s.setTime(sDate);
                try {
                    datestart = DatatypeFactory.newInstance().newXMLGregorianCalendar(s);
                    GregorianCalendar e = new GregorianCalendar();
                    e.setTime(sDate);
                    sDate = datestart.toGregorianCalendar().getTime();
                    dateend = DatatypeFactory.newInstance().newXMLGregorianCalendar(e);
                    eDate = dateend.toGregorianCalendar().getTime();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                counts = ppw.getMonitorTrackingByNameDuration(sessionId, null, _settingName, sDate, eDate);
                date = new Date();
            }
        }
        try {
            PrintWriter out = response.getWriter();
            try {
                json.put("_count", counts);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
        } catch (Exception e) {
        } finally {
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
