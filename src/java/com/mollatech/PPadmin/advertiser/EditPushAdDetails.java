/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.advertiser;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.commons.MakerChaker;
import com.mollatech.serviceguard.nucleus.db.SgAdvPreApprovalPackagedetails;
import com.mollatech.serviceguard.nucleus.db.SgApprovedAdPackagedetails;
import com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvPreApprovalPackageManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.ApprovedAdPackageManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "EditPushAdDetails", urlPatterns = {"/EditPushAdDetails"})
public class EditPushAdDetails extends HttpServlet {

    static final Logger logger = Logger.getLogger(EditPushAdDetails.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Request servlet is #EditPushAdDetails from #PPAdmin at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Push details updated successfully";        
        String channelId = (String) request.getSession().getAttribute("_channelId");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String packageID = request.getParameter("packageId");
        logger.debug("Value of packageID  = " + packageID);
        int pId = 0;
        if (packageID != null) {
            pId = Integer.parseInt(packageID);
        }
        SgAdvPreApprovalPackagedetails packObj = new AdvPreApprovalPackageManagement().getPackageDetails(SessionId, channelId, pId);
        try {
            if (packObj == null) {
                result = "error";
                message = "Can't edit the package details.";
                json.put("result", result);
                json.put("message", message);
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        String PartnerVisibility[] = request.getParameterValues("_partnerId");
        String pushAllowLogo = request.getParameter("pushAllowLogo");
        String pushMessageLength = request.getParameter("pushMessageLength");
        String pushAdPerDayCount = request.getParameter("pushAdPerDayCount");
        String pushAdPerCreditDeduction = request.getParameter("pushAdPerCreditDeduction");
        logger.debug("Value of pushAllowLogo  = " + pushAllowLogo);
        logger.debug("Value of pushMessageLength  = " + pushMessageLength);
        logger.debug("Value of pushAdPerDayCount  = " + pushAdPerDayCount);
        logger.debug("Value of pushAdPerCreditDeduction  = " + pushAdPerCreditDeduction);
        JSONObject pushAdConf = new JSONObject();        
        try {
            if(pushAllowLogo != null){
                pushAdConf.put("pushAllowLogo", "yes");
            }
            if(pushMessageLength != null){
                pushAdConf.put("pushMessageLength", pushMessageLength);
            }
            if(pushAdPerDayCount != null){
                pushAdConf.put("pushAdPerDay", pushAdPerDayCount);
            }
            if(pushAdPerDayCount != null){
                pushAdConf.put("pushCreditDeductionPerAd", pushAdPerCreditDeduction);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            try {                
                json.put("result", "error");
                json.put("message", "Push details failed to update.");
                return;
            } finally {
                out.print(json);
                out.flush();
            }
        }        
        packObj.setPushAdConfiguration(pushAdConf.toString());
        
        if (packObj.getTabShowFlag() != GlobalStatus.FEATURE) {
            packObj.setTabShowFlag(GlobalStatus.APRATE);
        }
        
        String partnerId = "";
        if (PartnerVisibility != null) {
            for (int i = 0; i < PartnerVisibility.length; i++) {
                partnerId += PartnerVisibility[i] + ",";
            }
        }
        packObj.setPartnerVisibility(partnerId);
        
        int retValue = new AdvPreApprovalPackageManagement().editPackage(SessionId,packObj);
        MakerChaker makerChakerObj = (MakerChaker) request.getSession().getAttribute("makerChacker");
        SgApprovedAdPackagedetails existsObj = new ApprovedAdPackageManagement().getReqPackageByName(SessionId, channelId, packObj.getPackageName());
        boolean makerChackerFlag = true;
        if (makerChakerObj == null) {
            makerChackerFlag = false;
        } else if (makerChakerObj.status == GlobalStatus.SUSPEND) {
            makerChackerFlag = false;
        }
        try {
            if (retValue == -1) {
                result = "error";
                message = "Package rate failed to updated.";
                json.put("result", result);
                logger.debug("Response of #EditPushAdDetails from #PPAdmin Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of #EditPushAdDetails from #PPAdmin Servlet's Parameter  message is " + message);
            } else {
                if (makerChackerFlag == false && existsObj != null) {                    
                    existsObj.setMainCredits(packObj.getMainCredits());                    
                    existsObj.setTabShowFlag(packObj.getTabShowFlag());
                    existsObj.setMakerflag(packObj.getMakerflag());
                    existsObj.setPartnerVisibility(packObj.getPartnerVisibility());                    
                    existsObj.setTax(packObj.getTax());
                    existsObj.setRecurrenceBillingPlanId(packObj.getRecurrenceBillingPlanId());
                    existsObj.setPushAdConfiguration(packObj.getPdfAdConfiguration());                    
                    new ApprovedAdPackageManagement().editReqPackage(SessionId, channelId, existsObj);
                }
                json.put("result", result);
                json.put("message", message);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            logger.info("Response of #EditPushAdDetails from #PPAdmin " + json.toString());
            logger.info("Response of #EditPushAdDetails from #PPAdmin Servlet at " + new Date());
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
