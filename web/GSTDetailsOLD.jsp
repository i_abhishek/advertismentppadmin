
<%@page import="com.mollatech.serviceguard.nucleus.commons.UtilityFunctions"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgTaxDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TaxDetailsManagement"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SlabPriceCalculationManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.CreditManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgCreditInfo"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.HourlyTxManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgHourlytransactiondetails"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.DailyTxManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgDailytransactiondetails"%>
<script src="js/GSTbillingreport.js" type="text/javascript"></script>
<script src="./js/partnerRequest.js"></script>
<%
    TaxDetailsManagement taxm = new TaxDetailsManagement();
    String ChannelId = (String) request.getSession().getAttribute("_channelId");
    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
    String startDate = request.getParameter("_sdate");
    String endDate = request.getParameter("_edate");
    String stime = request.getParameter("_stime");
    String etime = request.getParameter("_etime");

   // SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
//    Date sDate = format.parse(startDate);
//    Date eDate = format.parse(endDate);
//    Date cdate = format.parse(format.format(new Date()));
//    DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
    
    SgTaxDetails[] details = taxm.getGSTDetails(SessionId, ChannelId, startDate, endDate, stime, etime);
    session.setAttribute("gstBillingReport", details);
%>
<div class="col-lg-12">

    <form class="form-horizontal " method="get">
       
        <div class="dataTable_wrapper">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th><font style="font-size: 15px">Sr.No.</th>
                        <th><font style="font-size: 15px">Total Amount</th>
                        <th><font style="font-size: 15px">GST Tax</th>
                        <th><font style="font-size: 15px">GST Amount</th>
                        <th><font style="font-size: 15px">VAT Tax</th>
                        <th><font style="font-size: 15px">VAT Amount</th>
                        <th><font style="font-size: 15px">ST Tax</th>
                        <th><font style="font-size: 15px">ST Amount</th>
                        <th><font style="font-size: 15px">Payment Date</th>
                    </tr>
                </thead>
                
                <%
                    float totGstAmount = 0;
                    float totVatAmount = 0;
                    float totStAmount = 0;
                    float totTaxCalculation = 0; 
                     if (details != null) {                               
                     for(int i = 0;i < details.length;i++)
                     {
                       //float amt = details[i].getTotalAmount();
                         float gstTotal = (details[i].getGstTax()*details[i].getTotalAmount())/100;
                         String gstAmount = String.format("%.02f", gstTotal);
                         float vatTotal = (details[i].getVatTax()*details[i].getTotalAmount())/100;
                         String vatAmount = String.format("%.02f", vatTotal);
                         float stTotal = (details[i].getStTax()*details[i].getTotalAmount())/100;
                         String stAmount = String.format("%.02f", stTotal);
                         totGstAmount = gstTotal + totGstAmount;
                         totVatAmount = vatTotal + totVatAmount;
                         totStAmount = stTotal + totStAmount;
                         totTaxCalculation = totGstAmount+totVatAmount+totStAmount;

                 %> 
                <tr>
                            <td><%=i + 1%></td>
                            <td><%=details[i].getTotalAmount()%></td>
                            <td><%=details[i].getGstTax()%></td>
                            <td ><%=gstAmount%></td>
                            <td><%=details[i].getVatTax()%></td>
                            <td><%=vatAmount%></td>
                            <td><%=details[i].getStTax()%></td>
                            <td><%=stAmount%></td>
                            <td><%=UtilityFunctions.getTMReqDate(details[i].getPaymentDate())%></td>
                            
                </tr>
                
                <% }
 
                           
                            String totgstAmount = String.format("%.02f", totGstAmount);
                            String totvatAmount = String.format("%.02f", totVatAmount);
                            String totstAmount = String.format("%.02f", totStAmount);
                            String totTaxAmount = String.format("%.02f", totTaxCalculation);
                            
                            
                %>
                <tr>
                            <td></td>
                            <td></td>
                            <td><b>Total GST</b></td>
                            <td><b><%=totgstAmount%></b></td>
                            <td><b>Total VAT</b></td>
                            <td><b><%=totvatAmount%></b></td>
                            <td><b>Total ST</b></td>
                            <td><b><%=totstAmount%></b></td>
                            <td></td>
                            
                </tr>
                
                <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><b>Total TAX </b></td>
                            <td><b><%=totTaxAmount%></b></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                    
                </tr>
                
               
                <%
                        } else {%>
                        <tr>
                            <td>1</td>
                            <td>No Record Found</td>
                            <td>No Record Found</td>
                            <td>No Record Found</td>
                            <td>No Record Found</td>
                            <td>No Record Found</td>
                            <td>No Record Found</td>
                            <td>No Record Found</td>
                            <td>No Record Found</td>
                            
                        </tr>
                        <% }
                        %>
                </tbody>
            </table>
                
                
                <br>
<br> 

 <table>
       <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="#" class="btn btn-primary" onclick="gstPDFDownload(1)" >
                       <i></i>CSV</a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                
            </td>  
            
            <td>    
                 <a href="#" class="btn btn-primary" onclick="gstPDFDownload(0)" >
                        <i></i>PDF</a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>  
            
            <td>    
                 <a href="#" class="btn btn-primary" onclick="gstPDFDownload(2)" >
                        <i></i>TXT</a>
                
       
            </td>  
                
       </tr>     
           
 </table>    
</div>
    </form>

    <script src="bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>
    <!-- DataTables JavaScript -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>
    <script src="js/bootbox.min.js" type="text/javascript"></script>

    <script>
                $(document).ready(function () {
                    $('#dataTables-example').DataTable({
                        responsive: true
                    });
                });
    </script>

