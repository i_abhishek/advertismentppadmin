<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartyManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPartydetails"%>
<%@page import="javax.xml.datatype.XMLGregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.nio.channels.Channels"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@include file="header.jsp" %>
<script src="js/partyManager.js" type="text/javascript"></script>

<%
    SgPartydetails[] sgPartylist = new PartyManagement().getAllPartyDetails(channelId);
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
%>
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header">Party Management</h3>
            </div>
            <!-- /.col-lg-10 -->
        </div>
        <div class="row">
            <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47;  <i class="fa fa-table"></i><b> List of Parties</b>
                    </div>
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover display nowrap" id="partnerDetails">
                                <thead>
                                    <tr>
                                        <td/>
                                        <th><font style="font-size: 11px">No.</th>
                                        <th><font style="font-size: 11px">Name</th>                                        
                                        <th><font style="font-size: 11px">Company name</th>                                        
                                        <th><font style="font-size: 11px">Phone No</th>
                                        <th><font style="font-size: 11px">Email Id</th>
                                        <th><font style="font-size: 11px">Manage</th>
                                        <th><font style="font-size: 11px">Created On</th>
                                        <th><font style="font-size: 11px">Updated On</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%if (sgPartylist != null) {
                                            for (int i = 0; i < sgPartylist.length; i++) {
                                                Date crtdate = sgPartylist[i].getCreatedon();
                                                Date updateddate = sgPartylist[i].getLastupdatedon();
                                    %>
                                    <tr>
                                        <td/>
                                        <td ><font style="font-size:11px "><%=i + 1%></font></td>
                                        <td ><font style="font-size: 11px"><%= sgPartylist[i].getPartyName()%></font></td> 
                                        <td ><font style="font-size: 11px"><%= sgPartylist[i].getPartyCompanyName()%></font></td>
                                        <td ><font style="font-size: 11px"><%= sgPartylist[i].getPartyPhone()%></font></td>
                                        <td ><font style="font-size: 11px"><%= sgPartylist[i].getPartyEmailid()%></font></td>
                                        <td >
                                            <div class="btn btn-group btn-xs">
                                                <button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" id="<%=sgPartylist[i].getPartyId()%>"<%if (sgPartylist[i].getStatus() == GlobalStatus.ACTIVE) {%> <font style="font-size: 11px;">ACTIVE</font> <%} else {%>< font style="font-size: 11px;"> SUSPENDED</font> <%}%> <span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" onclick="editPartyModel('<%=sgPartylist[i].getPartyCompanyName()%>', '<%=sgPartylist[i].getPartyEmailid()%>', '<%=sgPartylist[i].getPartyName()%>', '<%=sgPartylist[i].getPartyPhone()%>', '<%=sgPartylist[i].getPartyId()%>')"> Edit</a></li>
                                                    <li><a class="divider"></a></li> 
                                                    <li><a href="#"  onclick="changePartyStatus(<%=GlobalStatus.ACTIVE%>, <%=sgPartylist[i].getPartyId()%>)" >Mark as Active?</a></li>
                                                    <li><a href="#"  onclick="changePartyStatus(<%=GlobalStatus.SUSPEND%>, <%=sgPartylist[i].getPartyId()%>)" >Mark as Suspended?</a></li>                                                                                                                      

                                                </ul>
                                            </div>
                                        </td>                                                                                 
                                        <td><font style="font-size: 11px"><%=sdf.format(crtdate)%></td>
                                        <td><font style="font-size: 11px"><%=sdf.format(updateddate)%></td>
                                    </tr> 
                                    <%}
                                        }%>
                                </tbody>
                            </table>
                        </div>
                        <a href="./createParty.jsp" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Add party</a>        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="editParty" class="modal fade" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <b id="editPartyModel"></b>
            </div>          
            <div class="modal-body">
                <div class="row-fluid">
                    <form class="form-horizontal" id="editPartyForm" name="editPartyForm">
                        <fieldset>
                            <div class="control-group ">
                                <label class="control-label col-lg-3">Name</label>
                                <div class="col-lg-7">
                                    <input type="text" class="form-control "id="editPartyName" name="editPartyName">
                                </div>
                            </div>
                            <br><br>
                            <div class="control-group ">
                                <label class="control-label col-lg-3">Phone</label>
                                <div class="col-lg-7">
                                    <input type="text" class="form-control "id="editPartyPhone" name="editPartyPhone" onkeypress="return isNumericKey(event)">
                                </div>
                            </div>
                            <br><br>
                            <div class="control-group ">
                                <label class="control-label col-lg-3">Email Id</label>
                                <div class="col-lg-7">
                                    <input type="text" class="form-control " id="editPartyEmailId" name="editPartyEmailId">
                                </div>
                            </div>
                            <br><br>
                            <div class="control-group ">
                                <label class="control-label col-lg-3">Company name</label>
                                <div class="col-lg-7">
                                    <input type="text" class="form-control " id="editPartyCompany" name="editPartyCompany">                                    
                                    <input type="hidden" id="editPartyId" name="editPartyId">
                                </div>
                            </div>                            
                        </fieldset>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div id="edit-party-result"></div>
                <button class="btn btn-info btn-xs" data-dismiss="modal">Close</button>
                <button class="btn btn-success btn-xs" onclick="editParty()" id="addPartnerButtonE">Save</button>
            </div>
        </div>
    </div>
</div>
<script src="bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript 
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.min.js"></script>
<script src="js/morris-data.js"></script>-->

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->

</script>
</body>
</html>
<%@include file="footer.jsp" %>
