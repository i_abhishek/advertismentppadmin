<!DOCTYPE html>
<%@include file="header.jsp" %>
<html lang="en">
    <head>
        <title>Admin Portal</title>
        <script src="bower_components/jquery/dist/jquery.min.js"></script>
        <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
        <script src="dist/js/sb-admin-2.js"></script>
        <script src="js/caasNumbers.js" type="text/javascript"></script>
        <script src="js/bootbox.min.js" type="text/javascript"></script>
        <script src="js/ajaxfileupload.js" type="text/javascript"></script>
        <script>
            window.onload = function () {
                var dropbox = document.getElementById("dropbox");
                dropbox.addEventListener("dragenter", noop, false);
                dropbox.addEventListener("dragexit", noop, false);
                dropbox.addEventListener("dragover", noop, false);
                dropbox.addEventListener("drop", dropUpload, false);
            }
            function noop(event) {
                event.stopPropagation();
                event.preventDefault();
            }

            function dropUpload(event) {
                noop(event);
                var files = event.dataTransfer.files;
                if (files.length > 1) {
                    showAlert("Please upload one file at a time.", "danger", 3000);
                    return;
                }
                for (var i = 0; i < files.length; i++) {
                    upload(files[i]);
                }
            }
            if (typeof String.prototype.endsWith != 'function') {
                String.prototype.endsWith = function (str) {
                    //return this.substring( this.length - str.length, this.length ) === str;  
                    return str.length > 0 && this.substring(this.length - str.length, this.length) === str;
                }
            }
            ;

            function upload(file) {
//                document.getElementById("status").innerHTML = "Uploading " + file.name;
                var formData = new FormData();
                formData.append("file", file);
                var xhr = new XMLHttpRequest();
                xhr.upload.addEventListener("progress", uploadProgress, false);
                xhr.addEventListener("load", uploadComplete, false);
//                xhr.open("POST", "UploadServlet", true); // If async=false, then you'll miss progress bar support.
//                xhr.send(formData);
                pleaseWaitProcess();
                if (!(file.name.toLowerCase().endsWith("xlsx") || file.name.toLowerCase().endsWith("xls"))) {
                    waiting.modal('hide');
                    showAlert("Please upload Excel file only !", "danger", 3000);
                    return;
                }
                $.ajax({
                    url: 'DragCaasNumbers',
                    data: formData,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    dataType: 'json',
                    success: function (data) {
                        if (strCompare(data._result, "error") === 0) {
                            waiting.modal("hide");
                            showAlert(data._message, "danger", 3000);
                        } else if (strCompare(data._result, "success") === 0) {
                            waiting.modal('hide');
                            showAlert(data._message, "success", 3000);
                        }
                    }
                });

            }
            function uploadProgress(event) {
                // Note: doesn't work with async=false.
                var progress = Math.round(event.loaded / event.total * 100);
                document.getElementById("status").innerHTML = "Progress " + progress + "%";
            }
            function uploadComplete(event) {
                document.getElementById("status").innerHTML = event.target.responseText;
            }
        </script>
        <style>
            #dropbox {
                margin-top: 2%;
                margin-left:  25%;
                width: 300px;
                height: 200px;
                border: 1px solid gray;
                border-radius: 5px;
                padding: 80px;
                color: gray;
            }
        </style>
        <script>
            var waiting;
            function showAlert(message, type, closeDelay) {
                if ($("#alerts-container").length === 0) {
                    // alerts-container does not exist, create it
                    $("body")
                            .append($('<div id="alerts-container" style="position: fixed;' +
                                    'width: 50%; left: 25%; top: 10%;">'));
                }
                // default to alert-info; other options include success, warning, danger
                type = type || "info";
                // create the alert div
                message = '<i class="fa fa-info-circle"></i> ' + message;
                var alert = $('<div class="alert alert-' + type + ' fade in">')
                        .append(
                                $('<button type="button" class="close" data-dismiss="alert">')
                                .append("&times;")
                                )
                        .append(message);
                // add the alert div to top of alerts-container, use append() to add to bottom
                $("#alerts-container").prepend(alert);
                // if closeDelay was passed - set a timeout to close the alert
                if (closeDelay)
                    window.setTimeout(function () {
                        alert.alert("close")
                    }, closeDelay);
            }

            function pleaseWaitProcess() {
                waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
                        '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>' +
                        '<div class="progress progress-striped active">' +
                        '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div></div></div></div>');
                waiting.modal();
            }
            function strCompare(a, b)
            {
                return (a < b ? -1 : (a > b ? 1 : 0));
            }
        </script>

    </head>

    <body>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header text-uppercase">CaaS Number Management</h3>
                </div>
            </div>
            <div class="row">
                <div id="alerts-container" style="width: 50%; left: 25%; top: 2%;margin-left: 25%"></div>
                <div class="col-lg-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-home"></i><a href="home.jsp"> Home </a>&#47;  <i class="fa fa-table"></i><b> Upload numbers</b>
                        </div>
                        <div id="dropbox">Drag and drop a .xlsx or .xls file here.</div>
                        <div id="status"></div>
                        <br>
                        <hr width=50% style="margin-left:  15%">
                        <br>
                        <div class="form-group">
                            <div class="col-lg-offset-0">
                                <label id="assignLabel" class="control-label col-lg-2">(Or Upload manually)</label>
                            </div>
                            <div style="width:150px;margin-left: 260px;">
                                <input type="file" id="ziptoupload" name="ziptoupload"> 
                            </div>
                            <div style="width:130px;margin-left: 495px;margin-top: -30px">
                                <font style="font-size: 11px"><a class="btn btn-success" id="Button" onclick="uploadCaasFile()">Upload</a></font>
                            </div>
                            <br>
                            <div class="col-lg-12 col-lg-offset-0">
                                <p>Choose file manually if your browser not support to drag and drop option.</p>
                            </div>
                            <br>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<%@include file="footer.jsp" %>