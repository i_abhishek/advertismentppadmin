<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceCountMgmt"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgResourcecount"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Channels"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgResourceowner"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceOwnerManagement"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@include file="header.jsp" %>
<script src="js/resourcePrice.js"></script>
<script src="js/json_sans_eval.js" type="text/javascript"></script>
<!--<script src="./js/jquery-1.8.3.min.js"></script>-->
<script src="./js/json3.min.js"></script>
<script src="./js/morris.min.js"></script>
<script src="./js/raphael-min.js"></script>
<script src="./js/morris.js"></script>
<link rel="stylesheet" href="./css/morris.css">
<script src="js/reports.js" type="text/javascript"></script>
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">By owner</h3>
                </div>
            </div>
            <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47;  <i class="fa fa-table"></i><b> Resource Sharing</b>
                    </div>
                    <div class="panel-body">
                        <form class="form-inline" role="form" method="POST">
                            <div class="form-group">
                                <label>Res. owner Name</label>&nbsp;&nbsp;
                                <select class="form-control" id="_revOwnername" name="_revOwnername" onchange="ResourceChange(this.value)">
                                    <option value="-1" selected>Select owner</option>
                                    <%
                                        SgResourceowner[] SgResourceowner = new ResourceOwnerManagement().listResourceowners(SessionId);
                                        if (SgResourceowner != null) {
                                            for (SgResourceowner details : SgResourceowner) {
                                                if (details.getStatus() == GlobalStatus.APPROVED) {
                                    %>
                                    <option value="<%=details.getResourceId()%>"><%=details.getResourceOwnerName()%></option>
                                    <%  }
                                            }
                                        }
                                    %>
                                </select>
                            </div>&nbsp;&nbsp;

                            <div class="form-group">
                                <label>Resource</label>&nbsp;&nbsp;
                                <select class="form-control" id="_revResource" name="_revResource">
                                    <option value="-1" selected>Select Resource</option>                                    
                                </select>                                
                            </div>&nbsp;&nbsp;
                            <div class="form-group">
                                <label>Month</label>&nbsp;&nbsp;
                                <select class="form-control" id="_revMonth" name="_revMonth">
                                    <option value="Jan">Jan</option>
                                    <option value="Feb">Feb</option>
                                    <option value="Mar">Mar</option>
                                    <option value="Apr">Apr</option>
                                    <option value="May">May</option>
                                    <option value="Jun">Jun</option>
                                    <option value="Jul">Jul</option>
                                    <option value="Aug">Aug</option>
                                    <option value="Sep">Sep</option>
                                    <option value="Oct">Oct</option>
                                    <option value="Nov">Nov</option>
                                    <option value="Dec">Dec</option>
                                </select>

                            </div>&nbsp;&nbsp;
                            <div class="form-group">
                                <label>Year</label>&nbsp;&nbsp;
                                <select class="form-control" id="_revYear" name="_revYear">
                                    <%
                                        SimpleDateFormat format = new SimpleDateFormat("yyyy");
                                        for (int year = (Integer.parseInt(format.format(new Date())) - 10); year <= Integer.parseInt(format.format(new Date())); year++) {%>
                                    <option value="<%=year%>"><%=year%></option>
                                    <% }

                                    %>
                                </select>
                                <script>
                                    document.getElementById('_revYear').value = '<%=format.format(new Date())%>';
                                </script>
                            </div>&nbsp;&nbsp;
                            <a class="btn btn-success btn-xl" onclick="revenueTextualAndGraphicalReport()"><i class="fa fa-plus-circle"></i> Generate</a>                    
                            </br></br></br>
                            <div id="revGraphicalAndTxt"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function ResourceChange(value) {
        var s = './getResourceByResourceOwner.jsp?_revOwnerId=' + value;
        $.ajax({
            type: 'GET',
            url: s,
            success: function (data) {
                $('#_revResource').html(data);
            }
        });
    }
</script>

<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="js/json_sans_eval.js" type="text/javascript"></script>
<script src="dist/js/sb-admin-2.js"></script>
<%@include file="footer.jsp" %>




