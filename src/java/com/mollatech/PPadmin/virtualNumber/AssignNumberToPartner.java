/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.virtualNumber;


import com.mollatech.infoblastservice.SdpConnectRsp;
import com.mollatech.infoblastservice.SendOrderRsp;
import com.mollatech.infoblastservice.SendUserCodeRsp;
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgNumbers;
import com.mollatech.serviceguard.nucleus.db.connector.management.NumbersManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;


@WebServlet(name = "AssignNumberToPartner", urlPatterns = {"/AssignNumberToPartner"})
public class AssignNumberToPartner extends HttpServlet {

    static final Logger logger = Logger.getLogger(AssignNumberToPartner.class);
    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    private static final String NUMERIC_STRING = "0123456789";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        logger.info("Requested Servlet is AssignNumberToPartner at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Assign Number successfully";
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        String[] numbers = request.getParameterValues("NumberForPartners");
        logger.debug("Value of number " + numbers);
        String partnerId = request.getParameter("partnerde");
        logger.debug("Value of partnerId " + partnerId);
        String numberType = request.getParameter("numberType");
        logger.debug("Value of numberType " + numberType);
        String res = ",";
        
        if(numbers != null && numbers.length > 1){
            result = "error";
            message = "Only one number can be assign";
            json.put("_result", result);
            json.put("_message", message);
            logger.info("AssignNumberToPartner result value " + result);
            logger.info("AssignNumberToPartner message value " + message);
            out.print(json);
            out.flush();
            out.close();
            return;            
        }
        PartnerDetails partner = new PartnerManagement().getPartnerDetails(SessionId, channelId, Integer.parseInt(partnerId));

        NumbersManagement nm = new NumbersManagement();
        String alreadyAssign = partner.getVirtualNumbers();
        if (numberType.equalsIgnoreCase("Live")) {
            alreadyAssign = partner.getVirtualNumbersLive();
        }
        String[] assignNumArr = null;
        boolean setToFreeFlag = false;
        List<String> list = null;
        TreeSet<String> set = null;
        if (alreadyAssign != null && !alreadyAssign.isEmpty()) {
            assignNumArr = alreadyAssign.split(",");
        }
        if (assignNumArr != null) {
            list = new ArrayList<String>(Arrays.asList(assignNumArr));
            set = new TreeSet<String>(list);
            set.removeAll(Arrays.asList(numbers));
            System.out.println(set);
            setToFreeFlag = true;
        }
        
        for (int i = 0; i < numbers.length; i++) {
            res += numbers[i] + ",";
            SgNumbers num = nm.getMobileNumberById(SessionId, Integer.parseInt(numbers[i]));
            num.setPartnerId(partner.getPartnerId());
            num.setChannelId(channelId);
            num.setStatus(GlobalStatus.IBNUMBERASSIGN);
            num.setUpdatedOn(new Date());
            // Inform IB for virtual no. assign
            SdpConnectRsp connectRes = new InfoblastServiceUtil().getSDPConnect();
            if(connectRes == null){
                result = "error";
                message = " IB sdpConnect service failed to connect !";
                json.put("_result", result);
                json.put("_message", message);
                logger.info("AssignNumberToPartner result value " + result);
                logger.info("AssignNumberToPartner message value " + message);
                out.print(json);
                out.flush();
                out.close();
                return;
            }else if(connectRes.getStatus() != 0){
                result = "error";
                message = connectRes.getStatusDesc();
                json.put("_result", result);
                json.put("_message", message);
                logger.info("AssignNumberToPartner result value " + result);
                logger.info("AssignNumberToPartner message value " + message);
                out.print(json);
                out.flush();
                out.close();
                return;
            }
            String infoBlastPassword = randomAlphaNumericPassw(8);
            String infoBlastAccount  = randomAccountNumber(8);
            String virtualNo = num.getMobileNo().replaceAll("[-+.^:,]","");
            SendOrderRsp sendOrderResponse = new InfoblastServiceUtil().getSendOrder(connectRes.getSessionId(), virtualNo, infoBlastPassword, infoBlastAccount, partner);
            if(sendOrderResponse == null){
                result = "error";
                message = " IB sendOrder service failed to connect!";
                json.put("_result", result);
                json.put("_message", message);
                logger.info("AssignNumberToPartner result value " + result);
                logger.info("AssignNumberToPartner message value " + message);
                out.print(json);
                out.flush();
                out.close();
                return;
            }else if(sendOrderResponse.getStatus() != 0){
                result = "error";
                message = sendOrderResponse.getStatusDesc();
                json.put("_result", result);
                json.put("_message", message);
                logger.info("AssignNumberToPartner result value " + result);
                logger.info("AssignNumberToPartner message value " + message);
                out.print(json);
                out.flush();
                out.close();
                return;
            }
            num.setInfoblastId(virtualNo);
            num.setInfoblastAccountNo(infoBlastAccount);
            num.setInfoblastPassword(infoBlastPassword);
            nm.updateDetails(num);
        }
        if (setToFreeFlag) {
            for (String freeNum : set) {
                if (!freeNum.isEmpty()) {
                    SgNumbers freeNumObj = nm.getMobileNumberById(SessionId, Integer.parseInt(freeNum));
                    freeNumObj.setPartnerId(null);
                    freeNumObj.setChannelId(null);
                    freeNumObj.setStatus(GlobalStatus.ACTIVE);
                    SdpConnectRsp connectRes = new InfoblastServiceUtil().getSDPConnect();
                    if(connectRes == null){
                        result = "error";
                        message = " IB sdpConnect service failed to connect !";
                        json.put("_result", result);
                        json.put("_message", message);
                        logger.info("AssignNumberToPartner result value " + result);
                        logger.info("AssignNumberToPartner message value " + message);
                        out.print(json);
                        out.flush();
                        out.close();
                        return;
                    }else if(connectRes.getStatus() != 0){
                        result = "error";
                        message = connectRes.getStatusDesc();
                        json.put("_result", result);
                        json.put("_message", message);
                        logger.info("AssignNumberToPartner result value " + result);
                        logger.info("AssignNumberToPartner message value " + message);
                        out.print(json);
                        out.flush();
                        out.close();
                        return;
                    }
                    String unassignNumber = freeNumObj.getMobileNo().replaceAll("[-+.^:,]","");
                    SendUserCodeRsp sendUserCodeRes = new InfoblastServiceUtil().sendUserCode(connectRes.getSessionId(), unassignNumber, "T", null);
                    if(sendUserCodeRes == null){
                        result = "error";
                        message = " IB sdpUserCode service failed to connect !";
                        json.put("_result", result);
                        json.put("_message", message);
                        logger.info("AssignNumberToPartner result value " + result);
                        logger.info("AssignNumberToPartner message value " + message);
                        out.print(json);
                        out.flush();
                        out.close();
                        return;
                    }else if(sendUserCodeRes.getStatus() != 0){
                        result = "error";
                        message = sendUserCodeRes.getStatusDesc();
                        json.put("_result", result);
                        json.put("_message", message);
                        logger.info("AssignNumberToPartner result value " + result);
                        logger.info("AssignNumberToPartner message value " + message);
                        out.print(json);
                        out.flush();
                        out.close();
                        return;
                    }
                    freeNumObj.setInfoblastId(null);
                    freeNumObj.setInfoblastPassword(null);
                    freeNumObj.setInfoblastAccountNo(null);
                    nm.updateDetails(freeNumObj);
                }
            }
        }
        if (numberType.equalsIgnoreCase("Test")) {
            partner.setVirtualNumbers(res);
        } else {
            partner.setVirtualNumbersLive(res);
        }
        int partRes = new PartnerManagement().updateDetails(partner);
        if (partRes == 0) {
            result = "success";
            message = "Assign Number successfully";
        } else {
            result = "error";
            message = " No any number assign to Developer  !";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
            logger.info("AssignNumberToPartner result value " + result);
            logger.info("AssignNumberToPartner message value " + message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }
    
    public static String randomAlphaNumericPassw(int count) {        
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }
    
    public static String randomAccountNumber(int count) {        
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int) (Math.random() * NUMERIC_STRING.length());
            builder.append(NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
