<%@page import="com.mollatech.serviceguard.nucleus.commons.UtilityFunctions"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPromocode"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PromocodeManagement"%>
<%@include file="header.jsp" %>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgBucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement"%>
<script src="js/promocode.js" type="text/javascript"></script>
<%
    int ACTIVE = 1;
    if (makerChakerObj != null) {
        if (!makerChakerObj.maker.contains(opObj.getEmailid()) && makerChakerObj.status == ACTIVE) {
            response.sendRedirect("oplogout.jsp");
            return;
        }
    }
%>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Promocode Details</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
    <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>    
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-home"></i><a href="home.jsp"> Home</a> &#47;
                <i class="fa fa-gears"></i> Promocode Details &#47;                                                          
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered table-hover">
                    <thead >
                        <tr >
                            <th style="text-align: center">Sr.No.</th>
                            <th style="text-align: center">Status</th>
                            <th style="text-align: center">Promocode</th>
                            <th style="text-align: center">Usage</th>
                            <th style="text-align: center">Developer usage</th>
                            <th style="text-align: center">Discount</th>
                            <th style="text-align: center">Discount Type</th>
                            <th style="text-align: center">Allow Developer</th>
                            <th style="text-align: center">Manage</th>
                            <th style="text-align: center">Created On</th>
                            <th style="text-align: center">Last Updated On</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                            int count = 0;
                            String resultInfo = "No Record Found";
                            
                            PromocodeManagement um = new PromocodeManagement();
                            SgPromocode[] rDetails = null;
                            int pId = -1;
                            rDetails = um.listpromocode(SessionId);                            
                            if (rDetails != null) {
                                for (int i = 0; i < rDetails.length; i++) {
                                    count++;                                   
                                    Date createdDate  = rDetails[i].getCreatedon();
                                    Date expiryDate = rDetails[i].getExpireOn();
                                    Date lastUpdatedOn = rDetails[i].getLastUpdatedOn();
                                    String crtDate = "NA";String expDate = "NA"; String lastDate="NA";
                                    if(createdDate != null){
                                        crtDate = UtilityFunctions.getTMReqDate(createdDate);
                                    }
                                    if(expDate != null){
                                        expDate = UtilityFunctions.getTMReqDate(expiryDate);
                                    }
                                    if(lastUpdatedOn != null){
                                        lastDate = UtilityFunctions.getTMReqDate(lastUpdatedOn);
                                    }
                                    String partnerEmail = "";
                                    String allowPartner = rDetails[i].getPartnerId();
                                    if(allowPartner != null && !allowPartner.equals("") && allowPartner.contains("all")){
                                        partnerEmail = "all";
                                    }else if(allowPartner != null && !allowPartner.equals("")){
                                        String[] parDet = allowPartner.split(",");
                                        for(int j=0; j<parDet.length;j++){
                                            int parId = Integer.parseInt(parDet[j]);
                                            PartnerDetails parObj = new PartnerManagement().getPartnerDetails(parId);
                                            partnerEmail += parObj.getPartnerEmailid()+"\n";
                                        }
                                    }else{
                                        partnerEmail = "NA";
                                    }
                        %>
                        <tr style="text-align: center">
                            <td><%=count%></td>
                            <%if (rDetails[i].getStatus() == GlobalStatus.SUSPEND && rDetails[i].getRequestFlag() == GlobalStatus.SUSPEND || rDetails[i].getRequestFlag() == GlobalStatus.SENDTO_CHECKER) {%>
                            <td>
                                <a href="#" class="btn btn-warning btn-xs btn-circle" data-toggle="tooltip" data-placement="right" title="Promocode Inactive"  ><i class="fa fa-thumbs-o-down"></i></a>
                            </td>
                            <% } else if (rDetails[i].getRequestFlag() == GlobalStatus.CANCLE_BYCHECKER) { %>
                            <td>
                                <a href="#" class="btn btn-danger btn-xs btn-circle" data-toggle="tooltip" data-placement="right" title="Request rejected" ><i class="fa fa-thumbs-o-down"></i></a>
                            </td>
                            <%} else if (rDetails[i].getStatus() == GlobalStatus.ACTIVE){%>
                            <td>
                                <a href="#" class="btn btn-success btn-xs btn-circle" data-toggle="tooltip" data-placement="right" title="Request approved"><i class="fa fa-thumbs-o-up"></i></a>
                            </td>
                            <%}%>
                           
                            <td><%=rDetails[i].getCode()%></td>                                
                            <td><%=rDetails[i].getUsageCount()%></td>
                            <td><%=rDetails[i].getPartnerUsageCount()%></td>
                            <td><%=rDetails[i].getDiscount()%></td>
                            <td><%=rDetails[i].getDiscountType()%></td>
                            <td> 
                                <button type="button" class="btn btn-info btn-xs" data-container="body" data-toggle="popover" data-placement="right" data-content="<%=partnerEmail%>">
                                    View
                                </button>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <a class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown" href="" >Manage <span class="caret"><font style="font-size: 12px"></font></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="promoCode.jsp?edit=<%=rDetails[i].getPromoCodeId()%>">Edit</a></li>                                       
                                        <li><a href="#" onclick="SendToChecker('<%=rDetails[i].getPromoCodeId()%>','<%=GlobalStatus.SENDTO_CHECKER%>')" >Send to checker</a></li>
                                        <li><a href="#" onclick="notifyToPartner('<%=rDetails[i].getPromoCodeId()%>')" >Share with Developer</a></li>                                        
                                        <% if (rDetails[i].getRequestFlag() == GlobalStatus.CANCLE_BYCHECKER) {%>
                                        <li><a class="divider"></a></li>    
                                        <li><a href="#" onclick="promoRejectionDetails('<%=rDetails[i].getCode()%>', '<%=rDetails[i].getRejectionReason()%>')" ><font color="color:red"> Rejection Details</font></a></li>                                                                                                                           
                                                <%}%>
                                    </ul>
                                </div>
                            </td>
                            <td><%=crtDate%></td>                           
                            <td><%=lastDate%></td>                            
                        </tr>
                        <%}
                            }else {%>
                        <tr style="text-align: center">
                            <td><%=resultInfo%></td><td><%=resultInfo%></td><td><%=resultInfo%></td><td><%=resultInfo%></td><td><%=resultInfo%></td> <td><%=resultInfo%></td>
                            <td><%=resultInfo%><td><%=resultInfo%></td><td><%=resultInfo%></td> <td><%=resultInfo%></td> <td><%=resultInfo%></td> 
                        </tr>
                        <%}%>
                    </tbody>
                </table>
                <a href="./promoCode.jsp" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Create Promocode</a>
            </div>
        </div>
    </div>
</div>
</div>

<div id="viewRejection" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="rejectPackageModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 id="rejectPackageModal"><b>Rejection details</b></h4>
            </div>          
            <div class="modal-body">
                <div class="row-fluid">
                    <form class="form-horizontal" id="viewrejectPackageForm">
                        <fieldset>
                            <div class="control-group">
                                <label class="control-label col-lg-2"  for="partnername">Reason</label>
                                <div class="controls col-lg-10">
                                    <!--                                    <input type="hidden" readonly id="_packageName" name="_packageName" >
                                                                        <input type="hidden" readonly id="_packagestatus" name="_packagestatus" >-->
                                    <textarea id="_rejectionDetails" name="_rejectionDetails" class="form-control" rows="4" readonly></textarea>
                                </div>
                            </div>                            
                        </fieldset>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div id="edit-partner-result"></div>
                <button class="btn btn-info btn-xs" data-dismiss="modal"><i class="fa fa-info-circle"></i> OK</button>
                <!--                <button class="btn btn-success btn-xs" onclick="rejectPackageRequest()" id="addPartnerButtonE">Reject request</button>-->
            </div>
        </div>
    </div>
</div>

<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>
<script src="js/bootbox.min.js" type="text/javascript"></script>
<script>
                                            $(document).ready(function () {
                                                $('#dataTables-example').DataTable({
                                                    responsive: true
                                                });
                                            });
 $("[data-toggle=popover]")
        .popover()                                            
</script>   

</body>
</html>
<%@include file="footer.jsp" %>