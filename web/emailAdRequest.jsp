<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserSubscriptionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgAdvertiseSubscriptionDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgAdvertiserDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgAdvertiserAdDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserAdManagement"%>
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Properties"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.UtilityFunctions"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement"%>
<%@include file="header.jsp" %>
<script src="js/packageOperation.js" type="text/javascript"></script>
<%
    String options = "<option value='Select' selected>Select Reason for Rejection </option>\n";
    Enumeration enuKeys = LoadSettings.g_emailADRejectionSettings.keys();
    while (enuKeys.hasMoreElements()) {
        String key = (String) enuKeys.nextElement();
        String value = LoadSettings.g_emailADRejectionSettings.getProperty(key);
        options += "<option value='" + value + "'>" + value + "</option>\n";
    }
%>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Email Ad Request</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-home"></i><a href="home.jsp"> Home</a> &#47;
                    <i class="fa fa-shopping-cart"></i> Email Ad Request &#47;                                                          
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover display nowrap" id="dataTables-example">
                        <thead>
                            <tr>
                                <td/>
                                <th>No.</th>
                                <th>Status</th>
                                <th>Name</th>
                                <th>Company Name</th>                                
                                <th>Package</th>
                                <th>Ad Image</th>
                                <th>Manage</th>
                                <th>Request On</th>
                                <th>Action On</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                int count = 0;                                
                                String updationDate = "Not Updated yet";
                                SgAdvertiserAdDetails[] adDetails = new AdvertiserAdManagement().getAdvertiserPDFAdByStatus(null);                                                                
                                SgAdvertiserDetails advertiserDetails;
                                SgAdvertiseSubscriptionDetails advSubscriptionDetails;
                                if (adDetails != null) {
                                    for (int i = 0; i < adDetails.length; i++) {                                                                              
                                        advertiserDetails = new AdvertiserManagement().getAdvertiserDetails(adDetails[i].getAdvertiserId());                                        
                                        advSubscriptionDetails = new AdvertiserSubscriptionManagement().getAdvertiserSubscriptionbyAdId(adDetails[i].getAdvertiserId());
                                        if(adDetails[i].getEmailAdStatus()!= null && adDetails[i].getEmailAdImage() != null){
                                            count++;  
                            %>
                            <tr style="text-align: center">
                                <td/>
                                <td><%=count%></td>
                                <%if (adDetails[i].getEmailAdStatus()!= null && adDetails[i].getEmailAdStatus() == GlobalStatus.SENDTO_CHECKER) {%>
                                <td>
                                    <a href="#" class="btn btn-warning btn-sm btn-circle" data-toggle="tooltip" data-placement="right" title="Request is pending for approval"><i class="fa fa-thumbs-o-down"></i></a>
                                </td>
                                <% } else if (adDetails[i].getEmailAdStatus()!= null && adDetails[i].getEmailAdStatus() == GlobalStatus.REJECTED) { %>
                                <td>
                                    <a href="#" class="btn btn-danger btn-sm btn-circle" data-toggle="tooltip" data-placement="right" title="Request is rejected" ><i class="fa fa-thumbs-o-down"></i></a>
                                </td>
                                <%} else {%>
                                <td>
                                    <a href="#" class="btn btn-success btn-sm btn-circle" data-toggle="tooltip" data-placement="right" title="Request is approved"><i class="fa fa-thumbs-o-up"></i></a>
                                </td>
                                <%}%>
                                <td><%=advertiserDetails.getAdvertisername()%></td>                                
                                <td><%=advertiserDetails.getCompanyName()%></td>
                                <td><%=advSubscriptionDetails.getPackageName()%></td>
                                <td>
                                     <image src="data:image/jpg;base64,<%=adDetails[i].getEmailAdImage()%>" alt="PDF Ad Image" width="40" height="40"> 
                                </td>
                                <td>                                                                                                                                                                         
                                    <div class="btn btn-group">
                                        <button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown"><%if (adDetails[i].getEmailAdStatus() == GlobalStatus.APPROVED) {%> <font style="font-size: 10px;">Approved</font> <%} else if (adDetails[i].getEmailAdStatus() == GlobalStatus.REJECTED) {%><font style="font-size: 10px;"> Rejected</font> <%} else {%> <font style="font-size: 10px;"> New</font> <%}%><span class="caret"></span></button>
                                        <ul class="dropdown-menu">
                                            <%
                                                if (adDetails[i].getEmailAdStatus() != GlobalStatus.APPROVED) {
                                            %>
                                            <li><a href="#" onclick="changeEmailAdStatus('<%=adDetails[i].getAdvertiserAdId()%>')" >Approve?</a></li>
                                            <li><a href="#" onclick="rejectEmailAdmodal('<%=GlobalStatus.REJECTED%>', '<%=adDetails[i].getAdvertiserAdId()%>')" >Reject?</a></li>                                                                                                           
                                           <%}%>
                                            <li><a href="#" onclick="emailLogoAdModelShow('<%=adDetails[i].getAdvertiserAdId()%>')" >View Ad Details</a></li>                                                                                                                                                       
                                        </ul>
                                    </div>
                                </td>
                                <td><%=UtilityFunctions.getTMReqDate(adDetails[i].getCreationDate())%></td>
                                <%if (adDetails[i].getUpdationEmailDate() == null) {%>
                                <td><%=updationDate%></td>
                                <%} else {%>
                                <td><%=UtilityFunctions.getTMReqDate(adDetails[i].getUpdationEmailDate())%></td>
                                <%}%>                                                                                        
                            </tr>
                            <%
                                }
                            }%>
                            <%if(count == 0){%>
                            <tr>
                                </td>
                                <td>No record found</td>
                                <td>No record found</td>
                                <td>No record found</td>
                                <td>No record found</td>
                                <td>No record found</td>
                                <td>No record found</td>
                                <td>No record found</td>
                                <td>No record found</td>
                                <td>No record found</td>
                            </tr>
                            <%}%>
                            <%}else{%>
                            <tr>
                                </td>
                                <td>No record found</td>
                                <td>No record found</td>
                                <td>No record found</td>
                                <td>No record found</td>
                                <td>No record found</td>
                                <td>No record found</td>
                                <td>No record found</td>
                                <td>No record found</td>
                                <td>No record found</td>
                            </tr>
                            <%}%>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
                        
        <div id="imageEmailAd"></div>
                                            
        <div id="rejectEmailAD" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="rejectPackageModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                        <b id="rejectPackageModal">Reject AD</b>
                    </div>          
                    <div class="modal-body">
                        <div class="row-fluid">
                            <form class="form-horizontal" id="rejectPackageForm">
                                <fieldset>
                                    <div class="control-group">
                                        <label class="control-label col-lg-2"  for="partnername">Reason</label>
                                        <div class="controls col-lg-10">
                                            <input type="hidden"  id="_adId" name="_adId" >
                                            <input type="hidden"  id="_adstatus" name="_adstatus" >
                                            <select id="reason" name="reason" class="form-control" >
                                                <%=options%>        
                                            </select>
                                        </div>
                                    </div>                            
                                </fieldset>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div id="edit-partner-result"></div>
                        <button class="btn btn-info btn-xs" data-dismiss="modal">Close</button>
                        <button class="btn btn-success btn-xs" onclick="rejectEmailADRequest()" id="addPartnerButtonE">Reject request</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<script src="js/bootbox.min.js" type="text/javascript"></script>
<script src="js/bootbox.min(1).js" type="text/javascript"></script>  
</body>
</html>
<%@include file="footer.jsp"%>