/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.virtualNumber;

import com.mollatech.infoblastservice.SdpConnectReq;
import com.mollatech.infoblastservice.SdpConnectRsp;
import com.mollatech.infoblastservice.SendOrderReq;
import com.mollatech.infoblastservice.SendOrderRsp;
import com.mollatech.infoblastservice.SendUserCodeReq;
import com.mollatech.infoblastservice.SendUserCodeRsp;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.sg.infoblast.InfoblastConnector;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.log4j.Logger;

public class InfoblastServiceUtil {

    static final Logger logger = Logger.getLogger(InfoblastServiceUtil.class);

    public SdpConnectRsp getSDPConnect() {
        SdpConnectReq sdpConnReq = new SdpConnectReq();
        sdpConnReq.setCpId(14);
        sdpConnReq.setLogin("serviceguard");
        sdpConnReq.setPassword("bc4cf854dv");
        String msgId = getMsgId();
        String timeStamp = getTimeStamp();
        sdpConnReq.setMsgId(msgId);
        sdpConnReq.setTimestamp(timeStamp);
        System.out.println("Invoke the sdpConnect API ...");
        logger.info("Invoke the sdpConnect API ...");
        logger.debug("CP ID     >> " + 14);
        logger.debug("Login     >> " + "serviceguard");
        logger.debug("Password  >> " + "bc4cf854dv");
        logger.debug("MsgId     >> " + msgId);
        logger.debug("timestamp >> " + timeStamp);
        SdpConnectRsp res = InfoblastConnector.sdpConnect(sdpConnReq);
        return res;
    }

    public SendOrderRsp getSendOrder(String ibSessionId, String virtalNumber, String password, String accountNumber, PartnerDetails partnerObj) {
        SendOrderReq sendOrderRequest = new SendOrderReq();
        try {
            sendOrderRequest.setCpId(14);
            String msgId = getMsgId();
            sendOrderRequest.setMsgId(getMsgId());
            sendOrderRequest.setSessionId(ibSessionId);
            sendOrderRequest.setSegment("R");
            sendOrderRequest.setSegmentId("segment");
            sendOrderRequest.setServiceNumber(virtalNumber);
            sendOrderRequest.setAccountNumber(accountNumber);
            sendOrderRequest.setUserId("900101140000");
            sendOrderRequest.setCustomerName(partnerObj.getPartnerName());
            sendOrderRequest.setRegisterName(partnerObj.getPartnerName());
            sendOrderRequest.setContactNumber(partnerObj.getPartnerPhone());
            sendOrderRequest.setInfoblastId(virtalNumber);
            sendOrderRequest.setInfoblastPassword(password);
            sendOrderRequest.setEmailAddress(partnerObj.getPartnerEmailid());
            sendOrderRequest.setService("1");
            sendOrderRequest.setMoUrl("https://developer.tm.com.my/SGSMSConnector/SMSConnector");
            String timestamp = getTimeStamp();
            sendOrderRequest.setTimestamp(getTimeStamp());

            System.out.println("Invoke the sendOrder API ...");
            logger.info("Invoke the sendOrder API ...");
            logger.debug("CP ID           >> " + 14);
            logger.debug("msgId           >> " + msgId);
            logger.debug("sessionId       >> " + ibSessionId);
            logger.debug("Segment         >> " + "R");
            logger.debug("SegmentId       >> " + "segment");
            logger.debug("ServiceNumber   >> " + virtalNumber);
            logger.debug("AccountNumber   >> " + accountNumber);
            logger.debug("UserId          >> " + "900101140000");
            logger.debug("CustomerName    >> " + partnerObj.getPartnerName());
            logger.debug("RegisterName    >> " + partnerObj.getPartnerName());
            logger.debug("ContactNumber   >> " + partnerObj.getPartnerPhone());
            logger.debug("InfoblastId     >> " + virtalNumber);
            logger.debug("password        >> " + password);
            logger.debug("EmailAddress    >> " + partnerObj.getPartnerEmailid());
            logger.debug("Service         >> " + 1);
            logger.debug("MoUrl           >> " + " ");
            logger.debug("Timestamp       >> " + timestamp);
            SendOrderRsp sendOrderResponse = InfoblastConnector.sendOrder(sendOrderRequest);
            return sendOrderResponse;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public SendUserCodeRsp sendUserCode(String sessionID, String virtualNumber, String addProfileParam, String ibPassword) {
        SendUserCodeRsp sendUserCode = null;
        SendUserCodeReq userCodeReq = new SendUserCodeReq();
        try {
            System.out.println("Invoke the sendUserCode API ...");
            logger.info("Invoke the sendUserCode API ...");
            userCodeReq.setCpId(14);
            String msgId = getMsgId();
            userCodeReq.setMsgId(msgId);
            userCodeReq.setSessionId(sessionID);
            userCodeReq.setServiceNumber(virtualNumber);
            userCodeReq.setServiceType("1");
            userCodeReq.setInfoblastId(virtualNumber);
            userCodeReq.setAddProfile(addProfileParam);
            String timeStamp = getTimeStamp();
            userCodeReq.setTimestamp(timeStamp);
            if (addProfileParam.equalsIgnoreCase("P") && ibPassword != null) {
                userCodeReq.setInfoblastPassword(ibPassword);
            }
            logger.debug("CP ID               >> " + 14);
            logger.debug("msgId               >> " + msgId);
            logger.debug("sessionId           >> " + sessionID);
            logger.debug("ServiceNumber       >> " + virtualNumber);
            logger.debug("ServiceType         >> " + "1");
            logger.debug("InfoblastId         >> " + virtualNumber);
            logger.debug("AddProfile          >> " + addProfileParam);
            logger.debug("Timestamp           >> " + timeStamp);
            logger.debug("InfoblastPassword   >> " + ibPassword);

            sendUserCode = InfoblastConnector.sendUserCode(userCodeReq);
            return sendUserCode;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sendUserCode;
    }

    private static String getMsgId() {
        String randomMsgId = "0";
        try {
            int aNumber = 0;
            aNumber = (int) ((Math.random() * 900000000) + 1000000000);
            System.out.print((aNumber));
            randomMsgId = String.valueOf(aNumber);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return randomMsgId;
    }

    private static String getTimeStamp() {
        String timeStamp = "0";
        try {
            DateFormat sdf = new SimpleDateFormat("YYYYMMDDHHMMSS");
            timeStamp = sdf.format(new Date());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return timeStamp;
    }
}
