<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserAdManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgAdvertiserAdDetails"%>
<%
    String id = request.getParameter("_id");
    SgAdvertiserAdDetails adDetails = new AdvertiserAdManagement().getAdById(Integer.parseInt(id));
%>        
<div id="pdfAdModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="rejectPackageModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <b id="rejectPackageModal">PDF Ad Image</b>
            </div>          
            <div class="modal-body">
                <div class="row-fluid">
                    <form class="form-horizontal" id="rejectPackageForm">
                        <fieldset>
                            <div class="control-group">                                
                                <input type="hidden"  id="_adId" name="_adId" >
                                <div class="controls col-lg-12" style="margin-left: 15%">                                    
                                    <image src="data:image/jpg;base64,<%=adDetails.getPdfAdImage()%>" alt="PDF Ad Image" width="350" height="350"> 
                                </div>
                            </div>                            
                        </fieldset>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div id="edit-partner-result"></div>
                <button class="btn btn-info btn-xs" data-dismiss="modal">Close</button>
                <button class="btn btn-success btn-xs" onclick="rejectPDFAdmodal('<%=GlobalStatus.REJECTED%>', '<%=adDetails.getAdvertiserAdId()%>')" id="addPartnerButtonE">Reject request</button>
            </div>
        </div>
    </div>
</div>
