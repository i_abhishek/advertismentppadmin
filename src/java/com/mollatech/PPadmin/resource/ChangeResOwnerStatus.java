package com.mollatech.PPadmin.resource;

import static com.mollatech.PPadmin.resource.ChangeResOwnerStatus.logger;
import com.mollatech.serviceguard.nucleus.db.SgResourceowner;
import com.mollatech.serviceguard.nucleus.db.connector.management.ResourceOwnerManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author Ash
 */
@WebServlet(name = "ChangeResOwnerStatus", urlPatterns = { "/ChangeResOwnerStatus" })
public class ChangeResOwnerStatus extends HttpServlet {

    static final Logger logger = Logger.getLogger(ChangeResOwnerStatus.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        logger.info("Requested Servlet is ChangeResOwnerStatus at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        JSONObject json = new JSONObject();
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        try {
            int ownerId = Integer.parseInt(request.getParameter("resId"));
            logger.debug("value of ownerId : " + ownerId);
            int status = Integer.parseInt(request.getParameter("status"));
            logger.debug("value of status : " + status);
            SgResourceowner SgResourceowner = new ResourceOwnerManagement().getResourceownerbyId(SessionId, ownerId);
            SgResourceowner.setStatus(status);
            int res = new ResourceOwnerManagement().updateDetails(SgResourceowner);
            if (res == 0) {
                json.put("result", "success");
                json.put("message", "Owner Status Updated Successfully.");
            } else {
                json.put("result", "error");
                json.put("message", "Error in Updating Resource Ownner Status.");
            }
        } catch (Exception ex) {
            logger.error("Exception at ChangeResOwnerStatus ", ex);
            json.put("result", "error");
            json.put("message", "Error in Updating Resource Ownner Status.");
        } finally {
            logger.info("Response of ChangeResOwnerStatus " + json.toString());
            logger.info("Response of ChangeResOwnerStatus Servlet at " + new Date());
            out.print(json);
            out.flush();
            out.close();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
