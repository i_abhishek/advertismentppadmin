<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceOwnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@include file="header.jsp"%>
<link href="./css/bootstrap-timepicker.css" rel="stylesheet"/>
<link  rel="stylesheet" href="./css/datepicker.css">
<script src="./js/bootstrap.timepicker.min.js"></script>
<script src="./js/bootstrap-datepicker.js"></script>
<script src="./js/json3.min.js"></script>
<script src="./js/bootstrap-timepicker.js"></script>
<script src="js/reports.js" type="text/javascript"></script>
<script src="./js/modal.js"></script>


<%    
   
   ResourceDetails[] resourceDetailses = new ResourceManagement().getAllResources();
   String resourceName = rUser.getResourceName();
   String[] resArray = null;
   Map<String, String> resMap = new HashMap<String, String>();
   if (resourceName != null) {
        resArray = resourceName.split(",");
    }
    if (resArray != null) {
        for (String resId : resArray) {
            if (!resId.isEmpty()) {
                if (resMap.get(resId) == null) {
                   ResourceDetails resourceObj = new ResourceManagement().getResourceById(Integer.parseInt(resId));
                   if (resourceObj != null) {
                        resMap.put(resId, resourceObj.getName());
                    }
                }
            }
        }
    }

%>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header text-uppercase">Monthly Revenue Details</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-cubes"></i><b> Revenue details </b>                
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST">
                        <div class="form-group">
                            
                             <label class="control-label col-lg-1">Name</label>
                            <div class="col-lg-2">

                                <select class="form-control" id="_revOwnername" name="_revOwnername">
                                    <option value="-1">Select</option>
                                    <%                                        if (resourceDetailses != null) {
                                            for (ResourceDetails details : resourceDetailses) {%>
                                    <option value="<%=details.getResourceId()%>"><%=details.getName()%></option>
                                    <% }
                                        }
                                    %>
                                </select>

                            </div>
                            
                            <label class="control-label col-lg-1">Resource</label>
                            <div class="col-lg-2">
                                <select id="_revResource" name="_revResource"  class="form-control" style="width: 100%">
                                    <option value="-1" selected>Select Resource</option>
                                    <%  if (!resMap.isEmpty()) {
                                            Set<String> keyS = resMap.keySet();
                                            for (String keyData : keyS) {
                                                String resName = resMap.get(keyData);
                                    %>
                                    <option value="<%=keyData%>"><%=resName%></option>
                                    <% }
                                        }
                                    %>                                                              
                                </select>
                            </div>
                            <label class="control-label col-lg-1">Month</label>
                            <div class="col-lg-2">
                                <select id="_revMonth" name="_revMonth"  class="form-control" style="width: 100%">                                
                                    <option value="Jan">Jan</option>
                                    <option value="Feb">Feb</option>
                                    <option value="Mar">Mar</option> 
                                    <option value="Apr">Apr</option>
                                    <option value="May">May</option>
                                    <option value="Jun">Jun</option>
                                    <option value="Jul">Jul</option>
                                    <option value="Aug">Aug</option>
                                    <option value="Sep">Sep</option>
                                    <option value="Oct">Oct</option>
                                    <option value="Nov">Nov</option>
                                    <option value="Dec">Dec</option>
                                </select>
                            </div>
                            <label class="control-label col-lg-1">Year</label>
                            <div class="col-lg-2">
                                <select class="form-control" id="_revYear" name="_revYear">
                                    <%
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy");
                                    for (int year = (Integer.parseInt(format.format(new Date())) - 10); year <= Integer.parseInt(format.format(new Date())); year++) {%>
                                    <option value="<%=year%>"><%=year%></option>
                                    <% }%>
                                </select>
                                <script>
                                    document.getElementById('_revYear').value = '<%=format.format(new Date())%>';
                                </script>
                            </div>                          
                            <div class="col-lg-2">                            
                                <a class=" btn btn-success btn-sm" onclick="revenueGraphicalReport()"><i class="fa fa-bar-chart"></i> Generate Report</a>                                  
                            </div>                      
                        </div>
                    </form>
                    </br>
<!--                    <div class="row-fluid">
                        <div class="span12" id="reportDiv" style="display: none">
                            <div class="span12" style="alignment-adjust: central">    
                                <h5 align="center" class="text-primary" style="font-weight: bold">Last 7 day Revenue Status</h5>
                                <font style="font-family: monospace">
                                Revenue details
                                </font>
                                <div id="_revGraphData"></div>    
                                <div  align="center" style="font-family: monospace" >
                                    Days
                                </div>
                            </div>
                            <br>
                            <div  class="span12" style="alignment-adjust: central">
                                <h5 align="center" class="text-primary" style="font-weight: bold">Weekly Revenue Status</h5>
                                <font style="font-family: monospace">
                                Revenue details
                                </font>
                                <div id="_revBarData"></div>         
                                <div  align="center" style="font-family: monospace" >
                                    Week
                                </div>
                            </div> 
                                        <div class="form-group" id="_revGraphData"></div>                           
                        </div>
                    </div>-->
                </div>
            </div>               
