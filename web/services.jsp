<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Warfiles"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MSConfig"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="java.util.Date"%>
<%@page import="javax.xml.datatype.XMLGregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@include file="header.jsp" %>
<script src="./js/sgservices.js"></script>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header text-uppercase">Services Details</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47; <i class="fa fa-list-ol"></i><b> List of Services</b>
                </div>

                <div class="panel-body">
                    <%SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm a");%>
                    <table class="table table-striped table-bordered table-hover display" id="dataTables-example">
                        <thead>
                            <tr>
                                <td/>
                                <th>No.</th>
                                <th>Access Point</th>
                                <th>Res. Name</th>
                                <th>Env</th>
                                <th>Version</th>
                                <th>Desc</th>
                                <th>Original WS</th>
                                <th>TF WSDL</th>
                                <th>TF WADL</th>

                            </tr>
                        </thead>
                        <tbody>
                            <%
                                Object sobject = null;
                                sobject = new SettingsManagement().getSetting(SessionId, channelId, SettingsManagement.MATERSLAVE, SettingsManagement.PREFERENCE_ONE);
                                MSConfig msConfig = (MSConfig) sobject;
                                Accesspoint Res[] = null;
                                Res = new AccessPointManagement().getAllAccessPoint(SessionId, channelId);
                                int count = 0;
                                if (Res != null) {
                                    for (int i = 0; i < Res.length; i++) {
                                        if (Res[i].getStatus() != -99) {
                                            Accesspoint apdetails = Res[i];
                                            if (apdetails != null) {
                                                int resID = -1;
                                                String userStatus = "user-status-value-" + i;
                                                String resourceList = apdetails.getResources();
                                                String[] resids = resourceList.split(",");
                                                for (int j = 0; j < resids.length; j++) {
                                                    if (!resids[j].isEmpty() && resids[j] != null) {
                                                        resID = Integer.parseInt(resids[j]);
                                                        ResourceDetails rsName = new ResourceManagement().getResourceById(SessionId, channelId, resID);
                                                        TransformDetails tmdetail = new TransformManagement().getTransformDetails(SessionId, channelId, apdetails.getApId(), resID);
                                                        if (rsName != null && tmdetail != null) {
                                                            Warfiles warfiles = new WarFileManagement().getWarFile(SessionId, channelId, apdetails.getApId());
                                                            if (warfiles != null) {
                                                                Map map = (Map) Serializer.deserialize(warfiles.getWfile());
                                                                for (Object key : map.keySet()) {
                                                                    if (key.toString().split(":")[1].equalsIgnoreCase("Running")) {
                                                                        String ver = key.toString().split(":")[0].replace("SB", "");
                                                                        String env = "";
                                                                        String strEnv = "Production";
                                                                        org.json.JSONObject object = new JSONObject(tmdetail.getImplClassName());
                                                                        String implName = object.getString(ver);
                                                                        if (key.toString().split(":")[0].endsWith("SB")) {
                                                                            env = "SB";
                                                                            strEnv = "Test";
                                                                        }
                                                                        count++;
                            %>
                            <tr>
                                <td/>
                                <td><%=count%></td>
                                <td><%=apdetails.getName()%></td>
                                <td><%=rsName.getName()%></td>
                                <td><%=strEnv%></td>
                                <td><%=ver%></td>
                                <td><a class="btn btn-success btn-xs" href = './transformDetails.jsp?_apid=<%=apdetails.getApId()%>&_resId=<%=resID%>&ver=<%=ver%>'><i class="fa fa-eye"></i> View</a></td>
                                <td><a href="#" class="btn btn-info btn-xs" onclick="viewoWSURL('<%=rsName.getName()%>', '<%=rsName.getWsdlUrl()%>')" ><i class="fa fa-eye"></i> View</a></td>
                                <%if (msConfig != null) {
                                        String ssl = "http";
                                        if (msConfig.ssl.equalsIgnoreCase("yes")) {
                                            ssl = "https";
                                        }
                                %>
                                <td><a href="#" class="btn btn-warning btn-xs" onclick="viewWSURL('<%=ssl%>://<%=msConfig.hostIp%>:<%=msConfig.hostPort%>/<%=apdetails.getName().replaceAll(" ", "") + env + "V" + ver%>/<%=implName%>?wsdl')" >View</a></td>
                                <td><a href="#" class="btn btn-danger btn-xs" onclick="viewWSURL('<%=ssl%>://<%=msConfig.hostIp%>:<%=msConfig.hostPort%>/<%=apdetails.getName().replaceAll(" ", "") + env + "V" + ver%>/<%=implName%>/application.wadl')" >View</a></td>
                                <%} else {%>
                                <td><button  class="btn btn-warning btn-xs" onclick="viewWSURL('Please do Global Configuration')" ><i class="fa fa-eye"></i> View</button></td>
                                <td><button  class="btn btn-danger btn-xs" onclick="viewWSURL('Please do Global Configuration')" ><i class="fa fa-eye"></i> View</button></td>
                                <%}%>

                            </tr>

                            <%
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            %>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript 
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.min.js"></script>
<script src="js/morris-data.js"></script>-->

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->
<!--<script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>
<script src="js/bootbox.min.js" type="text/javascript"></script>-->
<script>
//                                    $(document).ready(function () {
//                                        $('#dataTables-example').DataTable({
//                                            responsive: true
//                                        });
//                                    });
</script>   
</body>
</html>
<%@include file="footer.jsp" %>
