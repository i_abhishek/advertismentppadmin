/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.billing;

import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.connector.communication.SGStatus;
import com.mollatech.serviceguard.nucleus.commons.UtilityFunctions;
import com.mollatech.serviceguard.nucleus.db.Operators;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgPaymentdetails;
import com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PDFInvoiceManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PaymentManagement;
import com.mollatech.serviceguard.nucleus.settings.SendNotification;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.apache.log4j.Logger;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "SendInvoiceToDeveloper", urlPatterns = {"/SendInvoiceToDeveloper"})
public class SendInvoiceToDeveloper extends HttpServlet {

    static final Logger logger = Logger.getLogger(SendInvoiceToDeveloper.class);
    public static final int PENDING = 2;
    public static final int SEND = 0;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Request servlet is #SendInvoiceToDeveloper from #PPortal at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String channelName = "ServiceGuard Portal";
        String result = "success";
        String message = "Invoice successfully send to developer";
        String channelId = (String) request.getSession().getAttribute("_channelId");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        JSONObject tierOrSlabUsage = (JSONObject) request.getSession().getAttribute("manualTierOrSlabUsageDetails");
        JSONObject manualcaasServiceUsageDetails = (JSONObject) request.getSession().getAttribute("manualcaasServiceUsageDetails");
        JSONObject pgTransactionUsage = (JSONObject) request.getSession().getAttribute("manualpgTransactionDetails");
        Double totalPaymentAmount = (Double) request.getSession().getAttribute("manualtotalPaymentAmount");
        String _partnerId = request.getParameter("_partnerId");
        String invoiceId = request.getParameter("invoiceId");
        String cancellationCharge = request.getParameter("cancellationCharge");
        String latePenaltyCharge = request.getParameter("latePenaltyCharge");
        int partnerid = 0;
        Double packageAmount = null;
        Double changePackageCharge = null;
        Double subscribedserviceCharge = null;
        Double subscribedreactivationCharge = null;
        Double subscribedcancellationCharge = null;
        Double subscribedlatePenaltyCharge = null;
        if (_partnerId != null && !_partnerId.isEmpty()) {
            partnerid = Integer.parseInt(_partnerId);
        }
        if (cancellationCharge != null && !cancellationCharge.isEmpty() && !cancellationCharge.equals("null")) {
            subscribedcancellationCharge = Double.parseDouble(cancellationCharge);
            if(subscribedcancellationCharge <= 0){
                subscribedcancellationCharge = null;
            }
        }
        if (latePenaltyCharge != null && !latePenaltyCharge.isEmpty() && !latePenaltyCharge.equals("null")) {
            subscribedlatePenaltyCharge = Double.parseDouble(latePenaltyCharge);
        }
        PartnerDetails partnerObj = new PartnerManagement().getPartnerDetails(partnerid);
        try {
            SgSubscriptionDetails packageSubscribed = new PackageSubscriptionManagement().getSubscriptionbyPartnerId(SessionId, channelId, partnerid);
            if (packageSubscribed.getServiceCharge() != null) {
                subscribedserviceCharge = Double.parseDouble(packageSubscribed.getServiceCharge());
            }
            packageAmount = new Float(packageSubscribed.getPlanAmount()).doubleValue();
            
            SgPaymentdetails paymentObj = new PaymentManagement().getPaymentDetailsbyPartnerAndSubscriptionID(packageSubscribed.getBucketId(), partnerObj.getPartnerId());
            
            String tmessage = (String) LoadSettings.g_templateSettings.getProperty("email.package.manualPayment.notification");
            if (tmessage != null) {
                tmessage = tmessage.replaceAll("#partnerName#", partnerObj.getPartnerName());
                tmessage = tmessage.replaceAll("#channel#", channelName);
                tmessage = tmessage.replaceAll("#packageName#", packageSubscribed.getBucketName());
                tmessage = tmessage.replaceAll("#datetime#", UtilityFunctions.getTMReqDate(new Date()));
                tmessage = tmessage.replaceAll("#subscriptionDate#", UtilityFunctions.getTMReqDate(new Date()));
                tmessage = tmessage.replaceAll("#email#", partnerObj.getPartnerEmailid());
            }
            String mimeType[] = {"application/octet-stream"};

            String invoiceFilePath = new PDFInvoiceManagement().createInvoice(tierOrSlabUsage, changePackageCharge, subscribedserviceCharge, subscribedreactivationCharge, subscribedcancellationCharge, subscribedlatePenaltyCharge, packageAmount, partnerObj, null, invoiceId, packageSubscribed, null,manualcaasServiceUsageDetails,pgTransactionUsage);
            String[] arrInvoicePath = {invoiceFilePath};
            int productType = 3;
            Operators[] operatorObj = new OperatorsManagement().getAllOperators(channelId);
            String[] operatorEmail = null;
            if (operatorObj != null) {
                operatorEmail = new String[operatorObj.length];
                for (int i = 0; i < operatorObj.length; i++) {
                    operatorEmail[i] = (String) operatorObj[i].getEmailid();
                }
            }
            SGStatus status = new SendNotification().SendEmailWithAttachment(channelId, partnerObj.getPartnerEmailid(), "Pay your bill for TM API ", tmessage, operatorEmail, null, arrInvoicePath, mimeType, productType);
            //SGStatus status = new SendNotification().SendEmail(channelId, partnerObj.getPartnerEmailid(), "Pay your bill for TM API ( " + packageSubscribed.getBucketName() + " )", tmessage, operatorEmail, null, arrInvoicePath, mimeType, productType);
            if (status.iStatus != PENDING && status.iStatus != SEND) {
                result = "error";
                message = "Invoice failed to send to developer on his/her email.";
                try {
                    json.put("_result", result);
                    logger.debug("Response of SendInvoiceToDeveloper Servlet's Parameter  result is " + result);
                    json.put("_message", message);
                    logger.debug("Response of SendInvoiceToDeveloper Servlet's Parameter  message is " + message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return;
            }
            if (paymentObj == null) {
                paymentObj = new SgPaymentdetails();
                paymentObj.setInvoiceNo(invoiceId);
                paymentObj.setSubscriptionId(packageSubscribed.getBucketId());
                paymentObj.setPartnerId(partnerObj.getPartnerId());
                if(subscribedlatePenaltyCharge != null){
                    paymentObj.setLatePenaltyCharge(Float.parseFloat(subscribedlatePenaltyCharge.toString()));
                }
                if(subscribedcancellationCharge != null){
                    paymentObj.setCancellationCharge(Float.parseFloat(subscribedcancellationCharge.toString()));
                }
                paymentObj.setInvoiceSendedOn(new Date());
                if (tierOrSlabUsage != null) {
                    paymentObj.setTierOrSlabUsage(tierOrSlabUsage.toString());
                }
                if (manualcaasServiceUsageDetails != null && manualcaasServiceUsageDetails.length() != 0) {
                    paymentObj.setCaasServiceUsage(manualcaasServiceUsageDetails.toString());
                }
                if (pgTransactionUsage != null && pgTransactionUsage.length() != 0) {
                    paymentObj.setPgtransactionUsage(pgTransactionUsage.toString());
                }
                paymentObj.setPaidamount(Float.parseFloat(totalPaymentAmount.toString()));
                new PaymentManagement().createPaymentDetails(SessionId, channelId, paymentObj);
            } else {
                paymentObj.setInvoiceNo(invoiceId);
                paymentObj.setSubscriptionId(packageSubscribed.getBucketId());
                paymentObj.setPartnerId(partnerObj.getPartnerId());
                if(subscribedlatePenaltyCharge != null){
                    paymentObj.setLatePenaltyCharge(Float.parseFloat(subscribedlatePenaltyCharge.toString()));
                }
                if(subscribedcancellationCharge != null){
                    paymentObj.setCancellationCharge(Float.parseFloat(subscribedcancellationCharge.toString()));
                }
                paymentObj.setInvoiceSendedOn(new Date());
                if (tierOrSlabUsage != null) {
                    paymentObj.setTierOrSlabUsage(tierOrSlabUsage.toString());
                }
                if (manualcaasServiceUsageDetails != null && manualcaasServiceUsageDetails.length() != 0) {
                    paymentObj.setCaasServiceUsage(manualcaasServiceUsageDetails.toString());
                }
                if (pgTransactionUsage != null && pgTransactionUsage.length() != 0) {
                    paymentObj.setPgtransactionUsage(pgTransactionUsage.toString());
                }
                paymentObj.setPaidamount(Float.parseFloat(totalPaymentAmount.toString()));
                new PaymentManagement().updatePaymentDetails(paymentObj);
            }
            File deleteFile = new File(invoiceFilePath);
            deleteFile.delete();
            result = "success";
            message = "Invoice sended to developer on his/her email.";
            json.put("_result", result);
            logger.debug("Response of SendInvoiceToDeveloper Servlet's Parameter  result is " + result);
            json.put("_message", message);
            logger.debug("Response of SendInvoiceToDeveloper Servlet's Parameter  message is " + message);
            return;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            logger.info("Response of #SendInvoiceToDeveloper from #PPAdmin " + json.toString());
            logger.info("Response of #SendInvoiceToDeveloper from #PPAdmin Servlet at " + new Date());
            out.print(json);
            out.flush();
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
