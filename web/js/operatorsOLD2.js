var Operatortype = 0;

function strCompare(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function bootboxmodel(content) {
    //alert(" ");
    var popup = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' +
            '<button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>' +
            content +
            '</div></div></div></div>');
    popup.modal();
}
var waiting;
function pleasewait() {
    waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Authenticating...</h4>'+
            
            '<div class="progress progress-striped active">'+
                '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">'+
                '</div>'+
            '</div>'+
            
            '</div>' +
            '</div></div></div></div>');
    //waiting = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"></div></div></div>');
    waiting.modal();
}
function showAlert(message, type, closeDelay){
   if ($("#alerts-container").length == 0) {
        // alerts-container does not exist, create it
        $("body")
            .append( $('<div id="container" style="'+
                'width: %; margin-left: 65%; margin-top: 90%;">') );
        }
        // default to alert-info; other options include success, warning, danger
        type = type || "info";  
        
        // create the alert div
    var alert = $('<div class="alert alert-' + type + ' fade in">')
        .append(
            $('<button type="button" class="close" data-dismiss="alert">')
            .append("&times;")
        )
        .append(message);
        
    // add the alert div to top of alerts-container, use append() to add to bottom
    $("#alerts-container").prepend(alert);
    
    // if closeDelay was passed - set a timeout to close the alert
    if (closeDelay)
        window.setTimeout(function() { alert.alert("close") }, closeDelay);     

}
function signup() {
    window.location.href = "register.jsp";
}

function updateprofile() {
  
    var s = './editPartner';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#update-profile").serialize(),
        success: function(data) {
            if (strCompare(data._result, "error") == 0) {
                alert(data._message);
                bootboxmodel("<h2><font color=blue>" + data._message + "</font></h2>");
            }
            else if (strCompare(data._result, "success") == 0) {
                alert(data._message);

            }
        }
    });
}

function changePstatus(status, partid, uidiv) {
    var s = './ChangePstatus?_status=' + status + '&_partid=' + partid;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strCompare(data._result, "error") == 0) {
//                Alert4serviceguard("<span><font color=red>" + data._message + "</font></span>");
            }
            else if (strCompare(data._result, "success") == 0) {
                bootboxmodel("<h2><font color=blue>" + data._message + "</font></h2>");
//                function (_result) {
//                    window.location = "./partners.jsp";
//                });
            }
        }
    });
}
function alertShow(msg){
        $("#alert_placeholder").append('<div class="alert alert-success" role="alert">'+
    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
    '<strong>Success!</strong> You have been signed in successfully!'+
    '</div>');
    }
function opratorlogin(name, password) {
//      var username = document.getElementById("_name").value;
//        var password = document.getElementById("_passwd").value;
////    if ($('#rememberpasswordoperator').is(":checked")) {
//      
//        document.cookie = "Operatorusername=" + username;
//        document.cookie = "Operatorpassword=" + password;
//        document.cookie = "OperatorChecked=1";
//    } else {
//        document.cookie = "OperatorChecked=0";
//    }
//alert("Hi ");
//    var button = document.getElementById("loginData").value;
//    alert(button+ "buttonData");
    var s = './operatorLogin';
    pleasewait();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#login_form").serialize(),
        success: function(data) {
            if (strCompare(data._result, "error") === 0) {
                //bootboxmodel("<h2><font color=red>" + data._message + "</font></h2>");
                waiting.modal('hide');
                showAlert(data._message, "danger", 3000);
            }
            else if (strCompare(data._result, "success") === 0) {
               // window.setTimeout(bootboxmodel("<h2><font color=blue>" + data._message + "</font></h2>"), 3000);
                waiting.modal('hide');
                showAlert(data._message, "success", 2000);
                window.location.href = "./home.jsp";
//                alert(data._message);

            }
        }
    });
}
//alert(_0x27b7[0]);
