/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.billing;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.SgBucketdetails;
import com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

/**
 *
 * @author Ashu
 */
@WebServlet(name = "SetDefaultPackage", urlPatterns = {"/SetDefaultPackage"})
public class SetDefaultPackage extends HttpServlet {

    static final Logger logger = Logger.getLogger(EditTierPrice.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Request servlet is #EditTierPrice from #PPAdmin at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        int bucketId = Integer.parseInt(request.getParameter("packageId"));
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        PackageManagement management = new PackageManagement();
        RequestPackageManagement requestPackage = new RequestPackageManagement();
        try {
            SgBucketdetails bucketdetails = management.getPackageByDefault(SessionId, channelId);
            SgReqbucketdetails requestedPackageObj = requestPackage.getRequestPackageByDefault(SessionId, channelId);
            SgBucketdetails packageObj = management.getPackageById(SessionId, channelId, bucketId);
            if (packageObj.getStatus() != GlobalStatus.APPROVED) {
                json.put("_result", "error");
                logger.debug("Response of SetDefaultPackage Servlet's Parameter  result is: " + "error");
                json.put("_message", "Package not approved yet");
                logger.debug("Response of SetDefaultPackage Servlet's Parameter  message is: " + "Package not approved yet");
                return;
            }
            if (bucketdetails != null) {
                bucketdetails.setMarkAsDefault(null);
                management.editPackage(SessionId, bucketdetails);
            }
            if (requestedPackageObj != null) {
                requestedPackageObj.setMarkAsDefault(null);
                requestPackage.editReqPackage(SessionId, channelId, requestedPackageObj);
            }

            packageObj.setMarkAsDefault(GlobalStatus.DEFAULT);
            management.editPackage(SessionId, packageObj);
            requestedPackageObj = requestPackage.getReqPackageByName(SessionId, channelId, packageObj.getBucketName());
            requestedPackageObj.setMarkAsDefault(GlobalStatus.DEFAULT);
            int res = requestPackage.editReqPackage(SessionId, channelId, requestedPackageObj);
            if (res == 0) {
                json.put("_result", "success");
                logger.debug("Response of SetDefaultPackage Servlet's Parameter  result is: " + "success");
                json.put("_message", "Package is Set As Default");
                logger.debug("Response of SetDefaultPackage Servlet's Parameter  message is: " + "Package is Set As Default");
            } else {
                json.put("_result", "error");
                logger.debug("Response of SetDefaultPackage Servlet's Parameter  result is: " + "error");
                json.put("_message", "Package Can Not Set As Default");
                logger.debug("Response of SetDefaultPackage Servlet's Parameter  message is: " + "Package Can Not Set As Default");
            }
        } catch (Exception ex) {
            logger.error("Exception at SetDefaultPackage ", ex);
            json.put("_result", "error");
            logger.debug("Response of SetDefaultPackage Servlet's Parameter  result is: " + "error");
            json.put("_message", ex.getMessage());
            logger.debug("Response of SetDefaultPackage Servlet's Parameter  message is: " + ex.getMessage());
        } finally {
            logger.info("Response of SetDefaultPackage Servlet at " + new Date());
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
