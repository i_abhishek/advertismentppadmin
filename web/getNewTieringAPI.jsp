<%@page import="com.mollatech.serviceguard.nucleus.db.SgBucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement"%>

<%@page import="com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MethodName"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Classes"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Methods"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <div class="control-group">
        <label class="control-label"  for="username"></label>
        <div class="controls">
            <select id="_APIForTieringPricing2" name="_APIForTieringPricing2"  class="form-control span2" onchange="showSubscribeTieringPricing(this.value)" style="width: 100%">
                <%
                    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
                    String channelId = (String) request.getSession().getAttribute("_channelId");
                    String packageName = request.getParameter("packageName");
                    String apName = request.getParameter("_apName");
                    if(apName != null && !apName.equals("-1")){
//                        SgReqbucketdetails reqObj = new RequestPackageManagement().getReqPackageByName(SessionId, channelId, packageName);
                        SgBucketdetails reqObj = new PackageManagement().getPackageByName(SessionId, channelId, packageName);
                        if (reqObj != null) {
                            String tierDetails = reqObj.getTierRateDetails();
                            if (tierDetails != null) {
                                JSONArray jsOld = new JSONArray(tierDetails);
                                String ver = "";
                                for (int j = 0; j < jsOld.length(); j++) {
                                    JSONObject jsonexists1 = jsOld.getJSONObject(j);
                                    
                                    Iterator<String> keys = jsonexists1.keys();
                                    while (keys.hasNext()) {
                                        String keyData = keys.next();
                                        String[] keyDetails = keyData.split(":");                                    
                                            //res += keyDetails[1] + ",";
                                            if(ver.isEmpty()){
                                                ver += keyDetails[3] + ",";
                                            }else if(ver.contains(keyDetails[3])){
                                                continue;
                                            }else{
                                                ver += keyDetails[3] + ",";
                                            }
                                        
                                    }
                                }
                                if (!ver.equals("")) {
                                    String[] versionDetails = ver.split(",");
                                    if (versionDetails != null) {
                                        for (int i = 0; i < versionDetails.length; i++) {
                %>
                <option value="<%=versionDetails[i]%>"><%=versionDetails[i]%></option>               
                <%}
                                }
                            }
                        }
                    }
                }else {%>                 
                <option value="-1">Select Resource</option>
                <%}%>
            </select>
        </div>
    </div>
