<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgBucketdetails"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <div class="control-group">
        <label class="control-label"  for="username"></label>
        <div class="controls">
            <%
                String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
                String channelId = (String) request.getSession().getAttribute("_channelId");
                String apName = request.getParameter("_apName");
                String packageName = request.getParameter("_packageName");               
                SgBucketdetails reqObj = new PackageManagement().getPackageByName(SessionId, channelId, packageName);
                String flatPriceDetails = reqObj.getFlatPrice();
                JSONArray jsOld = new JSONArray(flatPriceDetails);
                String res = "";
                for (int j = 0; j < jsOld.length(); j++) {
                    JSONObject jsonexists1 = jsOld.getJSONObject(j);
                    Iterator<String> keys = jsonexists1.keys();
                    while (keys.hasNext()) {
                        String keyData = keys.next();
                        String[] keyDetails = keyData.split(":");
                        if (keyDetails[0].equals(apName)) {
                            res += keyDetails[1] + ",";
                        }
                    }
                }
                String resName = "";
                if (!res.equals("")) {
                    String[] resourcesDetails = res.split(",");
                    if (resourcesDetails != null) {
                        for (int i = 0; i < resourcesDetails.length; i++) {
                            if (!resName.contains(resourcesDetails[i] + ",")) {
                                resName += resourcesDetails[i] + ",";
            %>
            <select id="_flatAccesspoint" name="_flatAccesspoint"  class="form-control span2" onchange="flatVersion(this.value, '<%=packageName%>')" style="width: 100%">
                <option value="<%=resourcesDetails[i]%>"><%=resourcesDetails[i]%></option>
            </select>
            <%}
                    }
                }
            } else {%>
            <select id="_flatAccesspoint" name="_flatAccesspoint"  class="form-control span2" onchange="flatVersion(this.value, '<%=packageName%>')" style="width: 100%">
                <option value="-1">Select Resource</option>
            </select>
            <%}%>
        </div>
    </div>
