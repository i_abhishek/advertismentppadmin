<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MethodName"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Classes"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Methods"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        AccessPointManagement ppw = new AccessPointManagement();
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        String apName = request.getParameter("_apname");
        String packageName = request.getParameter("_packageName");
        String resourceName = request.getParameter("_resourceName");
        String version = request.getParameter("versionData");
        SgReqbucketdetails reqObj = new RequestPackageManagement().getReqPackageByName(SessionId, channelId, packageName);
        String apDetails = reqObj.getApRateDetails();
        JSONArray jsOld = new JSONArray(apDetails);
        String ver = "";
        String key = apName + ":" + resourceName + ":" + version;
        JSONObject reqJSONObj = null;
        for (int j = 0; j < jsOld.length(); j++) {
            JSONObject jsonexists1 = jsOld.getJSONObject(j);
            if (jsonexists1.has(key)) {
                reqJSONObj = jsonexists1.getJSONObject(key);
                if (reqJSONObj != null) {
                    break;
                }
            }
        }
        //float amountOfApi = 0.0f;
        if (reqJSONObj != null) {
            //String methodN = reqJSONObj.names().getString(index);
            //amountOfApi = Float.parseFloat(methodN);
            Iterator<String> keys = reqJSONObj.keys();
            String mainCredit = reqJSONObj.getString("mainCredit");
            String freeCredit = reqJSONObj.getString("freeCredit");

    %>
    <!--                <div class="form-group">
                        <label class="col-lg-2 control-label">Main credit</label>
                        <div class="col-lg-3">
                            <div class="input-group">
                                <span class="input-group-addon">$</span>-->
    <input type="hidden" class="form-control" disabled id="mainCreditForAP" name="mainCreditForAP" placeholder="Main Credit" value="<%=mainCredit%>">                                    
    <!--                        </div>
                        </div>
                        <label class="col-lg-2 control-label">Free credit</label>
                        <div class="col-lg-3" style="margin-left: 8.5%">
                            <div class="input-group">
                                <span class="input-group-addon">$</span>-->
    <input type="hidden" class="form-control" id="freeCreditForAP" disabled name="freeCreditForAP" placeholder="Free Credit" value="<%=freeCredit%>">                                    
    <!--                        </div>
                        </div>
                    </div>-->
    <%            int count = 1;
        while (keys.hasNext()) {
            int selectedAPI = GlobalStatus.Per_API;
            String keyData = keys.next();
            String valueData = reqJSONObj.getString(keyData);
            String charge;
            if (valueData.split(":").length == 1) {
                charge = valueData;
            } else {
                charge = valueData.split(":")[0];
                selectedAPI = Integer.parseInt(valueData.split(":")[1]);
            }
            if (keyData.equals("freeCredit") || keyData.equals("mainCredit")) {
                continue;
            }

    %>
    <div class="form-group">
        <label class=" control-label col-lg-2"><%=keyData%></label>

        <div class="col-lg-3" >
            <div class="input-group">
                <span class="input-group-addon">$</span>
                <input type="text" class="form-control" placeholder="apiPrice" id="apiPrice" name="apiPrice" disabled value="<%=charge%>">                                                                  
            </div>
        </div>
        <div class="col-lg-4" >
            <div class="input-group">
                <select  class="form-control col-lg-4" name="chargingSrategy<%=count - 1%>" id="chargingSrategy<%=count - 1%>" disabled>
                    <option value="<%=GlobalStatus.Per_User_Per_Renewal%>">Per User Per Renewal</option>
                    <option value="<%=GlobalStatus.Per_API%>">Per API</option>
                    <option value="<%=GlobalStatus.Per_Entry_Per_API%>">Per Entry Per API</option>
                </select>

            </div>
        </div>
    </div>
    <script>
        document.getElementById("chargingSrategy<%=count - 1%>").value = "<%=selectedAPI%>";
    </script>
    <%count++;
            }
        }

    %>



