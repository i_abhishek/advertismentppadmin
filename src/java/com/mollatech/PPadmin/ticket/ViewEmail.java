/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.ticket;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.SgEmailticket;
import com.mollatech.serviceguard.nucleus.db.connector.management.EmailManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author bluebricks
 */
@WebServlet(name = "ViewEmail", urlPatterns = {"/ViewEmail"})
public class ViewEmail extends HttpServlet {

    static final Logger logger = Logger.getLogger(ViewEmail.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String result = "success";
        String message = "View successfully";
        PrintWriter out = response.getWriter();
        logger.info("Request servlet is #deleteEmail from #PPAdmin at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        JSONObject json = new JSONObject();

        String emailNo = request.getParameter("emailNo");
        logger.debug("value of emailNo : " + emailNo);

        String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String emailId = request.getParameter("emailId");
        logger.debug("value of emailId : " + emailId);

//        int emaildetailsNo = Integer.parseInt(emailNo);
        SgEmailticket updateStatus = new EmailManagement().getEmailDetailsById(sessionId, emailNo);

        updateStatus.setReadStatusByOperator(GlobalStatus.UPDATED);
        int res = new EmailManagement().updateViewStatus(sessionId, updateStatus);
        if (res == 0) {
            result = "success";
            logger.debug("Response of #ViewEmail from #PPAdmin Servlet's Parameter  result is " + result);
            message = "Email View Succefully !";
            logger.debug("Response of #ViewEmail from #PPAdmin Servlet's Parameter  message is " + message);

        }
        try {
            json.put("_result", result);
            logger.debug("Response of #ViewEmail from #PPAdmin Servlet's Parameter result is " + result);
            json.put("_message", message);
            logger.debug("Response of #ViewEmail from #PPAdmin Servlet's Parameter message is " + message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
