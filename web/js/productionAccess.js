/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function bootboxmodelForProduction(content) {
    var popup = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' +
            '<button aria-hidden="true" data-dismiss="modal" class="close" type="button btn-xs">X</button><font style="color: blue">' +
            content +
            '</font><a class="btn btn-info btn-xs" data-dismiss="modal" class="close" style="align:right"> OK </a></div></div></div></div>');
    popup.modal();
}
function Alert4Users(msg) {
    bootboxmodelForProduction("<h4>" + msg + "</h4>");
}

function changeProductionAccessStatus(productionAccessId, status) {
    if (status === 7) {
        bootbox.confirm({
            message: "<h4><span><font color=red>Are You Sure?</font></span></h4>",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success btn-xs'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger btn-xs'
                }
            },
            callback: function (result) {
                if (result === true) {
                    changePAS(productionAccessId, status);
                }
            }});
    } else {
        changePAS(productionAccessId,status);
    }
}

function changePAS(productionAccessId,status) {    
    var s = './ChangeStatusForProductionAccess?status=' + status + '&productionAccessId=' + productionAccessId;
    pleasewait();
    $.ajax({
        type: 'POST',
        url: s,
        datatype: 'json',        
        success: function (data) {
            if (data.result === 'error') {
                waiting.modal('hide');
                showAlert(data.message,'error',4000);
                //Alert4Users("<span><font color=red>" + data.message + "</font></span>");
            } else {
                waiting.modal('hide');
                showAlert(data.message,'success',4000);
                //Alert4Users("<span><font color=blue>" + data.message + "</font></span>");
                setTimeout(function () {
                    location.reload();
                }, 3000);
            }
        }
    });
}

function showAlert(message, type, closeDelay){
   if ($("#alerts-container").length == 0) {
        // alerts-container does not exist, create it
        $("body")
            .append( $('<div id="alerts-container" style="position: fixed;'+
                'width: 50%; left: 25%; top: 10%;">') );
        }
        // default to alert-info; other options include success, warning, danger
        type = type || "info";          
        // create the alert div
        message = '<i class="fa fa-info-circle"></i> '+message;
    var alert = $('<div class="alert alert-' + type + ' fade in">')
        .append(
            $('<button type="button" class="close" data-dismiss="alert">')
            .append("&times;")
        )
        .append(message);        
    // add the alert div to top of alerts-container, use append() to add to bottom
    $("#alerts-container").prepend(alert);    
    // if closeDelay was passed - set a timeout to close the alert
    if (closeDelay)
        window.setTimeout(function() { alert.alert("close") }, closeDelay);     
}

var waiting;
function pleasewait() {
    waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');
    waiting.modal();
}