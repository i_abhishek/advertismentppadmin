<%@page import="com.mollatech.serviceguard.nucleus.commons.MethodName"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Methods"%>
<%@page import="org.bouncycastle.util.encoders.Base64"%>
<%@page import="org.bouncycastle.util.encoders.Base64"%>
<%@page import="java.util.Map"%>
<%@include file="header.jsp" %>
<%    String _resId = request.getParameter("_resId");
    int resId = Integer.parseInt(_resId);
    String value = request.getParameter("value");
    String ver = request.getParameter("ver");
    Map methodMap = (Map) session.getAttribute("emethods");
    Methods methods = (Methods) methodMap.get(value);
    String mName = request.getParameter("methodName");
    MethodName methodName = null;
    String id = session.getAttribute("_apId").toString();
    for (int i = 0; i < methods.methodClassName.methodNames.size(); i++) {
        if (methods.methodClassName.methodNames.get(i).methodname.equals(mName)) {
            methodName = methods.methodClassName.methodNames.get(i);
            break;
        }
    }
%>   
<script src = "./js/acceptPoint.js" ></script>
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header text-uppercase">Services Description Details</h3>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47; <i class="fa fa-cube"></i><a href="services.jsp"> Services</a>
                        &#47; <strong>"<%=mName%>" API Description</strong>
                    </div>
                    <div class="panel-body">
                        <form role="form" id="saveEditMethodAPIDescform" name="saveEditMethodAPIDescform">
                            <table class="table table-striped">
                                <tr align=?left?>
                                    <th style="width: 10%">API Name</th>
                                    <th style="width: 10%"><%=mName%></th>

                                    <td><textarea readonly class="form-control" rows="3" id="description<%=mName%>" name="description<%=mName%>" style="width: 75%"><%if (methodName.description != null) {%><%=new String(Base64.decode(methodName.description))%><%} else {%>No Description<%}%></textarea>
                                    </td>
                                </tr>
                                <tr align=?left?>
                                    <th style="width: 10%">Return Type</th>
                                    <th style="width: 10%"><%=methodName.returntype.replace("<", "&lt;").replace(">", "&gt;")%></th>
                                    <td><textarea readonly class="form-control" rows="3"  id="description<%=methodName.returntype%>" name="description<%=methodName.returntype%>" style="width: 75%"><%if (methodName.rDescription != null) {%><%=new String(Base64.decode(methodName.rDescription))%><%} else {%>No Description<%}%></textarea></td>
                                </tr>
                                <%if (methodName.paramses != null) {
                                        for (int j = 0; j < methodName.paramses.size(); j++) {
                                %>
                                <tr align=?left?>
                                    <th style="width: 10%">Input Parameter</th>
                                    <th style="width: 10%"><%=methodName.paramses.get(j).type.replace("<", "&lt;").replace(">", "&gt;")%> <%=methodName.paramses.get(j).name.split(";")[0] %></th>
                                    <td><textarea readonly class="form-control" rows="3" id="description<%=methodName.paramses.get(j).name%>" name="description<%=methodName.paramses.get(j).name%>" style="width: 75%"><%if (methodName.paramses.get(j).pDescription != null) {
                                            %><%=new String(Base64.decode(methodName.paramses.get(j).pDescription))%><%} else {%>No Description<%}%></textarea></td>
                                </tr>
                                <%}
                            }%>
                            </table>
                            <div class="control-group">
                                <label class="control-label"  for="username"></label>
                                <div class="controls">
                                    &nbsp;&nbsp;&nbsp;<a href="./transformDetails.jsp?_apid=<%=id%>&_resId=<%=resId%>&ver=<%=ver%>" class="btn btn-warning btn-sm" type="button"><i class="fa fa-hand-o-left"></i> Back </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <script src="bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript 
    <script src="bower_components/raphael/raphael-min.js"></script>
    <script src="bower_components/morrisjs/morris.min.js"></script>
    <script src="js/morris-data.js"></script>-->

    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>
    <!-- DataTables JavaScript -->
    <script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>
    <script src="js/bootbox.min.js" type="text/javascript"></script>
</body>
</html>
<%@include file="footer.jsp" %> 