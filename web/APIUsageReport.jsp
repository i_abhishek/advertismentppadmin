<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Warfiles"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.GroupManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.GroupDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgBucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@include file="header.jsp" %>
<link href="./css/select2.css" rel="stylesheet"/>
<script src="./js/select2.js"></script>
<script src="./js/json3.min.js"></script>
<link href="./css/bootstrap-timepicker.css" rel="stylesheet"/>
<link rel="stylesheet" href="./css/datepicker.css">
<script src="./js/operatorsTextReports.js"></script>
<script src="./js/modal.js"></script>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="./js/apiUsage.js"></script>
<div id="page-wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header text-uppercase">API Usage Report</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-home"></i><a href="home.jsp"> Home</a> &#47;               
                    <i class="fa fa-shield fa-fw"></i> API Usage Report
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" id="apiReporForm" role="form" method="POST">
                        <div class="form-group">                    
                            <label class="col-lg-2 control-label" style="">Access Point</label>                                  
                            <div class="col-lg-2">
                                <select id="_Accesspoint2" name="_Accesspoint2"  class="form-control span2" onchange="acesspointChangeResource(this.value, '', '')" style="width: 100%">
                                    <option value="-1" selected>Select AP</option>
                                    <%
                                        Accesspoint[] accesspoints2 = null;
                                        PartnerDetails[] pds = new PartnerManagement().listpartners();
                                        accesspoints2 = new AccessPointManagement().getAllAccessPoint(SessionId, channelId);
                                        if (accesspoints2 != null) {
                                            for (int i = 0; i < accesspoints2.length; i++) {
                                                Warfiles warfiles = new WarFileManagement().getWarFile(SessionId, channelId, accesspoints2[i].getApId());
                                                if (warfiles != null && accesspoints2[i].getStatus() != GlobalStatus.DELETED) {
                                    %>
                                    <option value="<%=accesspoints2[i].getName()%>"><%=accesspoints2[i].getName()%></option>
                                    <%}
                                        }
                                    } else {%>
                                    <option value="-1">No Accesspoint assign</option>
                                    <%}%>
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <select id="_ResourceForSlabPricing2" name="_ResourceForSlabPricing2"  class="form-control span2" onchange="showVersion(this.value, '')" style="width: 100%">
                                    <option value="-1" selected>Select Resource</option>                                                                                  
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <select id="_VersionForSlabPricing2" name="_VersionForSlabPricing2"  class="form-control span2 " onchange="showAPI(this.value)" style="width: 100%">
                                    <option value="-1" selected>Select Version</option>                                                                                  
                                </select>
                            </div>
                            <div class="col-lg-2">
                                <select id="_APIForSlabPricing2" name="_APIForSlabPricing2"  class="form-control span2" style="width: 100%">
                                    <option value="-1" selected>Select API</option>                                                                                  
                                </select>
                            </div> 
                            <div class="col-lg-2">
                                <select id="partnerS" name="partnerS"  class="form-control span2" style="width: 100%">
                                    <option value="-1" selected>Select Partner</option>    

                                    <%if (pds != null) {
                                            for (int i = 0; i < pds.length; i++) {%>
                                    <option value="<%=pds[i].getPartnerId()%>"><%=pds[i].getPartnerName()%></option>                                                                                  
                                    <% }
                                        }%>
                                </select>
                            </div> 
                            <br><br>
                            <label class="col-lg-2 control-label" style="">From: Till</label> 
                            <div class="col-lg-2">
                                <div id="datetimepicker1" class="date">
                                    <input id="_startdate" class="datepicker" name="_startdate" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth" style="width: 100%" />
                                    <span class="add-on" hidden>
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" ></i>
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div id="datetimepicker2" class="date">
                                    <input id="_enddate" class="datepicker" name="_enddate" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth" style="width: 100%" />
                                    <span class="add-on" hidden>
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" ></i>
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <button class="btn btn-success" type="button" onclick="generateReport()">Generate Report</button>
                            </div>
                        </div>
                    </form>
                    <div id="apiGraphicalReport"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="dist/js/moment.min.js" type="text/javascript"></script> 

<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>
<script src="js/bootbox.min.js" type="text/javascript"></script>
<script src="./js/bootstrap-timepicker.js"></script>
<script src="./js/bootstrap-datepicker.js"></script>
<script>
    $(function () {
        $('#datetimepicker1').datepicker({
            language: 'pt-BR'
        });
    });
    $('#datetimepicker1').on('changeDate', function (ev) {
        $(this).datepicker('hide');
    });
    $(function () {
        $('#datetimepicker2').datepicker({
            language: 'pt-BR'
        });
    });
    $('#datetimepicker2').on('changeDate', function (ev) {
        $(this).datepicker('hide');
    });
    function acesspointChangeResource(value, resName, version) {
        var s = './GetResources?_apName=' + value;
        $.ajax({
            type: 'GET',
            url: s,
            success: function (data) {
                $('#_ResourceForSlabPricing2').html('');
                jQuery.each(data, function (i, val) {
                    $("#_ResourceForSlabPricing2").append('<option value=' + val + '>' + val + '</option>');
                });

                var res = document.getElementById('_ResourceForSlabPricing2').value;
                var ap = document.getElementById('_Accesspoint2').value;
                if (res === '-1' && resName === '') {
                } else if (resName === '' && version === '') {
                    showVersion(res, '');
                } else {
                    document.getElementById('_ResourceForSlabPricing2').value = resName;
                    showVersion(resName, version);
                }
            }
        });
    }

    function showVersion(resName, version) {
        var ap = document.getElementById('_Accesspoint2').value;
        var s = './GetVersion?_resourceId=' + resName + '&_apId=' + ap;
        $.ajax({
            type: 'GET',
            url: s,
            success: function (data) {
                $('#_VersionForSlabPricing2').html('');
                jQuery.each(data, function (i, val) {
                    $("#_VersionForSlabPricing2").append('<option value=' + val + '>' + val + '</option>');
                });
                var versionData = document.getElementById('_VersionForSlabPricing2').value;
                if (versionData === '-1' && version === '') {
                } else if (version === '') {
                    assignToken(versionData);
                } else {
                    document.getElementById('_VersionForSlabPricing2').value = version;
                    assignToken(version);
                }
            }
        });
    }

    function assignToken() {
        var accesspointName = document.getElementById("_Accesspoint2").value;
        var resourceName = document.getElementById("_ResourceForSlabPricing2").value;
        var version = document.getElementById("_VersionForSlabPricing2").value;
        var s = './GetAPI?_apId=' + accesspointName + '&_resourceId=' + resourceName + '&version=' + version;
        $.ajax({
            type: 'GET',
            url: s,
            success: function (data) {
                $('#_APIForSlabPricing2').html('');
                jQuery.each(data, function (i, val) {
                    $("#_APIForSlabPricing2").append('<option value=' + val + '>' + val + '</option>');
                });
            }
        });
    }
    <%
        String apName = request.getParameter("apName");
        String resName = request.getParameter("resName");
        String version = request.getParameter("version");
        if (resName != null && apName != null && version != null) {%>
    document.getElementById("_Accesspoint2").value = "<%=apName%>";
    acesspointChangeResource(document.getElementById("_Accesspoint2").value, '<%=resName%>', '<%=version%>');
    <%}

    %>
</script>                     
<script src="./js/json_sans_eval.js"></script>
<script src="./js/raphael-min.js"></script>
<script src="./js/morris.js"></script>
<script src="./js/morris.min.js"></script>
<link rel="stylesheet" href="./css/morris.css">

<%@include file="footer.jsp" %>  
