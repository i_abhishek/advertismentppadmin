package com.mollatech.PPadmin.billing;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.commons.UtilityFunctions;
import com.mollatech.serviceguard.nucleus.db.SgBucketdetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

@WebServlet(name = "CreatePackage", urlPatterns = { "/CreatePackage" })
public class CreatePackage extends HttpServlet {

    final String itemtype = "Billing Manager(CreatePackage)";

    static final Logger logger = Logger.getLogger(CreatePackage.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Request servlet is #CreatePackage from #PPAdmin at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Package have been created successfully";
        String channelId = (String) request.getSession().getAttribute("_channelId");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String packageName = request.getParameter("_packageName");
        String packagePrice = request.getParameter("_packagePrice");
        String packageDuration = request.getParameter("_packageDuration");
        String paymentMode = request.getParameter("_paymentMode");
        String packageDesc = request.getParameter("packageDesc");
        String _recurrienceBillingID = request.getParameter("_recurrienceBillingID");
        String PartnerVisibility[] = request.getParameterValues("visibleTo");
        if (PartnerVisibility != null) {
            logger.debug("Value of PartnerVisibility = " + Arrays.toString(PartnerVisibility));
        }
        logger.debug("Value of _packageName = " + packageName);
        logger.debug("Value of _packagePrice = " + packagePrice);
        logger.debug("Value of _packageDuration = " + packageDuration);
        logger.debug("Value of _paymentMode = " + paymentMode);
        logger.debug("Value of packageDesc = " + packageDesc);
        logger.debug("Value of _recurrienceBillingID = " + _recurrienceBillingID);
        System.out.println("package " + packageName);
        System.out.println("price " + packagePrice);
        System.out.println("duration " + packageDuration);
        System.out.println("mode " + paymentMode);
        if (UtilityFunctions.isValidPhoneNumber(packagePrice) == false) {
            result = "error";
            message = "Price should be in digit.";
            try {
                json.put("result", result);
                logger.debug("Response of #CreatePackage from #PPAdmin Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of #CreatePackage from #PPAdmin Servlet's Parameter  message is " + message);
            } catch (Exception ex) {
                logger.error("Exception at #CreatePackage from #PPAdmin ", ex);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        }
        SgBucketdetails packageObject = new PackageManagement().getPackageByName(SessionId, channelId, packageName);
        if (packageObject != null) {
            result = "error";
            message = "Package name already taken, Try with other name.";
            try {
                json.put("result", result);
                logger.debug("Response of #CreatePackage from #PPAdmin Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of #CreatePackage from #PPAdmin Servlet's Parameter  message is " + message);
            } catch (Exception ex) {
                logger.error("Exception at #CreatePackage from #PPAdmin ", ex);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        }
        String partnerId = "";
        if (PartnerVisibility != null) {
            for (int i = 0; i < PartnerVisibility.length; i++) {
                partnerId += PartnerVisibility[i] + ",";
            }
        }
        int price = Integer.parseInt(packagePrice);
        SgBucketdetails bucketObj = new SgBucketdetails();
        bucketObj.setBucketName(packageName.trim());
        bucketObj.setPlanAmount(price);
        bucketObj.setPaymentMode(paymentMode);
        bucketObj.setBucketDuration(packageDuration);
        bucketObj.setStatus(GlobalStatus.SUSPEND);
        bucketObj.setChannelId(channelId);
        bucketObj.setMinimumBalance(0);
        bucketObj.setFreeCredits(0);
        bucketObj.setTabShowFlag(GlobalStatus.RATE);
        bucketObj.setCreationDate(new Date());
        bucketObj.setServiceCharge("0");
        bucketObj.setChangePackageRate(0);
        bucketObj.setReActivationCharge(0.0);
        bucketObj.setCancellationRate(0);
        bucketObj.setPartnerVisibility(partnerId);
        bucketObj.setPackageDescription(packageDesc);
        bucketObj.setRecurrenceBillingPlanId(_recurrienceBillingID);
        int retValue = new PackageManagement().CreatePackage(SessionId, channelId, bucketObj);
        try {
            if (retValue > 0) {
                json.put("result", result);
                json.put("message", message);
                json.put("retValue", retValue);
            } else {
                result = "error";
                message = "Package does not created.";
                json.put("result", result);
                logger.debug("Response of #CreatePackage from #PPAdmin Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of #CreatePackage from #PPAdmin Servlet's Parameter  message is " + message);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            logger.info("Response of #CreatePackage from #PPAdmin " + json.toString());
            logger.info("Response of #CreatePackage from #PPAdmin Servlet at " + new Date());
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
