<%@page import="com.mollatech.serviceguard.nucleus.commons.MethodName"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Classes"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Methods"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <div class="control-group">
        <label class="control-label"  for="username"></label>
        <div class="controls">
            <select id="_ResourceForTieringPricing21" name="_ResourceForTieringPricing21"  class="form-control span2" onchange="showTieringVersion(this.value)" style="width: 100%">
                <%
                    AccessPointManagement ppw = new AccessPointManagement();
                    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
                    String channelId = (String) request.getSession().getAttribute("_channelId");
                    String apName = request.getParameter("_apName");
//                    if(apId != null){
//                    int acpId = Integer.parseInt(apId);
//                    }
                    if (!apName.equals("-1")) {
                        //Accesspoint accesspoint = ppw.getAccessPointById(SessionId, channelId, acpId);
                        Accesspoint accesspoint = ppw.getAccessPointByName(SessionId, channelId, apName);
                        String resources = accesspoint.getResources();
                        if (resources != null) {
                            String[] resourcesDetails = resources.split(",");
                            if (resourcesDetails != null) {
                                for (int i = 0; i < resourcesDetails.length; i++) {
                                    int resourceId = Integer.parseInt(resourcesDetails[i]);
                                    ResourceDetails rs = new ResourceManagement().getResourceById(resourceId);
                %>
                <option value="<%=rs.getName()%>"><%=rs.getName()%></option>               
                <%
                            }
                        }
                    }
                } else {%>
                <option value="-1">Select Resource</option>  
                <% }%>
            </select>           
        </div>
    </div>
</html>
