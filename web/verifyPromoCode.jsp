<%@page import="java.util.Enumeration"%>
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.UtilityFunctions"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@include file="header.jsp" %>

<link href="select2/select2.css" rel="stylesheet" type="text/css"/>
<script src="js/promocode.js" type="text/javascript"></script>
<%
    String promocodeRejectionOptions = "<option value='Select' selected>Select Reason for Rejection </option>\n";
//String value = ;

    Enumeration enuKeys = LoadSettings.g_promocodeRejectionSettings.keys();
    while (enuKeys.hasMoreElements()) {
        String key = (String) enuKeys.nextElement();
        String value = LoadSettings.g_promocodeRejectionSettings.getProperty(key);
        // for (int i = 0; i < key.length(); i++) {
        promocodeRejectionOptions += "<option value='" + value + "'>" + value + "</option>\n";

    }
    
    String promocodeId = request.getParameter("promocode");
    int ipromocodeId = 0;
    if (promocodeId != null) {
        ipromocodeId = Integer.parseInt(promocodeId);
    }
    SgPromocode promoObj = new PromocodeManagement().getPromocodeById(SessionId, channelId, ipromocodeId);
    String expDate = "NA";
    if (promoObj != null) {
        expDate = UtilityFunctions.getTMReqDate(promoObj.getExpireOn());
    }
    String partnerIds = promoObj.getPartnerId();
    String options = "<option value=all selected>NA</option>\n";
    if (partnerIds != null && !partnerIds.isEmpty() && partnerIds.contains("all")) {
        options = "<option value=all selected>All</option>\n";
    } else if (partnerIds != null && !partnerIds.isEmpty()) {
        String[] parArray = partnerIds.split(",");
        options = "";
        for (int i = 0; i < parArray.length; i++) {
            int parId = Integer.parseInt(parArray[i]);
            String emailId = "";
            PartnerDetails parObj = new PartnerManagement().getPartnerDetails(parId);
            if (parObj != null) {
                emailId = parObj.getPartnerEmailid();
            }
            options += "<option selected value='" + emailId + "'>" + emailId + "</option>\n";
        }
    }
    String discountType = promoObj.getDiscountType();
%>
<div id="page-wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Verify Promo code</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-home"></i><a href="home.jsp"> Home</a> &#47;                    
                    <i class="fa fa-edit"></i> Promo code
                </div>
                <div class="panel-body">
                    <h4>Promo code details</h4>
                    <hr>                   
                    <form class="form-horizontal" id="promoCode_form" name="promoCode_form" role="form" method="POST">
                        <div class="form-group">
                            <label class="control-label col-lg-2">Code</label>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-cogs"></i></span>
                                    <input type="text" id="_promoCode" name="_promoCode" class="form-control" placeholder="Code" value="<%=promoObj.getCode()%>" readonly>
                                </div>
                                <div id="checkAvailability-result">
                                </div>
                            </div>
                            <label class="control-label col-lg-2">Expiry</label>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon "><i class="fa fa-calendar"></i></span>
                                    <input type="text" id="_promoExpiry" name="_promoExpiry" class="form-control" placeholder="Expire after" value="<%=promoObj.getExpiryInNoOfDays()%>" readonly>
                                </div>
                            </div>                            
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2">Usage Count</label>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                                    <input type="text" id="_usageCount" name="_usageCount" class="form-control" placeholder="Usage Count" value="<%=promoObj.getUsageCount()%>" readonly>                                    
                                </div>
                            </div>
                            <label class="control-label col-lg-2">Partner Usage Count</label>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                                    <input type="text" id="_partnerUsageCount" name="_partnerUsageCount" class="form-control" placeholder="Developer Usage Count" value="<%=promoObj.getPartnerUsageCount()%>" readonly>                                    
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2">Discount</label>
                            <div class="col-lg-3">
                                <select id="_promoCodeDiscountType_edit" name="_promoCodeDiscountType_edit" class="form-control" onchange="discountChangeType(this.value)" disabled>
                                    <%if (discountType.equalsIgnoreCase("flat")) {%>
                                    <option selected value="Flat">Flat</option>
                                    <option value="Custom">Custom</option>
                                    <%} else {%>
                                    <option  value="Flat">Flat</option>
                                    <option selected value="Custom">Custom</option>
                                    <%}%>
                                </select>
                            </div>
                            <%if (discountType.equalsIgnoreCase("flat")) {%>        
                            <label class="control-label col-lg-2" id="discountTypeDetail">In Price</label>
                            <%} else {%>
                            <label class="control-label col-lg-2" id="discountTypeDetail">In Percentage</label>
                            <%}%>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                    <input type="text" id="_promoDiscount_edit" name="_promoDiscount_edit" class="form-control" placeholder="Discount" value="<%=promoObj.getDiscount()%>" readonly>                                    
                                </div>
                            </div>
                        </div>        
                        <div class="form-group">
                            <label class="control-label col-lg-2">For Developer</label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-users"></i></span>
                                    <select id ="visibleTo" name="visibleTo" multiple class="span6" disabled style="width: 100%">
                                        <%=options%>
                                    </select>                                  
                                </div>
                            </div>
                        </div>
                        <a  class="btn btn-success btn-xs" onclick="approvePromoRequest('<%=promoObj.getPromoCodeId()%>', '<%=GlobalStatus.ACTIVE%>', '<%=GlobalStatus.SUSPEND%>')" style="margin-left: 17%"><i class="fa fa-check-circle"></i> Approve</a>
                        <a  class="btn btn-danger btn-xs" onclick="rejectPromoCodeRequest('<%=promoObj.getPromoCodeId()%>', '<%=GlobalStatus.CANCLE_BYCHECKER%>')" style="margin-left: 1%"><i class="fa fa-close"></i> Reject</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="rejectPromocode" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="rejectPackageModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <b id="rejectPackageModal">Reject Promo Code</b>
            </div>          
            <div class="modal-body">
                <div class="row-fluid">
                    <form class="form-horizontal" id="rejectPromoForm">
                        <fieldset>
                            <div class="control-group">
                                <label class="control-label col-lg-2"  for="partnername">Reason</label>
                                <div class="controls col-lg-10">
                                    <input type="hidden"  id="_promoId" name="_promoId">
                                    <input type="hidden"  id="_promostatus" name="_promostatus" >
                                    <select id="reason" name="reason" class="form-control" >
                                        <%=promocodeRejectionOptions%>        
                                    </select>
                                </div>
                            </div>                            
                        </fieldset>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div id="edit-partner-result"></div>
                <button class="btn btn-info btn-xs" data-dismiss="modal">Close</button>
                <button class="btn btn-success btn-xs" onclick="rejectPromoRequest()" id="addPartnerButtonE">Reject request</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $("#visibleTo").select2();
    });

</script>
<!--<script src="js/jquery-ui-1.10.4.min.js" type="text/javascript"></script>-->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->
<!--<script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>-->
<!--<script src="js/bootbox.min.js" type="text/javascript"></script>  -->
<script src="select2/select2.js" type="text/javascript"></script>
<script src="js/bootbox.min(1).js" type="text/javascript"></script>
</body>
</html>                               