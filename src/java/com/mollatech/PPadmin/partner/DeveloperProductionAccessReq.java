/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.partner;

import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.connector.communication.SGStatus;
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessEnvtManagement;
import com.mollatech.serviceguard.nucleus.settings.SendNotification;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "DeveloperProductionAccessReq", urlPatterns = {"/DeveloperProductionAccessReq"})
public class DeveloperProductionAccessReq extends HttpServlet {

    public static final int SEND = 0;
    static final Logger logger = Logger.getLogger(DeveloperProductionAccessReq.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Requested Servlet is DeveloperProductionAccessReq at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());

        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Developer production request approved successfully.";
        PrintWriter out = response.getWriter();
        String productionId = request.getParameter("productionAccessId");
        String status = request.getParameter("status");
        String developerId = request.getParameter("developerId");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        int ireqStatus = 0;
        int iproductionId = 0;
        int retValue = -1;
        int ipartnerId = 0;
        if (status != null) {
            ireqStatus = Integer.parseInt(status);
        }
        if (developerId != null) {
            ipartnerId = Integer.parseInt(developerId);
        }
        if (productionId != null) {
            iproductionId = Integer.parseInt(productionId);
        }
        logger.debug("value of productionId : " + productionId);
        logger.debug("value of status       : " + status);
        PartnerDetails partnerDetails = new PartnerManagement().getPartnerDetails(ipartnerId);

        retValue = new ProductionAccessEnvtManagement().ChangeRequestStatus(SessionId, iproductionId, ireqStatus,null);
        if (retValue == 3) {
            result = "error";
            message = "Developer production request already approved.";
        } else if (retValue == 0) {
            String tmessage = LoadSettings.g_templateSettings.getProperty("email.partner.productionRequestApprove");
            if (tmessage != null) {
                tmessage = tmessage.replaceAll("#Name#", partnerDetails.getPartnerName());
                tmessage = tmessage.replaceAll("#channel#", "Service Guard");
                tmessage = tmessage.replaceAll("#datetime#", new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
            }
            int productType = 3;
            String Subject = "Production access request approved";
            SGStatus statusSG = new SendNotification().SendEmail(channelId, partnerDetails.getPartnerEmailid(), Subject, tmessage, null, null, null, null, productType);
            if (statusSG.iStatus != GlobalStatus.PENDING && statusSG.iStatus != SEND) {
                result = "error";
                message = partnerDetails.getPartnerName() + ", Developer request approved successfully but failed to sent email notification.";
                try {
                    json.put("_result", result);
                    logger.debug("Response of DeveloperProductionAccessReq Servlet's Parameter  result is " + result);
                    json.put("_message", message);
                    logger.debug("Response of DeveloperProductionAccessReq Servlet's Parameter  message is " + message);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                out.print(json);
                out.flush();
                return;
            }
            result = "success";
            message = "Developer production request approved successfully.";
        }else{
            result = "error";
            message = "Developer production request did not approved ";
        }
        try {
            json.put("_result", result);
            logger.debug("Response of DeveloperProductionAccessReq Servlet's Parameter  result is " + result);
            json.put("_message", message);
            logger.debug("Response of DeveloperProductionAccessReq Servlet's Parameter  message is " + message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
