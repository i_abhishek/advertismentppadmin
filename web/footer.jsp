<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>
<script src="js/bootbox.min.js" type="text/javascript"></script>
<%
    Date dFooter = new Date();
    SimpleDateFormat sdfFooter = new SimpleDateFormat("yyyy");
    String strYYYY = sdfFooter.format(dFooter);

    long LongTime = dFooter.getTime() / 1000;
    SimpleDateFormat tz = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
    String completeTimewithLocalTZ = tz.format(dFooter);


%>
<hr>
<script>
    setInterval(function () {
        checkValidSession()
    }, 180000);

    function checkValidSession() {
        var s = './validateSession';
        $.ajax({
            type: 'GET',
            url: s,
            dataType: 'json',
            success: function (data) {
                if (data._result === "error") {
                    window.location = "./oplogout.jsp";
                }
            }
        });
    }

//    $.fn.dataTable.tables({visible: true, api: true}).columns.adjust();
    $(document).ready(function () {
        $('table.display').DataTable({
            responsive: {
                details: {
                    type: 'column',
                    target: 'tr'
                }
            },
            columnDefs: [{
                    className: 'control',
                    orderable: false,
                    targets: 0
                }],
            order: [1, 'asc'],
            "language": {
                "emptyTable": "No Data Available."
            }
        });
    });
</script>
<div class="row">
    <footer>
        <div align="center" style="margin-top: 5%">
            <p>&copy; Molla Technologies 2009-<%=strYYYY%> <a href="http://www.mollatech.com" target="_blank"> (www.mollatech.com)</a></p>
            <p>Local Date and Time::<%=completeTimewithLocalTZ%> (<%=LongTime%>)</p>                
        </div>
    </footer>
</div>