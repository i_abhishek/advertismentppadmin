<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ApprovedAdPackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgApprovedAdPackagedetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Warfiles"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MakerChaker"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@include file="header.jsp" %>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Dashboard</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-3 col-md-6">
            <%  int totalpartner = 0;
                int rlength = 0;
                int sglength = 0;
                
            %>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <%
                            Accesspoint[] ap = new AccessPointManagement().getAllAccessPoint(SessionId, channelId);
                            if (ap != null) {
                                for (int i = 0; i < ap.length; i++) {
                                    Warfiles warfiles = new WarFileManagement().getWarFile(channelId, ap[i].getApId());
                                    if (warfiles != null) {
                                        rlength++;
                                    }
                                }
                            }
                        %>
                        <div class="col-xs-3">
                            <i class="fa fa-comments fa-4x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><%= rlength%></div>
                            <div>Transform Services</div>
                        </div>
                    </div>
                </div>
                <a href="services.jsp">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <%
            PartnerDetails[] ps = new PartnerManagement().getAllPartnerDetails(SessionId, channelId);
            if (ps != null) {
                totalpartner = ps.length;
            }
        %>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-tasks fa-4x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><%= totalpartner%></div>
                            <div>Total Developers</div>
                        </div>
                    </div>
                </div>
                <a href="partners.jsp">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-users fa-4x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><%=partnerRequest%></div>
                            <div>Developer Request</div>
                        </div>
                    </div>
                </div>
                <a href="partnerRequest.jsp">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <%
            Object setting1 = new SettingsManagement().getSetting(channelId, SettingsManagement.MAKERCHAKER, SettingsManagement.PREFERENCE_ONE);
            MakerChaker makerChakerObj1 = (MakerChaker) setting1;
            if (makerChakerObj1 != null) {
                if (makerChakerObj1.chaker.contains(opObj.getEmailid()) && makerChakerObj.status == GlobalStatus.ACTIVE) {
                    int newPackageRequestCount = 0;
                    SgReqbucketdetails[] sgRequestedPackage = null;
                    sgRequestedPackage = new RequestPackageManagement().listPackageRequestsbystatus(SessionId, 1, 2);
                    if (sgRequestedPackage != null) {
                        newPackageRequestCount = sgRequestedPackage.length;
                    }
                    
                    int sgPromocodeRequestLength = 0;
                    SgPromocode[] sgPromocodeRequest = null;
                    sgPromocodeRequest = new PromocodeManagement().listPromocodeByReqFlag(SessionId, channelId, GlobalStatus.SUSPEND, GlobalStatus.SENDTO_CHECKER);
                    if (sgPromocodeRequest != null) {
                        sgPromocodeRequestLength = sgPromocodeRequest.length;
                    }
                    
        %>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-shopping-cart fa-4x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><%=newPackageRequestCount%></div>
                            <div>Package Request</div>
                        </div>
                    </div>
                </div>
                <a href="packageRequest.jsp">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <%if(promocodeFunctionality != null && !promocodeFunctionality.isEmpty() && promocodeFunctionality.equalsIgnoreCase("true")){ %>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-users fa-4x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><%=sgPromocodeRequestLength%></div>
                            <div>Promocode Request</div>
                        </div>
                    </div>
                </div>
                <a href="promocodeRequest.jsp">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>                    
        <%}
                }
            }
        %>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-tasks fa-4x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><%= partnerProductionRequest%></div>
                            <div>Production Request</div>
                        </div>
                    </div>
                </div>
                <a href="developerProductionAccessRequest.jsp">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-tasks fa-4x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><%= resourceOwnerRequest%></div>
                            <div>Res.owner Request</div>
                        </div>
                    </div>
                </div>
                <a href="resourceOwnerManager.jsp">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div> 
        <%
        
            if (makerChakerObj1 != null) {
                if (makerChakerObj1.chaker.contains(opObj.getEmailid()) && makerChakerObj.status == GlobalStatus.ACTIVE) {
                    int newPackageRequestCount = 0;
                    SgApprovedAdPackagedetails[] sgRequestedPackage = null;
                    sgRequestedPackage = new ApprovedAdPackageManagement().listPackageRequestsbystatus(SessionId, 1, 2);
                    if (sgRequestedPackage != null) {
                        newPackageRequestCount = sgRequestedPackage.length;
                    }
        %>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-shopping-cart fa-4x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><%= newPackageRequestCount%></div>
                            <div>Ad Package Request</div>
                        </div>
                    </div>
                </div>
                <a href="adPackageRequest.jsp">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div> 
        <% }
                }
            %>                
                            
    </div>
    <!-- /.row -->
</div>
<!-- /#page-wrapper -->
<!-- /#wrapper -->

<!-- jQuery -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript 
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.min.js"></script>
<script src="js/morris-data.js"></script>-->

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>

</body>

</html>
<%@include file="footer.jsp" %>
