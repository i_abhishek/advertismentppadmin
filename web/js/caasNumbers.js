function updateCaasNo() {
    var NumberForPartners = document.getElementById("NumberForPartners").value;
    var partnerName = document.getElementById("partnerde").value;
    var message = null;
    if (partnerName === '-1' || NumberForPartners === "") {
        message = "Please select number for partner";
        showAlert(message, "danger", 3000);
        return;
    }
    pleaseWaitProcess();
    var s = './CaasAssignNumbers';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#partnerInfoForm").serialize(),
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                waiting.modal('hide');
                showAlert(data._message, "danger", 3000);
                setTimeout(function () {
                    window.location.href = "./caasAssignNumbers.jsp";
                }, 3000);
            } else {
                waiting.modal('hide');
                showAlert(data._message, "success", 3000);
                setTimeout(function () {
                    window.location.href = "./caasAssignNumbers.jsp";
                }, 3000);
            }
        }
    });
}

function strCompare(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function showAlert(message, type, closeDelay) {
    if ($("#alerts-container").length == 0) {
        // alerts-container does not exist, create it
        $("body")
                .append($('<div id="alerts-container" style="position: fixed;' +
                        'width: 50%; left: 25%; top: 10%;">'));
    }
    // default to alert-info; other options include success, warning, danger
    type = type || "info";
    // create the alert div
    message = '<i class="fa fa-info-circle"></i> ' + message;
    var alert = $('<div class="alert alert-' + type + ' fade in">')
            .append(
                    $('<button type="button" class="close" data-dismiss="alert">')
                    .append("&times;")
                    )
            .append(message);
    // add the alert div to top of alerts-container, use append() to add to bottom
    $("#alerts-container").prepend(alert);
    // if closeDelay was passed - set a timeout to close the alert
    if (closeDelay)
        window.setTimeout(function () {
            alert.alert("close")
        }, closeDelay);
}

function uploadCaasFile() {
    var s = './UploadCassNumbers';
    pleaseWaitProcess();
    $.ajaxFileUpload({
        type: 'POST',
        fileElementId: 'ziptoupload',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                waiting.modal('hide');
                showAlert(data.message, "danger", 4000);
//                    document.getElementById("submitMail").disabled = false;
            } else if (strCompare(data.result, "success") === 0) {
                waiting.modal('hide');
                addCaasNumber();
            }
        }
    });
}

function addCaasNumber() {
    pleaseWaitProcess();
    var s = './AddCaasNumbers';
    $.ajax({
        type: 'POST',
        fileElementId: 'ziptoupload',
        url: s,
        dataType: 'json',
        data: $("#filechooser").serialize(),
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                waiting.modal('hide');
                showAlert(data._message, "danger", 3000);
            } else if (strCompare(data._result, "success") === 0) {
                waiting.modal('hide');
                showAlert(data._message, "success", 3000);
            }
        }
    });
}


function editCassNumber() {
    $('#editNumbers').modal('hide');
    var editedNo = document.getElementById("editNo").value;
    if (editedNo === "" || editedNo === null) {
        var message = 'Empty number is not allowed';
        showAlert(message, "danger", 3000);
        return;
    }
    pleasewaitV1();
    var no = document.getElementById("editNo").value;
    var noId = document.getElementById("numberId").value;
    var s = './EditCaasNumbers';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#editnumberForm").serialize(),
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                waitingV1.modal('hide');
                showAlert(data.message, "danger", 3000);
            } else {
                waitingV1.modal('hide');
                showAlert(data.message, "success", 3000);
                setTimeout(function () {
                    window.location.reload();
//                    window.location.href = "./editNumber.jsp";
                }, 5000);
            }
        }
    });
}

function deleteCaasNo(numberId) {
    bootbox.confirm({
        message: ("<h4><font color=red>" + "Are you sure ?" + "</font><h4>"),
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success btn-xs'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger btn-xs'
            }
        },
        callback: function (result) {
            //console.log('This was logged in the callback: ' + result);
            if (result === true) {
                pleasewaitV1();
                var s = './DeleteCaasNumber?numberId=' + numberId;
                $.ajax({
                    type: 'GET',
                    url: s,
                    dataType: 'json',
                    success: function (data) {
                        if (strCompare(data.result, "error") === 0) {
                            waitingV1.modal('hide');
                            showAlert(data.message, "danger", 3000);
                        } else {
                            waitingV1.modal('hide');
                            showAlert(data.message, "success", 3000);
                            setTimeout(function () {
                                window.location.reload();
//                    window.location.href = "./editNumber.jsp";
                            }, 5000);
                        }
                    }
                });
            }
        }
    });
}

function editNumberModel(number, numberId) {
    document.getElementById("editNumberModel").innerHTML = "Edit Number " + number;
    document.getElementById("editNo").value = number;
    document.getElementById("numberId").value = numberId;
    $('#editNumbers').modal();
}

function isNumericKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if ((charCode < 48 || charCode > 57)) {
        var showMessage = 'Enter Numeric Values Only';
        Alert4Users("<span><font color=red>" + showMessage + "</font></span>");

        return false;
    }
    return true;
}

var waitingV1;
function pleasewaitV1() {
    waitingV1 = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');
    waitingV1.modal('show');
}


function changeCaasStatus(status, id) {
    pleasewaitV1();
    var s = './ChangeStatusCaas?status=' + status + "&id=" + id;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                waitingV1.modal('hide');
                showAlert(data.message, "danger", 3000);
            } else {
                waitingV1.modal('hide');
                showAlert(data.message, "success", 3000);
                setTimeout(function () {
                    window.location.reload();
//                    window.location.href = "./editNumber.jsp";
                }, 5000);
            }
        }
    });
}