/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function generateReport() {
    document.getElementById('apiGraphicalReport').innerHTML='';
    var sdate = document.getElementById('_startdate').value;
    var edate = document.getElementById('_enddate').value;
    if (sdate === '' || edate === '') {
        Alert4Users("<h2><font color=red>" + "Please Select the Date ranges.</font>" + "<h2>");
        return;
    }
    var s = './GetGraphicalReport';
    $.ajax({
        type: 'GET',
        url: s,
        data: $("#apiReporForm").serialize(),
        success: function (data) {
            if (data.result === 'error') {
                Alert4Users("<h2><font color=red>" + data.message + "</font>" + "<h2>");
                return;
            } else {
//                console.log(data.message);
                var myJsonObj = (data.message);
                Morris.Line({
                    element: 'apiGraphicalReport',
                    data: myJsonObj,
                    xLabelAngle: 45,
                    padding: 50,
                    xLabelMargin: 5,
                    parseTime: false,
                    xkey: 'label',
                    xLabels: 'Date',
                    ykeys: ['value'],
                    labels: ['Count'],
                    lineWidth: 2,
                    pointSize: 1,
//                    postUnits: 'ms',
                    smooth: false
                });
            }
        }});
}

function Alert4Users(msg) {
    bootboxmodel("<h4>" + msg + "</h4>");
}