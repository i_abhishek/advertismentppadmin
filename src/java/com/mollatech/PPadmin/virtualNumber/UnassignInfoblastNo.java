/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.virtualNumber;

import com.mollatech.infoblastservice.SdpConnectRsp;
import com.mollatech.infoblastservice.SendUserCodeRsp;
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgNumbers;
import com.mollatech.serviceguard.nucleus.db.connector.management.NumbersManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "UnassignInfoblastNo", urlPatterns = {"/UnassignInfoblastNo"})
public class UnassignInfoblastNo extends HttpServlet {

    static final Logger logger = Logger.getLogger(UnassignInfoblastNo.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        logger.info("Requested Servlet is UnassignInfoblastNo at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        String result = "success";
        String message = "Virtual number unassign successfully!!!";
        JSONObject json = new JSONObject();
        int numberID = Integer.parseInt(request.getParameter("numberId"));
        String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        logger.debug("value of numberId : " + numberID);
        int res = -1;
        SgNumbers numberData = new NumbersManagement().getMobileNumberById(sessionId, numberID);
        PartnerDetails partner = new PartnerManagement().getPartnerDetails(sessionId, channelId, numberData.getPartnerId());
        if (numberData != null) {
            SdpConnectRsp connectRes = new InfoblastServiceUtil().getSDPConnect();
            if (connectRes == null) {
                result = "error";
                message = " IB sdpConnect service failed to connect !";
                json.put("result", result);
                json.put("message", message);
                logger.info("UnassignInfoblastNo result value " + result);
                logger.info("UnassignInfoblastNo message value " + message);
                System.out.println("result " + result);
                System.out.println("message " + message);
                out.print(json);
                out.flush();
                out.close();
                return;
            } else if (connectRes.getStatus() != 0) {
                result = "error";
                message = connectRes.getStatusDesc();
                json.put("result", result);
                json.put("message", message);
                logger.info("UnassignInfoblastNo result value " + result);
                logger.info("UnassignInfoblastNo message value " + message);
                System.out.println("result " + result);
                System.out.println("message " + message);
                out.print(json);
                out.flush();
                out.close();
                return;
            }

            String virtualNo = numberData.getMobileNo().replaceAll("[-+.^:,]", "");
            SendUserCodeRsp sendUserCodeRes = new InfoblastServiceUtil().sendUserCode(connectRes.getSessionId(), virtualNo, "T", null);
            if (sendUserCodeRes == null) {
                result = "error";
                message = " IB sdpUserCode service failed to connect !";
                json.put("result", result);
                json.put("message", message);
                logger.info("UnassignInfoblastNo result value " + result);
                logger.info("UnassignInfoblastNo message value " + message);
                System.out.println("result " + result);
                System.out.println("message " + message);
                out.print(json);
                out.flush();
                out.close();
                return;
            } else if (sendUserCodeRes.getStatus() != 0) {
                result = "error";
                message = sendUserCodeRes.getStatusDesc();
                json.put("result", result);
                json.put("message", message);
                logger.info("UnassignInfoblastNo result value " + result);
                logger.info("UnassignInfoblastNo message value " + message);
                System.out.println("result " + result);
                System.out.println("message " + message);
                out.print(json);
                out.flush();
                out.close();
                return;
            }
            String envtType = numberData.getType();
            numberData.setPartnerId(null);
            numberData.setChannelId(null);
            numberData.setInfoblastId(null);
            numberData.setInfoblastPassword(null);
            numberData.setInfoblastAccountNo(null);
            numberData.setStatus(GlobalStatus.ACTIVE);
            res = new NumbersManagement().updateDetails(numberData);
            if (partner != null) {
                if (res == 0 && envtType.equalsIgnoreCase("test")) {
                    String partnerVirtual = partner.getVirtualNumbers();
                    if (partnerVirtual != null) {
                        partnerVirtual = partnerVirtual.replaceAll("," + numberData.getNumberId() + ",", ",");
                        partner.setVirtualNumbers(partnerVirtual);
                        new PartnerManagement().updateDetails(partner);
                    }
                } else if (res == 0) {
                    String partnerVirtual = partner.getVirtualNumbersLive();
                    if (partnerVirtual != null) {
                        partnerVirtual = partnerVirtual.replaceAll("," + numberData.getNumberId() + ",", ",");
                        partner.setVirtualNumbersLive(partnerVirtual);
                        new PartnerManagement().updateDetails(partner);
                    }
                }
            }
        }
        if (res == 0) {
            result = "success";
            message = "Virtual number terminated successfully!!!";

            System.out.println("result " + result);
            System.out.println("message " + message);
        } else {
            result = "error";
            message = " Virtual number not able to terminate !!!";
            System.out.println("result " + result);
            System.out.println("message " + message);
        }
        try {
            json.put("result", result);
            json.put("message", message);
            logger.info("UnassignInfoblastNo result value " + result);
            logger.info("UnassignInfoblastNo message value " + message);
            System.out.println("result " + result);
            System.out.println("message " + message);
            return;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
            out.close();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
