package com.mollatech.PPadmin.billing;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.SgBucketdetails;
import com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "changeStatusPackage", urlPatterns = { "/changeStatusPackage" })
public class changeStatusPackage extends HttpServlet {


    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Package approved successfully";
        PrintWriter out = response.getWriter();
        String _packageName = request.getParameter("_packageName");
        String reqStatus = request.getParameter("_status");
        int ireqStatus = 0;
        if (reqStatus != null) {
            ireqStatus = Integer.parseInt(reqStatus);
        }
        int a = -1;
        RequestPackageManagement pm = new RequestPackageManagement();
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        if (SessionId != null) {
            SgReqbucketdetails sgReqPackage = pm.getReqPackageByName(SessionId, channelId, _packageName);
            if (sgReqPackage != null) {
                SgBucketdetails bucketdetails = new PackageManagement().getPackageByName(SessionId, channelId, _packageName);
                
                sgReqPackage.setApRateDetails(bucketdetails.getApRateDetails());
                sgReqPackage.setBucketDuration(bucketdetails.getBucketDuration());
                sgReqPackage.setCancellationRate(bucketdetails.getCancellationRate());
                sgReqPackage.setChangePackageRate(bucketdetails.getChangePackageRate());
                sgReqPackage.setDaysForFreeTrial(bucketdetails.getDaysForFreeTrial());
                sgReqPackage.setFeatureList(bucketdetails.getFeatureList());
                sgReqPackage.setFreeCredits(bucketdetails.getFreeCredits());
                sgReqPackage.setLatePenalitesRate(bucketdetails.getLatePenaltyRate());
                sgReqPackage.setMainCredits(bucketdetails.getMainCredits());
                sgReqPackage.setMinimumBalance(bucketdetails.getMinimumBalance());
                sgReqPackage.setReActivationCharge(bucketdetails.getReActivationCharge());
                sgReqPackage.setSecurityAndAlertDetails(bucketdetails.getSecurityAndAlertDetails());
                sgReqPackage.setServiceCharge(bucketdetails.getServiceCharge());
                sgReqPackage.setSlabApRateDetails(bucketdetails.getSlabApRateDetails());
                sgReqPackage.setTax(bucketdetails.getTax());
                sgReqPackage.setTierRateDetails(bucketdetails.getTierRateDetails());
                sgReqPackage.setPartnerVisibility(bucketdetails.getPartnerVisibility());
                sgReqPackage.setMakerflag(GlobalStatus.SUSPEND);
                sgReqPackage.setUpdationDate(new Date());
                sgReqPackage.setApiThrottling(bucketdetails.getApiThrottling());
                sgReqPackage.setFlatPrice(bucketdetails.getFlatPrice());
                pm.updateDetails(sgReqPackage);
                a = pm.ChangeRequestStatus(SessionId, channelId, sgReqPackage.getBucketName(), ireqStatus);
            }
            if (a == 0) {
                result = "success";
                message = "Package approved successfully";
            } else if (a == 3) {
                result = "error";
                if (ireqStatus == GlobalStatus.ACTIVE) {
                    message = "Package details already approved  ";
                } else {
                    message = "Package did not approved ";
                }
            } else {
                result = "error";
                message = "Package did not approved ";
            }
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
