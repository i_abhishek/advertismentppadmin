package com.mollatech.PPadmin.billing;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.commons.MakerChaker;
import com.mollatech.serviceguard.nucleus.db.Operators;
import com.mollatech.serviceguard.nucleus.db.SgBucketdetails;
import com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "EditPackage", urlPatterns = { "/EditPackage" })
public class EditPackage extends HttpServlet {

    static final Logger logger = Logger.getLogger(EditPackage.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Request servlet is #EditPackage from #PPAdmin at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Package rate have been updated successfully";
        Operators operators = (Operators) request.getSession().getAttribute("_apOprDetail");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String packageID = request.getParameter("packageId");
        logger.debug("Value of packageID  = " + packageID);
        int pId = 0;
        if (packageID != null) {
            pId = Integer.parseInt(packageID);
        }
        SgBucketdetails packObj = new PackageManagement().getPackageDetails(SessionId, channelId, pId);
        try {
            if (packObj == null) {
                result = "error";
                message = "Can't edit the package details.";
                json.put("result", result);
                json.put("message", message);
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String minimumBalance = request.getParameter("minimumBalance");
        String freeCredits = request.getParameter("freeCredits");
        String freeTrail = request.getParameter("freeTrail");
        String mainCredits = request.getParameter("mainCredits");
        String serviceChargeRate = request.getParameter("serviceChargeRate");
        String PartnerVisibility[] = request.getParameterValues("_partnerId");
        if (serviceChargeRate == null) {
            serviceChargeRate = "0";
        }
        String changePackageRate = request.getParameter("ChangePackageChargeRate");
        if (changePackageRate == null) {
            changePackageRate = "0";
        }
        String reactivationRate = request.getParameter("ReactivationChargeRate");
        if (reactivationRate == null) {
            reactivationRate = "0";
        }
        String cancellationRate = request.getParameter("CancellationChargeRate");
        if (cancellationRate == null) {
            cancellationRate = "0";
        }
        String latePenaltyChargeSDay1 = request.getParameter("LatePenaltyChargeSDay1");
        if (latePenaltyChargeSDay1 == null) {
            latePenaltyChargeSDay1 = "0";
        }
        String latePenaltyChargeEDay1 = request.getParameter("LatePenaltyChargeEDay1");
        if (latePenaltyChargeEDay1 == null) {
            latePenaltyChargeEDay1 = "0";
        }
        String latePenaltyChargeRate1 = request.getParameter("LatePenaltyChargeRate1");
        if (latePenaltyChargeRate1 == null) {
            latePenaltyChargeRate1 = "0";
        }
        String latePenaltyChargeSDay2 = request.getParameter("LatePenaltyChargeSDay2");
        if (latePenaltyChargeSDay2 == null) {
            latePenaltyChargeSDay2 = "0";
        }
        String latePenaltyChargeEDay2 = request.getParameter("LatePenaltyChargeEDay2");
        if (latePenaltyChargeEDay2 == null) {
            latePenaltyChargeEDay2 = "0";
        }
        String latePenaltyChargeRate2 = request.getParameter("LatePenaltyChargeRate2");
        if (latePenaltyChargeRate2 == null) {
            latePenaltyChargeRate2 = "0";
        }
        String gstRate = request.getParameter("gstRate");
        String vatRate = request.getParameter("vatRate");
        String stRate = request.getParameter("stRate");
        JSONObject latePCJSON = new JSONObject();
        JSONObject taxJSON = new JSONObject();
        logger.debug("Value of minimumBalance = " + minimumBalance);
        logger.debug("Value of freeCredits = " + freeCredits);
        logger.debug("Value of mainCredits = " + mainCredits);
        logger.debug("Value of freeTrail = " + freeTrail);
        logger.debug("Value of serviceChargeRate = " + serviceChargeRate);
//        System.out.println("minimumBalance " + minimumBalance);
//        System.out.println("freeCredits " + freeCredits);
//        System.out.println("freeTrail " + freeTrail);
//        System.out.println("serviceCharge " + serviceChargeRate);
        float miniBal = 0;
        float freeCre = 0f;
        float chRate = 0;
        float caRate = 0;
        float mainCreditsF = 0;
        double reAcCh = 0.0;
        try {
            if(minimumBalance != null && !minimumBalance.isEmpty()){
                miniBal = Float.parseFloat(minimumBalance);
            }
            freeCre = Float.parseFloat(freeCredits);
            chRate = Float.parseFloat(changePackageRate);
            caRate = Float.parseFloat(cancellationRate);
            reAcCh = Double.parseDouble(reactivationRate);
            mainCreditsF = Float.parseFloat(mainCredits);
        } catch (Exception ex) {
            try {
                json.put("result", "error");
                json.put("message", "Price value can not be empty");
                return;
            } finally {
                out.print(json);
                out.flush();
            }
        }
        packObj.setMinimumBalance(miniBal);
        packObj.setFreeCredits(freeCre);
        packObj.setDaysForFreeTrial(freeTrail);
        packObj.setMainCredits(mainCreditsF);
        packObj.setServiceCharge(serviceChargeRate);
        packObj.setChangePackageRate(chRate);
        packObj.setReActivationCharge(reAcCh);
        packObj.setCancellationRate(caRate);
        if (packObj.getTabShowFlag() != GlobalStatus.FEATURE) {
            packObj.setTabShowFlag(GlobalStatus.APRATE);
        }
        //packObj.setMakerflag(PackageManagement.REQUEST_SUSPENDED_STATUS);
        String partnerId = "";
        if (PartnerVisibility != null) {
            for (int i = 0; i < PartnerVisibility.length; i++) {
                partnerId += PartnerVisibility[i] + ",";
            }
        }
        packObj.setPartnerVisibility(partnerId);
        latePCJSON.put("latePenaltyChargeStartDay1", 0);
        latePCJSON.put("latePenaltyChargeEndDay1", 0);
        latePCJSON.put("latePenaltyChargeRate1", 0);
        latePCJSON.put("latePenaltyChargeStartDay2", 0);
        latePCJSON.put("latePenaltyChargeEndDay2", 0);
        latePCJSON.put("latePenaltyChargeRate2", 0);
        if (latePenaltyChargeSDay1 != null && !latePenaltyChargeSDay1.equals("")) {
            latePCJSON.put("latePenaltyChargeStartDay1", latePenaltyChargeSDay1);
        }
        if (latePenaltyChargeEDay1 != null && !latePenaltyChargeEDay1.equals("")) {
            latePCJSON.put("latePenaltyChargeEndDay1", latePenaltyChargeEDay1);
        }
        if (latePenaltyChargeRate1 != null && !latePenaltyChargeSDay1.equals("")) {
            latePCJSON.put("latePenaltyChargeRate1", latePenaltyChargeRate1);
        }
        if (latePenaltyChargeSDay2 != null && !latePenaltyChargeSDay2.equals("")) {
            latePCJSON.put("latePenaltyChargeStartDay2", latePenaltyChargeSDay2);
        }
        if (latePenaltyChargeEDay2 != null && !latePenaltyChargeEDay2.equals("")) {
            latePCJSON.put("latePenaltyChargeEndDay2", latePenaltyChargeEDay2);
        }
        if (latePenaltyChargeRate2 != null && !latePenaltyChargeRate2.equals("")) {
            latePCJSON.put("latePenaltyChargeRate2", latePenaltyChargeRate2);
        }
        packObj.setLatePenaltyRate(latePCJSON.toString());
        taxJSON.put("gstTax", 0);
        taxJSON.put("vatTax", 0);
        taxJSON.put("stTax", 0);
        if (gstRate != null && !gstRate.equals("")) {
            taxJSON.put("gstTax", gstRate);
        }
        if (vatRate != null && !vatRate.equals("")) {
            taxJSON.put("vatTax", vatRate);
        }
        if (stRate != null && !stRate.equals("")) {
            taxJSON.put("stTax", stRate);
        }
        packObj.setTax(taxJSON.toString());
        int retValue = new PackageManagement().editPackage(SessionId, packObj);
        MakerChaker makerChakerObj = (MakerChaker) request.getSession().getAttribute("makerChacker");
        SgReqbucketdetails existsObj = new RequestPackageManagement().getReqPackageByName(SessionId, channelId, packObj.getBucketName());
        boolean makerChackerFlag = true;
        if (makerChakerObj == null) {
            makerChackerFlag = false;
        } else if (makerChakerObj.status == GlobalStatus.SUSPEND) {
            makerChackerFlag = false;
        }
        try {
            if (retValue == -1) {
                result = "error";
                message = "Package rate failed to updated.";
                json.put("result", result);
                logger.debug("Response of #EditPackage from #PPAdmin Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of #EditPackage from #PPAdmin Servlet's Parameter  message is " + message);
            } else {
                if (makerChackerFlag == false && existsObj != null) {
                    existsObj.setMinimumBalance(packObj.getMinimumBalance());
                    existsObj.setFreeCredits(packObj.getFreeCredits());
                    existsObj.setDaysForFreeTrial(packObj.getDaysForFreeTrial());
                    existsObj.setMainCredits(packObj.getMainCredits());
                    existsObj.setServiceCharge(packObj.getServiceCharge());
                    existsObj.setChangePackageRate(packObj.getChangePackageRate());
                    existsObj.setReActivationCharge(packObj.getReActivationCharge());
                    existsObj.setCancellationRate(packObj.getCancellationRate());
                    existsObj.setTabShowFlag(packObj.getTabShowFlag());
                    existsObj.setMakerflag(packObj.getMakerflag());
                    existsObj.setPartnerVisibility(packObj.getPartnerVisibility());
                    existsObj.setLatePenalitesRate(packObj.getLatePenaltyRate());
                    existsObj.setTax(packObj.getTax());
                    new RequestPackageManagement().editReqPackage(SessionId, channelId, existsObj);
                }
                json.put("result", result);
                json.put("message", message);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            logger.info("Response of #EditPackage from #PPAdmin " + json.toString());
            logger.info("Response of #EditPackage from #PPAdmin Servlet at " + new Date());
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
