<%--<%@page import="com.partner.web.TransformInfo"%>--%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.ClassName"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MethodName"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Classes"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Methods"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@include file="header.jsp" %>
<%    String _apid = request.getParameter("_apid");
    String _resId = request.getParameter("_resId");
    int resId = Integer.parseInt(_resId);
    int apid = Integer.parseInt(_apid);
    session.setAttribute("_apId", apid);
    Accesspoint ap = new AccessPointManagement().getAccessPointById(SessionId, channelId, apid);
    String apName = ap.getName();
    Map classMap = null;
    Map methodMap = null;
    String value = apName;
//    Methods methods = (Methods) methodMap.get(value);/    
    TransformDetails tf = new TransformManagement().getTransformDetails(SessionId, channelId, apid, resId);
    byte[] me = tf.getMethods();
    byte[] cl = tf.getClasses();
    String ver = request.getParameter("ver");
    Map tmpMethods = (Map) Serializer.deserialize(me);
    Map tmpclasses = (Map) Serializer.deserialize(cl);
    Classes classes = (Classes) tmpclasses.get("" + ver);
    Methods methods = (Methods) tmpMethods.get("" + ver);
    if (classMap == null) {
        classMap = new HashMap();
    }
    if (methodMap == null) {
        methodMap = new HashMap();
    }
    classMap.put(value, classes);
    methodMap.put(value, methods);
    session.setAttribute("eclasses", classMap);
    session.setAttribute("emethods", methodMap);
    int c = classes.pname.classs.size();
%>
<script src = "./js/acceptPoint.js" ></script>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header text-uppercase">Services Description Details</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47; <i class="fa fa-cube"></i><a href="partners.jsp"> Developer Management</a>
                    &#47; <i class="fa fa-cube"></i><a href="javascript:history.back()"> Group Services</a>&#47; <i class="fa fa-cube"></i> Service Descriptions
                </div>
                <div class="panel-body">
                    <ul class="nav nav-pills">
                        <li class="active"><a href="#primary" data-toggle="tab">API Level</a>
                        </li>
                        <%if (c != 0) {%><li><a href="#secondary" data-toggle="tab">Datatype Level</a>
                        </li>
                        <%}%>
                    </ul>
                    </br>
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="primary">
                            <form role="form" class="form-horizontal" id="validateMethodAPIform" name="validateMethodAPIform">
                                <div class="dataTable_wrapper">
                                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>API Names</th>
                                                <th>Return Types</th>                                                
                                                <th>Description</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <% List list = methods.methodClassName.methodNames;
                                                int count = 0;
                                                for (int i = 0; i < list.size(); i++) {
                                                    MethodName methodName = (MethodName) list.get(i);
                                                    if (methodName.visibility.equalsIgnoreCase("yes")) {
                                                        count++;
                                            %>
                                            <tr align=?left?>
                                                <TD><%=count%></TD>
                                                <TD><%=methodName.methodname%></TD>

                                                <TD ><%=methodName.returntype.replace("<", "&lt;").replace(">", "&gt;")%></TD>
                                                <TD>
                                                    <a class="btn btn-info btn-xs" href="./editDesc.jsp?_resId=<%=resId%>&value=<%=value%>&methodName=<%=methodName.methodname%>&ver=<%=ver%>" data-html="true"><i class="fa fa-info-circle"></i> Description</a>

                                                </TD>
                                            </tr>
                                            <% }
                                                }
                                            %>
                                        </tbody>
                                    </table>
                                </div>
                            </form>
                        </div>



                        <div class="tab-pane" id="secondary">
                            <div class="row-fluid">
                                <form class="form-horizontal" id="validateClassAPIform" name="validateClassAPIform" >
                                    <fieldset>                               

                                        <div id='Settings_table'>
                                            <table class="table table-striped table-bordered table-hover" id="dataTables-example1">
                                                <thead>
                                                    <tr >
                                                        <TD><FONT ><B>Sr No</B></FONT></TD>
                                                        <TD><FONT ><B>Datatype</B></FONT></TD>
                                                        <TD><FONT ><B>Description</B></FONT></TD>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <%
                                                        List classList = classes.pname.classs;
                                                        for (int i = 0; i < classList.size(); i++) {
                                                            ClassName className = (ClassName) classList.get(i);
                                                    %>
                                                    <tr align=?left?>
                                                        <TD style="width: 10%"><%=(i + 1)%></TD>
                                                        <!--<TD ><input type="checkbox" name="<%=className.classname%>" id="<%=className.classname%>"></TD>-->
                                                        <TD style="width: 10%"><%=className.classname%></TD>
                                                        <TD>
                                                            <a href="./editClassDesc.jsp?_resId=<%=resId%>&value=<%=value%>&classname=<%= className.classname%>&ver=<%=ver%>" class="btn btn-info btn-xs" data-html="true"><i class="fa fa-info-circle"></i> Description</a>
                                                        </TD>

                                                    </tr>
                                                    <%}
                                                    %>
                                                </tbody>
                                            </table>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript 
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.min.js"></script>
<script src="js/morris-data.js"></script>-->

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>
<script src="js/bootbox.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>
<script>
    $(document).ready(function () {
        $('#dataTables-example1').DataTable({
            responsive: true
        });
    });
</script> 
</body>
</html>

<%@include file="footer.jsp" %> 