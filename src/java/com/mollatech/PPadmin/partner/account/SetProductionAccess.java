/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.partner.account;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author Ashu
 */
@WebServlet(name = "SetProductionAccess", urlPatterns = {"/SetProductionAccess"})
public class SetProductionAccess extends HttpServlet {

    static final Logger logger = Logger.getLogger(SetProductionAccess.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        logger.info("Requested Servlet is SetProductionAccess from PPAdmin at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        String result, message;
        JSONObject json = new JSONObject();
        try {

        } catch (Exception e) {
            logger.error("Exception at SetProductionAccess ", e);
            result = "error";
            message = "Error in Checking Availability";
            json.put("result", result);
            logger.debug("Response of SetProductionAccess Servlet's from PPAdmin Parameter   result is " + result);
            json.put("message", message);
            logger.debug("Response of SetProductionAccess Servlet's from PPAdmin Parameter   message is " + message);
        } finally {
            logger.info("Response of SetProductionAccess from PPAdmin" + json.toString());
            logger.info("Response of SetProductionAccess Servlet from PPAdmin at " + new Date());
            out.print(json);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
