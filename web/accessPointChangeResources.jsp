<%--<%@page import="portalconnect.PartnerPortalWrapper"%>--%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.GroupDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.GroupManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="java.util.List"%>
<%--<%@page import="com.partner.web.ResourceDetails"%>
<%@page import="com.partner.web.GroupDetails"%>
<%@page import="com.partner.web.Accesspoint"%>--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <div class="control-group">
        <label class="control-label"  for="username"></label>
        <div class="controls">
            <select class="span10" name="_resourceselect" id="_resourceselect">
                <%
                   // PartnerPortalWrapper ppw = new PartnerPortalWrapper();
                    AccessPointManagement ppw = new AccessPointManagement();
                    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
                    String channelId = (String) request.getSession().getAttribute("_channelId");
                    String apId = request.getParameter("_apId");
                    int acpId = Integer.parseInt(apId);
                    if (acpId != -1) {
                        //getAll States here 
//                        LocationManagement lManagement = new LocationManagement();
                        //Statelist[] stateList = lManagement.getAllStateListByCountry(iCountryId);
                        Accesspoint accesspoint = ppw.getAccessPointById(SessionId, channelId, acpId);
                        int gpId = ppw.getGroupIDbyAceesspointID(SessionId, channelId, acpId);
                        //GroupDetails g = new GroupManagement().getGroupDetails(SessionId, null, gpId);
                        String resourceList = accesspoint.getResources();
                        String[] res = resourceList.split(",");
                        for (int i = 0; i < res.length; i++) {
                            if (!res[i].isEmpty() && res[i] != null) {
                                int resID = Integer.parseInt(res[i]);
                                ResourceDetails rsName = new ResourceManagement().getResourceById(SessionId, channelId, resID);
                %>
                <option value="<%=rsName.getResourceId()%>"><%=rsName.getName()%></option>
                <%}
                    }
                } else {%>
                <option value="-1">Select All</option>  
                <%
                    ResourceDetails[] rdetails = null;
                    rdetails = new ResourceManagement().getAllResources(SessionId, channelId);

                    for (int i = 0; i < rdetails.length; i++) {%>
                <option value="<%=rdetails[i].getResourceId()%>"><%=rdetails[i].getName()%></option>
                <%}
                    }%>            

            </select>
        </div>
    </div>

