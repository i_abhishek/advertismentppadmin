function pageRefreshgraphTransaction(type) {
    window.location.href = "./operatorReports.jsp?_type=" + type + "&_opType=1";
}
function showAlert(message, type, closeDelay) {
    if ($("#alerts-container").length == 0) {
        $("body")
                .append($('<div id="container" style="' +
                        'width: %; margin-left: 65%; margin-top: 10%;">'));
    }    
    type = type || "info";
    var alert = $('<div class="alert alert-' + type + ' fade in">')
            .append(
                    $('<button type="button" class="close" data-dismiss="alert">')
                    .append("&times;")
                    )
            .append(message);
    $("#alerts-container").prepend(alert);
    if (closeDelay)
        window.setTimeout(function () {
            alert.alert("close")
        }, closeDelay);
}
function generateOperatorreports(type, opType) {
    var sdate = document.getElementById('_startdate').value;
    var edate = document.getElementById('_enddate').value;
    if (sdate === '' || edate === '') {
//        bootboxmodel("<h2><font style='color:red'>Please Select the Date ranges!!!</font><h2>");
        showAlert('Please Select the Date ranges !',"danger",4000);
        return;
    } else {
        var waiting1 = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
                '<div class="modal-dialog"><div class="modal-content"><div class="modal-body"><h2><font style="color:  blue; font-weight: bolder">Please Wait</fon></h2></div></div></div></div>');
        waiting1.modal();
        document.getElementById('Button').disabled = true;
        document.getElementById('_ApEndTime').disabled = true;
        document.getElementById('_ApStartTime').disabled = true;
        document.getElementById('_startdate').disabled = true;
        document.getElementById('_enddate').disabled = true;
        document.getElementById('_Accesspoint').disabled = true;
        document.getElementById('_Group').disabled = true;
        document.getElementById('_Resources').disabled = true;
        document.getElementById('_Partner').disabled = true;        
        var Accesspoint = document.getElementById('_Accesspoint').value;
        var Group = document.getElementById('_Group').value;
        var Resource = document.getElementById('_Resources').value;
        var partner = document.getElementById('_Partner').value;
        var stime = document.getElementById('_ApStartTime').value;
        var etime = document.getElementById('_ApEndTime').value;
        var s = './chartreportsOperators.jsp?_type=' + type + '&_accesspoint=' + Accesspoint + '&_group=' + Group + '&_resources=' + Resource + '&_partner=' + partner + '&_sdate=' + sdate + '&_edate=' + edate + '&_stime=' + stime + '&_etime=' + etime;
        $.ajax({
            type: 'GET',
            url: s,
            success: function(data) {
                $('#report_data').html(data);
                if (type === 1) {
                    $("#bargraph").empty();
                    var day_data = UserReportBarChart(opType, Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
                    Morris.Bar({
                        element: 'bargraph',
                        data: day_data,
                        xkey: 'label',
                        ykeys: ['value'],
                        labels: ['Transactions count'],
                        barColors: function(type) {
                            if (type === 'bar') {
                                return '#0066CC';
                            }
                            else {
                                return '#0066CC';
                            }
                        }
                    });

                    $("#linegraph").empty();
                    var linedata = UserReportLineChart(opType, Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
//              alert(linedata);
                    Morris.Line({
                        element: 'linegraph',
                        data: linedata,
                        xkey: 'label',
                        ykeys: ['value'],
                        xLabels: 'day',
                        labels: ['value']
                    });
                } else if (type === 2) {
                    $("#linertgraph").empty();
                    var linedata = ResponseReportLineChart(opType, Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
                    Morris.Line({
                        element: 'linertgraph',
                        data: linedata,
                        xkey: 'label',
                        ykeys: ['value'],
                        xLabels: 'day',
                        labels: ['value'],
                        lineWidth: 0,
                        pointSize: 5,
                        smooth: false,
                        postUnits: 'ms'
                    });
                }
                waiting1.modal('hide');
            }
        });
    }
}

function generatereports1(opType) {
    var waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body"><h2><font style="color:  blue; font-weight: bolder">Please Wait</fon></h2></div></div></div></div>');
    waiting.modal();
    var type = document.getElementById('_type').value;
    var Accesspoint = document.getElementById('_Accesspoint').value;
    var Group = document.getElementById('_Group').value;
    var Resource = document.getElementById('_Resources').value;
    var partner = document.getElementById('_Partner').value;
    var sdate = document.getElementById('_startdate').value;
    var edate = document.getElementById('_enddate').value;
    var stime = document.getElementById('_ApStartTime').value;
    var etime = document.getElementById('_ApEndTime').value;
    var s = './chartreportsOperators.jsp?_type=' + type + '&_accesspoint=' + Accesspoint + '&_group=' + Group + '&_resources=' + Resource + '&_partner=' + partner + '&_sdate=' + sdate + '&_edate=' + edate + '&_stime=' + stime + '&_etime=' + etime;
    $.ajax({
        type: 'GET',
        url: s,
        success: function(data) {
            $('#report_data').html(data);

            $("#linegraph").empty();
            var linedata = UserReportLineChart(opType, Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
            Morris.Line({
                element: 'linegraph',
                data: linedata,
                xkey: 'label',
                ykeys: ['value'],
                xLabels: 'day',
                labels: ['value']
            });
            $("#linertgraph").empty();
            var linedata = ResponseReportLineChart(opType, Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
            Morris.Line({
                element: 'linertgraph',
                data: linedata,
                xkey: 'label',
                ykeys: ['value'],
                xLabels: 'day',
                labels: ['value']
            });
            waiting.modal('hide');
        }
    });
}

function UserReportBarChart(opType, Accesspoint, Group, Resource, partner, sdate, edate, stime, etime) {
    var s = './barchartreport?_opType=' + opType + '&_accesspoint=' + Accesspoint + '&_group=' + Group + '&_resource=' + Resource + '&_partner=' + partner + '&_sdate=' + sdate + '&_edate=' + edate + '&_stime=' + stime + '&_etime=' + etime;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = JSON.parse(jsonData);
    return myJsonObj;
}

function UserReportLineChart(opType, Accesspoint, Group, Resource, partner, sdate, edate, stime, etime) {
    var s = './linechartreport?_opType=' + opType + '&_accesspoint=' + Accesspoint + '&_group=' + Group + '&_resource=' + Resource + '&_partner=' + partner + '&_sdate=' + sdate + '&_edate=' + edate + '&_stime=' + stime + '&_etime=' + etime;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = JSON.parse(jsonData);
    return myJsonObj;
}

function ResponseReportLineChart(opType, Accesspoint, Group, Resource, partner, sdate, edate, stime, etime) {
    var s = './linechartresponsereport?_opType=' + opType + '&_accesspoint=' + Accesspoint + '&_group=' + Group + '&_resource=' + Resource + '&_partner=' + partner + '&_sdate=' + sdate + '&_edate=' + edate + '&_stime=' + stime + '&_etime=' + etime;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = JSON.parse(jsonData);
    return myJsonObj;
}

function userReportPDF(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime) {
//    var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
//    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
//    var val4 = encodeURIComponent(document.getElementById('_auditUserID').value);
    var s = './getReports?_apid=' + Accesspoint + "&_grid=" + Group + "&_resid=" + Resource + "&_pid=" + partner + "&_sdate=" + sdate + "&_stime=" + stime + "&_edate=" + edate + "&_etime=" + etime + "&_format=0";
    window.location.href = s;
    return false;
}

function userReportCSV(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime) {
    var s = './getReports?_apid=' + Accesspoint + "&_grid=" + Group + "&_resid=" + Resource + "&_pid=" + partner + "&_sdate=" + sdate + "&_stime=" + stime + "&_edate=" + edate + "&_etime=" + etime + "&_format=1";
    window.location.href = s;
    return false;
}

function userReportTXT(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime) {
    var s = './getReports?_apid=' + Accesspoint + "&_grid=" + Group + "&_resid=" + Resource + "&_pid=" + partner + "&_sdate=" + sdate + "&_stime=" + stime + "&_edate=" + edate + "&_etime=" + etime + "&_format=2";
    window.location.href = s;
    return false;
}
