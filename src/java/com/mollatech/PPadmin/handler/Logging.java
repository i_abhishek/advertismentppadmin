/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.handler;

import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.nucleus.db.Operators;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author Ashu
 */
@WebFilter(filterName = "Logging", urlPatterns = {"/*"})
public class Logging implements Filter {

    private FilterConfig filterConfig = null;
    private Operators operators = null;
    private String IP = null;

    public Logging() {
    }
    static final Logger logger = Logger.getLogger("admin");

    private void doBeforeProcessing(RequestWrapper request, ResponseWrapper response)
            throws IOException, ServletException {
        String loggingbody = "";
        String str = request.getRequestURL().toString();
        if (str.endsWith(".jsp") || str.endsWith(".html") || str.endsWith(".css") || str.endsWith(".js")) {
            return;
        }
        String values = "";
        Map requestMap = request.getParameterMap();
        for (Object key : requestMap.keySet()) {
            String body = "[ ";
            String[] objects = (String[]) requestMap.get(key);
            if (objects != null) {
                for (String value : objects) {
                    body += value;
                }
            }
            body += " ]";
            values += key + " = " + body + ", ";
        }
        if (operators != null) {
            loggingbody += "Request From " + operators.getName() + " To ";
//            System.out.println(values);
        }
        loggingbody += " URL " + str + "Operator's IP is: " + IP + " And Parameters " + values;
        logger.info(loggingbody);

    }

    private void doAfterProcessing(RequestWrapper request, ResponseWrapper response)
            throws IOException, ServletException {
        String loggingbody = "";
        String str = request.getRequestURL().toString();
        if (str.endsWith(".jsp") || str.endsWith(".html") || str.endsWith(".css") || str.endsWith(".js")) {
            return;
        }

        if (operators != null) {
            loggingbody += "Request From " + operators.getName() + " To ";
//            System.out.println(values);
        }
        String originalResponse = response.getResponseContent();
        loggingbody += " URL " + str + "Operator's IP is: " + IP + " And Response Is " + originalResponse;
        logger.info(loggingbody);
    }

    /**
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are creating
     * @param chain The filter chain we are processing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain)
            throws IOException, ServletException {

        String str = ((HttpServletRequest) request).getRequestURL().toString();
        if (str.endsWith(".jsp") || str.endsWith(".html") || str.endsWith(".css") || str.endsWith(".js") || str.endsWith("MonitorHandling") || str.endsWith("operatorLogin")
                || str.endsWith("PPAdmin") || str.endsWith("PPAdmin/") || str.endsWith("png") || str.endsWith("ico") || str.endsWith("changeoprpassword")) {
            chain.doFilter(request, response);
            return;
        }
        operators = (Operators) ((HttpServletRequest) request).getSession().getAttribute("_apOprDetail");
        IP = ((HttpServletRequest) request).getRemoteAddr();
        RequestWrapper wrappedRequest = new RequestWrapper((HttpServletRequest) request);
        ResponseWrapper wrappedResponse = new ResponseWrapper((HttpServletResponse) response);

        doBeforeProcessing(wrappedRequest, wrappedResponse);

        Throwable problem = null;
        String error = null;

        try {
            chain.doFilter(wrappedRequest, wrappedResponse);
        } catch (Throwable t) {

            problem = t;
            StringWriter errors = new StringWriter();
            t.printStackTrace(new PrintWriter(errors));
            error = errors.toString();
//            t.printStackTrace();
        }

        doAfterProcessing(wrappedRequest, wrappedResponse);

        // Let's convert response content to upper case
        String originalResponse = wrappedResponse.getResponseContent();
        response.getOutputStream().print(originalResponse);
        response.getOutputStream().flush();
        response.getOutputStream().close();
        // If there was a problem, we want to rethrow it if it is
        // a known type, otherwise log it.
        if (problem != null) {
            sendProcessingError(error);
            if (problem instanceof ServletException) {
                throw (ServletException) problem;
            }
            if (problem instanceof IOException) {
                throw (IOException) problem;
            }

        }
    }

    /**
     * Return the filter configuration object for this filter.
     */
    public FilterConfig getFilterConfig() {
        return (this.filterConfig);
    }

    /**
     * Set the filter configuration object for this filter.
     *
     * @param filterConfig The filter configuration object
     */
    public void setFilterConfig(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
    }

    /**
     * Destroy method for this filter
     */
    public void destroy() {
    }

    /**
     * Init method for this filter
     */
    @Override
    public void init(FilterConfig filterConfig) {
        this.filterConfig = filterConfig;
        logger.setLevel(Level.OFF);
        Properties properties = new Properties();
        properties.setProperty("log4j.logger.admin", LoadSettings.g_sSettings.getProperty("admin.log.level") + ", adminAppender");
        properties.setProperty("solr.log", LoadSettings.g_strPath.replace("serviceguard-settings", "") + "SGLogs" + File.separator);
        properties.setProperty("log4j.appender.adminAppender", "org.apache.log4j.DailyRollingFileAppender");
        properties.setProperty("log4j.appender.adminAppender.File", "${solr.log}PPAdmin.log");
        properties.setProperty("log4j.appender.adminAppender.DatePattern", "'.'yyyy-MM-dd");
        properties.setProperty("log4j.additivity.admin", "false");
        properties.setProperty("log4j.appender.adminAppender.layout.ConversionPattern", "[%-5p] %d{dd-MMM-yy hh-mm-ss} %c %M - %m%n");
        properties.setProperty("log4j.appender.adminAppender.layout", "org.apache.log4j.PatternLayout");
        PropertyConfigurator.configure(properties);
    }

    /**
     * Return a String representation of this object.
     */
    @Override
    public String toString() {
        if (filterConfig == null) {
            return ("Logging()");
        }
        StringBuffer sb = new StringBuffer("Logging(");
        sb.append(filterConfig);
        sb.append(")");
        return (sb.toString());

    }

    private void sendProcessingError(String error) {
        logger.error(error);
    }

    public static String getStackTrace(Throwable t) {
        String stackTrace = null;
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            t.printStackTrace(pw);
            pw.close();
            sw.close();
            stackTrace = sw.getBuffer().toString();
        } catch (Exception ex) {
        }
        return stackTrace;
    }

    public void log(String msg) {
        filterConfig.getServletContext().log(msg);
    }

    /**
     * This request wrapper class extends the support class
     * HttpServletRequestWrapper, which implements all the methods in the
     * HttpServletRequest interface, as delegations to the wrapped request. You
     * only need to override the methods that you need to change. You can get
     * access to the wrapped request using the method getRequest()
     */
    class RequestWrapper extends HttpServletRequestWrapper {

        public RequestWrapper(HttpServletRequest request) {
            super(request);
        }

        // You might, for example, wish to add a setParameter() method. To do this
        // you must also override the getParameter, getParameterValues, getParameterMap,
        // and getParameterNames methods.
        protected Hashtable localParams = null;

        public void setParameter(String name, String[] values) {

            if (localParams == null) {
                localParams = new Hashtable();
                // Copy the parameters from the underlying request.
                Map wrappedParams = getRequest().getParameterMap();
                Set keySet = wrappedParams.keySet();
                for (Iterator it = keySet.iterator(); it.hasNext();) {
                    Object key = it.next();
                    Object value = wrappedParams.get(key);
                    localParams.put(key, value);
                }
            }
            localParams.put(name, values);
        }

        @Override
        public String getParameter(String name) {

            if (localParams == null) {
                return getRequest().getParameter(name);
            }
            Object val = localParams.get(name);
            if (val instanceof String) {
                return (String) val;
            }
            if (val instanceof String[]) {
                String[] values = (String[]) val;
                return values[0];
            }
            return (val == null ? null : val.toString());
        }

        @Override
        public String[] getParameterValues(String name) {

            if (localParams == null) {
                return getRequest().getParameterValues(name);
            }
            return (String[]) localParams.get(name);
        }

        @Override
        public Enumeration getParameterNames() {

            if (localParams == null) {
                return getRequest().getParameterNames();
            }
            return localParams.keys();
        }

        @Override
        public Map getParameterMap() {

            if (localParams == null) {
                return getRequest().getParameterMap();
            }
            return localParams;
        }
    }

    /**
     * This response wrapper class extends the support class
     * HttpServletResponseWrapper, which implements all the methods in the
     * HttpServletResponse interface, as delegations to the wrapped response.
     * You only need to override the methods that you need to change. You can
     * get access to the wrapped response using the method getResponse()
     */
    class ResponseWrapper extends HttpServletResponseWrapper {

        public ResponseWrapper(HttpServletResponse response) {
            super(response);
        }
        private StringWriter stringWriter;
        private boolean isOutputStreamCalled;

        @Override
        public ServletOutputStream getOutputStream() throws IOException {
            if (this.stringWriter != null) {
                throw new IllegalStateException("The getWriter() is already called.");
            }
            isOutputStreamCalled = true;
            return super.getOutputStream();
        }

        @Override
        public PrintWriter getWriter() throws IOException {
            if (isOutputStreamCalled) {
                throw new IllegalStateException("The getOutputStream() is already called.");
            }

            this.stringWriter = new StringWriter();

            return new PrintWriter(this.stringWriter);
        }

        public String getResponseContent() {
            try {
                if (this.stringWriter != null) {
                    return this.stringWriter.toString();
                }
            } catch (Exception ex) {
                return "";
            }
            return "";
        }

    }

}
