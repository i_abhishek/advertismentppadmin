function downloadDocments(id){
//    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div></div></div>');
//    pleaseWaitDiv.modal();
    pleasewaitV1();
//    var waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
//                '<div class="modal-dialog"><div class="modal-content"><div class="modal-body"><h2><font style="color:  blue; font-weight: bolder">Please Wait</font></h2></div></div></div></div>');
//        waiting.modal();
    var s = './downloadDoc?id='+id;
    //alert("id from js "+id);
    window.location.href = s;
//    pleaseWaitDiv.modal('hide');
    waitingV1.modal('hide');
    return false;
    
}

function bootboxmodel(content) {
    var popup = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' +
            '<button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button><font style="font-weight: 900;color: blue">' +
            content +
            '</font><br><a class="btn btn-info btn-xs" data-dismiss="modal" class="close" style="align:right"> &nbsp; OK </a></div></div></div></div>');
    popup.modal();
}
var waiting;
//function pleasewait() {
//    waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
//            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h2><font style="font-weight:900">Please Wait</font></h2>' +
//            '</div></div></div></div>');
//    waiting.modal();
//}

function pleasewait() {
    waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h3>Processing...</h3></div>' +
            '</div></div></div></div>');
    //waiting = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"></div></div></div>');
    waiting.modal();
}
function strCompare(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function Alert4Users(msg) {
    bootboxmodel("<h4>" + msg + "</h4>");
}

function userRegistration() {
    var s = './registerPartner';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#register_form").serialize(),
        success: function(data) {
            if (strCompare(data._result, "error") === 0) {
                Alert4Users(data._message);
            }
            else if (strCompare(data._result, "success") === 0) {
                Alert4Users(data._message);
                setTimeout(function() {
                    window.location.href = "./login.jsp";
                }, 3000);
            }
        }
    });
}
function signup() {
    window.location.href = "register.jsp";
}


function showNewImage() {
    var _securePhrase = document.getElementById("_securePhrase").value;
    var _select_color = document.getElementById("_apColor").value;
//                alert('_select_color'+_select_color);
    var imagelink = "<image src='./editImage?_securePhrase=" + _securePhrase + "&_select_color=" + encodeURIComponent(_select_color) + "'/>";
    $('#showNewImage').html(imagelink);
}

function userLogin() {
    if ($('#remember_password').is(":checked")) {
        var username = document.getElementById("_name").value;
        var password = document.getElementById("_passwd").value;
        document.cookie = "username=" + username;
        document.cookie = "password=" + password;
        document.cookie = "partnerChecked=1";
    } else {
        document.cookie = "partnerChecked=0";
    }
    var xCoordinate = document.getElementById('x').innerHTML;
    var yCoordinate = document.getElementById('y').innerHTML;
    var s = './login?x=' + xCoordinate + '&y=' + yCoordinate;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#login_form").serialize(),
        success: function(data) {
            if (strCompare(data._result, "error") === 0) {
                Alert4Users(data._message);
            }
            else if (strCompare(data._result, "success") === 0) {
                Alert4Users(data._message);
                if (strCompare(data._url, "image.jsp") === 0) {
                    window.location.href = data._url;
                } else {
                    GetWTUser2FA(3);
                }
//                var istokenAvailable = CheckWebTokenIsAvailable(data._identifier);
//                selectPage(istokenAvailable);
//              window.location.href = data._url;//"./image.jsp";
            }
        }
    });
}

function SetColor(colorCodeObj) {
//    alert(colorCodeObj);
    document.getElementById("_apColor").value = colorCodeObj;
//    alert(document.getElementById("_apColor").value);
    showNewImage();
}

function setSecurePhrase(_category) {

//    bootbox.confirm("<h2><font color=blue>Are you sure?</font></h2>", function (result) {
//        if (result == false) {
//        }
//        else {
    var xCoordinate = document.getElementById('x').innerHTML;
    var yCoordinate = document.getElementById('y').innerHTML;
    var color = document.getElementById('_apColor').value;
    var securePhrase = document.getElementById('_securePhrase').value;
//            alert(xCoordinate);
//            alert(yCoordinate);alert(color);alert(securePhrase);

    var latitude;
    var longitude;
    var s = './SetSecurePhrase?_category=' + _category + '&_select_color=' + encodeURIComponent(color)
            + '&_securePhrase=' + securePhrase + '&_lattitude=' + latitude
            + '&_longitude=' + longitude + '&_xCoordinate=' + xCoordinate + '&_yCoordinate=' + yCoordinate;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strCompare(data._result, "error") === 0) {
//                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                alert(data._message);
            }
            else if (strCompare(data._result, "success") === 0) {
                alert(data._message);
                window.location.href = "./login.jsp";
                Alert4Users("<span><font color=blue>" + data._message + "</font></span>");
            }
        }
    });
//        }
//    });
}

function showImage() {

    var _name = document.getElementById('_name').value;
    var imagelink = "<image src='./ShowImage?_name=" + _name + "'/>";
    $('#showImage').html(imagelink);
//    var _securePhrase = document.getElementById("_securePhrase").value;
//
//    var _select_color = document.getElementById("_apColor").value;
////                alert(_securePhrase);
//    var imagelink = "<image src='./editImage?_securePhrase=" + _securePhrase + "&_select_color=" + _select_color + "'/>";
//    $('#showNewImage').html(imagelink);

}

function SetType(colorCodeObj) {
    document.getElementById("_apdevicetype").value = colorCodeObj;
//    alert(document.getElementById("_apColor").value);
    showNewImage()
}

function VerifyCredential() {

    var _slientotp = document.getElementById('_otp').value;
    var s = './VerifyCredential?_slientotp=' + _slientotp;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#login_form").serialize(),
        success: function(data) {
            if (strCompare(data._result, "error") === 0) {
                alert(data._message);
            }
            else if (strCompare(data._result, "success") === 0) {
//                alert(data._message);
//                alert('Login Success');
                window.location.href = "./home.jsp";
            }
        }
    });
}

function SilentVerifyCredential() {

//     var _slientotp = document.getElementById('_otp').value;
    var s = './VerifyCredential';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#ActivateWebTokenForm").serialize(),
        success: function(data) {
            if (strCompare(data._result, "error") === 0) {
                alert(data._message);
            }
            else if (strCompare(data._result, "success") == 0) {
                alert(data._message);
                window.setTimeout(
                        function() {
                            window.location.href = "./login.jsp";
                        }, 2000);
            }
        }
    });
}

// new change

var waitingV1;
function pleasewaitV1() {
    waitingV1 = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');
    //waiting = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"></div></div></div>');
    waitingV1.modal();
}

function changePstatus(status, partid, uidiv) {
    var s = './ChangePstatus?_status=' + status + '&_partid=' + partid;
    pleasewaitV1();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strCompare(data._result, "error") === 0) {
                waitingV1.modal('hide');
                //Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                showAlert(data._message,"error",4000);
                setTimeout(function() {
                    window.location = "./partners.jsp";
                }, 3000);
                
            }
            else if (strCompare(data._result, "success") === 0) {
                waitingV1.modal('hide');
                //Alert4Users("<h2><font color=blue>" + data._message + "</font></h2>");
                showAlert(data._message,"success",4000);
                setTimeout(function() {
                    window.location = "./partners.jsp";
                }, 3000);
            }
        }
    });
}


function viewmembers(partnerid) {
    window.location.href = './viewmembers.jsp?partnerId=' + partnerid;
}
function sendrandompassword(partid) {
    var uri = encodeURIComponent(partid);
    var s = './setandresenduserpassword?_partnerId=' + uri;
    pleasewaitV1();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strCompare(data._result, "error") === 0) {
                waitingV1.modal('hide');
                showAlert(data._message,"error",4000);
                setTimeout(function() {
                    window.location = "./partners.jsp";
                }, 3000);
            }
            else if (strCompare(data._result, "success") === 0) {
                waitingV1.modal('hide');
                showAlert(data._message,"success",4000);
                setTimeout(function() {
                    window.location = "./partners.jsp";
                }, 3000);
            }
        }
    });
}

function showAlert(message, type, closeDelay){
   if ($("#alerts-container").length == 0) {
        $("body")
            .append( $('<div id="alerts-container" style="position: fixed;'+
                'width: 50%; left: 25%; top: 10%;">') );
        }
        type = type || "info";          
        message = '<i class="fa fa-info-circle"></i> '+message;
    var alert = $('<div class="alert alert-' + type + ' fade in">')
        .append(
            $('<button type="button" class="close" data-dismiss="alert">')
            .append("&times;")
        )
        .append(message);        
    $("#alerts-container").prepend(alert);    
    if (closeDelay)
        window.setTimeout(function() { alert.alert("close") }, closeDelay);     
}