<%@page import="java.util.Enumeration"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgBucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@include file="header.jsp" %>
<script src="js/designPackage.js" type="text/javascript"></script>
<script src="js/newDetails.js" type="text/javascript"></script>
<script src="js/packageOperation.js" type="text/javascript"></script>
<link href="./select2/select2.css" rel="stylesheet"/>
<%
    String packageName = request.getParameter("_edit");
    SgBucketdetails packageObj = new PackageManagement().getPackageByName(SessionId, channelId, packageName);

    String partnerIds = packageObj.getPartnerVisibility();
    DecimalFormat decimalFormat = new DecimalFormat("#");
    decimalFormat.setMaximumFractionDigits(0);
    boolean flag = false;
    String options = "<option value=all selected>NA</option>\n";
    if (partnerIds != null && !partnerIds.isEmpty() && partnerIds.contains("all,")) {
        options = "<option value=all selected>All</option>\n";
    } else if (partnerIds != null && !partnerIds.isEmpty()) {
        String[] parArray = partnerIds.split(",");
        options = "";
        for (int i = 0; i < parArray.length; i++) {
            int parId = Integer.parseInt(parArray[i]);
            String emailId = "";
            PartnerDetails parObj = new PartnerManagement().getPartnerDetails(parId);
            if (parObj != null) {
                emailId = parObj.getPartnerEmailid();
            }
            options += "<option selected value='" + parId + "'>" + emailId + "</option>\n";
        }
        flag = true;
    }
    PartnerDetails[] partnerObj = new PartnerManagement().getAllPartnerDetails(SessionId, channelId);
    if (partnerObj != null) {
        if (flag) {
            options += "<option value=all>ALL</option>\n";
        }
        for (int i = 0; i < partnerObj.length; i++) {
            if (partnerIds != null && partnerIds.contains(String.valueOf(partnerObj[i].getPartnerId()))) {
                continue;
            }
            if (partnerObj[i].getStatus() == 1) {
                options += "<option value='" + partnerObj[i].getPartnerId() + "'>" + partnerObj[i].getPartnerEmailid() + "</option>\n";
            }
        }
    }
    
    String packageRejectionoptions = "<option value='Select' selected>Select Reason for Rejection </option>\n";
    Enumeration enuKeys = LoadSettings.g_packageRejectionSettings.keys();
    while (enuKeys.hasMoreElements()) {
        String key = (String) enuKeys.nextElement();
        String value = LoadSettings.g_packageRejectionSettings.getProperty(key);
        packageRejectionoptions += "<option value='" + value + "'>" + value + "</option>\n";
    }
    
    String promocodeEnable = LoadSettings.g_sSettings.getProperty("functionalityOf.feature.promocode.booleanValue");
    String loanEnable      = LoadSettings.g_sSettings.getProperty("functionalityOf.feature.loan.booleanValue");
    String vatTax          = LoadSettings.g_sSettings.getProperty("functionalityOf.feature.vatTax.booleanValue");
    String serviceTax      = LoadSettings.g_sSettings.getProperty("functionalityOf.feature.serviceTax.booleanValue");
    boolean featureFlag = false;
    if(promocodeEnable.equalsIgnoreCase("true")||loanEnable.equalsIgnoreCase("true")){
        featureFlag = true;
    }
    String minimumValue = String.valueOf(packageObj.getMinimumBalance());
    Double dMinimumVal  = Double.parseDouble(minimumValue);
%>
<div id="page-wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">" <%=packageObj.getBucketName()%> " package details</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-home"></i><a href="home.jsp"> Home</a> &#47;
                    <i class="fa fa-shopping-cart"></i><a href="packageRequest.jsp"> Package Request</a> &#47;
                    <i class="fa fa-edit"></i> Package Details
                </div>
                <div class="panel-body">
                    <h4>Package details</h4>
                    <hr>                   
                    <!--                    <form class="form-horizontal" id="edit_package_form1" name="edit_package_form1" role="form">-->
                    <div class="col-lg-3">
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fa fa-shopping-cart"></i></span>                                
                            <input type="text" class="form-control" placeholder="Package name" value="<%=packageObj.getBucketName()%>" disabled>                                
                        </div>
                    </div>                                
                    <div class="col-lg-3">
                        <div class="form-group input-group">
                            <span class="input-group-addon">$</span>                               
                            <input type="text" class="form-control" placeholder="Package price" value="<%=packageObj.getPlanAmount()%>" disabled>                                
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>                                                                 
                            <input type="text" class="form-control" placeholder="Package price" value="<%=packageObj.getBucketDuration()%>" disabled>                                                                  
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>                                                                
                            <input type="text" class="form-control" placeholder="Package price" value="<%=packageObj.getPaymentMode()%>" disabled>                                  
                        </div>
                    </div>
                    <label class="control-label col-sm-3"><span style="padding-left:75px"> Developer Visibility</span></label>
                    <div class="left"></div>
                    <div class="col-lg-8">
                        <select id="visible" name="visible" disabled multiple="multiple" style="width: 114%">
                            <%=options%> 
                        </select>
                    </div>
                    <label class="control-label col-sm-3" style="margin-top: 10px"><span style="padding-left:75px;margin-top: 10px"> Package Description</span></label>
                    <div class="col-lg-8" style="margin-top: 10px">
                        <textarea class="form-control" rows="4" cols="1500" width="786px" height="90px" readonly><%=packageObj.getPackageDescription()%></textarea>
                    </div>      
                <div class="col-lg-12">
                    <br>
                    <ul class="nav nav-pills" id="addPackageTab">
                        <li class="active" id="rateTab"><a href="#rate" data-toggle="tab">Rate</a></li>                    
                        <li id="accessPointTab"><a href="#apRate" data-toggle="tab">Access Point Rate</a></li>
                        <li id="flatRateTab"><a href="#flatRate" data-toggle="tab">Flat Rate</a></li>
                        <li id="apiThrottlingTab"><a href="#apiThrottling" data-toggle="tab">API Throttling</a></li>
                        <li id="slabPricingTab"><a href="#promoCode" data-toggle="tab">Slab Pricing</a></li>
                        <li id="tieringPricingTab"><a href="#tieringPrice" data-toggle="tab">Tier Pricing</a></li>
                        <li id="securityTab"><a href="#alerts" data-toggle="tab">Security & Alerts</a></li>
                        <%if(featureFlag){%>
                        <li id="featureTab"><a href="#feature" data-toggle="tab">Feature</a></li>
                        <%}%>
                    </ul>
                    <br>
                    <div class="tab-content">
                        <!-- Rate Tab -->
                        <div class="tab-pane fade in active" id="rate">
                            <form class="form-horizontal" id="edit_package_form" name="edit_package_form" role="form">
                                <input type="hidden" id="firstTab" name="firstTab" value="<%=packageObj.getTabShowFlag()%>">
                                <div class="form-group">                                    
                                    <label class="col-lg-3 control-label" >Minimum balance</label>
                                    <div class="col-lg-2">
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control" disabled id="minimumBalance" name="minimumBalance" placeholder="Price" value="<%=BigDecimal.valueOf(dMinimumVal)%>">                                    
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">                                    
                                    <label class="col-lg-3 control-label" >Main Credits</label>
                                    <div class="col-lg-2">
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control" disabled id="mainCredit" name="mainCredit" placeholder="Price" value="<%=new BigDecimal(packageObj.getMainCredits())%>">                                    
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">                                    
                                    <label class="col-lg-3 control-label" >Free Credits</label>
                                    <div class="col-lg-2">
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control" disabled id="freeCredits" name="freeCredits" placeholder="Price" value="<%=new BigDecimal(packageObj.getFreeCredits())%>">                                    
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">                                    
                                    <label class="col-lg-3 control-label">Free Period</label>
                                    <div class="col-lg-2">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <% if (packageObj.getDaysForFreeTrial() != null && !packageObj.getDaysForFreeTrial().equals("")) {
                                                        if (packageObj.getDaysForFreeTrial().equals("7")) {%>
                                            <input type="text" class="form-control" disabled id="freeCredits" name="freeCredits" placeholder="Price" value="7 Days" >
                                            <%} else if (packageObj.getDaysForFreeTrial().equals("14")) {%>
                                            <input type="text" class="form-control" disabled id="freeCredits" name="freeCredits" placeholder="Price" value="14 Days" >
                                            <%} else if (packageObj.getDaysForFreeTrial().equals("1")) {%>
                                            <input type="text" class="form-control" disabled id="freeCredits" name="freeCredits" placeholder="Price" value="1 Month" >
                                            <%} else if (packageObj.getDaysForFreeTrial().equals("0")) {%>
                                            <input type="text" class="form-control" disabled id="freeCredits" name="freeCredits" placeholder="Price" value="Days" >
                                            <%}
                                            } else {%>                                                
                                            <input type="text" class="form-control" disabled id="freeCredits" name="freeCredits" placeholder="Price" value="NA" >
                                            <%}%>
                                        </div>  
                                    </div>                                    
                                </div>
                                <div class="form-group">                            
                                    <label class="col-lg-3 control-label">1st Setup Service Charge</label>
                                    <%if (Float.parseFloat(packageObj.getServiceCharge()) > 0) {%>
                                    <div class=col-sm-2>
                                        <select id="ServiceCharge" disabled name="ServiceCharge" class="form-control">
                                            <option selected value="Enable">Enable</option>
                                            <option value="Disable">Disable</option>
                                        </select>
                                    </div>
                                    <div class=col-sm-2>
                                        <input type="text" id="serviceChargeRate" disabled name="serviceChargeRate" value="<%=packageObj.getServiceCharge()%>" class="form-control" placeholder="Price">
                                    </div>

                                    <%} else {%>
                                    <div class=col-sm-2>
                                        <select id="ServiceCharge" disabled name="ServiceCharge" class="form-control">
                                            <option  value="Enable">Enable</option>
                                            <option selected value="Disable">Disable</option>
                                        </select>
                                    </div>
                                    <%}%>
                                </div>
                                <div class="form-group">                            
                                    <label class="col-lg-3 control-label">Change Package Charge</label>
                                    <%if (packageObj.getChangePackageRate() > 0) {%>
                                    <div class=col-sm-2>
                                        <select id="ServiceCharge" disabled name="ServiceCharge" class="form-control">
                                            <option selected value="Enable">Enable</option>
                                            <option value="Disable">Disable</option>
                                        </select>
                                    </div>
                                    <div class=col-sm-2>
                                        <input type="text" id="serviceChargeRate" disabled name="serviceChargeRate" value="<%=packageObj.getChangePackageRate()%>" class="form-control" placeholder="Price">
                                    </div>
                                    <%} else {%>
                                    <div class=col-sm-2>
                                        <select id="ServiceCharge" disabled name="ServiceCharge" class="form-control">
                                            <option  value="Enable">Enable</option>
                                            <option selected value="Disable">Disable</option>
                                        </select>
                                    </div>
                                    <%}%>                                    
                                </div>
                                <div class="form-group">                            
                                    <label class="col-lg-3 control-label">Reactivation Charge</label>
                                    <%if (packageObj.getReActivationCharge() > 0) {%>
                                    <div class=col-sm-2>
                                        <select id="ServiceCharge" disabled name="ServiceCharge" class="form-control">
                                            <option selected value="Enable">Enable</option>
                                            <option value="Disable">Disable</option>
                                        </select>
                                    </div>
                                    <div class=col-sm-2>
                                        <input type="text" id="serviceChargeRate" disabled name="serviceChargeRate" value="<%=packageObj.getReActivationCharge()%>" class="form-control" placeholder="Price">
                                    </div>
                                    <%} else {%>
                                    <div class=col-sm-2>
                                        <select id="ServiceCharge" disabled name="ServiceCharge" class="form-control">
                                            <option  value="Enable">Enable</option>
                                            <option selected value="Disable">Disable</option>
                                        </select>
                                    </div>
                                    <%}%>      
                                </div>
                                <div class="form-group">                            
                                    <label class="col-lg-3 control-label">Cancellation Charge</label>
                                    <%if (packageObj.getCancellationRate() > 0) {%>
                                    <div class=col-sm-2>
                                        <select id="ServiceCharge" disabled name="ServiceCharge" class="form-control">
                                            <option selected value="Enable">Enable</option>
                                            <option value="Disable">Disable</option>
                                        </select>
                                    </div>
                                    <div class=col-sm-2>
                                        <input type="text" id="serviceChargeRate" disabled name="serviceChargeRate" value="<%=packageObj.getCancellationRate()%>" class="form-control" placeholder="Price">
                                    </div>
                                    <%} else {%>
                                    <div class=col-sm-2>
                                        <select id="ServiceCharge" disabled name="ServiceCharge" class="form-control">
                                            <option  value="Enable">Enable</option>
                                            <option selected value="Disable">Disable</option>
                                        </select>
                                    </div>
                                    <%}%>      
                                </div>

                                <%if (packageObj.getLatePenaltyRate() != null && !packageObj.getLatePenaltyRate().equals("")) {
                                        String latePenalites = packageObj.getLatePenaltyRate();
                                        String startD1 = "0";
                                        String endD1 = "0";
                                        String penalities1 = "0";
                                        String startD2 = "0";
                                        String endD2 = "0";
                                        String penalities2 = "0";
                                        //String stRate  = "0";
                                        if (latePenalites != null) {
                                            JSONObject latePenalitesJson = new JSONObject(latePenalites);
                                            startD1 = latePenalitesJson.getString("latePenaltyChargeStartDay1");
                                            endD1 = latePenalitesJson.getString("latePenaltyChargeEndDay1");
                                            penalities1 = latePenalitesJson.getString("latePenaltyChargeRate1");
                                            startD2 = latePenalitesJson.getString("latePenaltyChargeStartDay2");
                                            endD2 = latePenalitesJson.getString("latePenaltyChargeEndDay2");
                                            penalities2 = latePenalitesJson.getString("latePenaltyChargeRate2");
                                        }
                                        if (Float.parseFloat(startD1) > 0 && Float.parseFloat(endD1) > 0 && Float.parseFloat(penalities1) > 0
                                                && Float.parseFloat(penalities2) > 0 && Float.parseFloat(endD2) > 0 && Float.parseFloat(startD2) > 0) {
                                %>
                                <div class="form-group">                            
                                    <label class="col-lg-3 control-label">Late Penalty Charge</label>
                                    <div class=col-sm-2>
                                        <select id="ServiceCharge" disabled name="ServiceCharge" class="form-control">
                                            <option selected value="Enable">Enable</option>
                                            <option value="Disable">Disable</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3"></div>
                                    <div class=col-sm-2>
                                        <div class="input-group">
                                            <span class="input-group-addon">From day</span>
                                            <input type="text" id="serviceChargeRate" disabled name="serviceChargeRate" value="<%=startD1%>" class="form-control" placeholder="Price">
                                        </div>
                                    </div>
                                    <div class=col-sm-2>
                                        <div class="input-group">
                                            <span class="input-group-addon">To day</span>
                                            <input type="text" id="serviceChargeRate" disabled name="serviceChargeRate" value="<%=endD1%>" class="form-control" placeholder="Price">
                                        </div>
                                    </div>
                                    <div class=col-sm-2>
                                        <div class="input-group">
                                            <span class="input-group-addon">Price</span>
                                            <input type="text" id="serviceChargeRate" disabled name="serviceChargeRate" value="<%=penalities1%>" class="form-control" placeholder="Price">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3"></div>
                                    <div class=col-sm-2>
                                        <div class="input-group">
                                            <span class="input-group-addon">From day</span>
                                            <input type="text" id="serviceChargeRate" disabled name="serviceChargeRate" value="<%=startD2%>" class="form-control" placeholder="Price">
                                        </div>
                                    </div>
                                    <div class=col-sm-2>
                                        <div class="input-group">
                                            <span class="input-group-addon">To day</span>
                                            <input type="text" id="serviceChargeRate" disabled name="serviceChargeRate" value="<%=endD2%>" class="form-control" placeholder="Price">
                                        </div>
                                    </div>
                                    <div class=col-sm-2>
                                        <div class="input-group">
                                            <span class="input-group-addon">Price</span>
                                            <input type="text" id="serviceChargeRate" disabled name="serviceChargeRate" value="<%=penalities2%>" class="form-control" placeholder="Price">
                                        </div>
                                    </div>
                                </div>
                                <%} else {%>
                                <div class="form-group">                            
                                    <label class="col-lg-3 control-label">Late Penalty Charge</label>
                                    <div class=col-sm-2>
                                        <select id="ServiceCharge" disabled name="ServiceCharge" class="form-control">
                                            <option  value="Enable">Enable</option>
                                            <option selected value="Disable">Disable</option>
                                        </select>
                                    </div>
                                </div>
                                <%}
                                } else {%>
                                <div class="form-group">                            
                                    <label class="col-lg-3 control-label">Late Penalty Charge</label>
                                    <div class=col-sm-2>
                                        <select id="ServiceCharge" disabled name="ServiceCharge" class="form-control">
                                            <option  value="Enable">Enable</option>
                                            <option selected value="Disable">Disable</option>
                                        </select>
                                    </div>
                                </div>
                                <%}%>      

                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Taxes</label>
                                    <% String taxRate = packageObj.getTax();
                                        String gstRate = "0";
                                        String vatRate = "0";
                                        String stRate = "0";
                                        if (taxRate != null) {
                                            JSONObject taxJson = new JSONObject(taxRate);
                                            gstRate = taxJson.getString("gstTax");
                                            vatRate = taxJson.getString("vatTax");
                                            stRate = taxJson.getString("stTax");
                                        }
                                    %>
                                    <div class="col-lg-2">                                       
                                        <div class="input-group">
                                            <span class="input-group-addon">GST</span>
                                            <input type="text" class="form-control" disabled id="gstRate" name="gstRate" value=<%=gstRate%> % placeholder="GST in %">                                    
                                        </div>
                                    </div>
                                    <%if(vatTax != null && vatTax.equalsIgnoreCase("true")){%>
                                    <div class="col-lg-2">
                                        <div class="input-group">
                                            <span class="input-group-addon">VAT</span>
                                            <input type="text" class="form-control" disabled id="vatRate" name="vatRate" value=<%=vatRate%> % placeholder="VAT in %">                                    
                                        </div>
                                    </div>
                                    <%}if(serviceTax != null && serviceTax.equalsIgnoreCase("true")){%>
                                    <div class="col-lg-2">
                                        <div class="input-group">
                                            <span class="input-group-addon">ST</span>
                                            <input type="text" class="form-control" disabled id="stRate" name="stRate" value=<%=stRate%> % placeholder="ST in %">                                    
                                        </div>
                                    </div>
                                    <%}%>     
                                </div>
                                <a  class="btn btn-success btn-xs" style="margin-left: 26%" onclick="showAPRate()">Next <i class="fa fa-forward"></i> </a>
                            </form>
                        </div>
                        <!-- Rate tab end -->

                        <!-- Tab for AP-->
                        <div  class="tab-pane fade" id="apRate">
                            <form class="form-horizontal" id="edit_ap_form" name="edit_ap_form" role="form">
                                <input type="hidden" name="_newPackageName" id="_newPackageName" value="<%=packageName%>">
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Access Point</label>                                   
                                    <div class="col-lg-3">
                                        <select id="_Accesspoint12" name="_Accesspoint12"  class="form-control" onchange="acesspointResource(this.value, '<%=packageName%>')">
                                            <option value="select">Select Accesspoint</option>
                                            <%
                                                String apRate = packageObj.getApRateDetails();
                                                JSONArray jsOld = new JSONArray(apRate);
                                                String keyS = "";
                                                for (int j = 0; j < jsOld.length(); j++) {
                                                    JSONObject jsonexists1 = jsOld.getJSONObject(j);
                                                    Iterator<String> keys = jsonexists1.keys();
                                                    while (keys.hasNext()) {
                                                        String keyData = keys.next();
                                                        String[] keyDetails = keyData.split(":");
                                                        if (!keyS.contains(keyDetails[0] + ",")) {
                                                            keyS += keyDetails[0] + ",";

                                            %>
                                            <option value="<%=keyDetails[0]%>"><%=keyDetails[0]%></option>
                                            <%}
                                                    }
                                                }%>
                                        </select>
                                    </div>

                                    <div class="col-lg-3">                                    
                                        <select id="_ResourceForAPPricing2" name="_ResourceForAPPricing2"  class="form-control span2" onchange="showAPVersion(this.value, '<%=packageName%>')" style="width: 100%">
                                            <option value="-1">Select Resource</option>
                                        </select>                                    
                                    </div>
                                    <div class="col-lg-3">
                                        <select id="_VersionForAPricing2" name="_VersionForAPricing2"  class="form-control span2" onchange="accesspointPrice('<%=packageName%>')" style="width: 100%">
                                            <option value="-1">Select Version</option>
                                        </select>
                                    </div>                                
                                </div>                                                                  
                                <div id="listOfAPI">                                            
                                </div>
                                <a  class="btn btn-success btn-xs"  onclick="showRate()" style="margin-left: 17%"><i class="fa fa-backward"></i> Back</a>
                                <a  class="btn btn-success btn-xs"  onclick="showFlatPrice()"> Next <i class="fa fa-forward"></i></a>
                            </form>
                        </div>
                                <!-- Tab for flat price -->
                    <div  class="tab-pane fade" id="flatRate">
                        <div class="form-group">                            
                            <label class="col-sm-2 control-label">Flat price setting</label>                                  
                            <div class=col-sm-2>                                   
                                <%if(packageObj.getFlatPrice()!= null){%>                                
                                <select id="flatPriceSetting" name="flatPriceSetting" class="form-control" disabled>                                    
                                    <option  value="Disable" >Disable</option>
                                    <option selected value="Enable" >Enable</option>
                                </select>                                        
                                <%}else{%>
                                <select id="flatPriceSetting" name="flatPriceSetting" class="form-control" disabled> 
                                    <option selected value="Disable" >Disable</option>
                                    <option value="Enable" >Enable</option>
                                </select>     
                                <%}%>                                
                            </div>                                    
                        </div>
                            <br><br><br>
                        <form class="form-horizontal" id="edit_flatPrice_form" name="edit_flatPrice_form" role="form">      
                        <%if(packageObj.getFlatPrice()!= null){%>                              
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Flat price</label>                                   
                                <div class="col-lg-2">
                                    <select id="_accesspointFlatPrice" name="_accesspointFlatPrice" class="form-control" onchange="flatPriceResource(this.value, '<%=packageName%>')">
                                        <option value="-1">Select Accesspoint</option>
                                        <%
                                            String flatPrice = packageObj.getFlatPrice();
                                            JSONArray jsapiThrottling= new JSONArray(flatPrice);
                                            String keyThrottling = "";
                                            for (int j = 0; j < jsapiThrottling.length(); j++) {
                                                JSONObject jsonexists1 = jsapiThrottling.getJSONObject(j);
                                                Iterator<String> keys = jsonexists1.keys();
                                                while (keys.hasNext()) {
                                                    String keyData = keys.next();
                                                    String[] keyDetails = keyData.split(":");
                                                    if (!keyThrottling.contains(keyDetails[0] + ",")) {
                                                        keyThrottling += keyDetails[0] + ",";

                                        %>
                                        <option value="<%=keyDetails[0]%>"><%=keyDetails[0]%></option>
                                        <%}
                                                }
                                            }%>
                                    </select>
                                </div>
                                <div class="col-lg-2">                                    
                                    <select id="_resourceFlatPrice" name="_resourceFlatPrice"  class="form-control span2" onchange="flatVersion(this.value, '<%=packageName%>')" style="width: 100%">
                                        <option value="-1">Select Resource</option>
                                    </select>                                    
                                </div>
                                <div class="col-lg-2">
                                    <select id="_versionFlatPrice" name="_versionFlatPrice"  class="form-control span2" onchange="flatPriceDetails('<%=packageName%>')" style="width: 100%">
                                        <option value="-1">Select Version</option>
                                    </select>
                                </div>
                                <div id="listflatPriceDetails">                                            
                                </div>        
                            </div>                                                                                              
                        <%}%>                  
                            <a  class="btn btn-success btn-xs"  onclick="showAPRate()" style="margin-left: 18%"><i class="fa fa-backward"></i> Back</a>
                            <a  class="btn btn-success btn-xs"  onclick="showAPIThrottling()"> Next <i class="fa fa-forward"></i></a>
                        </form>                                        
                    </div>
                    <!-- End of Flat price tab -->                        
                        <!-- Tab for throttling -->
                    <div  class="tab-pane fade" id="apiThrottling">
                        <div class="form-group">                            
                            <label class="col-lg-2 control-label">Throttling setting</label>                                  
                            <div class=col-sm-2>                                   
                                <%if(packageObj.getApiThrottling()!= null){%>                                
                                <select id="throttlingSetting" name="throttlingSetting" class="form-control" disabled>                                    
                                    <option  value="Disable" >Disable</option>
                                    <option selected value="Enable" >Enable</option>
                                </select>                                        
                                <%}else{%>
                                <select id="throttlingSetting" name="throttlingSetting" class="form-control" disabled> 
                                    <option selected value="Disable" >Disable</option>
                                    <option value="Enable" >Enable</option>
                                </select>     
                                <%}%>                                
                            </div>                                    
                        </div>
                            <br><br><br>
                        <form class="form-horizontal" id="edit_ap_form" name="edit_ap_form" role="form">      
                        <%if(packageObj.getApiThrottling()!= null){%>                              
                            <div class="form-group">
                                <label class="col-lg-2 control-label">API throttling</label>                                   
                                <div class="col-lg-3">
                                    <select id="_throttlingAccesspoint" name="_throttlingAccesspoint" class="form-control" onchange="throttlingResource(this.value, '<%=packageName%>')">
                                        <option value="select">Select Accesspoint</option>
                                        <%
                                            String apithrottling = packageObj.getApiThrottling();
                                            JSONArray jsapiThrottling= new JSONArray(apithrottling);
                                            String keyThrottling = "";
                                            for (int j = 0; j < jsapiThrottling.length(); j++) {
                                                JSONObject jsonexists1 = jsapiThrottling.getJSONObject(j);
                                                Iterator<String> keys = jsonexists1.keys();
                                                while (keys.hasNext()) {
                                                    String keyData = keys.next();
                                                    String[] keyDetails = keyData.split(":");
                                                    if (!keyThrottling.contains(keyDetails[0] + ",")) {
                                                        keyThrottling += keyDetails[0] + ",";

                                        %>
                                        <option value="<%=keyDetails[0]%>"><%=keyDetails[0]%></option>
                                        <%}
                                                }
                                            }%>
                                    </select>
                                </div>

                                <div class="col-lg-3">                                    
                                    <select id="_resourceAPIThrottling" name="_resourceAPIThrottling"  class="form-control span2" onchange="throttlingVersion(this.value, '<%=packageName%>')" style="width: 100%">
                                        <option value="-1">Select Resource</option>
                                    </select>                                    
                                </div>
                                <div class="col-lg-3">
                                    <select id="_versionofAPIThrottling" name="_versionofAPIThrottling"  class="form-control span2" onchange="throttlingVersion('<%=packageName%>')" style="width: 100%">
                                        <option value="-1">Select Version</option>
                                    </select>
                                </div>                                
                            </div>                                                                  
                            <div id="listOfthrottlingAPI">                                            
                            </div>
                        <%}%>                  
                            <a  class="btn btn-success btn-xs"  onclick="showFlatPrice()" style="margin-left: 18%"><i class="fa fa-backward"></i> Back</a>
                            <a  class="btn btn-success btn-xs"  onclick="showSlabPrice()"> Next <i class="fa fa-forward"></i></a>
                        </form>                                        
                    </div>                    
                        <!-- Tab for Promo Code -->
                        <div  class="tab-pane fade" id="promoCode">
                            <form class="form-horizontal" id="edit_sp_form" name="edit_sp_form" role="form">
                                <% if (packageObj.getSlabApRateDetails() != null && !packageObj.getSlabApRateDetails().equals("")) {%>
                                <div class="form-group">                            
                                    <label class="col-lg-2 control-label"> Slab Pricing</label>                                  
                                    <div class=col-sm-2>
                                        <select id="SlabPricing" disabled name="SlabPricing" class="form-control " onchange="slabPriceDiv(this.value)">
                                            <option  selected value="Enable">Enable</option>
                                        </select>                               
                                    </div>                                    
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" style="">Access Point</label>                                   
                                    <input type="hidden" name="_slabPackageName" id="_slabPackageName" value="<%=packageName%>">
                                    <div class="col-lg-2">
                                        <select id="_Accesspoint2" name="_Accesspoint2"  class="form-control span2" onchange="acesspointChangeResource(this.value)" style="width: 100%">
                                            <option value="-1" selected>Select AP</option>
                                            <%
                                                String apSlabRate = packageObj.getSlabApRateDetails();
                                                JSONArray jsSlabOld = new JSONArray();
                                                if (apSlabRate != null) {
                                                    jsSlabOld = new JSONArray(apSlabRate);
                                                }
                                                String slabAccesspoint = "";
                                                for (int k = 0; k < jsSlabOld.length(); k++) {
                                                    JSONObject jsonexistsObj = jsSlabOld.getJSONObject(k);
                                                    Iterator<String> keys = jsonexistsObj.keys();
                                                    while (keys.hasNext()) {
                                                        String keyData = keys.next();
                                                        String[] keyDetails = keyData.split(":");
                                                        if (slabAccesspoint.isEmpty()) {
                                                            slabAccesspoint += keyDetails[0] + ",";
                                                        } else if (slabAccesspoint.contains(keyDetails[0])) {
                                                            continue;
                                                        } else {
                                                            slabAccesspoint += keyDetails[0] + ",";
                                                        }
                                                    }
                                                }
                                                if (!slabAccesspoint.equals("")) {
                                                    String[] accesspointDetails = slabAccesspoint.split(",");
                                                    if (accesspointDetails != null) {
                                                        for (int i = 0; i < accesspointDetails.length; i++) {
                                            %>
                                            <option value="<%=accesspointDetails[i]%>"><%=accesspointDetails[i]%></option>
                                            <%
                                                        }
                                                    }
                                                }
                                            %>
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <select id="_ResourceForSlabPricing2" name="_ResourceForSlabPricing2"  class="form-control span2" onchange="showSlabVersion(this.value)" style="width: 100%">
                                            <option value="-1" selected>Select Resource</option>                                                                                  
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <select id="_VersionForSlabPricing2" name="_VersionForSlabPricing2"  class="form-control span2" onchange="showSlabAPI(this.value)" style="width: 100%">
                                            <option value="-1" selected>Select Version</option>                                                                                  
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <select id="_APIForSlabPricing2" name="_APIForSlabPricing2"  class="form-control span2" onchange="showSlabPricing(this.value)" style="width: 100%">
                                            <option value="-1" selected>Select API</option>                                                                                  
                                        </select>
                                    </div>                                        
                                </div>
                                <%} else {%>
                                <div class="form-group">                            
                                    <label class="col-lg-2 control-label"> Slab Pricing</label>                                  
                                    <div class=col-sm-2>                                  
                                        <select id="SlabPricing" disabled name="SlabPricing"  class="form-control" onchange="slabPriceDiv(this.value)">
                                            <option selected value="Disable">Disable</option>
                                        </select>
                                    </div>                                    
                                </div>
                                <%}%>                                                                                    
                                <div id="_slabPriceWindow">                                            
                                </div>                                       
                                <a  class="btn btn-success btn-xs" Style="margin-left: 17%" onclick="showAPIThrottling()"><i class="fa fa-backward"></i> Back</a>
                                <a  class="btn btn-success btn-xs" onclick="showTierPrice()"> Next <i class="fa fa-forward"></i></a>
                            </form>
                        </div>
                        <div  class="tab-pane fade" id="tieringPrice">
                            <form class="form-horizontal" id="edit_sp_form" name="edit_sp_form" role="form">
                                <input type="hidden" id="packageName" name="packageName" value="<%=packageObj.getBucketName()%>">
                                <input type="hidden" id="packageID" name="packageID" value="<%=packageObj.getBucketId()%>">
                                <% if (packageObj.getTierRateDetails() != null && !packageObj.getTierRateDetails().equals("")) {%>
                                <div class="form-group">                            
                                    <label class="col-lg-2 control-label">Tier Pricing</label>                                  
                                    <div class=col-sm-2>
                                        <select id="SlabPricing" disabled name="SlabPricing" class="form-control " onchange="tieringPriceDiv(this.value)">
                                            <option  selected value="Enable">Enable</option>
                                        </select>                               
                                    </div>                                    
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-2 control-label">Access Point</label>                                   
                                    <div class="col-lg-2">
                                        <select id="_Accesspoint3" name="_Accesspoint3"  class="form-control span2" onchange="acessChangeResForTiering(this.value, '<%=packageName%>')" style="width: 100%">
                                            <option value="-1" selected>Select AP</option>
                                            <%
                                                String tierRate = packageObj.getTierRateDetails();
                                                JSONArray jsSlabOld = new JSONArray();
                                                if (tierRate != null) {
                                                    jsSlabOld = new JSONArray(tierRate);
                                                }
                                                String tierAccesspoint = "";
                                                for (int k = 0; k < jsSlabOld.length(); k++) {
                                                    JSONObject jsonexistsObj = jsSlabOld.getJSONObject(k);
                                                    Iterator<String> keys = jsonexistsObj.keys();
                                                    while (keys.hasNext()) {
                                                        String keyData = keys.next();
                                                        String[] keyDetails = keyData.split(":");
                                                        if (tierAccesspoint.isEmpty()) {
                                                            tierAccesspoint += keyDetails[0] + ",";
                                                        } else if (tierAccesspoint.contains(keyDetails[0])) {
                                                            continue;
                                                        } else {
                                                            tierAccesspoint += keyDetails[0] + ",";
                                                        }
                                                    }
                                                }
                                                if (!tierAccesspoint.equals("")) {
                                                    String[] accesspointDetails = tierAccesspoint.split(",");
                                                    if (accesspointDetails != null) {
                                                        for (int i = 0; i < accesspointDetails.length; i++) {
                                            %>
                                            <option value="<%=accesspointDetails[0]%>"><%=accesspointDetails[0]%></option>
                                            <%  }
                                                    }
                                                }%>
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <select id="_ResourceForTieringPricing2" name="_ResourceForTieringPricing2"  class="form-control span2" onchange="showTieringVersion(this.value)" style="width: 100%">
                                            <option value="-1" selected>Select Resource</option>                                                                                  
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <select id="_VersionForTieringPricing2" name="_VersionForTieringPricing2"  class="form-control span2" onchange="showTieringAPI(this.value)" style="width: 100%">
                                            <option value="-1" selected>Select Version</option>                                                                                  
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <select id="_APIForTieringPricing2" name="_APIForTieringPricing2"  class="form-control span2" onchange="showTieringPricing(this.value)" style="width: 100%">
                                            <option value="-1" selected>Select API</option>                                                                                  
                                        </select>
                                    </div>                                        
                                </div>
                                <%} else {%>
                                <div class="form-group">                            
                                    <label class="col-lg-2 control-label">Tier Pricing</label>                                  
                                    <div class=col-sm-2>                                  
                                        <select id="SlabPricing" disabled name="SlabPricing"  class="form-control" onchange="slabPriceDiv(this.value)">
                                            <option selected value="Disable">Disable</option>
                                        </select>
                                    </div>                                    
                                </div>
                                <%}%>                                                                                    
                                <div id="_tieringPriceWindow">                                            
                                </div>                                       
                                <a  class="btn btn-success btn-xs" Style="margin-left: 17%" onclick="showSlabPrice()"><i class="fa fa-backward"></i> Back</a>
                                <a  class="btn btn-success btn-xs" onclick="showSecurityAndAlerts()"> Next <i class="fa fa-forward"></i></a>
                            </form>
                        </div>        
                        <div  class="tab-pane fade" id="alerts">
                            <%
                                String alertandsecurity = packageObj.getSecurityAndAlertDetails();
                                JSONObject reqJSONObj = null;
                                if (alertandsecurity != null) {
                                    JSONArray alertJson = new JSONArray(alertandsecurity);
                                    for (int i = 0; i < alertJson.length(); i++) {
                                        JSONObject jsonexists1 = alertJson.getJSONObject(i);
                                        if (jsonexists1.has(packageName)) {
                                            reqJSONObj = jsonexists1.getJSONObject(packageName);
                                            if (reqJSONObj != null) {
                                                break;
                                            }
                                        }
                                    }
                                }
                            %>
                            <label class="col-lg-12 control-label">In message where you want to add Developer name just put #DeveloperName, for date just put #dateTime,</label>
                            <form class="form-horizontal" id="edit_seandart_form" name="edit_seandart_form" role="form">
                                <div class="form-group">
                                    <input type="checkbox" class="col-lg-1" id="trialCheck">
                                    <label class="col-lg-2 control-label">Sign Up</label>
                                    <div class="col-lg-6">
                                        <div class=" input-group">
                                            <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>                                                                                                 
                                            <textarea class="form-control" id="signUpAlertMesssage" disabled name="signUpAlertMesssage" placeholder="Message to be send to Developer when he/she do sign up"><%=reqJSONObj.getString("signUpAlertMesssage")%></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" class="col-lg-1" id="trialCheck">
                                    <label class="col-lg-2 control-label">Trial Package</label>
                                    <div class="col-lg-6">
                                        <div class=" input-group">
                                            <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                            <textarea class="form-control" id="trialPackageAlertMesssage" disabled  name="trialPackageAlertMesssage" placeholder="Message to be send to Developer when trial package is going to expire"><%=reqJSONObj.getString("trialPackageAlertMesssage")%></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" class="col-lg-1" id="trialCheck">
                                    <label class="col-lg-2 control-label">Package changes</label>
                                    <div class="col-lg-6">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                            <textarea class="form-control" id="changeOnPackageAlertMesssage" disabled  name="changeOnPackageAlertMesssage" placeholder="Message to be send to Developer when change his/her package"><%=reqJSONObj.getString("changeOnPackageAlertMesssage")%></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" class="col-lg-1" id="trialCheck">
                                    <label class="col-lg-2 control-label">Late Penalties</label>
                                    <div class="col-lg-6">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                            <textarea class="form-control" id="latePenaltiesAlertMesssage" disabled  name="latePenaltiesAlertMesssage" placeholder="Message to be send to Developer when late penalties charged"><%=reqJSONObj.getString("latePenaltiesAlertMesssage")%></textarea>
                                        </div>
                                    </div>
                                </div>                                
                                <div class="form-group">
                                    <input type="checkbox" class="col-lg-1" id="trialCheck">
                                    <label class="col-lg-2 control-label">Service Reactivation</label>
                                    <div class="col-lg-6">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                            <textarea class="form-control" id="reActivationAlertMesssage" disabled  name="reActivationAlertMesssage" placeholder="Message to be send to Developer when he/she reactivate the service"><%=reqJSONObj.getString("reActivationAlertMesssage")%></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" class="col-lg-1" id="trialCheck">
                                    <label class="col-lg-2 control-label">Low balance</label>
                                    <div class="col-lg-6">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                            <textarea class="form-control" id="lowBalanceMesssage" disabled  name="lowBalanceMesssage"  placeholder="Message to be send to Developer when Developer have low balance"><%=reqJSONObj.getString("lowBalanceMesssage")%></textarea>
                                        </div>
                                    </div>
                                </div>                                
                                <% if (reqJSONObj.getString("pdfSigning").equals("enable")) {%>
                                <input type="checkbox" class="" id="pdfSigningCheck" disabled name="pdfSigningCheck"  value="pdfSigningEnable" style="margin: 2%" checked>
                                <%} else {%>
                                <input type="checkbox" class="" id="pdfSigningCheck" disabled name="pdfSigningCheck"  value="pdfSigningEnable" style="margin: 2%">
                                <%}%>
                                <label class=" control-label" style="margin-right: 1%">PDF Signing</label>

                                <% if (reqJSONObj.getString("encryptedPDF").equals("enable")) {%>
                                <input type="checkbox" class="" id="encryptedPDFCheck" disabled name="encryptedPDFCheck" value="encryptedPDFEnable" style="margin: 2%" checked>
                                <%} else {%>
                                <input type="checkbox" class="" id="encryptedPDFCheck" disabled name="encryptedPDFCheck" value="encryptedPDFEnable" style="margin: 2%">
                                <%}%>    
                                <label class=" control-label">Encrypted PDF</label><br><br>
                                <a  class="btn btn-success btn-xs" Style="margin-left: 26%" onclick="showTierPrice()"><i class="fa fa-backward"></i> Back</a>
                                <%if(featureFlag){%>
                                <a  class="btn btn-success btn-xs" onclick="showfeature()"> Next <i class="fa fa-forward"></i></a>
                                <%}else{%>
                                <a  class="btn btn-danger btn-xs" onclick="rejectPackagemodal('<%=GlobalStatus.REJECTED%>', '<%=packageName%>')"><i class="fa fa-close"></i> Reject</a> 
                                <a  class="btn btn-success btn-xs" onclick="changePackageStatus('<%=GlobalStatus.APPROVED%>', '<%=packageName%>')"><i class="fa fa-check-circle"></i> Approve</a> 
                                <%}%>
                            </form>
                        </div>
                        <%if(featureFlag){%>        
                        <div  class="tab-pane fade" id="feature">
                            <%
                                String feature = packageObj.getFeatureList();
                                JSONObject featJSONObj = null;
                                String promoCodeFlag = "";
                                String loanLimit = "0";
                                String interestRate = "0";
                                String reedmePrice = "0";
                                String reedmePoint = "0";
                                if (feature != null) {
                                    JSONArray alertJson = new JSONArray(feature);
                                    for (int i = 0; i < alertJson.length(); i++) {
                                        JSONObject jsonexists1 = alertJson.getJSONObject(i);
                                        if (jsonexists1.has(packageName)) {
                                            featJSONObj = jsonexists1.getJSONObject(packageName);
                                            if (featJSONObj != null) {
                                                if (!promoCodeFlag.equals("Disable")) {
                                                    loanLimit = featJSONObj.getString("loanLimit");
                                                    interestRate = featJSONObj.getString("interestRate");
                                                    reedmePoint = featJSONObj.getString("reedmePoint");
                                                    reedmePrice = featJSONObj.getString("reedmePrice");
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                            %>
                            <form class="form-horizontal" id="edit_feature_form" name="edit_feature_form" role="form">
                                <%if(promocodeEnable.equalsIgnoreCase("true")){%>
                                <div class="form-group">                            
                                    <label class="col-lg-3 control-label">Promo Code</label>                                  
                                    <div class=col-sm-2>
                                        <select id="promoCodeFlag"  disabled name="promoCodeFlag" class="form-control" onchange="">
                                            <% if (featJSONObj.getString("promoCodeFlag").equals("Enable")) {%>
                                            <option selected value="Enable">Enable</option>
                                            <%} else {%>
                                            <option selected value="Disable">Disable</option>
                                            <%}%>
                                        </select>
                                    </div>                                    
                                </div>
                                <%} if(loanEnable!= null && loanEnable.equalsIgnoreCase("true")){%>        
                                <div class="form-group">
                                    <label class=" control-label col-lg-3">Loan with additional Charge</label>
                                    <div class="col-lg-2">
                                        <select id="loanAddChar" disabled name="loanAddChar" class="form-control" onchange="loanPriceDivvv(this.value)">
                                            <% if (Float.parseFloat(loanLimit) > 0 && Float.parseFloat(interestRate) > 0) {%>
                                            <option selected value="Enable">Enable</option>
                                            <%} else {%>
                                            <option selected value="Disable">Disable</option>
                                            <%}%>
                                        </select>
                                    </div>
                                    <% if (Float.parseFloat(loanLimit) > 0 && Float.parseFloat(interestRate) > 0) {%>
                                    <div class="col-lg-2">
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control" id="loanLimit" disabled name="loanLimit" value="<%=featJSONObj.getString("loanLimit")%>" placeholder="Loan limit">                                    
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="input-group">
                                            <span class="input-group-addon">%</span>
                                            <input type="text" class="form-control" id="interestRate" disabled name="interestRate" value="<%=featJSONObj.getString("interestRate")%>" placeholder="Interest in %">                                    
                                        </div>
                                    </div>
                                    <%}%>
                                </div>
                                <%}%>
                                <div class="form-group">
<!--                                    <label class=" control-label col-lg-3 ">Redeem Credit Point</label>
                                    <div class="col-lg-2">
                                        <select id="reedmeCredit" disabled name="reedmeCredit" class="form-control" onchange="creditPointDivvvv(this.value)">
                                            <% if (!featJSONObj.isNull("reedmePrice")) {%>
                                            <option selected value="Enable">Enable</option>
                                            <%} else {%>
                                            <option selected value="Disable">Disable</option>
                                            <%}%>
                                        </select>
                                    </div>
                                    <% if (!featJSONObj.isNull("reedmePoint")) {%>
                                    <div class="col-lg-2">
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" id="reedmePrice" disabled name="reedmePrice" value="<%=featJSONObj.getString("reedmePoint")%>" class="form-control" placeholder="Price">                                    
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="input-group">
                                            <span class="input-group-addon">%</span>
                                            <input type="text" id="reedmePoint" disabled name="reedmePoint" value="<%=featJSONObj.getString("reedmePrice")%>" class="form-control" placeholder="Point">                                    
                                        </div>
                                    </div>-->
                                    <%}%>
                                </div>
                                <a  class="btn btn-success btn-xs" onclick="showSecurityAndAlerts()" Style="margin-left: 26%" ><i class="fa fa-backward"></i> Back</a> 
                                <a  class="btn btn-danger btn-xs" onclick="rejectPackagemodal('<%=GlobalStatus.REJECTED%>', '<%=packageName%>')"><i class="fa fa-close"></i> Reject</a> 
                                <a  class="btn btn-success btn-xs" onclick="changePackageStatus('<%=GlobalStatus.APPROVED%>', '<%=packageName%>')"><i class="fa fa-check-circle"></i> Approve</a> 
                            </form>
                        </div>
                        <%}%>    
                    </div><br><br>
                </div>                                                    
            </div>                                           
        </div>
    </div>
    
    <div id="rejectPackage" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="rejectPackageModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                        <b id="rejectPackageModal">Reject package</b>
                    </div>          
                    <div class="modal-body">
                        <div class="row-fluid">
                            <form class="form-horizontal" id="rejectPackageForm">
                                <fieldset>
                                    <div class="control-group">
                                        <label class="control-label col-lg-2"  for="partnername">Reason</label>
                                        <div class="controls col-lg-10">
                                            <input type="hidden"  id="_packageName" name="_packageName" >
                                            <input type="hidden"  id="_packagestatus" name="_packagestatus" >
                                            <select id="reason" name="reason" class="form-control" >
                                                <%=packageRejectionoptions%>        
                                            </select>
                                        </div>
                                    </div>                            
                                </fieldset>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div id="edit-partner-result"></div>
                        <button class="btn btn-info btn-xs" data-dismiss="modal">Close</button>
                        <button class="btn btn-success btn-xs" onclick="rejectPackageRequest()" id="addPartnerButtonE">Reject request</button>
                    </div>
                </div>
            </div>
        </div>
</div>
</div>
<script>
    $(document).ready(function () {
        $("#visible").select2();
    });
</script>                            
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<script src="./select2/select2.js"></script>
<!-- DataTables JavaScript -->
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>
<script src="js/bootbox.min.js" type="text/javascript"></script>  
<script src="js/bootbox.min(1).js" type="text/javascript"></script>
</body>
</html>                
<%@include file="footer.jsp" %>
