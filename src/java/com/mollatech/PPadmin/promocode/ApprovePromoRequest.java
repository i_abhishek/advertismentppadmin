package com.mollatech.PPadmin.promocode;

import com.mollatech.serviceguard.nucleus.db.SgPromocode;
import com.mollatech.serviceguard.nucleus.db.connector.management.PromocodeManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "ApprovePromoRequest", urlPatterns = {"/ApprovePromoRequest"})
public class ApprovePromoRequest extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = " Promocode request approved sucessfully!!!";
        PrintWriter out = response.getWriter();
        String promoCodeId = request.getParameter("_promoId");
        String promoCodeStatus = request.getParameter("_promoStatus");
        String promocodeReqFlag = request.getParameter("_promoRequestStatus");
        int ireqStatus = 0;
        int ipromoId = 0;
        int ipromoStatus = 0;
        if (promocodeReqFlag != null) {
            ireqStatus = Integer.parseInt(promocodeReqFlag);
        }
        if (promoCodeId != null) {
            ipromoId = Integer.parseInt(promoCodeId);
        }
        if (promoCodeStatus != null) {
            ipromoStatus = Integer.parseInt(promoCodeStatus);
        }
        int a = -1;
        PromocodeManagement pm = new PromocodeManagement();
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        SgPromocode promoObj = pm.getPromocodeById(SessionId, channelId, ipromoId);
        if (promoObj != null) {
            Calendar calendarObj = Calendar.getInstance();
            calendarObj.setTime(new Date());
            calendarObj.add(Calendar.DATE, promoObj.getExpiryInNoOfDays());
            a = pm.activePromoCode(SessionId, channelId, ipromoId, ipromoStatus, ireqStatus, calendarObj.getTime());
            if (a == 0) {
                result = "success";
                message = "Promocode request approved sucessfully !!!";
            } else if (a == 3) {
                result = "error";
                message = "Promocode request already approved !!!";
            } else {
                result = "error";
                message = "Promocode request failed to approved !!!";
            }
        } else {
            result = "error";
            message = "Promo code details failed to load";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
