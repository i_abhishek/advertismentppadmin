/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.cdr;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.SgCdrData;
import com.mollatech.serviceguard.nucleus.db.connector.management.CDRManagement;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Ashu
 */
@WebServlet(name = "AddCDRInDB", urlPatterns = {"/AddCDRInDB"})
public class AddCDRInDB extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String filename = (String) request.getSession().getAttribute("cdrPath");
        Workbook workbook = null;
        JSONObject object = new JSONObject();
        DataFormatter objDefaultFormat = new DataFormatter();
        FormulaEvaluator objFormulaEvaluator = null;
        InputStream excelFile = new FileInputStream(new File(filename));
        if (filename.endsWith("xlsx")) {
            workbook = new XSSFWorkbook(excelFile);
            objFormulaEvaluator = new XSSFFormulaEvaluator((XSSFWorkbook) workbook);

        } else if (filename.endsWith("xls")) {
            workbook = new HSSFWorkbook(excelFile);
            objFormulaEvaluator = new HSSFFormulaEvaluator((HSSFWorkbook) workbook);
        }
        Sheet datatypeSheet = workbook.getSheetAt(0);
        Iterator<Row> iterator = datatypeSheet.iterator();
        int rowCount = 0;
        DateFormat startDate = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat startTime = new SimpleDateFormat("hh:mm:ss");
        DateFormat FinalDate = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        JSONArray array = new JSONArray();
        try {
            while (iterator.hasNext()) {
                JSONObject json = new JSONObject();
                if (rowCount != 0) {
                    Row currentRow = iterator.next();
                    Iterator<Cell> cellIterator = currentRow.iterator();
                    int colCount = 0;
                    String startDateString = "";
                    String startTimeString = "";
                    String finalString = "";
                    while (cellIterator.hasNext()) {
                        Cell currentCell = cellIterator.next();
                        switch (colCount) {
                            case 0:
                                startDateString = startDate.format(currentCell.getDateCellValue());
                                break;
                            case 3:
                                objFormulaEvaluator.evaluate(currentCell);
                                String cellValueStr = objDefaultFormat.formatCellValue(currentCell, objFormulaEvaluator);
                                json.put("Calling Party", cellValueStr.replaceAll("\\s+", ""));

                                break;
                            case 5:
                                startTimeString = startTime.format(currentCell.getDateCellValue());
                                break;
                            case 15:
                                json.put("RoundUp", currentCell.getNumericCellValue());
                                break;
                        }
                        colCount++;
                    }
                    finalString = FinalDate.format(FinalDate.parse(startDateString + " " + startTimeString));
                    json.put("finalString", finalString);
                    array.put(json);
                } else {
                    rowCount++;
                    iterator.next();
                }
            }
            int resId = Integer.parseInt(request.getParameter("resId"));
            SgCdrData data = new SgCdrData();
            data.setCdr(array.toString());
            data.setCreatedOn(new Date());
            data.setResourceId(resId);
            data.setStatus(GlobalStatus.NOT_PROCESSED);
            data.setUpdatedOn(new Date());
            int result = new CDRManagement().addDetails(data);
            if (result == 0) {
                object.put("_result", "success");
                object.put("_message", "CDR Added Successfully.");
            } else {
                object.put("_result", "error");
                object.put("_message", "Please upload proper file for CDR.");
            }
        } catch (Exception ex) {
            object.put("_result", "error");
//            object.put("_message", ex.getMessage());
            object.put("_message", "Please upload proper file for CDR.");

        } finally {
            PrintWriter out = response.getWriter();
            out.print(object);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
