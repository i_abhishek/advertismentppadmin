<%@page import="com.mollatech.serviceguard.nucleus.db.Warfiles"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MethodName"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Classes"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Methods"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <div class="control-group">
        <label class="control-label"  for="username"></label>
        <div class="controls">
            <select id="_APIForSlabPricing2" name="_APIForSlabPricing2"  class="form-control span2" onchange="showSlabPricing(this.value)" style="width: 100%">
                <%
                    AccessPointManagement ppw = new AccessPointManagement();
                    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
                    String channelId = (String) request.getSession().getAttribute("_channelId");
                    String apNameParam = request.getParameter("_apId");
                    String versionData = request.getParameter("versionData");
                    String _resourceId = request.getParameter("_resourceId");
                    //int acpId = Integer.parseInt(apId);
                    if (apNameParam != null && !apNameParam.equals("-1")) {
                        //Accesspoint accesspoint = ppw.getAccessPointById(SessionId, channelId, acpId);
                        Accesspoint accesspoint = ppw.getAccessPointByName(SessionId, channelId, apNameParam);
                    //int resId = Integer.parseInt(_resourceId);
                        //new change
                        String apName = accesspoint.getName();
                        Map classMap = null;
                        Map methodMap = null;
                        String value = apName;
                        //int resourceID = -1;
                        ResourceDetails rs= new ResourceManagement().getResourceByName(SessionId, channelId, _resourceId);
//                        if(rs != null){
//                            resourceID = rs.getResourceId();
//                        };
                        TransformDetails tf = new TransformManagement().getTransformDetails(SessionId, channelId, accesspoint.getApId(), rs.getResourceId());
                        Warfiles warfiles = new WarFileManagement().getWarFile(SessionId, channelId, accesspoint.getApId());
                        int version = 1;
                        if (warfiles != null) {
                            Map map = (Map) Serializer.deserialize(warfiles.getWfile());
                            version = map.size() / 2;
                        }
                        // new change
                        byte[] meBy = tf.getMethods();
                        Object obj = Serializer.deserialize(meBy);
                        Map tmpMethods = (Map) obj;
                        Methods methods = (Methods) tmpMethods.get("" + versionData);
                        //new change end
                        byte[] me = tf.getMethods();
                        byte[] cl = tf.getClasses();
                        //Methods methods = (Methods) Serializer.deserialize(me);
//                        Classes classes = (Classes) Serializer.deserialize(cl);
//                        if (classMap == null) {
//                            classMap = new HashMap();
//                        }
                        if (methodMap == null) {
                            methodMap = new HashMap();
                        }
                        //classMap.put(value, classes);
                        methodMap.put(value, methods);
                        String mName = "";
                        List list = methods.methodClassName.methodNames;
                        int count = 0;
                        for (int i = 0; i < list.size(); i++) {
                            MethodName methodName = (MethodName) list.get(i);                                               
                                if (methodName.visibility.equalsIgnoreCase("yes")) {
                                    count++;
                                    mName = methodName.methodname.split("::")[0];
                                    if (!methodName.transformedname.equals("")) {
                                        mName = methodName.transformedname.split("::")[0];
                                    }
                                    String methodNameStr=methodName.methodname;
                                    if(!methodName.transformedname.equals("")){
                                        methodNameStr=methodName.transformedname;
                                    }
                %>
                <option value="<%=methodNameStr%>"><%=methodNameStr%></option>
                
                <%}
                    }%>
<!--                    <button class="btn btn-success" id="Button" onclick="generateOperatorreports()"><i class="fa fa-money" style="font-size:15px;"></i> Save Price</button>   -->
                    <%} else {%>
                    <option value="-1">Select API</option>  
                <% }%>
                </select>
           
        </div>
    </div>