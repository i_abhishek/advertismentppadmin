function strCompare(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function Operatortextreport(Optype) {
    var type = document.getElementById('_Rtype').value;
    var sdate = document.getElementById('_startdate').value;
    var edate = document.getElementById('_enddate').value;
    if (sdate === '' || edate === '') {
        bootboxmodel("<h2><font style='color:red'>Please Select the Date ranges!!!</font><h2>");
        return;
    } else if (type === -1) {
        bootboxmodel("<h3>Please Select the Report type!!!<h3>");
        return;
    } else {
        var waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
                '<div class="modal-dialog"><div class="modal-content"><div class="modal-body"><h2><font style="color:  blue; font-weight: bolder">Please Wait</fon></h2></div></div></div></div>');
        waiting.modal();
        if (Optype === 1) {
            var Accesspoint = document.getElementById('_Accesspoint').value;
            var Group = document.getElementById('_Group').value;
            var Resource = document.getElementById('_Resources').value;
            var partner = document.getElementById('_Partner').value;
            var stime = document.getElementById('_ApStartTime').value;
            var etime = document.getElementById('_ApEndTime').value;
            if (type === 'pdf') {
                userReportPDF(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
            }
            else if (type === 'txt') {
                userReportTXT(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
            }
            else if (type === 'csv') {
                userReportCSV(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
            }
            waiting.modal('hide');
        }
        else {
            var Accesspoint = -1;
            var Group = -1;
            var Resource = -1;
            var partner = document.getElementById('pId').value;
            var stime = document.getElementById('_ApStartTime').value;
            var etime = document.getElementById('_ApEndTime').value;
            if (type === 'pdf') {
                userReportPDF(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
            }
            else if (type === 'txt') {
                userReportTXT(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
            }
            else if (type === 'csv') {
                userReportCSV(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
            }
            waiting.modal('hide');
        }
    }
}
function userReportPDF(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime) {
    var s = './getReports?_apid=' + Accesspoint + "&_grid=" + Group + "&_resid=" + Resource + "&_pid=" + partner + "&_sdate=" + sdate + "&_stime=" + stime + "&_edate=" + edate + "&_etime=" + etime + "&_format=0";
    window.location.href = s;
    return false;
}

function userReportCSV(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime) {
    var s = './getReports?_apid=' + Accesspoint + "&_grid=" + Group + "&_resid=" + Resource + "&_pid=" + partner + "&_sdate=" + sdate + "&_stime=" + stime + "&_edate=" + edate + "&_etime=" + etime + "&_format=1";
    window.location.href = s;
    return false;
}

function userReportTXT(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime) {
    var s = './getReports?_apid=' + Accesspoint + "&_grid=" + Group + "&_resid=" + Resource + "&_pid=" + partner + "&_sdate=" + sdate + "&_stime=" + stime + "&_edate=" + edate + "&_etime=" + etime + "&_format=2";
    window.location.href = s;
    return false;
}
