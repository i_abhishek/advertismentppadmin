<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgUsers"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@include file="header.jsp" %>
<script src="./js/teamMembers.js"></script>
<script src="./js/sgservices.js"></script>
<%    SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
    String partnerId = request.getParameter("partnerId");
    int pId = Integer.parseInt(partnerId);
    String PartnerName = "None";
    PartnerDetails p = new PartnerManagement().getPartnerDetails(SessionId, channelId, pId);
    if (p != null) {
        PartnerName = p.getPartnerName();
    }
%>
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">                
                <h3 class="page-header text-uppercase">Team Members of &nbsp;&nbsp; "<%= PartnerName%>"</h3>
            </div>
            <!-- /.col-lg-10 -->
        </div>
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47;  <i class="fa fa-user"></i><a href="javascript:history.back()"> Developer Management</a>&#47; <i class="fa fa-user"></i> <b>Team Members</b>              
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                            <th ><font style="font-size: 14px">No.</font></th>
                            <th><font style="font-size: 14px">Name</font></th>
                            <th><font style="font-size: 14px">Email</font></th>
                            <th ><font style="font-size: 14px">Mobile Number</font></th>                                                
                            <th ><font style="font-size: 14px">Operator Type</font></th>       
                            <th ><font style="font-size: 14px">Created On</font></th>                            
                            </thead>
                            <tbody>
                                <%
                                    UsersManagement um = new UsersManagement();
                                    SgUsers[] rDetails = null;

                                    rDetails = um.getSgUsersByPartnerId(SessionId, channelId, pId);
                                    if (rDetails
                                            != null) {
                                        if (rDetails.length == 0) {
                                            rDetails = null;
                                        }
                                    }
                                    if (rDetails != null) {
                                        for (int i = 0; i < rDetails.length; i++) {
                                %>
                                <tr>
                                    <td><font style="font-size: 14px"><%= i + 1%></font></td>
                                    <td><font style="font-size: 14px"><%= rDetails[i].getUsername()%></font></td>
                                    <td><font style="font-size: 14px"> <%= rDetails[i].getEmail()%> </font></td>
                                    <td><font style="font-size: 14px"> <%= rDetails[i].getPhone()%> </font></td>
                                        <%
                                            String TYPE = null;
                                            if (rDetails[i].getType() == OPERATOR) {
                                                TYPE = "Operator";
                                            } else if (rDetails[i].getType() == REPORTER) {
                                                TYPE = "Reporter";
                                            }
                                        %>
                                    <td><font style="font-size: 14px"><%= TYPE%></font></td>                                              
                                    <td><font style="font-size: 14px"><%=sdf.format(rDetails[i].getCreatedon())%></font></td>                                              
                                </tr>
                                <%}

                                } else {%>
                                <tr>
                                    <td>No Record Found</td>
                                    <td>No Record Found</td>
                                    <td>No Record Found</td>
                                    <td>No Record Found</td>
                                    <td>No Record Found</td>
                                    <td>No Record Found</td>                                                    
                                </tr>
                                <%}%>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript 
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.min.js"></script>
<script src="js/morris-data.js"></script>-->

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>
<script src="js/bootbox.min.js" type="text/javascript"></script>
<script>
                                            $(document).ready(function () {
                                                $('#dataTables-example').DataTable({
                                                    responsive: true
                                                });
                                            });
</script>                         