package com.mollatech.PPadmin.billing;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "RejectPackageRequest", urlPatterns = { "/RejectPackageRequest" })
public class RejectPackageRequest extends HttpServlet {

//    static final int REQUEST_ACTIVE_STATUS = 1;
//
//    static final int REQUEST_SUSPENDED_STATUS = 0;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Package rejected successfully";
        PrintWriter out = response.getWriter();
        String packageName = request.getParameter("_packageName");
        String reqStatus = request.getParameter("_packagestatus");
        String reason = request.getParameter("reason");
        int ireqStatus = 0;
        if (reqStatus != null) {
            ireqStatus = Integer.parseInt(reqStatus);
        }
        int a = -1;
        RequestPackageManagement pm = new RequestPackageManagement();
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        if (SessionId != null) {
            SgReqbucketdetails sgReqPackage = pm.getReqPackageByName(SessionId, channelId, packageName);
            if (sgReqPackage != null) {
                a = pm.RejectPackageRequest(SessionId, channelId, sgReqPackage.getBucketName(), ireqStatus, reason);
            }
            if (a == 0) {
                result = "success";
                message = "Package rejected successfully";
            } else if (a == 3) {
                result = "error";
                if (ireqStatus == GlobalStatus.ACTIVE) {
                    message = "Package already rejected  ";
                } else {
                    message = "Package did not rejected ";
                }
            } else {
                result = "error";
                message = "Package did not rejected ";
            }
        }
        try {
            json.put("result", result);
            json.put("message", message);
            out.print(json);
            out.flush();
            out.close();
            return;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
            out.close();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
