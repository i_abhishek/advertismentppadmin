package com.mollatech.PPadmin.billing;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.commons.MakerChaker;
import com.mollatech.serviceguard.nucleus.db.SgBucketdetails;
import com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "EditSlabPrice", urlPatterns = { "/EditSlabPrice" })
public class EditSlabPrice extends HttpServlet {

    static final Logger logger = Logger.getLogger(EditSlabPrice.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Request servlet is #EditAPPrice from #PPAdmin at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Slab Price have been updated successfully";
        String channelId = (String) request.getSession().getAttribute("_channelId");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String packageID = request.getParameter("packageId");
        logger.debug("Value of packageID  = " + packageID);
        int pId = 0;
        if (packageID != null) {
            pId = Integer.parseInt(packageID);
        }
        SgBucketdetails packObj = new PackageManagement().getPackageDetails(SessionId, channelId, pId);
        try {
            if (packObj == null) {
                result = "error";
                message = "Can't edit the package details.";
                json.put("result", result);
                json.put("message", message);
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        PackageManagement pkMangObj = new PackageManagement();
        String slabPricing = request.getParameter("SlabPricing");
        if (slabPricing.equalsIgnoreCase("Disable")) {
//            if (packObj.getTabShowFlag() != GlobalStatus.FEATURE) {
                packObj.setTabShowFlag(GlobalStatus.TIERINGPRICE);
//            }
            packObj.setSlabApRateDetails(null);
            pkMangObj.editPackage(SessionId, packObj);
            try {
                json.put("result", result);
                json.put("message", message);
                return;
            } finally {
                logger.info("Response of #EditSlabPrice from #PPAdmin " + json.toString());
                logger.info("Response of #EditSlabPrice from #PPAdmin Servlet at " + new Date());
                out.print(json);
                out.flush();
            }
        }
        if (packObj.getTierRateDetails() != null) {
            result = "error";
            message = "Tier Price applied, Does not apply Slab price.";
            json.put("result", result);
            json.put("message", message);
            out.print(json);
            out.flush();
            return;
        }
        String ACPoint = request.getParameter("_Accesspoint2");
        logger.debug("Value of ACPoint  = " + ACPoint);
        String resource = request.getParameter("_ResourceForSlabPricing2");
        logger.debug("Value of resource  = " + resource);
        String version = request.getParameter("_VersionForSlabPricing2");
        logger.debug("Value of version  = " + version);
        String APIName = request.getParameter("_APIForSlabPricing2");
        logger.debug("Value of APIName  = " + APIName);
        String key = ACPoint + ":" + resource + ":" + version + ":" + APIName;
        String rangeFrom1 = request.getParameter("rangeFrom1");
        logger.debug("Value of rangeFrom1  = " + rangeFrom1);
        String rangeTo1 = request.getParameter("rangeTo1");
        logger.debug("Value of rangeTo1  = " + rangeTo1);
        String price1 = request.getParameter("price1");
        logger.debug("Value of rangeTo1  = " + price1);
        String rangeFrom2 = request.getParameter("rangeFrom2");
        logger.debug("Value of rangeFrom1  = " + rangeFrom2);
        String rangeTo2 = request.getParameter("rangeTo2");
        logger.debug("Value of rangeTo1  = " + rangeTo2);
        String price2 = request.getParameter("price2");
        logger.debug("Value of rangeTo1  = " + price2);
        String rangeFrom3 = request.getParameter("rangeFrom3");
        logger.debug("Value of rangeFrom1  = " + rangeFrom3);
        String rangeTo3 = request.getParameter("rangeTo3");
        logger.debug("Value of rangeTo1  = " + rangeTo3);
        String price3 = request.getParameter("price3");
        logger.debug("Value of rangeTo1  = " + price3);
        String rangeFrom4 = request.getParameter("rangeFrom4");
        logger.debug("Value of rangeFrom4  = " + rangeFrom4);
        String rangeTo4 = request.getParameter("rangeTo4");
        logger.debug("Value of rangeTo4  = " + rangeTo4);
        String price4 = request.getParameter("price4");
        logger.debug("Value of rangeTo4  = " + price4);
        String rangeFrom5 = request.getParameter("rangeFrom5");
        logger.debug("Value of rangeFrom5  = " + rangeFrom5);
        String rangeTo5 = request.getParameter("rangeTo5");
        logger.debug("Value of rangeTo5  = " + rangeTo5);
        String price5 = request.getParameter("price5");
        logger.debug("Value of price5  = " + price5);
        JSONArray apRateArray = new JSONArray();
        JSONObject jsonAPRate = new JSONObject();
        JSONObject jsonSAPRate = new JSONObject();
        try {
            jsonAPRate.put("price5", price5);
            jsonAPRate.put("rangeTo5", rangeTo5);
            jsonAPRate.put("rangeFrom5", rangeFrom5);
            jsonAPRate.put("price4", price4);
            jsonAPRate.put("rangeTo4", rangeTo4);
            jsonAPRate.put("rangeFrom4", rangeFrom4);
            jsonAPRate.put("price3", price3);
            jsonAPRate.put("rangeTo3", rangeTo3);
            jsonAPRate.put("rangeFrom3", rangeFrom3);
            jsonAPRate.put("price2", price2);
            jsonAPRate.put("rangeTo2", rangeTo2);
            jsonAPRate.put("rangeFrom2", rangeFrom2);
            jsonAPRate.put("price1", price1);
            jsonAPRate.put("rangeTo1", rangeTo1);
            jsonAPRate.put("rangeFrom1", rangeFrom1);
            jsonSAPRate.put(key, jsonAPRate);
            int retValue = 0;
            if (packObj.getTabShowFlag() != GlobalStatus.FEATURE) {
                packObj.setTabShowFlag(GlobalStatus.TIERINGPRICE);
            }
            String PartnerVisibility[] = request.getParameterValues("visibilityPartner");
            String partnerId = "";
            if (PartnerVisibility != null) {
                for (int i = 0; i < PartnerVisibility.length; i++) {
                    partnerId += PartnerVisibility[i] + ",";
                }
            }
            packObj.setPartnerVisibility(partnerId);
            if (packObj.getSlabApRateDetails() == null || packObj.getSlabApRateDetails().equals("")) {
                apRateArray.add(jsonSAPRate);
                packObj.setSlabApRateDetails(apRateArray.toString());
                retValue = pkMangObj.editPackage(SessionId, packObj);
            } else {
                boolean flag = true;
                String jsonString = packObj.getSlabApRateDetails();
                org.json.JSONArray jsOld = new org.json.JSONArray(jsonString);
                for (int i = 0; i < jsOld.length(); i++) {
                    org.json.JSONObject jsonexists1 = jsOld.getJSONObject(i);
                    if (jsonexists1.has(key)) {
                        jsonexists1.remove(key);
                        jsonexists1.put(key, jsonAPRate);
                        jsOld.put(i, jsonexists1);
                        packObj.setSlabApRateDetails(jsOld.toString());
                        retValue = pkMangObj.editPackage(SessionId, packObj);
                        flag = false;
                        break;
                    }
                }
                if (flag) {
                    jsOld.put(jsonSAPRate);
                    packObj.setSlabApRateDetails(jsOld.toString());
                    retValue = pkMangObj.editPackage(SessionId, packObj);
                }
            }
            MakerChaker makerChakerObj = (MakerChaker) request.getSession().getAttribute("makerChacker");
            SgReqbucketdetails existsObj = new RequestPackageManagement().getReqPackageByName(SessionId, channelId, packObj.getBucketName());
            boolean makerChackerFlag = true;
            if (makerChakerObj == null) {
                makerChackerFlag = false;
            } else if (makerChakerObj.status == GlobalStatus.SUSPEND) {
                makerChackerFlag = false;
            }
            if (retValue == -1) {
                result = "error";
                message = "Slab Price does not saved.";
                json.put("result", result);
                logger.debug("Response of #EditSlabPrice from #PPAdmin Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of #EditSlabPrice from #PPAdmin Servlet's Parameter  message is " + message);
            } else {
                if (makerChackerFlag == false && existsObj != null) {
                    existsObj.setPartnerVisibility(packObj.getPartnerVisibility());
                    existsObj.setTabShowFlag(packObj.getTabShowFlag());
                    existsObj.setSlabApRateDetails(packObj.getSlabApRateDetails());
                    new RequestPackageManagement().editReqPackage(SessionId, channelId, existsObj);
                }
                json.put("result", result);
                json.put("message", message);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Exception at #EditSlabPrice from #PPAdmin " + e);
        } finally {
            logger.info("Response of #EditSlabPrice from #PPAdmin " + json.toString());
            logger.info("Response of #EditSlabPrice from #PPAdmin Servlet at " + new Date());
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
