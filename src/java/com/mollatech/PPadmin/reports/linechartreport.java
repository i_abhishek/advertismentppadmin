package com.mollatech.PPadmin.reports;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestTrackingManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class linechartreport extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        String channel = null, sessionId = null;
        int OperatorType = -1;
        String _OperatorType = request.getParameter("_opType");
        if (_OperatorType != null) {
            OperatorType = Integer.parseInt(_OperatorType);
        }
        sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        channel = (String) request.getSession().getAttribute("_channelId");
        int partnerid = -1, apid = -1, groupid = -1, resid = -1;
        long tpd;
        String _accesspoint = request.getParameter("_accesspoint");
        if (_accesspoint != null) {
            apid = Integer.parseInt(_accesspoint);
        }
        String _group = request.getParameter("_group");
        if (_group != null) {
            groupid = Integer.parseInt(_group);
        }
        String _resources = request.getParameter("_resources");
        if (_resources != null) {
            resid = Integer.parseInt(_resources);
        }
        String _partner = request.getParameter("_partner");
        if (_partner != null) {
            partnerid = Integer.parseInt(_partner);
        }
        String _sdate = request.getParameter("_sdate");
        String _edate = request.getParameter("_edate");
        String _stime = request.getParameter("_stime");
        String _etime = request.getParameter("_etime");
        DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        DateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat timeformat = new SimpleDateFormat("hh:mm a");
        Date startDate = null;
        Date endDate = null;
        Date startTime = null;
        Date endTime = null;
        try {
            if (_sdate != null && !_sdate.isEmpty()) {
                startDate = (Date) formatter.parse(_sdate);
            }
            if (_edate != null && !_edate.isEmpty()) {
                endDate = (Date) formatter.parse(_edate);
            }
            if (_stime != null && !_stime.isEmpty()) {
                startTime = (Date) timeformat.parse(_stime);
            }
            if (_etime != null && !_etime.isEmpty()) {
                endTime = (Date) timeformat.parse(_etime);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Calendar current = Calendar.getInstance();
        current.setTime(endDate);
        Date endDates = current.getTime();
        current.setTime(startDate);
        Date startDates = current.getTime();
        Date startd = current.getTime();
        long diff = endDates.getTime() - startDates.getTime();
        long dates = diff / (24 * 60 * 60 * 1000);
        long difftime = endTime.getTime() - startTime.getTime();
        try {
            ArrayList<line> sample = new ArrayList<line>();
            if (dates == 0) {
                if (difftime != 0) {
                    String d = formatter.format(startd);
                    tpd = new RequestTrackingManagement().getTPDbyDuration(sessionId, channel, apid, resid, groupid, partnerid, d);
                    String datess = formatter1.format(startd);
                    sample.add(new line(tpd, datess));
                } else {
                    String datess = formatter1.format(startd);
                    sample.add(new line(0, datess));
                }
            } else {
                for (long i = 0; i <= dates; i++) {
                    String d = formatter.format(startd);
                    tpd = new RequestTrackingManagement().getTPDbyDuration(sessionId, channel, apid, resid, groupid, partnerid, d);
                    String datess = formatter1.format(startd);
                    sample.add(new line(tpd, datess));
                    current.add(Calendar.DATE, 1);
                    startd = current.getTime();
                }
            }
            Gson gson = new Gson();
            JsonElement element = gson.toJsonTree(sample, new TypeToken<List<line>>() {
            }.getType());
            JsonArray jsonArray = element.getAsJsonArray();
            out.print(jsonArray);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.close();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
