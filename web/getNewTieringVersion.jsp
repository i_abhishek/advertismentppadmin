<%@page import="com.mollatech.serviceguard.nucleus.db.SgBucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement"%>

<%@page import="com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MethodName"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Classes"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Methods"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <div class="control-group">
        <label class="control-label"  for="username"></label>
        <div class="controls">
            <select id="_VersionForTieringPricing2" name="_VersionForTieringPricing2"  class="form-control span2" onchange="showTieringAPI(this.value)" style="width: 100%">
                <%
                    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
                    String channelId = (String) request.getSession().getAttribute("_channelId");
                    String apName = request.getParameter("_apName");
                    String packageName = request.getParameter("packageName");
                    if(apName != null && !apName.equals("-1")){
                        //SgReqbucketdetails reqObj = new RequestPackageManagement().getReqPackageByName(SessionId, channelId, packageName);
                        SgBucketdetails reqObj = new PackageManagement().getPackageByName(SessionId, channelId, packageName);
                        if (reqObj != null) {
                            String tierDetails = reqObj.getTierRateDetails();
                            if (tierDetails != null) {
                                JSONArray jsOld = new JSONArray(tierDetails);
                                String ver = "";
                                for (int j = 0; j < jsOld.length(); j++) {
                                    JSONObject jsonexists1 = jsOld.getJSONObject(j);
                                    
                                    Iterator<String> keys = jsonexists1.keys();
                                    while (keys.hasNext()) {
                                            String keyData = keys.next();
                                            String[] keyDetails = keyData.split(":");
                                            if (ver.isEmpty()) {
                                                ver += keyDetails[2] + ",";
                                            } else if (ver.contains(keyDetails[2])) {
                                                continue;
                                            } else {
                                                ver += keyDetails[2] + ",";
                                            }

                                        }
                                }
                                if (!ver.equals("")) {
                                    String[] versionDetails = ver.split(",");
                                    if (versionDetails != null) {
                                        for (int i = 0; i < versionDetails.length; i++) {
                %>
                <option value="<%=versionDetails[i]%>"><%=versionDetails[i]%></option>               
                <%}
                                }
                            }
                        }
                    }
                } else {%>                 
                <option value="-1">Select Version</option>
                <%}%>
            </select>
        </div>
    </div>
