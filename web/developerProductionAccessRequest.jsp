
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessEnvtManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgDeveloperProductionEnvt"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.UtilityFunctions"%>
<%@page import="java.util.Enumeration"%>
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="java.util.Properties"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.GroupDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.GroupManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.AccessPolicy"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.Calendar"%>
<%@page import="javax.xml.datatype.XMLGregorianCalendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@page import="java.nio.channels.Channels"%>
<%@include file="header.jsp" %>
<script src="./js/modal.js"></script>
<script src="js/developerTMProduction.js" type="text/javascript"></script>

<%
    Properties partnerRejectionProperties = null;
    partnerRejectionProperties = LoadSettings.g_partnerRejectionSettings;

    String options = "<option value='Select' selected>Select Reason for Rejection </option>\n";

    if (partnerRejectionProperties != null) {
        Enumeration enuKeys = partnerRejectionProperties.keys();
        while (enuKeys.hasMoreElements()) {
            String key = (String) enuKeys.nextElement();
            String value = partnerRejectionProperties.getProperty(key);
            options += "<option value='" + value + "'>" + value + "</option>\n";
        }
    }
%>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-header">Production Access Request</h4>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47; <i class="fa fa-users"></i><b> All Pending Requests</b>
                </div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <form class="form-horizontal " method="get">                    
                            <table class="table table-striped table-bordered table-hover display nowrap">
                                <thead>
                                    <tr>
                                        <td/>
                                        <th>No.</th>
                                        <th>Name</th>                                        
                                        <th>CompanyName</th>
                                        <th>FixedLineNumber</th>
                                        <th>Manage</th>                                    
                                        <th>Requested On</th>                                    
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        SgDeveloperProductionEnvt[] SgDeveloperProductionEnvt = new ProductionAccessEnvtManagement().listPartnerProductionRequests(SessionId);                                        
                                        if (SgDeveloperProductionEnvt != null && SgDeveloperProductionEnvt.length > 0) {
                                            for (int i = 0; i < SgDeveloperProductionEnvt.length; i++) {
                                                String partnerName = "NA";
                                                Date date = SgDeveloperProductionEnvt[i].getCreatedOn();
                                                PartnerDetails par = new PartnerManagement().getPartnerDetails(SgDeveloperProductionEnvt[i].getPartnerid());
                                                if(par != null){
                                                    partnerName = par.getPartnerName();
                                                }
                                    %>

                                    <tr>
                                        <td/>
                                        <td ><%=i + 1%></td>
                                        <td ><%=partnerName%></td>                                       
                                        <td ><%=SgDeveloperProductionEnvt[i].getComapnyName()%></td>
                                        <td ><%=SgDeveloperProductionEnvt[i].getFixedLineNumber()%></td>                                        
                                        <td>
                                            <div class="btn-group">                                                
                                                <button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown"><%if (SgDeveloperProductionEnvt[i].getStatus() == GlobalStatus.APPROVED) {%> <font style="font-size: 10px;">Approved</font> <%} else if (SgDeveloperProductionEnvt[i].getStatus() == GlobalStatus.REJECTED) {%><font style="font-size: 10px;"> Rejected</font> <%} else {%> <font style="font-size: 10px;"> New</font> <%}%><span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#" onclick="approveProductionRequest('<%=SgDeveloperProductionEnvt[i].getDeveloperProudId()%>', '<%=GlobalStatus.APPROVED%>', '<%=SgDeveloperProductionEnvt[i].getPartnerid()%>')">Approve</a></li>
                                                    <li><a href="#" onclick="rejectDeveloperProductionRequest('<%=SgDeveloperProductionEnvt[i].getDeveloperProudId()%>', '<%=GlobalStatus.REJECTED%>','<%=SgDeveloperProductionEnvt[i].getPartnerid()%>')">Reject</a></li>                                                    
                                                </ul>
                                            </div>
                                        </td>
                                        <td><%= UtilityFunctions.getTMReqDate(date)%></td>
                                    </tr>
                                    <%}
                                        } %>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>                              
    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="rejectionProductionModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>
                    <h4 class="modal-title">Reject Developer Production Environment Request</h4>
                </div>
                <div class="modal-body">
                    <form class="form-inline" id="productionRequestRejectionForm">
                        <fieldset>
                            <div class="control-group">
                                <label class="control-label col-lg-2"  for="partnername">Reason</label>
                                <div class="controls col-lg-10">
                                    <select id="reason" name="reason" class="form-control" >
                                        <%=options%>
                                    </select>
                                    <input type="hidden"  id="_rejectionProductionId" name="_rejectionProductionId"/> 
                                    <input type="hidden"  id="_rejectionPartnerRequestStatus" name="_rejectionPartnerRequestStatus"/>
                                    <input type="hidden"  id="_productionpartnerId" name="_productionpartnerId"/> 
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="col-lg-12">
                                    <a class="btn btn-success btn-sm" onclick="ChangeDeveloperProductionStatus()" style="float: right; margin: 15px; "><span class="icon_profile"></span> <i class="fa fa-check-circle"></i> Reject Developer</a>
                                    <button class="btn btn-danger btn-sm" style="float: right; margin: 15px;" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>                
                </div>
            </div>            
        </div>
    </div>
</div>



<script src="bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="js/bootbox.min(1).js" type="text/javascript"></script>
<!-- Morris Charts JavaScript 
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.min.js"></script>
<script src="js/morris-data.js"></script>-->

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->

</body>
</html>
<%@include file="footer.jsp" %>
