<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.CreditManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgCreditInfo"%>
<%
    String developerIdStr = request.getParameter("developerId");
    if (developerIdStr == null || developerIdStr.equals("-1")) {
        return;
    }
    int devId = Integer.parseInt(developerIdStr);
    SgCreditInfo info = new CreditManagement().getDetails(devId);
    double remainingFC = 0f;
    double remainingMC = 0f;
    if (info != null) {
        remainingFC = info.getFreeCredit();
        remainingMC = info.getMainCredit();
    }
%>
<table>
    <tr>
        <td>
            <label class="control-label" >Remaining Free Credits</label>&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
            <div class="col-lg-12">
                <div class="input-group">
                    <span class="input-group-addon">$</span>
                    <input type="text" class="form-control"  placeholder="Price"  value="<%=remainingFC%>" readonly>                                    
                </div>
            </div>
        </td>
        <td>
            <label class="control-label" >&nbsp;&nbsp;&nbsp;&nbsp;Remaining Main Credits</label>&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
            <div class="col-lg-12">
                <div class="input-group">
                    <span class="input-group-addon">$</span>
                    <input type="text" class="form-control"  placeholder="Price" value="<%=remainingMC%>" readonly>                                    
                </div>
            </div>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
        <td>
            <label class="control-label" >Free Credits</label>&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
            <div class="col-lg-12">
                <div class="input-group">
                    <span class="input-group-addon">$</span>
                    <input type="text" class="form-control" id="freeCredits" name="freeCredits" placeholder="Price"  value="0" >                                    
                </div>
            </div>
        </td>
        <td>
            <label class="control-label" >&nbsp;&nbsp;&nbsp;&nbsp;Main Credits</label>&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
        <td>
            <div class="col-lg-12">
                <div class="input-group">
                    <span class="input-group-addon">$</span>
                    <input type="text" class="form-control" id="mainCredits" name="mainCredits" placeholder="Price" value="0" >                                    
                </div>
            </div>
        </td>
    </tr>
</table>
<br><br>
<a  class="btn btn-success  btn-sm" onclick="addCredits('<%=devId%>')"><i class="fa fa-edit"></i> Add Credits</a> 