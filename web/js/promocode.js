/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var waitingV1
    function pleasewaitV1() {
    waitingV1 = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
        '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>'+            
        '<div class="progress progress-striped active">'+
                            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">'+
                            '</div>'+
                        '</div>'+            
                        '</div>' +
                        '</div></div></div></div>');    
                waitingV1.modal('show');
            }
            
function showAlert(message, type, closeDelay){
   if ($("#alerts-container").length == 0) {
        // alerts-container does not exist, create it
        $("body")
            .append( $('<div id="alerts-container" style="position: fixed;'+
                'width: 50%; left: 25%; top: 10%;">') );
        }
        // default to alert-info; other options include success, warning, danger
        type = type || "info";          
        // create the alert div
        message = '<i class="fa fa-info-circle"></i> '+message;
    var alert = $('<div class="alert alert-' + type + ' fade in">')
        .append(
            $('<button type="button" class="close" data-dismiss="alert">')
            .append("&times;")
        )
        .append(message);        
    // add the alert div to top of alerts-container, use append() to add to bottom
    $("#alerts-container").prepend(alert);    
    // if closeDelay was passed - set a timeout to close the alert
    if (closeDelay)
        window.setTimeout(function() { alert.alert("close") }, closeDelay);     
}
 var errorCount = 0;
function showAlertV1(message, type, closeDelay){
   if(errorCount === 0){
    if ($("#alerts-container").length == 0) {
        $("body")
            .append( $('<div id="alerts-container" style="position: fixed;'+
                'width: 50%; left: 25%; top: 10%;">') );
        }
        type = type || "info";          
        message = '<i class="fa fa-info-circle"></i> '+message;
    var alert = $('<div class="alert alert-' + type + ' fade in">')
        .append(
            $('<button type="button" class="close" data-dismiss="alert">')
            .append("&times;")
        )
        .append(message);            
    $("#alerts-container").prepend(alert);    
        if (closeDelay){
            window.setTimeout(function() { alert.alert("close") }, closeDelay);
            errorCount = 0;
        }
   }
}

function strCompare(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function createPromoCode() {
    var _promoCode = document.getElementById('_promoCode').value;
    var _promoExpiry = document.getElementById('_promoExpiry').value;
    var _usageCount = document.getElementById('_usageCount').value;
    var _partnerUsageCount = document.getElementById('_partnerUsageCount').value; 
    var _promoDiscount = document.getElementById('_promoDiscount').value;
    var visibleTo = document.getElementById('visibleTo').value;
    
    if (_promoCode.trim() === '' || _promoCode.indexOf(' ') >= 0) {        
        showAlert("Please enter promocode","danger",3000);
        return;
    }
    
    if (_promoExpiry.trim() === '' || _promoExpiry.indexOf(' ') >= 0) {        
        showAlert("Please enter expiry","danger",3000);
        return;
    }
    if (_promoExpiry === '0') {        
        showAlert("Expiry can not be zero","danger",3000);
        return;
    }
    if (_usageCount.trim() === '' || _usageCount.indexOf(' ') >= 0) {       
        showAlert("Please enter usage count","danger",3000);
        return;
    }
    if (_usageCount === '0') {        
        showAlert("Usage can not be zero","danger",3000);
        return;
    }
    if (_partnerUsageCount.trim() === '' || _partnerUsageCount.indexOf(' ') >= 0) {       
        showAlert("Please enter partner usage count","danger",3000);
        return;
    }
    if (_partnerUsageCount === '0') {        
        showAlert("Partner usage can not be zero","danger",3000);
        return;
    }
    if (_promoDiscount.trim() === '' || _promoDiscount.indexOf(' ') >= 0) {        
        showAlert("Please enter discount amount","danger",3000);
        return;
    }
    if (_promoDiscount === '0') {        
        showAlert("Discount can not be zero","danger",3000);
        return;
    }
    if (visibleTo.trim() === '' || visibleTo.indexOf(' ') >= 0) {
        showAlert("Please select partner(s)","danger",3000);
        return;
    }
    var s = './CreatePromocode';
    pleasewaitV1();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#promoCode_form").serialize(),
        success: function(data) {
            if (strCompare(data.result, "error") === 0) {
                waitingV1.modal('hide');
                showAlert(data.message,"danger",3000);
            }
            else if (strCompare(data.result, "success") === 0) {                
                waitingV1.modal('hide');
                showAlert(data.message,"success",3000);
                setTimeout(function() {
                    window.location.href = "./promocodelist.jsp";
                }, 3000);
            }
        }
    });
}

function CheckPromocodeAvailability() {
    var value = $("#_promoCode").val();
    var s = './CheckPromocodeAvailability?_promocode=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data.result, "error") == 0) {
                $('#checkAvailability-result').html("<span><font color=red>" + data.message + "</font></span></small>");
            } else {
                $('#checkAvailability-result').html("<span><font color=blue>" + data.message + "</font></span></small>");
            }
        }
    });
}

function notifyToPartner(promocodeId){
    var s = './SendPromocode?promoCodeId='+promocodeId;
    pleasewaitV1();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',        
        success: function(data) {
            if (strCompare(data._result, "error") === 0) {
                waitingV1.modal('hide');
                showAlert(data._message,"danger",3000);
//                setTimeout(function() {
//                    window.location.href = "./home.jsp";
//                }, 3000);
            }
            else if (strCompare(data._result, "success") === 0) {                
                waitingV1.modal('hide');
                showAlert(data._message,"success",3000);
//                setTimeout(function() {
//                    window.location.href = "./home.jsp";
//                }, 3000);
            }
        }
    });
}

function SendToChecker(promocodeId,promocodeReqFlag){
    var s = './SendPromocodeToChecker?promoCodeId='+promocodeId+"&promocodeReqFlag="+promocodeReqFlag;
    pleasewaitV1();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',        
        success: function(data) {
            if (strCompare(data._result, "error") === 0) {
                waitingV1.modal('hide');
                showAlert(data._message,"danger",3000);
//                setTimeout(function() {
//                    window.location.href = "./home.jsp";
//                }, 3000);
            }
            else if (strCompare(data._result, "success") === 0) {                
                waitingV1.modal('hide');
                showAlert(data._message,"success",3000);
                setTimeout(function() {
                    window.location.href = "./promocodelist.jsp";
                }, 3000);
            }
        }
    });
}

function rejectPromoCodeRequest( _promoId,_promostatus) {
   // document.getElementById("rejectPromoCodeModal").innerHTML = "Reject Promocode ";

    document.getElementById("_promoId").value = _promoId;
    document.getElementById("_promostatus").value = _promostatus;

    $('#rejectPromocode').modal();
}

function PromoDiscount(val) {
    if (val === 'Flat') {
        var content =
                '<div class="form-group">'+
                '<label class="control-label col-lg-2">Discount</label>'+
                '<div class=col-sm-3>'+
                '<select id="_promoCodeDiscountType" name="_promoCodeDiscountType" class="form-control" onchange="VolumeRest()">' +
                '<option selected value="Flat">Flat</option>' +
                '<option value="Custom">Custom</option>' +
                '</select>' +
                '</div>'+        
                '<label class="control-label col-lg-2">In Price</label>'+
                '<div class="col-lg-3">'+
                '<div class="input-group">'+
                '<span class="input-group-addon"><i class="fa fa-money"></i></span>'+
                '<input type="text" id="_promoDiscount" name="_promoDiscount" class="form-control" placeholder="Discount" onkeypress="return isNumberKey(event)">'+
                '</div>'+
                '</div>'+
                '</div>';
        document.getElementById('_promoDiscountSelect').innerHTML = content;
    } else {
        var content = 
                '<div class="form-group">'+
                '<label class="control-label col-lg-2">Discount</label>'+
                '<div class="col-lg-3">'+
                '<select id="_promoCodeDiscountType" name="_promoCodeDiscountType" class="form-control" onchange="VolumeRest()">' +
                ' <option value="Flat">Flat</option>' +
                '<option selected value="Custom">Custom</option>' +
                '</select>'+
                '</div>'+
                
                '<label class="control-label col-lg-2">In Percentage</label>'+
                '<div class="col-lg-3">'+
                '<div class="input-group">'+
                '<span class="input-group-addon"><i class="fa fa-money"></i></span>'+
                '<input type="text" id="_promoDiscount" name="_promoDiscount" class="form-control" placeholder="Discount" onkeypress="return isNumberKey(event)">'+
                '</div>'+
                '</div>'+
                '</div>';

        document.getElementById('_promoDiscountSelect').innerHTML = content;
    }
}

function VolumeRest() {
    value = document.getElementById("_promoCodeDiscountType").value;
    PromoDiscount(value);
}

function editPromoCode(promocodeId){
    var _promoCode_edit = document.getElementById('_promoCode_edit').value;
    var _promoExpiry_edit = document.getElementById('_promoExpiry_edit').value;
    var _usageCount_edit = document.getElementById('_usageCount_edit').value;
    var _partnerUsageCount_edit = document.getElementById('_partnerUsageCount_edit').value; 
    var _promoDiscount_edit = document.getElementById('_promoDiscount_edit').value;
    var visibleTo_edit = document.getElementById('visibleTo_edit').value;
    
    if (_promoCode_edit.trim() === '' || _promoCode_edit.indexOf(' ') >= 0) {        
        showAlert("Please enter promocode","danger",3000);
        return;
    }
    
    if (_promoExpiry_edit.trim() === '' || _promoExpiry_edit.indexOf(' ') >= 0) {        
        showAlert("Please enter expiry","danger",3000);
        return;
    }
    if (_promoExpiry_edit === '0') {        
        showAlert("Expiry can not be zero","danger",3000);
        return;
    }
    if (_usageCount_edit.trim() === '' || _usageCount_edit.indexOf(' ') >= 0) {       
        showAlert("Please enter usage count","danger",3000);
        return;
    }
    if (_usageCount_edit === '0') {        
        showAlert("Usage can not be zero","danger",3000);
        return;
    }
    if (_partnerUsageCount_edit.trim() === '' || _partnerUsageCount_edit.indexOf(' ') >= 0) {       
        showAlert("Please enter partner usage count","danger",3000);
        return;
    }
    if (_partnerUsageCount_edit === '0') {        
        showAlert("Partner usage can not be zero","danger",3000);
        return;
    }
    if (_promoDiscount_edit.trim() === '' || _promoDiscount_edit.indexOf(' ') >= 0) {        
        showAlert("Please enter discount amount","danger",3000);
        return;
    }
    if (_promoDiscount_edit === '0') {        
        showAlert("Discount can not be zero","danger",3000);
        return;
    }
    if (visibleTo_edit.trim() === '' || visibleTo_edit.indexOf(' ') >= 0) {
        showAlert("Please select partner(s)","danger",3000);
        return;
    }
    var s = './EditPromocode?promocodeId='+promocodeId;
    pleasewaitV1();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#promoCode_form_edit").serialize(),
        success: function(data) {
            if (strCompare(data.result, "error") === 0) {
                waitingV1.modal('hide');
                showAlert(data.message,"danger",3000);
            }
            else if (strCompare(data.result, "success") === 0) {                
                waitingV1.modal('hide');
                showAlert(data.message,"success",3000);
                setTimeout(function() {
                    window.location.href = "./promocodelist.jsp";
                }, 3000);
            }
        }
    });
}
function bootboxmodelshow(content) {
    var popup = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' +
            '<button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>' +
            content +
            '<a class="btn btn-info btn-xs" data-dismiss="modal" class="close"> OK</a></div></div></div></div>');
    popup.modal();
}

function Alert4Users(msg) {
    bootboxmodelshow("<h5>" + msg + "</h5>");
}

//function rejectPromoRequest() {
//    var s = './RejectPromoRequest';
//    var reason = document.getElementById("reason").value;
//    if (reason.trim() === '') {
//        Alert4Users("<h4><font color=red>" + "Please enter proper reason details" + "</font></h4>");
//        return;
//    }
//    pleasewaitV1();
//    $.ajax({
//        type: 'POST',
//        url: s,
//        datatype: 'json',
//        data: $("#rejectPromoForm").serialize(),
//        success: function (data) {
//            if (strCompare(data.result, "error") === 0) {
//                waitingV1.modal('hide');
//                Alert4Users("<span><font color=red>" + data.message + "</font></span>");
//                ///showAlert(data.message,"danger",3000);
////                setTimeout(function () {
////                    window.location.href = "./packageRequest.jsp";
////                }, 3000);
//            } else if (strCompare(data.result, "success") === 0) {
//                waitingV1.modal('hide');
//                //showAlert(data.message,"success",3000);
//                Alert4Users("<span><font color=blue>" + data.message + "</font></span>");
//                setTimeout(function () {
//                    window.location.href = "./promocodeRequest.jsp";
//                }, 3000);
//            }
//        }
//    });
//}

function rejectPromoRequest() {
    var reason = document.getElementById("reason").value;
    if (reason === "Select") {
        Alert4Users("<h4><span><font color=red>Please Select Any Reason</font></span><h4>");
    } else {
        bootbox.confirm({
            message: ("<h4><font color=red>" + "Are you sure ?" + "</font></h4>"),
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {                
                if (result === true) {
                    var id = document.getElementById("_promoId").value;
                    var status = document.getElementById("_promostatus").value;
                    $('#rejectPromocode').modal('hide');
                    var s = './RejectPromoRequest?promoId=' + id + '&promostatus=' + status + '&reason=' + reason;
                    pleasewaitV1();
                    $.ajax({
                        type: 'POST',
                        url: s,
                        datatype: 'json',
                        data: $("#rejectPromoForm").serialize(),
                        success: function (data) {
                            if (strCompare(data.result, "error") === 0) {
                                waitingV1.modal('hide');
                                Alert4Users("<span><font color=red>" + data.message + "</font></span>");
                            } else if (strCompare(data.result, "success") === 0) {
                                waitingV1.modal('hide');
                                Alert4Users("<span><font color=blue>" + data.message + "</font></span>");
                                setTimeout(function () {
                                    window.location.href = "./promocodeRequest.jsp";
                                }, 3000);
                            }
                        }
                    });

                }
            }});
    }
}

function promoRejectionDetails(promocode,rejectionDetails) {
    document.getElementById("rejectPackageModal").innerHTML = "Rejection details of " + promocode;
    document.getElementById("_rejectionDetails").value = rejectionDetails;
    $('#viewRejection').modal();
}


function approvePromoRequest(promoId,promoStatus,promoRequestStatus) {
    var s = './ApprovePromoRequest?_promoId='+promoId+'&_promoStatus='+promoStatus+'&_promoRequestStatus='+promoRequestStatus;
    pleasewaitV1();
    $.ajax({
        type: 'POST',
        url: s,
        datatype: 'json',        
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                waitingV1.modal('hide');
                //Alert4Users("<span><font color=red>" + data.message + "</font></span>");
                showAlert(data._message,"danger",3000);
//                setTimeout(function () {
//                    window.location.href = "./packageRequest.jsp";
//                }, 3000);
            } else if (strCompare(data._result, "success") === 0) {
                waitingV1.modal('hide');
                showAlert(data._message,"success",3000);
                //Alert4Users("<span><font color=blue>" + data.message + "</font></span>");
                setTimeout(function () {
                    window.location.href = "./promocodeRequest.jsp";
                }, 3000);
            }
        }
    });
}

function isNumericKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if ((charCode < 48 || charCode > 57)) {
        var showMessage = 'Enter Numeric Values Only';
        Alert4Users("<span><font color=red>" + showMessage + "</font></span>");
        
        return false;
    }
    return true;
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode !== 46 && charCode > 31
            && (charCode < 48 || charCode > 57)) {
        var showMessage = 'Enter Numeric Or Decimal Values Only';
        Alert4Users("<span><font color=red>" + showMessage + "</font></span>");
        return false;
    }
    return true;
}