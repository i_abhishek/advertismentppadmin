
<%@page import="com.mollatech.serviceguard.nucleus.db.Monitorsettings"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.MonitorSettingsManagement"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="java.util.Map"%>
<%
    String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
    String channelId = (String) request.getSession().getAttribute("_channelId");
    String _settingId = request.getParameter("_settingId");
    int settingId = Integer.parseInt(_settingId);
   // PartnerPortalWrapper ppw = new PartnerPortalWrapper();
    Monitorsettings msetting = new MonitorSettingsManagement().getMonitorSettingbyId(sessionId, channelId, settingId);
    //String settingName = msetting.getMonitorName();
    int type = msetting.getMonitorType();
%>
<script src="./js/jquery.js"></script>
<script src="./js/d3.js"></script>
<script src="./js/epoach.js"></script>
<link rel="stylesheet" type="text/css" href="./css/epoach.css">
<script src="./js/ZeroClipboard.min.js"></script>
<script src="./js/realtimemonitoring.js"></script>
<div  class="span12" style="alignment-adjust: central">
    <h5 align="center" class="text-primary" style="font-weight: bold">REAL TIME MONITORING REPORT</h5>
    <font style="font-family: monospace">
    Response Time in Milliseconds
    </font>
    <div id="real-time-area1" class="epoch" style="height: 300px; width: 95%"></div> 
    <div  align="center" style="font-family: monospace" >
        Time
    </div>
</div>
<script> loadmsg('<%=_settingId%>', '<%=type%>');
</script>

