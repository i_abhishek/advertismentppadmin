<%@page import="org.json.JSONArray"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgResourceowner"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceOwnerManagement"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartyManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPartydetails"%>
<%
    int id = Integer.parseInt(request.getParameter("_id"));
    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
//    SgResourceowner SgResourceowner = new ResourceOwnerManagement().getResourceownerbyId(SessionId, id);
    String firstParty = request.getParameter("_firstParty");
    int resourceId = Integer.parseInt(request.getParameter("resourceId"));
    String channelId = (String) request.getSession().getAttribute("_channelId");
//    SgPartydetails[] sgPartylist = new PartyManagement().getAllPartyDetails(channelId);

    SgPartydetails[] sgPartylist = (SgPartydetails[]) session.getAttribute("sgPartylist");
    SgResourceowner SgResourceowner = (SgResourceowner) session.getAttribute("SgResourceowner");

    int firstPartyId = Integer.parseInt(firstParty);
%>
<select id="_selectSecondParties" name="_selectSecondParties"  class="form-control" onchange="" >
    <%
        int secondParty = 0;
        JSONObject jsonObj = new JSONObject();
        JSONObject jsonexists = null;
        if (firstPartyId != -1) {
            if (SgResourceowner.getData() != null) {
//            JSONObject jsonObj = new JSONObject(SgResourceowner.getData());
                JSONArray revenueDetailsArray = new JSONArray(SgResourceowner.getData());
                for (int i = 0; i < revenueDetailsArray.length(); i++) {
                    jsonexists = revenueDetailsArray.getJSONObject(i);
                    if (jsonexists.has(String.valueOf(resourceId))) {
                        jsonObj = jsonexists.getJSONObject(String.valueOf(resourceId));
                        break;
                    }
                }
                if (jsonObj.has("_secondParty")) {
                    SgPartydetails partyObj = new PartyManagement().getPartyById(channelId, Integer.parseInt(jsonObj.getString("_secondParty")));
                    if (partyObj != null && partyObj.getStatus() == GlobalStatus.ACTIVE) {
                        secondParty = partyObj.getPartyId();
    %>                                                        
    <option value="-1">Select Party</option>
    <option selected value="<%=partyObj.getPartyId()%>"><%=partyObj.getPartyName()%></option>
    <%
    } else {%>
    <option value="-1" selected>Select Party</option>            
    <%}
        }
    } else {
    %>                                       
    <option value="-1" selected>Select Party</option>
    <%
        }

        if (sgPartylist != null) {
            for (int i = 0; i < sgPartylist.length; i++) {
                if (sgPartylist[i].getStatus() == GlobalStatus.ACTIVE) {
                    SgPartydetails apdetails = sgPartylist[i];
                    if (apdetails != null && apdetails.getPartyId() != firstPartyId && apdetails.getPartyId() != secondParty) {%>
    <option value="<%=apdetails.getPartyId()%>"><%=apdetails.getPartyName()%></option>
    <%}

                }
            }
        }
    } else {
    %>
    <option value="-1" selected>Select Party</option>
    <%}%>
</select>