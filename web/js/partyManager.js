/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function strCompare(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
var waiting;
function pleasewait() {
    waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');
    //waiting = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"></div></div></div>');
    waiting.modal();
}
function showAlert(message, type, closeDelay) {
    if ($("#alerts-container").length == 0) {
        $("body")
                .append($('<div id="container" style="' +
                        'width: %; margin-left: 65%; margin-top: 10%;">'));
    }
    type = type || "info";
    var alert = $('<div class="alert alert-' + type + ' fade in">')
            .append(
                    $('<button type="button" class="close" data-dismiss="alert">')
                    .append("&times;")
                    )
            .append(message);
    $("#alerts-container").prepend(alert);
    if (closeDelay)
        window.setTimeout(function () {
            alert.alert("close")
        }, closeDelay);

}
function checkPartyAvailability() {
    var partyName = $("#_partyName").val();
    var s = './CheckPartyAvailability?_partyName=' + partyName;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data.result, "error") == 0) {
                $('#checkAvailability-result').html("<span><font color=red>" + data.message + "</font></span></small>");
            } else {
                $('#checkAvailability-result').html("<span><font color=blue>" + data.message + "</font></span></small>");
            }
        }
    });
}
function createParty() {
    var s = './CreateParty';
    var name = document.getElementById("_partyName").value;
    var companyName = document.getElementById("_companyName").value;
    var phoneNumber = document.getElementById("_phoneNumber").value;
    var emailId = document.getElementById("_emailId").value;
    if (name.trim() === '') {
        showAlert("Please enter party name", "danger", 3000);
        return;
    }
    if (companyName.trim() === '') {
        showAlert("Please enter company name", "danger", 3000);
        return;
    }
    if (phoneNumber.trim() === '' || phoneNumber.indexOf(' ') >= 0) {
        showAlert("Please enter phone number", "danger", 3000);
        return;
    }
    if (emailId.trim() === '' || emailId.indexOf(' ') >= 0) {
        showAlert("Please enter email Id", "danger", 3000);
        return;
    }
    pleasewait();
    $.ajax({
        type: 'POST',
        url: s,
        datatype: 'json',
        data: $("#party_form").serialize(),
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                waiting.modal('hide');
                //Alert4Users("<span><font color=red>" + data.message + "</font></span>");
                showAlert(data.message, "danger", 3000);
            } else if (strCompare(data.result, "success") === 0) {
                waiting.modal('hide');
                showAlert(data.message, "success", 3000);
                //Alert4Users("<span><font color=blue>" + data.message + "</font></span>");
                setTimeout(function () {
                    window.location.href = "./partyDetails.jsp";
                }, 3000);
            }
        }
    });
}
function getResourceForParties(value) {
    var s = './getResourceForParty.jsp?_apName=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_ResourceForParties').html(data);
            getParties();
        }
    });
}
function getParties() {
    var s = './getPartyForMap.jsp';
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_partiesDetails').html(data);
        }
    });
}
function VolumeCC() {
    value = document.getElementById("_selectParties").value;
    partyShare(value);
}
function partyShare(val) {
    if (val === '-1') {
//        var content =
//                '<div class=col-sm-3>' +
//                '<select id="CancellationCharge" name="CancellationCharge" class="form-control" onchange="VolumeCC()">' +
//                '<option selected value="Enable">Enable</option>' +
//                '<option value="Disable">Disable</option>' +
//                ' </select>' +
//                '</div>' +
//                '<div class=col-sm-3>' +
//                '<input type="text" id="CancellationChargeRate" name="CancellationChargeRate" onkeypress="return isNumberKey(event)" class="form-control" placeholder="Price">' +
//                '</div>'
//                ;
        document.getElementById('_partyShare').innerHTML = content;
    } else {
//        var content = ' <div class="col-sm-3">' +
//                '<select id="CancellationCharge" name="CancellationCharge" class="form-control" onchange="VolumeCC()">' +
//                ' <option value="Enable">Enable</option>' +
//                '<option selected value="Disable">Disable</option>' +
//                ' </select>'
//                + '</div>';
        var content = '<div class="form-group" style="padding: 1%">' +
                '<input type="text" class="form-control" id="_myResType" name="_myResType" placeholder="Value" >' +
                '</div>';
        document.getElementById('_partyShare').innerHTML = content;
    }
}

function partyCheck(value, resourceId) {
    var resourceOwnId = document.getElementById("resourceOwnId").value;
    var s = './getPartyForSecondSharing.jsp?_firstParty=' + value + "&_id=" + resourceOwnId + "&resourceId=" + resourceId;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_selectSecondParties').html(data);
            selectThirdParty(resourceId);
        }
    });
}
function selectThirdParty(resourceId) {
    var firstParty = document.getElementById("_selectFirstParties").value;
    var secondParty = document.getElementById("_selectSecondParties").value;
    var resourceOwnId = document.getElementById("resourceOwnId").value;
    var s = './getPartyForThirdSharing.jsp?_firstParty=' + firstParty + "&_secondParty=" + secondParty + "&_id=" + resourceOwnId + "&resourceId=" + resourceId;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_selectThirdParties').html(data);
            selectFourthParty(resourceId);
        }
    });
}
function selectFourthParty(resourceId) {
    var firstParty = document.getElementById("_selectFirstParties").value;
    var secondParty = document.getElementById("_selectSecondParties").value;
    var thirdParty = document.getElementById("_selectThirdParties").value;
    var resourceOwnId = document.getElementById("resourceOwnId").value;
    var s = './getPartyForFourthSharing.jsp?_firstParty=' + firstParty + "&_secondParty=" + secondParty + "&_thirdParty=" + thirdParty + "&_id=" + resourceOwnId + "&resourceId=" + resourceId;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_selectFourthParties').html(data);
        }
    });
}

function editPartyModel(companyName, partyEmailId, partyName, partyPhone, partyId) {
    document.getElementById("editPartyModel").innerHTML = "Edit " + partyName;
    document.getElementById("editPartyName").value = partyName;
    document.getElementById("editPartyPhone").value = partyPhone;
    document.getElementById("editPartyCompany").value = companyName;
    document.getElementById("editPartyEmailId").value = partyEmailId;
    document.getElementById("editPartyId").value = partyId;
    $('#editParty').modal();
}
function editParty() {
    var name = document.getElementById("editPartyName").value;
    var companyName = document.getElementById("editPartyCompany").value;
    var phoneNumber = document.getElementById("editPartyPhone").value;
    var emailId = document.getElementById("editPartyEmailId").value;
    if (name.trim() === '') {
        document.getElementById("edit-party-result").innerHTML = "<span style='color: red'>Please enter party name</span>";
        return;
    }
    if (companyName.trim() === '') {
        document.getElementById("edit-party-result").innerHTML = "<span style='color: red'>Please enter company name</span>";
        return;
    }
    if (phoneNumber.trim() === '' || phoneNumber.indexOf(' ') >= 0) {
        document.getElementById("edit-party-result").innerHTML = "<span style='color: red'>Please enter phone number</span>";
        return;
    }
    if (emailId.trim() === '' || emailId.indexOf(' ') >= 0) {
        document.getElementById("edit-party-result").innerHTML = "<span style='color: red'>Please enter email Id</span>";
        return;
    }
    $('#editParty').modal('hide');
    pleasewait();
    var s = './EditParty';
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#editPartyForm").serialize(),
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                waiting.modal('hide');
                if (data.hasOwnProperty("_message")) {
                    showAlert(data._message, "danger", 3000);
                } else {
                    showAlert(data.message, "danger", 3000);
                }
            } else {
                waiting.modal('hide');
                showAlert(data.message, "success", 3000);
                setTimeout(function () {
                    window.location.reload();
                }, 5000);
            }
        }
    });
}

function changePartyStatus(status, partyId) {
    pleasewait();
    var s = './ChangePartyStatus?status=' + status + '&partyId=' + partyId;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                waiting.modal('hide');
                showAlert(data.message, "danger", 3000);
            } else {
                waiting.modal('hide');
                showAlert(data.message, "success", 3000);
                setTimeout(function () {
                    window.location.reload();
                }, 5000);
            }
        }
    });
}