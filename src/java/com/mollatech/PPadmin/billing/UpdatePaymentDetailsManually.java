/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.billing;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.SgPaymentdetails;
import com.mollatech.serviceguard.nucleus.db.SgSapReceiptFile;
import com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PaymentManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.SAPBillManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "UpdatePaymentDetailsManually", urlPatterns = {"/UpdatePaymentDetailsManually"})
public class UpdatePaymentDetailsManually extends HttpServlet {

    static final Logger logger = Logger.getLogger(UpdatePaymentDetailsManually.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Request servlet is #UpdatePaymentDetailsManually from #PPAdmin at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();        
        String result = "success";
        String message = "Package have been subscriped successfully";        
        
        String paymentId      = request.getParameter("paymentId");
        String paymentDate    = request.getParameter("_paymentDate");
        String receiptNo      = request.getParameter("receiptNumber");
        String subscriptionId = request.getParameter("subscriptionId");
        String SessionId      = (String) request.getSession().getAttribute("_partnerSessionId");
        String ChannelId      = (String) request.getSession().getAttribute("_channelId"); 
        String sapBillPath    = (String) request.getSession().getAttribute("_sapBillFile");
        int payId = 0;
        int retValue = -1;
        int subscriptionintId = 0;
        try{
            if(sapBillPath == null){
                result = "error";
                message = "Please upload SAP bill file";
                json.put("_result", result);
                logger.debug("Response of UpdatePaymentDetailsManually Servlet's Parameter  result is " + result);
                json.put("_message", message);
                logger.debug("Response of UpdatePaymentDetailsManually Servlet's Parameter  message is " + message);
                return;
            }
            Path path = Paths.get(sapBillPath);
            byte[] sapBill = Files.readAllBytes(path);
            if(paymentId != null && !paymentId.isEmpty()){
               payId = Integer.parseInt(paymentId);
            }
            if(subscriptionId != null && !subscriptionId.isEmpty()){
               subscriptionintId = Integer.parseInt(subscriptionId);
            }
            SgPaymentdetails paymentObj = new PaymentManagement().getPaymentDetailsbyPaymentID(payId);

            if(paymentObj != null){
                paymentObj.setInvoiceNo(receiptNo);
                SimpleDateFormat formatter =  new SimpleDateFormat("MM/dd/yyyy");
                Date dt = formatter.parse(paymentDate);
                paymentObj.setPaidOn(dt);
                paymentObj.setPaymentStatus(GlobalStatus.PROCESSED);
                retValue = new PaymentManagement().updatePaymentDetails(paymentObj);                                
            }else{
                result = "error";
                message = "Payment details updation failed.";
                json.put("_result", result);
                logger.debug("Response of UpdatePaymentDetailsManually Servlet's Parameter  result is " + result);
                json.put("_message", message);
                logger.debug("Response of UpdatePaymentDetailsManually Servlet's Parameter  message is " + message);
                return;
            }            
            if(retValue != 0){
                result = "error";
                message = "Payment details updation failed.";
                json.put("_result", result);
                logger.debug("Response of UpdatePaymentDetailsManually Servlet's Parameter  result is " + result);
                json.put("_message", message);
                logger.debug("Response of UpdatePaymentDetailsManually Servlet's Parameter  message is " + message);
                return;
            }else{
                SgSubscriptionDetails subscriptionObj = new PackageSubscriptionManagement().getSubscriptionbyId(SessionId, ChannelId, subscriptionintId);
                if(subscriptionObj != null){
                    subscriptionObj.setStatus(GlobalStatus.PAID);
                    retValue = new PackageSubscriptionManagement().updateDetails(subscriptionObj);
                    if(retValue == 0){
                          SgSapReceiptFile sapFile = new SAPBillManagement().getSapReceiptByPartnerIdSubscribeId(subscriptionObj.getPartnerId(),subscriptionObj.getBucketId());
                          if(sapFile != null){
                              sapFile.setUpdatedOn(new Date());
                              sapFile.setDocuments(sapBill);
                              sapFile.setPaymentId(payId);
                              sapFile.setPartnerId(subscriptionObj.getPartnerId());
                              sapFile.setSubscriptionId(subscriptionintId);
                              retValue = new SAPBillManagement().updateSapReceiptFileDetail(SessionId, sapFile);
                          }else{
                              sapFile = new SgSapReceiptFile();
                              sapFile.setUpdatedOn(new Date());
                              sapFile.setCreatedOn(new Date());
                              sapFile.setDocuments(sapBill);
                              sapFile.setPaymentId(payId);
                              sapFile.setPartnerId(subscriptionObj.getPartnerId());
                              sapFile.setSubscriptionId(subscriptionintId);
                              retValue = new SAPBillManagement().addSAPBillDetails(sapFile);
                          }
                          if(retValue >= 0){
                            result  = "success";
                            message = "Payment details updated successfully.";
                            json.put("_result", result);
                            logger.debug("Response of UpdatePaymentDetailsManually Servlet's Parameter  result is " + result);
                            json.put("_message", message);
                            logger.debug("Response of UpdatePaymentDetailsManually Servlet's Parameter  message is " + message);
                            return;
                          }else{
                            result  = "error";
                            message = "SAP Bill Details filed to update.";
                            json.put("_result", result);
                            logger.debug("Response of UpdatePaymentDetailsManually Servlet's Parameter  result is " + result);
                            json.put("_message", message);
                            logger.debug("Response of UpdatePaymentDetailsManually Servlet's Parameter  message is " + message); 
                            return;
                          }
                    }else{
                        result = "error";
                        message = "Subscription details failed to update.";
                        json.put("_result", result);
                        logger.debug("Response of UpdatePaymentDetailsManually Servlet's Parameter  result is " + result);
                        json.put("_message", message);
                        logger.debug("Response of UpdatePaymentDetailsManually Servlet's Parameter  message is " + message);
                        return;
                    }
                }else{
                    result = "error";
                    message = "Subscription details failed to update.";
                    json.put("_result", result);
                    logger.debug("Response of UpdatePaymentDetailsManually Servlet's Parameter  result is " + result);
                    json.put("_message", message);
                    logger.debug("Response of UpdatePaymentDetailsManually Servlet's Parameter  message is " + message);                    
                }                
            }
        }catch(Exception e){
            e.printStackTrace();
            result = "error";
            message = "Payment details updation failed.";
            json.put("_result", result);
            logger.debug("Response of UpdatePaymentDetailsManually Servlet's Parameter  result is " + result);
            json.put("_message", message);
            logger.debug("Response of UpdatePaymentDetailsManually Servlet's Parameter  message is " + message);
        }finally{
            logger.info("Response of #UpdatePaymentDetailsManually from #PPAdmin " + json.toString());
            logger.info("Response of #UpdatePaymentDetailsManually from #PPAdmin Servlet at " + new Date());
            out.print(json);
            out.flush();
            out.close();
        }                
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
