<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgBucketdetails"%>
<%
    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
    String channelId = (String) request.getSession().getAttribute("_channelId");

    String accesspoint = request.getParameter("_apId");
    String resource = request.getParameter("_resourceId");
    String version = request.getParameter("versionData");
    String packageId = request.getParameter("packageIDFlat");

    int pId = 0;
    if (packageId != null) {
        pId = Integer.parseInt(packageId);
    }
    SgBucketdetails packageObj = new PackageManagement().getPackageDetails(SessionId, channelId, pId);
    String price = "";
    String jsonFlatPriceDetails = null;
    if (packageObj != null && packageObj.getFlatPrice() != null) {
        String flatPrice = packageObj.getFlatPrice();
        String key = accesspoint + ":" + resource + ":" + version;
        JSONArray jsOld = new JSONArray(flatPrice);
        for (int j = 0; j < jsOld.length(); j++) {
            JSONObject jsonexists1 = jsOld.getJSONObject(j);
            if (jsonexists1.has(key)) {
                jsonFlatPriceDetails = jsonexists1.getString(key);
                if (jsonFlatPriceDetails != null) {
                    break;
                }
            }
        }
        if(jsonFlatPriceDetails != null){            
            String[] priceDetails = jsonFlatPriceDetails.split(":");
            price = priceDetails[1];
        }
    }


%>   
<div class="col-lg-2">
    <select id="_flatPriceUnlimited" name="_flatPriceUnlimited"  class="form-control span2">
        <option value="-99" selected>Unlimited</option>                                                                                  
    </select>
</div>
<div class="col-lg-2">
    <div class="input-group">
        <span class="input-group-addon">$</span>
        <input type="text" class="form-control" id="apiFlatPrice" name="apiFlatPrice" placeholder="Price" value="<%=price%>" onkeypress="return isNumberKey(event)">                                    
    </div>
</div> 
