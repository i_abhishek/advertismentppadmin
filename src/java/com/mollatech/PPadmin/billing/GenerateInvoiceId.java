package com.mollatech.PPadmin.billing;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;


public class GenerateInvoiceId {

    private final Random random = new Random();

    private final int range;

    private int previous;

    GenerateInvoiceId(int range) {
        this.range = range;
    }

    int nextRnd() {
        if (previous == 0) {
            return previous = random.nextInt(range) + 1;
        }
        final int rnd = random.nextInt(range - 1) + 1;
        return previous = (rnd < previous ? rnd : rnd + 1);
    }

    public static String getDate() {
        String pattern = "yyddMM";
        SimpleDateFormat date = new SimpleDateFormat(pattern);
        return date.format(new Date());
    }

    public static String getRandom() {
        final GenerateInvoiceId t = new GenerateInvoiceId(4);
        String num = "";
        for (int i = 0; i < 4; i++) {
            num += t.nextRnd();
        }
        return num;
    }
}
