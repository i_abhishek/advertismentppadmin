
<%@page import="com.mollatech.serviceguard.nucleus.db.GroupDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.GroupManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%--<%@page import="portalconnect.PartnerPortalWrapper"%>--%>
<%@page import="java.util.List"%>
<%--<%@page import="com.partner.web.GroupDetails"%>--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <div class="control-group">
        <label class="control-label"  for="username"></label>
        <div class="controls">
            <select class="span10" name="_groupselect" id="_groupselect">
                <%
                   // PartnerPortalWrapper ppw = new PartnerPortalWrapper();
                    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
                    String channelId = (String) request.getSession().getAttribute("_channelId");
                    String apId = request.getParameter("_apId");
                    int acpId = Integer.parseInt(apId);
                    if (acpId != -1) {
                        int gpId = new AccessPointManagement().getGroupIDbyAceesspointID(SessionId, channelId, acpId);
                        GroupDetails g = new GroupManagement().getGroupDetails(SessionId, channelId, gpId);
                %>
                <option value="<%=g.getGroupId()%>"><%=g.getGroupName()%></option>
                <%} else {%>
                <option value="-1">Select All</option>
                <%
                    //List<GroupDetails> glist = null;
                    GroupDetails[] gdetails = null;
                    gdetails = new GroupManagement().getAllGroupDetails(SessionId, channelId);
//                    if (glist != null || !glist.isEmpty()) {
//                        gdetails = new GroupDetails[glist.size()];
//                        for (int i = 0; i < glist.size(); i++) {
//                            gdetails[i] = glist.get(i);
//                        }
//                    }
                    for (int i = 0; i < gdetails.length; i++) {
                %>
                <option value="<%=gdetails[i].getGroupId()%>"><%=gdetails[i].getGroupName()%></option>
                <%}
                    }%>
            </select>
        </div>
    </div>

