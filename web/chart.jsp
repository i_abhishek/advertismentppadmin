<%@page import="com.mollatech.serviceguard.nucleus.db.Monitortracking"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Monitorsettings"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.MonitorSettingsManagement"%>
<%@page import="javax.xml.datatype.DatatypeFactory"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="javax.xml.datatype.XMLGregorianCalendar"%>
<%--<%@page import="portalconnect.PartnerPortalWrapper"%>
<%@page import="com.partner.web.Monitortracking"%>
<%@page import="com.partner.web.Monitorsettings"%>--%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%
    //PartnerPortalWrapper ppw = new PartnerPortalWrapper();
    int types = 1;
    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
    String _settingId = request.getParameter("_settingId");
    String channelId = (String) request.getSession().getAttribute("_channelId");
    int settingId = Integer.parseInt(_settingId);
    Monitorsettings ms = new MonitorSettingsManagement().getMonitorSettingbyId(SessionId,channelId , settingId);
    int type = ms.getMonitorType();
    String settingName = ms.getMonitorName();
    String start = request.getParameter("_startdate");
    String end = request.getParameter("_enddate");
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    Date startdate = sdf.parse(start);
    GregorianCalendar st = new GregorianCalendar();
    st.setTime(startdate);
    XMLGregorianCalendar XMLsDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(st);
    Date enddate = sdf.parse(end);
    GregorianCalendar et = new GregorianCalendar();
    st.setTime(enddate);
    XMLGregorianCalendar XMLeDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(et);
    Monitortracking[] monitortrackings = new MonitorSettingsManagement().getMonitorTrackByNameDuration(SessionId, channelId, settingName, startdate, enddate, types);
%>
<script src="./js/raphael-min.js"></script>
<script src="./js/morris.js"></script>
<script src="./js/morris.min.js"></script>
<link rel="stylesheet" href="./css/morris.css">
<script>
    Morris.Line({
    element: 'lineCharts',
            data: [
    <%     if (monitortrackings != null) {
            for (int i = 0; i < monitortrackings.length; i++) {
                Monitortracking mt = (Monitortracking) monitortrackings[i];
                String per = mt.getPerformance();
                String c = per.replaceAll("ms", "");
                int a = Integer.parseInt(c);
                //XMLGregorianCalendar xcal = mt.getExecutionEndOn();
                Date xcal = mt.getExecutionEndOn();
                //java.util.Date d = xcal.toGregorianCalendar().getTime();
                DateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                String datess = formatter1.format(xcal);
    %>
            {y: '<%=datess%>', a: <%=a%>} <%if (i != monitortrackings.length) {%>,<%}%>
    <%
        }
    } else {
        Date d = new Date();
        DateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String date = formatter1.format(d);
    %>{y: '<%=date%>', a: 0}<%}%>
            ],
            xkey: 'y',
            ykeys: ['a'],
            xLabels: 'y',
            labels: ['value'],
            lineWidth: 2,
            pointSize: 3,
            postUnits: 'ms'
    });

</script>
<div  class="span12" style="alignment-adjust: central">
    <h5 align="center" class="text-primary" style="font-weight: bold">MONITORING REPORT</h5>
    <font style="font-family: monospace">
    Time in Milliseconds
    </font>
    <div id="lineCharts"></div>       
    <div  align="center" style="font-family: monospace" >
        Time
    </div>
</div>


