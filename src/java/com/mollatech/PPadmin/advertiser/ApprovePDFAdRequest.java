/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.advertiser;

import static com.mollatech.PPadmin.advertiser.RejectPDFAdRequest.SEND;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.connector.communication.SGStatus;
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.commons.MSConfig;
import com.mollatech.serviceguard.nucleus.db.SgAdvertiserAdDetails;
import com.mollatech.serviceguard.nucleus.db.SgAdvertiserDetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserAdManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.SettingsManagement;
import com.mollatech.serviceguard.nucleus.settings.SendNotification;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "ApprovePDFAdRequest", urlPatterns = {"/ApprovePDFAdRequest"})
public class ApprovePDFAdRequest extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "PDF Ad approved successfully";
        PrintWriter out = response.getWriter();
        String adId = request.getParameter("adId");        
        int iadId = 0;
        if (adId != null) {
            iadId = Integer.parseInt(adId);
        }
        int a = -1;       
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        if (SessionId != null) {
            SgAdvertiserAdDetails adDetails = new AdvertiserAdManagement().getAdById(iadId);
            SgAdvertiserDetails advertiserDetails = new AdvertiserManagement().getAdvertiserDetails(adDetails.getAdvertiserId());
            if (adDetails != null) {
                Integer currentStatus = adDetails.getPdfAdStatus();
                if(currentStatus == GlobalStatus.APPROVED){
                     result = "error";                
                    message = "PDF Ad already approved "; 
                    try {
                    json.put("_result", result);                    
                    json.put("_message", message);                    
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    out.print(json);
                    out.flush();
                    return;
                    }
                adDetails.setPdfAdStatus(GlobalStatus.APPROVED);
                adDetails.setPdfAdShownFlag(GlobalStatus.PDF_AD_REMAIN_TOSHOW);
                a = new AdvertiserAdManagement().updateDetails(adDetails);
            }
            if (a == 0) {
                result = "success";
                message = "PDF Ad approved successfully";
                
            String tmessage = LoadSettings.g_templateSettings.getProperty("email.pdf.ad.approved");
            if (tmessage != null) {
                tmessage = tmessage.replaceAll("#Name#", advertiserDetails.getAdvertisername());
                tmessage = tmessage.replaceAll("#channel#", "Advertiser Portal");                
                tmessage = tmessage.replaceAll("#datetime#", new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
                
                String enquiryId = (String) LoadSettings.g_sSettings.getProperty("email.enquiry.ad");
                String supportId = (String) LoadSettings.g_sSettings.getProperty("email.question.ad");
                String ideaId = (String) LoadSettings.g_sSettings.getProperty("email.idea.ad");
                String[] enquiryEmailDetails = enquiryId.split(":");
                String[] supportEmailDetails = supportId.split(":");
                String[] ideaEmailDetails = ideaId.split(":");
            
                MSConfig config = (MSConfig) new SettingsManagement().getSetting(SessionId, channelId, SettingsManagement.MATERSLAVE, SettingsManagement.PREFERENCE_ONE);
                String schemes = "http";
                if (config != null) {
                    if (config.apssl.equalsIgnoreCase("yes")) {
                        schemes = "https";
                    }
                }
                String path = request.getContextPath();
                String greenbackGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/greenback.gif";
                String spadeGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/spade.gif";
                String addressbookGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/addressbook.gif";
                String penpaperGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/penpaper.gif";
                String lightbulbGIF = schemes + "://" + config.aphostIp + ":" + config.aphostPort + path + "/images/email/lightbulb.gif";
                tmessage = tmessage.replace("#greenbackGIF#", greenbackGIF);
                tmessage = tmessage.replace("#spadeGIF#", spadeGIF);
                tmessage = tmessage.replace("#addressbookGIF#", addressbookGIF);
                tmessage = tmessage.replace("#penpaperGIF#", penpaperGIF);
                tmessage = tmessage.replace("#lightbulbGIF#", lightbulbGIF);

                tmessage = tmessage.replaceAll("#enquiryId#", enquiryId);
                tmessage = tmessage.replaceAll("#supportId#", supportId);
                tmessage = tmessage.replaceAll("#ideaId#", ideaId);

                tmessage = tmessage.replaceAll("#enquiryEmailLabel#", enquiryEmailDetails[1]);
                tmessage = tmessage.replaceAll("#supportEmailLabel#", supportEmailDetails[1]);
                tmessage = tmessage.replaceAll("#ideaEmailLabel#", ideaEmailDetails[1]);
            }
            SendEmailNotificationToAdvertiser signupNotification = new SendEmailNotificationToAdvertiser(tmessage, channelId, SessionId, advertiserDetails.getEmail(),"Your PDF advertisement approved", "Approved of PDF ad");
            Thread signupNotificationThread = new Thread(signupNotification);
            signupNotificationThread.start();
            result = "success";
            message = "PDF Ad request approved successfully.";
            } else {
                result = "error";
                message = "PDF Ad failed to approved";
            }
        }else{
            result = "error";
            message = "PDF Ad failed to approved";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);            
            return;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
