<%@page import="com.mollatech.serviceguard.nucleus.db.GroupDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.GroupManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgUsers"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement"%>
<%@page import="javax.xml.datatype.XMLGregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.nio.channels.Channels"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@include file="header.jsp" %>
<script src="./js/partnerRequest.js"></script>

<%    
    PartnerDetails[] sglist = new PartnerManagement().getAllPartnerDetails(SessionId, channelId);
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
    int count = 0;
    boolean flag = true;
%>
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header text-uppercase">Developer Management</h3>
            </div>
            <!-- /.col-lg-10 -->
        </div>
        <div class="row">
            <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47;  <i class="fa fa-table"></i><b> List of Developers</b>
                    </div>
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover display nowrap" id="partnerDetails">
                                <thead>
                                    <tr>
                                        <td/>
                                        <th><font style="font-size: 11px">No.</th>
                                        <th><font style="font-size: 11px">Developer</th>
                                        <!--                                        <th><font style="font-size: 11px">Company Name</th>                                                -->
                                        <th><font style="font-size: 11px">Group</th>
                                        <th><font style="font-size: 11px">IP</th>
                                        <th><font style="font-size: 11px">Manage</th>
                                        <!--                                        <th><font style="font-size: 11px">Mobile No</th>-->
                                        <th><font style="font-size: 11px">Email Id</th>
                                        <!--                                        <th><font style="font-size: 11px">Application</th>-->
                                        <!--<th><font style="font-size: 11px">Members</th>-->
                                        <th><font style="font-size: 11px">Created On</th>
                                        <th><font style="font-size: 11px">Updated On</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%if (sglist != null) {
                                            for (int i = 0; i < sglist.length; i++) {
                                                count++;
                                                String userStatus = "user-status-value-" + i;
                                                SgPartnerrequest preq = new PartnerRequestManagement().getPartnerRequestsPartnerbyId(SessionId, sglist[i].getPartnerId());

                                    %>
                                    <tr>
                                        <td/>
                                        <td ><font style="font-size:11px "><%=i + 1%></font></td>
                                        <td ><font style="font-size: 11px">
                                            <a href="./PartnerInfo.jsp?_partnerId=<%=sglist[i].getPartnerId()%>"><u><%= sglist[i].getPartnerName()%></u></a>
                                        </td>
                                        <!--                                        <td ><font style="font-size: 11px">
                                        <% if (preq == null) {%>No information<%} else {%>
                                        <%= preq.getComapanyRegName()%> <%}%></td>-->
                                        <% String gid = sglist[i].getPartnerGroupId();
                                            int groupid = Integer.parseInt(gid);
                                            GroupDetails grp = new GroupManagement().getGroupDetails(SessionId, channelId, groupid);
                                        %>                                                     
                                        <td ><font style="font-size: 11px"><a href="./GroupInfo.jsp?_groupId=<%=grp.getGroupId()%>"><u><%= grp.getGroupName()%></u></a></td>
                                        <td ><font style="font-size: 11px">
                                            <%= sglist[i].getAllowedIps()%>
                                        </td>                                                    
                                        <td >
                                            <div class="btn btn-group btn-xs">
                                                <button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" id="<%=sglist[i].getPartnerId()%>"><%if (sglist[i].getStatus() == GlobalStatus.ACTIVE) {%> <font style="font-size: 11px;">ACTIVE</font> <%} else {%><font style="font-size: 11px;"> SUSPENDED</font> <%}%><span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#"  onclick="changePstatus(<%=GlobalStatus.ACTIVE%>, <%=sglist[i].getPartnerId()%>, '<%=userStatus%>')" >Mark as Active?</a></li>
                                                    <li><a href="#" onclick="changePstatus(<%=GlobalStatus.SUSPEND%>, <%=sglist[i].getPartnerId()%>, '<%=userStatus%>')" >Mark as Suspended?</a></li>                                                               
                                                        <% if (preq != null && preq.getDocment() != null) {%>
                                                    <li><a class="divider"></a></li>
                                                    <li>                                   
                                                        <a href="#" onclick="downloadDocments('<%=sglist[i].getPartnerId()%>')"><i class="fa fa-download"></i>  Download Documents   </a>                                                                                                                                                              
                                                    </li>
                                                    <%}%>  
                                                    <li><a class="divider"></a></li>
                                                        <%
                                                            String userid = null;
                                                            int partnerid = sglist[i].getPartnerId();
                                                            UsersManagement um = new UsersManagement();
                                                            SgUsers[] users = um.getSgUsersByPartnerId(SessionId, channelId, partnerid);
                                                            if (users != null) {
                                                                if (users.length != 0) {
                                                                    for (int a = 0; a < users.length; a++) {
                                                                        if (users[a].getType() == 0) {
                                                                            userid = users[a].getUserid();
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        %>
                                                    <li><a href="#" onclick="sendrandompassword('<%=userid%>')" >Send Random Password</a></li>                                                               
                                                </ul>
                                            </div>
                                        </td>
<!--                                        <td ><font style="font-size: 11px"><%= sglist[i].getPartnerPhone()%></font></td>-->
                                        <td ><font style="font-size: 11px">
                                            <%= sglist[i].getPartnerEmailid()%></font>

                                            <!--                                        <td >                                                       
                                                                                        <font style="font-size: 11px"><a class="btn btn-info" ><i class="fa fa-eye"></i> view</a> </font>                                                      
                                                                                    </td>-->

                                            <%
                                                Date date = sglist[i].getCreatedon();
                                            %>
                                        <td ><font style="font-size: 11px"><%=sdf.format(date)%></td>
                                            <%
                                                date = sglist[i].getLastupdatedon();
                                            %>
                                        <td ><font style="font-size: 11px"><%=sdf.format(date)%></td>
                                    </tr> 
                                    <%}
                                        }
                                    %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript 
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.min.js"></script>
<script src="js/morris-data.js"></script>-->

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->

</body>
</html>
<%@include file="footer.jsp" %>
