<%@page import="com.mollatech.serviceguard.nucleus.db.Warfiles"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MethodName"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Classes"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Methods"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <div class="control-group">
        <label class="control-label"  for="username"></label>
        <div class="controls">
            <select id="_APIForSlabResource2" name="_APIForSlabResource2"  class="form-control span2" onchange="showSlabAPI(this.value)" style="width: 100%">
                <%
                    AccessPointManagement ppw = new AccessPointManagement();
                    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
                    String channelId = (String) request.getSession().getAttribute("_channelId");
                    String apId = request.getParameter("_resourceId");
                    String appId = request.getParameter("_apId");
                    //int acppId = Integer.parseInt(appId);
                    if(!apId.equals("-1") && !appId.equals("-1")){
                    Accesspoint accesspoint = ppw.getAccessPointByName(SessionId, channelId, appId);
                    Warfiles warFiles=new WarFileManagement().getWarFile(SessionId, channelId, accesspoint.getApId());
                    int version = 1;
                    if(warFiles != null){
                        Map warMap=(Map)(Serializer.deserialize(warFiles.getWfile()));
                        version = warMap.size()/2;
                        for(int i = 1; i <= version; i++){             
                %>
                <option value="<%=i%>"><%=i%></option>               

                    <%}
                        }    
                            }else {%>
                    <option value="-1">Select Version</option>  
                <% }%>
                </select>         
        </div>
    </div>