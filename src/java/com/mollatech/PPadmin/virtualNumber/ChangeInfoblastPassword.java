/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.virtualNumber;

import com.mollatech.infoblastservice.SdpConnectRsp;
import com.mollatech.infoblastservice.SendUserCodeRsp;
import com.mollatech.serviceguard.nucleus.db.SgNumbers;
import com.mollatech.serviceguard.nucleus.db.connector.management.NumbersManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "ChangeInfoblastPassword", urlPatterns = {"/ChangeInfoblastPassword"})
public class ChangeInfoblastPassword extends HttpServlet {

    static final Logger logger = Logger.getLogger(ChangeInfoblastPassword.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        logger.info("Requested Servlet is ChangeInfoblastPassword at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        String result = "success";
        String message = "Virtual number password change successfully!!!";
        JSONObject json = new JSONObject();
        int numberID = Integer.parseInt(request.getParameter("numberId"));
        String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        logger.debug("value of numberId : " + numberID);
        int res = -1;
        SgNumbers numberData = new NumbersManagement().getMobileNumberById(sessionId, numberID);
        if (numberData != null) {
            SdpConnectRsp connectRes = new InfoblastServiceUtil().getSDPConnect();
            if (connectRes == null) {
                result = "error";
                message = " IB sdpConnect service failed to connect !";
                json.put("result", result);
                json.put("message", message);
                logger.info("ChangeInfoblastPassword result value " + result);
                logger.info("ChangeInfoblastPassword message value " + message);
                out.print(json);
                out.flush();
                out.close();
                return;
            } else if (connectRes.getStatus() != 0) {
                result = "error";
                message = connectRes.getStatusDesc();
                json.put("result", result);
                json.put("message", message);
                logger.info("ChangeInfoblastPassword result value " + result);
                logger.info("ChangeInfoblastPassword message value " + message);
                out.print(json);
                out.flush();
                out.close();
                return;
            }
            String infoBlastPassword = randomAlphaNumericPassw(8);
            SendUserCodeRsp sendUserCodeRes = new InfoblastServiceUtil().sendUserCode(connectRes.getSessionId(), numberData.getMobileNo(), "P", infoBlastPassword);
            if (sendUserCodeRes == null) {
                result = "error";
                message = " IB sdpUserCode service failed to connect !";
                json.put("result", result);
                json.put("message", message);
                logger.info("ChangeInfoblastPassword result value " + result);
                logger.info("ChangeInfoblastPassword message value " + message);
                out.print(json);
                out.flush();
                out.close();
                return;
            } else if (sendUserCodeRes.getStatus() != 0) {
                result = "error";
                message = sendUserCodeRes.getStatusDesc();
                json.put("result", result);
                json.put("message", message);
                logger.info("ChangeInfoblastPassword result value " + result);
                logger.info("ChangeInfoblastPassword message value " + message);
                out.print(json);
                out.flush();
                out.close();
                return;
            }
            numberData.setInfoblastPassword(infoBlastPassword);
            res = new NumbersManagement().updateDetails(numberData);
        }
        if (res == 0) {
            result = "success";
            message = "Virtual number password change successfully!!!";
        } else {
            result = "error";
            message = "Virtual number password change failed!!!";
        }
        try {
            json.put("result", result);
            json.put("message", message);
            logger.info("ChangeInfoblastPassword result value " + result);
            logger.info("ChangeInfoblastPassword message value " + message);
            return;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
            out.close();
        }
    }
    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    public static String randomAlphaNumericPassw(int count) {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }



// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
/**
 * Handles the HTTP <code>GET</code> method.
 *
 * @param request servlet request
 * @param response servlet response
 * @throws ServletException if a servlet-specific error occurs
 * @throws IOException if an I/O error occurs
 */
@Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
        public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
