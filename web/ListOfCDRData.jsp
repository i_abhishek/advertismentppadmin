<%@page import="com.fasterxml.jackson.databind.ObjectMapper"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.bouncycastle.util.encoders.Base64"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.UtilityFunctions"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.CDRManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgCdrData"%>
<!DOCTYPE html>
<%@include file="header.jsp" %>
<html lang="en">
    <head>
        <script src="bower_components/jquery/dist/jquery.min.js"></script>
        <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
        <script src="dist/js/sb-admin-2.js"></script>
        <script src="js/bootbox.min(1).js" type="text/javascript"></script>
        <script src="js/bootbox.min.js" type="text/javascript"></script>
        <script src="js/CDR.js" type="text/javascript"></script>
    </head>

    <%
        SgCdrData[] datas = new CDRManagement().getDetails();
    %>

    <body>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header text-uppercase">CDR Management</h3>
                </div>
            </div>
            <div class="row">
                <div id="alerts-container" style="width: 50%; left: 25%; top: 2%;margin-left: 25%"></div>
                <div class="col-lg-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <i class="fa fa-home"></i><a href="home.jsp"> Home </a>&#47;  <i class="fa fa-table"></i><b> CDR List</b>
                        </div>
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover display nowrap" id="partnerDetails">
                                    <thead>
                                        <tr>
                                            <td/>
                                            <th><font style="font-size: 11px">No.</th>
                                            <th><font style="font-size: 11px">Status</th>
                                            <th><font style="font-size: 11px">Manage</th>
                                            <th><font style="font-size: 11px">Details</th>
                                            <th><font style="font-size: 11px">Created On</th>
                                            <th><font style="font-size: 11px">Updated On</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                            if (datas != null) {
                                                int count = 1;
                                                for (SgCdrData data : datas) {
                                                    String status = "<span class='label label-success'>Processed</span>";
                                                    String strStatus = "Processed";
                                                    if (data.getStatus() == GlobalStatus.NOT_PROCESSED) {
                                                        status = "<span class='label label-danger'>Not Processed</span>";
                                                    } else if (data.getStatus() == GlobalStatus.START_PROCESS) {
                                                        status = "<span class='label label-info'>Processeing</span>";
                                                        strStatus = "Processing";
                                                    }
                                                    JSONArray newArray = new JSONArray();
                                                    JSONArray array = new JSONArray(data.getCdr());
                                                    for (int i = 0; i < array.length(); i++) {
                                                        JSONObject json = new JSONObject(array.getString(i));
                                                        JSONObject newJson = new JSONObject();
                                                        newJson.put("Number", json.get("Calling Party"));
                                                        newJson.put("Total Call Count", json.get("RoundUp"));
                                                        newArray.put(newJson);
                                                    }
                                                    ObjectMapper mapper = new ObjectMapper();
                                                    Object json = mapper.readValue(newArray.toString(), Object.class);
                                                    String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);

                                        %>
                                        <tr>
                                            <td/>
                                            <td><%=count%></td>
                                            <td><%=status%></td>
                                            <td>
                                                <div class="btn btn-group btn-xs">
                                                    <button class="btn btn-default btn-xs dropdown-toggle" id ="myBtn<%=count%>" data-toggle="dropdown">
                                                        <%if (data.getStatus() == GlobalStatus.PROCESSED || data.getStatus() == GlobalStatus.START_PROCESS) {%> 
                                                        <font style="font-size: 11px;"><%=strStatus%></font> 
                                                        <script>
                                                            document.getElementById("myBtn<%=count%>").disabled = true;
                                                        </script>
                                                        <%} else if (data.getStatus() == GlobalStatus.NOT_PROCESSED) {%>
                                                        <font style="font-size: 11px;"> Not Processed</font> 
                                                        <%}%>
                                                        <span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#" onclick="startProcessing(<%=data.getId()%>,<%=GlobalStatus.START_PROCESS%>)">Start Processing?</a></li>
                                                        <li><a href="#" onclick="removeData(<%=data.getId()%>,<%=GlobalStatus.DELETED%>)"><font color="red">Remove</font></a></li>
                                                    </ul>
                                                </div>
                                            </td>
                                            <td><a class="btn btn-info btn-xs" onclick="showCDRDetails('<%=new String(Base64.encode(indented.getBytes()))%>')">View</a></td>
                                            <td><%=UtilityFunctions.getTMReqDate(data.getCreatedOn())%></td>
                                            <td><%=UtilityFunctions.getTMReqDate(data.getUpdatedOn())%></td>
                                        </tr>
                                        <% count++;
                                                }
                                            }
                                        %>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="editParty" class="modal fade" tabindex="-1" role="dialog"  aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                        <b id="editPartyModel">CDR Details.</b>
                    </div>          
                    <div class="modal-body">
                        <div class="row-fluid">
                            <form class="form-horizontal" id="editPartyForm" name="editPartyForm">
                                <fieldset>
                                    <div class="control-group ">
                                        <label class="control-label col-lg-3">Details</label>
                                        <div class="col-lg-7">
                                            <textarea id="cdrDetails" rows="10" cols="40" name="cdrDetails">
                                                
                                            </textarea>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div id="edit-party-result"></div>
                        <button class="btn btn-info btn-xs" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<%@include file="footer.jsp" %>