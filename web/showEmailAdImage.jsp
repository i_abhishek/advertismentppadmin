<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AdvertiserAdManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgAdvertiserAdDetails"%>
<%
    String id = request.getParameter("_id");
    SgAdvertiserAdDetails adDetails = new AdvertiserAdManagement().getAdById(Integer.parseInt(id));
%>        
<div id="emailAdModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="rejectPackageModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 id="rejectPackageModal">Email Ad</h4>
            </div>          
            <div class="modal-body">
                <div class="row-fluid">
                    <form class="form-horizontal" id="rejectPackageForm">
                        <fieldset>
                            <div class="control-group">                                
                                <input type="hidden"  id="_adId" name="_adId" >
                                
                                <div class="controls col-lg-12" style="margin-left: 1%">                                    
                                    <h4>Ad Title - <%=adDetails.getEmailAdTitle()%></h4>
                                </div>
                                <div class="controls col-lg-12" style="margin-left: 1%">    
                                <h4>Ad Logo - </h4>
                                </div>
                                <div class="controls col-lg-12" style="margin-left: 35%; margin-bottom: 5%">                                    
                                    <image src="data:image/jpg;base64,<%=adDetails.getEmailAdImage()%>" alt="PDF Ad Image" width="150" height="150"> 
                                </div>
                                <br><br><br>
                                <div class="controls col-lg-12" style="margin-left: 1%">    
                                <h4>Ad Message - </h4>
                                </div>
                                <div class="controls col-lg-12" style="margin-left: 1%">                                    
                                    <p><%=adDetails.getEmailAdContent()%></p>
                                </div>
                            </div>                            
                        </fieldset>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div id="edit-partner-result"></div>
                <button class="btn btn-info btn-xs" data-dismiss="modal">Close</button>
                <button class="btn btn-success btn-xs" onclick="rejectEmailAdmodal('<%=GlobalStatus.REJECTED%>', '<%=adDetails.getAdvertiserAdId()%>')" id="addPartnerButtonE">Reject request</button>
            </div>
        </div>
    </div>
</div>
