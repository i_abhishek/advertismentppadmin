/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    function slabPriceDiv(value) {
        if (value === 'Enable') {
            document.getElementById('apSlabrate').style.display = 'block';
        } else {
            document.getElementById('apSlabrate').style.display = 'none';
        }
    }

    function loanPriceDiv(value) {
        if (value === 'Enable') {
            document.getElementById('loanDiv').style.display = 'block';
        } else {
            document.getElementById('loanDiv').style.display = 'none';
        }
    }

    function creditPointDiv(value) {
        if (value === 'Enable') {
            document.getElementById('creditDiv').style.display = 'block';
        } else {
            document.getElementById('creditDiv').style.display = 'none';
        }
    }
    function accesspointChangeAPIPrice(value) {
        var s = './accessPointWiseAPI.jsp?_apId=' + value;
        $.ajax({
            type: 'GET',
            url: s,
            success: function (data) {
                $('#_APIForSlabPricing2').html(data);
            }
        });
    }


    function acesspointChangeAPI(value) {
        var s = './accessPointChangeAPI.jsp?_apId=' + value;
        $.ajax({
            type: 'GET',
            url: s,
            success: function (data) {
                $('#_APIForSlabPricing2').html(data);
                showSlabPricing(value);
            }
        });
    }
    function accesspointChangePrice(value, versionData, _resourceId, packageId) {
        var s = './accessPointChangeBilling.jsp?_apId=' + value + '&versionData=' + versionData + '&_resourceId=' + _resourceId + '&_bucketId=' + packageId;
        if (value != -2) {
            $.ajax({
                type: 'GET',
                url: s,
                success: function (data) {
                    $('#listOfAPI').html(data);
                }
            });
        } else {
            $('#addPackageTab a[href="#apRate"]').tab('show');
        }
    }



    // show resourec as per ap
    function showSlabPricing(apName, _resourceName, versionData, apiName, packageName) {
        var s = './getSlabPriceWindow.jsp?apName=' + apName + '&_resourceName=' + _resourceName + '&versionData=' + versionData + '&apiName=' + apiName + '&packageName=' + packageName;
        $.ajax({
            type: 'GET',
            url: s,
            success: function (data) {
                $('#_slabPriceWindow').html(data);
                // acesspointChangeAPIPrice(value);
            }
        });
    }

    function showSlabAPI(versionData, ap, _resourceId, packageName) {
        var s = './showReqAPI.jsp?versionData=' + versionData + '&_apName=' + ap + '&_resourceName=' + _resourceId + '&_packageName=' + packageName;
        $.ajax({
            type: 'GET',
            url: s,
            success: function (data) {
                $('#_APIForSlabPricing2').html(data);
                var apiName = document.getElementById('_APIForSlabPricing2').value;
                // showSlabAPI(versionData,ap);
                showSlabPricing(ap, _resourceId, versionData, apiName, packageName);
            }
        });
    }
    function showSlabVersion(value, ap, packageName) {
        var s = './getSlabVersion.jsp?_resourceName=' + value + '&_apname=' + ap + '&_packageName=' + packageName;
        $.ajax({
            type: 'GET',
            url: s,
            success: function (data) {
                $('#_VersionForSlabPricing2').html(data);
                var versionData = document.getElementById('_VersionForSlabPricing2').value;
                showSlabAPI(versionData, ap, value, packageName);
            }
        });
    }
    function acesspointChangeResource(value) {
        var packageName = document.getElementById("oldPackageDetails").value;
        var s = './getSlabResource.jsp?_apName=' + value + '&_packageName=' + packageName;
        $.ajax({
            type: 'GET',
            url: s,
            success: function (data) {
                $('#_ResourceForSlabPricing2').html(data);
                var res = document.getElementById('_ResourceForSlabPricing2').value;
                //alert(res+"res");
                var ap = document.getElementById('_Accesspoint2').value;
                //alert(ap+"ap");
                showSlabVersion(res, ap, packageName);
            }
        });
    }

    //ap details
    function showAPAPI(versionData, ap, _resourceId, packageId) {
        var s = './slabAPI.jsp?versionData=' + versionData + '&_apId=' + ap + '&_resourceId=' + _resourceId;
        accesspointChangePrice(ap, versionData, _resourceId, packageId);
    }

    function accesspointPrice(packageName) {
        var _apname = document.getElementById('_Accesspoint12').value;
        var versionData = document.getElementById('_VersionForAPricing2').value;
        var _resourceId = document.getElementById('_ResourceForAPPricing2').value;
        var s = './accessPointPriceBilling.jsp?_apname=' + _apname + '&versionData=' + versionData + '&_resourceName=' + _resourceId + '&_packageName=' + packageName;

        $.ajax({
            type: 'GET',
            url: s,
            success: function (data) {
                $('#listOfAPI').html(data);
            }
        });

    }
    function showAPVersion(value, packageName) {
        var ap = document.getElementById('_Accesspoint12').value;
        var s = './showReqVersion.jsp?_resourceName=' + value + '&_apname=' + ap + '&_packageName=' + packageName;
        $.ajax({
            type: 'GET',
            url: s,
            success: function (data) {
                $('#_VersionForAPricing2').html(data);
                var versionData = document.getElementById('_VersionForAPricing2').value;
                //showAPAPI(versionData,ap,value,packageId);
                accesspointPrice(packageName);
            }
        });
    }
    function acesspointResource(value, packageName) {
        var s = './showReqResource.jsp?_apName=' + value + '&_packageName=' + packageName;
        $.ajax({
            type: 'GET',
            url: s,
            success: function (data) {
                $('#_ResourceForAPPricing2').html(data);
                var res = document.getElementById('_ResourceForAPPricing2').value;
                //alert(res+"res");
                var ap = document.getElementById('_Accesspoint12').value;
                //alert(ap+"ap");
                showAPVersion(res, packageName);
            }
        });
    }



function acessChangeResForTiering() {
    var value = document.getElementById("_Accesspoint3").value;
    var packageName = document.getElementById("oldPackageDetails").value;
    var s = './getTieringResource.jsp?_apName=' + value+'&packageName='+packageName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_ResourceForTieringPricing2').html(data);
            var res = document.getElementById('_ResourceForTieringPricing2').value;
            var ap = document.getElementById('_Accesspoint3').value;
            showTieringVersion(res);
        }
    });
}

function showTieringVersion(value) {
    var ap = document.getElementById("_Accesspoint3").value;
    var packageName = document.getElementById("oldPackageDetails").value;
    var s = './getTieringVersion.jsp?packageName=' + packageName +'&_apName=' +ap;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_VersionForTieringPricing2').html(data);
            var versionData = document.getElementById('_VersionForTieringPricing2').value;
            showTieringAPI(versionData, ap, value);
        }
    });
}

function showTieringAPI(versionData) {
    var ap = document.getElementById("_Accesspoint3").value;
    var packageName = document.getElementById("oldPackageDetails").value;
    var _resourceId = document.getElementById("_ResourceForTieringPricing2").value;
    var s = './getTieringAPI.jsp?packageName=' + packageName +'&_apName=' +ap;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_APIForTieringPricing2').html(data);
            var apiName = document.getElementById('_APIForTieringPricing2').value;
            // showSlabAPI(versionData,ap);
            showTieringPricing(apiName);
        }
    });
}

function showTieringPricing(value) {

    var accesspointName = document.getElementById("_Accesspoint3").value;
    var resourceName = document.getElementById("_ResourceForTieringPricing2").value;
    var version = document.getElementById("_VersionForTieringPricing2").value;
    var packageID = document.getElementById("packageID").value;
    var packageName = document.getElementById("oldPackageDetails").value;
    var s = './getTierPricingDetails.jsp?apName=' + accesspointName + '&_resourceName=' + resourceName + '&version=' + version + '&apiName=' + value +'&packageName=' +packageName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_tieringPriceWindow').html(data);

        }
    });
}

function tieringPriceDiv(value) {
    if (value === 'Enable') {
        document.getElementById('apTieringrate').style.display = 'block';
    } else {
        document.getElementById('apTieringrate').style.display = 'none';
    }
}

function throttlingResource(value, packageName) {
    var s = './showRequestedThrottlingResource.jsp?_apName=' + value + '&_packageName=' + packageName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_resourceAPIThrottling').html(data);
            var res = document.getElementById('_resourceAPIThrottling').value;
            
            throttlingVersion(res, packageName);
        }
    });
}

function throttlingVersion(value, packageName) {
    var ap = document.getElementById('_throttlingAccesspoint').value;
    var s  = './showRequestedThrottlingVersion.jsp?_resourceName=' + value + '&_apname=' + ap + '&_packageName=' + packageName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#_versionofAPIThrottling').html(data);                        
            throttlingAPIDetails(packageName);
        }
    });
}

function throttlingAPIDetails(packageName) {
    var _apname = document.getElementById('_throttlingAccesspoint').value;
    var versionData = document.getElementById('_versionofAPIThrottling').value;
    var _resourceId = document.getElementById('_resourceAPIThrottling').value;
    var s = './showAPIThrottlingDetails.jsp?_apname=' + _apname + '&versionData=' + versionData + '&_resourceName=' + _resourceId + '&_packageName=' + packageName;

    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#listOfthrottlingAPI').html(data);
        }
    });
}