package com.mollatech.PPadmin.handler;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.Channels;
import com.mollatech.serviceguard.nucleus.db.Operators;
import com.mollatech.serviceguard.nucleus.db.connector.ChannelsUtils;
import com.mollatech.serviceguard.nucleus.db.connector.RemoteAccessUtils;
import com.mollatech.serviceguard.nucleus.db.connector.SessionFactoryUtil;
import com.mollatech.serviceguard.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.SessionManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Session;
import org.json.simple.JSONObject;

public class operatorLogin extends HttpServlet {

    final String itemType = "LOGIN";

    final int SUCCESS = 0;

    final int FAILED = -1;

    final String BLOCKED = "BLOCKED_IP";

    final int BANKOPERATOR = 0;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Successful credential verification.";
        String url = "home.jsp";
        PrintWriter out = response.getWriter();
        String _channelName = "ServiceGuard";
        SessionFactoryUtil suChannel = new SessionFactoryUtil(SessionFactoryUtil.channels);
        Session sChannel = suChannel.openSession();
        ChannelsUtils cUtil = new ChannelsUtils(suChannel, sChannel);
        SessionFactoryUtil suRemoteAcess = new SessionFactoryUtil(SessionFactoryUtil.remoteaccess);
        Session sRemoteAcess = suRemoteAcess.openSession();
        RemoteAccessUtils rUtil = new RemoteAccessUtils(suRemoteAcess, sRemoteAcess);
        Channels channel = cUtil.getChannel(_channelName);
        String[] credentialInfo = rUtil.GetRemoteAccessCredentials(channel.getChannelid());
        String portalSessionId = new SessionManagement().OpenSession(channel.getChannelid(), credentialInfo[0], credentialInfo[1], request.getSession().getId());
        if (portalSessionId == null) {
            System.out.println("\n Session not created ");
        }
        if (portalSessionId == null) {
            result = "error";
            message = "Server is Down";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        try {
            String _operatorName = request.getParameter("_name");
            String _password = request.getParameter("_passwd");
            if (_operatorName == null || _password == null) {
                result = "error";
                message = "Enter Proper Credentials";
                url = "index.jsp";
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
                out.print(json);
                out.flush();
                return;
            }
            int retValue = -10;
            Operators opObj = null;
            try {
                if (portalSessionId != null) {
                    retValue = -1;
                    opObj = new OperatorsManagement().GetOperatorInner(portalSessionId, channel.getChannelid(), _operatorName);
                    if (opObj != null) {
                        opObj = new OperatorsManagement().VerifyCredential(portalSessionId, channel.getChannelid(), _operatorName, _password);
                    }
                    if (opObj != null) {
                        //if (opObj.getStatus() != -1) {
                        if (opObj.getStatus() != GlobalStatus.LOCKED) {    
                            retValue = 0;
                        } else {
                            retValue = -1;
                        }
                    } else {
                        retValue = -1;
                    }
                    if (opObj != null) {
                        if (opObj.getStatus() != GlobalStatus.ACTIVE) {
                            result = "error";
                            switch (opObj.getStatus()) {
                                //case -1:
                                case GlobalStatus.LOCKED:    
                                    message = "Account is locked as many wrong attempts were made";
                                    retValue = 0;
                                    break;
                                //case 0:
                                case GlobalStatus.SUSPEND:    
                                    message = "Account is suspended";
                                    break;
                                case GlobalStatus.DELETED:
                                    message = "Account is removed, please contact administrator";
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
                if (retValue == 0) {
                    HttpSession session = request.getSession(true);
                    session.setAttribute("_operatorType", BANKOPERATOR);
                    session.setAttribute("_apOprAuth", "yes");
                    session.setAttribute("_apOprDetail", opObj);
                    session.setAttribute("_partnerSessionId", portalSessionId);
                    session.setAttribute("_channelId", channel.getChannelid());
                } else {
                    HttpSession session = request.getSession(true);
                    session.setAttribute("_apOprAuth", null);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                result = "error";
                message = ex.getLocalizedMessage();
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                }
                out.print(json);
                out.flush();
            }
            if (retValue != 0) {
                result = "error";
                //if (opObj != null && opObj.getStatus() != -1) {
                if (opObj != null && opObj.getStatus() != GlobalStatus.LOCKED) {    
                    message = "Invalid Credentials  Click here to <a href=\"#\" onclick=\"forgotpassword()\" >Get Password</a>";
                } else {
                    message = "Invalid Credentials ";
                }
                url = "index.jsp";
            }
            if (retValue == 10) {
                result = "error";
                message = "Unable to connect ";
            }
            try {
                json.put("_result", result);
                json.put("_message", message);
                json.put("_url", url);
            } finally {
                out.print(json);
                out.flush();
                return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            result = "error";
            message = ex.getLocalizedMessage();
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
            }
            out.print(json);
            out.flush();
            return;
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
