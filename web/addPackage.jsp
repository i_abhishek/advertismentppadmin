<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@include file="header.jsp" %>
<script src="js/designPackage.js" type="text/javascript"></script>
<div id="page-wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add Package</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-home"></i><a href="home.jsp"> Home</a> &#47;
                    <i class="fa fa-shopping-cart"></i><a href="package.jsp"> Package Details</a> &#47;
                    <i class="fa fa-edit"></i> Package Details
                </div>
                <div class="panel-body">
                    <h4>Package details</h4>
                    <hr>                   
                    <form class="form-horizontal" id="register_form" role="form">
                        <div class="col-lg-3">
                            <div class="form-group input-group">
                                <span class="input-group-addon"><i class="fa fa-shopping-cart"></i></span>
                                <input type="text" class="form-control" placeholder="Package name">
                            </div>
                        </div>                                
                        <div class="col-lg-3">
                            <div class="form-group input-group">
                                <span class="input-group-addon">$</span>
                                <input type="text" class="form-control" placeholder="Price">                                    
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <select class="form-control">
                                    <option>Select Duration</option>
                                    <option>Daily</option>
                                    <option>Weekly</option>
                                    <option>Monthly</option>
                                    <option>Quarterly</option>
                                    <option>Half yearly</option>
                                    <option>Yearly</option>
                                </select>                                    
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group input-group">
                                <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                <select class="form-control">
                                    <option>Select Payment type</option>
                                    <option>Prepaid</option>
                                    <option>PostPaid</option>
                                </select>                                    
                            </div>
                        </div>
                        <ul class="nav nav-pills" id="addPackageTab">
                            <li class="active"><a href="#rate" data-toggle="tab">Rate</a></li>                    
                            <li><a href="#apRate" data-toggle="tab">Access Point Rate</a></li>
                            <li><a href="#promoCode" data-toggle="tab">Slab Pricing</a></li>
                            <li><a href="#alerts" data-toggle="tab">Security & Alerts</a></li>
                            <li><a href="#feature" data-toggle="tab">Feature</a></li>
                        </ul>
                        <br>
                        <div class="tab-content">
                            <!-- Rate Tab -->
                            <div class="tab-pane fade in active" id="rate">
                                <div class="form-group">                                    
                                        <label class="col-lg-3 control-label" >Minimum balance</label>
                                        <div class="col-lg-2">
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control" placeholder="Price">                                    
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">                                    
                                    <label class="col-lg-3 control-label" >Free Credits</label>
                                    <div class="col-lg-2">
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control" placeholder="Price">                                    
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">                                    
                                    <label class="col-lg-3 control-label">Free Period</label>
                                    <div class="col-lg-2">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <select class="form-control" >
                                                <option>Days</option>                                                
                                                <option>Week</option>
                                                <option>2 Week</option>
                                                <option>1 Month</option>
                                            </select>                                                    
                                        </div>
                                    </div>                                    
                                </div>
                                <div class="form-group">                            
                                    <label class="col-lg-3 control-label">1st Setup Service Charge</label>
                                    <div id="_ServiceCharge">
                                    </div>
                                </div>
                                <div class="form-group">                            
                                    <label class="col-lg-3 control-label">Change Package Charge</label>
                                    <div id="_ChangePackageCharge">
                                    </div>
                                </div>
                                <div class="form-group">                            
                                    <label class="col-lg-3 control-label">Reactivation Charge</label>
                                    <div id="_ReactivationCharge">
                                    </div>
                                </div>
                                <div class="form-group">                            
                                    <label class="col-lg-3 control-label">Cancellation Charge</label>
                                    <div id="_CancellationCharge">
                                    </div>
                                </div>
                                <div class="form-group">                            
                                    <label class="col-lg-3 control-label">Late Penalty Charge</label>
                                    <div id="_LatePenaltyCharge">
                                    </div>
                                </div>
                                <div class="form-group">
                                   <label class="col-lg-3 control-label">Taxes</label>
                                   <div class="col-lg-2">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" class="form-control" placeholder="GST in %">                                    
                                    </div>
                                    </div>
                                   <div class="col-lg-2">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" class="form-control" placeholder="VAT in %">                                    
                                    </div>
                                    </div>
                                   <div class="col-lg-2">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" class="form-control" placeholder="ST in %">                                    
                                    </div>
                                    </div>
                                </div>                                
                            </div>
                            <!-- Rate tab end -->
                            <!-- Tab for AP-->
                            <div  class="tab-pane fade" id="apRate">
                                <label class="col-lg-1 control-label">Access Point</label>                                   
                                    <div class="col-lg-2">
                                        <select id="_Accesspoint12" name="_Accesspoint12"  class="form-control" onchange="accesspointChangePrice(this.value)" style="width: 90%">
                                            <option value="-1" selected>Select</option>
                                            <%
                                                Accesspoint[] accesspoints1 = null;
                                                accesspoints1 = new AccessPointManagement().getAllAccessPoint(SessionId, channelId);
                                                if (accesspoints1 != null) {
                                                    for (int i = 0; i < accesspoints1.length; i++) {
                                            %>
                                            <option value="<%=accesspoints1[i].getApId()%>"><%=accesspoints1[i].getName()%></option>
                                            <%}
                                                }%>
                                        </select>
                                    </div>
                                    <div class="col-lg-2" style="margin-right: 1%">
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control" placeholder="Main Credit">                                    
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" class="form-control" placeholder="Free Credit">                                    
                                    </div>
                                    </div>
<!--                                    <div class="col-lg-2">
                                        <a class="btn btn-info btn-sm"  onclick="addAccessPointRate()" id="addUserButton" ><span class="fa fa-plus-circle"></span></a>
                                        <a class="btn btn-danger btn-sm"  onclick="removeddAccessPointRate()" id="addUserButton" ><span class="fa fa-minus-circle"></span></a>
                                    </div>-->
<!--                                    <div class="addAccessPointDetails">
                                    </div>-->
                                    <div id="listOfAPI">                                            
                                    </div><br><br>
                            </div>
                            <!-- Tab for Promo Code -->
                            <div  class="tab-pane fade" id="promoCode">
<!--                                <div class="form-group">                            
                                    <label class="col-lg-3 control-label">Promo Code</label>                                  
                                    <div class=col-sm-3>
                                        <select id="SlabPricing" name="SlabPricing" class="form-control" onchange="">
                                        <option selected value="Enable">Enable</option>
                                        <option value="Disable">Disable</option>
                                        </select>
                                    </div>                                    
                                </div>
                                -->
                                <div class="form-group">                            
                                    <label class="col-lg-3 control-label">Postpaid Slab Pricing</label>                                  
                                    <div class=col-sm-3>
                                        <select id="SlabPricing" name="SlabPricing" class="form-control" onchange="">
                                        <option selected value="Enable">Enable</option>
                                        <option value="Disable">Disable</option>
                                        </select>
                                    </div>                                    
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-3 control-label" style="">Access Point</label>                                   
                                    <div class="col-lg-3">
                                        <select id="_Accesspoint2" name="_Accesspoint2"  class="form-control span2" onchange="acesspointChangeAPI(this.value)" style="width: 100%">
                                            <option value="-1" selected>Select AP</option>
                                            <%
                                                Accesspoint[] accesspoints = null;
                                                accesspoints = new AccessPointManagement().getAllAccessPoint(SessionId, channelId);
                                                if (accesspoints != null) {
                                                    for (int i = 0; i < accesspoints.length; i++) {
                                            %>
                                            <option value="<%=accesspoints[i].getApId()%>"><%=accesspoints[i].getName()%></option>
                                            <%}
                                                }%>
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                        <select id="_APIForSlabPricing2" name="_APIForSlabPricing2"  class="form-control span2" onchange="showSlabPricing(this.value)" style="width: 100%">
                                            <option value="-1" selected>Select API</option>
                                            <%
                                                //Accesspoint[] accesspoints = null;
//                                                accesspoints = new AccessPointManagement().getAllAccessPoint(SessionId, channelId);
//                                                if (accesspoints != null) {
//                                                    for (int i = 0; i < accesspoints.length; i++) {
                                            %>
                                            <option value=""></option>
                                            <%
                                                //}
                                                //}%>
                                        </select>
                                    </div>                                        
                                </div>
                                <div id="_slabPriceWindow">                                            
                                </div>
                                        
                                        
                                        <div id="_API">
                                            
                                        </div>   
                                        
<!--                                    <label class="col-lg-1 control-label">API</label>                                    -->
<!--                                    <div class="col-lg-2">
                                        <select id="_API1" name="_API1"  class="form-control span2" onchange="aaacesspointChange(this.value)" style="width: 90%">
                                            <option value="-1" selected>Select API</option>
                                            <%
//                                                Accesspoint[] accesspoints1 = null;
//                                                accesspoints = new AccessPointManagement().getAllAccessPoint(SessionId, channelId);
//                                                if (accesspoints != null) {
//                                                    for (int i = 0; i < accesspoints.length; i++) {
                                            %>
                                             <option value="Enable">OpenSession</option>
                                             <option value="Disable">AssignToken</option>
                                             <option value="Disable">GetPartnerDetails</option>
                                            <%//}
                                                //}%>
                                        </select>
                                    </div>-->
<!--                                    <div class="col-lg-2">
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control" placeholder="Usage">                                    
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-2">
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control" placeholder="Price">                                    
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <a class="btn btn-info btn-sm"  onclick="aaaddHeader()" id="addUserButton" ><span class="fa fa-plus-circle"></span></a>
                                        <a class="btn btn-danger btn-sm"  onclick="aaaddHeader()" id="addUserButton" ><span class="fa fa-minus-circle"></span></a>
                                    </div>-->
<!--                                        </div>-->
                                </div>
                            
                            
                            <div  class="tab-pane fade" id="alerts">                                
                                <div class="form-group">
                                    <input type="checkbox" class="col-lg-1" id="trialCheck">
                                    <label class="col-lg-2 control-label">Sign Up</label>
                                    <div class="col-lg-6">
                                        <div class=" input-group">
                                            <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                            <textarea class="form-control" id="signUpAlertMesssage" placeholder="Message to be send to Developer when he/she do sign up"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" class="col-lg-1" id="trialCheck">
                                    <label class="col-lg-2 control-label">Trial Package</label>
                                    <div class="col-lg-6">
                                        <div class=" input-group">
                                            <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                            <textarea class="form-control" id="signUpAlertMesssage" placeholder="Message to be send to Developer when trial package is going to expire"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" class="col-lg-1" id="trialCheck">
                                    <label class="col-lg-2 control-label">Package changes</label>
                                    <div class="col-lg-6">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                            <textarea class="form-control" id="signUpAlertMesssage" placeholder="Message to be send to Developer when change his/her package"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" class="col-lg-1" id="trialCheck">
                                    <label class="col-lg-2 control-label">Late Penalties</label>
                                    <div class="col-lg-6">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                            <textarea class="form-control" id="signUpAlertMesssage" placeholder="Message to be send to Developer when late penalties charged"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" class="col-lg-1" id="trialCheck">
                                    <label class="col-lg-2 control-label">Service Termination</label>
                                    <div class="col-lg-6">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                            <textarea class="form-control" id="signUpAlertMesssage" placeholder="Message to be send to Developer when service is terminated"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" class="col-lg-1" id="trialCheck">
                                    <label class="col-lg-2 control-label">Service Reactivation</label>
                                    <div class="col-lg-6">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                            <textarea class="form-control" id="signUpAlertMesssage" placeholder="Message to be send to Developer when he/she reactivate the service"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" class="col-lg-1" id="trialCheck">
                                    <label class="col-lg-2 control-label">Low balance</label>
                                    <div class="col-lg-6">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                            <textarea class="form-control" id="signUpAlertMesssage" placeholder="Message to be send to Developer when Developer have low balance"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="checkbox" class="col-lg-1" id="trialCheck">
                                    <label class="col-lg-2 control-label">Service Suspension</label>
                                    <div class="col-lg-6">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                            <textarea class="form-control" id="signUpAlertMesssage" placeholder="Message to be send to Developer when service of Developer is suspended"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <input type="checkbox" class="" id="trialCheck" style="margin: 2%">
                                    <label class=" control-label" style="margin-right: 1%">PDF Signing</label>                                    
                                    <input type="checkbox" class="" id="trialCheck" style="margin: 2%">
                                    <label class=" control-label">Encrypted PDF</label>
                            </div>
                            
                            <div  class="tab-pane fade" id="feature">                                    
<!--                                <input type="checkbox" class="" id="trialCheck" style="margin-left: 3%">
                                    <label class=" control-label" style="margin-right: 2%">PDF Signing</label>                                    
                                    <input type="checkbox" class="" id="trialCheck">
                                    <label class=" control-label">Encrypted PDF</label><br><br>-->
                                <div class="form-group">                            
                                    <label class="col-lg-3 control-label">Promo Code</label>                                  
                                    <div class=col-sm-2>
                                        <select id="SlabPricing" name="SlabPricing" class="form-control" onchange="">
                                        <option selected value="Enable">Enable</option>
                                        <option value="Disable">Disable</option>
                                        </select>
                                    </div>                                    
                                </div>
                                    
                                <div class="form-group">
                                    <label class=" control-label col-lg-3">Loan with additional Charge</label>
                                    <div class="col-lg-2">
                                    <select id="SlabPricing" name="SlabPricing" class="form-control" onchange="">
                                        <option selected value="Enable">Enable</option>
                                        <option value="Disable">Disable</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control" placeholder="Loan limit">                                    
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="input-group">
                                            <span class="input-group-addon">%</span>
                                            <input type="text" class="form-control" placeholder="Charge in %">                                    
                                        </div>
                                    </div>
                                    </div>
                                    
                                    <div class="form-group">
                                    <label class=" control-label col-lg-3 ">Redeem Credit Point</label>
                                    <div class="col-lg-2">
                                    <select id="SlabPricing" name="SlabPricing" class="form-control" onchange="">
                                        <option selected value="Enable">Enable</option>
                                        <option value="Disable">Disable</option>
                                    </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control" placeholder="Price">                                    
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="input-group">
                                            <span class="input-group-addon">%</span>
                                            <input type="text" class="form-control" placeholder="Point">                                    
                                        </div>
                                    </div>
                                    </div>
<!--                                    <div class="form-group">
                                    <label class=" control-label col-lg-3">For price</label>
                                    <div class="col-lg-2" style="margin-left: ">
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control" placeholder="Price">                                    
                                        </div>
                                    </div>
                                    <label class=" control-label col-lg-1" style="padding-left: 0%">give</label>
                                    <div class="col-lg-2">
                                        <div class="input-group">
                                            <span class="input-group-addon">$</span>
                                            <input type="text" class="form-control" placeholder="Point" >                                   
                                        </div>
                                    </div>
                                    <label class=" control-label col-lg-1">point</label>-->
                                    </div>
                        </div><br><br>
                                <button class="btn btn-success" id="Button" onclick="generateOperatorreports()"><i class="fa fa-shopping-cart" style="font-size:15px;"></i> Save Package</button>
                        </div>
                                           
                           </form> 
                        </div>                                           
                </div>
            </div>                
        </div>
</div>
                <script>
                    ServiceCharge("Enable");
                    ChangePackageCharge("Enable");
                    ReactivationCharge("Enable");
                    CancellationCharge("Enable");
                    LatePenaltyCharge("Enable");
                    //SlabPricing("Enable");
                    LoanFeature("Enable");
                    
                    function accesspointChangeAPIPrice(value){
                        var s = './accessPointWiseAPI.jsp?_apId=' + value;
                        $.ajax({
                            type: 'GET',
                            url: s,
                            success: function(data) {
                                $('#_APIForSlabPricing2').html(data);
                                
                            }
                        });
                    }
                    
                    function showSlabPricing(value){
                        var s ='./showSlabPriceWindow.jsp?_apId=' + value;
//                        if(value != -1){
                        $.ajax({
                            type: 'GET',
                            url: s,
                            success: function(data) {
                                $('#_slabPriceWindow').html(data);
                               // acesspointChangeAPIPrice(value);
                            }
                        });
                      //}
                    }
                    function acesspointChangeAPI(value) {
                        var s = './accessPointChangeAPI.jsp?_apId=' + value;
                        $.ajax({
                            type: 'GET',
                            url: s,
                            success: function(data) {
                                $('#_APIForSlabPricing2').html(data);
                                showSlabPricing(value);
                            }
                        });
                    }             
    //document.getElementById('addPriceForAP').style.display='none';        
//    function accesspointChangePrice(value){
//                        //alert(value+"value");
//                        if (value === '-1') {
//                            document.getElementById('addPriceForAP').style.display='none';
//                        }else{
//                            document.getElementById('addPriceForAP').style.display='block';
//                        }
//                    }

            function accesspointChangePrice(value) {
                        var s = './accessPointChangeBilling.jsp?_apId=' + value;
                        if(value != -2){
                        $.ajax({
                            type: 'GET',
                            url: s,
                            success: function(data) {
                                $('#listOfAPI').html(data);                                
                            }
                        });
                    }else{
                         $('#addPackageTab a[href="#apRate"]').tab('show');
                    }
                }    
                    
                </script>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>
<script src="js/bootbox.min.js" type="text/javascript"></script>  
</body>
</html>                