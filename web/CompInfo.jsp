<%@page import="com.mollatech.serviceguard.nucleus.db.SgPartnerrequest"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerRequestManagement"%>
<%@page import="java.io.ObjectInputStream"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@include file="header.jsp" %>
<script src="./js/partnerRequest.js"></script>
<script src="./js/profile.js"></script>
<%    String _requestId = request.getParameter("_requestId");
    int requestId = Integer.parseInt(_requestId);
    SgPartnerrequest Preq = new PartnerRequestManagement().getPartnerRequestsbyReqId(SessionId, requestId);
%>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Company Information</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47; <i class="fa fa-user"></i><a href="partnerRequest.jsp"> Developer Request</a>&#47; <i class="fa fa-university"></i> Company Information
            </div>
            <div class="panel-body">
                <form class="form-validate form-horizontal" id="register_form" method="POST" action="#">                    
                    <div class="row" style="margin-left: 10px;">
                        <label >
                            <font style=" font-weight: bold; font-size: 20px" class="text-primary">&nbsp;&nbsp;Company Information&nbsp;&nbsp;</font>
                        </label>                                                               
                    </div>                                    
                    <br>                    
                    <div class="row">
                        <font style=" font-weight: bolder;">
                        <label for="curl" class="control-label col-lg-2">Company Name <span class="required">*</span></label>
                        </font>
                        <div class="col-lg-4">
                            <input class="form-control " id="partnerComName" name="partnerComName" placeholder="Company Name" type="text" value="<%= Preq.getComapanyRegName()%>" required/>
                        </div> 
                        <font style=" font-weight: bolder;">
                        <label for="curl" class="control-label col-lg-2">Office Landline No:</label>
                        </font>
                        <div class="col-lg-4">
                            <input class="form-control " id="partnerLandlineNo" name="partnerLandlineNo" placeholder="office landline" type="text" value=" <%=  Preq.getLandline()%> " required/>
                        </div>

                    </div>
                    <br>
                    <div class="row">
                        <font style=" font-weight: bolder;">
                        <label for="cemail" class="control-label col-lg-2">Fax number </label>
                        </font>
                        <div class="col-lg-4">
                            <input class="form-control " id="partnerFax"  name="partnerFax"  type="text"  placeholder="Fax Number" value=" <%= Preq.getFax()%> " required />
                        </div>
                        <font style=" font-weight: bolder;">
                        <label for="curl" class="control-label col-lg-2">Website </label>
                        </font>
                        <div class="col-lg-4">
                            <input class="form-control " id="partnerWebSite" name="partnerWebsite" placeholder="Companty Website" type="url" value=" <%= Preq.getWebsite()%> " required/>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <font style=" font-weight: bolder;">
                        <label for="curl" class="control-label col-lg-2">Domian / IP Address </label>
                        </font>
                        <div class="col-lg-4">
                            <input class="form-control " id="partnerIP" name="partnerIP" placeholder="IP address" type="text" value=" <%= Preq.getIpAddress()%> " required/>
                        </div>
                        <font style=" font-weight: bolder;">
                        <label for="cname" class="control-label col-lg-2">Company Address</label>
                        </font>
                        <div class="col-lg-4">
                            <input class="form-control" id="partnerAddress" name="partnerAddress" minlength="5" placeholder="Companyy Address" type="text" value=" <%= Preq.getAddress()%> " required />
                        </div>
                    </div>
                    <br>   
                    <div class="row">
                        <font style=" font-weight: bolder;">
                        <label for="cname" class="control-label col-lg-2"> Pin Code</label>
                        </font>
                        <div class="col-lg-4">
                            <input class="form-control" id="partnerPincode" name="partnerPincode" minlength="5" type="number" placeholder="Pincode" value=<%= Preq.getPincode()%>  required />
                        </div>
                    </div>                                      
                </form>
            </div>
        </div>
    </div>
</div>

<script src="bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript 
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.min.js"></script>
<script src="js/morris-data.js"></script>-->

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->
        <script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
        <script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>

</body>
</html>
