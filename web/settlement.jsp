<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@include file="header.jsp" %>
<script src="js/resourcePrice.js"></script>
<!--<script src="./js/jquery-1.8.3.min.js"></script>-->
<script src="./js/raphael-min.js"></script>
<script src="./js/morris.js"></script>
<script src="./js/morris.min.js"></script>
<link rel="stylesheet" href="./css/morris.css">
<%
    ResourceDetails[] resourceDetailses = new ResourceManagement().getAllResources();

%>
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header text-uppercase">Settlement Management</h3>
            </div>
            <!-- /.col-lg-10 -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47;  <i class="fa fa-table"></i><b> Resource Sharing</b>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST">
                            <div class="form-group">
                                <label class="control-label col-lg-1">Resource</label>
                                <div class="col-lg-2">

                                    <select class="form-control" id="_revResource" name="_revResource">
                                        <option value="-1">Select</option>
                                        <%                                        if (resourceDetailses != null) {
                                                for (ResourceDetails details : resourceDetailses) {
                                                    if (GlobalStatus.ACTIVE == details.getStatus()) {%>
                                        <option value="<%=details.getResourceId()%>"><%=details.getName()%></option>
                                        <% }
                                                }
                                            }
                                        %>
                                    </select>

                                </div>
                                <label class="control-label col-lg-1">Month</label>
                                <div class="col-lg-2">

                                    <select class="form-control" id="_revMonth" name="_revMonth">
                                        <option value="Jan">Jan</option>
                                        <option value="Feb">Feb</option>
                                        <option value="Mar">Mar</option>
                                        <option value="Apr">Apr</option>
                                        <option value="May">May</option>
                                        <option value="Jun">Jun</option>
                                        <option value="Jul">Jul</option>
                                        <option value="Aug">Aug</option>
                                        <option value="Sep">Sep</option>
                                        <option value="Oct">Oct</option>
                                        <option value="Nov">Nov</option>
                                        <option value="Dec">Dec</option>
                                    </select>

                                </div>
                                <label class="control-label col-lg-1">Year</label>
                                <div class="col-lg-2">

                                    <select class="form-control" id="_revYear" name="_revYear">

                                        <%
                                            SimpleDateFormat format = new SimpleDateFormat("yyyy");
                                            for (int year = (Integer.parseInt(format.format(new Date())) - 10); year <= Integer.parseInt(format.format(new Date())); year++) {%>
                                        <option value="<%=year%>"><%=year%></option>
                                        <% }

                                        %>
                                    </select>
                                    <script>
                                        document.getElementById('_revYear').value = '<%=format.format(new Date())%>';
                                    </script>
                                </div>
                                <a  class="btn btn-success btn-xl" onclick="getRevenue()"><i class="fa fa-plus-circle"></i> Generate</a>
                            </div>
                            <div class="form-group" id="_revData">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="dist/js/sb-admin-2.js"></script>
<%@include file="footer.jsp" %>
