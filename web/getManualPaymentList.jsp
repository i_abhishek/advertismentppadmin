<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SAPBillManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgSapReceiptFile"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement"%>
<%@page import="javax.xml.datatype.XMLGregorianCalendar"%>
<%@page import="javax.xml.datatype.DatatypeFactory"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPaymentdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PaymentManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.MonitorSettingsManagement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="js/ajaxfileupload.js" type="text/javascript"></script>
        <title>Admin Portal</title>
    </head>
    <body>
        <%
            List<SgSubscriptionDetails> bucket = new ArrayList<SgSubscriptionDetails>();

            PackageSubscriptionManagement pac = new PackageSubscriptionManagement();
            PartnerManagement partnerManagement = new PartnerManagement();
            PartnerDetails partnerDetails = null;

            PaymentManagement ppw = new PaymentManagement();

            String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
            String channelId = (String) request.getSession().getAttribute("_channelId");
            String startdate = request.getParameter("_startdate");
            String enddate = request.getParameter("_enddate");
            String partnerId = request.getParameter("_partnername");

            // when search by partner id.
            int partid = 0;
            if (partnerId != null && !partnerId.equalsIgnoreCase("-1")) {
                partid = Integer.parseInt(partnerId);
            }
            if (partnerId != "") {
                partnerDetails = partnerManagement.getPartnerDetails(sessionId, channelId, partid);
                if (partnerDetails != null) {
                    SgSubscriptionDetails[] subscriptionObj = pac.listOfPackageSubscripedbyPartnerId(partnerDetails.getPartnerId());
                    if (subscriptionObj != null) {
                        for (int k = 0; k < subscriptionObj.length; k++) {
                            bucket.add(subscriptionObj[k]);
                        }
                    }
                }
            }
            // when search by date and partnerId
            if ((startdate != "" || enddate != "")&& !partnerId.equals("-1")) {
                bucket = pac.getSubscriptionDetailByDateAndPartnerId(sessionId, channelId, startdate, enddate, partid);
            }
            
            // when search by date
            if ((startdate != "" || enddate != "") && partnerId.equals("-1")) {
                bucket = pac.getSubscriptionDetailByDate(sessionId, channelId, startdate, enddate);
            }

            
        %>
        <div class="container-fluid">           
            <div class="row-fluid">
<!--                <a href="#myModal" role="button" class="btn" data-toggle="modal">Launch demo modal</a>-->
                <div id="licenses_data_table">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Developer Name</th>
                                    <th>Package Name</th>                                    
                                    <th>Invoice No.</th>
                                    <th>Paid Amount</th>                                   
                                    <th>Edit Payment details</th>                                     
                                    <th>Paid On</th>
                                    <th>Subscribe On</th>
                                    <th>Subscription End</th>
                                    <th>Invoice Send On</th>
                                    <th>View Invoice</th>
                                </tr>
                            </thead>
                            <%   
                                
                                DecimalFormat df = new DecimalFormat();
                                df.setMaximumFractionDigits(2);
                                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                                PartnerDetails partnerObject = null;
                                int i = 1;boolean defaultPackageFlag = true;
                                if (bucket != null && !bucket.isEmpty()) {
                                    SgPaymentdetails paymentdetails = null;
                                    for (SgSubscriptionDetails obj : bucket) {
                                        String pName = "NA";
                                        String packageName = "NA";
                                        String paidDate = "NA";
                                        String invoiceSendDate = "NA";
                                        String invoiceNo = "NA";
                                        String subscribeDate = "NA";
                                        String subscriptionEndDate = "NA";
                                        Float paidAmount = 0.0f;
                                        defaultPackageFlag = true;
                                        int subscriptionId = 0;
                                        paymentdetails = ppw.getPaymentDetailsbyPartnerAndSubscriptionID(obj.getBucketId(), obj.getPartnerId());
                                        partnerObject = partnerManagement.getPartnerDetails(sessionId, channelId, obj.getPartnerId());
                                        if (partnerObject != null) {
                                            pName = partnerObject.getPartnerName();
                                        }
                                        packageName         = obj.getBucketName();
                                        subscriptionId      = obj.getBucketId();
                                        subscribeDate       = sdf.format(obj.getCreationDate());
                                        subscriptionEndDate = sdf.format(obj.getExpiryDateNTime());

                                        if (paymentdetails != null) {
                                            if (paymentdetails.getPaidOn() != null) {
                                                paidDate = sdf.format(paymentdetails.getPaidOn());
                                            }
                                            if(paymentdetails.getInvoiceNo() != null){
                                                invoiceNo = paymentdetails.getInvoiceNo();
                                            }
                                            if (paymentdetails.getPaidamount() != null) {
                                                paidAmount = paymentdetails.getPaidamount();
                                            }
                                            if (paymentdetails.getInvoiceSendedOn() != null) {
                                                invoiceSendDate = sdf.format(paymentdetails.getInvoiceSendedOn());
                                            }
                                        }
                                    if(obj.getStatus() ==GlobalStatus.DEFAULT){
                                        defaultPackageFlag = false;
                                    }
                                    if(defaultPackageFlag){
                                     SgSapReceiptFile sapFile = new SAPBillManagement().getSapReceiptByPartnerIdSubscribeId(obj.getPartnerId(),obj.getBucketId());    
                            %>
                            <tr>
                                <td><%=i++%></td>
                                <td><%= pName%></td>
                                <td><%= packageName%></td>
                                <td><%= invoiceNo%></td>
                                <td><%= df.format(paidAmount)%></td>    
                                <% if (paymentdetails != null){%>
                                <td><a href="#" class="btn btn-info btn-xs" onclick="editPaymentModel('<%=paymentdetails.getPaymentId()%>','<%=paymentdetails.getSubscriptionId()%>')"><i class="glyphicon glyphicon-edit"></i> Edit</a></td>                                                                
                                <%}else{%>
                                <td><a href="#" class="btn btn-info btn-xs disabled" ><i class="glyphicon glyphicon-edit"></i> Edit</a></td>                                                                
                                <%}%>
                                <td><%= paidDate%></td>
                                <td><%= subscribeDate%></td>
                                <td><%= subscriptionEndDate%></td>
                                <td><%=invoiceSendDate%></td>
                                <%if(sapFile == null){%>
                                <% if (paymentdetails != null && paymentdetails.getPaidOn() != null) {%>
                                <td>
                                    <font style="font-size: 11px"><a target="_blank" href="getDeveloperInvoice.jsp?_subscriptionId=<%=subscriptionId%>&_partnerid=<%=obj.getPartnerId()%>" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> Online Paid Bill</a> </font>
                                </td>
                                <%} else {%>
                                <td>
                                    <font style="font-size: 11px"><a  target="_blank" href="developerTMInvoice.jsp?_subscriptionId=<%=subscriptionId%>&_partnerid=<%=obj.getPartnerId()%>" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> Unpaid </a> </font>
                                </td>
                                <%}%>
                                <%}else{%>
                                <td>
                                    <font style="font-size: 11px"><a href="#" onclick="getSAPReceipt('<%=sapFile.getSapFileId()%>','<%=pName%>','<%=invoiceNo%>')" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> SAP Paid Bill</a> </font>
                                </td>
                                <%}%>
                            </tr>
                            <%}
                                }
                                if(!defaultPackageFlag && i == 1){%>
                                <tr>
                                    <td>1</td>
                                    <td>No Record Found</td>
                                    <td>No Record Found</td>
                                    <td>No Record Found</td>
                                    <td>No Record Found</td>                                
                                    <td>No Record Found</td>                                
                                    <td>No Record Found</td>
                                    <td>No Record Found</td>
                                    <td>No Record Found</td>
                                    <td>No Record Found</td>
                                    <td>No Record Found</td>
                                </tr>
                                <%} 

                                    }else {%>
                            <tr>
                                <td>1</td>
                                <td>No Record Found</td>
                                <td>No Record Found</td>
                                <td>No Record Found</td>
                                <td>No Record Found</td>                                
                                <td>No Record Found</td>                                
                                <td>No Record Found</td>
                                <td>No Record Found</td>
                                <td>No Record Found</td>
                                <td>No Record Found</td>
                                <td>No Record Found</td>
                            </tr>
                            <%
                                }
                            %>
                        </table>
                    </div>
                </div>
            </div>  
        </div>
        <!-- Modal -->
        <div id="editPayment" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                        <b id="editPaymentModel"></b>
                    </div>          
                    <div class="modal-body">
                        <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>
                        <div class="row-fluid">
                            <form class="form-horizontal" id="editPaymentForm" name="editPaymentForm">
                                <fieldset>
                                    <div class="control-group ">
                                        <label class="control-label col-lg-4">Receipt Number</label>
                                        <div class="col-lg-5">
                                            <input type="text" class="form-control " id="receiptNumber" name="receiptNumber" onkeypress="return isNumericKey(event)">
                                            <input type="hidden" id="paymentId" name="paymentId">
                                            <input type="hidden" id="subscriptionId" name="subscriptionId">  
                                        </div>                               
                                    </div>
                                    <br><br>
                                    <div class="control-group">
                                        <label class="control-label col-lg-4">Payment Date</label>
                                        <div class="col-lg-6">
                                            <div id="datetimepicker3" class="date">
                                                <input id="_paymentDate" name="_paymentDate" class="datepicker" type="text" data-format="dd/MM/yyyy HH:mm:ss" data-bind="value: vm.ActualDoorSizeDepth" style="width: 80%"/>
                                                <span class="add-on" hidden>
                                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar" ></i>
                                                </span>
                                            </div>                                            
                                        </div>
                                    </div>
                                    <br><br>
                                    <div class="control-group">                                        
                                        <label class="control-label col-lg-4">SAP Bill</label>                                        
                                        <div class="col-lg-3">
                                            <input type="file" id="SAPBill" name="SAPBill"/>                                         
                                        </div>                                        
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div id="edit-partner-result"></div>
                        <button class="btn btn-info btn-xs" data-dismiss="modal">Close</button>
                        <button class="btn btn-success btn-xs" onclick="sapBillUpload()" id="addPartnerButtonE">Save</button>
                    </div>
                </div>
            </div>
        </div>                              
    </body>
        <style>
            .datepicker {
                z-index:1051 !important;
            }
        </style>
</html>
