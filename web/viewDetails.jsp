<%@page import="java.util.Enumeration"%>
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="java.util.Properties"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.RequestProductionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgRequestForProduction"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessManagement"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgProductionAccess"%>
<%@page import="java.util.Map"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgBucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@include file="header.jsp" %>
<%
    Map<Integer, String> partnerMap = (Map) session.getAttribute("partnerMap");
    Map<Integer, String> apMap = (Map) session.getAttribute("apMap");
    Map<Integer, String> resMap = (Map) session.getAttribute("resMap");
    Map<String, SgRequestForProduction> productionMap = new HashMap<String, SgRequestForProduction>();
    int partnerId = Integer.parseInt(request.getParameter("pid"));
    int apId = Integer.parseInt(request.getParameter("apId"));
    int resId = Integer.parseInt(request.getParameter("resId"));
    int version = Integer.parseInt(request.getParameter("version"));
    SgRequestForProduction[] productionAccesses = new RequestProductionManagement().getDetails(SessionId, channelId, apId, resId, version, partnerId);
    if (productionAccesses != null) {
        for (int i = 0; i < productionAccesses.length; i++) {
            if(productionAccesses[i].getStatus() == GlobalStatus.PENDING){
                productionMap.put(productionAccesses[i].getApiname(), productionAccesses[i]);
            }
        }
    }
    session.setAttribute("RequestForProductionMap", productionMap);
    
    Properties productionRejectionProperties = null;
    productionRejectionProperties = LoadSettings.g_productionAccessRejectionSettings;

    String options = "<option value='Select' selected>Select Reason for Rejection </option>\n";

    if (productionRejectionProperties != null) {
        Enumeration enuKeys = productionRejectionProperties.keys();
        while (enuKeys.hasMoreElements()) {
            String key = (String) enuKeys.nextElement();
            String value = productionRejectionProperties.getProperty(key);
            options += "<option value='" + value + "'>" + value + "</option>\n";
        }
    }
%>
<div id="page-wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header text-uppercase">Access Management</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-home"></i><a href="home.jsp"> Home</a> &#47;
                    <i class="fa fa-shopping-cart"></i><a href="productionAccManager.jsp"> ACCESS DETAILS</a> &#47;
                    <i class="fa fa-edit"></i> Access Management
                </div>
                <form name="sampleInOut" id="sampleInOut">
                    <div class="panel-body">
                        <h3><%= partnerMap.get(partnerId)%> </h3>
                        <hr>
                        <div class="form-group">
                            <div class="col-lg-3">
                                <select id="_Accesspoint2" name="_Accesspoint2"  class="form-control span2" style="width: 100%">
                                    <option value="-1" selected>Select AccessPoint</option>                                                                                  
                                    <option value="<%=apId%>"><%=apMap.get(apId)%></option>
                                </select>
                            </div>
                            <div class="col-lg-3">
                                <select id="_ResourceForSlabPricing2" name="_ResourceForSlabPricing2"  class="form-control span2" style="width: 100%">
                                    <option value="-1" selected>Select Resource</option>  
                                    <option value="<%=resId%>"><%=resMap.get(resId)%></option>
                                </select>
                            </div>
                            <div class="col-lg-3">
                                <select id="_VersionForSlabPricing2" name="_VersionForSlabPricing2"  class="form-control span2" style="width: 100%">
                                    <option value="-1" selected>Select Version</option> 
                                    <option value="<%=version%>"><%=version%></option>
                                </select>
                            </div>
                            <div class="col-lg-3">
                                <select id="_APIForSlabPricing2" name="_APIForSlabPricing2"  class="form-control span2" style="width: 100%" onchange="loadData()">
                                    <option value="-1" selected>Select API</option> 
                                    <%for (Object obj : productionMap.keySet()) {%>
                                    <option value="<%=obj%>" ><%=obj%></option> 
                                    <% }%>
                                </select>
                            </div>                                        
                        </div>
                        <br><br>   
                        <div id="apiRequestResponse">                                           
                        </div>                                       
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="rejectionProductionModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close btn-xs" type="button">X</button>
                    <h4 class="modal-title">Reject production request</h4>
                </div>
                <div class="modal-body">
                    <form class="form-inline" method="post">
                        <fieldset>
                            <div class="control-group">
                                <label class="control-label col-lg-2"  for="partnername">Reason</label>
                                <div class="controls col-lg-10">
                                    <select id="reason" name="reason" class="form-control" >
                                        <%=options%>
                                    </select>
                                </div>
                            </div>
                            <br><br><br>        
                            <div class="control-group">
                                <div class="col-lg-12">
                                    <a class="btn btn-danger btn-xs" data-dismiss="modal" style="margin-left: 18%"> Close</a>
                                    <a class="btn btn-success btn-xs" onclick="rejectRequest('<%=GlobalStatus.REJECTED%>')"> Reject</a>
                                </div>
                            </div>
                        </fieldset>
                    </form>                
                </div>
            </div>            
        </div>
    </div>                                


<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>
<script src="js/bootbox.min.js" type="text/javascript"></script> 
<script src="js/bootbox.min(1).js" type="text/javascript"></script>
<script>
                                    function loadData() {
                                        var apId = document.getElementById("_Accesspoint2").value;
                                        if (apId === '-1') {
                                            Alert4Users("<span><font color=red>Please Select Access Point Name.</font></span>");
                                            document.getElementById("apiRequestResponse").innerHTML = "";
                                            return;
                                        }
                                        var resId = document.getElementById("_ResourceForSlabPricing2").value;
                                        if (resId === '-1') {
                                            Alert4Users("<span><font color=red>Please Select Resource Name.</font></span>");
                                            document.getElementById("apiRequestResponse").innerHTML = "";
                                            return;
                                        }
                                        var version = document.getElementById("_VersionForSlabPricing2").value;
                                        if (version === '-1') {
                                            Alert4Users("<span><font color=red>Please Select Version.</font></span>");
                                            document.getElementById("apiRequestResponse").innerHTML = "";
                                            return;
                                        }
                                        var apI = document.getElementById("_APIForSlabPricing2").value;
                                        if (apI === '-1') {
                                            Alert4Users("<span><font color=red>Please Select API Name</font></span>");
                                            document.getElementById("apiRequestResponse").innerHTML = "";
                                            return;
                                        }
                                        var s = './getInOut.jsp';

                                        $.ajax({
                                            type: 'POST',
                                            url: s,
                                            datatype: 'json',
                                            data: $("#sampleInOut").serialize(),
                                            success: function (data) {
                                                document.getElementById("apiRequestResponse").innerHTML = data;
                                            }
                                        });
                                    }
                                    function bootboxmodelForProduction(content) {
                                        var popup = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
                                                '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' +
                                                '<button aria-hidden="true" data-dismiss="modal" class="close" type="button btn-xs">X</button><font style="color: blue">' +
                                                content +
                                                '</font><a class="btn btn-info btn-xs" data-dismiss="modal" class="close" style="align:right"> OK </a></div></div></div></div>');
                                        popup.modal();
                                    }
                                    function Alert4Users(msg) {
                                        bootboxmodelForProduction("<h4>" + msg + "</h4>");
                                    }
                                    function requestStatus(status) {
                                        if (status === '<%=GlobalStatus.REJECTED%>') {
                                            bootbox.confirm({
                                                message: "<h4><span><font color=red>Are You Sure?</font></span></h4>",
                                                buttons: {
                                                    confirm: {
                                                        label: 'Yes',
                                                        className: 'btn-success btn-xs'
                                                    },
                                                    cancel: {
                                                        label: 'No',
                                                        className: 'btn-danger btn-xs'
                                                    }
                                                },
                                                callback: function (result) {
                                                    if (result === true) {
                                                        changeStatus(status);
                                                    }
                                                }});
                                        } else {
                                            changeStatus(status);
                                        }
                                    }
                                    
                                    function rejectRequest(status){
                                        var rejectionReason = document.getElementById("reason").value;
                                        
                                        if (rejectionReason === "Select") {
                                            Alert4Users("<span><font color=red>" + "Please select proper reason" + "</font></span>");
                                            return;
                                        }
                                        bootbox.confirm({
                                                message: "<h4><span><font color=red>Are You Sure?</font></span></h4>",
                                                buttons: {
                                                    confirm: {
                                                        label: 'Yes',
                                                        className: 'btn-success btn-xs'
                                                    },
                                                    cancel: {
                                                        label: 'No',
                                                        className: 'btn-danger btn-xs'
                                                    }
                                                },
                                                callback: function (result) {
                                                    if (result === true) {
                                                        changeStatus(status);
                                                    }
                                                }});                                        
                                        }
                                    
                                    function changeStatus(status) {
                                        var rejectionReason = document.getElementById("reason").value;
                                        var s = './ChangeStatusForAPI?status=' + status +'&rejectReason='+rejectionReason;
                                        $.ajax({
                                            type: 'POST',
                                            url: s,
                                            datatype: 'json',
                                            data: $("#sampleInOut").serialize(),
                                            success: function (data) {
                                                if (data.result === 'error') {
                                                    Alert4Users("<span><font color=red>" + data.message + "</font></span>");
                                                } else {
                                                    Alert4Users("<span><font color=blue>" + data.message + "</font></span>");
                                                    setTimeout(function () {
                                                        location.reload();
                                                    }, 3000);
                                                }
                                            }
                                        });
                                    }
                                    
                                    function rejectProductionAccessRequest() {                                        
                                        $('#rejectionProductionModal').modal('show');
                                    }

</script>  
</body>
</html>    
<%@include file="footer.jsp" %>