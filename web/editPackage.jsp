<%@page import="java.math.BigDecimal"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Warfiles"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgBucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@include file="header.jsp" %>
<script src="js/designPackage.js" type="text/javascript"></script>
<script src="js/packageOperation.js" type="text/javascript"></script>
<script src="js/editPackage.js" type="text/javascript"></script>
<link href="./select2/select2.css" rel="stylesheet"/> 
<%
    String packageId = request.getParameter("_edit");
    int pId = 0;
    if (packageId != null) {
        pId = Integer.parseInt(packageId);
    }
    SgBucketdetails packageObj = new PackageManagement().getPackageDetails(SessionId, channelId, pId);
    String partnerIds = packageObj.getPartnerVisibility();
    boolean flag = false;
    String options = "<option value=all selected>NA</option>\n";
    if (partnerIds != null && !partnerIds.isEmpty() && partnerIds.contains("all,")) {
        options = "<option value=all selected>All</option>\n";
    } else if (partnerIds != null && !partnerIds.isEmpty()) {
        String[] parArray = partnerIds.split(",");
        options = "";
        for (int i = 0; i < parArray.length; i++) {
            int parId = Integer.parseInt(parArray[i]);
            String emailId = "";
            PartnerDetails parObj = new PartnerManagement().getPartnerDetails(parId);
            if (parObj != null) {
                emailId = parObj.getPartnerEmailid();
            }
            options += "<option selected value='" + parId + "'>" + emailId + "</option>\n";
        }
        flag = true;
    }
    PartnerDetails[] partnerObj = new PartnerManagement().getAllPartnerDetails(SessionId, channelId);
    if (partnerObj != null) {
        if (flag) {
            options += "<option value=all>ALL</option>\n";
        }
        for (int i = 0; i < partnerObj.length; i++) {
            if (partnerIds != null && partnerIds.contains(String.valueOf(partnerObj[i].getPartnerId()))) {
                continue;
            }
            if (partnerObj[i].getStatus() == 1) {
                options += "<option value='" + partnerObj[i].getPartnerId() + "'>" + partnerObj[i].getPartnerEmailid() + "</option>\n";
            }
        }
    }
    String promocodeEnable = LoadSettings.g_sSettings.getProperty("functionalityOf.feature.promocode.booleanValue");
    String loanEnable = LoadSettings.g_sSettings.getProperty("functionalityOf.feature.loan.booleanValue");
    String vatTax = LoadSettings.g_sSettings.getProperty("functionalityOf.feature.vatTax.booleanValue");
    String serviceTax = LoadSettings.g_sSettings.getProperty("functionalityOf.feature.serviceTax.booleanValue");
    boolean featureFlag = false;
    if (promocodeEnable.equalsIgnoreCase("true") || loanEnable.equalsIgnoreCase("true")) {
        featureFlag = true;
    }
    String minimumValue = String.valueOf(packageObj.getMinimumBalance());
    Double dMinimumVal = Double.parseDouble(minimumValue);
%>
<div id="page-wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Edit " <%=packageObj.getBucketName()%> " package details</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-home"></i><a href="home.jsp"> Home</a> &#47;
                    <i class="fa fa-shopping-cart"></i><a href="package.jsp"> Package Details</a> &#47;
                    <i class="fa fa-edit"></i> Package Details
                </div>
                <div class="panel-body">
                    <h4>Package details</h4>
                    <hr>                   
                    <!--                    <form class="form-horizontal" id="edit_package_form1" name="edit_package_form1" role="form">-->
                    <div class="col-lg-3">
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fa fa-shopping-cart"></i></span>
                                <%if (packageObj != null) {%>
                            <input type="text" class="form-control" placeholder="Package name" value="<%=packageObj.getBucketName()%>" disabled>
                            <%} else {%>
                            <input type="text" class="form-control" placeholder="Package name" id="_packageName" name="_packageName">
                            <%}%>
                        </div>
                    </div>                                
                    <div class="col-lg-3">
                        <div class="form-group input-group">
                            <span class="input-group-addon">$</span>
                            <%if (packageObj != null) {%>
                            <input type="text" class="form-control" placeholder="Package price" value="<%=packageObj.getPlanAmount()%>" disabled>
                            <%} else {%>
                            <input type="text" class="form-control" placeholder="Price">                                    
                            <%}%>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>                                
                                <%if (packageObj != null) {%>
                            <select class="form-control" disabled>
                                <option value="<%=packageObj.getBucketDuration()%>"><%=packageObj.getBucketDuration()%></option>
                            </select>
                            <%} else {%>
                            <select class="form-control">
                                <option value="">Select Duration</option>
                                <option >Daily</option>
                                <option>Weekly</option>
                                <option>Monthly</option>
                                <option>Quarterly</option>
                                <option>Half yearly</option>
                                <option>Yearly</option>
                            </select>                                     
                            <%}%>                                                                  
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>                                
                                <%if (packageObj != null) {%>
                            <select class="form-control" disabled>
                                <option value="<%=packageObj.getPaymentMode()%>"><%=packageObj.getPaymentMode()%></option>
                            </select>
                            <%} else {%>
                            <select class="form-control">
                                <option>Select Payment type</option>
                                <option>Prepaid</option>
                                <option>PostPaid</option>
                            </select>
                            <%}%>                                 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3"><span style="padding-left:75px"> Developer Visibility</span></label>
                        <div class="left"></div>
                        <div class="col-lg-8">
                            <select id="visibleTo" name="visibleTo" multiple="multiple" style="width: 114%">
                                <%=options%> 
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <ul class="nav nav-pills" id="addPackageTab">
                        <li class="active" id="rateTab"><a href="#rate" data-toggle="tab">Rate</a></li>                    
                        <li id="accessPointTab"><a href="#apRate" data-toggle="tab">Access Point Rate</a></li>
                        <li id="flatRateTab"><a href="#flatRate" data-toggle="tab">Flat Rate</a></li>
                        
                        <li id="apiThrottlingTab"><a href="#apiThrottling" data-toggle="tab">API Throttling</a></li>
                        <li id="slabPricingTab"><a href="#promoCode" data-toggle="tab">Slab Pricing</a></li>
                        <li id="tieringPricingTab"><a href="#tieringPrice" data-toggle="tab">Tier Pricing</a></li>
                        <li id="securityTab"><a href="#alerts" data-toggle="tab">Security & Alerts</a></li>
                            <%if (featureFlag) {%>
                        <li id="featureTab"><a href="#feature" data-toggle="tab">Feature</a></li>
                            <%}%>
                    </ul>
                </div>
                <br><br><br>
                <div class="tab-content">
                    <!-- Rate Tab -->
                    <div class="tab-pane fade in active" id="rate">
                        <form class="form-horizontal" id="edit_package_form" name="edit_package_form" role="form">
                            <input type="hidden" id="firstTab" name="firstTab" value="<%=packageObj.getTabShowFlag()%>">
                            <%if (!packageObj.getPaymentMode().equalsIgnoreCase("postpaid")) {%>
                            <div class="form-group">                                    
                                <label class="col-lg-3 control-label" >Minimum balance</label>
                                <div class="col-lg-2">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" class="form-control" id="minimumBalance" name="minimumBalance" placeholder="Price" value="<%=BigDecimal.valueOf(dMinimumVal)%>" onkeypress="return isNumberKey(event)">                                    
                                    </div>
                                </div>
                            </div>
                            <%}%>        
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" >Main Credits</label>
                                <div class="col-lg-2">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" class="form-control" id="mainCredits" name="mainCredits" placeholder="Price" onkeypress="return isNumberKey(event)" value="<%=new BigDecimal(packageObj.getMainCredits())%>">                                    
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label" >Free Credits</label>
                                <div class="col-lg-2">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" class="form-control" id="freeCredits" name="freeCredits" placeholder="Price" onkeypress="return isNumberKey(event)" value="<%=new BigDecimal(packageObj.getFreeCredits())%>">                                    
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">                                    
                                <label class="col-lg-3 control-label">Free Period</label>
                                <div class="col-lg-2">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <select class="form-control" id="freeTrail" name="freeTrail">
                                            <% if (packageObj.getDaysForFreeTrial() != null) {
                                                    if (packageObj.getDaysForFreeTrial().equals("7")) {%>
                                            <option selected value="7">1 Week</option>
                                            <option value="14">2 Week</option>
                                            <option value="1">1 Month</option>
                                            <%} else if (packageObj.getDaysForFreeTrial().equals("14")) {%>
                                            <option selected value="14">2 Week</option>
                                            <option value="7">1 Week</option>
                                            <option value="1">1 Month</option>
                                            <%} else if (packageObj.getDaysForFreeTrial().equals("1")) {%>
                                            <option selected value="14">2 Week</option>
                                            <option value="7">1 Week</option>
                                            <option value="1">1 Month</option>
                                            <%} else {%>
                                            <option selected value="0">Days</option>                                                
                                            <option value="7">1 Week</option>
                                            <option value="14">2 Week</option>
                                            <option value="1">1 Month</option>
                                            <%}
                                            } else {%>
                                            <option selected value="0">Days</option>                                                
                                            <option value="7">1 Week</option>
                                            <option value="14">2 Week</option>
                                            <option value="1">1 Month</option>
                                            <%}%>
                                        </select>                                                    
                                    </div>
                                </div>                                    
                            </div>
                            <div class="form-group">                            
                                <label class="col-lg-3 control-label">1st Setup Service Charge</label>
                                <div id="_ServiceCharge">
                                </div>
                                <script>
                                    <%if (Integer.parseInt(packageObj.getServiceCharge()) > 0) {%>
                                    ServiceCharge("Enable");
                                    document.getElementById("serviceChargeRate").value = '<%=packageObj.getServiceCharge()%>';
                                    <%} else {%>
                                    ServiceCharge("Disable");
                                    <%}%>
                                </script>
                            </div>
                            <div class="form-group">                            
                                <label class="col-lg-3 control-label">Change Package Charge</label>
                                <div id="_ChangePackageCharge">
                                </div>
                                <script>
                                    <%if (packageObj.getChangePackageRate() > 0) {%>
                                    ChangePackageCharge("Enable");
                                    document.getElementById("ChangePackageChargeRate").value = '<%=packageObj.getChangePackageRate()%>';
                                    <%} else {%>
                                    ChangePackageCharge("Disable");
                                    <%}%>
                                </script>
                            </div>
                            <div class="form-group">                            
                                <label class="col-lg-3 control-label">Reactivation Charge</label>
                                <div id="_ReactivationCharge">
                                </div>
                                <script>
                                    <%if (packageObj.getReActivationCharge() > 0) {%>
                                    ReactivationCharge("Enable");
                                    document.getElementById("ReactivationChargeRate").value = '<%=packageObj.getReActivationCharge()%>';
                                    <%} else {%>
                                    ReactivationCharge("Disable");
                                    <%}%>
                                </script>
                            </div>
                            <div class="form-group">                            
                                <label class="col-lg-3 control-label">Cancellation Charge</label>
                                <div id="_CancellationCharge">
                                </div>
                                <script>
                                    <%if (packageObj.getCancellationRate() > 0) {%>
                                    CancellationCharge("Enable");
                                    document.getElementById("CancellationChargeRate").value = '<%=packageObj.getCancellationRate()%>';
                                    <%} else {%>
                                    CancellationCharge("Disable");
                                    <%}%>
                                </script>
                            </div>

                            <div class="form-group">                            
                                <label class="col-lg-3 control-label">Late Penalty Charge</label>
                                <div id="_LatePenaltyCharge">
                                </div>
                                <%
                                    String startD1 = "0";
                                    String endD1 = "0";
                                    String penalities1 = "0";
                                    String startD2 = "0";
                                    String endD2 = "0";
                                    String penalities2 = "0";
                                    if (packageObj.getLatePenaltyRate() != null && !packageObj.getLatePenaltyRate().equals("")) {
                                        String latePenalites = packageObj.getLatePenaltyRate();
                                        //String stRate  = "0";
                                        if (latePenalites != null) {
                                            JSONObject latePenalitesJson = new JSONObject(latePenalites);
                                            startD1 = latePenalitesJson.getString("latePenaltyChargeStartDay1");
                                            endD1 = latePenalitesJson.getString("latePenaltyChargeEndDay1");
                                            penalities1 = latePenalitesJson.getString("latePenaltyChargeRate1");
                                            startD2 = latePenalitesJson.getString("latePenaltyChargeStartDay2");
                                            endD2 = latePenalitesJson.getString("latePenaltyChargeEndDay2");
                                            penalities2 = latePenalitesJson.getString("latePenaltyChargeRate2");
                                        }

                                %>                                    
                                <%}%>
                                <script>
                                    <%if (Float.parseFloat(startD1) > 0 && Float.parseFloat(endD1) > 0 && Float.parseFloat(penalities1) > 0
                                                && Float.parseFloat(penalities2) > 0 && Float.parseFloat(endD2) > 0 && Float.parseFloat(startD2) > 0) {%>
                                    LatePenaltyCharge("Enable");
                                    document.getElementById("LatePenaltyChargeSDay1").value = '<%=startD1%>';
                                    document.getElementById("LatePenaltyChargeEDay1").value = '<%=endD1%>';
                                    document.getElementById("LatePenaltyChargeRate1").value = '<%=penalities1%>';
                                    document.getElementById("LatePenaltyChargeSDay2").value = '<%=startD2%>';
                                    document.getElementById("LatePenaltyChargeEDay2").value = '<%=endD2%>';
                                    document.getElementById("LatePenaltyChargeRate2").value = '<%=penalities2%>';
                                    <%} else {%>
                                    LatePenaltyCharge("Disable");
                                    <%}%>
                                </script>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Taxes</label>
                                <% String taxRate = packageObj.getTax();
                                    String gstRate = "0";
                                    String vatRate = "0";
                                    String stRate = "0";
                                    if (taxRate != null) {
                                        JSONObject taxJson = new JSONObject(taxRate);
                                        gstRate = taxJson.getString("gstTax");
                                        vatRate = taxJson.getString("vatTax");
                                        stRate = taxJson.getString("stTax");
                                    }
                                %>
                                <div class="col-lg-2">                                       
                                    <div class="input-group">
                                        <span class="input-group-addon">GST</span>
                                        <input type="text" class="form-control" id="gstRate" name="gstRate" onkeypress="return isNumberKey(event)" value="<%=gstRate%>" placeholder="GST in %">                                    
                                    </div>
                                </div>
                                <%if (vatTax != null && vatTax.equalsIgnoreCase("true")) {%>
                                <div class="col-lg-2">
                                    <div class="input-group">
                                        <span class="input-group-addon">VAT</span>
                                        <input type="text" class="form-control" id="vatRate" name="vatRate" onkeypress="return isNumberKey(event)" value="<%=vatRate%>" placeholder="VAT in %">                                    
                                    </div>
                                </div>
                                <%}
                                    if (serviceTax != null && serviceTax.equalsIgnoreCase("true")) {%>    
                                <div class="col-lg-2">
                                    <div class="input-group">
                                        <span class="input-group-addon">ST</span>
                                        <input type="text" class="form-control" id="stRate" name="stRate" onkeypress="return isNumberKey(event)" value="<%=stRate%>" placeholder="ST in %">                                    
                                    </div>
                                </div>
                                <%}%>    
                            </div>
                            <a  class="btn btn-success btn-xs" style="margin-left: 26%" onclick="testEditPackage('<%=packageId%>')"><i class="fa fa-edit"></i> Save & Next</a>
                        </form>
                    </div>
                    <!-- Rate tab end -->

                    <!-- Tab for AP-->
                    <div  class="tab-pane fade" id="apRate">                                
                        <form class="form-horizontal" id="edit_ap_form" name="edit_ap_form" role="form">
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Access Point Rate</label>                                   
                                <div class="col-lg-3">
                                    <select id="_Accesspoint12" name="_Accesspoint12"  class="form-control" onchange="acesspointTabResource(this.value, '<%=packageId%>')" >
                                        <option value="-1" selected>Select Access point</option>
                                        <%
                                            Accesspoint[] accesspoints1 = null;
                                            Warfiles warfiles = null;
                                            accesspoints1 = new AccessPointManagement().getAllAccessPoint(SessionId, channelId);
                                            if (accesspoints1 != null) {
                                                for (int i = 0; i < accesspoints1.length; i++) {
                                                    if (accesspoints1[i].getStatus() != GlobalStatus.DELETED) {
                                                        Accesspoint apdetails = accesspoints1[i];
                                                        TransformDetails transformDetails = new TransformManagement().getTransformDetails(SessionId, channelId, apdetails.getApId(), Integer.parseInt(apdetails.getResources().split(",")[0]));
                                                        if (transformDetails != null) {
                                                            warfiles = new WarFileManagement().getWarFile(SessionId, channelId, apdetails.getApId());
                                                            if (warfiles != null) {%>
                                        <option value="<%=apdetails.getName()%>"><%=apdetails.getName()%></option>
                                        <%}
                                                        }
                                                    }
                                                }
                                            }
                                        %>
                                    </select>
                                </div>

                                <div class="col-lg-3">                                    
                                    <select id="_ResourceForAPPricing2" name="_ResourceForAPPricing2"  class="form-control span2" onchange="showSlabVersionAP(this.value, '<%=packageId%>')" style="width: 100%">
                                        <option value="-1" selected>Select Resource</option>                                                                                  
                                    </select>                                    
                                </div>
                                <div class="col-lg-3">
                                    <select id="_VersionForAPricing2" name="_VersionForAPricing2"  class="form-control span2" onchange="showAPAPIAP(this.value, '<%=packageId%>')" style="width: 100%">
                                        <option value="-1" selected>Select Version</option>                                                                                  
                                    </select>
                                </div>    
                            </div>
                            <div id="listOfAPI">                                            
                            </div>
                            <a  class="btn btn-success btn-xs"  onclick="editAPPrice('<%=packageId%>')" style="margin-left: 43%"><i class="fa fa-edit"></i> Save & Next</a>
                        </form>
                    </div>
                    <!-- Flat price -->

                    <div  class="tab-pane fade" id="flatRate">                                
                        <form class="form-horizontal" id="edit_flatPrice_form" name="edit_flatPrice_form" role="form">
                            <div class="form-group">                            
                                <label class="col-lg-2 control-label">Flat price setting</label>                                  
                                <div class=col-sm-2>                                    
                                    <%if (packageObj.getFlatPrice() != null) {%>                                
                                    <select id="flatPriceSetting" name="flatPriceSetting" class="form-control" onchange="flatRateDiv(this.value)">                                    
                                        <option  value="Disable" >Disable</option>
                                        <option selected value="Enable" >Enable</option>
                                    </select>                                        
                                    <%} else {%>
                                    <select id="flatPriceSetting" name="flatPriceSetting" class="form-control" onchange="flatRateDiv(this.value)"> 
                                        <option selected value="Disable" >Disable</option>
                                        <option value="Enable" >Enable</option>
                                    </select>     
                                    <%}%>                                
                                </div>                                    
                            </div>
                            <input type="hidden" id="packageIDFlat" name="packageIDFlat" value="<%=packageId%>">                            
                            <div class="form-group" id="flatPriceDiv" style="display: none">
                                <%if (packageObj.getFlatPrice() != null) {%>
                                <script>
                                    flatRateDiv('Enable');
                                </script>
                                <%}%>
                                <label class="col-lg-2 control-label">Flat price</label>                                   
                                <div class="col-lg-2">
                                    <select id="_accesspointFlatPrice" name="_accesspointFlatPrice"  class="form-control" onchange="getResourceForFlatPrice(this.value)" >
                                        <option value="-1" selected>Select Access point</option>
                                        <%
                                            if (accesspoints1 != null) {
                                                for (int i = 0; i < accesspoints1.length; i++) {
                                                    if (accesspoints1[i].getStatus() != GlobalStatus.DELETED) {
                                                        Accesspoint apdetails = accesspoints1[i];
                                                        TransformDetails transformDetails = new TransformManagement().getTransformDetails(SessionId, channelId, apdetails.getApId(), Integer.parseInt(apdetails.getResources().split(",")[0]));
                                                        if (transformDetails != null) {
                                                            warfiles = new WarFileManagement().getWarFile(SessionId, channelId, apdetails.getApId());
                                                            if (warfiles != null) {%>
                                        <option value="<%=apdetails.getName()%>"><%=apdetails.getName()%></option>
                                        <%}
                                                        }
                                                    }
                                                }
                                            }
                                        %>
                                    </select>
                                </div>

                                <div class="col-lg-2">                                    
                                    <select id="_resourceForFlatPrice" name="_resourceForFlatPrice"  class="form-control span2" onchange="getVersionForFlatPrice(this.value)" style="width: 100%">
                                        <option value="-1" selected>Select Resource</option>                                                                                  
                                    </select>                                    
                                </div>
                                <div class="col-lg-2">
                                    <select id="_versionForFlatPrice" name="_versionForFlatPrice"  class="form-control span2" onchange="getAPIForFlatPrice(this.value)" style="width: 100%">
                                        <option value="-1" selected>Select Version</option>                                                                                  
                                    </select>
                                </div> 
                                <div id="_flatPriceDetails">                                            
                                </div>    
                            </div>

                            <a  class="btn btn-success btn-xs"  onclick="editFlatPrice('<%=packageId%>')" style="margin-left: 35%"><i class="fa fa-edit"></i> Save & Next</a>                        
                        </form>
                    </div>
                    <!-- End of Flat price Tab  -->   

                    <!-- Tab for Renewal price Tab  -->   

                    

                    <!-- End of Renewal price Tab  -->  

                    <!-- Tab for API Throttling -->
                    <div  class="tab-pane fade" id="apiThrottling">                                
                        <form class="form-horizontal" id="edit_apiThrottling_form" name="edit_apiThrottling_form" role="form">
                            <div class="form-group">                            
                                <label class="col-lg-2 control-label">Throttling setting</label>                                  
                                <div class=col-sm-2>                                    
                                    <%if (packageObj.getApiThrottling() != null) {%>                                
                                    <select id="throttlingSetting" name="throttlingSetting" class="form-control" onchange="throttlingDiv(this.value)">                                    
                                        <option  value="Disable" >Disable</option>
                                        <option selected value="Enable" >Enable</option>
                                    </select>                                        
                                    <%} else {%>
                                    <select id="throttlingSetting" name="throttlingSetting" class="form-control" onchange="throttlingDiv(this.value)"> 
                                        <option selected value="Disable" >Disable</option>
                                        <option value="Enable" >Enable</option>
                                    </select>     
                                    <%}%>                                
                                </div>                                    
                            </div>
                            <input type="hidden" id="packageIDThro" name="packageIDThro" value="<%=packageId%>">

                            <div class="form-group" id="throttlingDiv" style="display: none">
                                <%if (packageObj.getApiThrottling() != null) {%>
                                <script>
                                    throttlingDiv('Enable');
                                </script>
                                <%}%>
                                <label class="col-lg-2 control-label">API Throttling</label>                                   
                                <div class="col-lg-3">
                                    <select id="_accesspointAPIThrottling" name="_accesspointAPIThrottling"  class="form-control" onchange="getResourceForAPIThrottling(this.value)" >
                                        <option value="-1" selected>Select Access point</option>
                                        <%
                                            if (accesspoints1 != null) {
                                                for (int i = 0; i < accesspoints1.length; i++) {
                                                    if (accesspoints1[i].getStatus() != GlobalStatus.DELETED) {
                                                        Accesspoint apdetails = accesspoints1[i];
                                                        TransformDetails transformDetails = new TransformManagement().getTransformDetails(SessionId, channelId, apdetails.getApId(), Integer.parseInt(apdetails.getResources().split(",")[0]));
                                                        if (transformDetails != null) {
                                                            warfiles = new WarFileManagement().getWarFile(SessionId, channelId, apdetails.getApId());
                                                            if (warfiles != null) {%>
                                        <option value="<%=apdetails.getName()%>"><%=apdetails.getName()%></option>
                                        <%}
                                                        }
                                                    }
                                                }
                                            }
                                        %>
                                    </select>
                                </div>

                                <div class="col-lg-3">                                    
                                    <select id="_resourceForApiThrottling" name="_resourceForApiThrottling"  class="form-control span2" onchange="getAPIThrottlingVersion(this.value)" style="width: 100%">
                                        <option value="-1" selected>Select Resource</option>                                                                                  
                                    </select>                                    
                                </div>
                                <div class="col-lg-3">
                                    <select id="_versionForAPIThrottling" name="_versionForAPIThrottling"  class="form-control span2" onchange="getAPIForAPIThrottling(this.value)" style="width: 100%">
                                        <option value="-1" selected>Select Version</option>                                                                                  
                                    </select>
                                </div>    
                            </div>
                            <div id="_APIForThrottling">                                            
                            </div>                            
                            <a  class="btn btn-success btn-xs"  onclick="editAPIThrottling('<%=packageId%>')" style="margin-left: 43%"><i class="fa fa-edit"></i> Save & Next</a>                        
                        </form>
                    </div>
                    <!-- Tab for Promo Code -->
                    <div  class="tab-pane fade" id="promoCode">
                        <form class="form-horizontal" id="edit_sp_form" name="edit_sp_form" role="form">
                            <input type="hidden" id="packageID" name="packageID" value="<%=packageId%>">
                            <div class="form-group">                            
                                <label class="col-lg-2 control-label">Slab Pricing</label>                                  
                                <div class=col-sm-2>                                    
                                    <select id="SlabPricing" name="SlabPricing" class="form-control"  <%if (!packageObj.getPaymentMode().equalsIgnoreCase("postpaid")) {%>disabled<%}%> onchange="slabPriceDiv(this.value)">
                                        <option value="Enable" >Enable</option>
                                        <option selected value="Disable" >Disable</option>
                                    </select>
                                </div>                                    
                            </div>
                            <div id="apSlabrate" style="display: none">
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" style="">Access Point</label>                                  
                                    <div class="col-lg-2">
                                        <select id="_Accesspoint2" name="_Accesspoint2"  class="form-control span2" onchange="acesspointChangeResource()" style="width: 100%">
                                            <option value="-1">Select AP</option>                                           

                                            <%
                                                Accesspoint[] accesspoints2 = null;
                                                warfiles = null;
                                                accesspoints2 = new AccessPointManagement().getAllAccessPoint(SessionId, channelId);
                                                if (accesspoints2 != null) {
                                                    for (int i = 0; i < accesspoints1.length; i++) {
                                                        if (accesspoints1[i].getStatus() != GlobalStatus.DELETED) {
                                                            Accesspoint apdetails = accesspoints1[i];
                                                            TransformDetails transformDetails = new TransformManagement().getTransformDetails(SessionId, channelId, apdetails.getApId(), Integer.parseInt(apdetails.getResources().split(",")[0]));
                                                            if (transformDetails != null) {
                                                                warfiles = new WarFileManagement().getWarFile(SessionId, channelId, apdetails.getApId());
                                                                if (warfiles != null) {%>

                                            <option value="<%=accesspoints2[i].getName()%>"><%=accesspoints2[i].getName()%></option>
                                            <%}
                                                            }
                                                        }
                                                    }
                                                }%>
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <select id="_ResourceForSlabPricing2" name="_ResourceForSlabPricing2"  class="form-control span2" onchange="showSlabVersion(this.value)" style="width: 100%">
                                            <option value="-1" selected>Select Resource</option>                                                                                  
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <select id="_VersionForSlabPricing2" name="_VersionForSlabPricing2"  class="form-control span2" onchange="showSlabAPI(this.value)" style="width: 100%">
                                            <option value="-1" selected>Select Version</option>                                                                                  
                                        </select>
                                    </div>
                                    <div class="col-lg-2">

                                        <select id="_APIForSlabPricing2" name="_APIForSlabPricing2"  class="form-control span2" onchange="showSlabPricing(this.value)" style="width: 100%">
                                            <option value="-1" selected>Select API</option>                                                                                  
                                        </select>
                                    </div>                                        
                                </div>
                                <div id="_slabPriceWindow">                                            
                                </div>                                       
                                <!--                                <div id="_API">
                                                                </div>-->

                            </div>
                            <a  class="btn btn-success btn-xs" Style="margin-left: 17%" onclick="editSlabPrice('<%=packageId%>')"><i class="fa fa-edit"></i> Save & Next</a>
                        </form>
                    </div>

                    <div  class="tab-pane fade" id="tieringPrice">
                        <form class="form-horizontal" id="edit_tp_form" name="edit_tp_form" role="form">
                            <input type="hidden" id="packageID1" name="packageID1" value="<%=packageId%>">
                            <div class="form-group">                            
                                <label class="col-lg-2 control-label">Tier Pricing</label>                                  
                                <div class=col-sm-2>

                                    <select id="TieringPricing" name="TieringPricing" class="form-control" <% if (!packageObj.getPaymentMode().equalsIgnoreCase("postpaid")) {%> disabled <%}%>onchange="tieringPriceDiv(this.value)">
                                        <option   value="Enable">Enable</option>
                                        <option selected value="Disable">Disable</option>
                                    </select>
                                </div>                                    
                            </div>
                            <div id="apTieringrate" style="display: none">
                                <div class="form-group">
                                    <label class="col-lg-2 control-label" style="">Access Point</label>                                  
                                    <div class="col-lg-2">
                                        <select id="_Accesspoint3" name="_Accesspoint3"  class="form-control span2" onchange="acessChangeResForTiering()" style="width: 100%">
                                            <option value="-1">Select AP</option>
                                            <%
                                                Accesspoint[] accesspoints3 = null;
                                                warfiles = null;
                                                accesspoints3 = new AccessPointManagement().getAllAccessPoint(SessionId, channelId);
                                                if (accesspoints3 != null) {
                                                    for (int i = 0; i < accesspoints1.length; i++) {
                                                        if (accesspoints1[i].getStatus() != GlobalStatus.DELETED) {
                                                            Accesspoint apdetails = accesspoints1[i];
                                                            TransformDetails transformDetails = new TransformManagement().getTransformDetails(SessionId, channelId, apdetails.getApId(), Integer.parseInt(apdetails.getResources().split(",")[0]));
                                                            if (transformDetails != null) {
                                                                warfiles = new WarFileManagement().getWarFile(SessionId, channelId, apdetails.getApId());
                                                                if (warfiles != null) {%>
                                            <option value="<%=accesspoints3[i].getName()%>"><%=accesspoints3[i].getName()%></option>
                                            <%}
                                                            }
                                                        }
                                                    }
                                                }%>
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <select id="_ResourceForTieringPricing2" name="_ResourceForTieringPricing2"  class="form-control span2" onchange="showTieringVersion(this.value)" style="width: 100%">
                                            <option value="-1" selected>Select Resource</option>                                                                                  
                                        </select>
                                    </div>
                                    <div class="col-lg-2">
                                        <select id="_VersionForTieringPricing2" name="_VersionForTieringPricing2"  class="form-control span2" onchange="showTieringAPI(this.value)" style="width: 100%">
                                            <option value="-1" selected>Select Version</option>                                                                                  
                                        </select>
                                    </div>
                                    <div class="col-lg-2">

                                        <select id="_APIForTieringPricing2" name="_APIForTieringPricing2"  class="form-control span2" onchange="showTieringPricing(this.value)" style="width: 100%">
                                            <option value="-1" selected>Select API</option>                                                                                  
                                        </select>
                                    </div>                                        
                                </div>
                                <div id="_tieringPriceWindow">                                            
                                </div> 
                            </div>
                            <a  class="btn btn-success btn-xs" Style="margin-left: 17%" onclick="editTierPrice('<%=packageId%>')"><i class="fa fa-edit"></i> Save & Next</a>
                        </form>
                    </div>

                    <div  class="tab-pane fade" id="alerts">
                        <%
                            String signUpM = "";
                            String trialPackageM = "";
                            String changeOnPackageM = "";
                            String latePenaltiesM = "";
                            String terOfServiceM = "";
                            String reActivationM = "";
                            String lowBalanceM = "";
                            String pdfSigningC = "";
                            String encryptedPDF = "";
                            JSONObject alertJSONObj = null;
                            if (packageObj.getSecurityAndAlertDetails() != null && !packageObj.getSecurityAndAlertDetails().equals("")) {
                                String securityAndAlert = packageObj.getSecurityAndAlertDetails();
                                if (securityAndAlert != null) {
                                    JSONArray jsOld = new JSONArray(securityAndAlert);
                                    for (int j = 0; j < jsOld.length(); j++) {
                                        JSONObject latePenalitesJson = jsOld.getJSONObject(j);
                                        if (latePenalitesJson != null) {
                                            if (latePenalitesJson.has(packageObj.getBucketName())) {
                                                alertJSONObj = (JSONObject) latePenalitesJson.get(packageObj.getBucketName());
                                                signUpM = alertJSONObj.getString("signUpAlertMesssage");
                                                trialPackageM = alertJSONObj.getString("trialPackageAlertMesssage");
                                                changeOnPackageM = alertJSONObj.getString("changeOnPackageAlertMesssage");
                                                latePenaltiesM = alertJSONObj.getString("latePenaltiesAlertMesssage");
                                                terOfServiceM = alertJSONObj.getString("terOfServiceAlertMesssage");
                                                reActivationM = alertJSONObj.getString("reActivationAlertMesssage");
                                                lowBalanceM = alertJSONObj.getString("lowBalanceMesssage");
                                                pdfSigningC = alertJSONObj.getString("pdfSigning");
                                                encryptedPDF = alertJSONObj.getString("encryptedPDF");
                                            }
                                        }
                                    }
                                }
                            }
                        %>
                        <label class="col-lg-12 control-label">In message where you want to add Developer name just put #DeveloperName#, for date just put #dateTime#,</label>
                        <form class="form-horizontal" id="edit_seandart_form" name="edit_seandart_form" role="form">
                            <div class="form-group">
                                <input type="checkbox" class="col-lg-1" id="trialCheck">
                                <label class="col-lg-2 control-label">Sign Up</label>
                                <div class="col-lg-6">
                                    <div class=" input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>                                           
                                        <textarea class="form-control" id="signUpAlertMesssage" name="signUpAlertMesssage" placeholder="Message to be send to Developer when he/she do sign up"><%=signUpM%></textarea>                                           
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" class="col-lg-1" id="trialCheck">
                                <label class="col-lg-2 control-label">Trial Package</label>
                                <div class="col-lg-6">
                                    <div class=" input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                        <textarea class="form-control" id="trialPackageAlertMesssage" name="trialPackageAlertMesssage" placeholder="Message to be send to Developer when trial package is going to expire"><%=trialPackageM%></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" class="col-lg-1" id="trialCheck">
                                <label class="col-lg-2 control-label">Package changes</label>
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                        <textarea class="form-control" id="changeOnPackageAlertMesssage" name="changeOnPackageAlertMesssage" placeholder="Message to be send to Developer when change his/her package"><%=changeOnPackageM%></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" class="col-lg-1" id="trialCheck">
                                <label class="col-lg-2 control-label">Late Penalties</label>
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                        <textarea class="form-control" id="latePenaltiesAlertMesssage" name="latePenaltiesAlertMesssage" placeholder="Message to be send to Developer when late penalties charged"><%=latePenaltiesM%></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" class="col-lg-1" id="trialCheck">
                                <label class="col-lg-2 control-label">Service Termination</label>
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                        <textarea class="form-control" id="terOfServiceAlertMesssage" name="terOfServiceAlertMesssage" placeholder="Message to be send to Developer when service is terminated"><%=terOfServiceM%></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" class="col-lg-1" id="trialCheck">
                                <label class="col-lg-2 control-label">Service Reactivation</label>
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                        <textarea class="form-control" id="reActivationAlertMesssage" name="reActivationAlertMesssage" placeholder="Message to be send to Developer when he/she reactivate the service"><%=reActivationM%></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" class="col-lg-1" id="trialCheck">
                                <label class="col-lg-2 control-label">Low balance</label>
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                        <textarea class="form-control" id="lowBalanceMesssage" name="lowBalanceMesssage"  placeholder="Message to be send to Developer when Developer have low balance"><%=lowBalanceM%></textarea>
                                    </div>
                                </div>
                            </div>

                            <% if (pdfSigningC.equals("enable")) {%>
                            <input type="radio" class="" id="pdfFeature" name="pdfFeature" value="pdfSigningEnable" style="margin: 2%" checked>
                            <%} else {%>
                            <input type="radio" class="" id="pdfFeature" name="pdfFeature" value="pdfSigningEnable" style="margin: 2%">
                            <%}%>
                            <label class=" control-label" style="margin-right: 1%">PDF Signing</label>  
                            <% if (encryptedPDF.equals("enable")) {%>
                            <input type="radio" class="" id="pdfFeature" name="pdfFeature" value="encryptedPDFEnable" style="margin: 2%" checked>
                            <%} else {%>
                            <input type="radio" class="" id="pdfFeature" name="pdfFeature" value="encryptedPDFEnable" style="margin: 2%">
                            <%}%>
                            <label class=" control-label">Encrypted PDF</label><br><br>
                            <a  class="btn btn-success btn-xs" Style="margin-left: 26%" onclick="editSecurityAndAlert('<%=packageId%>', '<%=featureFlag%>')"><i class="fa fa-edit"></i> Save & Next</a> 
                        </form>
                    </div>
                    <%if (featureFlag) {%>    
                    <div  class="tab-pane fade" id="feature">                                    
                        <%
                            String feature = packageObj.getFeatureList();
                            JSONObject featJSONObj = null;
                            String promoCodeFlag = "";
                            String loanLimit = "0";
                            String interestRate = "0";
                            String reedmePrice = "0";
                            String reedmePoint = "0";
                            if (feature != null) {
                                JSONArray alertJson = new JSONArray(feature);
                                for (int i = 0; i < alertJson.length(); i++) {
                                    JSONObject jsonexists1 = alertJson.getJSONObject(i);
                                    if (jsonexists1.has(packageObj.getBucketName())) {
                                        featJSONObj = jsonexists1.getJSONObject(packageObj.getBucketName());
                                        if (featJSONObj != null) {
                                            promoCodeFlag = featJSONObj.getString("promoCodeFlag");
                                            if (!promoCodeFlag.equals("Disable")) {
                                                loanLimit = featJSONObj.getString("loanLimit");
                                                interestRate = featJSONObj.getString("interestRate");
                                                reedmePoint = featJSONObj.getString("reedmePoint");
                                                reedmePrice = featJSONObj.getString("reedmePrice");
                                            }
                                            break;
                                        }
                                    }
                                }
                            }
                        %>
                        <form class="form-horizontal" id="edit_feature_form" name="edit_feature_form" role="form">
                            <%if (promocodeEnable.equalsIgnoreCase("true")) {%>
                            <div class="form-group">                            
                                <label class="col-lg-3 control-label">Promo Code</label>                                  
                                <div class=col-sm-2>
                                    <select id="promoCodeFlag" name="promoCodeFlag" class="form-control" onchange="">
                                        <% if (promoCodeFlag.equals("Enable")) {%>
                                        <option selected value="Enable">Enable</option>
                                        <option value="Disable">Disable</option>
                                        <%} else {%>
                                        <option value="Enable">Enable</option>
                                        <option selected value="Disable">Disable</option>
                                        <%}%>
                                    </select>
                                </div>                                    
                            </div>
                            <%}
                                if (loanEnable != null && loanEnable.equalsIgnoreCase("true")) {%>        
                            <div class="form-group">                            
                                <label class="col-lg-3 control-label">Loan with additional Charge</label>
                                <div id="_LoanFacilityFeature">
                                </div>
                                <script>
                                    <%  if (!packageObj.getPaymentMode().equalsIgnoreCase("postpaid")) {
                                            if (Float.parseFloat(loanLimit) > 0 && Float.parseFloat(interestRate) > 0) {
                                    %>
                                    LoanFacilityFeature("Enable");
                                    document.getElementById("loanLimit").value = '<%=loanLimit%>';
                                    document.getElementById("interestRate").value = '<%=interestRate%>';
                                    <%} else {%>
                                    LoanFacilityFeature("Disable");
                                    <%}
                                    } else {%>
                                    LoanFacilityFeature("readOnly");
                                    <%}%>
                                </script>
                            </div>
                            <%}%>
                            <div class="form-group" style="display: none">                            
                                <label class="col-lg-3 control-label">Redeem Credit Point</label>
                                <div id="_ReedmeCreditPointFeature">
                                </div>
                                <script>
                                    <%if (Float.parseFloat(loanLimit) > 0 && Float.parseFloat(interestRate) > 0) {%>
                                    ReedmeCreditPointFeature("Enable");
                                    document.getElementById("reedmePrice").value = '<%=reedmePrice%>';
                                    document.getElementById("reedmePoint").value = '<%=reedmePoint%>';
                                    <%} else {%>
                                    ReedmeCreditPointFeature("Disable");
                                    <%}%>
                                </script>
                            </div>
                            <br><br>
                            <a  class="btn btn-success btn-xs btn-xs" Style="margin-left: 26%" onclick="editFeature('<%=packageId%>')"><i class="fa fa-edit"></i> Save Feature</a> 
                        </form>
                    </div>
                    <%}%>     
                </div>
                <br><br>
            </div>                                                    
        </div>                                           
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#visibleTo").select2();
    });

    $(document).ready(function () {
        var tabCount = document.getElementById("firstTab").value;
        if (tabCount == 1) {
            $('#rateTab').addClass('enable');
            $('#accessPointTab').addClass('disabled');
            $('#flatRateTab').addClass('disabled');
            $('#apiThrottlingTab').addClass('disabled');
            $('#slabPricingTab').addClass('disabled');
            $('#slabPricingTab').removeAttr('disabled');
            $('#tieringPricingTab').addClass('disabled');
            $('#securityTab').addClass('disabled');
            $('#featureTab').addClass('disabled');
        } else if (tabCount == 2) {
            $('#rateTab').addClass('enable');
            $('#accessPointTab').addClass('enable');
            $('#flatRateTab').addClass('disabled');
            $('#apiThrottlingTab').addClass('disabled');
            $('#slabPricingTab').addClass('disabled');
            $('#slabPricingTab').removeAttr('disabled');
            $('#tieringPricingTab').addClass('disabled');
            $('#securityTab').addClass('disabled');
            $('#featureTab').addClass('disabled');
        } else if (tabCount == 3) {
            $('#rateTab').addClass('enable');
            $('#accessPointTab').addClass('enable');
            $('#flatRateTab').addClass('enable');
            $('#apiThrottlingTab').addClass('disabled');
            $('#slabPricingTab').addClass('disabled');

            $('#tieringPricingTab').addClass('disabled');
            $('#securityTab').addClass('disabled');
            $('#featureTab').addClass('disabled');
        } else if (tabCount == 4) {
            $('#rateTab').addClass('enable');
            $('#accessPointTab').addClass('enable');
            $('#flatRateTab').addClass('enable');
            $('#apiThrottlingTab').addClass('enable');
            $('#slabPricingTab').addClass('disabled');
            $('#tieringPricingTab').addClass('disabled');
            $('#securityTab').addClass('disabled');
            $('#featureTab').addClass('disabled');
        } else if (tabCount == 5) {
            $('#rateTab').addClass('enable');
            $('#accessPointTab').addClass('enable');
            $('#flatRateTab').addClass('enable');
            $('#apiThrottlingTab').addClass('enable');
            $('#slabPricingTab').addClass('enable');
            $('#tieringPricingTab').addClass('disabled');
            $('#securityTab').addClass('disabled');
            $('#featureTab').addClass('disabled');
        } else if (tabCount == 6) {
            $('#rateTab').addClass('enable');
            $('#accessPointTab').addClass('enable');
            $('#flatRateTab').addClass('enable');
            $('#apiThrottlingTab').addClass('enable');
            $('#slabPricingTab').addClass('enable');
            $('#tieringPricingTab').addClass('enable');
            $('#securityTab').addClass('disabled');
            $('#featureTab').addClass('disabled');
        } else if (tabCount == 7) {
            $('#rateTab').addClass('enable');
            $('#accessPointTab').addClass('enable');
            $('#flatRateTab').addClass('enable');
            $('#apiThrottlingTab').addClass('enable');
            $('#slabPricingTab').addClass('enable');
            $('#tieringPricingTab').addClass('enable');
            $('#securityTab').addClass('enabled');
            $('#featureTab').addClass('disabled');
        }
    });
    $(document).ready(function () {
        $("#addPackageTab > li").click(function () {
            if ($(this).hasClass("disabled"))
                return false;
        });
    });




</script>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="dist/js/sb-admin-2.js"></script>
<script src="./select2/select2.js"></script>
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>
<script src="js/bootbox.min.js" type="text/javascript"></script>  
</body>
</html>                
<%@include file="footer.jsp" %>
