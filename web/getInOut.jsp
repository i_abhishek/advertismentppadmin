<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgRequestForProduction"%>
<%@page import="java.util.Map"%>
<%
    Map<String, SgRequestForProduction> productionMap = (Map) session.getAttribute("RequestForProductionMap");
    String apiName = request.getParameter("_APIForSlabPricing2");
    SgRequestForProduction forProduction = productionMap.get(apiName);

%>
<br><br>  

<div class="col-lg-6">
    <form  role="form" class="form-inline" id="apiRespone" name="apiRespone">
        <div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-code-fork"></i><b> Request</b>
            </div>
            <div class="panel-body">
                <table>
                    <tr>

                    <textarea style="width: 100%;height: 312px" class="form-control" id="responseID" name="responseID"><%=forProduction.getApiInput()%></textarea>

                    </tr>
                </table>
            </div>
        </div>
    </form>
</div>
<div class="col-lg-6">
    <form  role="form" class="form-inline" id="apiRespone" name="apiRespone">
        <div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-code-fork"></i><b> Response</b>
            </div>
            <div class="panel-body">
                <table>
                    <tr>

                    <textarea style="width: 100%;height: 312px" class="form-control" id="responseID" name="responseID"><%=forProduction.getApiOutput()%></textarea>

                    </tr>
                </table>
            </div>
        </div>
    </form>
</div>
<a  class="btn btn-danger btn-xs" onclick="rejectProductionAccessRequest()"><i class="fa fa-close"></i> Reject</a> 
<a  class="btn btn-success btn-xs" onclick="requestStatus('<%=GlobalStatus.APPROVED%>')"><i class="fa fa-check-circle"></i> Approve</a> 