<%@page import="java.util.Iterator"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgResourceprice"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourcePriceMgmt"%>
<%@include file="header.jsp" %>
<%
    int id = Integer.parseInt(request.getParameter("resId"));
    SgResourceprice resourceprice = new ResourcePriceMgmt().getDetailsFromResId(id);
    JSONObject json = new JSONObject(resourceprice.getPrice());
    String[] apiName = new String[json.length()];
%>
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header text-uppercase">Resource API Management</h3>
            </div>
            <!-- /.col-lg-10 -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47;  <i class="fa fa-table"></i><b> List of Resources</b>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal " method="post" id="editAPIPriceDetails" name="editAPIPriceDetails">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover"  width="400">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Name</th>
                                            <th>Price</th>
                                        </tr>
                                        <%int count = 0;
                                            if (json.length() != 0) {
                                                Iterator itr = json.keys();
                                                while (itr.hasNext()) {
                                                    String key = (String) itr.next();
                                                    apiName[count] = key;
                                                    count++;
                                        %>
                                        <tr>
                                            <td><%=count%></td>
                                            <td><%=key%></td>
                                            <td><input type="text" name="_eAPI<%=key%>" id=_eAPI<%=key%> placeholder="Enter Price" value="<%=json.getString(key)%>"></td>
                                        </tr>
                                        <%}
                                    } else {%>
                                        <tr>
                                            <th>No Record Found</th>
                                            <th>No Record Found</th>
                                            <th>No Record Found</th>
                                        </tr>
                                        <%}
                                            session.setAttribute("apiDetails", apiName);
                                        %>
                                    </thead>
                                </table>
                            </div>
                            <%if (count != 0) {%>
                            <a href="#" class="btn btn-info btn-sm" onclick="editAPIPrice(<%=id%>)" ><i class="fa fa-eye"></i> Save </a>
                            <%}%>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="./js/resourcePrice.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="dist/js/sb-admin-2.js"></script>
<%@include file="footer.jsp" %>