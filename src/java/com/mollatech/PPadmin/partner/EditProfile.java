package com.mollatech.PPadmin.partner;

import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerRequestManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Bluebricks
 */
public class EditProfile extends HttpServlet {

    static final int ACTIVE_STATUS = 1;

    static final int SUSPENDED_STATUS = 0;

    static final int PENDING_STATUS = -1;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Developer added successfully";
        PrintWriter out = response.getWriter();
        int TPD = -1, TPS = -1;
        int errorcode = -99;
        String pName = request.getParameter("partnerName");
        String pEmail = request.getParameter("partnerEmail");
        String pMobileNo = request.getParameter("partnerMobNo");
        String pID = request.getParameter("pID");
        int partnerId = -1;
        if (pID != null) {
            partnerId = Integer.parseInt(pID);
        }
        int pStatus = PENDING_STATUS;
        String ipAddress = request.getParameter("partnerIP");
        String iplive = request.getParameter("partnerIPLive");
        String pWebsite = request.getParameter("partnerWebsite");
        String pFax = request.getParameter("partnerFax");
        String Landline = request.getParameter("partnerLandlineNo");
        String pCompanyName = request.getParameter("partnerComName");
        String addr1 = request.getParameter("partnerAddress");
        String pAddress = addr1;
        String pPincode = request.getParameter("partnerPincode");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        int ps = new PartnerRequestManagement().editPartnerRequest(SessionId, partnerId, pName, pStatus, pMobileNo, pEmail, ipAddress, pWebsite, pFax, Landline, pAddress, pPincode, pCompanyName, iplive);
        if (ps == 0) {
            result = "success";
            message = "Developer Updated successfully";
        } else if (ps == -1) {
            result = "error";
            message = "Failed to updated Developer request";
        } else if (ps == -99) {
            result = "error";
            message = "Failed to updated Developer request";
        } else if (ps == -11) {
            result = "error";
            message = "Please fill all details";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
