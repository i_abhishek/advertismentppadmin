/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.advertiser;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.commons.MakerChaker;
import com.mollatech.serviceguard.nucleus.db.SgAdvPreApprovalPackagedetails;
import com.mollatech.serviceguard.nucleus.db.SgApprovedAdPackagedetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvPreApprovalPackageManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.ApprovedAdPackageManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "EditPDFAdDetails", urlPatterns = {"/EditPDFAdDetails"})
public class EditPDFAdDetails extends HttpServlet {

        static final Logger logger = Logger.getLogger(EditPDFAdDetails.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Request servlet is #EditFeature from #PPAdmin at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Package created successfully.";
        
        String channelId = (String) request.getSession().getAttribute("_channelId");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        MakerChaker makerChakerObj = (MakerChaker) request.getSession().getAttribute("makerChacker");
        String packageID = request.getParameter("packageId");
        logger.debug("Value of packageID  = " + packageID);
        int pId = 0;
        if (packageID != null) {
            pId = Integer.parseInt(packageID);
        }
        SgAdvPreApprovalPackagedetails packObj = new AdvPreApprovalPackageManagement().getPackageDetails(SessionId, channelId, pId);
        try {
            if (packObj == null) {
                result = "error";
                message = "Can't edit the package details.";
                json.put("result", result);
                json.put("message", message);
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String pdfAdPageLength = request.getParameter("pdfAdPageLength");
        String pdfAdPerDayCount = request.getParameter("pdfAdPerDayCount");
        String pdfAdCreditDeduction = request.getParameter("pdfAdCreditDeduction");        
        
        JSONObject pdfAdDetailsJson = new JSONObject();        
        if(pdfAdPageLength != null && !pdfAdPageLength.isEmpty()){
            pdfAdDetailsJson.put("pdfImageLength", pdfAdPageLength);
        }
        if (pdfAdPerDayCount != null && !pdfAdPerDayCount.equals("")) {
            pdfAdDetailsJson.put("pdfAdPerDay", pdfAdPerDayCount);
        }
        if (pdfAdCreditDeduction != null && !pdfAdCreditDeduction.equals("")) {
            pdfAdDetailsJson.put("creditDeductionPerAd", pdfAdCreditDeduction);
        }        
        int retValue = 0;
        packObj.setStatus(GlobalStatus.ACTIVE);
        String PartnerVisibility[] = request.getParameterValues("visibilityPartner");
        String partnerId = "";
        if (PartnerVisibility != null) {
            for (int i = 0; i < PartnerVisibility.length; i++) {
                partnerId += PartnerVisibility[i] + ",";
            }
        }
        packObj.setPartnerVisibility(partnerId);
        try {
            packObj.setPdfAdConfiguration(pdfAdDetailsJson.toString());
            if (packObj.getPdfAdConfiguration() == null || packObj.getPdfAdConfiguration().equals("")) {                                                
                if (makerChakerObj != null) {
                    if (makerChakerObj.status == GlobalStatus.SUSPEND) {
                        packObj.setStatus(GlobalStatus.APPROVED);
                    }
                }
                retValue = new AdvPreApprovalPackageManagement().editPackage(SessionId, packObj);
            } else {                                
                retValue = new AdvPreApprovalPackageManagement().editPackage(SessionId, packObj);
            }
            if (retValue == -1) {
                result = "error";
                message = "PDF Ad details does not saved.";
                json.put("result", result);
                logger.debug("Response of #EditFeature from #PPAdmin Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of #EditFeature from #PPAdmin Servlet's Parameter  message is " + message);
            } else {
                SgApprovedAdPackagedetails reqObj = new SgApprovedAdPackagedetails();
                SgApprovedAdPackagedetails existsObj = new ApprovedAdPackageManagement().getReqPackageByName(SessionId, channelId, packObj.getPackageName());
                
                if (makerChakerObj != null) {
                    if (makerChakerObj.status == GlobalStatus.ACTIVE) {
                        reqObj.setMakerflag(1);
                        reqObj.setStatus(packObj.getStatus());
                        message = "Package created successfully and send Checker to validate";
                    } else {
                        reqObj.setStatus(GlobalStatus.APPROVED);
                        reqObj.setMakerflag(0);
                    }
                } else {
                    reqObj.setStatus(packObj.getStatus());
                    reqObj.setMakerflag(GlobalStatus.APPROVED);
                }
                reqObj.setMainCredits(packObj.getMainCredits());
                if (existsObj == null) {
                    reqObj.setChannelId(packObj.getChannelId());
                    reqObj.setPackageName(packObj.getPackageName());
                    reqObj.setPlanAmount(packObj.getPlanAmount());
                    reqObj.setPaymentMode(packObj.getPaymentMode());
                    reqObj.setPackageDuration(packObj.getPackageDuration());
                    reqObj.setPartnerVisibility(packObj.getPartnerVisibility());                                                            
                    reqObj.setTax(packObj.getTax());
                    reqObj.setCreationDate(packObj.getCreationDate());
                    reqObj.setPushAdConfiguration(packObj.getPushAdConfiguration());
                    reqObj.setEmailAdConfiguration(packObj.getEmailAdConfiguration());
                    reqObj.setPdfAdConfiguration(packObj.getPdfAdConfiguration());                    
                    reqObj.setPackageDescription(packObj.getPackageDescription());
                    reqObj.setRecurrenceBillingPlanId(packObj.getRecurrenceBillingPlanId());
                    new ApprovedAdPackageManagement().CreateReqPackage(SessionId, channelId, reqObj);
                } else {
                    reqObj.setChannelId(existsObj.getChannelId());
                    reqObj.setPackageName(existsObj.getPackageName());
                    reqObj.setPlanAmount(existsObj.getPlanAmount());
                    reqObj.setPaymentMode(existsObj.getPaymentMode());
                    reqObj.setPackageDuration(existsObj.getPackageDuration());
                    reqObj.setPartnerVisibility(existsObj.getPartnerVisibility());                    
                    reqObj.setTax(existsObj.getTax());
                    reqObj.setCreationDate(existsObj.getCreationDate());
                    reqObj.setPushAdConfiguration(packObj.getPushAdConfiguration());
                    reqObj.setEmailAdConfiguration(packObj.getEmailAdConfiguration());
                    reqObj.setPdfAdConfiguration(packObj.getPdfAdConfiguration());                       
                    reqObj.setStatus(GlobalStatus.SENDTO_CHECKER);
                    reqObj.setPackageDescription(packObj.getPackageDescription());
                    reqObj.setRecurrenceBillingPlanId(packObj.getRecurrenceBillingPlanId());
                    new ApprovedAdPackageManagement().editReqPackage(SessionId, channelId, reqObj);
                   // message = "Package Updated successfully.";
                    message = "Package updated successfully and send Checker to validate";
                }
                json.put("result", result);
                json.put("message", message);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Exception at #EditFeature from #PPAdmin " + e);
        } finally {
            logger.info("Response of #EditFeature from #PPAdmin " + json.toString());
            logger.info("Response of #EditFeature from #PPAdmin Servlet at " + new Date());
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
