<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgResourcecount"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceCountMgmt"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceOwnerManagement"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgResourceowner"%>

<div id="rMonthlyDetails">
    <ul class="nav nav-pills" id="revenueDTab">
        <li class="active" id="rateTab"><a href="#graphReport" data-toggle="tab">Graphical Report</a></li>                    
        <li id="slabPricingTab"><a href="#tabReport" data-toggle="tab">Tabular Report</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade in active" id="graphReport">            
            <br><br>
            <div class="row-fluid">
                <div class="span12" style="alignment-adjust: central">    
                    <h5 align="center" class="text-primary" style="font-weight: bold">Revenue details</h5>
                    <div id="_revData"></div>    
                </div>
                <br>
                <div class="span12" style="alignment-adjust: central">    
                    <h5 align="center" class="text-primary" style="font-weight: bold">Last 7 day Revenue Status</h5>
                    <font style="font-family: monospace">Revenue details</font>
                    <div id="_revGraphData"></div>    
                    <div  align="center" style="font-family: monospace" >
                        Days
                    </div>
                </div>
                <br>
                <div  class="span12" style="alignment-adjust: central">
                    <h5 align="center" class="text-primary" style="font-weight: bold">Weekly Revenue Status</h5>
                    <font style="font-family: monospace">
                    Revenue details
                    </font>
                    <div id="_revBarData"></div>         
                    <div  align="center" style="font-family: monospace" >
                        Week
                    </div>
                </div> 
                <div class="form-group" id="_revGraphData"></div>                          
            </div>
        </div>
        <div class="tab-pane fade in " id="tabReport">
            <br>
<div class="row-fluid">
    <table class="table table-striped table-bordered table-hover" id="dataTables-example">  
        <tr class="">
            <th style="text-align: center">No.</th>
            <th style="text-align: center">Resource</th>        
            <th style="text-align: center">Environment</th>
            <th style="text-align: center">API</th>
            <th style="text-align: center">Call Count</th>
            <th style="text-align: center">Unit Cost</th>
            <th style="text-align: center">Total Cost</th>
            <th style="text-align: center">Date</th>
        </tr>
        <%
            String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
            int resOwnerId = Integer.parseInt(request.getParameter("resownname"));
            int ResourceId = Integer.parseInt(request.getParameter("resId"));
            String month = request.getParameter("month");
            String year = request.getParameter("year");
            String info = "No record found";
            DateFormat sdf = new SimpleDateFormat("yyyy-MMM-dd");
            Date today = sdf.parse(year + "-" + month + "-01");
            today.setHours(0);
            today.setMinutes(0);
            today.setSeconds(0);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(today);
            calendar.add(Calendar.MONTH, 1);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.add(Calendar.DATE, -1);
            Date lastDayOfMonth = calendar.getTime();
            lastDayOfMonth.setHours(23);
            lastDayOfMonth.setMinutes(59);
            lastDayOfMonth.setSeconds(59);
            Map resMap = new HashMap();
            SgResourceowner resourceowner = new ResourceOwnerManagement().getResourceownerbyId(SessionId, resOwnerId);
            int count = 1;
            if (resourceowner != null) {
                SgResourcecount[] resourcecount = new ResourceCountMgmt().getDetails(ResourceId, today, lastDayOfMonth);
                session.setAttribute("resourceOwnerReport", resourcecount);
                if (resourcecount != null) {
                    float totalRevenue = 0;
                    for (SgResourcecount sgResourcecount : resourcecount) {
                        JSONObject data = new JSONObject(sgResourcecount.getCountI());
                        String resName = "NA";
                        if (resMap.isEmpty()) {
                            ResourceDetails resObj = new ResourceManagement().getResourceById(sgResourcecount.getResourceId());
                            if (resObj != null) {
                                resMap.put(resObj.getResourceId(), resObj.getName());
                            }
                        } else if (resMap.get(sgResourcecount.getResourceId()) == null) {
                            ResourceDetails resObj = new ResourceManagement().getResourceById(sgResourcecount.getResourceId());
                            resMap.put(resObj.getResourceId(), resObj.getName());
                        }
                        resName = resMap.get(sgResourcecount.getResourceId()).toString();
                        Iterator iterator = data.keys();

                        while (iterator.hasNext()) {
                            float totalAmout = 0;
                            String key = (String) iterator.next();
                            String cost = data.getString(key).split(":")[0];
                            String call = data.getString(key).split(":")[1];
                            totalAmout += Float.parseFloat(data.getString(key).split(":")[0]) * Float.parseFloat(data.getString(key).split(":")[1]);

                            if (totalAmout != 0.0) {
        %>           
        <tr>
            <td style="text-align: center"><%=count%></td>
            <td style="text-align: center"><%=resName%></td>
            <td style="text-align: center"><%=sgResourcecount.getEnvironment()%></td>
            <td style="text-align: center"><%=key%></td>
            <td style="text-align: center"><%=call%></td>
            <td style="text-align: center"><%=cost%></td>
            <td style="text-align: center"><%=totalAmout%></td>
            <td style="text-align: center"><%=sdf.format(sgResourcecount.getExecutionDate())%></td>
        </tr>
        <%
                        count++;
                        totalRevenue = totalRevenue + totalAmout;
                    }
                }
            }
        %>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>Total Revenue</td>
            <td style="text-align: center"><%=totalRevenue%></td>
            <td></td>
        </tr>
        <%
        } else {
        %>
        <tr>
            <td style="text-align: center"><%=count%></td>
            <td><%=info%></td>
            <td><%=info%></td>
            <td><%=info%></td>
            <td><%=info%></td>
            <td><%=info%></td>
            <td><%=info%></td>
            <td><%=info%></td>
        </tr>
        <%    }
        } else {
        %>
        <tr>
            <td style="text-align: center"><%=count%></td>
            <td><%=info%></td>
            <td><%=info%></td>
            <td><%=info%></td>
            <td><%=info%></td>
            <td><%=info%></td>
            <td><%=info%></td>
            <td><%=info%></td>
        </tr>
        <%}%>        
    </table>
    <br>
    <table>
        <tr>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="#" class="btn btn-primary" onclick="revenuePDFDownload(1)" >
                    <i></i>Download CSV</a>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>  
            <td>    
                <a href="#" class="btn btn-primary" onclick="revenuePDFDownload(0)" >
                    <i></i>Download PDF</a>
            </td>  
        </tr>     
    </table>    
</div>
</div>
    </div>
</div>    