<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.GroupManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.GroupDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@include file="header.jsp"%>
<link href="./css/select2.css" rel="stylesheet"/>
<script src="./js/select2.js"></script>
<script src="./js/json3.min.js"></script>
<link href="./css/bootstrap-timepicker.css" rel="stylesheet"/>
<link rel="stylesheet" href="./css/datepicker.css">
<script src="./js/operatorsTextReports.js"></script>
<script src="./js/modal.js"></script>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="./js/reports.js"></script>
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">                
                <h4 class="page-header">Manual Payment</h4>
            </div>
            <!-- /.col-lg-10 -->
        </div>
        <div class="row">         
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47;
                        <i class="fa fa-table"></i> Manual Payment &#47;                                                                   
                    </div>
                    <div class="panel-body">
                        <table border="0">
                            <tr>
                                <td>                            
                                    From:
                                </td>
                                <td>
                                    <div id="datetimepicker1" class="date">
                                        &nbsp;<input id="_startdate" name="_startdate" class="datepicker" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth" style="width: 90%" />
                                        <span class="add-on" hidden>
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar" ></i>
                                        </span>
                                    </div>
                                </td>
                                <td>
                                    Till:
                                </td>
                                <td>
                                    <div id="datetimepicker2" class="date">
                                        <input id="_enddate" name="_enddate" class="datepicker" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth" style="width: 90%"/>
                                        <span class="add-on" hidden>
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar" ></i>
                                        </span>
                                    </div>  
                                </td>
                                <td>Developer Name:</td>&nbsp;&nbsp;&nbsp;
                                <td>
<!--                                    <input class="form-control" type="text" id="_partnername" name="_partnername" placeholder="Partner Name" style="width: 90%">-->
                                    <select id="_partnername" name="_partnername" class="form-control span2" style="width: 100%">
                                        <option value="-1" selected>Select Developer</option>     
                                        <%
                                            PartnerDetails[] pd = new PartnerManagement().getAllPartnerDetails(SessionId, channelId);
                                            if (pd != null) {
                                                for (int i = 0; i < pd.length; i++) {
                                        %>
                                        <option value="<%=pd[i].getPartnerId()%>"><%=pd[i].getPartnerName()%></option>
                                        <%}
                                    }%>                                          
                                    </select>
                                </td>
                                <td>
                                    <button class="btn btn-success form-control" id="Button" onclick="manualPayment()" style="margin-left: 10%"><i class="fa fa-table"></i> Generate</button>                        
                                </td>
                            </tr>   
                        </table> 
                        <br>
                        <div id="manualPaymentResult"></div>                    
                    </div>
                </div>                
            </div>
        </div>
    </div>
</div>
                                    <%@include file="footer.jsp" %>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="dist/js/moment.min.js" type="text/javascript"></script> 

<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>
<script src="js/bootbox.min.js" type="text/javascript"></script>
<script src="./js/bootstrap-timepicker.js"></script>
<script src="./js/bootstrap-datepicker.js"></script>                                    
<script>

                                        $(function () {
                                            $('#datetimepicker1').datepicker({
                                                language: 'pt-BR'
                                            });
                                        });
                                        $(function () {
                                            $('#datetimepicker2').datepicker({
                                                language: 'pt-BR'
                                            });
                                        });
                                        $('#_ApStartTime').timepicker({
                                            minuteStep: 1,
                                            showInputs: false,
                                            disableFocus: true

                                        });
                                        $('#_ApEndTime').timepicker({
                                            minuteStep: 1,
                                            showInputs: false,
                                            disableFocus: true
                                        });
                                        $('#datetimepicker1').on('changeDate', function (ev) {
                                            $(this).datepicker('hide');
                                        });
                                        $('#datetimepicker2').on('changeDate', function (ev) {
                                            $(this).datepicker('hide');
                                        });
</script>
