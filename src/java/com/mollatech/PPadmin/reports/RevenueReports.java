/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.reports;

import com.mollatech.serviceguard.nucleus.db.Channels;
import com.mollatech.serviceguard.nucleus.db.SgResourcecount;
import com.mollatech.serviceguard.nucleus.db.connector.management.ResourceOwnerManagement;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author urmila
 */
@WebServlet(name = "RevenueReports", urlPatterns = {"/RevenueReports"})
public class RevenueReports extends HttpServlet {

    static final Logger logger = Logger.getLogger(RevenueReports.class);

    private static int PDF_TYPE = 0;
    private static int CSV_TYPE = 1;
    private static final int BUFSIZE = 4096;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String _channelName = "ServiceGuard";

        logger.info("Requested Servlet is DownloadLogs at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        Channels channel = (Channels) request.getSession().getAttribute("_apSChannelDetails");
        String format = request.getParameter("_format");
        logger.debug("Value of format  = " + format);
        String ResOwnerName = request.getParameter("_resownname");
        logger.debug("Value of format  = " + ResOwnerName);
        String Rname = request.getParameter("_resId");
        logger.debug("Value of Rname  = " + Rname);
        String Mname = request.getParameter("_month");
        logger.debug("Value of Mname  = " + Mname);
        String Year = request.getParameter("_year");
        logger.debug("Value of Year  = " + Year);
        String filepath = null;
        SgResourcecount[] owner = (SgResourcecount[]) request.getSession().getAttribute("resourceOwnerReport");

        try {
            try {

                int iFormat = -9999;
                if (format != null && !format.isEmpty()) {
                    iFormat = Integer.parseInt(format);
                }
                if (PDF_TYPE == iFormat) {
                    iFormat = PDF_TYPE;
                } else if (CSV_TYPE == iFormat) {
                    iFormat = CSV_TYPE;
                }

                ResourceOwnerManagement rm = new ResourceOwnerManagement();
                filepath = rm.generateReport(iFormat, owner, _channelName, Mname, Year);

                /*ResourceOwnerManagement rom = new ResourceOwnerManagement();
                filepath=rom.generateReport(iFormat,owner.getChannelId(),Mname, Year);*/
                //filepath = cUtils.generateReport(iFormat, channel.getChannelId(), Mname, Year);
//                    requesttracks = null;
            } catch (Exception e) {
                logger.error("Exception at MessageReports ", e);
                e.printStackTrace();
            }
            File file = new File(filepath);
            int length = 0;
            ServletOutputStream outStream = response.getOutputStream();
            ServletContext context = getServletConfig().getServletContext();
            String mimetype = context.getMimeType(filepath);
            // sets response content type
            if (mimetype == null) {
                mimetype = "application/octet-stream";
            }
            response.setContentType(mimetype);
            response.setContentLength((int) file.length());
            String fileName = (new File(filepath)).getName();
            // sets HTTP header
            response.setHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
            byte[] byteBuffer = new byte[BUFSIZE];
            DataInputStream in = new DataInputStream(new FileInputStream(file));
            // reads the file's bytes and writes them to the response stream
            while ((in != null) && ((length = in.read(byteBuffer)) != -1)) {
                outStream.write(byteBuffer, 0, length);
            }
            in.close();
            outStream.close();
            file.delete();
        } catch (Exception ex) {
            // TODO handle custom exceptions here
            logger.error("Exception at RevenueReports ", ex);
            ex.printStackTrace();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
