<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.ISOMethodsList"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.ISOMethods"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ISOResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ISOResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.HttpMethodName"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.HttpMethods"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.HttpResorcemanagment"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.HttpresourceDetails"%>
<%@page import="org.w3c.dom.Node"%>
<%@page import="org.w3c.dom.Document"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.w3c.dom.NodeList"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="javax.xml.parsers.DocumentBuilderFactory"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.nio.charset.Charset"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.net.URLConnection"%>
<%@page import="java.security.KeyManagementException"%>
<%@page import="javax.net.ssl.TrustManager"%>
<%@page import="com.mollatech.PPadmin.commons.AllTrustManager"%>
<%@page import="java.security.NoSuchAlgorithmException"%>
<%@page import="javax.net.ssl.HttpsURLConnection"%>
<%@page import="com.mollatech.PPadmin.commons.AllVerifier"%>
<%@page import="javax.net.ssl.SSLContext"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@include file="header.jsp" %>
<%
    SSLContext sslContext = null;
    try {
        HttpsURLConnection.setDefaultHostnameVerifier(new AllVerifier());

        try {
            sslContext = SSLContext.getInstance("TLS");
        } catch (NoSuchAlgorithmException ex) {
            //Logger.getLogger(NewClass.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        sslContext.init(null, new TrustManager[]{new AllTrustManager()}, null);
        HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

    } catch (KeyManagementException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }

    int id = Integer.parseInt(request.getParameter("resId"));
    ResourceDetails details = new ResourceManagement().getResourceById(id);
    String data = "";
    String[] apiName = null;
    if (details.getType().equalsIgnoreCase("SOAP")) {
        data = callURL(details.getNewURL());
        if (data != null) {
            apiName = listOperationsUnique(data);
        }
    } else if (details.getType().equalsIgnoreCase("WSDLFile")) {
        data = new String(details.getWSDLFileData());
        apiName = listOperationsUnique(data);
    } else if (details.getType().equalsIgnoreCase("WADL")) {
        HttpresourceDetails hd = new HttpResorcemanagment().getHttpReourceId(SessionId, channelId, details.getResourceId());
        HashMap map = (HashMap) Serializer.deserialize(hd.getImplclass());
        List list = new ArrayList();
        for (Object key : map.keySet()) {
            String methodname = key.toString().split(":")[0];
            list.add(methodname);

        }
        apiName = new String[list.size()];
        int i = 0;
        for (Object Obj : list) {
            apiName[i] = Obj.toString();
            i++;
        }
    } else if (details.getType().equalsIgnoreCase("HTTP")) {
        HttpresourceDetails hd = new HttpResorcemanagment().getHttpReourceId(SessionId, channelId, details.getResourceId());
        HashMap map = (HashMap) Serializer.deserialize(hd.getImplclass());
        List list = new ArrayList();
        for (Object key : map.keySet()) {
            String strKey = (String) key;
            if (strKey.split(":")[2].equals("" + details.getResourceId())) {
                list.add(strKey.split(":")[0]);
            }
        }

        apiName = new String[list.size()];
        int i = 0;
        for (Object Obj : list) {
            apiName[i] = Obj.toString();
            i++;
        }
    } else if (details.getType().equalsIgnoreCase("iso")) {
        ISOResourceDetails hd = new ISOResourceManagement().geISOResourceByResId(SessionId, channelId, details.getResourceId());
        ISOMethods m = (ISOMethods) Serializer.deserialize(hd.getImplclass());
        List<ISOMethodsList> iSOMethodsList = m.iSOMethodsList;
        apiName = new String[iSOMethodsList.size()];
        int i = 0;
        for (ISOMethodsList methodName : iSOMethodsList) {
            apiName[i] = methodName.methodname;
        }
    }
    session.setAttribute("apiDetails", apiName);
%>
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header text-uppercase">Resource API Management</h3>
            </div>
            <!-- /.col-lg-10 -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47;  <i class="fa fa-table"></i><b> List of Resources</b>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal " method="post" id="saveAPIPriceDetails" name="saveAPIPriceDetails">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover display nowrap"  width="100%">
                                    <thead>
                                        <tr>
                                            <td/>
                                            <th>No.</th>
                                            <th>Name</th>
                                            <th>Price</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%int count = 0;
                                            if (apiName != null) {
                                                for (String nameS : apiName) {
                                                    count++;
                                        %>
                                        <tr>
                                            <td/>
                                            <td><%=count%></td>
                                            <td><%=nameS%></td>
                                            <td><input type="text" id="_API<%=nameS%>" name="_API<%=nameS%>" placeholder="Enter Price" value="0"></td>
                                        </tr>
                                        <%}
                                            }%>
                                    </tbody>
                                </table>
                            </div>
                            <%if (count != 0) {%>
                            <a href="#" class="btn btn-info btn-sm" onclick="saveAPIPrice(<%=id%>)" ><i class="fa fa-eye"></i> Save </a>
                            <%}%>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%!
    public String callURL(String myURL) {
        System.out.println("Requeted URL:" + myURL);
        StringBuilder sb = new StringBuilder();
        URLConnection urlConn = null;
        InputStreamReader in = null;
        try {
            URL url = new URL(myURL);
            urlConn = url.openConnection();
            if (urlConn != null) {
                urlConn.setReadTimeout(60 * 1000);
            }
            if (urlConn != null && urlConn.getInputStream() != null) {
                in = new InputStreamReader(urlConn.getInputStream(),
                        Charset.defaultCharset());
                BufferedReader bufferedReader = new BufferedReader(in);
                if (bufferedReader != null) {
                    int cp;
                    while ((cp = bufferedReader.read()) != -1) {
                        sb.append((char) cp);
                    }
                    bufferedReader.close();
                }
            }
            in.close();
        } catch (Exception e) {
            return null;
        }

        return sb.toString();
    }

    public String[] listOperations(String filename) throws Exception {
        Document d = DocumentBuilderFactory.newInstance().newDocumentBuilder()
                .parse(new ByteArrayInputStream(filename.getBytes()));
        NodeList nList = d.getElementsByTagName("*");
        Node n = null;
        String operationName = "";
        for (int i = 0; i < nList.getLength(); i++) {
            n = nList.item(i);
            if (n.getNodeName().toLowerCase().endsWith("operation")) {
                System.out.println("\nCurrent Element :" + n.getNodeName());
                operationName = n.getNodeName();
                break;
            }
        }
        System.out.println(operationName);
        NodeList elements = d.getElementsByTagName(operationName);
        ArrayList<String> operations = new ArrayList<String>();
        for (int i = 0; i < elements.getLength(); i++) {
            String tapiName = elements.item(i).getAttributes().getNamedItem("name").getNodeValue();
            String apiName = tapiName;
            if (tapiName.contains("_")) {
                apiName = "";
                for (int j = 0; j < tapiName.split("_").length; j++) {
                    if (j > 0) {
                        apiName += tapiName.split("_")[j].substring(0, 1).toUpperCase() + tapiName.split("_")[j].substring(1);
                    } else {
                        apiName += tapiName.split("_")[j];
                    }
                }
            }
            operations.add(apiName);
        }
        return operations.toArray(new String[operations.size()]);
    }

    public String[] listOperationsUnique(String filename) throws Exception {
        String[] nonUnique = listOperations(filename);
        HashSet<String> unique = new HashSet<String>(Arrays.asList(nonUnique));
        return unique.toArray(new String[unique.size()]);
    }


%>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="./js/resourcePrice.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="dist/js/sb-admin-2.js"></script>
<%@include file="footer.jsp" %>