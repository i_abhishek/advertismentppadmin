/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.cdr;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.SgCdrData;
import com.mollatech.serviceguard.nucleus.db.connector.management.CDRManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ashu
 */
@WebServlet(name = "ChangeCDRStatus", urlPatterns = {"/ChangeCDRStatus"})
public class ChangeCDRStatus extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        JSONObject json = new JSONObject();

        try {
            int id = Integer.parseInt(request.getParameter("id"));
            int status = Integer.parseInt(request.getParameter("status"));
            CDRManagement management = new CDRManagement();
            SgCdrData data = management.getDetailsById(id);
            data.setStatus(status);
            data.setUpdatedOn(new Date());
            int result = management.upateDetails(data);
            if (result == 0) {
                json.put("result", "success");
                if (status != GlobalStatus.DELETED) {
                    json.put("message", "Status Changed Successfuly.");
                } else {
                    json.put("message", "CDR Removed Successfuly.");
                }
            } else {
                if (status != GlobalStatus.DELETED) {
                    json.put("message", "Error In Chnaging The Status.");
                } else {
                    json.put("message", "Error In Removing CDR.");
                }
                json.put("result", "error");
            }

        } catch (Exception ex) {
            json.put("message", ex.getMessage());
            json.put("result", "error");
        } finally {
            PrintWriter out = response.getWriter();
            out.print(json);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
