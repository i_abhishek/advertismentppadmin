/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.virtualNumber.caas;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.SgCassNumbers;
import com.mollatech.serviceguard.nucleus.db.SgNumbers;
import com.mollatech.serviceguard.nucleus.db.connector.management.CaaSManagement;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;

/**
 *
 * @author Ashu
 */
@WebServlet(name = "AddCaasNumbers", urlPatterns = {"/AddCaasNumbers"})
public class AddCaasNumbers extends HttpServlet {

    static final Logger logger = Logger.getLogger(AddCaasNumbers.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        logger.info("Request servlet is #registerPartner from #PPortal at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String result = "success";
        String message = "Record inserted successfully";
        String partchannelID = (String) request.getSession().getAttribute("_channelId");
        JSONObject json = new JSONObject();
        String zippath = (String) request.getSession().getAttribute("_ZipPath");
        logger.debug("value of zippath : " + zippath);
        Path path = Paths.get(zippath);
//        BufferedReader br = null;
//        List<String> list = new ArrayList<String>();
        SgNumbers numbers = null;
        int counter = 0;
        int duplicateNo = 0;
        int notNumber = 0;
        String rex = "^\\+?\\d+$";
        try {
            FileInputStream excelFile = new FileInputStream(new File(zippath));
            Workbook workbook = null;
            if (zippath.endsWith("xlsx")) {
                workbook = new XSSFWorkbook(excelFile);
            } else if (zippath.endsWith("xls")) {
                workbook = new HSSFWorkbook(excelFile);
            }
            Sheet datatypeSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = datatypeSheet.iterator();
            String numberS = "";
            int rowCount = 0;
            int res = 0;
            List<CaaS> list = new LinkedList();
            while (iterator.hasNext()) {
                if (rowCount != 0) {

                    Row currentRow = iterator.next();
                    Iterator<Cell> cellIterator = currentRow.iterator();
                    int colCount = 0;
                    CaaS caaS = new CaaS();
                    while (cellIterator.hasNext()) {

                        Cell currentCell = cellIterator.next();
                        if (currentCell.getCellTypeEnum() == CellType.STRING) {
//                    System.out.print(currentCell.getStringCellValue() + "--");
                            switch (colCount) {
                                case 0:
                                    caaS.API = currentCell.getStringCellValue();
                                    break;
                                case 1:
                                    caaS.Number = currentCell.getStringCellValue();
                                    break;
                                case 2:
                                    caaS.ServiceType = currentCell.getStringCellValue();
                                    break;
                                case 3:
                                    caaS.Password = currentCell.getStringCellValue();
                                    break;
                                case 4:
                                    caaS.AppKey = currentCell.getStringCellValue();
                                    break;
                                case 5:
                                    caaS.AppSecret = currentCell.getStringCellValue();
                                    break;
                                case 6:
                                    caaS.envt = currentCell.getStringCellValue();
                                    break;
                            }
                            colCount++;
                        }
                    }
                    list.add(caaS);
                } else {
                    rowCount++;
                    iterator.next();
                }
            }

            SgCassNumbers mobileNumber = null;
            try {
                int i = 0;
                if (sessionId != null && !list.isEmpty()) {
                    for (CaaS no : list) {
                        if (!(no.Number.matches(rex))) {
                            notNumber++;
                        }
                    }
                    if (notNumber != 0) {
                        result = "error";
                        message = " File have invalid numbers !";
                    } else {
                        SgCassNumbers[] check = new CaaSManagement().getAllNumbers();
                        String oldNumbers = ",";
                        if (check != null) {
                            for (int k = 0; k < check.length; k++) {
                                oldNumbers += check[k].getNumber() + ",";
                            }
                        }
                        for (CaaS no : list) {

                            if (!oldNumbers.contains(no.Number.toString())) {
                                mobileNumber = new SgCassNumbers();
                                oldNumbers += (no.Number) + ",";
                                mobileNumber.setNumber(no.Number);
                                mobileNumber.setApi(no.API);
                                mobileNumber.setAppKey(no.AppKey);
                                mobileNumber.setAppSecret(no.AppSecret);
                                mobileNumber.setServiceType(no.ServiceType);
                                mobileNumber.setPassword(no.Password);
                                mobileNumber.setEnvt(no.envt);
                                mobileNumber.setCreatedOn(new Date());
                                mobileNumber.setUpdatedOn(new Date());
                                mobileNumber.setChannelId((String) request.getSession().getAttribute("_channelId"));
                                mobileNumber.setStatus(GlobalStatus.ACTIVE);
                                int resCount = new CaaSManagement().addDetails(mobileNumber);
                                if (resCount == 0) {
                                    res++;
                                }
                            } else {
                                duplicateNo++;
                            }
                        }
                        System.out.println(res + "===" + duplicateNo + "<======");
                        if (res != 0 && duplicateNo != 0) {
                            result = "success";
                            message = +res + " Record added successfully and " + duplicateNo + " ignored";
                        } else if (res != 0) {
                            result = "success";
                            message = +res + " Record added successfully !";
                        } else {
                            result = "error";
                            message = " Record already exist !";
                        }
                    }

                }

            } catch (Exception e) {
                System.out.println(e);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
