
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement"%>
<%@page import="javax.xml.datatype.XMLGregorianCalendar"%>
<%@page import="javax.xml.datatype.DatatypeFactory"%>
<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPaymentdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PaymentManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.MonitorSettingsManagement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin Portal</title>
    </head>
    <body>
        <%
            List<SgPaymentdetails> paydetails = new ArrayList<SgPaymentdetails>();
            List<SgSubscriptionDetails> bucket = new ArrayList<SgSubscriptionDetails>();

            PackageSubscriptionManagement pac = new PackageSubscriptionManagement();
            PartnerManagement partnerManagement = new PartnerManagement();
            PartnerDetails[] partnerDetails = null;

            PaymentManagement ppw = new PaymentManagement();

            String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
            String channelId = (String) request.getSession().getAttribute("_channelId");
            String startdate = request.getParameter("_startdate");
            String enddate = request.getParameter("_enddate");
            String invoiceno = request.getParameter("_invoiceno");
            String partnerName = request.getParameter("_partnername");
            String manualPaymentFunctionality = (String) LoadSettings.g_sSettings.getProperty("functionalityOf.manualPayment.booleanValue");
            // when search by partner name.
            if (partnerName != "") {
                partnerDetails = partnerManagement.partnerDetailsbyPartnerName(sessionId, channelId, partnerName);
                if (partnerDetails != null) {
                    for (int i = 0; i < partnerDetails.length; i++) {
                        paydetails = ppw.getPaymentDetailById(sessionId, channelId, partnerDetails[i].getPartnerId());                        
//                        SgSubscriptionDetails[] subscriptionObj = pac.listOfPackageSubscripedbyPartnerId(partnerDetails[i].getPartnerId());
//                        if(subscriptionObj != null){
//                            for(int k=0; k < subscriptionObj.length; k++){
//                                bucket.add(subscriptionObj[k]);
//                            }
//                        }
                    }
                }
            }
            // when seach by invoice id
            if (invoiceno != "" && !invoiceno.isEmpty()) {
                paydetails = ppw.getPaymentDetailByInvoiceId(sessionId, channelId, invoiceno);
           }
            // when search by date
            if (startdate != "" || enddate != "") {
                bucket = pac.getSubscriptionDetailByDate(sessionId, channelId, startdate, enddate);
            }
        %>
        <div class="container-fluid">           
            <div class="row-fluid">
                <div id="licenses_data_table">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Developer Name</th>
                                    <th>Package Name</th>
                                    <th>Payment Mode</th>
                                    <th>Invoice No.</th>
                                    <th>Paid Amount</th>
                                    <%if(manualPaymentFunctionality.equalsIgnoreCase("true")){%>
                                    <th>Edit Payment details</th> 
                                    <%}%>
                                    <th>Paid On</th>
                                    <th>Subscribe On</th>
                                    <th>Subscription End</th>
                                    <th>View Invoice</th>
                                </tr>
                            </thead>
                            <%
                                String pName = "NA";
                                String packageName= "NA";
                                String paidDate = "NA";
                                String payMode = "NA";
                                String invoiceNo = "NA";
                                String subscribeDate = "NA";
                                String subscriptionEndDate = "NA";
                                Float paidAmount = 0.0f;
                                int subscriptionId = 0;
                                int partnerId = 0;
                                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                                PartnerDetails partnerObject = null;
                                int i = 1;
                                if (paydetails != null && !paydetails.isEmpty()) {
                                    SgSubscriptionDetails subDetails = new SgSubscriptionDetails();                                   
                                    for (SgPaymentdetails paydata : paydetails) {
                                        partnerObject = partnerManagement.getPartnerDetails(sessionId, channelId, paydata.getPartnerId());                                       
                                        if (partnerObject != null) {
                                            pName = partnerObject.getPartnerName();
                                        }
                                        subDetails = pac.getSubscriptionbyId(sessionId, channelId, paydata.getSubscriptionId());
                                        packageName = subDetails.getBucketName();
                                        subscriptionId = subDetails.getBucketId();
                                        payMode = subDetails.getPaymentMode();
                                        paidDate = sdf.format(paydata.getPaidOn());
                                        subscribeDate = sdf.format(subDetails.getCreationDate());
                                        subscriptionEndDate = sdf.format(subDetails.getExpiryDateNTime());
                                        invoiceNo = paydata.getInvoiceNo();
                                        paidAmount = paydata.getPaidamount();
                                        partnerId = subDetails.getPartnerId();
                            %>
                            <tr>
                                <td><%=i++%></td>
                                <td><%= pName%></td>
                                <td><%= packageName%></td>
                                <td><%=payMode%></td>
                                <td><%= invoiceNo%></td>
                                <td><%= paidAmount%></td>
                                <% if(manualPaymentFunctionality.equalsIgnoreCase("true")){%>
                                <td><a href="#" onclick="editPaymentDetails('<%=subDetails.getBucketId()%>')"> Edit</a></td>
                                <%}%>
                                <td><%= paidDate%></td>
                                <td><%= subscribeDate%></td>
                                <td><%= subscriptionEndDate%></td>
                                <td>
                                    <font style="font-size: 11px"><a target="blank" href="getInvoice.jsp?_subscriptionId=<%=subscriptionId%>&_partnerid=<%=partnerId%>" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> View </a> </font>
                                </td>
                            </tr>
                            <%
                                }
                            } else if (bucket != null && !bucket.isEmpty()) {                                
                                SgPaymentdetails paymentdetails = null;
                                for (SgSubscriptionDetails obj : bucket) {
                                    paymentdetails = ppw.getPaymentDetailsbyPartnerAndSubscriptionID(obj.getBucketId(), obj.getPartnerId());
                                    partnerObject = partnerManagement.getPartnerDetails(sessionId, channelId, obj.getPartnerId());
                                    if(partnerObject != null){                                    
                                        pName = partnerObject.getPartnerName();
                                    }
                                    packageName = obj.getBucketName();
                                    subscriptionId = obj.getBucketId();
                                    subscribeDate = sdf.format(obj.getCreationDate());
                                    subscriptionEndDate = sdf.format(obj.getExpiryDateNTime());
                                    payMode = obj.getPaymentMode();
                                    if (paymentdetails != null) {
                                        paidDate = sdf.format(paymentdetails.getPaidOn());
                                        invoiceNo = paymentdetails.getInvoiceNo();
                                        paidAmount = paymentdetails.getPaidamount();
                                    } 
                                    partnerId = obj.getPartnerId();

                            %>
                            <tr>
                                <td><%=i++%></td>
                                <td><%= pName%></td>
                                <td><%= packageName%></td>
                                <td><%=payMode%></td>
                                <td><%= invoiceNo%></td>
                                <td><%= paidAmount%></td>
                                <% if(manualPaymentFunctionality.equalsIgnoreCase("true")){%>
                                <td><a href="#" class="btn btn-info btn-xs" onclick="editPaymentModel('<%=obj.getBucketId()%>')"><i class="glyphicon glyphicon-edit"></i> Edit</a></td>
                                <%}%>
                                <td><%= paidDate%></td>
                                <td><%= subscribeDate%></td>
                                <td><%= subscriptionEndDate%></td>
                                <% if (paymentdetails != null && paymentdetails.getPaidamount() != null) {%>
                                <td>
                                    <font style="font-size: 11px"><a target="blank" href="getInvoice.jsp?_subscriptionId=<%=subscriptionId%>&_partnerid=<%=partnerId%>" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> View </a> </font>
                                </td>
                                <%} else {%>
                                <td>
                                    <font style="font-size: 11px"><a  target="blank" href="developerTMInvoice.jsp?_subscriptionId=<%=subscriptionId%>&_partnerid=<%=partnerId%>" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> Unpaid </a> </font>
                                </td>
                                <%}%>
                            </tr>
                            <%}
                            } else {%>
                            <tr>
                                <td>1</td>
                                <td>No Record Found</td>
                                <td>No Record Found</td>
                                <td>No Record Found</td>
                                <td>No Record Found</td>
                                <td>No Record Found</td>
                                <%if(manualPaymentFunctionality.equalsIgnoreCase("true")){%>
                                <td>No Record Found</td>
                                <%}%>
                                <td>No Record Found</td>
                                <td>No Record Found</td>
                                <td>No Record Found</td>
                                <td>No Record Found</td>
                            </tr>
                            <% 
                                }
                            %>
                        </table>
                    </div>
                </div>
            </div>  
        </div>                                     
    </body>
</html>
