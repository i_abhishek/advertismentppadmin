
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TaxCalculationManagement"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.util.List"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PaymentManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPaymentdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPartnerrequest"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerRequestManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="invoice_css/style.css" rel="stylesheet" type="text/css"/>
        <link href="invoice_css/invoiceDetail.css" rel="stylesheet" type="text/css"/>
        <script src="invoice_css/invoiceDetail.js" type="text/javascript"></script>
        <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <title>Admin Portal</title>
    </head>
    <%
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
            if (SessionId == null) {
                response.sendRedirect("oplogout.jsp");
                return;
            }
        Date todayDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String toDate = sdf.format(todayDate);
        String packageName = "NA";
        String invoiceid = "0";
        int partnID = Integer.parseInt(request.getParameter("_partnerid"));

        
        String ChannelId = (String) request.getSession().getAttribute("_channelId");
        SgPartnerrequest reqPar = new PartnerRequestManagement().getPartnerRequestsPartnerbyId(SessionId, partnID);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        String invoiceDate = dateFormat.format(new Date());
        //int partner_id = Integer.parseInt(request.getParameter("_partnerid"));
        String subscriptionId = request.getParameter("_subscriptionId");
        Integer subId = Integer.parseInt(subscriptionId);

        SgSubscriptionDetails subscriObj = new PackageSubscriptionManagement().getSubscriptionbyId(SessionId, ChannelId, subId);

        double totalPaid = 0;
        SgPaymentdetails payObj = null;
        Map<String, Float> amount = new LinkedHashMap<String, Float>();
        Map<String, String> taxMap = new LinkedHashMap<String, String>();
        if (subscriObj != null) {
            payObj = new PaymentManagement().getPaymentDetailsbyPartnerAndSubscriptionID(subscriObj.getBucketId(), partnID);

            float planAmount = subscriObj.getPlanAmount();
            amount.put("Package Amount", planAmount);
            totalPaid += planAmount;
            packageName = subscriObj.getBucketName();

            float serviceCharge = Float.parseFloat(subscriObj.getServiceCharge());
            amount.put("Service Charge", serviceCharge);
            totalPaid += serviceCharge;
            if (payObj != null) {
                Date paymentDate = payObj.getPaidOn();
                invoiceDate = dateFormat.format(paymentDate);
                invoiceid = payObj.getInvoiceNo();
                if (payObj.getChangeOnPackageCharge() != null) {
                    amount.put("Change Package Charge", payObj.getChangeOnPackageCharge());
                    totalPaid += payObj.getChangeOnPackageCharge();
                }
                if (payObj.getReactivationPackageCharge() != null) {
                    amount.put("Reactivation Charge", payObj.getReactivationPackageCharge());
                    totalPaid += payObj.getReactivationPackageCharge();
                }
                if (payObj.getCancellationCharge() != null) {
                    amount.put("Cancellation Charge", payObj.getCancellationCharge());
                    totalPaid += payObj.getCancellationCharge();
                }
                if (payObj.getLatePenaltyCharge() != null) {
                    amount.put("Late Penalty", payObj.getLatePenaltyCharge());
                    totalPaid += payObj.getLatePenaltyCharge();
                }
            }

            String taxRate = subscriObj.getTax();

            taxMap = TaxCalculationManagement.calculateTax(taxRate, totalPaid);

            String calculatedGSTDetail = (String) taxMap.get("GST Tax");
            String calculatedVATDetail = (String) taxMap.get("VAT Tax");
            String calculatedSTDetail = (String) taxMap.get("Service Tax");

            String[] calculatedGST = calculatedGSTDetail.split(":");
            String[] calculatedVAT = calculatedVATDetail.split(":");
            String[] calculatedST = calculatedSTDetail.split(":");

            totalPaid += Double.parseDouble(calculatedGST[1]);
            totalPaid += Double.parseDouble(calculatedVAT[1]);
            totalPaid += Double.parseDouble(calculatedST[1]);

        }
    %>   
    <body>
        <div id="menu-container">            
            <a href="#" class="btn btn-primary" onclick="print_specific_div_content()"><i class="fa fa-print"></i> Print</a>
        </div>

        <div id="invoice-container">
            <div id="header">
                <table>
                    <tbody><tr>
                            <td id="company-name">
                                <%
                                    String imageURL = LoadSettings.g_strPath + "LOGO.png";
                                    String path = URLDecoder.decode(imageURL, "UTF-8");
                                    String companyName = LoadSettings.g_sSettings.getProperty("company.name");
                                    String addressline1 = LoadSettings.g_sSettings.getProperty("address.line1");
                                    String addressline2 = LoadSettings.g_sSettings.getProperty("address.line2");
                                    String addressline3 = LoadSettings.g_sSettings.getProperty("address.line3");
                                    String companyPhone = LoadSettings.g_sSettings.getProperty("company.phone");
                                %>
                                <img src="img/imgpsh_fullsize.jpg" alt="" width="25%" height="25%"/>                            <h2 style="color:#3b4966;"><%=companyName%></h2>

                                <p><%=addressline1%><br>                                <%=addressline2%><br>                                <%=addressline3%>                                                                  <br>                                                                <abbr>F:</abbr><%=companyPhone%>                            </p>
                            </td>
                            <td class="alignr"><h2 style="color:#3b4966;">Invoice Number : <%=invoiceid%></h2></td>
                        </tr>
                    </tbody></table>
            </div>

            <div style="height:20px"></div>

            <div id="invoice-to">

                <table style="width: 100%;">
                    <tbody><tr>
                            <td>
                                <h2><%=reqPar.getName()%></h2>
                                <p><%=reqPar.getAddress()%><br><abbr>Ph:</abbr><%=reqPar.getPhone()%><br>                            </p>
                            </td>
                            <td style="width:40%;"></td>
                            <td>
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>Paid On : </td>
                                            <td><%=invoiceDate%></td>
                                        </tr>                                    
                                        <tr>
                                            <td>Package Name : </td>
                                            <td> <%=packageName%></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody></table>
            </div>

            <div id="invoice-items">
                <table class="table table-striped  table-hover">
                    <thead>
                        <tr>
                            <th>Qty</th>
                            <th>Item</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Total</th>
                        </tr>
                    </thead>

                    <%
                        int count = 1;  double roundedTotalPaid = 0.00;
                        NumberFormat form = new DecimalFormat("#0.00");
                        double totalPayment = 0.00; double benefitAmount = 0.00;
                        for (Map.Entry<String, Float> entry : amount.entrySet()) {
                    %> 
                    <tr>
                        <td><%=count%></td>
                        <td><%=entry.getKey()%></td>
                        <td></td>
                        <td><%=form.format(entry.getValue())%></td>
                        <td><%=form.format(entry.getValue())%></td>
                    </tr>
                    <%
                            count++;
                            totalPayment = totalPayment + entry.getValue();
                        }
                        if (!taxMap.isEmpty()) {
                            for (Map.Entry<String, String> entry : taxMap.entrySet()) {
                                String[] taxArr = entry.getValue().split(":");
                    %>
                    <tr>
                        <td><%=count%></td>
                        <td><%=entry.getKey()%></td>
                        <td><%=taxArr[0]%> %</td>
                        <td><%=form.format(Double.parseDouble(taxArr[1]))%></td>
                        <td><%=form.format(Double.parseDouble(taxArr[1]))%></td>
                    </tr>
                    <%
                                count++;
                                totalPayment = totalPayment + Float.parseFloat(taxArr[1]);
                            }
                        }
                        roundedTotalPaid = (double) Math.round(totalPayment * 100) / 100;
                    %>
<!--                    <tr>
                        <td class="no-bottom-border" colspan="3"></td>                
                        <td>Total</td>
                        <td><strong><%=form.format(totalPayment)%></strong></td>
                    </tr>-->
                    <%

                        if (payObj != null) {
                            String promocodeDetails = payObj.getPromoCodeDetails();
                            if (promocodeDetails != null && !promocodeDetails.isEmpty()) {
                                String promoItem = "Promo code benefit";                                
                                String[] promocodeArray = promocodeDetails.split(",");
                                if(promocodeArray[1].equalsIgnoreCase("In percentage")){
                                    benefitAmount = (totalPayment * Double.parseDouble(promocodeArray[0].trim())) / 100;
                                    totalPayment  = totalPayment - benefitAmount;
                                }else{
                                    totalPayment = totalPayment - Double.parseDouble(promocodeArray[0]);
                                }
                                double roundedBenefitAmount = (double) Math.round(benefitAmount * 100) / 100;
                                roundedTotalPaid = (double) Math.round(totalPayment * 100) / 100;
                    %>
                    <tr>
                        <td><%=count%></td>
                        <td><%=promoItem%></td>
                        <td><%=promocodeDetails%></td>
                        <td><%=roundedBenefitAmount%></td>
                        <td><%= - roundedBenefitAmount%></td>
                    </tr>                    
                    <%
                            }
                        }
                    %>
                    <tr>
                        <td class="no-bottom-border" colspan="3"></td>                
                        <td>Total</td>
                        <td><strong><%=roundedTotalPaid%></strong></td>
                    </tr>
                </table>
                <div class="seperator"></div>

                <table class="table table-striped" style="width: 100%; font-size:9px; color:#3b4966;">
                    <thead>
                        <tr>
                            <th><h4>Terms &amp; Condition</h4></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>- All prices exclusive of sales/service tax which is to be paid by the client.<br>
                                - Molla Technologies shall reserves the right to review the pricing in this proposal once the job has commenced should there be a drastic change in the specification of the job.<br>
                                - 50% upon job confirmation, 50% upon delivery or project completion. 100% upon job confirmation for Maintenance, Domain &amp; Hosting Services.<br>
                                - The being designed or developed system belongs to Molla Technologies and the ownership will be transferred to the purchaser on Full Payment.<br>
                                - Order Cancellation: 30% cancellation charge (Based on total proposal value).<br>
                                - Approx. 3 weeks from the receive of content for the job. Molla Technologies reserves the right to make further adjustments to the<br>
                                duration from time-to-time, if new requirement carry out.<br>
                                - 30 days from the date mentioned above or based on Expiration Date.<br>
                                - The price quoted inclusive of 6 months maintenance for email, phone and remote support. For outstation support, Flipnix Solutions has the right to charges additional service charges based on the said location.<br>
                                <br>
                                Northern &amp; Southern Region : RM350.00 per day<br>
                                Klang Valley Areas : RM150.00 per trip<br>
                                Working Hour : Monday to Friday, 9.30am – 5.00pm (Within 24 hours response time)<br>
                                Print On : <%=toDate%><br>
                                <br>
                                <!--                                Payment Made To:<br>
                                                                BANK NAME : HONG LEONG BANK<br>
                                                                BENEFICIARY NAME : FLIPNIX SOLUTIONS<br>
                                                                BENEFICIARY ACCOUNT : 05600317978</td>-->
                        </tr>
                    </tbody>
                </table>
                <table class="table table-striped" style="width: 100%; font-size:8px; color:#3b4966;">
                    <tbody>
                        <tr>
                            <td>This is computer generated invoice, no signature required.</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <hr>
            <div style="font-size:10px; padding-left:10px">Invoice generated by <a href="#" target="_blank">Molla Technologies</a></div>
        </div>
    </body>
</html>
