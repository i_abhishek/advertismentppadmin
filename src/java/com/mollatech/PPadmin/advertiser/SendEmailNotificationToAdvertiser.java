/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.advertiser;

import com.mollatech.serviceguard.connector.communication.SGStatus;
import com.mollatech.serviceguard.nucleus.settings.SendNotification;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author abhishekingle
 */
public class SendEmailNotificationToAdvertiser implements Runnable {
    
    String email;
    String tmessage;
    String channels;
    String sessionId;
    public static final int PENDING = 2;
    public static final int SEND = 0;
    String subject;
    String emailFor;

    public SendEmailNotificationToAdvertiser() {

    }

    public SendEmailNotificationToAdvertiser(String message, String channel, String sessionId, String email, String subject, String emailFor) {
        this.email = email;
        this.tmessage = message;
        this.channels = channel;
        this.sessionId = sessionId;
        this.subject = subject;
        this.emailFor = emailFor;
    }
    String mimeType[] = {"application/octet-stream"};
    int productType = 3;

    public void run() {
        SGStatus status = new SendNotification().SendEmail(channels, email, subject, tmessage, null, null, null, mimeType, productType);
        if (status.iStatus == PENDING || status.iStatus == SEND) {
            System.out.println(emailFor + " Email sent successfully with retCode " + status.iStatus + " to" + email + " at " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
        } else {
            System.out.println(emailFor + " Email failed to sent with retCode " + status.iStatus + " to " + email + " at " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
        }        
    }
}
