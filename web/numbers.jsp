<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="javax.sound.midi.SysexMessage"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgNumbers"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.NumbersManagement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
    String channelid = (String) request.getSession().getAttribute("_channelId");

    String partId = request.getParameter("partnerId");
    String type = request.getParameter("type");
    SgNumbers[] numberDetails = null;

    String assignedNumbers = ",";
    PartnerDetails partDetails = new PartnerManagement().getPartnerDetails(sessionId, channelid, Integer.parseInt(partId));
    if (partDetails == null) {
        partDetails = new PartnerDetails();
    }
    if (type.equalsIgnoreCase("Live")) {

        SgNumbers[] num = new NumbersManagement().getAllNumbers("Live");
        numberDetails = num;
        if (num != null) {
            for (int i = 0; i < num.length; i++) {
                if (num[i].getPartnerId() == partDetails.getPartnerId()) {
                    assignedNumbers += num[i].getNumberId() + ",";
                }
            }
        }
    } else if (type.equalsIgnoreCase("Test")) {
        SgNumbers[] num = new NumbersManagement().getAllNumbers("Test");
        numberDetails = num;
        if (num != null) {
            for (int i = 0; i < num.length; i++) {
                if (num[i].getPartnerId() == partDetails.getPartnerId()) {
                    assignedNumbers += num[i].getNumberId() + ",";
                }
            }
        }
    }

%>
<div class="col-lg-8">
    <select id="NumberForPartners" name="NumberForPartners" multiple="multiple" style="width: 100%">       
        <%if (numberDetails != null) {
                for (SgNumbers details : numberDetails) {
                    if (assignedNumbers.contains("," + details.getNumberId() + ",")) {
        %>
        <option value="<%=details.getNumberId()%>" selected ><%=details.getMobileNo()%></option>
        <% 
        } else if (details.getPartnerId() == null && details.getStatus() == GlobalStatus.ACTIVE) {
        %>
        <option value="<%=details.getNumberId()%>" ><%=details.getMobileNo()%></option>
        <%}

                }
            }
        %>
    </select> 
</div>




