<%@page import="org.json.JSONArray"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartyManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPartydetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgResourceowner"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceOwnerManagement"%>
<%@page import="org.json.JSONObject"%>
<!--<script src="js/partyManager.js" type="text/javascript"></script>-->
<%
    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
    String channelId = (String) request.getSession().getAttribute("_channelId");
    int id = Integer.parseInt(request.getParameter("_requestId"));
    int resourceId = Integer.parseInt(request.getParameter("resourceId"));
    if (resourceId != -1) {
        SgResourceowner SgResourceowner = new ResourceOwnerManagement().getResourceownerbyId(SessionId, id);
        SgPartydetails[] sgPartylist = new PartyManagement().getAllPartyDetails(channelId);
        session.setAttribute("sgPartylist", sgPartylist);
        session.setAttribute("SgResourceowner", SgResourceowner);
%>
<br>
<h5 style="font-weight: bold">Revenue Sharing Details</h5>
<hr>
<div class="form-group">
    <label class="control-label col-lg-2">Owner Name</label>
    <div class="col-lg-3">
        <input type="text" class="form-control" placeholder="Owner name" value="<%=SgResourceowner.getResourceOwnerName()%>" readonly>
    </div>
    <label class="control-label col-lg-2">Revenue</label>
    <div class="col-lg-3">
        <div class="input-group">
            <span class="input-group-addon">%</span>
            <input type="text" id="_ownerRev" name="_ownerRev" class="form-control" placeholder="Revenue in %">                                    
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-lg-2">
        <select id="_selectFirstParties" name="_selectFirstParties"  class="form-control" onchange="partyCheck(this.value, '<%=resourceId%>')">
            <%  int firstParty = 0;
                JSONObject jsonObj = new JSONObject();
                JSONObject jsonexists = null;
                if (SgResourceowner.getData() != null) {
                    JSONArray revenueDetailsArray = new JSONArray(SgResourceowner.getData());
                    for (int i = 0; i < revenueDetailsArray.length(); i++) {
                        jsonexists = revenueDetailsArray.getJSONObject(i);
                        if (jsonexists.has(String.valueOf(resourceId))) {
                            jsonObj = jsonexists.getJSONObject(String.valueOf(resourceId));
                            break;
                        }
                    }
                    if (jsonObj.has("_firstParty")) {
                        SgPartydetails partyObj = new PartyManagement().getPartyById(channelId, Integer.parseInt(jsonObj.getString("_firstParty")));
                        if (partyObj != null) {
                            firstParty = partyObj.getPartyId();
            %>
            <option value="-1">Select Party</option>
            <option selected value="<%=partyObj.getPartyId()%>"><%=partyObj.getPartyName()%></option>
            <%
            } else {%>
            <option value="-1" selected>Select Party</option>            
            <%}
            } else {%>
            <option value="-1" selected>Select Party</option>    
            <%}
            } else {
            %>                                    
            <option value="-1" selected>Select Party</option>
            <%
                }
                if (sgPartylist != null) {
                    for (int i = 0; i < sgPartylist.length; i++) {
                        if (sgPartylist[i].getStatus() == GlobalStatus.ACTIVE) {
                            SgPartydetails apdetails = sgPartylist[i];
                            if (apdetails != null && apdetails.getPartyId() != firstParty) {%>
            <option value="<%=apdetails.getPartyId()%>"><%=apdetails.getPartyName()%></option>
            <%}

                        }
                    }
                }
            %>
        </select>
    </div>
    <div class="col-lg-3">
        <div class="input-group">
            <span class="input-group-addon">%</span>
            <input type="text" id="_aPartyRev" name="_aPartyRev" class="form-control" placeholder="Party Revenue">
        </div>
    </div>
    <div class="col-lg-2">
        <select id="_selectSecondParties" name="_selectSecondParties"  class="form-control span2" onchange="selectThirdParty('<%=resourceId%>')" style="width: 100%">
            <option value="-1" selected>Select Party</option>                                                                                  
        </select>   
    </div>
    <div class="col-lg-3">
        <div class="input-group">
            <span class="input-group-addon">%</span>
            <input type="text" id="_bPartyRev" name="_bPartyRev" class="form-control" placeholder="Party Revenue">                                    
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-lg-2">
        <select id="_selectThirdParties" name="_selectThirdParties"  class="form-control span2" onchange="selectFourthParty('<%=resourceId%>')" style="width: 100%">
            <option value="-1" selected>Select Party</option>                                                                                  
        </select>   
    </div>
    <div class="col-lg-3">
        <div class="input-group">
            <span class="input-group-addon">%</span>
            <input type="text" id="_cPartyRev" name="_cPartyRev" class="form-control" placeholder="Party Revenue">
        </div>
    </div>
    <div class="col-lg-2">
        <select id="_selectFourthParties" name="_selectFourthParties"  class="form-control span2" onchange="" style="width: 100%">
            <option value="-1" selected>Select Party</option>                                                                                  
        </select>   
    </div>    
    <div class="col-lg-3">
        <div class="input-group">
            <span class="input-group-addon">%</span>
            <input type="text" id="_dPartyRev" name="_dPartyRev" class="form-control" placeholder="Party Revenue">                                    
        </div>
    </div>
</div>
<%if (jsonObj.has("_firstParty")) {
%>
<script>
    document.getElementById('_dPartyRev').value = '<%=jsonObj.getString("_dPartyRev")%>';
    document.getElementById('_cPartyRev').value = '<%=jsonObj.getString("_cPartyRev")%>';
    document.getElementById('_bPartyRev').value = '<%=jsonObj.getString("_bPartyRev")%>';
    document.getElementById('_aPartyRev').value = '<%=jsonObj.getString("_aPartyRev")%>';
    document.getElementById('_ownerRev').value = '<%=jsonObj.getString("_ownerRev")%>';
</script>
<%}
%>
<a  class="btn btn-success btn-xs" onclick="editResOwnerInfo(<%=SgResourceowner.getResourceId()%>)"><i class="fa fa-plus-circle"></i> Save Details</a>
<!--<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="js/resourcePrice.js"></script>
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="dist/js/sb-admin-2.js"></script>
<script src="./select2/select2.js"></script>-->
<script>
    $(document).ready(function () {
        var _selectFirstParties = document.getElementById("_selectFirstParties").value;
        partyCheck(_selectFirstParties, '<%=resourceId%>');
//        selectThirdParty('<%=resourceId%>');
//        selectFourthParty('<%=resourceId%>');
    });
</script>
<%}%>

