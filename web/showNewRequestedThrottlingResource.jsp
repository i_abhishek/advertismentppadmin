<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgBucketdetails"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement"%>

<%@page import="com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MethodName"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Classes"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Methods"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <div class="control-group">
        <label class="control-label"  for="username"></label>
        <div class="controls">
            
                <%
                    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
                    String channelId = (String) request.getSession().getAttribute("_channelId");
                    String apName = request.getParameter("_apName");
                    String packageName = request.getParameter("_packageName");                    
                    SgBucketdetails reqObj = new PackageManagement().getPackageByName(SessionId, channelId, packageName);
                    String apDetails = reqObj.getApiThrottling();
                    JSONArray jsOld = new JSONArray(apDetails);
                    String res = "";
                    for (int j = 0; j < jsOld.length(); j++) {
                        JSONObject jsonexists1 = jsOld.getJSONObject(j);
                        Iterator<String> keys = jsonexists1.keys();
                        while (keys.hasNext()) {
                            String keyData = keys.next();
                            String[] keyDetails = keyData.split(":");
                            if (keyDetails[0].equals(apName)) {
                                res += keyDetails[1] + ",";
                            }
                        }
                    }
                    String resName = "";
                    if (!res.equals("")) {
                        String[] resourcesDetails = res.split(",");
                        if (resourcesDetails != null) {
                            for (int i = 0; i < resourcesDetails.length; i++) {
                                if (!resName.contains(resourcesDetails[i] + ",")) {
                                    resName += resourcesDetails[i] + ",";
                %>
                <select id="_throttlingAccesspoint" name="_throttlingAccesspoint"  class="form-control span2" onchange="throttlingVersion(this.value,'<%=packageName%>')" style="width: 100%">
                    <option value="<%=resourcesDetails[i]%>"><%=resourcesDetails[i]%></option>
                </select>
                <%}}
                    }
                } else {%>
                <select id="_throttlingAccesspoint" name="_throttlingAccesspoint"  class="form-control span2" onchange="throttlingVersion(this.value,'<%=packageName%>')" style="width: 100%">
                    <option value="-1">Select Resource</option>
                </select>
                <%}%>
            
        </div>
    </div>
