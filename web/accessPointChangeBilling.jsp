<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgBucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Warfiles"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.AccessPolicy"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MethodName"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Classes"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Methods"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%
        AccessPointManagement ppw = new AccessPointManagement();
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        String apId = request.getParameter("_apId");
        String versionData = request.getParameter("versionData");
        String _resourceId = request.getParameter("_resourceId");
        String packageId = request.getParameter("_bucketId");
        int pId = 0;
        if (packageId != null) {
            pId = Integer.parseInt(packageId);
        }
        SgBucketdetails pacObject = new PackageManagement().getPackageDetails(SessionId, channelId, pId);

        // int acpId = Integer.parseInt(apId);
        if (apId != null && !apId.equals("-1")) {
            Accesspoint accesspoint = ppw.getAccessPointByName(SessionId, channelId, apId);
            //int resId = Integer.parseInt(_resourceId);

            //new change
            String apName = accesspoint.getName();
            Map classMap = null;
            Map methodMap = null;
            String value = apName;

            byte[] accessPolicy = accesspoint.getLaccessPolicyEntry();
            AccessPolicy ap = (AccessPolicy) Serializer.deserialize(accessPolicy);
            float amountOfApi = 0.0f;
            int selectedAPI = GlobalStatus.Per_Entry_Per_API;
            if (ap != null) {
                amountOfApi = ap.Amount;
            }
            //new changee
            ResourceDetails rd = new ResourceManagement().getResourceByName(SessionId, channelId, _resourceId);
            //TransformDetails tf = new TransformManagement().getTransformDetails(SessionId, channelId, acpId, resourceID);
            TransformDetails tf = new TransformManagement().getTransformDetails(SessionId, channelId, accesspoint.getApId(), rd.getResourceId());
            Warfiles warfiles = new WarFileManagement().getWarFile(SessionId, channelId, accesspoint.getApId());
            int version = 1;
            if (warfiles != null) {
                Map map = (Map) Serializer.deserialize(warfiles.getWfile());
                version = map.size() / 2;
            }
            byte[] meBy = tf.getMethods();
            Object obj = Serializer.deserialize(meBy);
            Map tmpMethods = (Map) obj;
            Methods methods = (Methods) tmpMethods.get("" + versionData);

            if (methodMap == null) {
                methodMap = new HashMap();
            }

            methodMap.put(value, methods);
            String mName = "";
            List list = methods.methodClassName.methodNames;
            int count = 0;
            JSONObject reqJSONObj = null;
            String mainCredit = "0";
            String freeCredit = "0";
            //new change
            if (pacObject != null && pacObject.getApRateDetails() != null) {
                String apRateDetails = pacObject.getApRateDetails();
                String key = apName + ":" + _resourceId + ":" + versionData;

                //System.out.println("KEY "+ key);
                JSONArray jsOld = new JSONArray(apRateDetails);
                for (int j = 0; j < jsOld.length(); j++) {
                    JSONObject jsonexists1 = jsOld.getJSONObject(j);
                    if (jsonexists1.has(key)) {
                        reqJSONObj = jsonexists1.getJSONObject(key);
                        if (reqJSONObj != null) {
                            break;
                        }
                    }
                }

                if (reqJSONObj != null) {
                    mainCredit = reqJSONObj.getString("mainCredit");
                    freeCredit = reqJSONObj.getString("freeCredit");
                }
            }
    %>
    <!--    <div class="form-group">
            <label class="col-lg-2 control-label">Main credit</label>
            <div class="col-lg-3">
                <div class="input-group">
                    <span class="input-group-addon">$</span>-->
    <input type="hidden" class="form-control" id="mainCreditForAP" name="mainCreditForAP"  value="<%=mainCredit%>">                                    
    <!--            </div>
            </div>
            <label class="col-lg-2 control-label">Free credit</label>
            <div class="col-lg-3" style="margin-left: 8.5%">
                <div class="input-group">
                    <span class="input-group-addon">$</span>-->
    <input type="hidden" class="form-control" id="freeCreditForAP" name="freeCreditForAP"  value="<%=freeCredit%>">                                    
    <!--            </div>
            </div>
        </div>-->

    <%

        for (int i = 0; i < list.size(); i++) {
            selectedAPI = GlobalStatus.Per_API;
            MethodName methodName = (MethodName) list.get(i);
            if (methodName.visibility.equalsIgnoreCase("yes")) {
                count++;
                mName = methodName.methodname.split("::")[0];
                if (!methodName.transformedname.equals("")) {
                    mName = methodName.transformedname.split("::")[0];
                }
                String methodNameStr = mName;
                String methodN = null;

                if (reqJSONObj != null) {
                    if (reqJSONObj.isNull(methodNameStr)) {
                        amountOfApi = Float.parseFloat("0");
                    } else {
                        methodN = reqJSONObj.getString(methodNameStr);
                        if (methodN.split(":").length == 1) {
                            amountOfApi = Float.parseFloat(methodN);
                        } else {
                            amountOfApi = Float.parseFloat(methodN.split(":")[0]);
                            selectedAPI = Integer.parseInt(methodN.split(":")[1]);
                        }
                    }
                }

    %>

    <div class="form-group">
        <label class=" control-label col-lg-5"><%=methodNameStr%></label>
        <div class="col-lg-3" >
            <div class="input-group">
                <span class="input-group-addon">$</span>
                <input onkeypress="return isNumberKey(event)"  type="text" class="form-control" placeholder="apiPrice" id="apiPrice<%=count - 1%>" name="apiPrice<%=count - 1%>" value="<%=BigDecimal.valueOf(Double.parseDouble("" + amountOfApi))%>">                                    
                <input type="hidden" name="apiName<%=count - 1%>" id="apiName<%=count - 1%>" value="<%=methodNameStr%>">                                
            </div>
        </div>
        <div class="col-lg-4" >
            <div class="input-group">
                <select  class="form-control col-lg-4" name="chargingSrategy<%=count - 1%>" id="chargingSrategy<%=count - 1%>">
                    <option value="<%=GlobalStatus.Per_User_Per_Renewal%>">Per User Per Renewal</option>
                    <option value="<%=GlobalStatus.Per_API%>">Per API</option>
                    <option value="<%=GlobalStatus.Per_Entry_Per_API%>">Per Entry Per API</option>
                </select>

            </div>
        </div>
    </div>

    <%
        }
    %><script>
        document.getElementById("chargingSrategy<%=count - 1%>").value = "<%=selectedAPI%>";
    </script>
    <%
        }

    %>
    <input type="hidden" name="apNameRate" id="apNameRate" value="<%=apName%>">
    <input type="hidden" name="paramcountAPR" id="paramcountAPR" value="<%=count%>">
    <%} else {%>
    <p style="display: none"> NO Record Found</p>  
    <% }%>



