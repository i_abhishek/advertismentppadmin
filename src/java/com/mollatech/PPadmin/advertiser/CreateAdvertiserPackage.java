/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.advertiser;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.commons.UtilityFunctions;
import com.mollatech.serviceguard.nucleus.db.SgAdvPreApprovalPackagedetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvPreApprovalPackageManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

@WebServlet(name = "CreateAdvertiserPackage", urlPatterns = {"/CreateAdvertiserPackage"})
public class CreateAdvertiserPackage extends HttpServlet {
   
    static final Logger logger = Logger.getLogger(CreateAdvertiserPackage.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Request servlet is #CreateAdvertiserPackage from #PPAdmin at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Package have been created successfully";
        String channelId = (String) request.getSession().getAttribute("_channelId");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String packageName = request.getParameter("_packageName");
        String packagePrice = request.getParameter("_packagePrice");
        String packageDuration = request.getParameter("_packageDuration");
        String paymentMode = request.getParameter("_paymentMode");
        String packageDesc = request.getParameter("packageDesc");
        String _recurrienceBillingID = request.getParameter("_recurrienceBillingID");
        String _credit = request.getParameter("_credit");
        String _tax = request.getParameter("_tax");
        String PartnerVisibility[] = request.getParameterValues("visibleTo");
        if (PartnerVisibility != null) {
            logger.debug("Value of PartnerVisibility = " + Arrays.toString(PartnerVisibility));
        }
        logger.debug("Value of _packageName = " + packageName);
        logger.debug("Value of _packagePrice = " + packagePrice);
        logger.debug("Value of _packageDuration = " + packageDuration);
        logger.debug("Value of _paymentMode = " + paymentMode);
        logger.debug("Value of packageDesc = " + packageDesc);
        logger.debug("Value of _recurrienceBillingID = " + _recurrienceBillingID);
        logger.debug("Value of _credit = " + _credit);
        logger.debug("Value of _tax = " + _tax);
        if (UtilityFunctions.isValidPhoneNumber(packagePrice) == false) {
            result = "error";
            message = "Price should be in digit.";
            try {
                json.put("result", result);
                logger.debug("Response of #CreateAdvertiserPackage from #PPAdmin Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of #CreateAdvertiserPackage from #PPAdmin Servlet's Parameter  message is " + message);
            } catch (Exception ex) {
                logger.error("Exception at #CreateAdvertiserPackage from #PPAdmin ", ex);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        }
        SgAdvPreApprovalPackagedetails packageObject = new AdvPreApprovalPackageManagement().getPackageByName(SessionId, channelId, packageName);
        if (packageObject != null) {
            result = "error";
            message = "Package name already taken, Try with other name.";
            try {
                json.put("result", result);
                logger.debug("Response of #CreateAdvertiserPackage from #PPAdmin Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of #CreateAdvertiserPackage from #PPAdmin Servlet's Parameter  message is " + message);
            } catch (Exception ex) {
                logger.error("Exception at #CreateAdvertiserPackage from #PPAdmin ", ex);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        }
        String partnerId = "";
        if (PartnerVisibility != null) {
            for (int i = 0; i < PartnerVisibility.length; i++) {
                partnerId += PartnerVisibility[i] + ",";
            }
        }
        Float price = Float.parseFloat(packagePrice);
        Float credit = Float.parseFloat(_credit);
        SgAdvPreApprovalPackagedetails bucketObj = new SgAdvPreApprovalPackagedetails();
        bucketObj.setPackageName(packageName.trim());
        bucketObj.setPlanAmount(price);
        bucketObj.setPaymentMode(paymentMode);
        bucketObj.setPackageDuration(packageDuration);
        bucketObj.setStatus(GlobalStatus.SUSPEND);
        bucketObj.setChannelId(channelId);        
        bucketObj.setTabShowFlag(GlobalStatus.RATE);
        bucketObj.setCreationDate(new Date());        
        bucketObj.setPartnerVisibility(partnerId);
        bucketObj.setPackageDescription(packageDesc);
        bucketObj.setRecurrenceBillingPlanId(_recurrienceBillingID);
        bucketObj.setMainCredits(credit);
        if(_tax != null){
            bucketObj.setTax(_tax);
        }
        int retValue = new AdvPreApprovalPackageManagement().CreateAdvPreApprovalPackage(SessionId, channelId, bucketObj);
        try {
            if (retValue > 0) {
                json.put("result", result);
                json.put("message", message);
                json.put("retValue", retValue);
            } else {
                result = "error";
                message = "Package does not created.";
                json.put("result", result);
                logger.debug("Response of #CreateAdvertiserPackage from #PPAdmin Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of #CreateAdvertiserPackage from #PPAdmin Servlet's Parameter  message is " + message);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            logger.info("Response of #CreateAdvertiserPackage from #PPAdmin " + json.toString());
            logger.info("Response of #CreateAdvertiserPackage from #PPAdmin Servlet at " + new Date());
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
