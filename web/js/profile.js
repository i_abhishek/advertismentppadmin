/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function strCompare(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function Alert4Users(msg) {
    bootboxmodel("<h4>" + msg + "</h4>");
}

function updateProfile() {
    var val1 = document.getElementById('partnerComName').value;
    var val2 = document.getElementById('partnerLandlineNo').value;
    var val3 = document.getElementById('partnerFax').value;
    var val4 = document.getElementById('partnerWebSite').value;
    var val5 = document.getElementById('partnerIP').value;
    var val6 = document.getElementById('partnerAddress').value;
    var val7 = document.getElementById('partnerPincode').value;
    if(val1 == ""||val2 == ""||val3 ==""||val4 ==""||val5 =="" ||val6 ==""||val7 ==""){
        Alert4Users("Please enter all information");
        return;
    }
    var s = './addPartnerInfo';
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        data: $("#update-profile").serialize(),
        success: function(data) {
            if (strCompare(data._result, "error") === 0) {
                Alert4Users(data._message);
            }
            else if (strCompare(data._result, "success") === 0) {
                Alert4Users(data._message);
                setTimeout(function() {
                    window.location.href = "./partners.jsp";
                }, 3000);
            }
        }
    });
}
function cancelProfile() {
    window.setTimeout(function() {
        window.location.href = "profile.jsp";
    }, 3000);
}