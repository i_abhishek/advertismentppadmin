package com.mollatech.PPadmin.members;

import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgUsers;
import com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Bluebricks
 */
public class editTeamMember extends HttpServlet {

    static final int ACTIVE_STATUS = 1;

    static final int SUSPENDED_STATUS = 0;

    static final int PENDING_STATUS = -2;

    public final int PASSWORD = 1;

    public static final int USER_PASSWORD = 1;

    public static final int ADMIN = 0;

    public static final int OPERATOR = 1;

    public static final int REPORTER = 2;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Team Member added successfully";
        PrintWriter out = response.getWriter();
        String memName = request.getParameter("ememName");
        PartnerDetails pDetails = (PartnerDetails) request.getSession().getAttribute("_partnerDetails");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        String partnerId = pDetails.getPartnerName();
        String memEmail = request.getParameter("ememEmail");
        String memMobileNo = request.getParameter("ememMobileNo");
        String Operator = request.getParameter("ememOperator");
        int pStatus = PENDING_STATUS;
        String memStatus = request.getParameter("ememStatus");
        int imemStatus = 0;
        if (memStatus != null) {
            imemStatus = Integer.parseInt(memStatus);
        }
        int resp = -1;
        UsersManagement um = new UsersManagement();
        String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        SgUsers user = um.getSgUsers(sessionId, channelId, memName);
        SgUsers userObj = new SgUsers();
        if (user == null) {
            userObj.setUsername(memName);
            userObj.setEmail(memEmail);
            userObj.setPhone(memMobileNo);
            if (Operator.equalsIgnoreCase("OPERATOR")) {
                userObj.setType(OPERATOR);
            }
            if (Operator.equalsIgnoreCase("REPORTER")) {
                userObj.setType(REPORTER);
            }
            userObj.setPartnerid(pDetails.getPartnerId());
            resp = um.editSgUsers(sessionId, channelId, userObj);
            if (resp == 0) {
                message = "User Created Successfully.";
                json.put("_result", result);
                json.put("_message", message);
            }
        } else {
            message = "User Already Exists.";
            json.put("_result", result);
            json.put("_message", message);
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
