/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.party;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.commons.UtilityFunctions;
import com.mollatech.serviceguard.nucleus.db.SgPartydetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartyManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "CreateParty", urlPatterns = {"/CreateParty"})
public class CreateParty extends HttpServlet {

    static final Logger logger = Logger.getLogger(CreateParty.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        logger.info("Request servlet is #CreateParty from #PPAdmin at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Party have been created successfully";
        String channelId = (String) request.getSession().getAttribute("_channelId");

        String _partyName = request.getParameter("_partyName");
        String _companyName = request.getParameter("_companyName");
        String _phoneNumber = request.getParameter("_phoneNumber");
        String _emailId = request.getParameter("_emailId");

        logger.debug("Value of _packageName = " + _partyName);
        logger.debug("Value of _packagePrice = " + _companyName);
        logger.debug("Value of _packageDuration = " + _phoneNumber);
        logger.debug("Value of _paymentMode = " + _emailId);

        if (UtilityFunctions.isValidPhoneNumber(_phoneNumber) == false) {
            result = "error";
            message = "Phone number should be in digit.";
            try {
                json.put("result", result);
                logger.debug("Response of #CreateParty from #PPAdmin Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of #CreateParty from #PPAdmin Servlet's Parameter  message is " + message);
            } catch (Exception ex) {
                logger.error("Exception at #CreateParty from #PPAdmin ", ex);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        }
        SgPartydetails[] partyObject = new PartyManagement().getPartyByName(channelId, _partyName);
        if (partyObject != null && partyObject.length >= 1) {
            result = "error";
            message = "Party name already taken, Try with other name.";
            try {
                json.put("result", result);
                logger.debug("Response of #CreateParty from #PPAdmin Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of #CreateParty from #PPAdmin Servlet's Parameter  message is " + message);
            } catch (Exception ex) {
                logger.error("Exception at #CreateParty from #PPAdmin ", ex);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        }
        SgPartydetails[] sps = new PartyManagement().getAddedParties(_emailId, _phoneNumber);
        if (sps != null) {
            result = "error";
            message = "Email or Phone Already Used., Try with other.";
            try {
                json.put("result", result);
                logger.debug("Response of #CreateParty from #PPAdmin Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of #CreateParty from #PPAdmin Servlet's Parameter  message is " + message);
            } catch (Exception ex) {
                logger.error("Exception at #CreateParty from #PPAdmin ", ex);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        }

        SgPartydetails partyObj = new SgPartydetails();
        partyObj.setChannelId(channelId);
        partyObj.setPartyCompanyName(_companyName);
        partyObj.setPartyEmailid(_emailId);
        partyObj.setPartyName(_partyName);
        partyObj.setPartyPhone(_phoneNumber);
        partyObj.setLastupdatedon(new Date());
        partyObj.setCreatedon(new Date());
        partyObj.setStatus(GlobalStatus.ACTIVE);
        int retValue = new PartyManagement().createParty(channelId, partyObj);
        try {
            if (retValue > 0) {
                json.put("result", result);
                json.put("message", message);
            } else {
                result = "error";
                message = "Party does not created.";
                json.put("result", result);
                logger.debug("Response of #CreateParty from #PPAdmin Servlet's Parameter result is " + result);
                json.put("message", message);
                logger.debug("Response of #CreateParty from #PPAdmin Servlet's Parameter message is " + message);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            logger.info("Response of #CreatePackage from #PPAdmin " + json.toString());
            logger.info("Response of #CreatePackage from #PPAdmin Servlet at " + new Date());
            out.print(json);
            out.flush();
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
