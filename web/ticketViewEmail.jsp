<%@page import="com.mollatech.serviceguard.nucleus.commons.UtilityFunctions"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TicketManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgEmailticket"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.EmailManagement"%>
<%@include file="header.jsp" %>
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header text-uppercase">Received Message Details</h3>
            </div>
            <!-- /.col-lg-10 -->
        </div>
        <div class="row">
        <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-home"></i><a href="home.jsp"> Home </a>&#47;  <i class="fa fa-table"></i><b> Message</b>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal " method="get">
                        <div class="dataTable_wrapper">
                            <table class="table table-bordered" id="dataTables-example">
                               
                                    <%
                                    String emailIdNo = (request.getParameter("_emailNo"));
                                    String sessionid = (String) request.getSession().getAttribute("_partnerSessionId");
                                    SgEmailticket messageDetails = new EmailManagement().getEmailDetailsById(sessionid, emailIdNo);
                                    String emailFrom = request.getParameter("emailFrom");
                                    %>
                                    <div>
                                            <input type="hidden" id="emailIdNo" value="<%=emailIdNo%>">
                                            <input type="hidden" id="emailFrom" value="<%=emailFrom%>">
                                    </div>
                                    <div class="col-lg-2">Date:</div>
                                    <div class="col-lg-6"><%=UtilityFunctions.getTMReqDate(messageDetails.getCreatedon())%></div>
                                    <br><br>
                                    <div class="col-lg-2">Subject :</div>
                                    <div class="col-lg-6" maxlength="50px"><%=messageDetails.getSubject()%></div>
                                    <br><br>
                                    <div class="col-lg-2">From :</div>
                                    <div class="col-lg-6"><%=emailFrom%></div>
                                    <br><br>
                                    <div class="col-lg-2">Category :</div>
                                    <div class="col-lg-6"maxlength="50px"><%=messageDetails.getCategory()%></div>
                                    <br><br>
                                    <div class="col-lg-2">Message:</div>
                                    <div class="col-lg-6" maxlength="50px"><p><%=messageDetails.getMessageBody()%></p></div>
                                    <br><br><br>
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <a class="btn btn-success btn-xs" tabindex="1" id="back" name="back" onclick="history.back()"><i class="fa fa-backward"></i> Back </a>     
                                    </div></table></div></form></div></div></div></div></div></div>
<script>
viewDateails();
function strCompare(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}

function viewDateails() {
//    alert(getpartnerEmailid);
    var emailNo = document.getElementById("emailIdNo").value;
    var emailId = document.getElementById("emailFrom").value;

    var s = './ViewEmail?emailNo=' +emailNo+ '&emailId=' +emailId;
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function(data) {
            if (strCompare(data._result, "success") === 0) {
            }
        }
    });
}
</script>       

<script src="bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript 
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.min.js"></script>
<script src="js/morris-data.js"></script>-->

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>

<script src="js/bootbox.min.js" type="text/javascript"></script>
</div>
</body>
</html>
<%@include file="footer.jsp" %>