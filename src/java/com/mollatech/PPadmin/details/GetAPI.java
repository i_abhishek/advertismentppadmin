/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.details;

import com.mollatech.serviceguard.nucleus.commons.MethodName;
import com.mollatech.serviceguard.nucleus.commons.Methods;
import com.mollatech.serviceguard.nucleus.commons.Serializer;
import com.mollatech.serviceguard.nucleus.db.Accesspoint;
import com.mollatech.serviceguard.nucleus.db.ResourceDetails;
import com.mollatech.serviceguard.nucleus.db.TransformDetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ashu
 */
@WebServlet(name = "GetAPI", urlPatterns = {"/GetAPI"})
public class GetAPI extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        String apName = request.getParameter("_apId");
        String resourceName = request.getParameter("_resourceId");
        String version = request.getParameter("version");
        try {
            AccessPointManagement ppw = new AccessPointManagement();
            Accesspoint accesspoint = ppw.getAccessPointByName(SessionId, channelId, apName);
            Map methodMap = null;
            String value = apName;
            ResourceDetails rd = new ResourceManagement().getResourceByName(SessionId, channelId, resourceName);
            TransformDetails tf = new TransformManagement().getTransformDetails(SessionId, channelId, accesspoint.getApId(), rd.getResourceId());
            byte[] meBy = tf.getMethods();
            Object obj = Serializer.deserialize(meBy);
            Map tmpMethods = (Map) obj;
            Methods methods = (Methods) tmpMethods.get("" + version);
            if (methodMap == null) {
                methodMap = new HashMap();
            }
            methodMap.put(value, methods);
            String mName = "";
            List list = methods.methodClassName.methodNames;
            int count = 0;
            for (int i = 0; i < list.size(); i++) {
                MethodName methodName = (MethodName) list.get(i);
                if (methodName.visibility.equalsIgnoreCase("yes")) {
                    mName = methodName.methodname.split("::")[0];
                    if (!methodName.transformedname.equals("")) {
                        mName = methodName.transformedname.split("::")[0];
                    }
                    json.put(mName, mName);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            json.put("-1", "Select API");
        }
        out.print(json);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
