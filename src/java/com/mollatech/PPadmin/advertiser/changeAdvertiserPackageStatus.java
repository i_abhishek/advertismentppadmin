/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.advertiser;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.SgAdvPreApprovalPackagedetails;
import com.mollatech.serviceguard.nucleus.db.SgApprovedAdPackagedetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.AdvPreApprovalPackageManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.ApprovedAdPackageManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author abhishekingle
 */
@WebServlet(name = "changeAdvertiserPackageStatus", urlPatterns = {"/changeAdvertiserPackageStatus"})
public class changeAdvertiserPackageStatus extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Package approved successfully";
        PrintWriter out = response.getWriter();
        String _packageName = request.getParameter("_packageName");
        String reqStatus = request.getParameter("_status");
        int ireqStatus = 0;
        if (reqStatus != null) {
            ireqStatus = Integer.parseInt(reqStatus);
        }
        int a = -1;
        ApprovedAdPackageManagement pm = new ApprovedAdPackageManagement();
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        if (SessionId != null) {
            SgApprovedAdPackagedetails sgReqPackage = pm.getReqPackageByName(SessionId, channelId, _packageName);
            if (sgReqPackage != null) {
                SgAdvPreApprovalPackagedetails bucketdetails = new AdvPreApprovalPackageManagement().getPackageByName(SessionId, channelId, _packageName);
                
                sgReqPackage.setPushAdConfiguration(bucketdetails.getPushAdConfiguration());
                sgReqPackage.setPackageDuration(bucketdetails.getPackageDuration());
                sgReqPackage.setEmailAdConfiguration(bucketdetails.getEmailAdConfiguration());
                sgReqPackage.setPdfAdConfiguration(bucketdetails.getPdfAdConfiguration());                
                sgReqPackage.setMainCredits(bucketdetails.getMainCredits());
                sgReqPackage.setTax(bucketdetails.getTax());               
                sgReqPackage.setPartnerVisibility(bucketdetails.getPartnerVisibility());
                sgReqPackage.setMakerflag(GlobalStatus.SUSPEND);
                sgReqPackage.setUpdationDate(new Date());
                
                pm.updateDetails(sgReqPackage);
                a = pm.ChangeRequestStatus(SessionId, channelId, sgReqPackage.getPackageName(), ireqStatus);
            }
            if (a == 0) {
                result = "success";
                message = "Package approved successfully";
            } else if (a == 3) {
                result = "error";
                if (ireqStatus == GlobalStatus.ACTIVE) {
                    message = "Package details already approved  ";
                } else {
                    message = "Package did not approved ";
                }
            } else {
                result = "error";
                message = "Package did not approved ";
            }
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
