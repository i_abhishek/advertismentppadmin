package com.mollatech.PPadmin.partner;

import com.mollatech.PPadmin.commons.CommonUtility;
import com.mollatech.service.nucleus.crypto.AxiomProtect;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.connector.communication.SGStatus;
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.Operators;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgPartnerrequest;
import com.mollatech.serviceguard.nucleus.db.SgUsers;
import com.mollatech.serviceguard.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerRequestManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement;
import com.mollatech.serviceguard.nucleus.settings.SendNotification;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.bouncycastle.util.encoders.Base64;
import org.json.JSONObject;

/**
 *
 * @author nilesh
 */
public class partnerRequestStatus extends HttpServlet {

    public final int PASSWORD = 1;

    final int ADMIN = 0;

    final int OPERATOR = 1;

    final int REPORTER = 2;

    public static final int SEND = 0;
    static final Logger logger = Logger.getLogger(partnerRequestStatus.class);

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        logger.info("Requested Servlet is partnerRequestStatus at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());

        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Partner Status change successful.";
        PrintWriter out = response.getWriter();

        try {
            response.setContentType("text/html;charset=UTF-8");
            String partnerRequestId = request.getParameter("partnerRequestId");
            String partnerRequestStatus = request.getParameter("partnerRequestStatus");
            String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
            String channelId = (String) request.getSession().getAttribute("_channelId");
            String rejectReason = request.getParameter("reason");
            String channelName = "ServiceGuard Portal";
            PartnerManagement ppw = new PartnerManagement();
            int ipartnerRequestId = 0;
            if (partnerRequestId != null) {
                ipartnerRequestId = Integer.parseInt(partnerRequestId);
            }
            int ipartnerRequestStatus = 0;
            if (partnerRequestStatus != null) {
                ipartnerRequestStatus = Integer.parseInt(partnerRequestStatus);
            }
            UsersManagement um = new UsersManagement();
            SgPartnerrequest sgpartner = new PartnerRequestManagement().getPartnerRequestsbyReqId(sessionId, ipartnerRequestId);
            if (sgpartner != null) {
                PartnerDetails details[] = new PartnerManagement().getPartnerDetailsByEmail(sessionId, channelId, sgpartner.getEmail());
                if (details != null) {
                    result = "error";
                    message = "Developer With This Email Id Already Exist.";
                    json.put("_result", result);
                    json.put("_message", message);
                    return;
                }
                details = new PartnerManagement().getPartnerDetailsByPhone(sessionId, channelId, sgpartner.getPhone());
                if (details != null) {
                    result = "error";
                    message = "Developer With This Phone Number Already Exist.";
                    json.put("_result", result);
                    json.put("_message", message);
                    return;
                }
            }
            if (sgpartner != null) {
                if (ipartnerRequestStatus == GlobalStatus.ACTIVE) {

                    PartnerDetails[] detailses = new PartnerManagement().getAllPartnerDetails(sessionId, channelId);
                    if (detailses == null) {
                        detailses = new PartnerDetails[0];
                    }
                    String _groupId = request.getParameter("_groupId");
                    if (_groupId == null) {
                        result = "error";
                        message = "Please select group";
                        try {
                            json.put("_result", result);
                            logger.debug("Response of partnerRequestStatus Servlet's Parameter  result is " + result);
                            json.put("_message", message);
                            logger.debug("Response of partnerRequestStatus Servlet's Parameter  message is " + message);
                        } catch (Exception e) {
                            logger.error("Exception at partnerRequestStatus ", e);
                        }
                        out.print(json);
                        out.flush();
                        out.close();
                        return;
                    } else if (_groupId.equalsIgnoreCase("null")) {
                        result = "error";
                        message = "Please add group first";
                        try {
                            json.put("_result", result);
                            logger.debug("Response of partnerRequestStatus Servlet's Parameter  result is " + result);
                            json.put("_message", message);
                            logger.debug("Response of partnerRequestStatus Servlet's Parameter  message is " + message);
                        } catch (Exception e) {
                            logger.error("Exception at partnerRequestStatus ", e);
                        }
                        out.print(json);
                        out.flush();
                        out.close();
                        return;
                    }
                    if (AxiomProtect.GetPartnersAllowed() == -1) {
                        result = "error";
                        message = "Adding Developer is not available in this license";
                        try {
                            json.put("_result", result);
                            logger.debug("Response of partnerRequestStatus Servlet's Parameter  result is " + result);
                            json.put("_message", message);
                            logger.debug("Response of partnerRequestStatus Servlet's Parameter  message is " + message);
                        } catch (Exception e) {
                            logger.error("Exception at partnerRequestStatus ", e);
                        }
                        out.print(json);
                        out.flush();
                        out.close();
                        return;
                    }
                    if (AxiomProtect.GetPartnersAllowed() <= detailses.length) {
                        result = "error";
                        message = "Adding Developers Limit is Reached";
                        try {
                            json.put("_result", result);
                            logger.debug("Response of partnerRequestStatus Servlet's Parameter  result is " + result);
                            json.put("_message", message);
                            logger.debug("Response of partnerRequestStatus Servlet's Parameter  message is " + message);
                        } catch (Exception e) {
                            logger.error("Exception at partnerRequestStatus ", e);
                        }
                        out.print(json);
                        out.flush();
                        out.close();
                        return;
                    }
                    int pStatus = new PartnerRequestManagement().ChangePartnerRequestStatus(sessionId, ipartnerRequestId, GlobalStatus.ACTIVE);
                    if (pStatus == 0) {

                        String token = sgpartner.getName() + sgpartner.getEmail() + new Date() + sessionId + sgpartner.getPhone();
                        byte[] SHA1hash = CommonUtility.SHA1(token);
                        token = new String(Base64.encode(SHA1hash));
                        int pstatus = new PartnerManagement().CreatePartner(sessionId, channelId, sgpartner.getName(), sgpartner.getEmail(), GlobalStatus.ACTIVE, sgpartner.getPhone(), _groupId, GlobalStatus.ACTIVE, sgpartner.getIpAddress(), token, sgpartner.getIpLive());
                        if (pstatus != -1) {
                            int pstatus1 = new PartnerRequestManagement().ChangePartnerRequest(sessionId, ipartnerRequestId, pstatus);
                            if (pstatus1 == 0) {
                                SgUsers sguser = new SgUsers();
                                sguser.setUsername(sgpartner.getName());
                                sguser.setEmail(sgpartner.getEmail());
                                sguser.setPhone(sgpartner.getPhone());
                                sguser.setChannelid(channelId);
                                sguser.setAttempts(0);
                                sguser.setPartnerid(pstatus);
                                sguser.setCreatedon(new Date());
                                sguser.setLastlogindate(new Date());
                                sguser.setStatus(GlobalStatus.ACTIVE);
                                sguser.setType(ADMIN);
                                sguser.setPassword(sgpartner.getField1());
                                int ErrorCode = um.AddSgUsers(sessionId, channelId, sguser);
                                if (ErrorCode == 0) {
                                    result = "success";
                                    message = " Developer Request Approved.";
                                    json.put("_result", result);
                                    logger.debug("Response of partnerRequestStatus Servlet's Parameter  result is " + result);
                                    json.put("_message", message);
                                    logger.debug("Response of partnerRequestStatus Servlet's Parameter  message is " + message);
                                    return;
                                } else {
                                    pstatus = ppw.deletePartnerDetailByToken(sessionId, channelId, token);
                                    if (pstatus == 0) {
                                        result = "error";
                                        message = "Unable to approve Developer Request.";
                                        json.put("_result", result);
                                        logger.debug("Response of partnerRequestStatus Servlet's Parameter  result is " + result);
                                        json.put("_message", message);
                                        logger.debug("Response of partnerRequestStatus Servlet's Parameter  message is " + message);
                                        return;
                                    }
                                }
                            } else {
                                new PartnerRequestManagement().ChangePartnerRequestStatus(sessionId, ipartnerRequestId, GlobalStatus.PENDING);
                                ppw.deletePartnerDetailByToken(sessionId, null, token);
                                result = "error";
                                message = "Unable to approve Developer Request.";
                                json.put("_result", result);
                                logger.debug("Response of partnerRequestStatus Servlet's Parameter  result is " + result);
                                json.put("_message", message);
                                logger.debug("Response of partnerRequestStatus Servlet's Parameter  message is " + message);
                                return;
                            }
                        } else {
                            new PartnerRequestManagement().ChangePartnerRequestStatus(sessionId, ipartnerRequestId, GlobalStatus.PENDING);
                            result = "error";
                            message = "Unable to approve Developer Request.";
                            json.put("_result", result);
                            logger.debug("Response of partnerRequestStatus Servlet's Parameter  result is " + result);
                            json.put("_message", message);
                            logger.debug("Response of partnerRequestStatus Servlet's Parameter  message is " + message);
                            return;
                        }
                    } else {
                        new PartnerRequestManagement().ChangePartnerRequestStatus(sessionId, ipartnerRequestId, GlobalStatus.PENDING);
                        result = "error";
                        message = "Unable to approve Developer Request.";
                        json.put("_result", result);
                        logger.debug("Response of partnerRequestStatus Servlet's Parameter  result is " + result);
                        json.put("_message", message);
                        logger.debug("Response of partnerRequestStatus Servlet's Parameter  message is " + message);
                        return;
                    }
                } else if (ipartnerRequestStatus == GlobalStatus.REJECTED) {
                    sgpartner.setRejectReason(rejectReason);
                    sgpartner.setStatus(GlobalStatus.REJECTED);
                    int pStatus = new PartnerRequestManagement().editPartnerRequest(sessionId, sgpartner);
                    if (pStatus == 0) {
                        int productType = 3;
                        Operators[] operatorObj = new OperatorsManagement().getAdminOperator(channelId);
                        String[] operatorEmail = null;
                        if (operatorObj != null) {
                            operatorEmail = new String[operatorObj.length];
                            for (int i = 0; i < operatorObj.length; i++) {
                                operatorEmail[i] = (String) operatorObj[i].getEmailid();
                            }
                        }
                        String tmessage = LoadSettings.g_templateSettings.getProperty("email.partner.reject");
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                        if (tmessage != null) {
                            Date d = new Date();
                            tmessage = tmessage.replaceAll("#Name#", sgpartner.getName());
                            tmessage = tmessage.replaceAll("#EmailId#", sgpartner.getEmail());
                            tmessage = tmessage.replaceAll("#channel#", channelName);
                            tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                            tmessage = tmessage.replaceAll("#RejectReason#", rejectReason);
                        }
                        String Subject = LoadSettings.g_subjecttemplateSettings.getProperty("email.partner.reject.subject");
                        SGStatus statusSG = new SendNotification().SendEmail(channelId, sgpartner.getEmail(), Subject, tmessage, operatorEmail, null, null, null, productType);
                        if (statusSG.iStatus != GlobalStatus.PENDING && statusSG.iStatus != SEND) {
                            result = "error";
                            message = sgpartner.getName() + ", Developer request rejected successfully but failed to sent email notification.";
                            try {
                                json.put("_result", result);
                                logger.debug("Response of partnerRequestStatus Servlet's Parameter  result is " + result);
                                json.put("_message", message);
                                logger.debug("Response of partnerRequestStatus Servlet's Parameter  message is " + message);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            out.print(json);
                            out.flush();
                            return;
                        }
                        result = "success";
                        message = sgpartner.getName() + ", Developer request rejected successfully.";
                        json.put("_result", result);
                        logger.debug("Response of partnerRequestStatus Servlet's Parameter  result is " + result);
                        json.put("_message", message);
                        logger.debug("Response of partnerRequestStatus Servlet's Parameter  message is " + message);
                    } else if (pStatus != 0) {
                        result = "error";
                        logger.debug("Response of partnerRequestStatus Servlet's Parameter  result is " + result);
                        message = "Failed to reject Developer request...!!!";
                        logger.debug("Response of partnerRequestStatus Servlet's Parameter  message is " + message);
                        json.put("_result", result);
                        json.put("_message", message);
                        return;
                    }
                }
            } else {
                result = "error";
                message = "Developer request details does not found..!!!";
                json.put("_result", result);
                logger.debug("Response of partnerRequestStatus Servlet's Parameter  result is " + result);
                json.put("_message", message);
                logger.debug("Response of partnerRequestStatus Servlet's Parameter  message is " + message);
                return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
            return;
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
