package com.mollatech.PPadmin.promocode;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.SgPromocode;
import com.mollatech.serviceguard.nucleus.db.connector.management.PromocodeManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "EditPromocode", urlPatterns = { "/EditPromocode" })
public class EditPromocode extends HttpServlet {

    static final Logger logger = Logger.getLogger(EditPromocode.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Request servlet is #EditPromocode from #PPAdmin at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Promo code has been updated successfully";
        String channelId = (String) request.getSession().getAttribute("_channelId");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String promocodeId = request.getParameter("promocodeId");
        String promocode = request.getParameter("_promoCode_edit");
        String _promoExpiry = request.getParameter("_promoExpiry_edit");
        String _usageCount = request.getParameter("_usageCount_edit");
        String _partnerUsageCount = request.getParameter("_partnerUsageCount_edit");
        String[] visibleTo = request.getParameterValues("visibleTo_edit");
        String discountType = request.getParameter("_promoCodeDiscountType_edit");
        String _promoDiscount = request.getParameter("_promoDiscount_edit");
        logger.debug("Value of _promocode = " + promocode);
        logger.debug("Value of _promocodeId = " + promocodeId);
        logger.debug("Value of _packagePrice = " + _promoExpiry);
        logger.debug("Value of _usageCount = " + _usageCount);
        if (visibleTo != null) {
            logger.debug("Value of visibleTo = " + Arrays.toString(visibleTo));
        }
        logger.debug("Value of _promoDiscount = " + _promoDiscount);
        int iusageCount = 0;
        int flDiscount = 0;
        int ipartnerUsageCount = 0;
        int expiry = 0;
        int ipromocode = 0;
        try {
            if (_usageCount != null && !_usageCount.equals("")) {
                iusageCount = Integer.parseInt(_usageCount);
            }
            if (_partnerUsageCount != null && !_partnerUsageCount.equals("")) {
                ipartnerUsageCount = Integer.parseInt(_partnerUsageCount);
            }
            if (promocodeId != null && !promocodeId.isEmpty()) {
                ipromocode = Integer.parseInt(promocodeId);
            }
            if (_promoDiscount != null && !_promoDiscount.equals("")) {
                flDiscount = Integer.parseInt(_promoDiscount);
            }
            if (_promoExpiry != null && !_promoExpiry.equals("")) {
                expiry = Integer.parseInt(_promoExpiry);
            }
            Calendar calendarObj = Calendar.getInstance();
            calendarObj.setTime(new Date());
            calendarObj.add(Calendar.DATE, expiry);
            String partnerId = "";
            if (visibleTo != null) {
                for (int i = 0; i < visibleTo.length; i++) {
                    partnerId += visibleTo[i] + ",";
                }
            }
            SgPromocode promoObj = new PromocodeManagement().getPromocodeById(SessionId, channelId, ipromocode);
            if (promoObj != null) {
                promoObj.setChannelId(promoObj.getChannelId());
                promoObj.setExpiryInNoOfDays(expiry);
                promoObj.setCreatedon(promoObj.getCreatedon());
                promoObj.setUsageCount(iusageCount);
                promoObj.setDiscount(flDiscount);
                promoObj.setPartnerUsageCount(ipartnerUsageCount);
                promoObj.setCode(promocode);
                promoObj.setStatus(GlobalStatus.SUSPEND);
                promoObj.setRequestFlag(GlobalStatus.SUSPEND);
                promoObj.setExpireOn(calendarObj.getTime());
                promoObj.setPartnerId(partnerId);
                promoObj.setLastUpdatedOn(new Date());
                promoObj.setDiscountType(discountType);
                int retValue = new PromocodeManagement().editPromoDetails(SessionId, promoObj);
                if (retValue == 0) {
                    json.put("result", result);
                    json.put("message", message);
                } else {
                    result = "error";
                    message = "Promo code updation failed.";
                    json.put("result", result);
                    logger.debug("Response of #EditPromocode from #PPAdmin Servlet's Parameter  result is " + result);
                    json.put("message", message);
                    logger.debug("Response of #EditPromocode from #PPAdmin Servlet's Parameter  message is " + message);
                }
            } else {
                result = "error";
                message = "Promo code details not found to update.";
                json.put("result", result);
                logger.debug("Response of #EditPromocode from #PPAdmin Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of #EditPromocode from #PPAdmin Servlet's Parameter  message is " + message);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            logger.info("Response of #EditPromocode from #PPAdmin " + json.toString());
            logger.info("Response of #EditPromocode from #PPAdmin Servlet at " + new Date());
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
