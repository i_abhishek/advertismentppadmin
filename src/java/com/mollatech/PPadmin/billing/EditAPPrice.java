package com.mollatech.PPadmin.billing;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.commons.MakerChaker;
import com.mollatech.serviceguard.nucleus.db.SgBucketdetails;
import com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.json.JSONArray;

@WebServlet(name = "EditAPPrice", urlPatterns = {"/EditAPPrice"})
public class EditAPPrice extends HttpServlet {

    final String itemtype = "Billing Manager(EditAPPrice)";

    static final Logger logger = Logger.getLogger(EditAPPrice.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Request servlet is #EditAPPrice from #PPAdmin at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "AP Price have been updated successfully";
        String channelId = (String) request.getSession().getAttribute("_channelId");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String packageID = request.getParameter("packageId");
        logger.debug("Value of packageID  = " + packageID);
        int pId = 0;
        if (packageID != null) {
            pId = Integer.parseInt(packageID);
        }
        SgBucketdetails packObj = new PackageManagement().getPackageDetails(SessionId, channelId, pId);
        try {
            if (packObj == null) {
                result = "error";
                message = "Can't edit the package details.";
                json.put("result", result);
                json.put("message", message);
                return;
            }
        } catch (Exception e) {
        }
        String mainCredit = request.getParameter("mainCreditForAP");
        logger.debug("Value of MainCredit  = " + mainCredit);
        String freeCredits = request.getParameter("freeCreditForAP");
        logger.debug("Value of FreeCredit  = " + freeCredits);
        String paramcountAPR = request.getParameter("paramcountAPR");
        logger.debug("Value of No. API  = " + paramcountAPR);
        String apNameRate = request.getParameter("apNameRate");
        logger.debug("Value of AP Name  = " + apNameRate);
        String resourceName = request.getParameter("_ResourceForAPPricing2");
        logger.debug("Value of resource Name  = " + resourceName);
        String version = request.getParameter("_VersionForAPricing2");
        logger.debug("Value of version Name  = " + version);
        String PartnerVisibility[] = request.getParameterValues("visibilityPartner");
        JSONArray apRateArray = new JSONArray();
        JSONObject jsonAPRate = new JSONObject();
        int param = 0;
        int retValue = 0;
        JSONObject aps = new JSONObject();
        try {
            jsonAPRate.put("mainCredit", mainCredit);
            jsonAPRate.put("freeCredit", freeCredits);
            if (paramcountAPR != null) {
                param = Integer.parseInt(paramcountAPR);
            }
            for (int i = 0; i < param; i++) {
                String apiName;
                String apiPrice;
                apiName = request.getParameter("apiName" + i);
                apiPrice = request.getParameter("apiPrice" + i);
                String billingType = request.getParameter("chargingSrategy" + i);
                jsonAPRate.put(apiName, apiPrice + ":" + billingType);
            }
            String key = apNameRate + ":" + resourceName + ":" + version;
            aps.put(key, jsonAPRate);
            PackageManagement pkMangObj = new PackageManagement();
            packObj.setTabShowFlag(GlobalStatus.FLATPRICE);

            String partnerId = "";
            if (PartnerVisibility != null) {
                for (int i = 0; i < PartnerVisibility.length; i++) {
                    partnerId += PartnerVisibility[i] + ",";
                }
            }
            packObj.setPartnerVisibility(partnerId);
            if (packObj.getApRateDetails() == null || packObj.getApRateDetails().equals("")) {
                apRateArray.put(aps);
                packObj.setApRateDetails(apRateArray.toString());
                retValue = pkMangObj.editPackage(SessionId, packObj);
                request.getSession().setAttribute("apJSONArray", apRateArray);
                //System.out.println("apJson " + apRateArray.toString());
            } else {
                boolean flag = true;
                String jsonString = packObj.getApRateDetails();
                JSONArray jsOld = new JSONArray(jsonString);
                for (int i = 0; i < jsOld.length(); i++) {
                    JSONObject jsonexists1 = jsOld.getJSONObject(i);
                    if (jsonexists1.has(key)) {
                        jsonexists1.remove(key);
                        jsonexists1.put(key, jsonAPRate);
                        jsOld.put(i, jsonexists1);
                        packObj.setApRateDetails(jsOld.toString());
                        retValue = pkMangObj.editPackage(SessionId, packObj);
                        flag = false;
                        break;
                    }
                }
                if (flag) {
                    JSONObject jsObject = new JSONObject();
                    jsObject.put(key, jsonAPRate);
                    jsOld.put(jsObject);
                    packObj.setApRateDetails(jsOld.toString());
                    retValue = pkMangObj.editPackage(SessionId, packObj);
                }
            }
            MakerChaker makerChakerObj = (MakerChaker) request.getSession().getAttribute("makerChacker");
            SgReqbucketdetails existsObj = new RequestPackageManagement().getReqPackageByName(SessionId, channelId, packObj.getBucketName());
            boolean makerChackerFlag = true;
            if (makerChakerObj == null) {
                makerChackerFlag = false;
            } else if (makerChakerObj.status == GlobalStatus.SUSPEND) {
                makerChackerFlag = false;
            }
            if (retValue == -1) {
                result = "error";
                message = "AP Price does not saved.";
                json.put("result", result);
                logger.debug("Response of #EditAPPrice from #PPAdmin Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of #EditAPPrice from #PPAdmin Servlet's Parameter  message is " + message);
            } else {
                if (makerChackerFlag == false && existsObj != null) {
                    existsObj.setPartnerVisibility(packObj.getPartnerVisibility());
                    existsObj.setTabShowFlag(packObj.getTabShowFlag());
                    existsObj.setApRateDetails(packObj.getApRateDetails());
                    new RequestPackageManagement().editReqPackage(SessionId, channelId, existsObj);
                }
                json.put("result", result);
                json.put("message", message);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            logger.info("Response of #EditAPPrice from #PPAdmin " + json.toString());
            logger.info("Response of #EditAPPrice from #PPAdmin Servlet at " + new Date());
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
