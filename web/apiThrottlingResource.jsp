<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MethodName"%>
<%@page import="java.util.List"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Classes"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Methods"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.TransformDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TransformManagement"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <div class="control-group">
        <label class="control-label"  for="username"></label>
        <div class="controls">
            <select id="_resourceForApiThrottling" name="_resourceForApiThrottling"  class="form-control span2" onchange="getAPIThrottlingVersion(this.value)" style="width: 100%">
                <%
                    AccessPointManagement ppw = new AccessPointManagement();
                    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
                    String channelId = (String) request.getSession().getAttribute("_channelId");
                    String apName = request.getParameter("_apName");                    
                    if (!apName.equals("-1")) {                     
                        int activeResourceCount = 0;
                        Accesspoint accesspoint = ppw.getAccessPointByName(SessionId, channelId, apName);
                        String resources = accesspoint.getResources();
                        if (resources != null) {
                            String[] resourcesDetails = resources.split(",");
                            if (resourcesDetails != null) {
                                for (int i = 0; i < resourcesDetails.length; i++) {
                                    int resourceId = Integer.parseInt(resourcesDetails[i]);
                                    ResourceDetails rs = new ResourceManagement().getResourceById(resourceId);
                                    if(rs.getStatus() == GlobalStatus.ACTIVE){
                                        activeResourceCount ++;
                %>
                <option value="<%=rs.getName()%>"><%=rs.getName()%></option>               
                <%              }
                            } if(activeResourceCount == 0){%>
                <option value="-1">No Active Resource</option>        
<%                            }
                        }
                    }
                } else {%>
                <option value="-1">Select Resource</option>  
                <% }%>
            </select>           
        </div>
    </div>
</html>
