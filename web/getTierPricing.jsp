<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgBucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement"%>
<%
    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
    String channelId = (String) request.getSession().getAttribute("_channelId");
    String apId = request.getParameter("_apId");
    String _resourceId = request.getParameter("_resourceId");
    String version = request.getParameter("version");
    String apiName = request.getParameter("apiName");
    String packageName = request.getParameter("packageName");

    if (!apId.equalsIgnoreCase("-1")) {
        SgReqbucketdetails pacObject = new RequestPackageManagement().getReqPackageByName(SessionId, channelId, packageName);
        String rangeFrom5 = "";
        String rangeFrom4 = "";
        String rangeFrom3 = "";
        String rangeFrom2 = "";
        String rangeFrom1 = "";
        String rangeTo5 = "";
        String rangeTo4 = "";
        String rangeTo3 = "";
        String rangeTo2 = "";
        String rangeTo1 = "";
        String price5 = "";
        String price4 = "";
        String price3 = "";
        String price2 = "";
        String price1 = "";
        if (pacObject != null) {
            JSONObject reqJSONObj = null;
            String tierPricing = pacObject.getTierRateDetails();
            if (tierPricing != null && !tierPricing.equals("")) {
                String aptierPriceDetails = pacObject.getTierRateDetails();
                String key = apId + ":" + _resourceId + ":" + version + ":" + apiName;
                //System.out.println("KEY "+ key);
                JSONArray jsOld = new JSONArray(aptierPriceDetails);
                for (int j = 0; j < jsOld.length(); j++) {
                    JSONObject jsonexists1 = jsOld.getJSONObject(j);
                    if (jsonexists1.has(key)) {
                        reqJSONObj = jsonexists1.getJSONObject(key);
                        if (reqJSONObj != null) {
                            break;
                        }
                    }
                }
            }
            if (reqJSONObj != null) {
                rangeFrom5 = reqJSONObj.getString("rangeFrom5");
                rangeFrom4 = reqJSONObj.getString("rangeFrom4");
                rangeFrom3 = reqJSONObj.getString("rangeFrom3");
                rangeFrom2 = reqJSONObj.getString("rangeFrom2");
                rangeFrom1 = reqJSONObj.getString("rangeFrom1");
                rangeTo5 = reqJSONObj.getString("rangeTo5");
                rangeTo4 = reqJSONObj.getString("rangeTo4");
                rangeTo3 = reqJSONObj.getString("rangeTo3");
                rangeTo2 = reqJSONObj.getString("rangeTo2");
                rangeTo1 = reqJSONObj.getString("rangeTo1");
                price5 = reqJSONObj.getString("price5");
                price4 = reqJSONObj.getString("price4");
                price3 = reqJSONObj.getString("price3");
                price2 = reqJSONObj.getString("price2");
                price1 = reqJSONObj.getString("price1");
            }
        }
%>
<table id="_headerTable">
    <div class="form-group">   
        <label class="col-lg-2 control-label" style="">Range</label>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                <input type="text" id="rangeFrom1" name="rangeFrom1" class="form-control" placeholder="From " value="<%=rangeFrom1%>">                                    
            </div>
        </div>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                <input type="text" id="rangeTo1" name="rangeTo1" class="form-control" placeholder="To" value="<%=rangeTo1%>">                                    
            </div>
        </div>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                <input type="text" id="price1" name="price1" class="form-control" placeholder="Price" value="<%=price1%>">                                    
            </div>
        </div>
    </div>

    <div class="form-group">   
        <label class="col-lg-2 control-label" style="">Range</label>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                <input type="text" id="rangeFrom2" name="rangeFrom2" class="form-control" placeholder="From " value="<%=rangeFrom2%>">                                    
            </div>
        </div>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                <input type="text" id="rangeTo2" name="rangeTo2" class="form-control" placeholder="To" value="<%=rangeTo2%>">                                    
            </div>
        </div>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                <input type="text" id="price2" name="price2" class="form-control" placeholder="Price" value="<%=price2%>">                                    
            </div>
        </div>
    </div>
    <div class="form-group">   
        <label class="col-lg-2 control-label" style="">Range</label>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                <input id="rangeFrom3" name="rangeFrom3" class="form-control" placeholder="From " value="<%=rangeFrom3%>">                                    
            </div>
        </div>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                <input id="rangeTo3" name="rangeTo3" type="text" class="form-control" placeholder="To" value="<%=rangeTo3%>">                                    
            </div>
        </div>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                <input type="text" id="price3" name="price3" class="form-control" placeholder="Price" value="<%=price3%>">                                    
            </div>
        </div>
    </div>
    <div class="form-group">   
        <label class="col-lg-2 control-label" style="">Range</label>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                <input id="rangeFrom4" name="rangeFrom4" type="text" class="form-control" placeholder="From " value="<%=rangeFrom4%>">                                    
            </div>
        </div>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                <input type="text" id="rangeTo4" name="rangeTo4" class="form-control" placeholder="To" value="<%=rangeTo4%>">                                    
            </div>
        </div>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                <input type="text" id="price4" name="price4" class="form-control" placeholder="Price" value="<%=price4%>">                                    
            </div>
        </div>
    </div>
    <div class="form-group">   
        <label class="col-lg-2 control-label" style="">Range</label>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                <input type="text" id="rangeFrom5" name="rangeFrom5" class="form-control" placeholder="From " value="<%=rangeFrom5%>">                                    
            </div>
        </div>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                <input type="text" id="rangeTo5" name="rangeTo5" class="form-control" placeholder="To" value="<%=rangeTo5%>">                                    
            </div>
        </div>
        <div class="col-lg-2">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                <input type="text" id="price5" name="price5" class="form-control" placeholder="Price" value="<%=price5%>">                                    
            </div>
        </div>
    </div>
</table>
<%
} else {
%>
<div> </div>
<%}%>

