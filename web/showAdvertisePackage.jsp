<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AdvPreApprovalPackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgAdvPreApprovalPackagedetails"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgBucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@include file="header.jsp" %>
<script src="js/designPackage.js" type="text/javascript"></script>
<script src="js/advertiserPackageOperation.js" type="text/javascript"></script>
<script src="js/showPackage.js" type="text/javascript"></script>
<link href="./select2/select2.css" rel="stylesheet"/>
<%
    String packageName = request.getParameter("_edit");
    
    SgAdvPreApprovalPackagedetails packageObj = new AdvPreApprovalPackageManagement().getPackageByName(SessionId, channelId, packageName);
    String partnerIds = packageObj.getPartnerVisibility();
    DecimalFormat decimalFormat = new DecimalFormat("#");
    decimalFormat.setMaximumFractionDigits(0);
    boolean flag = false;
    String options = "<option value=all selected>NA</option>\n";
    if (partnerIds != null && !partnerIds.isEmpty() && partnerIds.contains("all,")) {
        options = "<option value=all selected>All</option>\n";
    } else if (partnerIds != null && !partnerIds.isEmpty()) {
        String[] parArray = partnerIds.split(",");
        options = "";
        for (int i = 0; i < parArray.length; i++) {
            int parId = Integer.parseInt(parArray[i]);
            String emailId = "";
            PartnerDetails parObj = new PartnerManagement().getPartnerDetails(parId);
            if (parObj != null) {
                emailId = parObj.getPartnerEmailid();
            }
            options += "<option selected value='" + parId + "'>" + emailId + "</option>\n";
        }
        flag = true;
    }
    PartnerDetails[] partnerObj = new PartnerManagement().getAllPartnerDetails(SessionId, channelId);
    if (partnerObj != null) {
        if (flag) {
            options += "<option value=all>ALL</option>\n";
        }
        for (int i = 0; i < partnerObj.length; i++) {
            if (partnerIds != null && partnerIds.contains(String.valueOf(partnerObj[i].getPartnerId()))) {
                continue;
            }
            if (partnerObj[i].getStatus() == 1) {
                options += "<option value='" + partnerObj[i].getPartnerId() + "'>" + partnerObj[i].getPartnerEmailid() + "</option>\n";
            }
        }
    }
    String promocodeEnable = LoadSettings.g_sSettings.getProperty("functionalityOf.feature.promocode.booleanValue");
    String loanEnable      = LoadSettings.g_sSettings.getProperty("functionalityOf.feature.loan.booleanValue");
    String vatTax          = LoadSettings.g_sSettings.getProperty("functionalityOf.feature.vatTax.booleanValue");
    String serviceTax      = LoadSettings.g_sSettings.getProperty("functionalityOf.feature.serviceTax.booleanValue");
    boolean featureFlag = false;
    if(promocodeEnable.equalsIgnoreCase("true")||loanEnable.equalsIgnoreCase("true")){
        featureFlag = true;
    }
    
    String pushConf = packageObj.getPushAdConfiguration();
    String pushAllowLogo = ""; String pushMessageChar = ""; String pushCountPerDay = ""; String pushCreditDeduction = "";
    if(pushConf != null && !pushConf.isEmpty()){       
        JSONObject pushConfJson = new JSONObject(pushConf);
        if(pushConfJson.has("pushAllowLogo")){
            pushAllowLogo = pushConfJson.getString("pushAllowLogo");
        }
        if(pushConfJson.has("pushMessageLength")){
            pushMessageChar = pushConfJson.getString("pushMessageLength");
        }
        if(pushConfJson.has("pushAdPerDay")){
            pushCountPerDay = pushConfJson.getString("pushAdPerDay");
        }
        if(pushConfJson.has("pushCreditDeductionPerAd")){
            pushCreditDeduction = pushConfJson.getString("pushCreditDeductionPerAd");
        }        
    }
    
    String emailConf = packageObj.getEmailAdConfiguration();
    String emailMessageChar = ""; String emailCountPerDay = ""; String emailCreditDeduction = "";
    if(emailConf != null && !emailConf.isEmpty()){       
        JSONObject emailConfJson = new JSONObject(emailConf);        
        if(emailConfJson.has("emailMessageCharacter")){
            emailMessageChar = emailConfJson.getString("emailMessageCharacter");
        }
        if(emailConfJson.has("emailAdPerDay")){
            emailCountPerDay = emailConfJson.getString("emailAdPerDay");
        }
        if(emailConfJson.has("emailCreditDeductionPerAd")){
            emailCreditDeduction = emailConfJson.getString("emailCreditDeductionPerAd");
        }        
    }
    
    String pdfConf = packageObj.getPdfAdConfiguration();
    String pdfAdImageLength = ""; String pdfAdCountPerDay = ""; String pdfAdCreditDeduction = "";
    if(pdfConf != null && !pdfConf.isEmpty()){       
        JSONObject pdfConfJson = new JSONObject(pdfConf);        
        if(pdfConfJson.has("pdfImageLength")){
            pdfAdImageLength = pdfConfJson.getString("pdfImageLength");
        }
        if(pdfConfJson.has("pdfAdPerDay")){
            pdfAdCountPerDay = pdfConfJson.getString("pdfAdPerDay");
        }
        if(pdfConfJson.has("creditDeductionPerAd")){
            pdfAdCreditDeduction = pdfConfJson.getString("creditDeductionPerAd");
        }        
    }
    
%>
<div id="page-wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">" <%=packageObj.getPackageName()%> " package details</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-home"></i><a href="home.jsp"> Home</a> &#47;
                    <i class="fa fa-shopping-cart"></i><a href="adPackageRequest.jsp"> Package Request</a> &#47;
                    <i class="fa fa-edit"></i> Package Details
                </div>
                <div class="panel-body">
                    <h4>Package details</h4>
                    <hr>                   
                    <!--                    <form class="form-horizontal" id="edit_package_form1" name="edit_package_form1" role="form">-->
                    <div class="col-lg-3">
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fa fa-shopping-cart"></i></span>                                
                            <input type="text" class="form-control" placeholder="Package name" value="<%=packageObj.getPackageName()%>" disabled>                                
                        </div>
                    </div>                                
                    <div class="col-lg-3">
                        <div class="form-group input-group">
                            <span class="input-group-addon">$</span>                               
                            <input type="text" class="form-control" placeholder="Package price" value="<%=packageObj.getPlanAmount()%>" disabled>                                
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>                                                                 
                            <input type="text" class="form-control" placeholder="Package price" value="<%=packageObj.getPackageDuration()%>" disabled>                                                                  
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>                                                                
                            <input type="text" class="form-control" placeholder="Package price" value="<%=packageObj.getPaymentMode()%>" disabled>                                  
                        </div>
                    </div>
                    <label class="control-label col-sm-3"><span style="padding-left:75px"> Developer Visibility</span></label>
                    <div class="left"></div>
                    <div class="col-lg-8">
                        <select id="visible" name="visible" disabled multiple="multiple" style="width: 114%">
                            <%=options%> 
                        </select>
                    </div>
                </div>
                <div class="col-lg-12">
                    <ul class="nav nav-pills" id="addPackageTab">
                        <li class="active" id="rateTab"><a href="#billingrate" data-toggle="tab">Bill Details</a></li>                    
                        <li id="rateTab"><a href="#rate" data-toggle="tab">Push Ad</a></li>                    
                        <li id="accessPointTab"><a href="#apRate" data-toggle="tab">Email Ad</a></li>
                        <li id="flatRateTab"><a href="#flatRate" data-toggle="tab">PDF Ad</a></li>                           
                    </ul>
                </div>
                <br><br>
                <br>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="billingrate">
                        <form class="form-horizontal" id="edit_package_form" name="edit_package_form" role="form">                                                                                                                       
                            <div class="form-group">                                    
                                <label class="col-lg-3 control-label" >Credit</label>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" class="form-control" id="pdfAdPerDayCount" name="pdfAdPerDayCount" placeholder="Main Credit" onkeypress="return isNumberKey(event)" value="<%=packageObj.getMainCredits()%>">                                    
                                    </div>
                                </div>                                   
                            </div>                                                                                                                                                                    
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" >Tax</label>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" class="form-control" id="pushAdPerDayCount" name="pushAdPerDayCount" placeholder="Tax" onkeypress="return isNumberKey(event)" value="<%=packageObj.getTax()%>">                                    
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" >Recurring Plan Id</label>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" class="form-control" id="pushAdPerCreditDeduction" name="pushAdPerCreditDeduction" placeholder="Recurring Plan Id" onkeypress="return isNumberKey(event)" value="<%=packageObj.getRecurrenceBillingPlanId()%>">                                    
                                    </div>
                                </div>
                            </div>        
                            <a  class="btn btn-success btn-xs" style="margin-left: 26%" onclick="showPushAdDetails()">Next <i class="fa fa-forward"></i> </a>
                        </form>
                    </div>
                    
                    
                    <!-- Rate Tab -->
                    <div class="tab-pane fade" id="rate">
                        <form class="form-horizontal" id="edit_package_form" name="edit_package_form" role="form">
                            <input type="hidden" id="firstTab" name="firstTab" value="<%=packageObj.getTabShowFlag()%>">                                                                                            
                            <div class="form-group">                                    
                                <label class="col-lg-3 control-label">Logo</label>
                                <div class="col-lg-2">
                                <% if (!pushAllowLogo.isEmpty() && pushAllowLogo.equalsIgnoreCase("yes")) {%>
                                <input type="checkbox" class="" id="pushAllowLogo" name="pushAllowLogo" value="pushAllowLogoEnable" style="margin: 2%" checked>
                                <%} else {%>
                                <input type="checkbox" class="" id="pushAllowLogo" name="pushAllowLogo" value="pushAllowLogoEnable" style="margin: 2%">
                                <%}%>
                                <label class=" control-label" style="margin-right: 1%">Allow Logo</label>
                                </div>                                    
                            </div>                                                                                                                                        
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Message Character</label>
                                <% 
                                    boolean msg160 = false;
                                    boolean msg340 = false;
                                    boolean msg520 = false;
                                    if (pushMessageChar != null && !pushMessageChar.isEmpty()) {
                                        if(pushMessageChar.equalsIgnoreCase("160")){
                                            msg160 = true;
                                        }
                                        if(pushMessageChar.equalsIgnoreCase("340")){
                                            msg340 = true;
                                        }
                                        if(pushMessageChar.equalsIgnoreCase("520")){
                                            msg520 = true;
                                        }
                                    }
                                %>
                                <div class="col-lg-2">                                       
                                    <% if (msg160) {%>
                                    <input type="radio" class="" id="pushMessageLength" name="pushMessageLength" value="160" style="margin: 2%" checked>
                                    <%} else {%>
                                    <input type="radio" class="" id="pushMessageLength" name="pushMessageLength" value="160" style="margin: 2%">
                                    <%}%>
                                    <label class=" control-label" style="margin-right: 1%">160 Character</label>
                                </div>
                                <div class="col-lg-2">                                       
                                    <% if (msg340) {%>
                                    <input type="radio" class="" id="pushMessageLength" name="pushMessageLength" value="340" style="margin: 2%" checked>
                                    <%} else {%>
                                    <input type="radio" class="" id="pushMessageLength" name="pushMessageLength" value="340" style="margin: 2%">
                                    <%}%>
                                    <label class=" control-label" style="margin-right: 1%">340 Character</label>
                                </div>
                                <div class="col-lg-2">                                       
                                    <% if (msg520) {%>
                                    <input type="radio" class="" id="pushMessageLength" name="pushMessageLength" value="520" style="margin: 2%" checked>
                                    <%} else {%>
                                    <input type="radio" class="" id="pushMessageLength" name="pushMessageLength" value="520" style="margin: 2%">
                                    <%}%>
                                    <label class=" control-label" style="margin-right: 1%">520 Character</label>
                                </div>                                                                        
                            </div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" >Ad Per Day Count</label>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">#</span>
                                        <input type="text" class="form-control" id="pushAdPerDayCount" name="pushAdPerDayCount" placeholder="Per Day Count" onkeypress="return isNumberKey(event)" value="<%=pushCountPerDay%>">                                    
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" >Credit Deduction Per Ad</label>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" class="form-control" id="pushAdPerCreditDeduction" name="pushAdPerCreditDeduction" placeholder="Credit Per Ad" onkeypress="return isNumberKey(event)" value="<%=pushCreditDeduction%>">                                    
                                    </div>
                                </div>
                            </div>        
                            <a  class="btn btn-success btn-xs"  onclick="showBillingRate()" style="margin-left: 26%"><i class="fa fa-backward"></i> Back</a>
                            <a  class="btn btn-success btn-xs"  onclick="showEmailAdDetails()"> Next <i class="fa fa-forward"></i></a>                                    
                        </form>
                    </div>
                
                
                    <!-- Rate Tab -->
                    <div  class="tab-pane fade" id="apRate">                                
                        <form class="form-horizontal" id="edit_ap_form" name="edit_ap_form" role="form">                            
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Message Character</label>
                                <% 
                                    boolean isEmailMsg160 = false;
                                    boolean isEmailMsg340 = false;
                                    boolean isEmailMsg520 = false;
                                    if (emailMessageChar != null && !emailMessageChar.isEmpty()) {
                                        if(emailMessageChar.equalsIgnoreCase("520")){
                                            isEmailMsg160 = true;
                                        }
                                        if(emailMessageChar.equalsIgnoreCase("740")){
                                            isEmailMsg340 = true;
                                        }
                                        if(emailMessageChar.equalsIgnoreCase("960")){
                                            isEmailMsg520 = true;
                                        }
                                    }
                                %>
                                <div class="col-lg-2">                                       
                                    <% if (isEmailMsg160) {%>
                                    <input type="radio" class="" id="emailMessageLength" name="emailMessageLength" value="520" style="margin: 2%" checked>
                                    <%} else {%>
                                    <input type="radio" class="" id="emailMessageLength" name="emailMessageLength" value="520" style="margin: 2%">
                                    <%}%>
                                    <label class=" control-label" style="margin-right: 1%">520 Character</label>
                                </div>
                                <div class="col-lg-2">                                       
                                    <% if (isEmailMsg340) {%>
                                    <input type="radio" class="" id="emailMessageLength" name="emailMessageLength" value="740" style="margin: 2%" checked>
                                    <%} else {%>
                                    <input type="radio" class="" id="emailMessageLength" name="emailMessageLength" value="740" style="margin: 2%">
                                    <%}%>
                                    <label class=" control-label" style="margin-right: 1%">740 Character</label>
                                </div>
                                <div class="col-lg-2">                                       
                                    <% if (isEmailMsg520) {%>
                                    <input type="radio" class="" id="emailMessageLength" name="emailMessageLength" value="960" style="margin: 2%" checked>
                                    <%} else {%>
                                    <input type="radio" class="" id="emailMessageLength" name="emailMessageLength" value="960" style="margin: 2%">
                                    <%}%>
                                    <label class=" control-label" style="margin-right: 1%">960 Character</label>
                                </div>                                                                        
                            </div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" >Ad Per Day Count</label>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">#</span>
                                        <input type="text" class="form-control" id="emailAdPerDayCount" name="emailAdPerDayCount" placeholder="Per Day Count" onkeypress="return isNumberKey(event)" value="<%=emailCountPerDay%>">                                    
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" >Credit Deduction Per Ad</label>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" class="form-control" id="emailAdCreditDeduction" name="emailAdCreditDeduction" placeholder="Credit Per Ad" onkeypress="return isNumberKey(event)" value="<%=emailCreditDeduction%>">                                    
                                    </div>
                                </div>
                            </div>                                          
                            <a  class="btn btn-success btn-xs"  onclick="showPushAdDetails()" style="margin-left: 26%"><i class="fa fa-backward"></i> Back</a>
                            <a  class="btn btn-success btn-xs"  onclick="showPDFAdDetails()"> Next <i class="fa fa-forward"></i></a>
                        </form>
                    </div>
                    <!-- Rate tab end -->

                    <!-- Tab for AP-->
                    
                    <!-- Tab for flat price -->
                    <div  class="tab-pane fade" id="flatRate">                                
                        <form class="form-horizontal" id="edit_feature_form" name="edit_feature_form" role="form">                            
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Image Size</label>
                                <% 
                                    boolean isAdFullPage = false;
                                    boolean isAdHalfPage = false;
                                    boolean isAdOneFourthPage = false;
                                    if (pdfAdImageLength != null && !pdfAdImageLength.isEmpty()) {
                                        if(pdfAdImageLength.equalsIgnoreCase("1")){
                                            isAdFullPage = true;
                                        }
                                        if(pdfAdImageLength.equalsIgnoreCase("1/2")){
                                            isAdHalfPage = true;
                                        }
                                        if(pdfAdImageLength.equalsIgnoreCase("1/4")){
                                            isAdOneFourthPage = true;
                                        }
                                    }
                                %>
                                <div class="col-lg-2">                                       
                                    <% if (isAdFullPage) {%>
                                    <input type="radio" class="" id="pdfAdPageLength" name="pdfAdPageLength" value="1" style="margin: 2%" checked>
                                    <%} else {%>
                                    <input type="radio" class="" id="pdfAdPageLength" name="pdfAdPageLength" value="1" style="margin: 2%">
                                    <%}%>
                                    <label class=" control-label" style="margin-right: 1%">Full Page</label>
                                </div>
                                <div class="col-lg-2">                                       
                                    <% if (isAdHalfPage) {%>
                                    <input type="radio" class="" id="pdfAdPageLength" name="pdfAdPageLength" value="1/2" style="margin: 2%" checked>
                                    <%} else {%>
                                    <input type="radio" class="" id="pdfAdPageLength" name="pdfAdPageLength" value="1/2" style="margin: 2%">
                                    <%}%>
                                    <label class=" control-label" style="margin-right: 1%">1/2 Page</label>
                                </div>
                                <div class="col-lg-2">                                       
                                    <% if (isAdOneFourthPage) {%>
                                    <input type="radio" class="" id="pdfAdPageLength" name="pdfAdPageLength" value="1/4" style="margin: 2%" checked>
                                    <%} else {%>
                                    <input type="radio" class="" id="pdfAdPageLength" name="pdfAdPageLength" value="1/4" style="margin: 2%">
                                    <%}%>
                                    <label class=" control-label" style="margin-right: 1%">1/4 Page</label>
                                </div>                                                                        
                            </div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" >Ad Per Day Count</label>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">#</span>
                                        <input type="text" class="form-control" id="pdfAdPerDayCount" name="pdfAdPerDayCount" placeholder="Per Day Count" onkeypress="return isNumberKey(event)" value="<%=pdfAdCountPerDay%>">                                    
                                    </div>
                                </div>
                            </div>
                            <div class="form-group"> 
                                <label class="col-lg-3 control-label" >Credit Deduction Per Ad</label>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" class="form-control" id="pdfAdCreditDeduction" name="pdfAdCreditDeduction" placeholder="Credit Per Ad" onkeypress="return isNumberKey(event)" value="<%=pdfAdCreditDeduction%>">                                    
                                    </div>
                                </div>
                            </div>                                          
                            <a  class="btn btn-success btn-xs"  onclick="showEmailAdDetails()" style="margin-left: 26%"><i class="fa fa-backward"></i> Back</a>                            
                        </form>
                    </div>                          
                </div><br><br>
            </div>                                                    
        </div>                                           
    </div>
</div>

<div id="rejectPackage" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="rejectPackageModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 id="rejectPackageModal">Reject package</h4>
            </div>          
            <div class="modal-body">
                <div class="row-fluid">
                    <form class="form-horizontal" id="rejectPackageForm">
                        <fieldset>
                            <div class="control-group">
                                <label class="control-label col-lg-2"  for="partnername">Reason</label>
                                <div class="controls col-lg-10">
                                    <input type="hidden"  id="_packageName" name="_packageName" >
                                    <input type="hidden"  id="_packagestatus" name="_packagestatus" >
                                    <textarea id="reason" name="reason" class="form-control" rows="4"></textarea>
                                </div>
                            </div>                            
                        </fieldset>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div id="edit-partner-result"></div>
                <button class="btn btn-info btn-xs" data-dismiss="modal" >Close</button>
                <button class="btn btn-success btn-xs" onclick="rejectPackageRequest()" id="addPartnerButtonE">Reject request</button>
            </div>
        </div>
    </div>
</div>                            

<script>
    $(document).ready(function () {
        $("#visible").select2();
    });
</script>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="dist/js/sb-admin-2.js"></script>
<script src="./select2/select2.js"></script>
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>
<script src="js/bootbox.min.js" type="text/javascript"></script>  
</body>
</html>                
<%@include file="footer.jsp" %>
