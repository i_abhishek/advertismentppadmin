package com.mollatech.PPadmin.promocode;

import com.mollatech.serviceguard.nucleus.db.connector.management.PromocodeManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "RejectPromoRequest", urlPatterns = { "/RejectPromoRequest" })
public class RejectPromoRequest extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Promo code rejected successfully!!!";
        PrintWriter out = response.getWriter();
        String promoId = request.getParameter("_promoId");
        String reqStatus = request.getParameter("_promostatus");
        String reason = request.getParameter("reason");
        int ireqStatus = 0;
        int ipromoCodeId = 0;
        if (reqStatus != null) {
            ireqStatus = Integer.parseInt(reqStatus);
        }
        if (promoId != null) {
            ipromoCodeId = Integer.parseInt(promoId);
        }
        int a = -1;
        PromocodeManagement pm = new PromocodeManagement();
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        if (SessionId != null) {
            a = pm.RejectPromocodeRequest(SessionId, channelId, ipromoCodeId, ireqStatus, reason);
            if (a == 0) {
                result = "success";
                message = "Promo code rejected successfully!!!";
            } else if (a == 3) {
                result = "error";
                message = "Promo code already rejected  !!!";
            } else {
                result = "error";
                message = "Promo code rejection rejected !!!";
            }
        }
        try {
            json.put("result", result);
            json.put("message", message);
            return;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
            out.close();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
