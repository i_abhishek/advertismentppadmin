/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var waiting;
function pleasewait() {
    waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');
    //waiting = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"></div></div></div>');
    waiting.modal();
}

function bootboxmodelshow(content) {
    var popup = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' +
            '<button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>' +
            content +
            '<a class="btn btn-info btn-xs" data-dismiss="modal" class="close"> OK</a></div></div></div></div>');
    popup.modal();
}

function Alert4Users(msg) {
    bootboxmodelshow("<h5>" + msg + "</h5>");
}
function strCompare(a, b)
{
    return (a < b ? -1 : (a > b ? 1 : 0));
}
function showAlert(message, type, closeDelay) {
    if ($("#alerts-container").length == 0) {
        // alerts-container does not exist, create it
        $("body")
                .append($('<div id="container" style="' +
                        'width: %; margin-left: 65%; margin-top: 10%;">'));
    }
    // default to alert-info; other options include success, warning, danger
    type = type || "info";

    // create the alert div
    var alert = $('<div class="alert alert-' + type + ' fade in">')
            .append(
                    $('<button type="button" class="close" data-dismiss="alert">')
                    .append("&times;")
                    )
            .append(message);

    // add the alert div to top of alerts-container, use append() to add to bottom
    $("#alerts-container").prepend(alert);

    // if closeDelay was passed - set a timeout to close the alert
    if (closeDelay)
        window.setTimeout(function () {
            alert.alert("close")
        }, closeDelay);

}

function createPackage() {
    var s = './CreatePackage';
    var name = document.getElementById("_packageName").value;
    var price = document.getElementById("_packagePrice").value;
    var duration = document.getElementById("_packageDuration").value;
    var mode = document.getElementById("_paymentMode").value;
    var packageDesc = document.getElementById("packageDesc").value;
    if (name.trim() === '' || name.indexOf(' ') >= 0) {
        Alert4Users("<h3><font color=red>" + "Enter package Name" + "</font></h3>");
        return;
    }
    if (price.trim() === '' || price.indexOf(' ') >= 0) {
        Alert4Users("<h3><font color=red>" + "Enter package price" + "</font></h3>");
        return;
    }
    if (duration.trim() === '' || duration.indexOf(' ') >= 0) {
        Alert4Users("<h3><font color=red>" + "Please select duration" + "</font></h3>");
        return;
    }
    if (mode.trim() === '' || mode.indexOf(' ') >= 0) {
        Alert4Users("<h3><font color=red>" + "Please select payment mode" + "</font></h3>");
        return;
    }
    if (packageDesc.length < 1) {
        Alert4Users("<h3><font color=red>" + "Please add meaningful description for package" + "</font></h3>");
        return;
    }
    pleasewait();
    $.ajax({
        type: 'POST',
        url: s,
        datatype: 'json',
        data: $("#register_form").serialize(),
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                waiting.modal('hide');
                Alert4Users("<span><font color=red>" + data.message + "</font></span>");
                //showAlert(data.message,"danger",3000);
            } else if (strCompare(data.result, "success") === 0) {
                waiting.modal('hide');
                //showAlert(data.message,"success",2000);
                Alert4Users("<span><font color=blue>" + data.message + "</font></span>");
                setTimeout(function () {
                    window.location.href = "./editPackage.jsp?_edit=" + data.retValue;
                }, 3000);
            }
        }
    });
}

function editPackage(val) {
    var partId = $("#visibleTo").val();
    var s = './EditPackage?packageId=' + val + "&_partnerId=" + partId;
    pleasewait();
    $.ajax({
        type: 'POST',
        url: s,
        datatype: 'json',
        data: $("#edit_package_form").serialize(),
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                waiting.modal('hide');
                Alert4Users("<span><font color=red>" + data.message + "</font></span>");
                //showAlert(data.message,"danger",3000);
            } else if (strCompare(data.result, "success") === 0) {
                waiting.modal('hide');
                //showAlert(data.message,"success",2000);
                Alert4Users("<span><font color=blue>" + data.message + "</font></span>");
//                setTimeout(function() {
//                    window.location.href = "./package.jsp";
//                }, 3000);   
                $('.nav li.active').next('li').removeClass('disabled');
                $('.nav li.active').next('li').find('a').attr("data-toggle", "tab");
                $('#addPackageTab a[href="#apRate"]').tab('show');
            }
        }
    });
}
function showAPRate() {
    $('#addPackageTab a[href="#apRate"]').tab('show');
}
function showAPIThrottling() {
    $('#addPackageTab a[href="#apiThrottling"]').tab('show');
}
function showRate() {
    $('#addPackageTab a[href="#rate"]').tab('show');
}
function showSlabPrice() {
    $('#addPackageTab a[href="#promoCode"]').tab('show');
}
function showTierPrice() {
    $('#addPackageTab a[href="#tieringPrice"]').tab('show');
}
function showSecurityAndAlerts() {
    $('#addPackageTab a[href="#alerts"]').tab('show');
}
function showfeature() {
    $('#addPackageTab a[href="#feature"]').tab('show');
}
function editAPPrice(val) {
    var partId = $("#visibleTo").val();
    var s = './EditAPPrice?packageId=' + val + '&visibilityPartner=' + partId;
    var selectAccesspoint = document.getElementById("_Accesspoint12").value;
    if (selectAccesspoint == '-1') {
        Alert4Users("<font color=red>" + "Please select atleast One accesspoint" + "</font>");
        return;
    }
    var mainCredits = document.getElementById("mainCreditForAP").value;
    var freeCreditForAP = document.getElementById("freeCreditForAP").value;
    if ((mainCredits.trim() === '' || mainCredits.indexOf(' ') >= 0) || (freeCreditForAP.trim() === '' || freeCreditForAP.indexOf(' ') >= 0)) {
        Alert4Users("<font color=red>" + "Credits should not be empty" + "</font>");
        return;
    }
    if (isNaN(mainCredits)) {
        Alert4Users("<font color=red>" + "Main credit should be in digit" + "</font>");
        return;
    }
    if (isNaN(freeCreditForAP)) {
        Alert4Users("<font color=red>" + "Free credit should be in digit" + "</font>");
        return;
    }

    pleasewait();
    $.ajax({
        type: 'POST',
        url: s,
        datatype: 'json',
        data: $("#edit_ap_form").serialize(),
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                waiting.modal('hide');
                Alert4Users("<span><font color=red>" + data.message + "</font></span>");
            } else if (strCompare(data.result, "success") === 0) {
                waiting.modal('hide');
                Alert4Users("<span><font color=blue>" + data.message + "</font></span>");
                $('.nav li.active').next('li').removeClass('disabled');
                $('.nav li.active').next('li').find('a').attr("data-toggle", "tab");
                $('#addPackageTab a[href="#flatRate"]').tab('show');
            }
        }
    });
}

function editSlabPrice(value) {
//    document.getElementById("SlabPricing").disabled = false;
//    var partId = $("#visibleTo").val();
//    var s = './EditSlabPrice?packageId=' + value + '&visibilityPartner=' + partId;
//    pleasewait();
//    $.ajax({
//        type: 'POST',
//        url: s,
//        datatype: 'json',
//        data: $("#edit_sp_form").serialize(),
//        success: function (data) {
//            document.getElementById("SlabPricing").disabled = true;
//            if (strCompare(data.result, "error") === 0) {
//                waiting.modal('hide');
//                Alert4Users("<span><font color=red>" + data.message + "</font></span>");              
//            } else if (strCompare(data.result, "success") === 0) {
//                waiting.modal('hide');                
//                Alert4Users("<span><font color=blue>" + data.message + "</font></span>");             
//                $('.nav li.active').next('li').removeClass('disabled');
//                $('.nav li.active').next('li').find('a').attr("data-toggle", "tab");
//                $('#addPackageTab a[href="#alerts"]').tab('show');
//            }
//        }
//    });

    var enableStatus = document.getElementById("SlabPricing").value;
    if (enableStatus === "Enable") {
        var range3;
        var rangeFirst1 = (document.getElementById("rangeFrom1").value);
        var message = null;
        for (var i = 1; i <= 5; i++) {
            if (rangeFirst1 != "1") {
                message = "First range value must be start from 1";
                Alert4Users("<span><font color=red>" + message + "</font></span>");
                break;
                return;
            }
            var temp = i + 1;
            var range1 = (document.getElementById("rangeFrom" + i).value);
            var range2 = (document.getElementById("rangeTo" + i).value);

            var price = (document.getElementById("price" + i).value);
            if (range1 !== "" && range2 !== "" && price === "") {
                message = "Empty price value is not allowed";
                Alert4Users("<span><font color=red>" + message + "</font></span>");
                return;
            }

            if (i <= 4) {
                range3 = (document.getElementById("rangeFrom" + (temp)).value);
            } else {
                range2 = (document.getElementById("rangeFrom5").value);
                range3 = (document.getElementById("rangeTo" + i).value);
            }
            var price1 = document.getElementById("price" + i).value;
            var double1 = Number(range1);
            var double2 = Number(range2);
            var double3 = Number(range3);

            if (range1 == "" && range2 == 0 && range3 != 0) {
                message = "Please Enter Slab details in sequence";
                Alert4Users("<span><font color=red>" + message + "</font></span>");
                break;
                return;
            }
            if ((range1 == "" && range2 == "")) {
                if (i === 5) {
                    successSlabPrice(value);
                }
                continue;
            }
            if (!!range1 && !!range2) {
                if ((double3 <= double2) && (double2 != 0 && double3 != 0)) {
                    message = "From value must be greater than previous To value";
                    Alert4Users("<span><font color=red>" + message + "</font></span>");
                    break;
                    return;
                }
            } else {
                message = "Please Fill Both the Values From and To.";
                Alert4Users("<span><font color=red>" + message + "</font></span>");
                break;
            }
            if (i !== 5) {
                if (double1 < double2) {

                } else {
                    message = "To value must be greater than From Value";
                    Alert4Users("<span><font color=red>" + message + "</font></span>");
                    break;
                    return;
                }
            }
            if (i === 5) {
                successSlabPrice(value);
            }
        }
    } else {
        successSlabPrice(value);
    }
}

function editSecurityAndAlert(value, featureFlag) {
    var partId = $("#visibleTo").val();
    var s = './EditSecurityAndAlert?packageId=' + value + '&visibilityPartner=' + partId;
    pleasewait();
    $.ajax({
        type: 'POST',
        url: s,
        datatype: 'json',
        data: $("#edit_seandart_form").serialize(),
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                waiting.modal('hide');
                Alert4Users("<span><font color=red>" + data.message + "</font></span>");
            } else if (strCompare(data.result, "success") === 0) {
                if (featureFlag === 'false') {
                    editFeature(value, featureFlag);
                } else {
                    waiting.modal('hide');
                    Alert4Users("<span><font color=blue>" + data.message + "</font></span>");

                    $('.nav li.active').next('li').removeClass('disabled');
                    $('.nav li.active').next('li').find('a').attr("data-toggle", "tab");
                    $('#addPackageTab a[href="#feature"]').tab('show');
                }
            }
        }
    });
}

function editFeature(value, featureFlag) {
    var partId = $("#visibleTo").val();
    var s = './EditFeature?packageId=' + value + '&visibilityPartner=' + partId;
    if (featureFlag !== 'false') {
        pleasewait();
    }
    $.ajax({
        type: 'POST',
        url: s,
        datatype: 'json',
        data: $("#edit_feature_form").serialize(),
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                waiting.modal('hide');
                Alert4Users("<span><font color=red>" + data.message + "</font></span>");
                //showAlert(data.message,"danger",3000);
            } else if (strCompare(data.result, "success") === 0) {
                waiting.modal('hide');
                //showAlert(data.message,"success",2000);
                Alert4Users("<span><font color=blue>" + data.message + "</font></span>");
                setTimeout(function () {
                    window.location.href = "./package.jsp";
                }, 3000);
            }
        }
    });
}



//function rejectPackageRequest() {
//
//    var reason = document.getElementById("reason").value;
//    var id = document.getElementById("_packageName").value;
//    var status = document.getElementById("_packagestatus").value;
//    if (reason != "Select") {
//        $('#rejectPackage').modal('hide');
//        var s = './RejectPackageRequest?packageName=' + id + '&packagestatus=' + status + '&reason=' + reason;
//
//        pleasewait();
//        $.ajax({
//            type: 'POST',
//            url: s,
//            datatype: 'json',
//            data: $("#rejectPackageForm").serialize(),
//            success: function (data) {
//                if (strCompare(data.result, "error") === 0) {
//                    waiting.modal('hide');
//                    Alert4Users("<span><font color=red>" + data.message + "</font></span>");
//
//                } else if (strCompare(data.result, "success") === 0) {
//                    waiting.modal('hide');
//                    Alert4Users("<span><font color=blue>" + data.message + "</font></span>");
//                    setTimeout(function () {
//                        window.location.href = "./packageRequest.jsp";
//                    }, 3000);
//                }
//            }
//        });
//    } else {
//        Alert4Users("<h3><span><font color=red>Please Select Reason</font></span></h3>");
//    }
//}


function rejectPackageRequest() {
    var reason = document.getElementById("reason").value;
    if (reason === "Select") {
        Alert4Users("<h5><span><font color=red>Please Select Any Reason</font></span><h5>");
    } else {
        bootbox.confirm({
            message: ("<h4><font color=red>" + "Are you sure ?" + "</font><h4>"),
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success btn-xs'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger btn-xs'
                }
            },
            callback: function (result) {
                if (result === true) {
                    var reason = document.getElementById("reason").value;
                    var id = document.getElementById("_packageName").value;
                    var status = document.getElementById("_packagestatus").value;
                    if (reason != "Select") {
                        $('#rejectPackage').modal('hide');
                        var s = './RejectPackageRequest?packageName=' + id + '&packagestatus=' + status + '&reason=' + reason;
                        pleaseWaitProcess();
                        $.ajax({
                            type: 'POST',
                            url: s,
                            datatype: 'json',
                            data: $("#rejectPackageForm").serialize(),
                            success: function (data) {
                                if (strCompare(data.result, "error") === 0) {
                                    waiting.modal('hide');
                                    Alert4Users("<span><font color=red>" + data.message + "</font></span>");
                                } else if (strCompare(data.result, "success") === 0) {
                                    waiting.modal('hide');
                                    Alert4Users("<span><font color=blue>" + data.message + "</font></span>");
                                    setTimeout(function () {
                                        window.location.href = "./packageRequest.jsp";
                                    }, 3000);
                                }
                            }
                        });
                    } else {
                        Alert4Users("<h3><span><font color=red>Please Select Reason</font></span></h3>");
                    }
                }
            }
        });
    }
}

function pleaseWaitProcess() {
    waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');
    waiting.modal();
}



function changePackageStatus(status, packageName) {
    var s = './changeStatusPackage?_status=' + status + '&_packageName=' + packageName;
    pleasewait();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                waiting.modal('hide');
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                setTimeout(function () {
                    window.location = "./packageForChecker.jsp";
                }, 3000);
            } else if (strCompare(data._result, "success") === 0) {
                waiting.modal('hide');
                Alert4Users("<font color=blue>" + data._message + "</font>");
                setTimeout(function () {
                    window.location = "./packageForChecker.jsp";
                }, 3000);
            }
        }
    });
}

function approvePackage(status, packageName) {
    var s = './approvePackage?_status=' + status + '&_packageName=' + packageName;
    pleasewait();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                waiting.modal('hide');
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                setTimeout(function () {
                    window.location = "./packageRequest.jsp";
                }, 3000);
            } else if (strCompare(data._result, "success") === 0) {
                waiting.modal('hide');
                Alert4Users("<font color=blue>" + data._message + "</font>");
                setTimeout(function () {
                    window.location = "./packageRequest.jsp";
                }, 3000);
            }
        }
    });
}

function rejectPackagemodal(_packagestatus, _packagename) {
    document.getElementById("rejectPackageModal").innerHTML = "Reject " + _packagename;

    document.getElementById("_packageName").value = _packagename;
    document.getElementById("_packagestatus").value = _packagestatus;

    $('#rejectPackage').modal();
}

function checkAvailability() {
    var value = $("#_packageName").val();
    var s = './CheckAvailability?__packageName=' + value;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data.result, "error") == 0) {
                $('#checkAvailability-result').html("<span><font color=red>" + data.message + "</font></span></small>");
            } else {
                $('#checkAvailability-result').html("<span><font color=blue>" + data.message + "</font></span></small>");
            }
        }
    });
}

function sendToChecker(status, packageName) {
    var s = './SendToChecker?_status=' + status + '&_packageName=' + packageName;
    pleasewait();
    $.ajax({
        type: 'POST',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                waiting.modal('hide');
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                setTimeout(function () {
                    window.location = "./package.jsp";
                }, 3000);
            } else if (strCompare(data._result, "success") === 0) {
                waiting.modal('hide');
                Alert4Users("<font color=blue>" + data._message + "</font>");
                setTimeout(function () {
                    window.location = "./package.jsp";
                }, 3000);
            }
        }
    });
}

function rejectionDetails(rejectionDetails, packageName) {
    document.getElementById("rejectPackageModal").innerHTML = "Rejection details of " + packageName;
    document.getElementById("_rejectionDetails").value = rejectionDetails;
    $('#viewRejection').modal();
}

function testEditPackage(val) {
    var enableStatus = document.getElementById("LatePenaltyCharge").value;
    if (enableStatus === "Enable") {
        var sDay1 = document.getElementById("LatePenaltyChargeSDay1").value;
        var eDay1 = document.getElementById("LatePenaltyChargeEDay1").value;
        var sDay2 = document.getElementById("LatePenaltyChargeSDay2").value;
        var eDay2 = document.getElementById("LatePenaltyChargeEDay2").value;
        var double1 = Number(sDay1);
        var double2 = Number(eDay1);
        var double3 = Number(sDay2);
        var double4 = Number(eDay2);
        var msg = null;
        var i = 0;
        //alert(double1+" "+double2+" "+double3+" "+double4);
        if (!!sDay1 && !!eDay1) {
            if (double1 < 1) {
                i = i + 1;
                msg = "Starting day must be greater than 0";
                Alert4Users("<span><font color=red>" + msg + "</font></span>");
            }
            if (sDay1 >= eDay1) {
                i = i + 1;
                msg = "end day  must be greater than start day";
                Alert4Users("<span><font color=red>" + msg + "</font></span>");
            }
        } else {
            i = i + 1;
            msg = "Insert start day and end day values in sequence";
            Alert4Users("<span><font color=red>" + msg + "</font></span>");
        }
        if (double3 > 0) {
            if (eDay1 >= sDay2) {
                i = i + 1;
                msg = "start day 2 must be greater than previous end day";
                Alert4Users("<span><font color=red>" + msg + "</font></span>");
            }
        }

        if (double1 > double2 || double3 > double4) {
            i = i + 1;
            msg = "start day must be less than end day";
            Alert4Users("<span><font color=red>" + msg + "</font></span>");
        }
        if (i === 0) {
            editPackage(val);
        }
    } else {
        editPackage(val);
    }
}

function showTierPrice() {
    $('#addPackageTab a[href="#tieringPrice"]').tab('show');
}

function successSlabPrice(value) {

    pleasewait();
    document.getElementById("SlabPricing").disabled = false;
    var partId = $("#visibleTo").val();
    var s = './EditSlabPrice?packageId=' + value + '&visibilityPartner=' + partId;
    $.ajax({
        type: 'POST',
        url: s,
        datatype: 'json',
        data: $("#edit_sp_form").serialize(),
        success: function (data) {
            document.getElementById("SlabPricing").disabled = false;
            if (strCompare(data.result, "error") === 0) {
                waiting.modal('hide');
//                showAlert(data.message, "danger", 3000);
                Alert4Users("<span><font color=red>" + data.message + "</font></span>");
                // showAlert(data.message, "danger", 3000);
            } else if (strCompare(data.result, "success") === 0) {
                waiting.modal('hide');
//                showAlert(data.message,"success",3000);
                Alert4Users("<span><font color=blue>" + data.message + "</font></span>");
//                setTimeout(function () {
//                    window.location.href = "./package.jsp";
//                }, 3000);
                $('.nav li.active').next('li').removeClass('disabled');
                $('.nav li.active').next('li').find('a').attr("data-toggle", "tab");
                $('#addPackageTab a[href="#tieringPrice"]').tab('show');
            }
        }
    });
}

function editTierPrice(value) {
    var enableStatus = document.getElementById("TieringPricing").value;
    if (enableStatus === "Enable") {
        var range3;
        var rangeFirst1 = (document.getElementById("rangeFrom1").value);
        var message = null;
        for (var i = 1; i <= 5; i++) {
            if (rangeFirst1 != "1") {
                message = "From value must be starting from 1";
                Alert4Users("<span><font color=red>" + message + "</font></span>");
                break;
            }
            var temp = i + 1;
            var range1 = (document.getElementById("rangeFrom" + i).value);
            var range2 = (document.getElementById("rangeTo" + i).value);

            var price = (document.getElementById("price" + i).value);
            if (range1 !== "" && range2 !== "" && price === "") {
                message = "Empty price value is not allowed";
                Alert4Users("<span><font color=red>" + message + "</font></span>");
                return;
            }

            if (i <= 4) {
                range3 = (document.getElementById("rangeFrom" + (temp)).value);
            } else {
                range3 = (document.getElementById("rangeTo5").value);
            }
            var price1 = document.getElementById("price" + i).value;
            var double1 = Number(range1);
            var double2 = Number(range2);
            var double3 = Number(range3);
            if (range1 == "" && range2 == 0 && range3 != 0) {
                message = "Please Enter Tier details in sequence";
                Alert4Users("<span><font color=red>" + message + "</font></span>");
                break;
            }
            if ((range1 == "" && range2 == "")) {
                if (i === 5) {
                    successTierPrice(value);
                }
                continue;
            }
            if (!!range1 && !!range2) {
                if ((double3 <= double2) && (double2 != 0 && double3 != 0)) {
                    message = "From value must be greater than previous To value";
                    Alert4Users("<span><font color=red>" + message + "</font></span>");
                    break;
                }
            } else {
                message = "Please Fill Both the Values From and To.";
                Alert4Users("<span><font color=red>" + message + "</font></span>");
                break;
            }
            if (double1 < double2) {
                if (i === 5) {
                    successTierPrice(value);
                }
            } else {
                message = "To value must be greater than From Value";
                Alert4Users("<span><font color=red>" + message + "</font></span>");
                break;
            }
        }
    } else {
        successTierPrice(value);
    }
}
function successTierPrice(value) {

    document.getElementById("TieringPricing").disabled = false;
    var partId = $("#visibleTo").val();
    var s = './EditTierPrice?packageId=' + value + '&visibilityPartner=' + partId;
    pleasewait();
    $.ajax({
        type: 'POST',
        url: s,
        datatype: 'json',
        data: $("#edit_tp_form").serialize(),
        success: function (data) {
            document.getElementById("TieringPricing").disabled = false;
            if (strCompare(data.result, "error") === 0) {
                waiting.modal('hide');
                Alert4Users("<span><font color=red>" + data.message + "</font></span>");
            } else if (strCompare(data.result, "success") === 0) {
                waiting.modal('hide');
                Alert4Users("<span><font color=blue>" + data.message + "</font></span>");
                $('.nav li.active').next('li').removeClass('disabled');
                $('.nav li.active').next('li').find('a').attr("data-toggle", "tab");
                $('#addPackageTab a[href="#alerts"]').tab('show');
            }
        }
    });
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode !== 46 && charCode > 31
            && (charCode < 48 || charCode > 57)) {
        var showMessage = 'Enter Numeric Or Decimal Values Only';
        Alert4Users("<span><font color=red>" + showMessage + "</font></span>");
        return false;
    }
    return true;
}
function isNumericKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if ((charCode < 48 || charCode > 57)) {
        var showMessage = 'Enter Numeric Values Only';
        Alert4Users("<span><font color=red>" + showMessage + "</font></span>");
        return false;
    }
    return true;
}

function setDefault(bucketId) {
    var s = './SetDefaultPackage?packageId=' + bucketId;
    pleasewait();
    $.ajax({
        type: 'POST',
        url: s,
        datatype: 'json',
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {
                waiting.modal('hide');
                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
            } else if (strCompare(data.result, "success") === 0) {
                waiting.modal('hide');
                Alert4Users("<font color=blue>" + data._message + "</font>");
                setTimeout(function () {
                    window.location = "./package.jsp";
                }, 3000);
            }
        }
    });
}

function editAPIThrottling(val) {
    var partId = $("#visibleTo").val();
    var s = './EditAPIThrottling?packageId=' + val + '&visibilityPartner=' + partId;
    var selectAccesspoint = document.getElementById("_accesspointAPIThrottling").value;
    var throttlingSetting = document.getElementById("throttlingSetting").value;
    if (throttlingSetting === 'Enable' && selectAccesspoint == '-1') {
        Alert4Users("<font color=red>" + "Please select atleast One accesspoint" + "</font>");
        return;
    }
    pleasewait();
    $.ajax({
        type: 'POST',
        url: s,
        datatype: 'json',
        data: $("#edit_apiThrottling_form").serialize(),
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                waiting.modal('hide');
                Alert4Users("<span><font color=red>" + data.message + "</font></span>");
            } else if (strCompare(data.result, "success") === 0) {
                waiting.modal('hide');
                Alert4Users("<span><font color=blue>" + data.message + "</font></span>");
                $('.nav li.active').next('li').removeClass('disabled');
                $('.nav li.active').next('li').find('a').attr("data-toggle", "tab");
                $('#addPackageTab a[href="#promoCode"]').tab('show');
            }
        }
    });
}

function editFlatPrice(val) {
    var partId = $("#visibleTo").val();
    var s = './EditFlatPrice?packageId=' + val + '&visibilityPartner=' + partId;
    var selectAccesspoint = document.getElementById("_accesspointFlatPrice").value;
    var flatPriceSetting = document.getElementById("flatPriceSetting").value;
    if (flatPriceSetting === 'Enable' && selectAccesspoint == '-1') {
        Alert4Users("<font color=red>" + "Please select atleast One accesspoint" + "</font>");
        return;
    }
    pleasewait();
    $.ajax({
        type: 'POST',
        url: s,
        datatype: 'json',
        data: $("#edit_flatPrice_form").serialize(),
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                waiting.modal('hide');
                Alert4Users("<span><font color=red>" + data.message + "</font></span>");
            } else if (strCompare(data.result, "success") === 0) {
                waiting.modal('hide');
                Alert4Users("<span><font color=blue>" + data.message + "</font></span>");
                $('.nav li.active').next('li').removeClass('disabled');
                $('.nav li.active').next('li').find('a').attr("data-toggle", "tab");
                $('#addPackageTab a[href="#apiThrottling"]').tab('show');
            }
        }
    });
}

function loadPackageModal(packageId, description) {
    $('#packageId').val(packageId);
    if (description != "null") {
        $('#packageInfo').val(description);
    }
    $("#addpackageDecription").modal();
}

function editPackageDescription(val) {
    var s = './EditPackageDescription';
    var selectAccesspoint = document.getElementById("packageInfo").value;
    if (selectAccesspoint.length < 1) {
        Alert4Users("<font color=red>" + "Please add meaningful description for package" + "</font>");
        return;
    }
    pleasewait();
    $.ajax({
        type: 'POST',
        url: s,
        datatype: 'json',
        data: $("#addPackageDescForm").serialize(),
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                waiting.modal('hide');
                Alert4Users("<span><font color=red>" + data.message + "</font></span>");
            } else if (strCompare(data.result, "success") === 0) {
                waiting.modal('hide');
                $("#addpackageDecription").modal('hide')
                Alert4Users("<span><font color=blue>" + data.message + "</font></span>");
            }
        }
    });
}

function viewImageModal(_pdfImage) {          
   pdfImageAdModelShow(_pdfImage);
}

function pdfImageAdModelShow(id,) {
    var s = './showPDFAdImage.jsp?_id='+id;    
    document.getElementById("_adId").value = id;
    $.ajax({
        type: 'GET',
        url: s,        
        success: function (data) {
             $('#imagePDFAd').html(data);
             $("#pdfAdModal").modal();
        }
    });
}

function emailLogoAdModelShow(id) {
    var s = './showEmailAdImage.jsp?_id='+id;    
    document.getElementById("_adId").value = id;
    $.ajax({
        type: 'GET',
        url: s,        
        success: function (data) {
             $('#imageEmailAd').html(data);
             $("#emailAdModal").modal();
        }
    });
}

function rejectPDFAdmodal(_adstatus, _adId) {
    //document.getElementById("rejectPackageModal").innerHTML = "Reject " + _packagename;

    document.getElementById("_adId").value = _adId;
    document.getElementById("_adstatus").value = _adstatus;

    $('#rejectPDFAD').modal();
}

function rejectEmailAdmodal(_adstatus, _adId) {
    //document.getElementById("rejectPackageModal").innerHTML = "Reject " + _packagename;

    document.getElementById("_adId").value = _adId;
    document.getElementById("_adstatus").value = _adstatus;

    $('#rejectEmailAD').modal();
}

function rejectEmailADRequest() {
    var reason = document.getElementById("reason").value;
    if (reason === "Select") {
        Alert4Users("<h5><span><font color=red>Please Select Any Reason</font></span><h5>");
    } else {
        bootbox.confirm({
            message: ("<h4><font color=red>" + "Are you sure ?" + "</font><h4>"),
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success btn-xs'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger btn-xs'
                }
            },
            callback: function (result) {
                if (result === true) {
                    var reason = document.getElementById("reason").value;
                    var id = document.getElementById("_adId").value;
                    var status = document.getElementById("_adstatus").value;
                    if (reason != "Select") {
                        $('#rejectEmailAD').modal('hide');
                        var s = './RejectEmailAdRequest?adId=' + id + '&adstatus=' + status + '&reason=' + reason;
                        pleaseWaitProcess();
                        $.ajax({
                            type: 'POST',
                            url: s,
                            datatype: 'json',
                            data: $("#rejectPackageForm").serialize(),
                            success: function (data) {
                                if (strCompare(data.result, "error") === 0) {
                                    waiting.modal('hide');
                                    Alert4Users("<span><font color=red>" + data.message + "</font></span>");
                                    setTimeout(function () {
                                        window.location.href = "./emailAdRequest.jsp";
                                    }, 4000);
                                } else if (strCompare(data.result, "success") === 0) {
                                    waiting.modal('hide');
                                    Alert4Users("<span><font color=blue>" + data.message + "</font></span>");
                                    setTimeout(function () {
                                        window.location.href = "./emailAdRequest.jsp";
                                    }, 3000);
                                }
                            }
                        });
                    } else {
                        Alert4Users("<h3><span><font color=red>Please Select Reason</font></span></h3>");
                    }
                }
            }
        });
    }
}

function rejectPDFADRequest() {
    var reason = document.getElementById("reason").value;
    if (reason === "Select") {
        Alert4Users("<h5><span><font color=red>Please Select Any Reason</font></span><h5>");
    } else {
        bootbox.confirm({
            message: ("<h4><font color=red>" + "Are you sure ?" + "</font><h4>"),
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success btn-xs'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger btn-xs'
                }
            },
            callback: function (result) {
                if (result === true) {
                    var reason = document.getElementById("reason").value;
                    var id = document.getElementById("_adId").value;
                    var status = document.getElementById("_adstatus").value;
                    if (reason != "Select") {
                        $('#rejectPackage').modal('hide');
                        var s = './RejectPDFAdRequest?adId=' + id + '&adstatus=' + status + '&reason=' + reason;
                        pleaseWaitProcess();
                        $.ajax({
                            type: 'POST',
                            url: s,
                            datatype: 'json',
                            data: $("#rejectPackageForm").serialize(),
                            success: function (data) {
                                if (strCompare(data.result, "error") === 0) {
                                    waiting.modal('hide');
                                    Alert4Users("<span><font color=red>" + data.message + "</font></span>");
                                    setTimeout(function () {
                                        window.location.href = "./pdfAdRequest.jsp";
                                    }, 4000);
                                } else if (strCompare(data.result, "success") === 0) {
                                    waiting.modal('hide');
                                    Alert4Users("<span><font color=blue>" + data.message + "</font></span>");
                                    setTimeout(function () {
                                        window.location.href = "./pdfAdRequest.jsp";
                                    }, 3000);
                                }
                            }
                        });
                    } else {
                        Alert4Users("<h3><span><font color=red>Please Select Reason</font></span></h3>");
                    }
                }
            }
        });
    }
}

function changePDFAdStatus(adId) {
    bootbox.confirm({
            message: ("<h4><font color=red>" + "Are you sure ?" + "</font><h4>"),
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success btn-xs'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger btn-xs'
                }
            },
            callback: function (result) {
                if (result === true) {
                    var s = './ApprovePDFAdRequest?adId=' + adId ;
                    pleasewait();
                    $.ajax({
                        type: 'POST',
                        url: s,
                        dataType: 'json',
                        success: function (data) {
                            if (strCompare(data._result, "error") === 0) {
                                waiting.modal('hide');
                                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                                setTimeout(function () {
                                    window.location = "./pdfAdRequest.jsp";
                                }, 3000);
                            } else if (strCompare(data._result, "success") === 0) {
                                waiting.modal('hide');
                                Alert4Users("<font color=blue>" + data._message + "</font>");
                                setTimeout(function () {
                                    window.location = "./pdfAdRequest.jsp";
                                }, 3000);
                            }
                        }
                    });
                }
            }
        });
    }
    
function changeEmailAdStatus(adId) {
    bootbox.confirm({
            message: ("<h4><font color=red>" + "Are you sure ?" + "</font><h4>"),
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success btn-xs'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger btn-xs'
                }
            },
            callback: function (result) {
                if (result === true) {
                    var s = './ApproveEmailAdRequest?adId=' + adId ;
                    pleasewait();
                    $.ajax({
                        type: 'POST',
                        url: s,
                        dataType: 'json',
                        success: function (data) {
                            if (strCompare(data._result, "error") === 0) {
                                waiting.modal('hide');
                                Alert4Users("<span><font color=red>" + data._message + "</font></span>");
                                setTimeout(function () {
                                    window.location = "./emailAdRequest.jsp";
                                }, 3000);
                            } else if (strCompare(data._result, "success") === 0) {
                                waiting.modal('hide');
                                Alert4Users("<font color=blue>" + data._message + "</font>");
                                setTimeout(function () {
                                    window.location = "./emailAdRequest.jsp";
                                }, 3000);
                            }
                        }
                    });
                }
            }
        });
    }    
    