package com.mollatech.PPadmin.reports;

/**
 *
 * @author mollatech1
 */
public class line {

    String label;

    long value;

    public line(long value, String label) {
        this.label = label;
        this.value = value;
    }

    public line(String label, long value) {
        this.label = label;
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }
}
