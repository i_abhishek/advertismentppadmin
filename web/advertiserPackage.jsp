<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ApprovedAdPackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgApprovedAdPackagedetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgAdvPreApprovalPackagedetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AdvPreApprovalPackageManagement"%>
<%@include file="header.jsp" %>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgBucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement"%>
<%
    if (makerChakerObj != null) {
        if (!makerChakerObj.maker.contains(opObj.getEmailid()) && makerChakerObj.status == GlobalStatus.ACTIVE) {
            response.sendRedirect("oplogout.jsp");
            return;
        }
    }
%>
<script src="js/advertiserPackageOperation.js" type="text/javascript"></script>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Package Details</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">              
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-home"></i><a href="home.jsp"> Home</a> &#47;
                    <i class="fa fa-shopping-cart"></i> Package Details &#47;                                                          
                </div>
                
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover display nowrap" id="dataTables-example">
                        <thead>
                            <tr>
                                <td/>
                                <th>No.</th>
                                <th>Status</th>
                                <th>Package</th>
                                <th>Default</th>
                                <th>Price</th>
                                <th>Duration</th>
                                <!--                                <th>Pay</th>-->
                                <th>Manage</th>
                                <th>Created On</th>
                                <th>Updated On</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                int count = 0;
                                String resultInfo = "No Record Found";
                                String updationDate = "Not Updated yet";
                                AdvPreApprovalPackageManagement um = new AdvPreApprovalPackageManagement();
                                SgAdvPreApprovalPackagedetails[] rDetails = null;

                                int pId = -1;
                                rDetails = um.listpackage(SessionId);
                                if (rDetails != null) {
                                    if (rDetails.length == 0) {
                                        rDetails = null;
                                    }
                                }
                                if (rDetails != null) {
                                    for (int i = 0; i < rDetails.length; i++) {
                                        count++;
                                        boolean fillAllDetailsFlag = false;
                                        SgApprovedAdPackagedetails requestedPackageObj = new ApprovedAdPackageManagement().getReqPackageByName(SessionId, channelId, rDetails[i].getPackageName());
                                        if (requestedPackageObj != null) {
                                            fillAllDetailsFlag = true;
                                        }
                                        String desc = null;
                                        if(rDetails[i].getPackageDescription() != null){
                                            desc = rDetails[i].getPackageDescription().replaceAll("[']","");
                                        }
                            %>
                            <tr style="text-align: center">
                                <td/>
                                <td><%=count%></td>
                                <%if (rDetails[i].getStatus() == GlobalStatus.SUSPEND) {%>
                                <td>
                                    <a href="#" class="btn btn-warning btn-xs btn-circle" data-toggle="tooltip" data-placement="right" title="Request did not send to checker yet"  ><i class="fa fa-thumbs-o-down"></i></a>
                                </td>
                                <% } else if (rDetails[i].getStatus() == GlobalStatus.ACTIVE || rDetails[i].getStatus() == GlobalStatus.SENDTO_CHECKER) { %>
                                <td>
                                    <a href="#" class="btn btn-info btn-xs btn-circle" data-toggle="tooltip" data-placement="right" title="Request sended to checker" ><i class="fa fa-thumbs-o-down"></i></a>
                                </td>
                                <% } else if (rDetails[i].getStatus() == GlobalStatus.REJECTED) { %>
                                <td>
                                    <a href="#" class="btn btn-danger btn-xs btn-circle" data-toggle="tooltip" data-placement="right" title="Request rejected" ><i class="fa fa-thumbs-o-down"></i></a>
                                </td>
                                <%} else {%>
                                <td>
                                    <a href="#" class="btn btn-success btn-xs btn-circle" data-toggle="tooltip" data-placement="right" title="Request approved" ><i class="fa fa-thumbs-o-up"></i></a>
                                </td>
                                <%}%>
                                <!--                                <td><span class="text-info">Not requested</span></td>-->
                                <td><%=rDetails[i].getPackageName()%></td> 
                                <td>
                                    <%if (rDetails[i].getMarkAsDefault() != null) {%>
                                    <a class="btn btn-info btn-xs">Default</a>
                                    <%} else {%>
                                    <a class="btn btn-info btn-xs"  onclick="setDefault('<%=rDetails[i].getPckId()%>')">Set</a>
                                    <%}%>
                                </td>
                                <td><%=rDetails[i].getPlanAmount()%></td>
                                <td><%=rDetails[i].getPackageDuration()%></td>
<!--                                <td><%=rDetails[i].getPaymentMode()%></td>-->

                                <td>                                    
                                    <div class="btn-group disabled">                                    
                                        <div class="btn-group">                                                                                                                                                     
                                            <a class="btn btn-info btn-xs dropdown-toggle" data-toggle="dropdown" href="" >Manage <span class="caret"><font style="font-size: 12px"></font></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="editAdvPackage.jsp?_edit=<%=rDetails[i].getPckId()%>"  >Edit</a></li>
                                                <li><a href="#" onclick="loadAdvertismentPackageModal(<%=rDetails[i].getPckId()%>,'<%=desc%>')" >Edit Description</a></li>
                                                    <% if (makerChakerObj != null) {
                                                        if (makerChakerObj.status == GlobalStatus.ACTIVE && fillAllDetailsFlag) {%>
                                                <li><a href="#" onclick="sendAdvertismentPackageToChecker('<%=GlobalStatus.SENDTO_CHECKER%>', '<%=rDetails[i].getPackageName()%>')" >Send to checker</a></li>                                                               
                                                    <%}
                                                    }%>
                                                    <% if (rDetails[i].getStatus() == GlobalStatus.REJECTED && rDetails[i].getReasonForRejection() != null) {%>   
                                                <li><a href="#" onclick="rejectionAdvertismentPackageDetails('<%=rDetails[i].getReasonForRejection()%>', '<%=rDetails[i].getPackageName()%>')" ><font color="color:red"> Rejection Details</font></a></li>                                                                                                                           
                                                        <%}%>
                                            </ul>                                                                               
                                        </div>
                                </td>
                                <td><%=rDetails[i].getCreationDate()%></td>
                                <%if (rDetails[i].getUpdationDate() == null) {%>
                                <td><%=updationDate%></td>
                                <%} else {%>
                                <td><%=rDetails[i].getUpdationDate()%></td>
                                <%}%>
                            </tr>
                            <%}
                                }%>
                        </tbody>
                    </table>
                    <a href="./createAdvertiserPackage.jsp" class="btn btn-success btn-xs"><i class="fa fa-plus-circle"></i> Add package</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="addpackageDecription" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>
                    <h4 class="modal-title">Edit Description</h4>
                </div>
                <div class="modal-body">
                    <form class="form-inline" id="addPackageDescForm">
                        <fieldset>
                            <div class="control-group">
                                <label class="control-label col-lg-2"  for="partnername">Description</label>
                                <div class="controls col-lg-10">
                                    <textarea id="packageInfo" name="packageInfo" rows="10" cols="700" style='width:455px !important' class="form-control"></textarea>
                                    <input type="hidden" id="packageId" name="packageId">                                    
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="col-lg-12">
                                    <a class="btn btn-success btn-sm" onclick="editAdvPackageDescription()" style="float: right; margin: 15px; "><span class="icon_profile"></span> <i class="fa fa-check-circle"></i> Change Description</a>                                    
                                </div>
                            </div>
                        </fieldset>
                    </form>                
                </div>
            </div>            
        </div>
    </div>
                                    
<div id="viewRejection" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="rejectPackageModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 id="rejectPackageModal"><b>Rejection details</b></h4>
            </div>          
            <div class="modal-body">
                <div class="row-fluid">
                    <form class="form-horizontal" id="viewrejectPackageForm">
                        <fieldset>
                            <div class="control-group">
                                <label class="control-label col-lg-2"  for="partnername">Reason</label>
                                <div class="controls col-lg-10">
                                    <!--                                    <input type="hidden" readonly id="_packageName" name="_packageName" >
                                                                        <input type="hidden" readonly id="_packagestatus" name="_packagestatus" >-->
                                    <textarea id="_rejectionDetails" name="_rejectionDetails" class="form-control" rows="4" readonly></textarea>
                                </div>
                            </div>                            
                        </fieldset>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div id="edit-partner-result"></div>
                <button class="btn btn-info btn-xs" data-dismiss="modal"><i class="fa fa-info-circle"></i> OK</button>                
            </div>
        </div>
    </div>
</div>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->

</body>
</html>
<%@include file="footer.jsp" %>
