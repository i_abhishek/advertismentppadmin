/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.virtualNumber;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.SgNumbers;
import com.mollatech.serviceguard.nucleus.db.connector.management.NumbersManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;


@WebServlet(name = "EditNumber", urlPatterns = {"/EditNumber"})
public class EditNumber extends HttpServlet {

    static final Logger logger = Logger.getLogger(EditNumber.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        logger.info("Requested Servlet is EditNumber at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        String result = "success";
        String message = "Mobile number updated successfully!!!";
        JSONObject json = new JSONObject();
        int numberID = Integer.parseInt(request.getParameter("numberId"));
        logger.debug("value of numberId : " + numberID);
        String mobileNo = request.getParameter("editNo");
        String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        logger.debug("value of sessionId : " + sessionId);
        String envt = request.getParameter("numberType");
        logger.debug("value of envt : " + envt);
        int res = -1;
        SgNumbers numberData    = new NumbersManagement().getMobileNumberById(sessionId, numberID);
        SgNumbers[] numberByVNum  = new NumbersManagement().getAllNumbersByMobileNo(sessionId, mobileNo);
         if (numberByVNum != null) {
             if (numberByVNum[0].getStatus() != GlobalStatus.DELETED) {
                if (numberByVNum.length > 1) {
                    result = "error";
                    message = "Virtual number already in used or added.";
                    try {
                        json.put("result", result);
                        logger.debug("Response of EditNumber (PPAdmin) Servlet's Parameter  result is " + result);
                        json.put("message", message);
                        logger.debug("Response of EditNumber (PPAdmin) Servlet's Parameter  message is " + message);
                    } catch (Exception ex) {
                        logger.error("Exception at EditNumber (PPAdmin) ", ex);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }else if(numberByVNum[0].getNumberId() != numberID){
                    result = "error";
                    message = "Virtual number already in used or added.";
                    try {
                        json.put("result", result);
                        logger.debug("Response of EditNumber (PPAdmin) Servlet's Parameter  result is " + result);
                        json.put("message", message);
                        logger.debug("Response of EditNumber (PPAdmin) Servlet's Parameter  message is " + message);
                    } catch (Exception ex) {
                        logger.error("Exception at EditNumber (PPAdmin) ", ex);
                    }
                    out.print(json);
                    out.flush();
                    return;
                }
            }             
         } 
            if (numberData != null) {
                numberData.setMobileNo(mobileNo);
                numberData.setUpdatedOn(new Date());
                numberData.setType(envt);
                numberData.setPlainNumber(mobileNo.replace("-", ""));
                res = new NumbersManagement().updateDetails(numberData);
            }
            if (res == 0) {
                result = "success";
                message = "Virtual number updated successfully!!!";
            } else {
                result = "error";
                message = "Virtual number not updated !!!";
            }
        
        try {
            json.put("result", result);
            json.put("message", message);
            return;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
