<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgResourceowner"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceOwnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartyManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPartydetails"%>
<%
    int id = Integer.parseInt(request.getParameter("_revOwnerId"));
    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
    
    SgResourceowner resourceowner = new ResourceOwnerManagement().getResourceownerbyId(SessionId, id);
    String[] resourceID = null;
    if(resourceowner != null){
        String resourceDetails = resourceowner.getResourceName();
        if(resourceDetails != null && !resourceDetails.isEmpty()){
            resourceID = resourceDetails.split(",");
        } 
    }
%>
<select id="_revResource" name="_revResource"  class="form-control" onchange="" >               
    <%                               
            if (resourceID != null) {
                for (int i = 0; i < resourceID.length; i++) {                    
                        String resourceId = resourceID[i];
                        if(!resourceId.isEmpty()){
                        ResourceDetails resourceObj = new ResourceManagement().getResourceById(Integer.parseInt(resourceId));
                        if(resourceObj != null){
    %>                    
    <option value="<%=resourceObj.getResourceId()%>"><%=resourceObj.getName()%></option>
    <%
                    }                    
                }
            }
        }else{%>
    <option value="-1" selected>Select Resource</option>
    <%}%>
</select>