
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgCassNumbers"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.CaaSManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.UtilityFunctions"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgNumbers"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.NumbersManagement"%>
<%@include file="header.jsp" %>
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header text-uppercase"> CaaS Number Management</h3>
            </div>
        </div>
        <div class="row">
            <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-home"></i><a href="home.jsp"> Home </a>&#47;  <i class="fa fa-table"></i><b> Edit Number</b>
                    </div>
                    <div class="panel-body">
                        <div class="dataTable_wrapper" class="col-lg-10">
                            <table class="table table-striped table-bordered table-hover display nowrap">
                                <thead>
                                    <tr>
                                        <td/>
                                        <th>Sr.No</th>
                                        <th>Numbers</th>
                                        <!--                                        <th>API</th>
                                                                                <th>ServiceType</th>-->
                                        <th>Status</th>
                                        <th>Developer Name</th>
                                        <th>Envt.</th>
                                        <th>Manage</th>
                                        <th>Created On</th>
                                        <!--<th>Updated On</th>-->
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        int count = 0;
                                        SgCassNumbers[] allNumbers = new CaaSManagement().getAllNumbers();
                                        if (allNumbers != null) {
                                            for (int i = 0; i < allNumbers.length; i++) {
                                                String updatedDate = new UtilityFunctions().getTMReqDate(allNumbers[i].getUpdatedOn());
                                                String createDate = new UtilityFunctions().getTMReqDate(allNumbers[i].getCreatedOn());
                                                if (allNumbers[i].getStatus() != GlobalStatus.DELETED) {
                                                    count++;
                                                    String developer = "NA";
                                                    if (allNumbers[i].getPartnerId() != null) {
                                                        developer = new PartnerManagement().getPartnerDetails(allNumbers[i].getPartnerId()).getPartnerName();
                                                    }
                                    %>
                                    <tr>
                                        <td/>
                                        <td><%= count%></td>
                                        <td><%= allNumbers[i].getNumber()%></td>
<!--                                        <td><%= allNumbers[i].getApi()%></td>
                                        <td><%= allNumbers[i].getServiceType()%></td>-->
                                        <%  if (allNumbers[i].getPartnerId() != null) {%>
                                        <td><span class="label-success label">Assigned</span></td>
                                        <% } else {%>
                                        <td><span class="label-info label">Unassign</span></td>
                                        <%}%>
                                        <td><%=developer%></td>
                                        <td><%=allNumbers[i].getEnvt()%></td>
                                        <td>
                                            <div class="btn btn-group btn-xs">
                                                <button class="btn btn-default btn-xs dropdown-toggle"  data-toggle="dropdown"><%if (allNumbers[i].getStatus() == GlobalStatus.ACTIVE) {%> <font style="font-size: 11px;">ACTIVE</font> <%} else if (allNumbers[i].getStatus() == GlobalStatus.SUSPEND) {%><font style="font-size: 11px;"> SUSPENDED</font> <%}%><span class="caret"></span></button>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#"  onclick="changeCaasStatus(<%=GlobalStatus.ACTIVE%>, <%=allNumbers[i].getNumberId()%>)" >Mark as (Re)Active?</a></li>
                                                    <li><a href="#" onclick="changeCaasStatus(<%=GlobalStatus.SUSPEND%>, <%=allNumbers[i].getNumberId()%>)" >Mark as Suspended?</a></li>
                                                        <%if (allNumbers[i].getPartnerId() != null) {%>
                                                    <li><a href="#" onclick="changeCaasStatus(<%=GlobalStatus.TERMINATED%>,<%=allNumbers[i].getNumberId()%>)" >Mark as Terminated?</a></li>
                                                        <%}%>
                                                </ul>
                                            </div>
                                        </td>
                                        <td><%=createDate%></td>
                                        <!--<td><%=updatedDate%></td>-->
                                        <td>
                                            <%  if (allNumbers[i].getPartnerId() != null) {%>
                                            <a href="#" class="btn btn-info btn-xs" data-toggle="tooltip"  data-placement="center" disabled><i class="glyphicon glyphicon-edit" ></i></a>
                                            <a href="#" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="center" disabled><i class="glyphicon glyphicon-trash"></i></a>
                                                <%} else {%>
                                            <a href="#" class="btn btn-info btn-xs" data-toggle="tooltip" onclick="editNumberModel('<%=allNumbers[i].getNumber()%>', '<%=allNumbers[i].getNumberId()%>')" data-placement="center"  ><i class="glyphicon glyphicon-edit"></i></a>
                                            <a href="#" class="btn btn-danger btn-xs" data-toggle="tooltip" onclick="deleteCaasNo('<%=allNumbers[i].getNumberId()%>')" data-placement="center"><i class="glyphicon glyphicon-trash"></i></a>

                                            <%}%>
                                        </td>
                                    </tr>
                                    <%}
                                            }
                                        }%>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="editNumbers" class="modal fade" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <b id="editNumberModel"></b>
            </div>          
            <div class="modal-body">
                <div class="row-fluid">
                    <form class="form-horizontal" id="editnumberForm" name="editnumberForm">
                        <fieldset>
                            <div class="control-group ">
                                <label class="control-label col-lg-2">Edit</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control " id="editNo" name="editNo" onkeypress="return isNumericKey(event)">
                                    <input type="hidden" id="numberId" name="numberId">
                                </div>
                            </div>                            
                        </fieldset>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div id="edit-partner-result"></div>
                <button class="btn btn-info btn-xs" data-dismiss="modal">Close</button>
                <button class="btn btn-success btn-xs" onclick="editCassNumber()" id="addPartnerButtonE">Save</button>
            </div>
        </div>
    </div>
</div>                    
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<script src="js/caasNumbers.js" type="text/javascript"></script>
<script src="js/bootbox.min(1).js" type="text/javascript"></script>
<!--<script src="js/bootbox.min (1).js" type="text/javascript"></script>-->
<script src="js/bootbox.min.js" type="text/javascript"></script>

</body>
</html>
<%@include file="footer.jsp" %>