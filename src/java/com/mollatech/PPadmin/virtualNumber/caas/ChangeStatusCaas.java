/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.virtualNumber.caas;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgCassNumbers;
import com.mollatech.serviceguard.nucleus.db.connector.management.CaaSManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ashu
 */
@WebServlet(name = "ChangeStatusCaas", urlPatterns = {"/ChangeStatusCaas"})
public class ChangeStatusCaas extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        try {
            int id = Integer.parseInt(request.getParameter("id"));
            CaaSManagement management = new CaaSManagement();
            SgCassNumbers cassNumbers = management.getNumberById(id);
            int status = Integer.parseInt(request.getParameter("status"));
            String message = "Status Changed Successfully.";
            if (status == GlobalStatus.ACTIVE && cassNumbers.getStatus() == GlobalStatus.ACTIVE) {
                message = "Status Already Active.";
                json.put("result", "error");
                json.put("message", message);
                return;
            } else if (status == GlobalStatus.SUSPEND && cassNumbers.getStatus() == GlobalStatus.SUSPEND) {
                message = "Status Already Suspended.";
                json.put("result", "error");
                json.put("message", message);
                return;
            }
            if (status == GlobalStatus.ACTIVE || status == GlobalStatus.SUSPEND) {
                cassNumbers.setStatus(status);
                management.updateDetails(cassNumbers);
                json.put("result", "success");
                json.put("message", message);
                return;
            } else {
                int partnerId = cassNumbers.getPartnerId();
                cassNumbers.setStatus(GlobalStatus.ACTIVE);
                cassNumbers.setPartnerId(null);
                management.updateDetails(cassNumbers);
                PartnerDetails details = new PartnerManagement().getPartnerDetails(partnerId);
                String caasNumbers = details.getCaasNumber();
                try {
                    caasNumbers = caasNumbers.replace("," + cassNumbers.getNumberId() + ",", "");
                } catch (Exception ex) {

                }
                details.setCaasNumber(caasNumbers);
                new PartnerManagement().updateDetails(details);
                json.put("result", "success");
                json.put("message", "Number Terminated Successfully.");
                return;
            }
        } catch (Exception ex) {
            json.put("result", "error");
            json.put("message", ex.getMessage());
        } finally {
            out.print(json);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
