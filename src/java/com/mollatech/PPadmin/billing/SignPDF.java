/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.billing;

import com.itextpdf.text.Meta;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.security.BouncyCastleDigest;
import com.itextpdf.text.pdf.security.ExternalDigest;
import com.itextpdf.text.pdf.security.ExternalSignature;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.itextpdf.text.pdf.security.MakeSignature.CryptoStandard;
import com.itextpdf.text.pdf.security.PrivateKeySignature;
import com.mollatech.service.nucleus.crypto.LoadSettings;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "SignPDF", urlPatterns = {"/SignPDF"})
public class SignPDF extends HttpServlet {
    static final Logger logger = Logger.getLogger(SignPDF.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        logger.info("Requested Servlet is GenerateRevenue at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "PDF Sign successfully.";
//        String sourceFile = LoadSettings.g_pdfSigningSettings.getProperty("sourceFilePath");
//        String destFile   = LoadSettings.g_pdfSigningSettings.getProperty("destFilePath");
//        String certFile   = LoadSettings.g_pdfSigningSettings.getProperty("certFilePath");
//        String password   = LoadSettings.g_pdfSigningSettings.getProperty("password");
        String sourceFile = "/home/mohanish/Documents/Abhishek/InventoryReport.pdf";
        String destFile   = "/home/mohanish/Documents/Abhishek/SignedInventoryReport.pdf";
        String certFile   = "/home/mohanish/Documents/Abhishek/Abhi/fixed.p12";
        String password   = "Passw0rd";
        try{
            boolean flag = signPdf(sourceFile,destFile,certFile,password,null);
            if(flag){
                json.put("result", result);
                json.put("message", message);
            }else{
                result = "error";
                message = "PDF Sign failed";
                json.put("result", result);
                json.put("message", message);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        }catch(Exception e){
            e.printStackTrace();
            logger.info("Exception at #SignPDF from #PPAdmin " + e);
            result = "error";
            message = "PDF Sign failed";
            json.put("result", result);
            json.put("message", message);
            
        }finally {
            logger.info("Response of #SignPDF from #PPAdmin " + json.toString());
            logger.info("Response of #SignPDF from #PPAdmin Servlet at " + new Date());
            out.print(json);
            out.flush();
        }
        
    }
    
    public boolean signPdf(String sourceFile, String destFile, String certFile, String password, String imagepath) throws Exception{
        InputStream input = null;
        try {

            logger.debug("Signing Data");
//            byte[] fileKey = Base64.decode(certFile);
//
//            String fileKeyPassword = password;
//
//            KeyStore ks = KeyStore.getInstance("pkcs12");
//
//            ks.load(new ByteArrayInputStream(fileKey), fileKeyPassword.toCharArray());
            java.security.Security.addProvider(new sun.security.provider.Sun());
            java.security.Security.addProvider(new com.sun.crypto.provider.SunJCE());
            java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
            KeyStore ks;
            char[] passphrase = password.toCharArray();
            ks = KeyStore.getInstance("PKCS12");
            ks.load(new FileInputStream(certFile), passphrase);

            String alias = (String) ks.aliases().nextElement();

           // PrivateKey pk = (PrivateKey) ks.getKey(alias, fileKeyPassword.toCharArray());
             PrivateKey pk = (PrivateKey) ks.getKey(alias, passphrase);
            Certificate[] chain = (Certificate[]) ks.getCertificateChain(alias);

            PdfReader reader = new PdfReader(sourceFile);
            logger.debug("Reading PDF");
            logger.debug("Destination file");
            FileOutputStream os = new FileOutputStream(destFile);
            Map<String, String> moreInfo = new HashMap<String, String>();
//            String PdfAuthor = rsInfo.qrCodeData;
//            String PdfTile = rsInfo.clientLocation;
            String PdfAuthor = "ServiceGuard";
            String PdfTile = "Invoice";
            moreInfo.put(Meta.PRODUCER, PdfTile);
            moreInfo.put(Meta.AUTHOR, PdfAuthor);
            PdfStamper stamper = PdfStamper.createSignature(reader, os, '\0', null, true);
            PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
            logger.debug("Stamping pdf");
            stamper.setMoreInfo(moreInfo);
            //appearance.setReason(rsInfo.reason);
            ExternalSignature es = new PrivateKeySignature(pk, "SHA1", "BC");
            ExternalDigest digest = new BouncyCastleDigest();
            logger.debug("Before digital Signature");
               MakeSignature.signDetached(appearance, digest, es, chain, null, null, null, 0, CryptoStandard.CMS);
            logger.debug("After Digital signature");
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Exception while signing a file " + ex.getMessage());
            throw new Exception(ex);
            
        }
  }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
