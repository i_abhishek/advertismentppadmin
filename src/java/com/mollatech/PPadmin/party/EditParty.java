/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.party;

import com.mollatech.serviceguard.nucleus.commons.UtilityFunctions;
import com.mollatech.serviceguard.nucleus.db.SgPartydetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartyManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "EditParty", urlPatterns = {"/EditParty"})
public class EditParty extends HttpServlet {
static final Logger logger = Logger.getLogger(EditParty.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        logger.info("Requested Servlet is EditParty at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        String result = "success";
        String message = "Party details has been updated successfully!!!";
        JSONObject json = new JSONObject();
        int partyID = Integer.parseInt(request.getParameter("editPartyId"));
        logger.debug("value of partyID : " + partyID);        
        String partyName = request.getParameter("editPartyName");
        String partyPhone = request.getParameter("editPartyPhone");
        String partyEmailId = request.getParameter("editPartyEmailId");
        String partyCompany = request.getParameter("editPartyCompany");
        String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        logger.debug("value of sessionId : " + sessionId);
        int res = -1;
        if (UtilityFunctions.isValidPhoneNumber(partyPhone) == false) {
            result = "error";
            message = "Phone number should be in digit.";
            try {
                json.put("result", result);
                logger.debug("Response of #EditParty from #PPAdmin Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of #EditParty from #PPAdmin Servlet's Parameter  message is " + message);
            } catch (Exception ex) {
                logger.error("Exception at #EditParty from #PPAdmin ", ex);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        }
        SgPartydetails[] partyObject = new PartyManagement().getPartyByName(channelId, partyName);
        SgPartydetails partyObj    = new PartyManagement().getPartyById(channelId, partyID);
        if (partyObject != null && partyObject.length > 1 ) {
            result = "error";
            message = "Party name already taken, Try with other name.";
            try {
                json.put("result", result);
                logger.debug("Response of #EditParty from #PPAdmin Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of #EditParty from #PPAdmin Servlet's Parameter  message is " + message);
            } catch (Exception ex) {
                logger.error("Exception at #EditParty from #PPAdmin ", ex);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        }else if (partyObject[0].getPartyId() != partyObj.getPartyId()) {
            result = "error";
            message = "Party name already taken, Try with other name.";
            try {
                json.put("_result", result);
                logger.debug("Response of EditParty Servlet's Parameter  result is " + result);
                json.put("_message", message);
                logger.debug("Response of EditParty Servlet's Parameter  message is " + message);
            } catch (Exception ex) {
                logger.error("Exception at EditParty ", ex);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        }
        partyObject = new PartyManagement().getAddedParties(partyEmailId, partyPhone);
        if (partyObject != null && partyObject.length > 1 ) {
            result = "error";
            message = "Party Phone or Email already taken, Try with other.";
            try {
                json.put("result", result);
                logger.debug("Response of #EditParty from #PPAdmin Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of #EditParty from #PPAdmin Servlet's Parameter  message is " + message);
            } catch (Exception ex) {
                logger.error("Exception at #EditParty from #PPAdmin ", ex);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        }else if (partyObject[0].getPartyId() != partyObj.getPartyId()) {
            result = "error";
            message = "Party Phone or Email already taken, Try with other.";
            try {
                json.put("_result", result);
                logger.debug("Response of EditParty Servlet's Parameter  result is " + result);
                json.put("_message", message);
                logger.debug("Response of EditParty Servlet's Parameter  message is " + message);
            } catch (Exception ex) {
                logger.error("Exception at EditParty ", ex);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        }
        if(partyObj != null){
            partyObj.setPartyName(partyName);
            partyObj.setPartyEmailid(partyEmailId);
            partyObj.setPartyCompanyName(partyCompany);
            partyObj.setPartyPhone(partyPhone);            
            partyObj.setLastupdatedon(new Date());
            res = new PartyManagement().updateDetails(partyObj);
        }
        if (res == 0) {
                result = "success";
                message = "Party details has been updated successfully!!!";
            } else{
                result = "error";
                message = "Party details not updated !!!";
            }
         try {
            json.put("result", result);
            json.put("message", message);
            return;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
