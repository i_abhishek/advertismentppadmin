<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Serializer"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.AccessPolicy"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%@page import="java.io.ObjectInputStream"%>
<%--<%@page import="com.partner.web.AccessPolicies"%>
<%@page import="portalconnect.PartnerPortalWrapper"%>
<%@page import="com.partner.web.Accesspoint"%>--%>
<%
    String _apId = request.getParameter("_apId");
    int apId = -1;
    if (_apId != null) {
        apId = Integer.parseInt(_apId);
    }
    String sessionId1 = (String) request.getSession().getAttribute("_partnerSessionId");
    String channelId = (String) request.getSession().getAttribute("_channelId");
    //PartnerPortalWrapper pww = new PartnerPortalWrapper();
    Accesspoint ap = new AccessPointManagement().getAccessPointById(sessionId1,channelId, apId);
    byte[] ape = ap.getAccessPolicyEntry();
    AccessPolicy apl = (AccessPolicy) Serializer.deserialize(ape);
     String reqEndTime = "Not Available";
    if(apl.ApEndTime != null){
        reqEndTime = apl.ApEndTime;
    }
    String reqStartTime = "Not Available";
    if(apl.ApStartTime != null){
        reqStartTime = apl.ApStartTime;
    }
    String currencyCode = "NA";
    if(apl.currencyCode != null){
        currencyCode = apl.currencyCode;
    }
%>
<div id="accesspolicy">

    <div class="row">
        <%
            //if ((apl.getVolumeRestriction()).equalsIgnoreCase("yes")) {
        %>
        <font style=" font-weight: bolder;">
        <label for="cname" class="control-label col-lg-2">Required TPS </label>
        </font>
        <div class="col-lg-2">
            <input class="form-control" id="partnerTPS" name="partnerTPS"  type="number" value="<%= apl.tps%>" placeholder="Required TPS" readonly />
        </div>

        <font style=" font-weight: bolder;">
        <label for="cname" class="control-label col-lg-2">Required TPD </label>
        </font>
        <div class="col-lg-2">
            <input class="form-control" id="partnerTPD" name="partnerTPD"  type="number" value="<%= apl.tpd%>"  placeholder="Required TPD"  readonly/>
        </div>

        <font style=" font-weight: bolder;">
        <label for="cname" class="control-label col-lg-2">Required Start Time </label>
        </font>
        <div class="col-lg-2">
            <input class="form-control" id="partnerST" name="partnerST" value="<%= reqStartTime%>"  type="text" readonly/>
        </div>
        <%//} else {%>
<!--        <div class="col-lg-2">
            No Volume Restrictions
        </div>-->
        
        <%//}%>
    </div>                                                                                                         
    <br>


    <div class="row">
        <%
          //  if ((apl.getTimeRestriction()).equalsIgnoreCase("yes")) {
        %>
        <font style=" font-weight: bolder;">
        <label for="cname" class="control-label col-lg-2">Required End Time </label>
        </font>
        <div class="col-lg-2">
            <input class="form-control" id="partnerET" name="partnerET" value="<%= reqEndTime%>"  type="text" readonly/>
        </div>
        <font style=" font-weight: bolder;">
        <label for="cname" class="control-label col-lg-2">Required Start Day </label>
        </font>

        <%
            String DayFrom = "Not Available";
            if (apl.dayFrom != null) {
                if (apl.dayFrom.equals("1")) {
                    DayFrom = "Sunday";
                } else if (apl.dayFrom.equals("2")) {
                    DayFrom = "Monday";
                } else if (apl.dayFrom.equals("3")) {
                    DayFrom = "Tuesday";
                } else if (apl.dayFrom.equals("4")) {
                    DayFrom = "Wednesday";
                } else if (apl.dayFrom.equals("5")) {
                    DayFrom = "Thursday";
                } else if (apl.dayFrom.equals("6")) {
                    DayFrom = "Friday";
                } else if (apl.dayFrom.equals("7")) {
                    DayFrom = "Saturday";
                }
            }
        %>
        <div class="col-lg-2">
            <input class="form-control" id="partnerSD" name="partnerSD"  value="<%= DayFrom%>"  type="text" readonly/>
        </div>
        <%
            String DayTo = "Not Available";
            if (apl.dayTo != null) {
                if (apl.dayTo.equals("1")) {
                    DayTo = "Sunday";
                } else if (apl.dayTo.equals("2")) {
                    DayTo = "Monday";
                } else if (apl.dayTo.equals("3")) {
                    DayTo = "Tuesday";
                } else if (apl.dayTo.equals("4")) {
                    DayTo = "Wednesday";
                } else if (apl.dayTo.equals("5")) {
                    DayTo = "Thursday";
                } else if (apl.dayTo.equals("6")) {
                    DayTo = "Friday";
                } else if (apl.dayTo.equals("7")) {
                    DayTo = "Saturday";
                }
            }
        %>
        <font style=" font-weight: bolder;">
        <label for="cname" class="control-label col-lg-2">Required End Day </label>
        </font>
        <div class="col-lg-2">                                            
            <input class="form-control" id="partnerED" name="partnerED" value="<%= DayTo%>"   type="text" readonly/>
        </div>
        <%//} else {%>
<!--         <div class="col-lg-2">
         No Time Restrictions
        </div>-->
        
        <%//}%>
    </div>
    <br>
    <br>
    <div class="row">
        <font style=" font-weight: bolder;">
        <label for="cname" class="control-label col-lg-2">Authentication Restriction</label>
        </font>
        <div class="col-lg-2">
           <input class="form-control" id="partnerAuth" name="partnerAuth"  type="text" value="<%= apl.authentication%>" placeholder="Required TPS" readonly />            
        </div>
        <font style=" font-weight: bolder;">
        <label for="cname" class="control-label col-lg-2">Certificate Check</label>
        </font>
        <div class="col-lg-2">
            <input class="form-control" id="partnerCert" name="partnerCert"  type="text" value="<%= apl.certCheck%>" placeholder="NA" readonly />
        </div>
        <font style=" font-weight: bolder;">
        <label for="cname" class="control-label col-lg-2">Token Check</label>
        </font>
        <div class="col-lg-2">
            <input class="form-control" id="partnerCert" name="partnerToken"  type="text" value="<%= apl.tokenCheck%>" placeholder="NA" readonly />
        </div>
    </div>
    <br>
    <div class="row">
        <font style=" font-weight: bolder;">
        <label for="cname" class="control-label col-lg-2">Per Transaction Billing</label>
        </font>
        <div class="col-lg-2">
           <input class="form-control" id="partnerBilling" name="partnerBilling"  type="text" value="<%= apl.BillingRestriction%>" placeholder="Required TPS" readonly />            
        </div>
        <font style=" font-weight: bolder;">
        <label for="cname" class="control-label col-lg-2">Currency Code</label>
        </font>
        <div class="col-lg-2">
            <input class="form-control" id="partnerCurrencyCode" name="partnerCurrencyCode"  type="text" value="<%= currencyCode%>" placeholder="NA" readonly />
        </div>
        <font style=" font-weight: bolder;">
        <label for="cname" class="control-label col-lg-2">Amount</label>
        </font>
        <div class="col-lg-2">
            <input class="form-control" id="partnerAmount" name="partnerAmount"  type="text" value="<%= apl.Amount%>" placeholder="NA" readonly />
        </div>
    </div>
    <br>

</div>