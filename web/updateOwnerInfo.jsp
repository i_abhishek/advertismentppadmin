<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartyManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPartydetails"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgResourceowner"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceOwnerManagement"%>
<%@include file="header.jsp" %>
<link href="./select2/select2.css" rel="stylesheet"/>
<script src="js/partyManager.js" type="text/javascript"></script>
<%
    int id = Integer.parseInt(request.getParameter("_requestId"));
    SgResourceowner SgResourceowner = new ResourceOwnerManagement().getResourceownerbyId(SessionId, id);
    ResourceDetails[] resourceDetailses = new ResourceManagement().getAllResources();
%>

<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h4 class="page-header">Resource Owner Management</h4>
            </div>
            <!-- /.col-lg-10 -->
        </div>
        <div class="row">
            <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47; <i class="fa fa-user"></i><a href="resourceOwnerManager.jsp"> Owner Manager</a>&#47; <i class="fa fa-table"></i><b> Update Owner</b>
                    </div>
                    <div class="panel-body">
                        <h4>Owner details</h4>
                        <hr>
                        <form class="form-horizontal" id="ownerInfoForm" name="ownerInfoForm" role="form" method="POST">
                            <input type="hidden" name="resourceOwnId" id="resourceOwnId"  value="<%=id%>">
                            <div class="form-group">
                                <label class="control-label col-lg-2">Assign Resource(s)</label>
                                <div class="col-lg-8">
                                    <select id="resourceDe" name="resourceDe"  multiple="multiple" style="width: 100%" onchange="updateResourceList()">
                                        <!--                                        <option value="-1" >Select Resource</option>-->
                                        <%if (resourceDetailses != null) {
                                                for (ResourceDetails details : resourceDetailses) {
                                                    int check = new ResourceOwnerManagement().checkResourceStatus("" + details.getResourceId(), SgResourceowner.getResourceId());
                                                    if (check != 0) {
                                        %>
                                        <option value="<%=details.getResourceId()%>" ><%=details.getName()%></option>
                                        <%}
                                                }
                                            }%>
                                    </select>
                                    <%
                                        if (SgResourceowner.getResourceName() != null) {
                                            String[] resId = SgResourceowner.getResourceName().split(",");
                                            for (int i = 0; i < resId.length; i++) {
                                    %>
                                    <script>
                                        var options = document.getElementById('resourceDe').options;
                                        n = options.length;
                                        for (i = 0; i < n; i++) {
                                            if (options[i].value === '<%=resId[i]%>') {
                                                options[i].selected = true;
                                            }
                                        }
                                    </script>
                                    <%   }
                                        }
                                    %>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Select Resource(s) for party</label>
                                <div class="col-lg-6">
                                    <select id ="assignedElement" name="assignedElement" class="span6 form-control" onchange="getRevenuResourceDetails(this.value, '<%=id%>')">                                        
                                    </select>
                                </div>
                            </div>
                            <div id="revenueDetailforParty"></div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="js/resourcePrice.js"></script>
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="dist/js/sb-admin-2.js"></script>
<script src="./select2/select2.js"></script>
<script>
                                        $(document).ready(function () {
                                            $("#resourceDe").select2();
                                            updateResourceList();

                                        });
                                        function getRevenuResourceDetails(resourceId, id) {
                                            var s = './loadPartyDetails.jsp?_requestId=' + id + '&resourceId=' + resourceId;
                                            $.ajax({
                                                type: 'GET',
                                                url: s,
                                                success: function (data) {
                                                    $('#revenueDetailforParty').html(data);
                                                }
                                            });
                                        }
                                        function updateResourceList() {
                                            var assignedOptions = document.getElementById('resourceDe').options;
                                            n = assignedOptions.length;
                                            document.getElementById('assignedElement').innerHTML = "";
                                            select = document.getElementById('assignedElement');
                                            var opt = document.createElement('option');
                                            opt.value = '-1';
                                            opt.innerHTML = 'Select Resource';
                                            select.appendChild(opt);
                                            for (i = 0; i < n; i++) {
                                                if (assignedOptions[i].selected === true) {
                                                    var opt = document.createElement('option');
                                                    opt.value = assignedOptions[i].value;
                                                    opt.innerHTML = assignedOptions[i].text;
                                                    select.appendChild(opt);
                                                }
                                            }
                                            getRevenuResourceDetails('-1', '<%=id%>');
                                        }

</script>
<%@include file="footer.jsp" %>