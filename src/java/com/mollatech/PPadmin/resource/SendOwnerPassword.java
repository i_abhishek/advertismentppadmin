package com.mollatech.PPadmin.resource;

import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.connector.communication.SGStatus;
import com.mollatech.serviceguard.nucleus.commons.UtilityFunctions;
import com.mollatech.serviceguard.nucleus.db.SgResourceowner;
import com.mollatech.serviceguard.nucleus.db.connector.management.ResourceOwnerManagement;
import com.mollatech.serviceguard.nucleus.settings.SendNotification;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ash
 */
@WebServlet(name = "SendOwnerPassword", urlPatterns = { "/SendOwnerPassword" })
public class SendOwnerPassword extends HttpServlet {

    final String itemtype = "USERPASSWORD";

    final String itemTypeAUTH = "AUTHORIZTION";

    public static final int PENDING = 2;

    public static final int SENT = 0;

    public static final int SEND = 0;

    public static final int RESET = 1;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        String channelName = "ServiceGuard Portal";
        String channelid = (String) request.getSession().getAttribute("_channelId");
        String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String _userid = request.getParameter("ownerId");
        String result = "success";
        String message = "Password set and send successfully";
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();

        if (_userid == null) {
            result = "error";
            message = "Resource owner not found";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        int retValue = -1;
        ResourceOwnerManagement um = new ResourceOwnerManagement();
        SgResourceowner user = um.getResourceownerbyId(sessionId, Integer.parseInt(_userid));
        UtilityFunctions u = new UtilityFunctions();
        Date d = new Date();
        String strPassword = u.HexSHA1(sessionId + channelid + _userid + d.toString());
        strPassword = strPassword.substring(0, 9);
        u = null;
        user.setPassword(strPassword);
        retValue = um.updateDetails(user);
        if (retValue == 0) {
        } else {
            result = "error";
            message = "password reset failed";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }

        String tmessage = LoadSettings.g_sSettings.getProperty("email.partner.password");
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
        if (tmessage != null) {
            d = new Date();
            tmessage = tmessage.replaceAll("#name#", user.getResourceOwnerName());
            tmessage = tmessage.replaceAll("#channel#", channelName);
            tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
            tmessage = tmessage.replaceAll("#password#", strPassword);
        }
        if (user == null || strPassword == null) {
            result = "error";
            message = "Password Not Sent";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }

        int productType = 3;
        SGStatus statusSG = new SendNotification().SendEmail(channelid, user.getResourceOwnerEmailid(), "RESET PASSWORD", tmessage, null, null, null, null, productType);
        if (statusSG.iStatus == PENDING || statusSG.iStatus == SENT) {
        } else {
            result = "error";
            message = "Password not email successfully";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            out.print(json);
            out.flush();
            return;
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
