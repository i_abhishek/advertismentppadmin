function pageRefreshoMonitoringReports() {
    window.location.href = "./operatorMonitoringReports.jsp";
}

function pageRefreshoRealTImeMonitoringReports() {
    window.location.href = "./operatorMonitoringRealTime.jsp";
}

function bootboxmodel(content) {    
    var popup = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' +
            '<button aria-hidden="true" data-dismiss="modal" class="close" type="button">X</button>' +
            content +
            '</div></div></div></div>');
    popup.modal();
}
function generatereports(type, opType, partner) {
    var sdate = document.getElementById('_startdate').value;
    var edate = document.getElementById('_enddate').value;
    if (sdate === '' || edate === '') {
        bootboxmodal("<h2>" + "Please Select the Date ranges!!!" + "<h2>");
        return;
    } else
        var waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
                '<div class="modal-dialog"><div class="modal-content"><div class="modal-body"><h2><font style="color:  blue; font-weight: bolder">Please Wait</fon></h2></div></div></div></div>');
    waiting.modal();
    var Accesspoint = document.getElementById('_Accesspoint').value;
    var Group = -1;
    var Resource = -1;
    var stime = document.getElementById('_ApStartTime').value;
    var etime = document.getElementById('_ApEndTime').value;
    var s = './chartreports.jsp?_type=' + type + '&_accesspoint=' + Accesspoint + '&_group=' + Group + '&_resources=' + Resource + '&_partner=' + partner + '&_sdate=' + sdate + '&_edate=' + edate + '&_stime=' + stime + '&_etime=' + etime;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#report_data').html(data);
            if (type === 1) {
                $("#bargraph").empty();
                var day_data = UserReportBarChart(opType, Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
                Morris.Bar({
                    element: 'bargraph',
                    data: day_data,
                    xkey: 'label',
                    ykeys: ['value'],
                    labels: ['value'],
                    barColors: function (type) {
                        if (type === 'bar') {
                            return '#0066CC';
                        } else {
                            return '#0066CC';
                        }
                    }
                });

                $("#linegraph").empty();
                var linedata = UserReportLineChart(opType, Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
//              alert(linedata);
                Morris.Line({
                    element: 'linegraph',
                    data: linedata,
                    xkey: 'label',
                    ykeys: ['value'],
                    xLabels: 'day',
                    labels: ['value']
                });
            } else if (type === 2) {
                $("#linertgraph").empty();
                var linedata = ResponseReportLineChart(opType, Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
                Morris.Line({
                    element: 'linertgraph',
                    data: linedata,
                    xkey: 'label',
                    ykeys: ['value'],
                    xLabels: 'day',
                    labels: ['value'],
                    lineWidth: 0,
                    pointSize: 5,
                    smooth: false,
                    postUnits: 'ms'
                });
            }
            waiting.modal('hide');
        }
    });
}


function generatereports1() {
    var waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body"><h2><font style="color:  blue; font-weight: bolder">Please Wait</fon></h2></div></div></div></div>');
    waiting.modal();
    var type = document.getElementById('_type').value;
    var Accesspoint = document.getElementById('_Accesspoint').value;
    var Group = document.getElementById('_Group').value;
    var Resource = document.getElementById('_Resources').value;
    var partner = document.getElementById('_Partner').value;
    var sdate = document.getElementById('_startdate').value;
    var edate = document.getElementById('_enddate').value;
    var stime = document.getElementById('_ApStartTime').value;
    var etime = document.getElementById('_ApEndTime').value;
    var s = './chartreports.jsp?_type=' + type + '&_accesspoint=' + Accesspoint + '&_group=' + Group + '&_resources=' + Resource + '&_partner=' + partner + '&_sdate=' + sdate + '&_edate=' + edate + '&_stime=' + stime + '&_etime=' + etime;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#report_data').html(data);

            $("#linegraph").empty();
            var linedata = UserReportLineChart(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
            Morris.Line({
                element: 'linegraph',
                data: linedata,
                xkey: 'label',
                ykeys: ['value'],
                xLabels: 'day',
                labels: ['value']
            });
            $("#linertgraph").empty();
            var linedata = ResponseReportLineChart(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime);
            Morris.Line({
                element: 'linertgraph',
                data: linedata,
                xkey: 'label',
                ykeys: ['value'],
                xLabels: 'day',
                labels: ['value']
            });
            waiting.modal('hide');
        }
    });
}

function UserReportBarChart(opType, Accesspoint, Group, Resource, partner, sdate, edate, stime, etime) {
    var s = './barchartreport?_opType=' + opType + '&_accesspoint=' + Accesspoint + '&_group=' + Group + '&_resource=' + Resource + '&_partner=' + partner + '&_sdate=' + sdate + '&_edate=' + edate + '&_stime=' + stime + '&_etime=' + etime;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = JSON.parse(jsonData);
    return myJsonObj;
}

function UserReportLineChart(opType, Accesspoint, Group, Resource, partner, sdate, edate, stime, etime) {
    var s = './linechartreport?_opType=' + opType + '&_accesspoint=' + Accesspoint + '&_group=' + Group + '&_resource=' + Resource + '&_partner=' + partner + '&_sdate=' + sdate + '&_edate=' + edate + '&_stime=' + stime + '&_etime=' + etime;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = JSON.parse(jsonData);
    return myJsonObj;
}

function ResponseReportLineChart(opType, Accesspoint, Group, Resource, partner, sdate, edate, stime, etime) {
    var s = './linechartresponsereport?_opType=' + opType + '&_accesspoint=' + Accesspoint + '&_group=' + Group + '&_resource=' + Resource + '&_partner=' + partner + '&_sdate=' + sdate + '&_edate=' + edate + '&_stime=' + stime + '&_etime=' + etime;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = JSON.parse(jsonData);
    return myJsonObj;
}

function userReportPDF(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime) {
//    var val1 = encodeURIComponent(document.getElementById('_auditStartDate').value);
//    var val2 = encodeURIComponent(document.getElementById('_auditEndDate').value);
//    var val4 = encodeURIComponent(document.getElementById('_auditUserID').value);
    var s = './getReports?_apid=' + Accesspoint + "&_grid=" + Group + "&_resid=" + Resource + "&_pid=" + partner + "&_sdate=" + sdate + "&_stime=" + stime + "&_edate=" + edate + "&_etime=" + etime + "&_format=0";
    window.location.href = s;
    return false;
}

function userReportCSV(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime) {
    var s = './getReports?_apid=' + Accesspoint + "&_grid=" + Group + "&_resid=" + Resource + "&_pid=" + partner + "&_sdate=" + sdate + "&_stime=" + stime + "&_edate=" + edate + "&_etime=" + etime + "&_format=1";
    window.location.href = s;
    return false;
}

function userReportTXT(Accesspoint, Group, Resource, partner, sdate, edate, stime, etime) {
    var s = './getReports?_apid=' + Accesspoint + "&_grid=" + Group + "&_resid=" + Resource + "&_pid=" + partner + "&_sdate=" + sdate + "&_stime=" + stime + "&_edate=" + edate + "&_etime=" + etime + "&_format=2";
    window.location.href = s;
    return false;
}

function showAlert(message, type, closeDelay) {
    if ($("#alerts-container").length == 0) {
        $("body")
                .append($('<div id="container" style="' +
                        'width: %; margin-left: 65%; margin-top: 10%;">'));
    }
    type = type || "info";
    var alert = $('<div class="alert alert-' + type + ' fade in">')
            .append(
                    $('<button type="button" class="close" data-dismiss="alert">')
                    .append("&times;")
                    )
            .append(message);
    $("#alerts-container").prepend(alert);
    if (closeDelay)
        window.setTimeout(function () {
            alert.alert("close")
        }, closeDelay);
}

var waiting;
function pleasewait() {
    waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Processing...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');
    //waiting = $('<div class="modal hide" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false"><div class="modal-header"><h1>Processing...</h1></div><div class="modal-body"><div class="progress progress-striped active"></div></div></div>');
    waiting.modal();
}
function chart() {

    var settingId = document.getElementById('_Ressource').value;
    var start = document.getElementById('_startdate').value;
    var end = document.getElementById('_enddate').value;
    if (start === "" || end === "") {
        showAlert('Please Select the Date ranges !', "danger", 4000);
        return;
    } else {
        pleasewait();
        document.getElementById('_Ressource').disabled = true;
        document.getElementById('_startdate').disabled = true;
        document.getElementById('_enddate').disabled = true;
        document.getElementById('Button').disabled = true;
        $('#tabchart').empty();
        var s = './chart.jsp?_settingId=' + settingId + '&_startdate=' + start + '&_enddate=' + end;
        $.ajax({
            type: 'GET',
            url: s,
            success: function (data) {
                waiting.modal('hide');
                $('#tabchart').html(data);
            }
        });
    }
}
function textualreport()
{
    var waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body"><h2><font style="color:  blue; font-weight: bolder">Please Wait</fon></h2></div></div></div></div>');
    waiting.modal();
    var settingId = document.getElementById('_Ressource').value;
    var start = document.getElementById('_startdate').value;
    var end = document.getElementById('_enddate').value;
    $('#tabText').empty();
    var s = './settingExecutionTextReport.jsp?_settingId=' + settingId + '&_startdate=' + start + "&_enddate=" + end;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            waiting.modal('hide');
            $('#tabText').html(data);
        }
    });
}

function realtime()
{
    var settingId = document.getElementById('_Ressource').value;
    document.getElementById('_Ressource').disabled = true;
    document.getElementById('Button').disabled = true;
    var s = './realtime.jsp?_settingId=' + settingId;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#realtimegraph').html(data);
        }
    });
}

function ReportTXT(name, type, checkfor)
{
    var s = './DownloadReport?name=' + name + "&_reporttype=" + 2 + "&monitorType=" + type + "&ChechFor=" + checkfor;

    window.location.href = s;
    return false;
}
function ReportPDF(name, type, checkfor)
{
    var s = './DownloadReport?name=' + name + "&_reporttype=" + 0 + "&monitorType=" + type + "&ChechFor=" + checkfor;

    window.location.href = s;
    return false;
}

function ReportCSV(name, type, checkfor)
{
    var s = './DownloadReport?name=' + name + "&_reporttype=" + 1 + "&monitorType=" + type + "&ChechFor=" + checkfor;
    window.location.href = s;
    return false;

}

function invoicereport() {
    var waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Please Wait...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');
    waiting.modal();
    var invoiceno = document.getElementById('_invoiceno').value;
    var start = document.getElementById('_startdate').value;
    var end = document.getElementById('_enddate').value;
    var partnerName = document.getElementById('_partnername').value;
    var s = './getInvoiceReport.jsp?_invoiceno=' + invoiceno + '&_startdate=' + start + "&_enddate=" + end + "&_partnername=" + partnerName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            waiting.modal('hide');
            $('#tabText').html(data);
        }
    });
}

function getRevenueDetails() {

    var resownname = document.getElementById('_revOwnername').value;
    var resId = document.getElementById('_revResource').value;
    var month = document.getElementById('_revMonth').value;
    var year = document.getElementById('_revYear').value;

    $('#_revGraphData').empty();
    $('#_revBarData').empty();
    $('#_revTextData').empty();

    s = "./GenerateRevenueReports?resownname=" + resownname + "&resId=" + resId + "&month=" + month + "&year=" + year;
    $.ajax({
        type: 'GET',
        url: s,
        dataType: 'json',
        data: $("#ownerInfoForm").serialize(),
        success: function (data) {
            document.getElementById('rMonthlyDetails').style.display = 'block';
//            document.getElementById('reportDiv').style.display = 'block';
            var myJsonObj = [{"label": "Owner", "value": 50},
                {"label": "Party A", "value": 10},
                {"label": "Party B", "value": 20},
                {"label": "Party C", "value": 15},
                {"label": "Party D", "value": 5}
            ];

            $("#_revData").empty();
            Morris.Donut({
                element: '_revData',
                data: data,
                xkey: 'label',
                ykeys: ['value'],
            });
            revenueTextualReport();
            revenueGraphicalReport();

        }
    });
}

function revenueTextualReport()
{
    var resownname = document.getElementById('_revOwnername').value;
    var resId = document.getElementById('_revResource').value;
    var month = document.getElementById('_revMonth').value;
    var year = document.getElementById('_revYear').value;

    var s = './revenueTextReport.jsp?resownname=' + resownname + '&resId=' + resId + '&month=' + month + "&year=" + year;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
//            waiting.modal('hide');
            $('#_revTextData').html(data);
        }
    });
}

function revenueGraphicalReport()
{

    var resownname = document.getElementById('_revOwnername').value;
    var resId = document.getElementById('_revResource').value;
    var month = document.getElementById('_revMonth').value;
    var year = document.getElementById('_revYear').value;

    var dayData = DayGraph(resownname, resId, month, year);

    Morris.Line({
        element: '_revGraphData',
        data: dayData,
        xkey: 'label',
        xLabelAngle: 45,
        padding: 50,
        xLabelMargin: 5,
        parseTime: false,
        ykeys: ['value'],
        labels: ['value'],
    });

    var weekData = WeekGraph(resownname, resId, month, year);
    Morris.Bar({
        element: '_revBarData',
        data: weekData,
        xkey: 'label',
        parseTime: 'false',
        ykeys: ['value'],
        labels: ['value'],
    });

//    waiting.modal('hide');
}


function DayGraph(resownname, resId, month, year) {
    var s = './AreaChart?resownname=' + resownname + '&resId=' + resId + '&month=' + month + '&year=' + year;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;

    if (jsonData.length == 0) {
        var date = new Date();
        date.setDate(date.getDate() - 1);
        var yesterDay = (date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear());
        jsonData = [{"label": yesterDay, "value": 0}];
        return jsonData;
    } else {
        var myJsonObj = jsonParse(jsonData);
        return myJsonObj;
    }
}

function WeekGraph(resownname, resId, month, year) {
    var s = './BarChart?resownname=' + resownname + '&resId=' + resId + '&month=' + month + '&year=' + year;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;

    if (jsonData.length == 0) {
        var date = new Date();
        date.setDate(date.getDate() - 1);
        var yesterDay = (date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear());
        jsonData = [{"label": yesterDay, "value": 0}];
        return jsonData;
    } else {
        var myJsonObj = jsonParse(jsonData);
        return myJsonObj;
    }
}


function revenuePDFDownload(reportType) {
    var resownname = document.getElementById('_revOwnername').value;
    var resId = document.getElementById('_revResource').value;
    var month = document.getElementById('_revMonth').value;
    var year = document.getElementById('_revYear').value

    var s = './RevenueReports?_format=' + reportType + "&_resownname=" + resownname + " &_resId=" + resId + "&_month=" + month + "&_year=" + year;
    window.location.href = s;
    return false;
}

function revenueTextualAndGraphicalReport()
{
    var resownname = document.getElementById('_revOwnername').value;
    var resId = document.getElementById('_revResource').value;
    var month = document.getElementById('_revMonth').value;
    var year = document.getElementById('_revYear').value;
    if (resownname === '-1') {
        showAlert('Please Select resource owner first !', "danger", 4000);
        return;
    }
    if (resId === '-1') {
        showAlert('Please Select resource first !', "danger", 4000);
        return;
    }
    var s = './revenueTextReport.jsp?resownname=' + resownname + '&resId=' + resId + '&month=' + month + "&year=" + year;

    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#revGraphicalAndTxt').html(data);

            var donutData = DountGraph(resownname, resId, month, year)
            Morris.Donut({
                element: '_revData',
                data: donutData,
                xkey: 'label',
                ykeys: ['value'],
            });

            var dayData = DayGraph(resownname, resId, month, year);
            Morris.Line({
                element: '_revGraphData',
                data: dayData,
                xkey: 'label',
                xLabelAngle: 45,
                padding: 50,
                xLabelMargin: 5,
                parseTime: false,
                ykeys: ['value'],
                labels: ['value'],
            });

            var weekData = WeekGraph(resownname, resId, month, year);
            Morris.Bar({
                element: '_revBarData',
                data: weekData,
                xkey: 'label',
                parseTime: 'false',
                ykeys: ['value'],
                labels: ['value'],
            });
        }
    });
}

function DountGraph(resownname, resId, month, year) {
    var s = "./GenerateRevenueReports?resownname=" + resownname + "&resId=" + resId + "&month=" + month + "&year=" + year;
    var jsonData = $.ajax({
        url: s,
        dataType: "json",
        async: false
    }).responseText;
    var myJsonObj = jsonParse(jsonData);
    return myJsonObj;
}
function editPaymentModel(paymentId,subscriptionId) {
    document.getElementById("editPaymentModel").innerHTML = "Edit Payment Details ";
    //document.getElementById("receiptNumber").value = number;
    document.getElementById("paymentId").value = paymentId;
    document.getElementById("subscriptionId").value=subscriptionId;
    $('#editPayment').modal();
    $('#datetimepicker3').datepicker({
        language: 'pt-BR'
    });
    $('#datetimepicker3').on('changeDate', function (ev) {
        $(this).datepicker('hide');
    });
}
function sapBillUpload() {
    var s = './UploadSAPInvoice';
    var receiptNumber = document.getElementById('receiptNumber').value;
    var _paymentDate = document.getElementById('_paymentDate').value;
    if (receiptNumber === '') {       
        showAlert("Please enter receipt number !", "danger", 3000);
        return;
    }
    if (_paymentDate === '') {        
        showAlert("Please select payment date !", "danger", 3000);
        return;
    }
    pleasewait();
    $.ajaxFileUpload({
        type: 'POST',
        fileElementId: 'SAPBill',
        url: s,
        dataType: 'json',
        success: function (data) {
            if (strCompare(data.result, "error") === 0) {
                waiting.modal('hide');                
                showAlert(data.message, "danger", 4000);                
            } else if (strCompare(data.result, "success") === 0) {
                //$('#_filename').val(data.filename);
                editPaymentDetails();
            }
        },
        error: function (data, status, e)
        {
            //waitingV1.modal('hide');
            alert(e);
        }
    });
 }

function editPaymentDetails() {    
    var s = './UpdatePaymentDetailsManually';
    
    //pleasewait();
    $.ajax({
        type: 'POST',
        url: s,
        datatype: 'json',
        data:$("#editPaymentForm").serialize(),
        success: function (data) {
            if (strCompare(data._result, "error") === 0) {                
                waiting.modal('hide');                
                showAlert(data._message, "danger", 4000);
            } else if (strCompare(data._result, "success") === 0) {
                waiting.modal('hide');
                $('#editPayment').modal('hide');
                showAlert(data._message, "success", 3000);
                refreshManualPaymentList(data._message);
            }
        }
    });
}


function manualPayment() {
    var waiting = $('<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="popup" class="modal fade">' +
            '<div class="modal-dialog"><div class="modal-content"><div class="modal-body">' + '<h4>Please Wait...</h4>' +
            '<div class="progress progress-striped active">' +
            '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="width: 100%">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div></div></div></div>');
    waiting.modal();
    var start = document.getElementById('_startdate').value;
    var end = document.getElementById('_enddate').value;
    var partnerName = document.getElementById('_partnername').value;
    var s = './getManualPaymentList.jsp?_startdate=' + start + "&_enddate=" + end + "&_partnername=" + partnerName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            waiting.modal('hide');
            $('#manualPaymentResult').html(data);
        }
    });
}

function refreshManualPaymentList(_message){
    var start = document.getElementById('_startdate').value;
    var end = document.getElementById('_enddate').value;
    var partnerName = document.getElementById('_partnername').value;
    var s = './getManualPaymentList.jsp?_startdate=' + start + "&_enddate=" + end + "&_partnername=" + partnerName;
    $.ajax({
        type: 'GET',
        url: s,
        success: function (data) {
            $('#manualPaymentResult').html(data);
        }
    });
}
function getSAPReceipt(id,developerName) {
    var s = './GetSAPReceipt?id=' + id +"&partnerName="+developerName;
    window.location.href = s;
    return false;
}
