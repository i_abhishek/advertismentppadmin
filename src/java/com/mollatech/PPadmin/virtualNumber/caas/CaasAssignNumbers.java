/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.virtualNumber.caas;

import com.mollatech.serviceguard.nucleus.db.PartnerDetails;
import com.mollatech.serviceguard.nucleus.db.SgCassNumbers;
import com.mollatech.serviceguard.nucleus.db.connector.management.CaaSManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author Ashu
 */
@WebServlet(name = "CaasAssignNumbers", urlPatterns = {"/CaasAssignNumbers"})
public class CaasAssignNumbers extends HttpServlet {

    static final Logger logger = Logger.getLogger(CaasAssignNumbers.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        logger.info("Requested Servlet is UpdateOwnerInfo at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Assign Number successfully";
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        String[] numbers = request.getParameterValues("NumberForPartners");
        logger.debug("Value of number " + numbers);
        String partnerName = request.getParameter("partnerde");
        logger.debug("Value of PartnerName " + partnerName);
        String numberType = request.getParameter("numberType");
        logger.debug("Value of numberType " + numberType);
        String res = ",";
        PartnerDetails partner = new PartnerManagement().PartnerDetailsbyName(SessionId, channelId, partnerName);
        if (numberType.equalsIgnoreCase("Test")) {
            if (partner.getCaasNumber() != null && !partner.getCaasNumber().isEmpty()) {
                result = "error";
                message = " Number is already assigned !!";
                try {
                    json.put("_result", result);
                    json.put("_message", message);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    out.print(json);
                    out.flush();
                    return;
                }
            }
        } else if (partner.getCaasNumberLive() != null && !partner.getCaasNumberLive().isEmpty()) {
            result = "error";
            message = " Number is already assigned !!";
            try {
                json.put("_result", result);
                json.put("_message", message);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                out.print(json);
                out.flush();
                return;
            }
        }
        CaaSManagement nm = new CaaSManagement();
        String alreadyAssign = partner.getCaasNumber();
        if (!numberType.equalsIgnoreCase("Test")) {
            alreadyAssign = partner.getCaasNumberLive();
        }
        String[] assignNumArr = null;
        boolean setToFreeFlag = false;
        List<String> list = null;
        TreeSet<String> set = null;
        if (alreadyAssign != null && !alreadyAssign.isEmpty()) {
            assignNumArr = alreadyAssign.split(",");
        }
        if (assignNumArr != null) {
            list = new ArrayList<String>(Arrays.asList(assignNumArr));
            set = new TreeSet<String>(list);
            set.removeAll(Arrays.asList(numbers));
            System.out.println(set);
            setToFreeFlag = true;
        }
        for (int i = 0; i < numbers.length; i++) {
            res += numbers[i] + ",";
            SgCassNumbers num = nm.getNumberById(Integer.parseInt(numbers[i]));
            num.setPartnerId(partner.getPartnerId());
            num.setChannelId(channelId);
            nm.updateDetails(num);
        }
        if (setToFreeFlag) {
            for (String freeNum : set) {
                if (!freeNum.isEmpty()) {
                    SgCassNumbers freeNumObj = nm.getNumberById(Integer.parseInt(freeNum));
                    freeNumObj.setPartnerId(null);
                    freeNumObj.setChannelId(null);
                    nm.updateDetails(freeNumObj);
                }
            }
        }
        if (numberType.equalsIgnoreCase("Test")) {
            partner.setCaasNumber(res);
        } else {
            partner.setCaasNumberLive(res);
        }
        int partRes = new PartnerManagement().updateDetails(partner);
        if (partRes == 0) {
            result = "success";
            message = "Assign Number successfully";
        } else {
            result = "error";
            message = " No any number assign to Developer  !";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
