package com.mollatech.PPadmin.virtualNumber;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.SgNumbers;
import com.mollatech.serviceguard.nucleus.db.connector.management.NumbersManagement;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import org.apache.commons.fileupload.FileItem;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;

/**
 *
 * @author Manoj Sherkhane
 */
@MultipartConfig
@WebServlet(name = "UploadServlet", urlPatterns = {"/UploadServlet"})
public class UploadServlet extends HttpServlet {

    static final Logger logger = Logger.getLogger(UploadServlet.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UploadServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UploadServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int res = 0;
        int duplicateNo = 0;
        int notNumber = 0;
        String rex = "^\\+?\\d+$";
        FileItem fileItem = null;
        String result = "success";
        String message = "Record inserted successfully";
        String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        logger.debug("value of sessionId in UploadServlet : " + sessionId);
        String partchannelID = (String) request.getSession().getAttribute("_channelId");
        logger.debug("value of partchannelID in UploadServlet : " + partchannelID);
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        Part file = request.getPart("file");
        logger.debug("value of file : " + file);
        String filename = getFilename(file);
        Workbook workbook = null;
        InputStream excelFile = file.getInputStream();
//        FileInputStream excelFile = new FileInputStream(new File(filename));
        if (filename.endsWith("xlsx")) {
            workbook = new XSSFWorkbook(excelFile);
        } else if (filename.endsWith("xls")) {
            workbook = new HSSFWorkbook(excelFile);
        }
        Sheet datatypeSheet = workbook.getSheetAt(0);
        Iterator<Row> iterator = datatypeSheet.iterator();
        Map NumbersMap = new LinkedHashMap();
        String numberS = "";
        while (iterator.hasNext()) {

            Row currentRow = iterator.next();
            Iterator<Cell> cellIterator = currentRow.iterator();

            while (cellIterator.hasNext()) {

                Cell currentCell = cellIterator.next();
                if (currentCell.getCellTypeEnum() == CellType.STRING) {
//                    System.out.print(currentCell.getStringCellValue() + "--");
                    if (currentCell.getStringCellValue().equalsIgnoreCase("Number") || currentCell.getStringCellValue().equalsIgnoreCase("Type")) {

                    } else if (currentCell.getStringCellValue().equalsIgnoreCase("LIVE") || currentCell.getStringCellValue().equalsIgnoreCase("TEST")) {
                        NumbersMap.put(numberS, currentCell.getStringCellValue());
                        numberS = "";
                    } else {
                        numberS = currentCell.getStringCellValue();
                    }
                }

            }
        }
//        System.out.println(NumbersMap);
//        InputStream filecontent = file.getInputStream();
        SgNumbers mobileNumber = null;
        try {
            int i = 0;
            if (sessionId != null && !NumbersMap.isEmpty()) {
                for (Object no : NumbersMap.keySet()) {
                    if (!(no.toString().replace("-", "").replace("+", "").matches(rex))) {
                        notNumber++;
                    }
                }
                if (notNumber != 0) {
                    result = "error";
                    message = " File have invalid numbers !";
                } else {
                    SgNumbers[] check = new NumbersManagement().getAllNumbers();
                    String oldNumbers = ",";
                    if (check != null) {
                        for (int k = 0; k < check.length; k++) {
                            oldNumbers += check[k].getMobileNo() + ",";
                        }
                    }
                    for (Object no : NumbersMap.keySet()) {

                        if (!oldNumbers.contains(no.toString())) {
                            mobileNumber = new SgNumbers();
                            oldNumbers += (no.toString()) + ",";
                            mobileNumber.setMobileNo(no.toString());
                            mobileNumber.setType((String) NumbersMap.get(no));
                            mobileNumber.setCreatedOn(new Date());
                            mobileNumber.setUpdatedOn(new Date());
                            mobileNumber.setChannelId((String) request.getSession().getAttribute("_channelId"));
                            mobileNumber.setStatus(GlobalStatus.ACTIVE);
                            mobileNumber.setPlainNumber(no.toString().replace("-", ""));
                            int resCount = new NumbersManagement().addNumber(mobileNumber, sessionId);
                            if (resCount == 0) {
                                res++;
                            }
                        } else {
                            duplicateNo++;
                        }
                    }
                    System.out.println(res + "===" + duplicateNo + "<======");
                    if (res != 0 && duplicateNo != 0) {
                        result = "success";
                        message = +res + " Record added successfully and " + duplicateNo + " ignored";
                    } else if (res != 0) {
                        result = "success";
                        message = +res + " Record added successfully !";
                    } else {
                        result = "error";
                        message = " Record already exist !";
                    }
                }

            } else {
                result = "error";
                message = "File Does Not Contain Proper Data.";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }

    }

    private static String getFilename(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                String filename = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                return filename.substring(filename.lastIndexOf('/') + 1).substring(filename.lastIndexOf('\\') + 1); // MSIE fix.
            }
        }
        return null;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
