<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessEnvtManagement"%>
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceOwnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.EmailManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgEmailticket"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPromocode"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PromocodeManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgBucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.MakerChaker"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SettingsManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPartnerrequest"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerRequestManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Operators"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SessionManagement"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Admin Portal</title>
        <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="dist/css/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="dist/css/sb-admin-2.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="bower_components/morrisjs/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- DataTables CSS -->
<!--        <link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="bower_components/datatables-responsive/css/responsive.bootstrap.scss" rel="stylesheet" type="text/css">-->
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="js/jquery.js"></script>
        <script src="js/jquery-ui-1.10.4.min.js"></script>
        <script src="js/jquery-1.8.3.min.js"></script>
        <!--        <script src="js/bootstrap.min.js"></script> -->
        <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
        <!--        <script src="js/bootbox.min.js" type="text/javascript"></script>-->
        <script src="./js/modal.js"></script>
        <link  rel="stylesheet" href="//cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
        <link  rel="stylesheet" href="//cdn.datatables.net/fixedheader/3.1.2/css/fixedHeader.bootstrap.min.css">
        <link  rel="stylesheet" href="//cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css">       

        <%
            final int ADMIN = 0;
            final int OPERATOR = 1;
            final int REPORTER = 2;
            final int SUSPENED = 0;
            long partnerProductionRequest = 0;
            long resourceOwnerRequest = 0;
            // new change
            long partnerRequest = 0;
            response.setHeader("Cache-Control", "no-cache,must-revalidate"); //Forces caches to obtain a new copy of the page from the origin server
            response.setHeader("Cache-Control", "no-store"); //Directs caches not to store the page under any circumstance
            response.setDateHeader("Expires", 0); //Causes the proxy cache to see the page as "stale"
            response.setHeader("Pragma", "no-cache"); //HTTP 1.0 backward compatibility
            String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
            if (SessionId == null) {
        %>
        <jsp:forward page="operatorlogin.jsp" />

        <%     }%>
        <%
            String channelId = (String) request.getSession().getAttribute("_channelId");
            // new change
            Operators opObj = null;
            String name = null;
            opObj = (Operators) request.getSession().getAttribute("_apOprDetail");
            name = opObj.getName();

            //partnerProductionRequest = new ProductionAccessManagement().getRowCount(SessionId, channelId);
            partnerProductionRequest = new ProductionAccessEnvtManagement().getRowCount(SessionId, channelId);
            resourceOwnerRequest = new ResourceOwnerManagement().getRowCount(SessionId, channelId);
            partnerRequest = new PartnerRequestManagement().getRowCountByStatus(SessionId, channelId, GlobalStatus.PENDING);

            String mailBoxFunctionality = LoadSettings.g_sSettings.getProperty("functionalityOf.ticket.email.booleanValue");
            String promocodeFunctionality = LoadSettings.g_sSettings.getProperty("functionalityOf.feature.promocode.booleanValue");
        %>
    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="home.jsp"><b>Admin Portal</b></a>
                </div>
                <!-- /.navbar-header -->

                <ul class="nav navbar-top-links navbar-right">
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"> 
                            <i class="fa fa-users fa-fw"></i>
                            <span class="badge bg-important"><%=partnerRequest%></span>
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li>
                                <a href="partnerRequest.jsp">
                                    <div>
                                        <i class="fa fa-comment fa-fw"></i> New Developer Request
                                        <span class="pull-right text-muted small"></span>
                                    </div>
                                </a>
                            </li>                                                     
                        </ul>

                    </li>
                    <%
                        if (mailBoxFunctionality != null && !mailBoxFunctionality.isEmpty() && mailBoxFunctionality.equalsIgnoreCase("true")) {
                            int newEmailcount = 0;
                            String partID = (opObj.getOperatorid());
                            SgEmailticket[] emaildetails = new EmailManagement().getCountByOperatorId(SessionId, partID);
                            if (emaildetails != null) {
                                //                            newEmailcount = emaildetails.length;
                                for (int j = 0; j < emaildetails.length; j++) {
                                    if (emaildetails[j].getDeletedByOperator() == GlobalStatus.SUCCESS) {
                                        newEmailcount++;
                                    }
                                }
                            }
                    %>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="glyphicon glyphicon-envelope"></i>
                            <span class="badge bg-important"><%=newEmailcount%></span>
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li>
                                <a href="ticketInboxDetail.jsp">
                                    <div>
                                        <i class="fa fa-comment fa-fw"></i> New Email
                                        <span class="pull-right text-muted small"></span>
                                    </div>
                                </a>
                            </li>                                          
                        </ul>    
                    </li>
                    <%}%>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="glyphicon glyphicon-user"></i>
                            <span class="badge bg-important"><%=resourceOwnerRequest%></span>
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li>
                                <a href="resourceOwnerManager.jsp">
                                    <div>
                                        <i class="fa fa-comment fa-fw"></i> Resourceowner request
                                        <span class="pull-right text-muted small"></span>
                                    </div>
                                </a>
                            </li>                                          
                        </ul>    
                    </li>

                    <%
                        Object setting = new SettingsManagement().getSetting(channelId, SettingsManagement.MAKERCHAKER, SettingsManagement.PREFERENCE_ONE);
                        MakerChaker makerChakerObj = (MakerChaker) setting;
                        session.setAttribute("makerChacker", makerChakerObj);
                        if (makerChakerObj != null) {
                            if (makerChakerObj.chaker.contains(opObj.getEmailid()) || makerChakerObj.status == GlobalStatus.SUSPEND) {
                                int sglength11 = 0;
                                SgReqbucketdetails[] sgrequest1 = null;
                                sgrequest1 = new RequestPackageManagement().listPackageRequestsbystatus(SessionId, 1, 2);
                                if (sgrequest1 != null) {
                                    sglength11 = sgrequest1.length;
                                }
                                int sgPromocodeLength = 0;
                                SgPromocode[] sgpromocode = null;
                                sgpromocode = new PromocodeManagement().listPromocodeByReqFlag(SessionId, channelId, GlobalStatus.SUSPEND, GlobalStatus.SENDTO_CHECKER);
                                if (sgpromocode != null) {
                                    sgPromocodeLength = sgpromocode.length;
                                }
                    %>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">

                            <i class="fa fa-shopping-cart fa-fw"></i>
                            <span class="badge bg-important"><%=sglength11%></span>
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li>
                                <a href="packageRequest.jsp">
                                    <div>
                                        <i class="fa fa-comment fa-fw"></i> New Package Request
                                        <span class="pull-right text-muted small"></span>
                                    </div>
                                </a>
                            </li>                                          
                        </ul>
                    </li>
                    <%if (promocodeFunctionality != null && !promocodeFunctionality.isEmpty() && promocodeFunctionality.equalsIgnoreCase("true")) {%>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-gears fa-fw"></i>
                            <span class="badge bg-important"><%=sgPromocodeLength%></span>
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li>
                                <a href="promocodeRequest.jsp">
                                    <div>
                                        <i class="fa fa-comment fa-fw"></i> New Promo Code Request
                                        <span class="pull-right text-muted small"></span>
                                    </div>
                                </a>
                            </li>                            
                        </ul>
                    </li>
                    <%}
                            }
                        }
                    %>

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-gears fa-fw"></i>
                            <span class="badge bg-important"><%=partnerProductionRequest%></span>
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li>
                                <a href="developerProductionAccessRequest.jsp">
                                    <div>
                                        <i class="fa fa-comment fa-fw"></i> New Production Access Request
                                        <span class="pull-right text-muted small"></span>
                                    </div>
                                </a>
                            </li>                            
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i> <span class="username"><%=name%></span>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">                            
                            <li><a href="oplogout.jsp"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
                <!-- /.navbar-top-links -->

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">

                            <li>
                                <a href="home.jsp"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                            </li>
                            <li>
                                <a href="services.jsp"><i class="fa fa-code fa-fw"></i> Service Manager</a>
                            </li>
<!--                            <li>
                                <a href="partnerRequest.jsp"><i class="fa fa-edit fa-fw"></i> Developer Request</a>
                            </li>-->
                            <li>
                                <a href="developerProductionAccessRequest.jsp"><i class="fa fa-edit fa-fw"></i> Production Access Request</a>
                            </li>
                            <li>
                                <a href="partners.jsp"><i class="fa fa-group fa-fw"></i> Developer Manager</a>
                            </li>
<!--                            <li>
                                <a href="productionAccManager.jsp"><i class="fa fa-group fa-fw"></i> Production Access Manager</a>
                            </li>-->
                            <%
                                if (makerChakerObj != null) {
                                    if (makerChakerObj.maker.contains(opObj.getEmailid())) {
                                        if (promocodeFunctionality != null && !promocodeFunctionality.isEmpty() && promocodeFunctionality.equalsIgnoreCase("true")) {%>
                            <li>
                                <a href="#"><i class="fa fa-money fa-fw"></i> Billing Manager<span class="fa arrow"></span></a>        
                                <ul class="nav nav-second-level">                       
                                    <li>                                  
                                        <a href="package.jsp">Package Manager</a>    
                                    </li>                              
                                    <li>                               
                                        <a href="promocodelist.jsp">Promo Code</a>      
                                    </li>                              
                                </ul>
                            </li>
                            <%} else {%>
                            <li>
                                <a href="package.jsp"><i class="fa fa-money fa-fw"></i> Package Manager</a>
                            </li>
                            <li>
                                <a href="advertiserPackage.jsp"><i class="fa fa-money fa-fw"></i> Adv Package Manager</a>
                            </li>
                            <li>
                                <a href="pdfAdRequest.jsp"><i class="fa fa-money fa-fw"></i> PDF Ad Request</a>
                            </li>
                            <li>
                                <a href="emailAdRequest.jsp"><i class="fa fa-money fa-fw"></i> Email Ad Request</a>
                            </li>
                            <% }
                            } else if (makerChakerObj.status == SUSPENED) {
                                if (promocodeFunctionality != null && !promocodeFunctionality.isEmpty() && promocodeFunctionality.equalsIgnoreCase("true")) {
                            %>
                            <li>
                                <a href="#"><i class="fa fa-money fa-fw"></i> Billing Manager<span class="fa arrow"></span></a>   
                                <ul class="nav nav-second-level">                        
                                    <li>                                  
                                        <a href="package.jsp">Package Manager</a>         
                                    </li>            
                                    <li>                        
                                        <a href="promocodelist.jsp">Promo Code</a>        
                                    </li>                         
                                </ul>
                            </li>
                            <%} else {%>
                            <li>
                                <a href="package.jsp"><i class="fa fa-money fa-fw"></i> Package Manager</a>
                            </li>
                            <li>
                                <a href="advertiserPackage.jsp"><i class="fa fa-money fa-fw"></i> Adv Package Manager</a>
                            </li>
                            <%}
                            } else if (promocodeFunctionality != null && !promocodeFunctionality.isEmpty() && promocodeFunctionality.equalsIgnoreCase("true")) {
                            %>
                            <li>
                                <a href="#"><i class="fa fa-money fa-fw"></i> Billing Manager<span class="fa arrow"></span></a>   
                                <ul class="nav nav-second-level">                        
                                    <li>                                  
                                        <a href="packageForChecker.jsp">Package Manager</a>         
                                    </li> 
                                    <li>
                                        <a href="adPackageForChecker.jsp"><i class="fa fa-money fa-fw"></i> Adv Package Manager</a>
                                    </li>
                                    <li>                        
                                        <a href="PromocodelistForChecker.jsp">Promo Code</a>        
                                    </li>                         
                                </ul>
                            </li>
                            <%} else {%>
                            <li>
                                <a href="packageForChecker.jsp"><i class="fa fa-money fa-fw"></i> Package Manager</a>
                            </li>
                            <li>
                                <a href="adPackageForChecker.jsp"><i class="fa fa-money fa-fw"></i> Adv Package Manager</a>
                            </li>
                            <li>
                                <a href="pdfAdRequest.jsp"><i class="fa fa-money fa-fw"></i> PDF Ad Request</a>
                            </li>
                            <li>
                                <a href="emailAdRequest.jsp"><i class="fa fa-money fa-fw"></i> Email Ad Request</a>
                            </li>
                            <%}
                            } else if (promocodeFunctionality != null && !promocodeFunctionality.isEmpty() && promocodeFunctionality.equalsIgnoreCase("true")) {
                            %>
                            <li>
                                <a href="#"><i class="fa fa-money fa-fw"></i> Billing Manager<span class="fa arrow"></span></a>     
                                <ul class="nav nav-second-level">    
                                    <li>                       
                                        <a href="package.jsp">Package Manager</a>       
                                    </li>                                 
                                    <li>                                
                                        <a href="promocodelist.jsp">Promo Code</a>         
                                    </li>                       
                                </ul>
                            </li>     
                            <%} else {%>
                            <li>
                                <a href="package.jsp"><i class="fa fa-money fa-fw"></i> Package Manager</a>
                            </li>
                            <%}
                            %>                           
<!--                            <li>
                                <a href="manualPayment.jsp"><i class="fa fa-money fa-fw"></i> Manual Payment</a>
                            </li>-->
                            <li>
                                <a href="addCredits.jsp"><i class="fa fa-money fa-fw"></i> Add Credits</a>
                            </li>
                            
                            <li>
                                <a href="partyDetails.jsp"><i class="fa fa-group fa-fw"></i> Party Manager</a> 
                                <!--                                <ul class="nav nav-second-level">    
                                                                    <li>                       
                                                                        <a href="partyDetails.jsp">Party Manager</a>       
                                                                    </li>                                 
                                                                    <li>                                
                                                                        <a href="mapPartyWithResource.jsp">Map Parties</a>         
                                                                    </li>                       
                                                                </ul>                               -->
                            </li>
                            <li>
                                <a href="resourceOwnerManager.jsp"><i class="fa fa-group fa-fw"></i> Resource Owner Manager</a>
                            </li>                            
                            <li>
                                <a href="resourceAPIManager.jsp"><i class="fa fa-gears"></i> Resource API Manager</a>
                            </li>
<!--                            <li>
                                <a href="#"><i class="fa fa-list-ul"></i> Infoblast Number Manager <span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="uploadNumberFile.jsp"><i class="fa fa-upload"></i> Upload Numbers </a>
                                    </li>
                                    <li>
                                        <a href="editNumber.jsp"><i class="glyphicon glyphicon-edit"></i> Edit Numbers </a>
                                    </li>
                                    <li>
                                        <a href="assignNumbers.jsp"><i class="fa fa-list"></i> Assign Numbers </a>
                                    </li>

                                </ul>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-list-ul"></i> CaaS Number Manager <span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="caasNumberFile.jsp"><i class="fa fa-upload"></i> Upload Numbers </a>
                                    </li>
                                    <li>
                                        <a href="caasEditNumber.jsp"><i class="glyphicon glyphicon-edit"></i> Edit Numbers </a>
                                    </li>
                                    <li>
                                        <a href="caasAssignNumbers.jsp"><i class="fa fa-list"></i> Assign Numbers </a>
                                    </li>

                                </ul>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-list-ul"></i> CDR Manager <span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="uploadCDR.jsp"><i class="fa fa-upload"></i> Upload CDR </a>
                                    </li>
                                    <li>
                                        <a href="ListOfCDRData.jsp"><i class="glyphicon glyphicon-edit"></i> View CDR </a>
                                    </li>

                                </ul>
                            </li>-->
                            <%if (mailBoxFunctionality != null && !mailBoxFunctionality.isEmpty() && mailBoxFunctionality.equalsIgnoreCase("true")) { %>
                            <li>
                                <a href="#"><i class="fa fa-table fa-fw"></i> Mail Box<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="ticketComposeMail.jsp"><i class="fa fa-compass"></i> Compose </a>
                                    </li>
                                    <li>
                                        <a href="ticketInboxDetail.jsp"><i class="fa fa-inbox"></i> Inbox </a>
                                    </li>
                                    <li>
                                        <a href="ticketOutboxEmail.jsp" class=""><i class="fa fa-outdent"></i> Outbox </a>
                                    </li>
                                </ul>                                
                            </li>
                            <% }%>
                            <li>
                                <a href="#"><i class="fa fa-list-ul"></i> Settlement Manager<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="settlement.jsp"><i class="fa fa-group fa-fw"></i> By Resource</a>
                                    </li>
                                    <li>
                                        <a href="settlementowner.jsp"><i class="fa fa-list"></i> By Owner</a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="#"><i class="fa fa-line-chart fa-fw"></i> Graphical Reports<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="operatorReports.jsp?_type=1&_opType=1">Transaction Report</a>
                                    </li>
                                    <li>
                                        <a href="operatorReports.jsp?_type=2&_opType=1">Performance Report</a>
                                    </li>
                                    <li>
                                        <a href="operatorMonitoringReports.jsp">Monitoring Report</a>
                                    </li>
                                    <li>
                                        <a href="operatorMonitoringRealTime.jsp">Real Time Monitoring</a>
                                    </li> 
                                    <li>
                                        <a href="APIUsageReport.jsp">API Usage Report</a>
                                    </li> 
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-table fa-fw"></i> Textual Reports<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="operatorTextReports.jsp?_opType=1&_type=1">Transaction Report</a>
                                    </li>
                                    <li>
                                        <a href="MonitoringTextReports.jsp?&type=3">Monitoring Report</a>
                                    </li>
<!--                                    <li>
                                        <a href="InvoiceReports.jsp">Invoice Report</a>
                                    </li>-->
<!--                                    <li>
                                        <a href="GSTbillingreport.jsp">Tax Report</a>
                                    </li>-->
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>
        </div>
    </body>
</html>
<!-- /.row -->
