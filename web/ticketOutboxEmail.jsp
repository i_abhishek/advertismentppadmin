<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.UtilityFunctions"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgEmailticket"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.TicketEmailSendingUtil"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.EmailManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.GroupDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.GroupManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgUsers"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement"%>
<%@page import="javax.xml.datatype.XMLGregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@include file="header.jsp" %>


<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header text-uppercase">Outbox</h3>
            </div>
            <!-- /.col-lg-10 -->
        </div>
        <div class="row">
        <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-home"></i><a href="home.jsp"> Home </a>&#47;  <i class="fa fa-table"></i><b> Outbox</b>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal " method="get">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th style="text-align: center">To</th>
                                        <th style="text-align: center">Subject</th>
                                        <th style="text-align: center">Date</th>
                                        <th style="text-align: center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <%
                                Operators operators = (Operators) request.getSession().getAttribute("_apOprDetail");
                                String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
                                String channelID = (String) request.getSession().getAttribute("_channelId");
                                String sendBy = operators.getOperatorid();
                                String emailNo ; boolean allDeletedFlag = true;
                                
                                SgEmailticket [] ticketDetails = new EmailManagement().getAllEmailDetailsBySenderId(sessionId, channelID, sendBy);
                                if(ticketDetails != null){
                                   for(int i = 0; i < ticketDetails.length; i++){
                                       String sendTo = ticketDetails[i].getEmailTo();
                                       int partnerId = Integer.parseInt(sendTo);
                                       PartnerDetails partnerdetail = new PartnerManagement().getPartnerDetails(partnerId);
                                       emailNo = ticketDetails[i].getId();
                                String createdOn = new UtilityFunctions().getTMReqDate(ticketDetails[i].getCreatedon());
                                        if(ticketDetails[i].getDeletedByOperator() != GlobalStatus.DELETED){
                                            allDeletedFlag = false;
                                %>
                                <tr class="center">
                                    <td style="text-align: center; cursor: pointer" onclick="window.location='ticketViewEmail.jsp?_emailNo=<%=ticketDetails[i].getId()%>&emailFrom=<%=partnerdetail.getPartnerEmailid()%>';"><%=partnerdetail.getPartnerEmailid()%></td>
                                    <td style="max-width: 300px; text-align: center; cursor: pointer" onclick="window.location='ticketViewEmail.jsp?_emailNo=<%=ticketDetails[i].getId()%>&emailFrom=<%=partnerdetail.getPartnerEmailid()%>';"><%=ticketDetails[i].getSubject()%></td>
                                    <td style="text-align: center; cursor: pointer" onclick="window.location='ticketViewEmail.jsp?_emailNo=<%=ticketDetails[i].getId()%>&emailFrom=<%=partnerdetail.getPartnerEmailid()%>';"><%=createdOn%></td>
                                    <td style="text-align: center">  
                                        <!--<font style="font-size: 11px"><a class="btn btn-info btn-xs" href="ticketViewEmail.jsp?_emailNo=<%=ticketDetails[i].getId()%>&emailFrom=<%=partnerdetail.getPartnerEmailid()%>"><i class="fa fa-eye"></i> view</a> </font>-->
                                    <font style="font-size: 11px"><a class="btn btn-danger btn-xs" onclick="deleteEmailById('<%=emailNo%>')"><i class="fa fa-dropbox"></i> Delete</a> </font></td>
                                </tr>
                               
                                      <% }}} else{%>
                                    <td style="text-align: center;">No Records found</td>
                                    <td style="text-align: center;">No Records found</td>
                                    <td style="text-align: center;">No Records found</td>
                                    <td style="text-align: center;">No Records found</td>
                                  <%  }%>  
                                  
                                </tbody>
                            </table></div></form></div></div></div></div></div></div>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="./js/ticketmanagement.js"></script>
<script src="js/bootbox.min(1).js" type="text/javascript"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>

</body>
</html>
<%@include file="footer.jsp" %>