<%@page import="com.mollatech.serviceguard.nucleus.db.SgCassNumbers"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.CaaSManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.NumbersManagement"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%
    String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
    String channelid = (String) request.getSession().getAttribute("_channelId");
//            PartnerDetails[] partnerDetails = new PartnerManagement().getAllPartnerDetails(sessionId, channelid);
    String partName = request.getParameter("partnerName");
    String type = request.getParameter("type");
    SgCassNumbers[] numberDetails = new CaaSManagement().getAllNumbersByEnv(type);

    String assignedNumbers = ",";
    PartnerDetails partDetails = new PartnerManagement().PartnerDetailsbyName(sessionId, channelid, partName);
    if (partDetails == null) {
        partDetails = new PartnerDetails();
    }
    if (partDetails.getCaasNumber() != null) {
        String[] virtualNumberOfPartner = partDetails.getCaasNumber().split(",");
        SgCassNumbers[] num = new CaaSManagement().getAllNumbers();
        for (int i = 0; i < virtualNumberOfPartner.length; i++) {
            if (!virtualNumberOfPartner[i].equals("")) {
                for (SgCassNumbers no : num) {
                    String getNumber = (no.getNumberId()).toString();
                    if ((getNumber).equals(virtualNumberOfPartner[i])) {
                        assignedNumbers += no.getNumberId() + ",";
                        break;
                    }
                }
            }
        }
    }

%>
<div class="col-lg-8">
    <select id="NumberForPartners" name="NumberForPartners" multiple="multiple" style="width: 100%">       
        <%if (numberDetails != null) {
                for (SgCassNumbers details : numberDetails) {
                    if (assignedNumbers.contains("," + details.getNumberId() + ",")) {
                        if (details.getStatus() == GlobalStatus.ACTIVE) {
        %>
        <option value="<%=details.getNumberId()%>" selected ><%=details.getNumber()%></option>
        <% }
        } else if (details.getPartnerId() == null && details.getStatus() == GlobalStatus.ACTIVE) {
        %>
        <option value="<%=details.getNumberId()%>" ><%=details.getNumber()%></option>
        <%}

                }
            }
        %>
    </select> 
</div>



