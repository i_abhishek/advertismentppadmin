/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.party;

import com.mollatech.serviceguard.nucleus.db.SgPartydetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartyManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "CheckPartyAvailability", urlPatterns = {"/CheckPartyAvailability"})
public class CheckPartyAvailability extends HttpServlet {
    static final Logger logger = Logger.getLogger(CheckPartyAvailability.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        logger.info("Requested Servlet is CheckPartyAvailability from PPAdmin at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "This party name is available";
        String channels = (String) request.getSession().getAttribute("_channelId");
        
        String _partyName = request.getParameter("_partyName");
        _partyName = _partyName.trim();
        Pattern p1 = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher m1 = p1.matcher(_partyName);
        boolean b1 = m1.find();
        Pattern p2 = Pattern.compile("[0-9]", Pattern.CASE_INSENSITIVE);
        Matcher m2 = p2.matcher(_partyName);
        boolean b2 = m2.find();
        boolean spaces = false;
        if (_partyName != null) {
            if (_partyName.contains(" ")) {
                spaces = true;
            }
        }
        try {
            if (_partyName.trim().length() == 0) {
                result = "error";
                message = "Please Enter party Name.";
                json.put("result", result);
                logger.debug("Response of CheckPartyAvailability Servlet's from PPAdmin Parameter   result is " + result);
                json.put("message", message);
                logger.debug("Response of CheckPartyAvailability Servlet's from PPAdmin Parameter   message is " + message);
                return;
            }  else if (b1) {
                result = "error";
                message = "Name should not contain any symbols.";
                json.put("result", result);
                logger.debug("Response of CheckPartyAvailability Servlet's from PPAdmin Parameter   result is " + result);
                json.put("message", message);
                logger.debug("Response of CheckPartyAvailability Servlet's from PPAdmin Parameter   message is " + message);
                return;
            } else if (b2) {
                result = "error";
                message = "Name should not contain digits.";
                json.put("result", result);
                logger.debug("Response of CheckPartyAvailability Servlet's from PPAdmin Parameter   result is " + result);
                json.put("message", message);
                logger.debug("Response of CheckPartyAvailability Servlet's from PPAdmin Parameter   message is " + message);
                return;
            }
            SgPartydetails[] partyObj = new PartyManagement().getPartyByName(channels, _partyName);
            if (partyObj != null && partyObj.length >= 1) {
                result = "error";
                message = "This party name is not available";
                json.put("result", result);
                logger.debug("Response of CheckPartyAvailability Servlet's from PPAdmin Parameter   result is " + result);
                json.put("message", message);
                logger.debug("Response of CheckPartyAvailability Servlet's from PPAdmin Parameter   message is " + message);
                return;
            } else {
                result = "success";
                message = "This party name is available";
                json.put("result", result);
                logger.debug("Response of CheckPartyAvailability Servlet's from PPAdmin Parameter   result is " + result);
                json.put("message", message);
                logger.debug("Response of CheckPartyAvailability Servlet's from PPAdmin Parameter   message is " + message);
                return;
            }
        } catch (Exception e) {
            logger.error("Exception at CheckPartyAvailability ", e);
            result = "error";
            message = "Error in checking availability";
            json.put("result", result);
            logger.debug("Response of CheckPartyAvailability Servlet's from PPAdmin Parameter   result is " + result);
            json.put("message", message);
            logger.debug("Response of CheckPartyAvailability Servlet's from PPAdmin Parameter   message is " + message);
        } finally {
            logger.info("Response of CheckPartyAvailability from PPAdmin" + json.toString());
            logger.info("Response of CheckPartyAvailability Servlet from PPAdmin at " + new Date());
            out.print(json);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
