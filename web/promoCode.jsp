<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@include file="header.jsp" %>

<link href="select2/select2.css" rel="stylesheet" type="text/css"/>
<script src="js/promocode.js" type="text/javascript"></script>
<%
    String promocodeId = request.getParameter("edit");
    if (promocodeId == null) {
        PartnerDetails[] partnerObj = new PartnerManagement().getAllPartnerDetails(SessionId, channelId);
        String options = "<option value=all>All</option>\n";
        if (partnerObj != null) {
            for (int i = 0; i < partnerObj.length; i++) {
                if (partnerObj[i].getStatus() == GlobalStatus.ACTIVE) {
                    options += "<option value='" + partnerObj[i].getPartnerId() + "'>" + partnerObj[i].getPartnerEmailid() + "</option>\n";
                }
            }
        }
%>
<div id="page-wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Create Promo code</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-home"></i><a href="home.jsp"> Home</a> &#47;                    
                    <i class="fa fa-edit"></i> Promo code
                </div>
                <div class="panel-body">
                    <h4>Promo code details</h4>
                    <hr>                   
                    <form class="form-horizontal" id="promoCode_form" name="promoCode_form" role="form" method="POST">
                        <div class="form-group">
                            <label class="control-label col-lg-2">Code</label>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-cogs"></i></span>
                                    <input type="text" id="_promoCode" name="_promoCode" class="form-control" placeholder="Code" onblur="CheckPromocodeAvailability()">
                                </div>
                                <div id="checkAvailability-result">
                                </div>
                            </div>
                            <label class="control-label col-lg-2">Expiry</label>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon "><i class="fa fa-calendar"></i></span>
                                    <input type="text" id="_promoExpiry" name="_promoExpiry" class="form-control" placeholder="Expire after" onkeypress="return isNumericKey(event)" >
                                </div>
                            </div>                            
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2">Usage Count</label>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                                    <input type="text" id="_usageCount" name="_usageCount" class="form-control" placeholder="Usage Count" onkeypress="return isNumericKey(event)">                                    
                                </div>
                            </div>
                            <label class="control-label col-lg-2">Partner Usage Count</label>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                                    <input type="text" id="_partnerUsageCount" name="_partnerUsageCount" class="form-control" placeholder="Usage Count" onkeypress="return isNumericKey(event)">                                    
                                </div>
                            </div>
                        </div>
                        <div id="_promoDiscountSelect">
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2">For Partner</label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-users"></i></span>
                                    <select id ="visibleTo" name="visibleTo" multiple class="span6" style="width: 100%" >
                                        <%=options%>
                                    </select>                                  
                                </div>
                            </div>
                        </div>                        
                        <a  class="btn btn-success btn-xs" onclick="createPromoCode()" style="margin-left: 17%"><i class="fa fa-plus-circle"></i> Create Promo code</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<%} else {
    int promoId = Integer.parseInt(promocodeId);
    SgPromocode promoObj = new PromocodeManagement().getPromocodeById(SessionId, channelId, promoId);
    String partnerIds = promoObj.getPartnerId();
    boolean flag = false;
    String options = "<option value=all selected>NA</option>\n";
    if (partnerIds != null && !partnerIds.isEmpty() && partnerIds.contains("all")) {
        options = "<option value=all selected>All</option>\n";
    } else if (partnerIds != null && !partnerIds.isEmpty()) {
        String[] parArray = partnerIds.split(",");
        options = "";
        for (int i = 0; i < parArray.length; i++) {
            int parId = Integer.parseInt(parArray[i]);
            String emailId = "";
            PartnerDetails parObj = new PartnerManagement().getPartnerDetails(parId);
            if (parObj != null) {
                emailId = parObj.getPartnerEmailid();
            }
            options += "<option selected value='" + parId + "'>" + emailId + "</option>\n";

        }
        flag = true;
    }

    PartnerDetails[] partnerObj = new PartnerManagement().getAllPartnerDetails(SessionId, channelId);
    if (partnerObj != null) {
        if (flag) {
            options += "<option value=all>All</option>\n";
        }
        for (int i = 0; i < partnerObj.length; i++) {
            if (partnerIds != null && partnerIds.contains(String.valueOf(partnerObj[i].getPartnerId()))) {
                continue;
            }

            if (partnerObj[i].getStatus() == GlobalStatus.ACTIVE) {
                options += "<option value='" + partnerObj[i].getPartnerId() + "'>" + partnerObj[i].getPartnerEmailid() + "</option>\n";
            }
        }
    }

    String discountType = promoObj.getDiscountType();

%>
<div id="page-wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Edit Promo code</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-home"></i><a href="home.jsp"> Home</a> &#47;                    
                    <i class="fa fa-edit"></i> Promo code
                </div>
                <div class="panel-body">
                    <h4>Promo code details</h4>
                    <hr>                   
                    <form class="form-horizontal" id="promoCode_form_edit" name="promoCode_form_edit" role="form" method="POST">
                        <div class="form-group">
                            <label class="control-label col-lg-2">Code</label>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-cogs"></i></span>
                                    <input type="text" id="_promoCode_edit" name="_promoCode_edit" class="form-control" readonly placeholder="Code" value="<%=promoObj.getCode()%>">
                                </div>
                                <div id="checkAvailability-result">
                                </div>
                            </div>
                            <label class="control-label col-lg-2">Expiry</label>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon "><i class="fa fa-calendar"></i></span>
                                    <input type="text" id="_promoExpiry_edit" name="_promoExpiry_edit" class="form-control" placeholder="Expire after" value="<%=promoObj.getExpiryInNoOfDays()%>" onkeypress="return isNumericKey(event)">
                                </div>
                            </div>                            
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2">Usage Count</label>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                                    <input type="text" id="_usageCount_edit" name="_usageCount_edit" class="form-control" placeholder="Usage Count" value="<%=promoObj.getUsageCount()%>" onkeypress="return isNumericKey(event)">                                    
                                </div>
                            </div>
                            <label class="control-label col-lg-2">Developer Usage Count</label>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-list-ol"></i></span>
                                    <input type="text" id="_partnerUsageCount_edit" name="_partnerUsageCount_edit" class="form-control" placeholder="Usage Count" value="<%=promoObj.getPartnerUsageCount()%>" onkeypress="return isNumericKey(event)">                                    
                                </div>
                            </div>
                        </div>                       
                        <div class="form-group">
                            <label class="control-label col-lg-2">Discount</label>
                            <div class="col-lg-3">
                                <select id="_promoCodeDiscountType_edit" name="_promoCodeDiscountType_edit" class="form-control" onchange="discountChangeType(this.value)" onkeypress="return isNumericKey(event)">
                                    <%if (discountType.equalsIgnoreCase("flat")) {%>
                                    <option selected value="Flat">Flat</option>
                                    <option value="Custom">Custom</option>
                                    <%} else {%>
                                    <option  value="Flat">Flat</option>
                                    <option selected value="Custom">Custom</option>
                                    <%}%>
                                </select>
                            </div>
                            <%if (discountType.equalsIgnoreCase("flat")) {%>        
                            <label class="control-label col-lg-2" id="discountTypeDetail">In Price</label>
                            <%} else {%>
                            <label class="control-label col-lg-2" id="discountTypeDetail">In Percentage</label>
                            <%}%>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                    <input type="text" id="_promoDiscount_edit" name="_promoDiscount_edit" class="form-control" placeholder="Discount" value="<%=promoObj.getDiscount()%>" onkeypress="return isNumberKey(event)">                                    
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-2">For Developer</label>
                            <div class="col-lg-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-users"></i></span>
                                    <select id ="visibleTo_edit" name="visibleTo_edit" multiple class="span6" style="width: 100%" onchange="updatePartnerList(this.value)">
                                        <%=options%>
                                    </select>                                  
                                </div>
                            </div>
                        </div>                        
                        <a  class="btn btn-success btn-xs" onclick="editPromoCode('<%=promoId%>')" style="margin-left: 17%"><i class="fa fa-plus-circle"></i> Edit Promo code</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<%}%>
<script>
    $(document).ready(function () {
        $("#visibleTo").select2();
    });

    $(document).ready(function () {
        $("#visibleTo_edit").select2();
    });
    
    $(function () {
        $('#_promoCode').keyup(function () {
            this.value = this.value.toUpperCase();
        });
    });
    
    $(function () {
        $('#_promoCode_edit').keyup(function () {
            this.value = this.value.toUpperCase();
        });
    });

    PromoDiscount("Flat");

    function discountChangeType(discountType) {
        if (discountType === 'Flat') {
            document.getElementById("discountTypeDetail").innerHTML = "In Price";
        } else if (discountType === 'Custom') {
            document.getElementById("discountTypeDetail").innerHTML = "In percentage";
        }

    }

    
    
</script>
<!--<script src="js/jquery-ui-1.10.4.min.js" type="text/javascript"></script>-->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->
<script src="select2/select2.js" type="text/javascript"></script>
</body>
</html>           
<%@include file="footer.jsp" %>