/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.virtualNumber;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.SgNumbers;
import com.mollatech.serviceguard.nucleus.db.connector.management.NumbersManagement;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONObject;

/**
 *
 * @author Manoj Sherkhane
 */
@WebServlet(name = "RegisterNumber", urlPatterns = {"/RegisterNumber"})
public class RegisterNumber extends HttpServlet {

    static final Logger logger = Logger.getLogger(RegisterNumber.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        logger.info("Request servlet is #registerPartner from #PPortal at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String result = "success";
        String message = "Record inserted successfully";
        String partchannelID = (String) request.getSession().getAttribute("_channelId");
        JSONObject json = new JSONObject();
        String zippath = (String) request.getSession().getAttribute("_ZipPath");
        logger.debug("value of zippath : " + zippath);
        Path path = Paths.get(zippath);
//        BufferedReader br = null;
        List<String> list = new ArrayList<String>();
        SgNumbers numbers = null;
        int counter = 0;
        int duplicateNo = 0;
        int notNumber = 0;
        String rex = "^\\+?\\d+$";
        try {
            FileInputStream excelFile = new FileInputStream(new File(zippath));
            Workbook workbook = null;
            if (zippath.endsWith("xlsx")) {
                workbook = new XSSFWorkbook(excelFile);
            } else if (zippath.endsWith("xls")) {
                workbook = new HSSFWorkbook(excelFile);
            }
            Sheet datatypeSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = datatypeSheet.iterator();
            Map NumbersMap = new LinkedHashMap();
            String numberS = "";
            while (iterator.hasNext()) {

                Row currentRow = iterator.next();
                Iterator<Cell> cellIterator = currentRow.iterator();

                while (cellIterator.hasNext()) {

                    Cell currentCell = cellIterator.next();
                    if (currentCell.getCellTypeEnum() == CellType.STRING) {
//                    System.out.print(currentCell.getStringCellValue() + "--");
                        if (currentCell.getStringCellValue().equalsIgnoreCase("Number") || currentCell.getStringCellValue().equalsIgnoreCase("Type")) {

                        } else if (currentCell.getStringCellValue().equalsIgnoreCase("LIVE") || currentCell.getStringCellValue().equalsIgnoreCase("TEST")) {
                            NumbersMap.put(numberS, currentCell.getStringCellValue());
                            numberS = "";
                        } else {
                            numberS = currentCell.getStringCellValue();
                        }
                    }

                }
            }
            for (Object no : NumbersMap.keySet()) {
                if (!(no.toString().replace("-", "").matches(rex))) {
                    notNumber++;
                }
            }
            if (notNumber != 0) {
                result = "error";
                message = " File have invalid numbers !";
            } else {
                SgNumbers[] check = new NumbersManagement().getAllNumbers();
                String oldNumbers = ",";
                if (check != null) {
                    for (int k = 0; k < check.length; k++) {
                        oldNumbers += check[k].getMobileNo() + ",";
                    }
                }
                for (Object no : NumbersMap.keySet()) {
                    if (!oldNumbers.contains(no.toString())) {
                        numbers = new SgNumbers();
                        oldNumbers += (no.toString()) + ",";
                        numbers.setMobileNo(no.toString());
                        numbers.setType((String) NumbersMap.get(no));
                        numbers.setCreatedOn(new Date());
                        numbers.setUpdatedOn(new Date());
                        numbers.setChannelId((String) request.getSession().getAttribute("_channelId"));
                        numbers.setStatus(GlobalStatus.ACTIVE);
                        int resCount = new NumbersManagement().addNumber(numbers, sessionId);
                        if (resCount == 0) {
                            counter++;
                        }
                    } else {
                        duplicateNo++;
                    }

                }

                if (counter != 0 && duplicateNo != 0) {
                    result = "success";
                    message = +counter + " Record added successfully and " + duplicateNo + " ignored";
                } else if (counter != 0) {
                    result = "success";
                    message = +counter + " Record added successfully !";
                } else if (counter == 0 && duplicateNo == 0) {
                    result = "success";
                    message = "Please Upload Proper File.";
                } else {
                    result = "error";
                    message = " Record already exist !";
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private boolean onlyContainsNumbers(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
