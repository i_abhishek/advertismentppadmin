<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.OperatorsManagement"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Properties"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileInputStream"%>
<%@include file="header.jsp" %>
<%
    Operators operators = (Operators) request.getSession().getAttribute("_apOprDetail");
    String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
    String channelID = (String) request.getSession().getAttribute("_channelId");
                        
    PartnerDetails [] partenrdetails = new PartnerManagement().getAllPartnerDetails(sessionId, channelID);
%>
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header text-uppercase">Compose Email</h3>
            </div>
            <!-- /.col-lg-10 -->
        </div>
        <div class="row">
            <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47;  <i class="fa fa-table"></i><b> Compose</b>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal " method="POST" id="ticketCompose">
                            <div class="row">
                                <font style=" font-weight:">
                                <label for="curl" class="control-label col-lg-2">To <span class="text-danger">*</span></label>
                                </font>
                                <div class="col-lg-4">
                                    <select class="form-control" id="to" name="to">
                                    <%
                                        if(partenrdetails != null){
                                        for(int i = 0; i < partenrdetails.length; i++){
                                            if(partenrdetails[i].getStatus() != GlobalStatus.SUSPEND){
                                    %>
                                    <option value="<%=partenrdetails[i].getPartnerId()%>"><%=partenrdetails[i].getPartnerEmailid()%></option>
                                    <%}
                                        }
                                            }else{%>
                                    <option value="">No Developer found</option>
                                    <%}%>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <font style=" font-weight:">
                                <label for="curl" class="control-label col-lg-2">Subject <span class="text-danger">*</span></label>
                                </font>
                                <%
                                        Properties emailSubjectProperties = null;
                                        emailSubjectProperties=LoadSettings.g_emailSubjectSettings;

                                        String emailSubjectOptions = "<option value='Select' selected>Select subject</option>\n";

                                        if(emailSubjectProperties!=null){
                                        Enumeration enuKeys = emailSubjectProperties.keys();
                                            while (enuKeys.hasMoreElements()) {
                                                String key = (String) enuKeys.nextElement();
                                                String value = emailSubjectProperties.getProperty(key);
                                                emailSubjectOptions += "<option value='" + value + "'>" + value + "</option>\n";
                                            }
                                        }    
                                %>  
                                <div class="col-lg-4">
                                    <select class="form-control " id="subject" name="subject"/>
                                        <%=emailSubjectOptions%>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <font style=" font-weight: bolder;">
                                <label for="curl" class="control-label col-lg-2"> Category <span class="text-danger">*</span></label>
                                </font>
                                <%
                                        Properties emailCategoryProperties = null;
                                        emailCategoryProperties=LoadSettings.g_emailCategorySettings;

                                        String emailCategoryOptions = "<option value='Select' selected>Select category</option>\n";

                                        if(emailCategoryProperties!=null){
                                        Enumeration enuKeys = emailCategoryProperties.keys();
                                            while (enuKeys.hasMoreElements()) {
                                                String key = (String) enuKeys.nextElement();
                                                String value = emailSubjectProperties.getProperty(key);
                                                emailCategoryOptions += "<option value='" + value + "'>" + value + "</option>\n";
                                            }
                                        }    
                                %> 
                                <div class="col-lg-4">
                                    <select class="form-control" id="_setCategory" name="_setCategory">
                                        <%=emailCategoryOptions%>
                                    </select></div></div>
                            <br>
                            <div class="row">
                                <font style=" font-weight: bolder;">
                                <label for="curl" class="control-label col-lg-2"> Message <span class="text-danger">*</span></label>
                                </font>
                                <div class="col-lg-4">
                                    <textarea class="form-control" cols="50" id="msgBody" name="msgBody"></textarea>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <font style=" font-weight: bolder;">
                                <label for="curl" class="control-label col-lg-2"> From <span class="required"></span></label>
                                </font>
                                <div class="col-lg-4">
                                    <input class="form-control" id="from"  name="from"  type="text" readonly/>
                                </div></div>
                            <br>
                            <div class="row">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <a class="btn btn-success btn-xs" tabindex="1" id="submitMail" name="submitMail" onclick="emailSend()"><i class="fa fa-check-circle"></i> Send </a>     
                                    <a class="btn btn-danger btn-xs" tabindex="2" href="./ticketComposeMail.jsp"><i class="fa fa-close"></i> Clear</a>
                                </div>
                            </div>
                            
                        </form></div></div></div></div></div></div>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="./js/ticketmanagement.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>

<script src="js/bootbox.min.js" type="text/javascript"></script>
<script>
    document.getElementById("from").value = '<%=operators.getName()%>';
</script>
</body>
</html>
<%@include file="footer.jsp" %>