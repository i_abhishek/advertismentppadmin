<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.UtilityFunctions"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.OperatorsManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Operators"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgEmailticket"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.EmailManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.GroupDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.GroupManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgUsers"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement"%>
<%@page import="javax.xml.datatype.XMLGregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@include file="header.jsp" %>
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header text-uppercase">Inbox</h3>
            </div>
            <!-- /.col-lg-10 -->
        </div>
        <div class="row">
        <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-home"></i><a href="home.jsp"> Home </a>&#47;  <i class="fa fa-table"></i><b> Inbox</b>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal " method="get">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th style="text-align: center">From</th>
                                        <th style="text-align: center">Subject</th>
                                        <th style="text-align: center">Date & Time</th>
                                        <th style="text-align: center">Action</th>
                                          </tr>
                                </thead>
                                <tbody>
                                    <%
                                    Operators operators = (Operators) request.getSession().getAttribute("_apOprDetail");
                                    String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
                                    String channelID = (String) request.getSession().getAttribute("_channelId");
                                    String emailTo = operators.getOperatorid();
                                    SgEmailticket [] ticketDetails = new EmailManagement().getAllEmailDetailsByReceivedId(sessionId, channelID, emailTo);
                                    String emailNo ; boolean allDeletedFlag = true;                            
                                    if(ticketDetails != null){
                                        for(int i = 0; i < ticketDetails.length; i++){
                                       int partnerId  = Integer.parseInt(ticketDetails[i].getEmailFrom());
                                       PartnerDetails partDetail = new PartnerManagement().getPartnerDetails(partnerId);
                                        emailNo = ticketDetails[i].getId();
                                        String createdOn = new UtilityFunctions().getTMReqDate(ticketDetails[i].getCreatedon());
                                        if(ticketDetails[i].getDeletedByOperator() != GlobalStatus.DELETED){
                                        if(ticketDetails[i].getReadStatusByOperator() == GlobalStatus.SUCCESS){
                                            allDeletedFlag = false;
                                        %>
                            <tr style="font-weight:bold">
                                <td style="text-align: center; cursor: pointer" onclick="window.location = 'ticketViewEmail.jsp?_emailNo=<%=emailNo%>&emailFrom=<%=partDetail.getPartnerEmailid()%>';"><%=partDetail.getPartnerEmailid()%> <span class="badge bg-important">new</span></td>
                                <td style="text-align: center; cursor: pointer" onclick="window.location = 'ticketViewEmail.jsp?_emailNo=<%=emailNo%>&emailFrom=<%=partDetail.getPartnerEmailid()%>';"><%=ticketDetails[i].getSubject()%></td>
                                <td style="text-align: center; cursor: pointer" onclick="window.location = 'ticketViewEmail.jsp?_emailNo=<%=emailNo%>&emailFrom=<%=partDetail.getPartnerEmailid()%>';"><%=createdOn%></td> 
                                <td style="text-align: center; cursor: pointer">  
<!--                                    <font style="font-size: 11px"><a class="btn btn-info btn-xs" onclick="viewDateails('<%=emailNo%>',' <%=sessionId%>','<%=partDetail.getPartnerEmailid()%>')"><i class="fa fa-eye"></i> view</a> </font>-->
                                    <!--<font style="font-size: 11px"><a class="btn btn-info btn-xs" href="ticketViewEmail.jsp?_emailNo=<%=emailNo%>&emailFrom=<%=partDetail.getPartnerEmailid()%>"><i class="fa fa-eye"></i> view</a> </font>-->
                                    <font style="text-align: center;font-size: 11px"><a class="btn btn-danger btn-xs"  onclick="deleteEmailById('<%=emailNo%>')"><i class="fa fa-dropbox"></i> Delete</a> </font>
                                </td>
                            </tr>
                                    <%}else{
                                            allDeletedFlag = false;
                                    %>
                                     <tr>
                                <td style="text-align: center; cursor: pointer" onclick="window.location = 'ticketViewEmail.jsp?_emailNo=<%=emailNo%>&emailFrom=<%=partDetail.getPartnerEmailid()%>';"><%=partDetail.getPartnerEmailid()%></td>
                                <td style="text-align: center; cursor: pointer" onclick="window.location = 'ticketViewEmail.jsp?_emailNo=<%=emailNo%>&emailFrom=<%=partDetail.getPartnerEmailid()%>';"><%=ticketDetails[i].getSubject()%></td>
                                <td style="text-align: center; cursor: pointer" onclick="window.location = 'ticketViewEmail.jsp?_emailNo=<%=emailNo%>&emailFrom=<%=partDetail.getPartnerEmailid()%>';"><%=createdOn%></td> 
                                <td style="text-align: center; cursor: pointer">  
                                    <!--<font style="font-size: 11px"><a class="btn btn-info btn-xs" href="ticketViewEmail.jsp?_emailNo=<%=emailNo%>&emailFrom=<%=partDetail.getPartnerEmailid()%>"><i class="fa fa-eye"></i> view</a> </font>-->
                                    <font style="font-size: 11px;text-align: center"><a class="btn btn-danger btn-xs" onclick="deleteEmailById('<%=emailNo%>')"><i class="fa fa-dropbox"></i> Delete</a> </font>
                                </td>
                            </tr><%}

                             }}}else{%>
                            <tr> <td style="text-align: center">No record found</td>
                                <td style="text-align: center">No record found</td>
                                <td style="text-align: center">No record found</td>
                                <td style="text-align: center">No record found</td></tr>
                            <%}%>
                                </tbody>
                            </table></div></form></div></div></div></div></div></div>
                      
<script src="bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript 
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.min.js"></script>
<script src="js/morris-data.js"></script>-->

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<script src="./js/ticketmanagement.js"></script>
<script src="js/bootbox.min(1).js" type="text/javascript"></script>
<script src="js/bootbox.min.js" type="text/javascript"></script>

</body>
</html>
<%@include file="footer.jsp" %>