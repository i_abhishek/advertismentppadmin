/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.details;

import com.mollatech.serviceguard.nucleus.commons.Serializer;
import com.mollatech.serviceguard.nucleus.db.Accesspoint;
import com.mollatech.serviceguard.nucleus.db.Warfiles;
import com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.WarFileManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Ashu
 */
@WebServlet(name = "GetVersion", urlPatterns = {"/GetVersion"})
public class GetVersion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        try {
            AccessPointManagement ppw = new AccessPointManagement();
            String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
            String channelId = (String) request.getSession().getAttribute("_channelId");
            String apId = request.getParameter("_resourceId");
            String appId = request.getParameter("_apId");
            if (!apId.equals("-1") && !appId.equals("-1")) {
                Accesspoint accesspoint = ppw.getAccessPointByName(SessionId, channelId, appId);
                Warfiles warFiles = new WarFileManagement().getWarFile(SessionId, channelId, accesspoint.getApId());
                int version = 1;
                if (warFiles != null) {
                    Map warMap = (Map) (Serializer.deserialize(warFiles.getWfile()));
                    version = warMap.size() / 2;
                    for (int i = 1; i <= version; i++) {
                        json.put("" + i, "" + i);
                    }
                }
            } else {
                json.put("-1", "Select Version");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        out.print(json);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
