<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Properties"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.UtilityFunctions"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPromocode"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PromocodeManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgBucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement"%>
<%@include file="header.jsp" %>
<script src="js/promocode.js" type="text/javascript"></script>
<%
    
    String options = "<option value='Select' selected>Select Reason for Rejection </option>\n";

    Enumeration enuKeys = LoadSettings.g_promocodeRejectionSettings.keys();
    while (enuKeys.hasMoreElements()) {
        String key = (String) enuKeys.nextElement();
        String value = LoadSettings.g_promocodeRejectionSettings.getProperty(key);
        // for (int i = 0; i < key.length(); i++) {
        options += "<option value='" + value + "'>" + value + "</option>\n";

    }

    if (makerChakerObj != null) {
        if (!makerChakerObj.chaker.contains(opObj.getEmailid()) && makerChakerObj.status == GlobalStatus.ACTIVE) {
            response.sendRedirect("oplogout.jsp");
            return;
        }
    }
%>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Promocode Request</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>    
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-home"></i><a href="home.jsp"> Home</a> &#47;
                    <i class="fa fa-gears"></i> Promocode Request Details &#47;                                                          
                </div>
                <div class="panel-body">
                    <table class="table table-striped table-bordered table-hover" id="table_main_pagination">
                        <thead>
                            <tr>
                                <th valign="top">Sr.No.</th>
                                <th valign="top">Status</th>
                                <th valign="top">Promocode</th>
                                <th valign="top">Usage</th>
                                <th valign="top">Developer usage</th>
                                <th valign="top">Discount</th>
                                <th valign="top">Discount Type</th>
                                <th valign="top">Allow Developer</th>
                                <th valign="top">Manage</th>
                                <th valign="top">Created On</th>
                                <th valign="top">Last updated On</th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                int count = 0;
                                String resultInfo = "No Record Found";

                                PromocodeManagement um = new PromocodeManagement();
                                SgPromocode[] rDetails = null;
                                int pId = -1;
                                rDetails = um.listPromocodeByReqFlag(SessionId, channelId, GlobalStatus.SUSPEND, GlobalStatus.SENDTO_CHECKER);
                                if (rDetails != null) {
                                    for (int i = 0; i < rDetails.length; i++) {
                                        count++;
                                        Date createdDate = rDetails[i].getCreatedon();
                                        Date expiryDate = rDetails[i].getExpireOn();
                                        Date lastDate = rDetails[i].getLastUpdatedOn();
                                        String crtDate = "NA";
                                        String expDate = "NA";
                                        String lastupdated = "NA";
                                        if (createdDate != null) {
                                            crtDate = UtilityFunctions.getTMReqDate(createdDate);
                                        }
                                        if (expDate != null) {
                                            expDate = UtilityFunctions.getTMReqDate(expiryDate);
                                        }
                                        if (lastDate != null) {
                                            lastupdated = UtilityFunctions.getTMReqDate(lastDate);
                                        }
                                        String partnerEmail = "";
                                        String allowPartner = rDetails[i].getPartnerId();
                                        if (allowPartner != null && !allowPartner.equals("") && allowPartner.contains("all")) {
                                            partnerEmail = "all";
                                        } else if (allowPartner != null && !allowPartner.equals("")) {
                                            String[] parDet = allowPartner.split(",");
                                            for (int j = 0; j < parDet.length; j++) {
                                                int parId = Integer.parseInt(parDet[j]);
                                                PartnerDetails parObj = new PartnerManagement().getPartnerDetails(parId);
                                                if (parObj != null) {
                                                    partnerEmail += parObj.getPartnerEmailid() + "\n";
                                                }
                                            }
                                        } else {
                                            partnerEmail = "NA";
                                        }
                            %>
                            <tr style="text-align: center">
                                <td><%=count%></td>
                                <%if (rDetails[i].getStatus() == GlobalStatus.SUSPEND) {%>
                                <td>
                                    <a href="#" class="btn btn-warning btn-xs btn-circle" data-toggle="tooltip" data-placement="right" title="Request pending for approval"  ><i class="fa fa-thumbs-o-down"></i></a>
                                </td>
                                <% } else if (rDetails[i].getStatus() == PackageManagement.REJECTED_STATUS) { %>
                                <td>
                                    <a href="#" class="btn btn-danger btn-xs btn-circle" data-toggle="tooltip" data-placement="right" title="Request rejected"><i class="fa fa-thumbs-o-down"></i></a>
                                </td>
                                <%} else {%>
                                <td>
                                    <a href="#" class="btn btn-success btn-xs btn-circle" data-toggle="tooltip" data-placement="right" title="Request approved"><i class="fa fa-thumbs-o-up"></i></a>
                                </td>
                                <%}%>

                                <td><%=rDetails[i].getCode()%></td>                                
                                <td><%=rDetails[i].getUsageCount()%></td>
                                <td><%=rDetails[i].getPartnerUsageCount()%></td>
                                <td><%=rDetails[i].getDiscount()%></td>
                                <td><%=rDetails[i].getDiscountType()%></td>
                                <td> 
                                    <button type="button" class="btn btn-primary btn-xs" data-container="body" data-toggle="popover" data-placement="right" data-content="<%=partnerEmail%>">
                                        Allowed Developer(s)
                                    </button>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a class="btn btn-success btn-xs dropdown-toggle" data-toggle="dropdown" href="" ><%if (rDetails[i].getRequestFlag() == GlobalStatus.SENDTO_CHECKER || rDetails[i].getStatus() == GlobalStatus.SUSPEND) {%>New Request<%} else if (rDetails[i].getStatus() == GlobalStatus.ACTIVE) {%> Approved<%}%> <span class="caret"><font style="font-size: 12px"></font></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="verifyPromoCode.jsp?promocode=<%=rDetails[i].getPromoCodeId()%>">Verify</a></li>
                                            <li><a class="divider"></a></li>
                                            <li><a href="#" onclick="approvePromoRequest('<%=rDetails[i].getPromoCodeId()%>', '<%=GlobalStatus.ACTIVE%>', '<%=GlobalStatus.SUSPEND%>')" >Approve ?</a></li>
                                            <li><a href="#" onclick="rejectPromoCodeRequest('<%=rDetails[i].getPromoCodeId()%>', '<%=GlobalStatus.CANCLE_BYCHECKER%>')" >Reject ?</a></li>                                                               
                                        </ul>
                                    </div>
                                </td>
                                <td><%=crtDate%></td>                           
                                <td><%=lastupdated%></td>                            
                            </tr>
                            <%}
                            } else {%>
                            <tr style="text-align: center">
                                <td><%=resultInfo%></td><td><%=resultInfo%></td><td><%=resultInfo%></td><td><%=resultInfo%></td> <td><%=resultInfo%></td>
                                <td><%=resultInfo%></td><td><%=resultInfo%><td><%=resultInfo%></td><td><%=resultInfo%></td> <td><%=resultInfo%></td> <td><%=resultInfo%></td> 
                            </tr>
                            <%}%>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>

<div id="rejectPromocode" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="rejectPackageModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <b id="rejectPackageModal">Reject Promo Code</b>
            </div>          
            <div class="modal-body">
                <div class="row-fluid">
                    <form class="form-horizontal" id="rejectPromoForm">
                        <fieldset>
                            <div class="control-group">
                                <label class="control-label col-lg-2"  for="partnername">Reason</label>
                                <div class="controls col-lg-10">
                                    <input type="hidden"  id="_promoId" name="_promoId">
                                    <input type="hidden"  id="_promostatus" name="_promostatus" >
                                    <select id="reason" name="reason" class="form-control" >
                                        <%=options%>        
                                    </select>
                                </div>
                            </div>                            
                        </fieldset>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div id="edit-partner-result"></div>
                <button class="btn btn-info btn-xs" data-dismiss="modal">Close</button>
                <button class="btn btn-success btn-xs" onclick="rejectPromoRequest()" id="addPartnerButtonE">Reject request</button>
            </div>
        </div>
    </div>
</div>



<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>
<script src="js/bootbox.min(1).js" type="text/javascript"></script>
<script src="js/bootbox.min.js" type="text/javascript"></script>
<script>

                    $("[data-toggle=popover]")
                            .popover();
</script>   
</body>
</html>
<%@include file="footer.jsp" %> 