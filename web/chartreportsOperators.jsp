<script src="./js/jquery-1.8.3.min.js"></script>
<script src="./js/raphael-min.js"></script>
<script src="./js/morris.js"></script>
<script src="./js/morris.min.js"></script>
<link rel="stylesheet" href="./css/morris.css">
<%
    String _accesspoint = request.getParameter("_accesspoint");
    String type = request.getParameter("_type");
    int types = -1;
    if (type != null) {
        types = Integer.parseInt(type);
    }
%>
<style type="text/css">
    .vericaltext{
        width:1px;
        word-wrap: break-word;
        font-family: monospace; /* this is just for good looks */
    }
    /*    .vertical-text {
            -ms-transform: rotate(270deg);
            -moz-transform: rotate(270deg);
            -webkit-transform: rotate(270deg);
            transform: rotate(270deg);    
            -ms-transform-origin: left top 0;
            -moz-transform-origin: left top 0;
            -webkit-transform-origin: left top 0;
            transform-origin: left top 0;   
            color: #black;
            margin-left: 40px;
            padding: 10px;
            border: 1px solid #ccc;
            text-transform: uppercase;
            border: 1px solid #B52C2C;
            text-transform: 1px 1px 0px rgba(0, 0, 0, 0.5);
            box-shadow: 2px -2px 0px rgba(0, 0, 0, 0.1);
    
            float: left;
        }*/
</style>
<!--<h3>Searched Results for All</h3>      -->
<div class="tab-pane active" id="usercharts">
    <div class="row-fluid">
        <div class="span12">
            <%
                if (types == 1) {%>
            <div class="span12" style="alignment-adjust: central">    
                <h5 align="center" class="text-primary" style="font-weight: bold">TRANSACTION STATUS REPORT</h5>
                <font style="font-family: monospace">
                Transactions count
                </font>
                <div id="bargraph"></div>    
                <div  align="center" style="font-family: monospace" >
                    Transactions Status
                </div>
            </div>
            <br>
            <div  class="span12" style="alignment-adjust: central">
                <h5 align="center" class="text-primary" style="font-weight: bold">TRANSACTION PER DAY REPORT</h5>
                <font style="font-family: monospace">
                Transactions count
                </font>
                <div id="linegraph"></div>         
                <div  align="center" style="font-family: monospace" >
                    Time
                </div>
            </div> 
            <%} else if (types == 2) {%>
            <h5 align="center" class="text-primary" style="font-weight: bold">RESPONSE REPORT</h5>
            <font style="font-family: monospace">
            Response time in Milliseconds
            </font>
            <div  class="span12" style="alignment-adjust: central">
                <div id="linertgraph"></div>           
                <div  align="center" style="font-family: monospace" >
                    Time
                </div>
            </div>  
            <%}%>
        </div>
    </div>
</div>
<div class="tab-pane" id="userreport">            
</div>    