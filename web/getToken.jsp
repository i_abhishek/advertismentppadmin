<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgBucketdetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@include file="header.jsp" %>
<div id="page-wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header text-uppercase">Token Management</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <i class="fa fa-home"></i><a href="home.jsp"> Home</a> &#47;
                <i class="fa fa-shopping-cart"></i><a href="package.jsp"> Package Details</a> &#47;
                <i class="fa fa-edit"></i> Package Details
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-lg-2 control-label" style="">Access Point</label>                                  
                    <div class="col-lg-2">
                        <select id="_Accesspoint2" name="_Accesspoint2"  class="form-control span2" onchange="acesspointChangeResource(this.value)" style="width: 100%">
                            <option value="-1" selected>Select AP</option>
                            <%
                                Accesspoint[] accesspoints2 = null;
                                accesspoints2 = new AccessPointManagement().getAllAccessPoint(SessionId, channelId);
                                if (accesspoints2 != null) {
                                    for (int i = 0; i < accesspoints2.length; i++) {
                            %>
                            <option value="<%=accesspoints2[i].getName()%>"><%=accesspoints2[i].getName()%></option>
                            <%}
                                    }%>
                        </select>
                    </div>
                    <div class="col-lg-2">
                        <select id="_ResourceForSlabPricing2" name="_ResourceForSlabPricing2"  class="form-control span2" onchange="showSlabVersion(this.value)" style="width: 100%">
                            <option value="-1" selected>Select Resource</option>                                                                                  
                        </select>
                    </div>
                    <div class="col-lg-2">
                        <select id="_VersionForSlabPricing2" name="_VersionForSlabPricing2"  class="form-control span2" onchange="showSlabAPI(this.value)" style="width: 100%">
                            <option value="-1" selected>Select Version</option>                                                                                  
                        </select>
                    </div>
                    <div class="col-lg-2">
                        <select id="_APIForSlabPricing2" name="_APIForSlabPricing2"  class="form-control span2" onchange="showSlabPricing(this.value)" style="width: 100%">
                            <option value="-1" selected>Select API</option>                                                                                  
                        </select>
                    </div>                                        
                </div>
                <div id="_slabPriceWindow">                                            
                </div>                                       
            </div>
        </div>
    </div>
</div>

<script>
    function acesspointChangeResource(value) {
        var s = './slabResource.jsp?_apName=' + value;
        $.ajax({
            type: 'GET',
            url: s,
            success: function(data) {
                $('#_ResourceForSlabPricing2').html(data);
                   var res = document.getElementById('_ResourceForSlabPricing2').value;
                   //alert(res+"res");
                   var ap = document.getElementById('_Accesspoint2').value;
                   //alert(ap+"ap");
                   showSlabVersion(res,ap);
                }
            });
        }
    
    function showSlabVersion(value,ap){
            var s = './slabVersion.jsp?_resourceId=' + value+'&_apId=' +ap;
            $.ajax({
            type: 'GET',
            url: s,
            success: function(data) {
                $('#_VersionForSlabPricing2').html(data);
                    var versionData = document.getElementById('_VersionForSlabPricing2').value;
                    showSlabAPI(versionData,ap,value);
                }
            });
        }
        
    function showSlabAPI(versionData,ap,_resourceId){
            var s='./slabAPI.jsp?versionData=' + versionData + '&_apId=' +ap + '&_resourceId='+_resourceId;
            $.ajax({
            type: 'GET',
            url: s,
            success: function(data) {
                $('#_APIForSlabPricing2').html(data);
                    var apiName = document.getElementById('_APIForSlabPricing2').value;
                   // showSlabAPI(versionData,ap);
                   showSlabPricing(apiName);
                }
            });
        }
    
    function showSlabPricing(value){
            var accesspointName = document.getElementById("_Accesspoint2").value;
            var resourceName = document.getElementById("_ResourceForSlabPricing2").value;
            var version = document.getElementById("_VersionForSlabPricing2").value;
            var packageID = document.getElementById("packageID").value;
            var s ='./showSlabPriceWindow.jsp?_apId=' + accesspointName +'&_resourceId='+resourceName +'&version='+version+'&apiName='+value+'&packageId='+packageID;
            $.ajax({
                type: 'GET',
                url: s,
                success: function(data) {
                    $('#_slabPriceWindow').html(data);
                        // acesspointChangeAPIPrice(value);
                    }
                });
        }
</script>                        
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>
<script src="js/bootbox.min.js" type="text/javascript"></script>  
</body>
</html>    
<%@include file="footer.jsp" %>