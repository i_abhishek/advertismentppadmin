<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.GroupManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.GroupDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@include file="header.jsp"%>
<link href="./css/select2.css" rel="stylesheet"/>
<script src="./js/select2.js"></script>
<script src="./js/json3.min.js"></script>
<link href="./css/bootstrap-timepicker.css" rel="stylesheet"/>
<link rel="stylesheet" href="./css/datepicker.css">
<script src="./js/operatorsReports.js"></script>
<%
    int opType = -1;
    String _opType = request.getParameter("_opType");
    if (_opType != null) {
        opType = Integer.parseInt(_opType);
    }
    String type = request.getParameter("_type");
    int Type = -1;
    if (type != null) {
        Type = Integer.parseInt(type);
    }
%>
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12"> 
                <% if (Type == 1) {%>
                <h3 class="page-header text-uppercase">Transaction Report</h3>
                <%} else if (Type == 2) {%>
                <h3 class="page-header text-uppercase">Performance Report</h3>
                <%}%>  
            </div>
            <!-- /.col-lg-10 -->
        </div>
        <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>    
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47;  <i class="fa fa-bar-chart"></i> Graphical Reports &#47; <% if (Type == 1) {%>
                        <i class="fa fa-line-chart"></i> Transaction Reports
                        <%} else if (Type == 2) {%>
                        <i class="fa fa-pie-chart"></i> Performance Reports
                        <%}%>                
                    </div>
                    <div class="panel-body">
                        <table border="0" >
                            <tr>
                                <td>
                                    Access Point&nbsp;
                                </td>
                                <td> 
                                    <select id="_Accesspoint" name="_Accesspoint"  class="form-control span2" onchange="acesspointChange(this.value)" style="width: 90%">
                                        <option value="-1" selected>Select All</option>
                                        <%
                                            Accesspoint[] accesspoints = null;
                                            accesspoints = new AccessPointManagement().getAllAccessPoint(SessionId, channelId);
                                            if (accesspoints != null) {
                                                for (int i = 0; i < accesspoints.length; i++) {
                                        %>
                                        <option value="<%=accesspoints[i].getApId()%>"><%=accesspoints[i].getName()%></option>
                                        <%}
                                    }%>
                                    </select>
                                </td>
                                <td>
                                    Group&nbsp;
                                </td>
                                <td>
                                    <select id="_Group" name="_Group" class="form-control span2" onchange="acesspointChangePartner(-1, this.value)" style="width: 90%">
                                        <option value="-1"  selected>Select All</option>            
                                        <%
                                            GroupDetails[] gd = null;
                                            gd = new GroupManagement().getAllGroupDetails(SessionId, channelId);
                                            if (gd != null) {
                                                for (int i = 0; i < gd.length; i++) {
                                        %>
                                        <option value="<%=gd[i].getGroupId()%>"><%=gd[i].getGroupName()%></option>
                                        <%}
                                    }%>
                                    </select>
                                </td>
                                <td>                                         
                                    Resource&nbsp;
                                </td>
                                <td>
                                    <select id="_Resources" name="_Resources"  class="form-control span2" style="width: 90%">
                                        <option value="-1" selected>Select All</option>
                                        <%
                                            ResourceDetails[] rd = null;
                                            //List<ResourceDetails> rlist = null;
                                            rd = new ResourceManagement().getAllResources(SessionId, channelId);
        //                                    if (!rlist.isEmpty() || rlist != null) {
        //                                        rd = new ResourceDetails[rlist.size()];
        //                                        for (int i = 0; i < rlist.size(); i++) {
        //                                            rd[i] = rlist.get(i);
        //                                        }
        //                                    }
                                            if (rd != null) {
                                                for (int i = 0; i < rd.length; i++) {
                                        %>
                                        <option value="<%=rd[i].getResourceId()%>"><%=rd[i].getName()%></option>
                                        <%}
                                    }%>
                                    </select>
                                </td>
                                <td>
                                    Developer&nbsp;
                                </td>
                                <td>
                                    <select id="_Partner" name="_Partner" class="form-control span2" style="width: 90%">
                                        <option value="-1" selected>Select All</option>     
                                        <%
                                            PartnerDetails[] pd = new PartnerManagement().getAllPartnerDetails(SessionId, channelId);
                                            if (pd != null) {
                                                for (int i = 0; i < pd.length; i++) {
                                        %>
                                        <option value="<%=pd[i].getPartnerId()%>"><%=pd[i].getPartnerName()%></option>
                                        <%      }
                                            }
                                        %>
                                        <option value="-2">Others</option>    
                                    </select>
                                </td>   
                                <td>
                                    <button class="btn btn-success" id="Button" onclick="generateOperatorreports(<%=Type%>,<%=opType%>)"><i class="fa fa-bar-chart-o"></i> Generate Report</button>                        
                                </td>
                            </tr>                  
                            <tr>

                                <td>                            
                                    From&nbsp;
                                </td>
                                <td>
                                    <div id="datetimepicker1" class="date">
                                        <input id="_startdate" class="datepicker" name="_startdate" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth" style="width: 60%" />
                                        <span class="add-on" hidden>
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar" ></i>
                                        </span>
                                    </div>
                                </td>
                                <td>                            
                                    Time&nbsp;
                                </td>      
                                <td>
                                    <div class="time">
                                        <input id="_ApStartTime" name="_ApStartTime" type="text" class="time" style="width: 60%">
                                    </div>
                                </td>
                                <td>
                                    Till&nbsp;
                                </td>
                                <td>
                                    <div id="datetimepicker2" class="date">
                                        <input id="_enddate" class="datepicker " name="_enddate" type="text" data-format="yyyy-MM-dd" data-bind="value: vm.ActualDoorSizeDepth" style="width: 60%"/>
                                        <span class="add-on" hidden>
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar" ></i>
                                        </span>
                                    </div>  
                                </td>
                                <td>
                                    Time
                                </td>
                                <td>
                                    <div class="bootstrap-timepicker">
                                        <input id="_ApEndTime" name="_ApEndTime" type="text" class="time" style="width: 60%">
                                    </div>
                                </td>                        
                                <td>                            
                                    <button class="btn btn-info" id="Refresh" onclick="pageRefreshgraphTransaction(<%=Type%>)"><i class="fa fa-refresh"></i> Refresh</button>                        
                                </td>
                            </tr>                    
                        </table>        
                        <input  type=hidden id="_type" name="_Accesspoint"  value="0">
                        <br>
                        <div id="report_data"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="dist/js/moment.min.js" type="text/javascript"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript 
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.min.js"></script>
<script src="js/morris-data.js"></script>-->

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>
<script src="js/bootbox.min.js" type="text/javascript"></script>
<script src="./js/bootstrap-timepicker.js"></script>
<script src="./js/bootstrap-datepicker.js"></script>

<script>
                                        function acesspointChangePartner(value1, value2) {
                                            var s2 = './accessPointChangePartner.jsp?_apId=' + value1 + '&_grId=' + value2;
                                            $.ajax({
                                                type: 'GET',
                                                url: s2,
                                                success: function (data) {
                                                    $('#_Partner').html(data);
                                                }
                                            });
                                        }

                                        function acesspointChangeResource(value1) {
                                            var s1 = './accessPointChangeResources.jsp?_apId=' + value1;
                                            $.ajax({
                                                type: 'GET',
                                                url: s1,
                                                success: function (data) {
                                                    $('#_Resources').html(data);
                                                    acesspointChangePartner(value1, -1);
                                                }
                                            });
                                        }
                                        function acesspointChange(value) {
                                            var s = './accessPointChangeValue.jsp?_apId=' + value;
                                            $.ajax({
                                                type: 'GET',
                                                url: s,
                                                success: function (data) {
                                                    $('#_Group').html(data);
                                                    acesspointChangeResource(value);
                                                }
                                            });
                                        }
                                        $(function () {
                                            $('#datetimepicker1').datepicker({
                                                language: 'pt-BR'
                                            });
                                        });
                                        $(function () {
                                            $('#datetimepicker2').datepicker({
                                                language: 'pt-BR'
                                            });
                                        });
                                        $('#_ApStartTime').timepicker({
                                            minuteStep: 1,
                                            showInputs: false,
                                            disableFocus: true

                                        });
                                        $('#_ApEndTime').timepicker({
                                            minuteStep: 1,
                                            showInputs: false,
                                            disableFocus: true

                                        });
                                        $('#datetimepicker1').on('changeDate', function (ev) {
                                            $(this).datepicker('hide');
                                        });
                                        $('#datetimepicker2').on('changeDate', function (ev) {
                                            $(this).datepicker('hide');
                                        });
</script>
<%@include file="footer.jsp" %>
