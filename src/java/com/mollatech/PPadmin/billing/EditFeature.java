package com.mollatech.PPadmin.billing;

import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.commons.MakerChaker;
import com.mollatech.serviceguard.nucleus.db.SgBucketdetails;
import com.mollatech.serviceguard.nucleus.db.SgReqbucketdetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.PackageManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestPackageManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.json.simple.JSONArray;

/**
 *
 * @author mohanish
 */
//@WebServlet(name = "EditFeature", urlPatterns = { "/EditFeature" })
public class EditFeature extends HttpServlet {

    static final Logger logger = Logger.getLogger(EditFeature.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logger.info("Request servlet is #EditFeature from #PPAdmin at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        PrintWriter out = response.getWriter();
        String result = "success";
        String message = "Package created successfully.";
        
        String channelId = (String) request.getSession().getAttribute("_channelId");
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        MakerChaker makerChakerObj = (MakerChaker) request.getSession().getAttribute("makerChacker");
        String packageID = request.getParameter("packageId");
        logger.debug("Value of packageID  = " + packageID);
        int pId = 0;
        if (packageID != null) {
            pId = Integer.parseInt(packageID);
        }
        SgBucketdetails packObj = new PackageManagement().getPackageDetails(SessionId, channelId, pId);
        try {
            if (packObj == null) {
                result = "error";
                message = "Can't edit the package details.";
                json.put("result", result);
                json.put("message", message);
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String promoCodeFlag = request.getParameter("promoCodeFlag");
        String loanLimit = request.getParameter("loanLimit");
        String interestRate = request.getParameter("interestRate");
        String reedmePrice = request.getParameter("reedmePrice");
        String reedmePoint = request.getParameter("reedmePoint");
        
        JSONObject featureJson = new JSONObject();
        featureJson.put("loanLimit", 0);
        featureJson.put("interestRate", 0);
        featureJson.put("reedmePrice", 0);
        featureJson.put("reedmePoint", 0);
        featureJson.put("promoCodeFlag", "Disable");
        if(promoCodeFlag != null && promoCodeFlag.isEmpty()){
            featureJson.put("promoCodeFlag", promoCodeFlag);
        }
        if (loanLimit != null && !loanLimit.equals("")) {
            featureJson.put("loanLimit", loanLimit);
        }
        if (interestRate != null && !interestRate.equals("")) {
            featureJson.put("interestRate", interestRate);
        }
        if (reedmePrice != null && !reedmePrice.equals("")) {
            featureJson.put("reedmePrice", reedmePrice);
        }
        if (reedmePoint != null && !reedmePoint.equals("")) {
            featureJson.put("reedmePoint", reedmePoint);
        }
        JSONObject featureListJson = new JSONObject();
        featureListJson.put(packObj.getBucketName(), featureJson);
        JSONArray featureArray = new JSONArray();
        PackageManagement pkMangObj = new PackageManagement();
        int retValue = 0;
        packObj.setStatus(GlobalStatus.ACTIVE);
        String PartnerVisibility[] = request.getParameterValues("visibilityPartner");
        String partnerId = "";
        if (PartnerVisibility != null) {
            for (int i = 0; i < PartnerVisibility.length; i++) {
                partnerId += PartnerVisibility[i] + ",";
            }
        }
        packObj.setPartnerVisibility(partnerId);
        try {
            if (packObj.getFeatureList() == null || packObj.getFeatureList().equals("")) {
                featureArray.add(featureListJson);
                packObj.setFeatureList(featureArray.toString());
                if (makerChakerObj != null) {
                    if (makerChakerObj.status == GlobalStatus.SUSPEND) {
                        packObj.setStatus(GlobalStatus.APPROVED);
                    }
                }
                retValue = pkMangObj.editPackage(SessionId, packObj);
            } else {
                boolean flag = true;
                String jsonString = packObj.getFeatureList();
                org.json.JSONArray jsOld = new org.json.JSONArray(jsonString);
                if (makerChakerObj != null) {
                    if (makerChakerObj.status == GlobalStatus.SUSPEND) {
                        packObj.setStatus(GlobalStatus.APPROVED);
                    }
                } else {
                    packObj.setStatus(GlobalStatus.APPROVED);
                }
                for (int i = 0; i < jsOld.length(); i++) {
                    org.json.JSONObject jsonexists1 = jsOld.getJSONObject(i);
                    if (jsonexists1.has(packObj.getBucketName())) {
                        jsonexists1.remove(packObj.getBucketName());
                        jsonexists1.put(packObj.getBucketName(), featureJson);
                        jsOld.put(i, jsonexists1);
                        packObj.setFeatureList(jsOld.toString());
                        retValue = pkMangObj.editPackage(SessionId, packObj);
                        flag = false;
                        break;
                    }
                }
                if (flag) {
                    org.json.JSONObject jsObject = new org.json.JSONObject();
                    jsObject.put(packObj.getBucketName(), featureListJson);
                    org.json.JSONArray jsonArrayObject = new org.json.JSONArray(jsonString);
                    jsonArrayObject.put(jsObject);
                    packObj.setFeatureList(jsonArrayObject.toString());
                    retValue = pkMangObj.editPackage(SessionId, packObj);
                }
            }
            if (retValue == -1) {
                result = "error";
                message = "Feature does not saved.";
                json.put("result", result);
                logger.debug("Response of #EditFeature from #PPAdmin Servlet's Parameter  result is " + result);
                json.put("message", message);
                logger.debug("Response of #EditFeature from #PPAdmin Servlet's Parameter  message is " + message);
            } else {
                SgReqbucketdetails reqObj = new SgReqbucketdetails();
                SgReqbucketdetails existsObj = new RequestPackageManagement().getReqPackageByName(SessionId, channelId, packObj.getBucketName());
                
                if (makerChakerObj != null) {
                    if (makerChakerObj.status == GlobalStatus.ACTIVE) {
                        reqObj.setMakerflag(1);
                        reqObj.setStatus(packObj.getStatus());
                        message = "Package created successfully and send Checker to validate";
                    } else {
                        reqObj.setStatus(GlobalStatus.APPROVED);
                        reqObj.setMakerflag(0);
                    }
                } else {
                    reqObj.setStatus(packObj.getStatus());
                    reqObj.setMakerflag(GlobalStatus.APPROVED);
                }
                reqObj.setMainCredits(packObj.getMainCredits());
                if (existsObj == null) {
                    reqObj.setChannelId(packObj.getChannelId());
                    reqObj.setBucketName(packObj.getBucketName());
                    reqObj.setPlanAmount(packObj.getPlanAmount());
                    reqObj.setPaymentMode(packObj.getPaymentMode());
                    reqObj.setBucketDuration(packObj.getBucketDuration());
                    reqObj.setPartnerVisibility(packObj.getPartnerVisibility());
                    reqObj.setMinimumBalance(packObj.getMinimumBalance());
                    reqObj.setFreeCredits(packObj.getFreeCredits());
                    reqObj.setDaysForFreeTrial(packObj.getDaysForFreeTrial());
                    reqObj.setServiceCharge(packObj.getServiceCharge());
                    reqObj.setChangePackageRate(packObj.getChangePackageRate());
                    reqObj.setReActivationCharge(packObj.getReActivationCharge());
                    reqObj.setCancellationRate(packObj.getCancellationRate());
                    reqObj.setLatePenalitesRate(packObj.getLatePenaltyRate());
                    reqObj.setTax(packObj.getTax());
                    reqObj.setCreationDate(packObj.getCreationDate());
                    reqObj.setApRateDetails(packObj.getApRateDetails());
                    reqObj.setSlabApRateDetails(packObj.getSlabApRateDetails());
                    reqObj.setSecurityAndAlertDetails(packObj.getSecurityAndAlertDetails());
                    reqObj.setFeatureList(packObj.getFeatureList());
                    reqObj.setTierRateDetails(packObj.getTierRateDetails());
                    reqObj.setApiThrottling(packObj.getApiThrottling());
                    reqObj.setFlatPrice(packObj.getFlatPrice());
                    reqObj.setPackageDescription(packObj.getPackageDescription());
                    reqObj.setRecurrenceBillingPlanId(packObj.getRecurrenceBillingPlanId());
                    new RequestPackageManagement().CreateReqPackage(SessionId, channelId, reqObj);
                } else {
                    reqObj.setChannelId(existsObj.getChannelId());
                    reqObj.setBucketName(existsObj.getBucketName());
                    reqObj.setPlanAmount(existsObj.getPlanAmount());
                    reqObj.setPaymentMode(existsObj.getPaymentMode());
                    reqObj.setBucketDuration(existsObj.getBucketDuration());
                    reqObj.setPartnerVisibility(existsObj.getPartnerVisibility());
                    reqObj.setMinimumBalance(existsObj.getMinimumBalance());
                    reqObj.setFreeCredits(existsObj.getFreeCredits());
                    reqObj.setDaysForFreeTrial(existsObj.getDaysForFreeTrial());
                    reqObj.setServiceCharge(existsObj.getServiceCharge());
                    reqObj.setChangePackageRate(existsObj.getChangePackageRate());
                    reqObj.setReActivationCharge(existsObj.getReActivationCharge());
                    reqObj.setCancellationRate(existsObj.getCancellationRate());
                    reqObj.setLatePenalitesRate(existsObj.getLatePenalitesRate());
                    reqObj.setTax(existsObj.getTax());
                    reqObj.setCreationDate(existsObj.getCreationDate());
                    reqObj.setApRateDetails(existsObj.getApRateDetails());
                    reqObj.setSlabApRateDetails(existsObj.getSlabApRateDetails());
                    reqObj.setSecurityAndAlertDetails(existsObj.getSecurityAndAlertDetails());
                    reqObj.setFeatureList(existsObj.getFeatureList());
                    reqObj.setTierRateDetails(existsObj.getTierRateDetails());
                    reqObj.setApiThrottling(existsObj.getApiThrottling());
                    reqObj.setBucketId(existsObj.getBucketId());
                    reqObj.setFlatPrice(existsObj.getFlatPrice());
                    reqObj.setStatus(GlobalStatus.SENDTO_CHECKER);
                    reqObj.setPackageDescription(packObj.getPackageDescription());
                    reqObj.setRecurrenceBillingPlanId(packObj.getRecurrenceBillingPlanId());
                    new RequestPackageManagement().editReqPackage(SessionId, channelId, reqObj);
                   // message = "Package Updated successfully.";
                    message = "Package updated successfully and send Checker to validate";
                }
                json.put("result", result);
                json.put("message", message);
            }
            out.print(json);
            out.flush();
            out.close();
            return;
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("Exception at #EditFeature from #PPAdmin " + e);
        } finally {
            logger.info("Response of #EditFeature from #PPAdmin " + json.toString());
            logger.info("Response of #EditFeature from #PPAdmin Servlet at " + new Date());
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
