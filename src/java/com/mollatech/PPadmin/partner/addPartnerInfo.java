package com.mollatech.PPadmin.partner;

import static com.mollatech.PPadmin.members.editTeamMember.OPERATOR;
import static com.mollatech.PPadmin.members.editTeamMember.REPORTER;
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.commons.RequestedAccessPolicyEntry;
import com.mollatech.serviceguard.nucleus.db.SgUsers;
import com.mollatech.serviceguard.nucleus.db.connector.management.PartnerRequestManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.UsersManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author Bluebricks
 */
public class addPartnerInfo extends HttpServlet {

//    static final int ACTIVE_STATUS = 1;
//
//    static final int SUSPENDED_STATUS = 0;
//
//    static final int PENDING_STATUS = -2;
//
//    public final int PASSWORD = 1;
//
//    public static final int USER_PASSWORD = 1;
//
    public static final int ADMIN = 0;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Request added successfully";
        PrintWriter out = response.getWriter();
        int TPD = -1, TPS = -1, resp = -1;
        int pId = -1;
        String partnerId = request.getParameter("partnerId");
        if (partnerId != null) {
            pId = Integer.parseInt(partnerId);
        }
        String pName = request.getParameter("partnerName");
        String pEmail = request.getParameter("partnerEmail");
        String pMobileNo = request.getParameter("partnerMobNo");
        int pStatus = GlobalStatus.ACTIVE;
        String ipAddress = request.getParameter("partnerIP");
        String ipForLive = request.getParameter("partnerIPLive");
        String pWebsite = request.getParameter("partnerWebsite");
        String pLandline = request.getParameter("partnerLandlineNo");
        String pFax = request.getParameter("partnerFax");
        String pCompanyName = request.getParameter("partnerComName");
        String pAddress = request.getParameter("partnerAddress");
        String pPincode = request.getParameter("partnerPincode");
        String pSD = request.getParameter("partnerSD");
        String pED = request.getParameter("partnerED");
        String pST = request.getParameter("partnerST");
        String pET = request.getParameter("partnerET");
        String _pTPD = request.getParameter("partnerTPD");
        String _pTPS = request.getParameter("partnerTPS");
        String Operator = "Admin";
        if (_pTPD != null) {
            if (!_pTPD.isEmpty()) {
                TPD = Integer.parseInt(_pTPD);
            }
        }
        if (_pTPS != null) {
            if (!_pTPS.isEmpty()) {
                TPS = Integer.parseInt(_pTPS);
            }
        }
        RequestedAccessPolicyEntry ap = new RequestedAccessPolicyEntry();
        ap.setApEndTime(pET);
        ap.setApStartTime(pST);
        ap.setDayFrom(pSD);
        ap.setDayTo(pED);
        ap.setTpd(TPD);
        ap.setTps(TPS);
        UsersManagement um = new UsersManagement();
        String channelId = (String) request.getSession().getAttribute("_channelId");
        String sessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        SgUsers rdetails = um.getSgUsers(sessionId, channelId, pName);
        if (rdetails == null) {
            String portalSessionId = (String) request.getSession().getAttribute("_partnerSessionId");
            if (sessionId != null) {
                SgUsers rssUser = new SgUsers();
                rssUser.setUsername(pName);
                rssUser.setChannelid(channelId);
                rssUser.setEmail(pEmail);
                rssUser.setPhone(pMobileNo);
                rssUser.setLastlogindate(new Date());
                rssUser.setCreatedon(new Date());
                rssUser.setStatus(GlobalStatus.ACTIVE);
                rssUser.setType(ADMIN);
                if (Operator.equalsIgnoreCase("OPERATOR")) {
                    rssUser.setType(OPERATOR);
                }
                if (Operator.equalsIgnoreCase("REPORTER")) {
                    rssUser.setType(REPORTER);
                }
                resp = um.AddSgUsers(sessionId, channelId, rssUser);
                if (resp == 0) {
                    int poStatus = new PartnerRequestManagement().addPartnerRequestInfo(portalSessionId, pId, pName, pStatus, pMobileNo, pEmail, ipAddress, pWebsite, pFax, pLandline, pAddress, pPincode, pCompanyName, ap, ipForLive);
                    if (poStatus == 0) {
                        message = "Developer's Detail updated Successfully";
                    } else {
                        message = "Developer's Detail  not added";
                    }
                } else if (resp == -1) {
                    result = "error";
                    message = "Developer's Detail not added";
                }
            }
        } else {
            result = "error";
            message = "Developer Name not available";
        }
        try {
            json.put("_result", result);
            json.put("_message", message);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
