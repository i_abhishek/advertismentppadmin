/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.partner.account;

import static com.mollatech.PPadmin.partner.account.ChangeStatusForAPI.logger;
import com.mollatech.serviceguard.nucleus.commons.GlobalStatus;
import com.mollatech.serviceguard.nucleus.db.SgProductionAccess;
import com.mollatech.serviceguard.nucleus.db.SgRequestForProduction;
import com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestProductionManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author mohanish
 */
@WebServlet(name = "ChangeStatusForProductionAccess", urlPatterns = {"/ChangeStatusForProductionAccess"})
public class ChangeStatusForProductionAccess extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        logger.info("Requested Servlet is ChangeStatusForProductionAccess from PPAdmin at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        String result = "success";
        String message = "Status updated successfully";
        JSONObject json = new JSONObject();
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        
        String status = request.getParameter("status");
        String productionAccessId = request.getParameter("productionAccessId");
        int productionAccId = -1; 
        try{
            if( productionAccessId != null && !productionAccessId.isEmpty()){
                productionAccId = Integer.parseInt(productionAccessId);
            }
            SgProductionAccess productionAccessObj = new ProductionAccessManagement().getDetailsByProductionId(SessionId, channelId, productionAccId);

            if(productionAccessObj != null){
                SgRequestForProduction[] productionRequestObj = new RequestProductionManagement().getDetails(SessionId, channelId, productionAccessObj.getAccesPointId(), productionAccessObj.getResourceId(), productionAccessObj.getVersion(), productionAccessObj.getPartnerid());
                if(productionRequestObj != null){
                    for(int i=0; i<productionRequestObj.length;i++){
                        productionRequestObj[i].setStatus(Integer.parseInt(status));
                        new RequestProductionManagement().updateRequest(SessionId, productionRequestObj[i]);
                    }
                    
                    productionAccessObj.setStatus(Integer.parseInt(status));
                    int res = new ProductionAccessManagement().updateRequest(SessionId, productionAccessObj);
                    if (res == 0) {
                        result = "success";
                        if (Integer.parseInt(status) == GlobalStatus.APPROVED) {
                            message = "Request Approved Successfully. ";
                        } else if(Integer.parseInt(status) == GlobalStatus.SUSPEND) {
                            message = "Request Suspend Successfully. ";
                        }else if(Integer.parseInt(status) == GlobalStatus.TERMINATED) {
                            message = "Request Terminated Successfully. ";
                        }
                    } else {
                        result = "error";
                        if (Integer.parseInt(status) == GlobalStatus.APPROVED) {
                            message = "Error in  Approving Request. ";
                        } else if(Integer.parseInt(status) == GlobalStatus.SUSPEND){
                            message = "Error in  Rejecting Request. ";
                        }else if(Integer.parseInt(status) == GlobalStatus.TERMINATED) {
                            message = "Error in Request Suspend Successfully. ";
                        }
                    }
                }else{
                    result  = "error";
                    message = "Error in Checking Availability";
                }
            }else{
                result  = "error";
                message = "Error in Checking Availability";
            }
            json.put("result", result);
            json.put("message", message);
        }catch(Exception e){
            logger.error("Exception at ChangeStatusForProductionAccess ", e);
            result = "error";
            message = "Error in Checking Availability";
            json.put("result", result);
            logger.debug("Response of ChangeStatusForProductionAccess Servlet's from PPAdmin Parameter   result is " + result);
            json.put("message", message);
            logger.debug("Response of ChangeStatusForProductionAccess Servlet's from PPAdmin Parameter   message is " + message);
        }finally{
            logger.info("Response of ChangeStatusForProductionAccess from PPAdmin" + json.toString());
            logger.info("Response of ChangeStatusForProductionAccess Servlet from PPAdmin at " + new Date());
            out.print(json);
            out.flush();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
