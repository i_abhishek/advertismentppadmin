package com.mollatech.PPadmin.resource;

import com.mollatech.serviceguard.nucleus.db.SgResourceprice;
import com.mollatech.serviceguard.nucleus.db.connector.management.ResourcePriceMgmt;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author Ash
 */
@WebServlet(name = "EditAPIPrice", urlPatterns = { "/EditAPIPrice" })
public class EditAPIPrice extends HttpServlet {

    static final Logger logger = Logger.getLogger(EditAPIPrice.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        logger.info("Requested Servlet is EditAPIPrice at " + new Date());
        logger.info("Remote Ip Address is " + request.getRemoteAddr());
        JSONObject json = new JSONObject();
        try {
            int resId = Integer.parseInt(request.getParameter("resId"));
            logger.debug("value of resId : " + resId);
            SgResourceprice resourceprice = new ResourcePriceMgmt().getDetailsFromResId(resId);
            String[] apiName = (String[]) request.getSession().getAttribute("apiDetails");
            if (apiName != null) {
                JSONObject apiPrice = new JSONObject();
                for (String s : apiName) {
                    logger.debug("Price of " + s + " API is : " + request.getParameter("_eAPI" + s));
                    apiPrice.put(s, request.getParameter("_eAPI" + s));
                }
                if (resourceprice == null) {
                    resourceprice = new SgResourceprice();
                    resourceprice.setCreatedOn(new Date());
                    resourceprice.setPrice(apiPrice.toString());
                    resourceprice.setResourceId(resId);
                    resourceprice.setUpdatedOn(new Date());
                    int result = new ResourcePriceMgmt().addDetails(resourceprice);
                    if (result == 0) {
                        json.put("result", "success");
                        json.put("message", "API Price Saved Successfully.");
                        return;
                    } else {
                        json.put("result", "error");
                        json.put("message", "Error in Saving API Price.");
                        return;
                    }
                } else {
                    resourceprice.setPrice(apiPrice.toString());
                    resourceprice.setUpdatedOn(new Date());
                    int result = new ResourcePriceMgmt().updateDetails(resourceprice);
                    if (result == 0) {
                        json.put("result", "success");
                        json.put("message", "API Price Saved Successfully.");
                        return;
                    } else {
                        json.put("result", "error");
                        json.put("message", "Error in Saving API Price.");
                        return;
                    }
                }
            } else {
                json.put("result", "error");
                json.put("message", "Error in Saving API Price.");
                return;
            }
        } catch (Exception ex) {
            logger.error("Exception at EditAPIPrice ", ex);
            json.put("result", "error");
            json.put("message", "Error in Saving API Price.");
        } finally {
            logger.info("Response of EditAPIPrice " + json.toString());
            logger.info("Response of EditAPIPrice Servlet at " + new Date());
            out.print(json);
            out.flush();
            out.close();
        }
    }

    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
