/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.resource;

import com.mollatech.service.nucleus.crypto.LoadSettings;
import com.mollatech.serviceguard.connector.communication.SGStatus;
import com.mollatech.serviceguard.nucleus.db.Operators;
import com.mollatech.serviceguard.nucleus.db.SgResourceowner;
import com.mollatech.serviceguard.nucleus.db.connector.management.OperatorsManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.ResourceOwnerManagement;
import com.mollatech.serviceguard.nucleus.settings.SendNotification;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author urmila
 */
@WebServlet(name = "RejectResourceRequest", urlPatterns = {"/RejectResourceRequest"})
public class RejectResourceRequest extends HttpServlet {
    
    //static final int REQUEST_ACTIVE_STATUS = 1;
    //static final int REQUEST_SUSPENDED_STATUS = 0;
    public static final int SEND = 0;
    static final int PENDING = -2;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        JSONObject json = new JSONObject();
        String result = "success";
        String message = "Resource rejected successfully";
        PrintWriter out = response.getWriter();
        
        String resourceId = request.getParameter("_ResourceId");
        String resourceStatus = request.getParameter("_Resourcestatus");
        String reason = request.getParameter("reason");
        int ireqStatus = 0; int iresourceId=0;
        if (resourceStatus != null) {
            ireqStatus = Integer.parseInt(resourceStatus);
        }
        
        if(resourceId != null){
            iresourceId = Integer.parseInt(resourceId);
        }
        
        int a = -1;
        ResourceOwnerManagement rm = new ResourceOwnerManagement();
        
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        String channelName = "ServiceGuard Portal";
        SgResourceowner sgresource = new ResourceOwnerManagement().getResourceownerbyId(SessionId, iresourceId);
        if (SessionId != null) {            
            a = rm.RejectResourceRequest(SessionId, channelId, iresourceId, ireqStatus, reason);            
            if (a == 0) {                       
                result = "success";
                message = "Resource rejected successfully";
                //new code added here
                int productType = 3;
                Operators[] operatorObj = new OperatorsManagement().getAdminOperator(channelId);
                        String[] operatorEmail = null;
                        if (operatorObj != null) {
                            operatorEmail = new String[operatorObj.length];
                            for (int i = 0; i < operatorObj.length; i++) {
                                operatorEmail[i] = (String) operatorObj[i].getEmailid();
                            }
                        }
                 
                String tmessage = LoadSettings.g_templateSettings.getProperty("email.resource.reject") ;
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy hh:mm:ss");
                  if (tmessage != null) {
                            Date d = new Date();
                            tmessage = tmessage.replaceAll("#Name#", sgresource.getResourceOwnerName());
                            tmessage = tmessage.replaceAll("#EmailId#", sgresource.getResourceOwnerEmailid());
                            tmessage = tmessage.replaceAll("#channel#", channelName);
                            tmessage = tmessage.replaceAll("#datetime#", sdf.format(d));
                            tmessage = tmessage.replaceAll("#RejectReason#", reason);
                        }
                  
                    String Subject = LoadSettings.g_subjecttemplateSettings.getProperty("email.resource.reject.subject");
                    SGStatus statusSG = new SendNotification().SendEmail(channelId, sgresource.getResourceOwnerEmailid(), Subject, tmessage, operatorEmail, null, null, null, productType);
                        if (statusSG.iStatus != PENDING && statusSG.iStatus != SEND) {
                            result = "error";
                            message = sgresource.getResourceOwnerName() + ", Resource request rejected successfully but failed to sent email notification.";
               
                        try {
                                json.put("result", result);
                                json.put("message", message);
                                out.print(json);
                                out.flush();
                                return;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }                            
                        }                            
            //end of code changes
            
            } else if (a == 3) {
                result = "error";
                message = "Resource already rejected  ";

            } else {
                result = "error";
                message = "Resource did not rejected ";
            }
            
        }
        try {
            json.put("result", result);
            json.put("message", message);
            out.print(json);
            out.flush();
            out.close();
            return;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            out.print(json);
            out.flush();
            out.close();
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
