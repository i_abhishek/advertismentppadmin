/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mollatech.PPadmin.reports;

import com.mollatech.serviceguard.nucleus.db.Accesspoint;
import com.mollatech.serviceguard.nucleus.db.ResourceDetails;
import com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.RequestTrackingManagement;
import com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

/**
 *
 * @author Ashu
 */
@WebServlet(name = "GetGraphicalReport", urlPatterns = {"/GetGraphicalReport"})
public class GetGraphicalReport extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        JSONObject json = new JSONObject();
        String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
        String channelId = (String) request.getSession().getAttribute("_channelId");
        String apName = request.getParameter("_Accesspoint2");
        String resName = request.getParameter("_ResourceForSlabPricing2");
        String version = request.getParameter("_VersionForSlabPricing2");
        String apiName = request.getParameter("_APIForSlabPricing2");
        String partnerId = request.getParameter("partnerS");
        String _enddate = request.getParameter("_enddate");
        String _startdate = request.getParameter("_startdate");
        SimpleDateFormat myFormat = new SimpleDateFormat("MM/dd/yyyy");
        String result = "success";
        String message = "";
        Integer pId = null;
        try {
            if (apName.equals("-1")) {
                result = "error";
                message = "Please Select Access Point.";
                json.put("result", result);
                json.put("message", message);
                return;
            }
            if (_enddate == null || _startdate == null) {
                result = "error";
                message = "Please Select Date Range.";
                json.put("result", result);
                json.put("message", message);
                return;
            }
            Date date1 = myFormat.parse(_startdate);
            Date date2 = myFormat.parse(_enddate);
            long diff = date2.getTime() - date1.getTime();
            long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            if (days <= -1) {
                result = "error";
                message = "Please Select Proper Date Range.";
                json.put("result", result);
                json.put("message", message);
                return;
            }
            if (!partnerId.equals("-1")) {
                pId = Integer.parseInt(partnerId);
            }
            Accesspoint accesspoint = new AccessPointManagement().getAccessPointByName(SessionId, channelId, apName);
            String resources = accesspoint.getResources();
            int resourceId = -1;
            if (resources != null) {
                String[] resourcesDetails = resources.split(",");
                if (resourcesDetails != null) {
                    for (int i = 0; i < resourcesDetails.length; i++) {
                        resourceId = Integer.parseInt(resourcesDetails[i]);
                        ResourceDetails rs = new ResourceManagement().getResourceById(resourceId);
                        if (rs.getName().equals(resName)) {
                            break;
                        }
                    }
                }
            }
            Calendar calendarStart = Calendar.getInstance();
            RequestTrackingManagement management = new RequestTrackingManagement();
            ArrayList<bar> sample = new ArrayList<bar>();
            for (int i = 0; i <= days; i++) {
                calendarStart.setTime(myFormat.parse(_startdate));
                calendarStart.add(Calendar.DATE, i);
                date1 = myFormat.parse(myFormat.format(calendarStart.getTime()));
                sample.add(new bar(management.getAPIUsageCount(channelId, accesspoint.getApId(), resourceId, pId, apiName, Integer.parseInt(version), date1), myFormat.format(date1)));
//                jsonOutput.put("" + date1, management.getAPIUsageCount(channelId, accesspoint.getApId(), resourceId, pId, apiName, Integer.parseInt(version), date1));
//                System.out.println(" calendarStart " + date1);
            }
            Gson gson = new Gson();
            JsonElement element = gson.toJsonTree(sample, new TypeToken<List<bar>>() {
            }.getType());
            JsonArray jsonArray = element.getAsJsonArray();
            json.put("result", result);
            json.put("message", jsonArray);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            out.print(json);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
