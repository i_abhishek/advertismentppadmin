<%--<%@page import="com.partner.web.Accesspoint"%>--%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPartnerrequest"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerRequestManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="java.io.ObjectInputStream"%>
<%@page import="java.io.ByteArrayInputStream"%>
<%--<%@page import="com.partner.web.RequestedAccessPolicyEntry"%>
<%@page import="com.partner.web.SgPartnerrequest"%>--%>
<%@page import="java.nio.channels.Channels"%>
<%--<%@page import="portalconnect.PartnerPortalWrapper"%>--%>
<%@include file="header.jsp" %>
<script src="./js/partnerRequest.js"></script>
<script src="./js/profile.js"></script>
<%//    RequestedAccessPolicyEntry accespolicy = null;
    String _partnerId = request.getParameter("_partnerId");
    int partnerId = Integer.parseInt(_partnerId);
    PartnerDetails partnerDetails = new PartnerManagement().getPartnerDetails(SessionId, channelId, partnerId);
    SgPartnerrequest Preq = new PartnerRequestManagement().getPartnerRequestsPartnerbyId(SessionId, partnerId);
%>
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header text-uppercase">Information of <%= partnerDetails.getPartnerName()%></h3>
            </div>
            <!-- /.col-lg-10 -->
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47;  <i class="fa fa-user"></i><a href="partners.jsp"> Developer Manager</a>&#47; <b>Developer Information</b>
                    </div>
                    <div class="panel-body">
                        <%if (Preq != null) {%>
                        <form class="form-validate form-horizontal" id="update-profile" method="POST" action="#">                    
                            <div class="row" style="margin-left: 10px;">
                                <label >
                                    <font style=" font-weight: bold; font-size: 20px" class="text-primary">Company Information&nbsp;&nbsp;</font>
                                </label>                                                               
                            </div>                                    
                            <br>                    
                            <div class="row">
                                <font style=" font-weight: bolder;">
                                <label for="curl" class="control-label col-lg-2">Company Name <span class="required">*</span></label>
                                </font>
                                <div class="col-lg-4">
                                    <input class="form-control " id="partnerComName" name="partnerComName" placeholder="Company Name" type="text" value="<%= Preq.getComapanyRegName()%>" required/>
                                </div>
                                <font style=" font-weight: bolder;">
                                <label for="curl" class="control-label col-lg-2">Office Landline <span class="required">*</span></label>
                                </font>
                                <div class="col-lg-4">
                                    <input class="form-control " id="partnerLandlineNo" name="partnerLandlineNo" placeholder="office landline" type="text" value=" <%= Preq.getLandline()%> " required/>
                                </div>

                            </div>
                            <br>
                            <div class="row">
                                <font style=" font-weight: bolder;">
                                <label for="cemail" class="control-label col-lg-2">Fax number <span class="required">*</span></label>
                                </font>
                                <div class="col-lg-4">
                                    <input class="form-control " id="partnerFax"  name="partnerFax"  type="text"  placeholder="Fax Number" value=" <%= Preq.getFax()%> " required />
                                </div>
                                <font style=" font-weight: bolder;">
                                <label for="curl" class="control-label col-lg-2">Website <span class="required">*</span></label>
                                </font>
                                <div class="col-lg-4">
                                    <input class="form-control " id="partnerWebSite" name="partnerWebsite" placeholder="Companty Website" type="url" value=" <%= Preq.getWebsite()%> " required/>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <font style=" font-weight: bolder;">
                                <label for="curl" class="control-label col-lg-2">IP (Test)<span class="required">*</span></label>
                                </font>
                                <div class="col-lg-4">
                                    <input class="form-control " id="partnerIP" name="partnerIP" placeholder="IP Test" type="text" value=" <%= Preq.getIpAddress()%> " required/>
                                </div>
                                <font style=" font-weight: bolder;">
                                <label for="curl" class="control-label col-lg-2">IP (Live)<span class="required">*</span></label>
                                </font>
                                <div class="col-lg-4">
                                    <input class="form-control " id="partnerIPLive" name="partnerIPLive" placeholder="IP Live" type="text" value=" <%= Preq.getIpLive()%> " required/>
                                </div>                            
                            </div>
                            <br>   
                            <div class="row">
                                <font style=" font-weight: bolder;">
                                <label for="cname" class="control-label col-lg-2">Company Address <span class="required">*</span></label>
                                </font>
                                <div class="col-lg-4">
                                    <input class="form-control" id="partnerAddress" name="partnerAddress" minlength="5" placeholder="Company Address" type="text" value=" <%= Preq.getAddress()%> " required />
                                </div>
                                <font style=" font-weight: bolder;">
                                <label for="cname" class="control-label col-lg-2"> Pin Code <span class="required">*</span></label>
                                </font>
                                <div class="col-lg-4">
                                    <input class="form-control" id="partnerPincode" name="partnerPincode" minlength="5" type="number" placeholder="Pincode" value=<%= Preq.getPincode()%>  required />
                                </div>
                            </div>                                      
                            <br>
                            <div class="row" style="margin-left: 10px;">
                                <font style="font-weight: bold; font-size: 20px" class="text-primary">Administrator Information&nbsp;&nbsp;</font>
                            </div>
                            <br>
                            <div class="row">
                                <font style=" font-weight: bolder;">
                                <label for="cname" class="control-label col-lg-2">  &nbsp;&nbsp;Full Name&nbsp;&nbsp;<span class="required">*</span></label>
                                </font>
                                <div class="col-lg-4">
                                    <input class="form-control" id="partnerName" name="partnerName" minlength="4" type="text" placeholder="Name" value="<%= partnerDetails.getPartnerName()%>" required readonly/>
                                </div>
                                <font style=" font-weight: bolder;">
                                <label for="cemail" class="control-label col-lg-2">E-Mail <span class="required">*</span></label>
                                </font>
                                <div class="col-lg-4">
                                    <input class="form-control " id="partnerEmail" name="partnerEmail" type="email" name="email" placeholder="Email Id" value="<%= partnerDetails.getPartnerEmailid()%>" required />
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <font style=" font-weight: bolder;">
                                <label for="cemail" class="control-label col-lg-2">Mobile number <span class="required">*</span></label>
                                </font>
                                <div class="col-lg-4">
                                    <input class="form-control " id="partnerMobNo"  name="partnerMobNo"  type="text" placeholder="Mobile Number" value="<%= partnerDetails.getPartnerPhone()%>" required />
                                </div>
                            </div>
                            <br>                                
                            <!--                        <div class="row" style="margin-left: 10px;">
                                                        <font style="font-weight: bold; font-size: 20px" class="text-primary">Throughput Requirement Details &nbsp;&nbsp;</font>
                                                    </div>  
                                                    <br>
                                                    <div class="row">
                                                        <font style=" font-weight: bolder;">
                                                        <label for="cname" class="control-label col-lg-2">Required TPS </label>
                                                        </font>
                                                        <div class="col-lg-2">
                            
                                                            <input class="form-control" id="partnerTPS" name="partnerTPS" minlength="1" type="number" readonly value=  placeholder="Required TPS"  />
                                                        </div>
                            
                                                        <font style=" font-weight: bolder;">
                                                        <label for="cname" class="control-label col-lg-2">Required TPD </label>
                                                        </font>
                                                        <div class="col-lg-2">
                            
                                                            <input class="form-control" id="partnerTPD" name="partnerTPD" minlength="2" type="number" readonly value=  placeholder="Required TPD"   />
                                                        </div>
                            
                                                        <font style=" font-weight: bolder;">
                                                        <label for="cname" class="control-label col-lg-2">Required Start Time </label>
                                                        </font>
                                                        <div class="col-lg-2">
                            <%
//                                    String apstart = null;
//                                    if (Preq.getApStartTime() == null || Preq.getApStartTime().isEmpty()) {
//                                        apstart = "NA";
//                                    }
//                                    if (Preq.getApStartTime() != null && !Preq.getApStartTime().isEmpty()) {
//                                        apstart = Preq.getApStartTime();
//                                    }
                            %>
                            <input class="form-control" id="partnerST" name="partnerST" readonly value= type="time"/>
                        </div>
                    </div>                                                                                                         
                    <br>
                    <div class="row">
                        <font style=" font-weight: bolder;">
                        <label for="cname" class="control-label col-lg-2">Required End Time </label>
                        </font>
                            <%
//                                String apend = null;
//                                if (Preq.getApEndTime() == null || Preq.getApEndTime().isEmpty()) {
//                                    apend = "NA";
//                                }
//                                if (Preq.getApEndTime() != null && !Preq.getApEndTime().isEmpty()) {
//                                    apend = Preq.getApEndTime();
//                                }
                            %>
                            <div class="col-lg-2">
                                <input class="form-control" readonly id="partnerET" name="partnerET" value= type="time"/>
                            </div>

                            <font style=" font-weight: bolder;">
                            <label for="cname" class="control-label col-lg-2">Required Start Day </label>
                            </font>
                            <div class="col-lg-2">
                                <select class="form-control" readonly id="partnerSD" name="partnerSD" value=>
                                    <option value="sunday">Sunday</option>
                                    <option value="monday">Monday</option>
                                    <option value="tuesday">Tuesday</option>
                                    <option value="wednesday">Wednesday</option>
                                    <option value="thursday">Thursday</option>
                                    <option value="friday">Friday</option>
                                    <option value="saturday">Saturday</option>
                                </select>
                            </div>

                            <font style=" font-weight: bolder;">
                            <label for="cname" class="control-label col-lg-2">Required End Day </label>
                            </font>
                            <div class="col-lg-2">                                            
                                <select class="form-control" id="partnerED" readonly name="partnerED" value=>
                                    <option value="sunday">Sunday</option>
                                    <option value="monday">Monday</option>
                                    <option value="tuesday">Tuesday</option>
                                    <option value="wednesday">Wednesday</option>
                                    <option value="thursday">Thursday</option>
                                    <option value="friday">Friday</option>
                                    <option value="saturday">Saturday</option>
                                </select>
                            </div>
                        </div>                                                                                                         
                        <br> -->
                            <br>
                            <div class="row" style="margin-left: 10px;">
                                <font style="font-weight: bold; font-size: 20px" class="text-primary">Access Policy Details :&nbsp;&nbsp;</font>
                            </div>  
                            <br>
                            <div class="row">
                                <font style=" font-weight: bolder;">
                                <label for="cname" class="control-label col-lg-2">Select Access Point</label>
                                </font>

                                <div class="col-lg-2">
                                    <%                                    Accesspoint[] ap = new AccessPointManagement().getAccessPointBypartnerId(SessionId, channelId, partnerDetails.getPartnerId());
                                    %>
                                    <select id="accesspointofpartnerId" class="form-control span6" onchange="changeDetails(this.value)">
                                        <option value="-1" disabled selected> Select</option>
                                        <%
                                            if (ap != null) {
                                                if (ap.length != 0) {
                                                    for (int i = 0; i < ap.length; i++) {
                                                        if (ap[i].getStatus() != -99) {
                                        %>
                                        <option value="<%= ap[i].getApId()%>"><%= ap[i].getName()%></option>
                                        <%}
                                                    }
                                                }
                                            }
                                        %>
                                    </select>
                                </div>                                    
                            </div>           
                            <br>
                            <div id="accessplicy"></div>      
                            <br>
                        </form>
                        <%} else {%>
                        <form class="form-validate form-horizontal" id="update-profile" method="POST" action="#">                    
                            <div class="row" style="margin-left: 10px;">
                                <label >
                                    <font style=" font-weight: bold; font-size: 20px" class="text-primary">&nbsp;&nbsp;Company Information&nbsp;&nbsp;</font>
                                </label>                                                               
                            </div>                                    
                            <br>                    
                            <div class="row">
                                <font style=" font-weight: bolder;">
                                <label for="curl" class="control-label col-lg-2">Company Name <span class="required">*</span></label>
                                </font>
                                <div class="col-lg-4">
                                    <input class="form-control " id="partnerComName" name="partnerComName" placeholder="Company Name" type="text"  required/>
                                </div>
                                <font style=" font-weight: bolder;">
                                <label for="curl" class="control-label col-lg-2">Office Landline <span class="required">*</span></label>
                                </font>
                                <div class="col-lg-4">
                                    <input class="form-control " id="partnerLandlineNo" name="partnerLandlineNo" placeholder="office landline" type="text"  required/>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <font style=" font-weight: bolder;">
                                <label for="cemail" class="control-label col-lg-2">Fax number <span class="required">*</span></label>
                                </font>
                                <div class="col-lg-4">
                                    <input class="form-control " id="partnerFax"  name="partnerFax"  type="text"  placeholder="Fax Number"  required />
                                </div>
                                <font style=" font-weight: bolder;">
                                <label for="curl" class="control-label col-lg-2">Website <span class="required">*</span></label>
                                </font>
                                <div class="col-lg-4">
                                    <input class="form-control " id="partnerWebSite" name="partnerWebsite" placeholder="Companty Website" type="url"  required/>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <font style=" font-weight: bolder;">
                                <label for="curl" class="control-label col-lg-2">IP (Test)<span class="required">*</span></label>
                                </font>
                                <div class="col-lg-4">
                                    <input class="form-control " id="partnerIP" name="partnerIP" placeholder="IP address" type="text" value=" <%= partnerDetails.getAllowedIps()%> " required/>
                                </div>
                                <font style=" font-weight: bolder;">
                                <label for="curl" class="control-label col-lg-2">IP (Live)<span class="required">*</span></label>
                                </font>
                                <div class="col-lg-4">
                                    <input class="form-control " id="partnerIPLive" name="partnerIPLive" placeholder="IP address" type="text" value=" <%= partnerDetails.getAllowedIpsForLive()%> " required/>
                                </div>

                            </div>
                            <br>   
                            <div class="row">
                                <font style=" font-weight: bolder;">
                                <label for="cname" class="control-label col-lg-2">Company Address <span class="required">*</span></label>
                                </font>
                                <div class="col-lg-4">
                                    <input class="form-control" id="partnerAddress" name="partnerAddress" minlength="5" placeholder="Companyy Address" type="text" required />
                                </div>
                                <font style=" font-weight: bolder;">
                                <label for="cname" class="control-label col-lg-2"> Pin Code <span class="required">*</span></label>
                                </font>
                                <div class="col-lg-4">
                                    <input class="form-control" id="partnerPincode" name="partnerPincode" minlength="5" type="number" placeholder="Pincode"  required />
                                </div>
                            </div>                                      
                            <br>
                            <div class="row" style="margin-left: 10px;">
                                <font style="font-weight: bold; font-size: 20px" class="text-primary">&nbsp;&nbsp;Administrator Information&nbsp;&nbsp;</font>
                            </div>
                            <br>
                            <div class="row">
                                <font style=" font-weight: bolder;">
                                <label for="cname" class="control-label col-lg-2">  &nbsp;&nbsp;Full Name&nbsp;&nbsp;<span class="required">*</span></label>
                                </font>
                                <div class="col-lg-4">
                                    <input class="form-control" id="partnerId" name="partnerId" minlength type="hidden" value="<%= partnerDetails.getPartnerId()%>" />
                                    <input class="form-control" id="partnerName" name="partnerName" minlength="4" type="text" placeholder="Name" value="<%= partnerDetails.getPartnerName()%>" required readonly/>
                                </div>
                                <font style=" font-weight: bolder;">
                                <label for="cemail" class="control-label col-lg-2">E-Mail <span class="required">*</span></label>
                                </font>
                                <div class="col-lg-4">
                                    <input class="form-control " id="partnerEmail" name="partnerEmail" type="email" name="email" placeholder="Email Id" value="<%= partnerDetails.getPartnerEmailid()%>" required />
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <font style=" font-weight: bolder;">
                                <label for="cemail" class="control-label col-lg-2">Mobile number <span class="required">*</span></label>
                                </font>
                                <div class="col-lg-4">
                                    <input class="form-control " id="partnerMobNo"  name="partnerMobNo"  type="text" placeholder="Mobile Number" value="<%= partnerDetails.getPartnerPhone()%>" required />
                                </div>
                            </div>                                                      
                            <br>
                            <div class="row" style="margin-left: 10px;">
                                <font style="font-weight: bold; font-size: 20px" class="text-primary">&nbsp;&nbsp;Access Policy Details :&nbsp;&nbsp;</font>
                            </div>  
                            <br>
                            <div class="row">
                                <font style=" font-weight: bolder;">
                                <label for="cname" class="control-label col-lg-2">Select Access Point </label>
                                </font>

                                <div class="col-lg-2">
                                    <%
                                        Accesspoint[] ap = new AccessPointManagement().getAccessPointBypartnerId(SessionId, channelId, partnerDetails.getPartnerId());
                                    %>
                                    <select id="accesspointofpartnerId" class="form-control span6" onchange="changeDetails(this.value)">
                                        <option value="-1" disabled selected> Select</option>
                                        <%
                                            if (ap != null) {
                                                if (ap.length != 0) {
                                                    for (int i = 0; i < ap.length; i++) {
                                                        if (ap[i].getStatus() != -99) {
                                        %>
                                        <option value="<%= ap[i].getApId()%>"><%= ap[i].getName()%></option>
                                        <%}
                                                    }
                                                }

                                            }
                                        %>
                                    </select>
                                </div>                                    
                            </div>           
                            <br>
                            <div id="accessplicy"></div>                                                                                                                                                               
                        </form>
                        <button class="btn btn-success" onclick="updateProfile()"><i class="fa fa-pencil"></i> Update Details</button>
                        <%}%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function changeDetails(val) {
        var s = './loadRequestDetails.jsp?_apId=' + val;
        $.ajax({
            type: 'POST',
            url: s,
            success: function (data) {
                document.getElementById("accessplicy").innerHTML = data;
            }
        });
    }
</script>
<script src="bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript 
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.min.js"></script>
<script src="js/morris-data.js"></script>-->

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>
<script src="js/bootbox.min.js" type="text/javascript"></script>
</body>
</html>
<%@include file="footer.jsp" %>
