<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgResourceowner"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceOwnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartyManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPartydetails"%>
<%
    int id = Integer.parseInt(request.getParameter("_id"));
    String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
    SgResourceowner SgResourceowner = new ResourceOwnerManagement().getResourceownerbyId(SessionId, id);
    String firstParty = request.getParameter("_firstParty");
    String secondParty = request.getParameter("_secondParty");
    String thirdParty = request.getParameter("_thirdParty");
    String channelId = (String) request.getSession().getAttribute("_channelId");
    SgPartydetails[] sgPartylist = new PartyManagement().getAllPartyDetails(channelId);
    int firstPartyId = Integer.parseInt(firstParty);
    int secondPartyId = Integer.parseInt(secondParty);
    int thirdPartyId  = Integer.parseInt(thirdParty);
%>
<select id="_selectThirdParties" name="_selectThirdParties"  class="form-control" onchange="" >
<%
        int fourthParty = 0;
        if (firstPartyId != -1) {
        if (SgResourceowner.getData() != null) {
            JSONObject jsonObj = new JSONObject(SgResourceowner.getData());
            if (jsonObj.has("_fourthParty")) {
                SgPartydetails partyObj = new PartyManagement().getPartyById(channelId, Integer.parseInt(jsonObj.getString("_fourthParty")));
                if (partyObj != null && partyObj.getStatus()==GlobalStatus.ACTIVE) {
                    fourthParty = partyObj.getPartyId();
%>                                                        
    <option value="-1">Select Party</option>
    <option selected value="<%=partyObj.getPartyId()%>"><%=partyObj.getPartyName()%></option>
    <%
                }else{%>
    <option value="-1" selected>Select Party</option>            
                                    <%}
                                        }
                                            }else{
                                    %>                         
    <option value="-1" selected>Select Party</option>
    <%
        }
        
            if (sgPartylist != null) {
                for (int i = 0; i < sgPartylist.length; i++) {
                    if (sgPartylist[i].getStatus() == GlobalStatus.ACTIVE) {
                        SgPartydetails apdetails = sgPartylist[i];
                        int partyId = apdetails.getPartyId();
                        if (apdetails != null && partyId != firstPartyId && partyId != secondPartyId && partyId != thirdPartyId) {%>
    <option value="<%=apdetails.getPartyId()%>"><%=apdetails.getPartyName()%></option>
    <%}

                    }
                }
            }
        }else{
    %>
    <option value="-1" selected>Select Party</option>
    <%}%>
</select>