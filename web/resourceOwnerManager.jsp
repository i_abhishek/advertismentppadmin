<%@page import="com.mollatech.serviceguard.nucleus.commons.GlobalStatus"%>
<%@page import="com.mollatech.service.nucleus.crypto.LoadSettings"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Properties"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgResourceowner"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceOwnerManagement"%>
<%@include file="header.jsp" %>
<%
    Properties resourceOwnerRejectionProperties = null;
    resourceOwnerRejectionProperties = LoadSettings.g_resourceOwnerRejectionSettings;;

    String options = "<option value='Select' selected>Select Reason for Rejection </option>\n";

    if (resourceOwnerRejectionProperties != null) {
        Enumeration enuKeys = resourceOwnerRejectionProperties.keys();
        while (enuKeys.hasMoreElements()) {
            String key = (String) enuKeys.nextElement();
            String value = resourceOwnerRejectionProperties.getProperty(key);
            options += "<option value='" + value + "'>" + value + "</option>\n";
        }
    }
%>
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header text-uppercase">Resource Owner Management</h3>
            </div>
            <!-- /.col-lg-10 -->
        </div>
        <div class="row">
            <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47;  <i class="fa fa-table"></i><b> List of Resource Owner</b>
                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal " method="get">
                            <div class="dataTable_wrapper">
                                <table class="table table-bordered table-responsive table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
<!--                                            <td/>-->
                                            <th>No.</th>
                                            <th>Name</th>
                                            <th>Company</th>
                                            <th>Package</th>
<!--                                            <th>Email</th>                                -->
                                            <th>Logo</th>
                                            <th>Heading</th>
                                            <th>Link</th>
                                            <th>Message</th>
                                            <th>Manage</th>
<!--                                            <th>Created</th> -->
                                            <th>Requested On</th> 
                                            <th>Action Taken On</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                            SgResourceowner[] SgResourceowner = new ResourceOwnerManagement().listResourceowners(SessionId); //- 2  pending

                                            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy  hh:mm aa");
                                            if (SgResourceowner != null && SgResourceowner.length > 0) {
                                                int count = 0;
                                                String strStatus = "Active";
                                                for (int i = 0; i < SgResourceowner.length; i++) {

                                                    Date date = SgResourceowner[i].getCreatedon();
                                                    if (SgResourceowner[i].getStatus() == GlobalStatus.PENDING) {
                                                        strStatus = "PENDING";
                                                    } else if (SgResourceowner[i].getStatus() == GlobalStatus.SUSPEND) {
                                                        strStatus = "SUSPENDED";
                                                    }
                                                    if (SgResourceowner[i].getStatus() != GlobalStatus.REJECTED) {
                                                        count++;
                                        %>  

                                        <tr>
                                            <td/>
                                            <td ><%=count%></td>
                                            <td ><%if (SgResourceowner[i].getStatus() == GlobalStatus.APPROVED) {%><a href="./updateOwnerInfo.jsp?_requestId=<%= SgResourceowner[i].getResourceId()%>"><u><%=SgResourceowner[i].getResourceOwnerName()%></u></a><%} else {%><%=SgResourceowner[i].getResourceOwnerName()%><%}%></td>
                                            <td ><a href="./resourceCompInfo.jsp?_requestId=<%= SgResourceowner[i].getResourceId()%>"><u><%=SgResourceowner[i].getComapanyRegName()%></u></a></td>
                                            <td ><%=SgResourceowner[i].getResourceOwnerPhone()%></td>
                                            <td ><%=SgResourceowner[i].getResourceOwnerEmailid()%></td>                               
                                            <td>
                                                <div class="btn-group">                                                    
                                                    <button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown"><%=strStatus%> &nbsp;&nbsp;<span class="caret"></span></button>
                                                    <ul class="dropdown-menu">
                                                        <%if (SgResourceowner[i].getStatus() == GlobalStatus.PENDING) {%>
                                                        <li><a href="#" onclick="changeResourceRequest('<%=SgResourceowner[i].getResourceId()%>', '<%=GlobalStatus.APPROVED%>')">Approve</a></li>
                                                        <li><a href="#" onclick="rejectResourceOwnerRequest('<%=SgResourceowner[i].getResourceId()%>', '<%=GlobalStatus.REJECTED%>')">Reject</a></li>
                                                            <%} else {%>
                                                        <li><a href="#" onclick="changeResourceRequest('<%=SgResourceowner[i].getResourceId()%>', '<%=GlobalStatus.APPROVED%>')">Active</a></li>
                                                        <li><a href="#" onclick="changeResourceRequest('<%=SgResourceowner[i].getResourceId()%>', '<%=GlobalStatus.SUSPEND%>')">Suspend</a></li>
                                                        <li><a class="divider"></a></li>
                                                        <li><a href="#" onclick="setResOwnerPassword('<%=SgResourceowner[i].getResourceId()%>')">Set & Send Password</a></li>
                                                            <%}%>

                                                    </ul>
                                                </div>
                                            </td>                              
                                            <td><%= sdf.format(date)%></td>
                                        </tr>
                                        <%}
                                                }
                                            }else{%>
                                            <tr>
<!--                                                <td/>-->
                                                <td>1</td>
                                                <td>Abhishek</td>
                                                <td>Abhishek Advertisement</td>
                                                <td>Standard</td>
                                                
                                                <td><img src="images/CertificateLogo.png" alt="" width="50"/></td>
                                                <td>Festive Offer</td>
                                                <td>http://www.myshopping.com</td>
                                                <td><button class="btn btn-default btn-xs"> Message&nbsp;&nbsp;</button></td>
                                                <td>
                                                    <div class="btn-group">                                                    
                                                        <button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown"> Approve&nbsp;&nbsp;<span class="caret"></span></button>
                                                        <ul class="dropdown-menu">
                                                            <%if (1 == 1) {%>
                                                            <li><a href="#" onclick="changeResourceRequest('', '<%=GlobalStatus.APPROVED%>')">Approve</a></li>
                                                            <li><a href="#" onclick="rejectResourceOwnerRequest('', '<%=GlobalStatus.REJECTED%>')">Reject</a></li>
                                                                <%} else {%>
                                                            <li><a href="#" onclick="changeResourceRequest('', '<%=GlobalStatus.APPROVED%>')">Active</a></li>
                                                            <li><a href="#" onclick="changeResourceRequest('', '<%=GlobalStatus.SUSPEND%>')">Suspend</a></li>
                                                            <li><a class="divider"></a></li>
                                                            <li><a href="#" onclick="setResOwnerPassword('')">Set & Send Password</a></li>
                                                                <%}%>

                                                        </ul>
                                                    </div>
                                                </td>
                                                <td>05/10/2017 10:20 AM</td>
                                                <td>06/10/2017 11:00 AM</td>
                                            </tr>
                                            <%}%>
                                    </tbody>
                                </table>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div id="rejectResource" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="rejectResourceModal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>

                        <b id="rejectResourceModal">Reject Resource</b>

                    </div>
                    <div class="modal-body">
                        <div class="row-fluid">
                            <form class="form-horizontal" id="rejectResourceForm">
                                <fieldset>
                                    <div class="control-group">
<!--                                        <label class="control-label col-lg-2"  for="partnername">Reason</label>-->
                                        <div class="controls col-lg-12">
                                            <input type="hidden" id="_ResourceId" name="_ResourceId" >
                                            <input type="hidden" id="_Resourcestatus" name="_Resourcestatus" >

<!--                                            <select id="reason" name="reason" class="form-control" >
                                                <%=options%>        
                                            </select>                                                -->
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <span>                                                                
                                                                We back with bigger discounts on your favourite Brands. Go ahead and shop from our Deals across categories
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </table>
                                        </div>
                                    </div>                            
                                </fieldset>
                                            
                            </form>
                        </div>
                    </div>
<!--                    <div class="modal-footer">
                        <div id="edit-partner-result"></div>
                        <button class="btn btn-info btn-xs" data-dismiss="modal">Close</button>
                        <button class="btn btn-success btn-xs" onclick="rejectResourceRequest()" id="addPartnerButtonE">Reject request</button>
                    </div>-->
                </div>
            </div>
        </div>                            
    </div>
</div>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="js/resourcePrice.js"></script>
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="dist/js/sb-admin-2.js"></script>
<script src="js/bootbox.min(1).js" type="text/javascript"></script>

<%@include file="footer.jsp" %>
