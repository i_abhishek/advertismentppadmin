<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.UtilityFunctions"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgNumbers"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.NumbersManagement"%>
<%@include file="header.jsp" %>
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header text-uppercase"> Infoblast Number Management</h3>
            </div>
        </div>
        <div class="row">
            <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <i class="fa fa-home"></i><a href="home.jsp"> Home </a>&#47;  <i class="fa fa-table"></i><b> Edit Number</b>
                    </div>
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover display nowrap" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <td/>
                                        <th>Sr.No</th>
                                        <th>Numbers</th>
                                        <th>Type</th>
                                        <th>Status</th>
                                        <th>Assigned Developer</th>
                                        <th>Created On</th>
                                        <th>Updated On</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        int count = 0;

                                        SgNumbers[] allNumbers = new NumbersManagement().getAllNumbers();
                                        PartnerDetails[] pds = new PartnerManagement().listpartners();
                                        Map<Integer, String> partnerMap = new HashMap();
                                        if (pds != null) {
                                            for (PartnerDetails details : pds) {
                                                partnerMap.put(details.getPartnerId(), details.getPartnerName());
                                            }
                                        }
                                        if (allNumbers != null) {
                                            for (int i = 0; i < allNumbers.length; i++) {
                                                String updatedDate = new UtilityFunctions().getTMReqDate(allNumbers[i].getUpdatedOn());
                                                String createDate = new UtilityFunctions().getTMReqDate(allNumbers[i].getCreatedOn());
                                                String partnerName = "NA";
                                                if (allNumbers[i].getStatus() != GlobalStatus.DELETED) {
                                                    count++;
                                                    if (allNumbers[i].getPartnerId() != null) {
                                                        String partner = partnerMap.get(allNumbers[i].getPartnerId());
                                                        if (partner != null) {
                                                            partnerName = partner;
                                                        }
                                                    }
                                    %>
                                    <tr>
                                        <td/>
                                        <td><%= count%></td>
                                        <td><%= allNumbers[i].getMobileNo()%></td>
                                        <td><%= allNumbers[i].getType()%></td>
                                        <%//  if (allNumbers[i].getPartnerId() != null) {%>
                                        <!--                                        <td><span class="label-success label">Assigned</span></td>-->
                                        <% //} else {%>
                                        <!--                                        <td><span class="label-info label">Unassign</span></td>-->
                                        <%//}%>
                                        <td>
                                            <div class="btn btn-group btn-xs">
                                                <% if (allNumbers[i].getPartnerId() != null) {%>
                                                <button class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown"><%if (allNumbers[i].getStatus() == GlobalStatus.IBNUMBERASSIGN) {%> <font style="font-size: 11px;">ASSIGN</font> <%} else if(allNumbers[i].getStatus() == GlobalStatus.SUSPEND) {%><font style="font-size: 11px;"> SUSPENDED</font> <%}else if(allNumbers[i].getStatus()==GlobalStatus.DELETED){%><font style="font-size: 11px;"> TERMINATED</font> <%}else if(allNumbers[i].getStatus()==GlobalStatus.ACTIVE){%> <font style="font-size: 11px;"> ACTIVE</font><%}%><span class="caret"></span></button>
                                                    <%}else{%>
                                                <button class="btn btn-default btn-xs dropdown-toggle" disabled data-toggle="dropdown"><%if (allNumbers[i].getStatus() == GlobalStatus.IBNUMBERASSIGN) {%> <font style="font-size: 11px;">ASSIGN</font> <%} else if(allNumbers[i].getStatus() == GlobalStatus.SUSPEND) {%><font style="font-size: 11px;"> SUSPENDED</font> <%}else if(allNumbers[i].getStatus()==GlobalStatus.DELETED){%><font style="font-size: 11px;"> TERMINATED</font> <%}else if(allNumbers[i].getStatus()==GlobalStatus.ACTIVE){%> <font style="font-size: 11px;"> ACTIVE</font><%}%><span class="caret"></span></button>
                                                    <%}%>
                                                <ul class="dropdown-menu">
                                                    <li><a href="#"  onclick="changeInfoblastUserStatus(<%=GlobalStatus.IBNUMBERASSIGN%>, <%=allNumbers[i].getNumberId()%>)" >Mark as (Re)Active?</a></li>
                                                    <li><a href="#" onclick="changeInfoblastUserStatus(<%=GlobalStatus.SUSPEND%>, <%=allNumbers[i].getNumberId()%>)" >Mark as Suspended?</a></li>
                                                    <li><a href="#" onclick="unassignInfoblastNo(<%=allNumbers[i].getNumberId()%>)" >Mark as Terminated?</a></li>
                                                    <li><a href="#" onclick="changeInfoblastPassword(<%=allNumbers[i].getNumberId()%>)" >Reset Password?</a></li>
                                                </ul>
                                            </div>
                                        </td>
                                        <td><%=partnerName%></td>
                                        <td><%=createDate%></td>
                                        <td><%=updatedDate%></td>
                                        <td>
                                            <%  if (allNumbers[i].getPartnerId() != null) {%>
                                            <a href="#" class="btn btn-info btn-xs" data-toggle="tooltip"  data-placement="center" disabled><i class="glyphicon glyphicon-edit" ></i></a>
                                            <a href="#" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="center" disabled><i class="glyphicon glyphicon-trash"></i></a>
                                                <%} else {%>
                                            <a href="#" class="btn btn-info btn-xs" data-toggle="tooltip" onclick="editNumberModel('<%=allNumbers[i].getMobileNo()%>', '<%=allNumbers[i].getNumberId()%>', '<%=allNumbers[i].getType()%>')" data-placement="center"  ><i class="glyphicon glyphicon-edit"></i></a>
                                            <a href="#" class="btn btn-danger btn-xs" data-toggle="tooltip" onclick="deleteNo('<%=allNumbers[i].getNumberId()%>')" data-placement="center"><i class="glyphicon glyphicon-trash"></i></a>                                            
                                                <%}%>
                                        </td>
                                    </tr>
                                    <%}
                                            }
                                        }%>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="editNumbers" class="modal fade" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <b id="editNumberModel"></b>
            </div>          
            <div class="modal-body">
                <div class="row-fluid">
                    <form class="form-horizontal" id="editnumberForm" name="editnumberForm">
                        <fieldset>
                            <div class="control-group ">
                                <label class="control-label col-lg-2">Edit</label>
                                <div class="col-lg-5">
                                    <input type="text" class="form-control " id="editNo" name="editNo" onkeypress="return isNumericKey(event)">
                                    <input type="hidden" id="numberId" name="numberId">
                                </div>
                                <div class="col-lg-5">
                                    <select class="form-control" id="numberType" name="numberType">                                         
                                    </select>
                                </div>
                            </div>                            
                        </fieldset>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <div id="edit-partner-result"></div>
                <button class="btn btn-info btn-xs" data-dismiss="modal">Close</button>
                <button class="btn btn-success btn-xs" onclick="editNumber()" id="addPartnerButtonE">Save</button>
            </div>
        </div>
    </div>
</div>                    
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<script src="js/numberOperation.js" type="text/javascript"></script>
<script src="js/bootbox.min(1).js" type="text/javascript"></script>
<!--<script src="js/bootbox.min (1).js" type="text/javascript"></script>-->
<script src="js/bootbox.min.js" type="text/javascript"></script>

</body>
</html>
<%@include file="footer.jsp" %>
