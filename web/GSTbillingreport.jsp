<%@include file="header.jsp"%>
<link href="./css/select2.css" rel="stylesheet"/>
<script src="./js/select2.js"></script>
<script src="./js/json3.min.js"></script>
<!--<link href="./css/bootstrap.min.css" rel="stylesheet"/>-->
<link href="./css/bootstrap-timepicker.css" rel="stylesheet"/>
<link rel="stylesheet" href="./css/datepicker.css">
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="dist/js/moment.min.js" type="text/javascript"></script>
<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript 
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.min.js"></script>
<script src="js/morris-data.js"></script>-->

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>
<script src="js/bootbox.min.js" type="text/javascript"></script>
<script src="./js/bootstrap-timepicker.js"></script>
<script src="./js/bootstrap-datepicker.js"></script>
<script src="js/GSTbillingreport.js" type="text/javascript"></script>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header text-uppercase">Tax Report</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div id="alerts-container" style="width: 50%; left: 25%; top: 2%;margin-left: 25%"></div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-cubes"></i><b> Tax Details </b>                
                </div>
                <div class="panel-body">
                    <table>
                        <tr>
                            <td> <span >From:</span>  
                            </td>
                            <td>
                                <div id="datetimepicker1" class="input-append date">
                                    <div class="input-prepend">
                                        <input id="_startdate" name="_startdate" type="text" data-format="yyyy-MM-dd" class="input-sm" style="width: 90%"  data-bind="value: vm.ActualDoorSizeDepth"/>
                                        <span class="add-on" hidden>
                                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </td>

                            <td> <span > To :</span>   
                            </td>
                            <td>
                                <div id="datetimepicker2" class="input-append date">
                                    <input id="_enddate" name="_enddate" type="text" data-format="yyyy-MM-dd" style="width: 90%" class="input-sm" data-bind="value: vm.ActualDoorSizeDepth"/>
                                    <span class="add-on" hidden>
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>                      
                                </div>
                            </td>
                           
                            <td>                            
                                <button class="btn btn-success btn-xs" id="Button" onclick="generateGSTreport()"><i class="fa fa-bar-chart"></i> Generate Reports</button>                                  
                            </td>
                        </tr>                                      
                    </table>
                    </br>
                    <div id="report_data"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('#datetimepicker1').datepicker({
            language: 'pt-BR'
        });
    });
    $(function () {
        $('#datetimepicker2').datepicker({
            language: 'pt-BR'
        });
    });
    $('#_ApStartTime').timepicker({minuteStep: 1, showInputs: false,
        disableFocus: true

    });
    $('#_ApEndTime').timepicker({minuteStep: 1, showInputs: false,
        disableFocus: true

    });
    $('#datetimepicker1').on('changeDate', function (ev) {
        $(this).datepicker('hide');
    });
    $('#datetimepicker2').on('changeDate', function (ev) {
        $(this).datepicker('hide');
    });
</script> 
<%@include file="footer.jsp"%>
                     
