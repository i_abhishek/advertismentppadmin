<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PgDailyTxManagement"%>
<%@page import="java.util.Calendar"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PgHourlyTXManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgDailyPgtx"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgHourlyPgtx"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.CAASCDRCalculationManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgCaastransactiondetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.CDRTxManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.PartnerDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ProductionAccessEnvtManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgDeveloperProductionEnvt"%>
<%@page import="java.text.NumberFormat"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.TaxCalculationManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.AccessPointManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.ResourceManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.ResourceDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.Accesspoint"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.DailyTxManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgDailytransactiondetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.SlabPriceCalculationManagement"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.LatePenalitiesCalculationManagement"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PackageSubscriptionManagement"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgSubscriptionDetails"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.SgPartnerrequest"%>
<%@page import="com.mollatech.serviceguard.nucleus.db.connector.management.PartnerRequestManagement"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="com.mollatech.PPadmin.billing.GenerateInvoiceId"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Admin Portal</title>
        <!-- Bootstrap Core CSS -->
        <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <!--    <link href="invoice_css/invoiceDetail.css" rel="stylesheet" type="text/css"/>
            <link href="invoice_css/style.css" rel="stylesheet" type="text/css"/>-->
        <!-- Custom CSS -->
        <link href="css/shop-homepage.css" rel="stylesheet">
        <script src="jshash/sha1-min.js" type="text/javascript"></script>
        <script src="jshash/sha1.js" type="text/javascript"></script>
        <script src="js/paymentOperation.js" type="text/javascript"></script>
        <script src="js/promocode.js" type="text/javascript"></script>
        <!-- jQuery -->
        <script src="js/jquery.js"></script>
        <script src="js/jquery-ui-1.10.4.min.js"></script>
        <script src="js/jquery-1.8.3.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
        <script src="./js/modal.js"></script>
    </head>
    <body>
        <%
            String SessionId = (String) request.getSession().getAttribute("_partnerSessionId");
            if (SessionId == null) {
                response.sendRedirect("oplogout.jsp");
                return;
            }
            SimpleDateFormat dailyTxFormat = new SimpleDateFormat("MM/dd/yyyy");
            SimpleDateFormat tmDateFormat  = new SimpleDateFormat("dd/MM/yyyy");
            String caasAccessPointName     = "caas";
            String invoiceid = "0";
            int partnID = Integer.parseInt(request.getParameter("_partnerid"));

            String ChannelId = (String) request.getSession().getAttribute("_channelId");            
            PartnerDetails parObj   = new PartnerManagement().getPartnerDetails(partnID);
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");            
            String subscriptionId       = request.getParameter("_subscriptionId");
            Integer subId               = Integer.parseInt(subscriptionId);

            SgSubscriptionDetails subscriObj = new PackageSubscriptionManagement().getSubscriptionbyId(SessionId, ChannelId, subId);
            Accesspoint accesspoint = new AccessPointManagement().getAccessPointByName(SessionId, ChannelId, caasAccessPointName);
            Double cancellationCharge = null;
            Double latePenaltyCharge = null;

            Map<String, Double> packageamount = new LinkedHashMap<String, Double>();
            Map<String, String> taxMap = new LinkedHashMap<String, String>();
            Double totalAmountWithoutTax = 0.00;
            String taxRate = null;

            if (subscriObj != null) {
                double planAmount = subscriObj.getPlanAmount();
                packageamount.put("Package Amount", planAmount);
                double serviceCharge = Double.parseDouble(subscriObj.getServiceCharge());
                packageamount.put("Service Charge", serviceCharge);

                String expiryDate = dateFormat.format(subscriObj.getExpiryDateNTime());
                String strDate    = dateFormat.format(new Date());
                Date curDate      = dateFormat.parse(strDate);
                Date exDate       = dateFormat.parse(expiryDate);
                if (curDate.before(exDate)) {
                    double packageCancellationCharge = subscriObj.getCancellationRate();
                    packageamount.put("Cancellation Charge", packageCancellationCharge);
                    cancellationCharge = packageCancellationCharge;
                }
                if (subscriObj.getPaymentMode().equalsIgnoreCase("postpaid")) {
                    if (curDate.after(exDate)) {
                        String latePenalites = subscriObj.getLatePenaltyRate();
                        if (latePenalites != null) {
                            double latePenalty = LatePenalitiesCalculationManagement.calculateLatePenalty(subscriObj);
                            packageamount.put("Late Penalty", latePenalty);
                            latePenaltyCharge = latePenalty;
                        }
                    }
                }
                taxRate = subscriObj.getTax();
            }
            JSONObject partnerObj = new JSONObject();
            SgDailytransactiondetails[] dailyTransObj = new DailyTxManagement().getTxDetails(ChannelId, partnID, dailyTxFormat.format(subscriObj.getCreationDate()), dailyTxFormat.format(new Date()), "00:00 AM", "11:59 PM");

            if (dailyTransObj != null) {
                for (int i = 0; i < dailyTransObj.length; i++) {
                    JSONObject data = new JSONObject(dailyTransObj[i].getRawData());
                    partnerObj.put("" + dailyTransObj[i].getExecuutionDate(), data.toString());
                }
            }
            
            if (subscriObj.getSlabApRateDetails() != null) {
                partnerObj = new SlabPriceCalculationManagement().slabPriceCalculation(partnerObj, subscriObj.getSlabApRateDetails(), "Live", "slab");
            } else if (subscriObj.getTierRateDetails() != null) {
                partnerObj = new SlabPriceCalculationManagement().slabPriceCalculation(partnerObj, subscriObj.getTierRateDetails(), "Live", "tier");
            }
            session.setAttribute("manualTierOrSlabUsageDetails", partnerObj);
            
            invoiceid = GenerateInvoiceId.getDate() + partnID + GenerateInvoiceId.getRandom();
            SimpleDateFormat invoiceDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            JSONObject caasCDRPrice = new JSONObject();
            SgDeveloperProductionEnvt productionEnvt = new ProductionAccessEnvtManagement().getProudDetailsByPartnerId(partnID);
            String billingAddress = "NA";String billingState = "NA"; String billingPostCode = "NA"; String billingCountry = "NA";
            if(productionEnvt != null){
                billingAddress  = productionEnvt.getBillingAddress();
                billingState    = productionEnvt.getBillingState();
                billingPostCode = productionEnvt.getBillingPostCode();
                billingCountry  = productionEnvt.getBillingCountry();
            }
            SgCaastransactiondetails[] caasCDRDetail = new CDRTxManagement().getTxDetails(parObj.getPartnerId(), subscriObj.getCreationDate(), subscriObj.getExpiryDateNTime(),"00:00 AM", "11:59 PM");
            if(caasCDRDetail != null){            
               caasCDRPrice = new CAASCDRCalculationManagement().calculateCDR(caasCDRDetail,accesspoint);
            }
            session.setAttribute("manualcaasServiceUsageDetails", caasCDRPrice);
            
            // tm payment calculation 
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        SgHourlyPgtx[] hourlytransactiondetails = null;
        SgDailyPgtx[] pgDailyTXObj = null;
        JSONObject pgTxObj    = new JSONObject();
        if(sdf.format(subscriObj.getCreationDate()).equals(sdf.format(new Date()))){
            hourlytransactiondetails = new PgHourlyTXManagement().getTxDetails(ChannelId, parObj.getPartnerId(), dailyTxFormat.format(subscriObj.getCreationDate()), dailyTxFormat.format(new Date()), "00:00 AM", "11:59 PM");            
        }else{
            Calendar pgCal = Calendar.getInstance();
            pgCal.setTime(new Date());
            pgCal.add(Calendar.DATE, -1);
            Date previousOneDate = pgCal.getTime();
            pgDailyTXObj = new PgDailyTxManagement().getTxDetails(ChannelId, parObj.getPartnerId(), dailyTxFormat.format(subscriObj.getCreationDate()), dailyTxFormat.format(previousOneDate), "00:00 AM", "11:59 PM");
            hourlytransactiondetails = new PgHourlyTXManagement().getTxDetails(ChannelId, parObj.getPartnerId(), dailyTxFormat.format(new Date()), dailyTxFormat.format(new Date()), "00:00 AM", "11:59 PM");
        }
        if (pgDailyTXObj != null) {
            for (int i = 0; i < pgDailyTXObj.length; i++) {
                JSONObject data = new JSONObject(pgDailyTXObj[i].getRawData());
                pgTxObj.put("" + pgDailyTXObj[i].getExecuutionDate()+";"+i, data.toString());
            }
        }
        if (hourlytransactiondetails != null) {
            for (int i = 0; i < hourlytransactiondetails.length; i++) {
                JSONObject data = new JSONObject(hourlytransactiondetails[i].getRawData());
                pgTxObj.put("" + hourlytransactiondetails[i].getExecuutionDate()+";"+i, data.toString());
            }
        }
        session.setAttribute("manualpgTransactionDetails", pgTxObj);
        %>
        <div style="padding-left: 110px;padding-top: 10px">            
            <a href="#" class="btn btn-info" onclick="sendInvoiceToDeveloper('<%=invoiceid%>', '<%=partnID%>', '<%=cancellationCharge%>', '<%=latePenaltyCharge%>')"> Send Invoice</a>        
        </div>
        <div id="alerts-container" style="width: 50%; left: 25%; top: 10%;margin-left: 25%"></div>
        <div id="invoice-container1">        
            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <a class="navbar-brand" href="#"><img src="img/logo_2.png" width="150" height="106" alt=""/></a>
                    </div>
                </div>
                <!-- /.container -->
            </nav>    
            <div class="container invoice">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-12 col-lg-12 col-md-12">
                                <div class="col-sm-6 col-lg-6 col-md-6">
                                    <h3>Tax Invoice</h3>
                                </div>
                                <div class="col-sm-6 col-lg-6 col-md-6 company-name">
                                    <p>TELEKOM MALAYSIA BERHAD( 128740-P )</p>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-12 col-md-12">
                                <div class="col-sm-6 col-lg-6 col-md-6">
                                    <table>
                                        <tr>
                                            <td class="table-header">Bill To</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p><%=parObj.getPartnerName()%><br/>
                                                    <%=billingAddress%><br/>
                                                    <%=billingState%><br/>
                                                    <%=billingPostCode%><br/>
                                                    <%=billingCountry%><br/>
                                            </td>
                                        </tr>
                                    </table>	
                                    <table>
                                        <tr>
                                            <td class="table-header">Remit To</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p><strong>Please issue cheque payable to :</strong><br/>
                                                    TELEKOM MALAYSIA BERHAD<br/>
                                                    And send to :<br/>
                                                    Wilayah Persekutuan Malaysia</p>
                                            </td>
                                        </tr>
                                    </table>								
                                </div>
                                <div class="col-sm-6 col-lg-6 col-md-6">
                                    <table>
                                        <tr>
                                            <td class="table-header">Information</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr class="information">
                                                        <td><strong>Invoice No</strong></td>
                                                        <td>:</td>
                                                        <td><%=invoiceid%> </td>
                                                    </tr>
                                                    <tr class="information">
                                                        <td><strong>Package Name</strong></td>
                                                        <td>:</td>
                                                        <td><%=subscriObj.getBucketName()%></td>
                                                    </tr>
                                                    <tr class="information">
                                                        <td><strong>Invoice Date</strong></td>
                                                        <td>:</td>
                                                        <td><%=invoiceDateFormat.format(new Date())%></td>
                                                    </tr>
                                                    <tr class="information">
                                                        <td><strong>Term of payment</strong></td>
                                                        <td>:</td>
                                                        <td><%=subscriObj.getBucketDuration()%></td>
                                                    </tr>
                                                    <tr class="information">
                                                        <td><strong>Due Date</strong></td>
                                                        <td>:</td>
                                                        <td><%=invoiceDateFormat.format(subscriObj.getExpiryDateNTime())%></td>
                                                    </tr>
                                                    <tr class="information">
                                                        <td><strong>Currency</strong></td>
                                                        <td>:</td>
                                                        <td>MYR</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>	
                                </div>
                            </div>					
                            <div class="col-sm-12 col-lg-12 col-md-12" style="padding: 30px;">						
                                <table id="invoiceDetailsTable">
                                    <thead>
                                        <tr>								
                                            <td class="table-header">Access point</td>
                                            <td class="table-header">Duration</td>
                                            <td class="table-header">API</td>
                                            <td class="table-header">Call Count</td>
                                            <td class="table-header">Unit Price</td>
                                            <td class="table-header">Total</td>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                    <tr>
                                        <%
                                            int count = 1;
                                            Double totalAmount = 0.0;
                                            DecimalFormat df = new DecimalFormat();
                                            df.setMaximumFractionDigits(2);
                                            String apName = "NA";
                                            String resName = "NA";
                                            String envt = "NA";
                                            String apiName = "NA";
                                            int version = 0;
                                            int callCount = 0;
                                            float amount = 0;
                                            
                                            if (partnerObj.length() != 0) {
                                                Iterator itrS = partnerObj.keys();
                                                while (itrS.hasNext()) {
                                                    Object keyS = itrS.next();
                                                    Date apiCallDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(keyS.toString());
                                                    JSONObject data = new JSONObject(partnerObj.getString((String) keyS));
                                                    Iterator itr = data.keys();
                                                    while (itr.hasNext()) {
                                                        float totalCharge = 0.0f;
                                                        Object key = itr.next();
                                                        String keyData = (String) key;
                                                        String value = data.getString((String) key);
                                                        int acc = Integer.parseInt(keyData.split(":")[0]);
                                                        int res = Integer.parseInt(keyData.split(":")[1]);
                                                        version = Integer.parseInt(keyData.split(":")[2]);
                                                        envt    = (keyData.split(":")[3]);
                                                        apiName = (keyData.split(":")[4]);
                                                        Accesspoint ap     = new AccessPointManagement().getAccessPointById(ChannelId, acc);
                                                        ResourceDetails rs = new ResourceManagement().getResourceById(res);
                                                        if (ap != null) {
                                                            apName = ap.getName();
                                                        }
                                                        if (rs != null) {
                                                            resName = rs.getName();
                                                        }
                                                        if (!envt.equalsIgnoreCase("Live")) {
                                                            continue;
                                                        }
                                                        callCount = Integer.parseInt(value.split(":")[0]);
                                                        amount = Float.parseFloat(value.split(":")[1]);
                                                        if(amount != -99){

                                                        totalCharge = callCount * amount;
                                        %> 
                                        <td><%=apName%></td>
                                        <td><%=tmDateFormat.format(apiCallDate)%></td>
                                        <td><%=apiName%></td>
                                        <td><%=callCount%></td>
                                        <td>RM <%=df.format(amount)%></td>
                                        <td>RM <%=df.format(totalCharge)%></td>
                                    </tr>
                                    <%
                                                        count++;
                                                        totalAmount = totalAmount + totalCharge;
                                                    }
                                                }
                                            }
                                        }
                                        if (subscriObj != null && subscriObj.getFlatPrice() != null) {
                                            String flatPricede = subscriObj.getFlatPrice();
                                            JSONArray jsOld = new JSONArray(flatPricede);
                                            for (int j = 0; j < jsOld.length(); j++) {
                                                JSONObject jsonexists1 = jsOld.getJSONObject(j);
                                                Iterator<String> keys = jsonexists1.keys();
                                                if (keys.hasNext()) {
                                                    String key = (String) keys.next();
                                                    String[] keyArr = key.split(":");
                                                    String value = jsonexists1.getString(key);
                                                    String[] valueArr = value.split(":");
                                    %>
                                    <tr>
                                        <td><%=keyArr[0]%></td>
                                        <td></td>
                                        <td>Flat Price</td>
                                        <td></td>
                                        <td>RM <%=df.format(Double.parseDouble(valueArr[1]))%></td>
                                        <td>RM <%=df.format(Double.parseDouble(valueArr[1]))%></td>
                                    </tr>        
                                    <%
                                                    totalAmount = totalAmount + Double.parseDouble(valueArr[1]);

                                                }
                                            }
                                        }
                                        if(caasCDRPrice.length() != 0){
                                            Iterator itrS = caasCDRPrice.keys();
                                                while (itrS.hasNext()) {
                                                Object keyS = itrS.next();                                                
                                                String caaskey = (String) keyS;
                                                String value = caasCDRPrice.getString((String) caaskey);
                                                String[] keyData = caaskey.split(";");
                                                Date caasAPIDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(keyData[2]);

                                    %>
                                    <tr>
                                        <td></td>
                                        <td><%=tmDateFormat.format(caasAPIDate)%></td>
                                        <td>CAAS</td>
                                        <td><%=keyData[1]%> Min</td>
                                        <td></td>
                                        <td>RM <%=df.format(Float.parseFloat(value))%></td>
                                    </tr>  
                                    <%
                                                totalAmount = totalAmount + Float.parseFloat(value);
                                            }
                                        }if(pgTxObj.length() != 0){
                                            Iterator itrS = pgTxObj.keys();
                                            while (itrS.hasNext()) {
                                                Object keyS = itrS.next();
                                                Date apiCallDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(keyS.toString().split(";")[0]);
                                                JSONObject data = new JSONObject(pgTxObj.getString((String) keyS));
                                                Iterator itr = data.keys();
                                                while (itr.hasNext()) {
                                                    
                                                    Object key = itr.next();
                                                    String keyData = (String) key;
                                                    String value = data.getString((String) key);
                                                    String[] valueDetails = value.split(":");
                                                    envt    = (keyData.split(":")[3]);
                                                    if (!envt.equalsIgnoreCase("Live")) {
                                                        continue;
                                                    }
                                                    
                                    %> 
                                    <tr>
                                        <td></td>
                                        <td><%=tmDateFormat.format(apiCallDate)%></td>
                                        <td>TM PG</td>
                                        <td><%=valueDetails[0]%> transaction</td>
                                        <td>RM <%=valueDetails[1]%></td>
                                        <td>RM <%=df.format(Float.parseFloat(valueDetails[1]))%></td>
                                    </tr>                             
                                    <%
                                        totalAmount = totalAmount + Double.parseDouble(valueDetails[1]);
                                            }
                                        }
                                    }
                                        for (Map.Entry<String, Double> entry : packageamount.entrySet()) {
                                    %>                       
                                    <tr>
                                        <td colspan="2"></td>
                                        <td><%=entry.getKey()%></td>
                                        <td></td>
                                        <td>RM <%=df.format(entry.getValue())%></td>
                                        <td>RM <%=df.format(entry.getValue())%></td>
                                    </tr>
                                    <%      count++;
                                            totalAmount = totalAmount + entry.getValue();
                                        }
                                        totalAmountWithoutTax = totalAmount;

                                        taxMap = TaxCalculationManagement.calculateTax(taxRate, totalAmountWithoutTax);
                                        if (!taxMap.isEmpty()) {
                                            for (Map.Entry<String, String> entry : taxMap.entrySet()) {
                                                if (entry.getKey().equalsIgnoreCase("GST Tax")) {
                                                    String[] taxArr = entry.getValue().split(":");

                                    %>
                                    <tr>
                                        <td colspan="2"></td>
                                        <td><%=entry.getKey()%></td>
                                        <td><%=taxArr[0]%> %</td>
                                        <td>RM <%=df.format(Double.parseDouble(taxArr[1]))%></td>
                                        <td>RM <%=df.format(Double.parseDouble(taxArr[1]))%></td>
                                    </tr>
                                    <%
                                                    count++;
                                                    totalAmount = totalAmount + Float.parseFloat(taxArr[1]);
                                                }
                                            }
                                        }
                                        session.setAttribute("manualtotalPaymentAmount", totalAmount);
                                    %>
                                    <tr>
                                        <td colspan="4"></td>
                                        <td style="text-align: right"><strong>TOTAL</strong></td>
                                        <td id="totalAmount">RM <%=df.format(totalAmount)%></td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            <h4>PRIVACY STATEMENT</h4>
                                            <p>PENYATAAN PRIVASI TM Dalam usaha memastikan pematuhan kepada Akta Perlindungan Data Peribadi 2010 (APDP), TM telah mewujudkan satu dasar perlindungan data peribadi yang akan mengawal penggunaan dan perlindungan data peribadi anda sebagai pelanggan TM. Untuk mengetahui dasar tersebut secara terperinci, sila rujuk Penyataan Privasi TM di http:///www.tm.com.my, yang mana tertakluk kepada perubahan dari masa ke semasa oleh TM. 
                                                <br><br>
                                                TM'S PRIVACY STATEMENT In its effort to ensure compliance to the Personal Data Protection Act 2010 (PDPA), TM has put in place a personal data protection policy which shall govern the use and protection of your personal data as TM's customer. For details of the policy, please refer to TM's Privacy Statement at http:///www.tm.com.my, which may be reviewed by TM from time to time.
                                            </p>
                                        </td>
                                    </tr>
                                </table>                        
                            </div>
                        </div>
                    </div>
                </div>
            </div>                      
            <!-- /.container -->
            <div class="container">
                <hr>
                <!-- Footer -->
                <footer>
                    <div class="row">
                        <div class="col-lg-12">
                            <p><strong>TELEKOM MALAYSIA BERHAD( 128740-P )</strong><br/>
                                Level 51 North Wing, Menara TM Jalan Pantai Baharu 50672 Kuala Lumpur Wilayah Persekutuan Malaysia<br/>
                                Tel: 03-2240 9494 Fax: 03-2283 2415 WWW.TM.COM.MY</p>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </body>
</html>
