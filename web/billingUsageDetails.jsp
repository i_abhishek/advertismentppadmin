<%@include file="header.jsp" %>
<script src="./js/partnerRequest.js"></script>
<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header text-uppercase">Billing Details</h3>
            </div>
            <!-- /.col-lg-10 -->
        </div>
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47;  <i class="fa fa-table"></i><b> Billing Details</b>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal " method="get">
                        <div class="form-group">   
                            <label class="control-label" style="margin-left: 3%">Package name : <span class="text-primary">Premium</span></label>
                            <label class=" control-label" style="margin-right:  12%; margin-left: 4%">Main Credit : <span class="text-primary">5000</span></label>
                            <label class=" control-label" style="margin-right:  3%">Free Credit : <span class="text-primary">1000</span></label>
                            <label class=" control-label" style="">Remaining Credit : <span class="text-primary">3300</span></label>
                        </div>
                        <div class="form-group">   
                            <label class="control-label" style="margin-left: 3%">Subscription on: <span class="text-primary">23-6-2016</span></label>
                            <label class=" control-label" style="margin-right:  3%; margin-left: 3%">End of Subscription : <span class="text-primary">23-6-2017</span></label>
                            <label class=" control-label" style="margin-right:  3%">Payment mode : <span class="text-primary">Postpaid</span></label>
<!--                            <label class=" control-label" style="">Remaining Credit : <span class="text-primary">3300</span></label>-->
                        </div> 
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th><font style="font-size: 15px">No.</th>
                                        <th><font style="font-size: 15px">Access Point</th>
                                        <th><font style="font-size: 15px">Resource</th>
                                        <th><font style="font-size: 15px">Version</th>
                                        <th><font style="font-size: 15px">API</th>                                        
                                        <th><font style="font-size: 15px">Call</th>
                                        <th><font style="font-size: 15px">Unit Charge</th>
                                        <th><font style="font-size: 15px">Charge</th>
                                        <th><font style="font-size: 15px">Date</th>
<!--                                        <th><font style="font-size: 10px">PDF</th>-->
                                    </tr>
                                </thead>
                                <tbody>                                   
                                    <tr>
                                        <td ><font style="font-size:15px "><%=1%></font></td>
                                        <td ><font style="font-size: 15px">XPAY</font></td>
                                        <td ><font style="font-size: 15px">Calculator</font></td>
                                        <td ><font style="font-size: 15px">1</font></td>
                                        <td ><font style="font-size: 15px">add</font></td>
                                        <td ><font style="font-size: 15px">200</font></td>
                                        <td ><font style="font-size: 15px">5</font></td>
                                        <td ><font style="font-size: 15px">1000</font></td>
                                        <td ><font style="font-size: 15px">26-6-2016</font></td>                                                                                 
                                    </tr>
                                    <tr>
                                        <td ><font style="font-size: 15px "><%=2%></font></td>
                                        <td ><font style="font-size: 15px">Axiom</font></td>
                                        <td ><font style="font-size: 15px">Axiom</font></td>
                                        <td ><font style="font-size: 15px">1</font></td>
                                        <td ><font style="font-size: 15px">openSession</font></td>
                                        <td ><font style="font-size: 15px">200</font></td>
                                        <td ><font style="font-size: 15px">1</font></td>
                                        <td ><font style="font-size: 15px">200</font></td>
                                        <td ><font style="font-size: 15px">26-6-2016</font></td>                                                                                 
                                    </tr> 
                                    <tr>
                                        <td ><font style="font-size:15px "><%=3%></font></td>
                                        <td ><font style="font-size: 15px">Axiom</font></td>
                                        <td ><font style="font-size: 15px">Axiom</font></td>
                                        <td ><font style="font-size: 15px">1</font></td>
                                        <td ><font style="font-size: 15px">assignToken</font></td>
                                        <td ><font style="font-size: 15px">20</font></td>
                                        <td ><font style="font-size: 15px">25</font></td>
                                        <td ><font style="font-size: 15px">500</font></td>
                                        <td ><font style="font-size: 15px">26-6-2016</font></td>                                                                                 
                                    </tr>
                                    
                                </tbody>
                                <tbody>
                                    <tr>
                                        <td ><font style="font-size:15px "></font></td>
                                        <td ><font style="font-size: 15px"> </font></td>
                                        <td ><font style="font-size: 15px"> </font></td>
                                        <td ><font style="font-size: 15px"> </font></td>
                                        <td ><font style="font-size: 15px"> </font></td>
                                        <td ><font style="font-size: 15px"> </font></td>
                                        <td ><font style="font-size: 15px">Total</font></td>
                                        <td ><font style="font-size: 15px">$ 1700</font></td>
                                        <td ><font style="font-size: 15px"></font></td>                                                                                 
                                    </tr> 
                                </tbody>
                            </table>
                                        <button class="btn btn-success btn-xs" value="Generate PDF"><i class="fa fa-file-pdf-o"></i> Generate PDF</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript 
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.min.js"></script>
<script src="js/morris-data.js"></script>-->

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>
<script src="js/bootbox.min.js" type="text/javascript"></script>
<script>
                                            $(document).ready(function () {
                                                $('#dataTables-example').DataTable({
                                                    responsive: true
                                                });
                                            });
</script>
</body>
</html>
