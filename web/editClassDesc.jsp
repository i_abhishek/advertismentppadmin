<%@page import="com.mollatech.serviceguard.nucleus.commons.ClassName"%>
<%@page import="com.mollatech.serviceguard.nucleus.commons.Classes"%>
<%@page import="org.bouncycastle.util.encoders.Base64"%>
<%--<%@page import="com.partner.web.ClassName"%>
<%@page import="com.partner.web.Classes"%>--%>
<%@page import="java.util.Map"%>
<%@include file="header.jsp" %>
<%    String value = request.getParameter("value");
    String _resId = request.getParameter("_resId");
    String ver = request.getParameter("ver");
    int resId = Integer.parseInt(_resId);
    Map classMap = (Map) session.getAttribute("eclasses");
    Classes classes = (Classes) classMap.get(value);
    String classname = request.getParameter("classname");
    ClassName className = null;
    String id = session.getAttribute("_apId").toString();
    for (int i = 0; i < classes.pname.classs.size(); i++) {
        if (classes.pname.classs.get(i).classname.equals(classname)) {
            className = classes.pname.classs.get(i);
            break;
        }
    }
%>   
<script src = "./assets/js/acceptPoint.js" ></script>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header text-uppercase">Services Description Details</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <i class="fa fa-home"></i><a href="home.jsp"> Home</a>&#47; <i class="fa fa-cube"></i><a href="services.jsp"> Services</a>
                    &#47; <strong>"<%=classname%>" Description</strong>
                </div>
                <div class="panel-body">
                    <form role="form" id="saveClassDescform" name="saveClassDescform">
                        <table class="table table-striped">
                            <tr align=?left?>
                                <th style="width: 10%">Class Name</th>
                                <th style="width: 10%"><%=classname%></th>
                                <td><textarea readonly class="form-control" rows="3" id="description<%=classname%>" name="description<%=classname%>" style="width: 75%"><%if (className.description != null) {%><%=new String(Base64.decode(className.description))%><%} else {%>No Description<%}%></textarea></td>
                            </tr>

                            <%if (className.paramses != null) {
                                    for (int j = 0; j < className.paramses.size(); j++) {
                            %>
                            <tr align=?left?>
                                <th style="width: 10%">Input Parameter</th>
                                <th style="width: 15%"><%=className.paramses.get(j).type.replace("<", "&lt;").replace(">", "&gt;")%> <%=className.paramses.get(j).name%></th>
                                <td><textarea readonly class="form-control" rows="3" id="description<%=className.paramses.get(j).name%>" name="description<%=className.paramses.get(j).name%>" style="width: 75%"><%if (className.paramses.get(j).pDescription != null) {%><%=new String(Base64.decode(className.paramses.get(j).pDescription))%><%} else {%>No Description<%}%></textarea></td>
                            </tr>
                            <%}
                            }%>
                        </table>
                        <div class="control-group">
                            <label class="control-label"  for="username"></label>
                            <div class="controls">
                                &nbsp;&nbsp;&nbsp;<a class="btn btn-warning btn-sm" href="./transformDetails.jsp?_apid=<%=id%>&_resId=<%=resId%>&ver=<%=ver%>"  type="button"><i class="fa fa-hand-o-left"></i> Back </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript 
<script src="bower_components/raphael/raphael-min.js"></script>
<script src="bower_components/morrisjs/morris.min.js"></script>
<script src="js/morris-data.js"></script>-->

<!-- Custom Theme JavaScript -->
<script src="dist/js/sb-admin-2.js"></script>
<!-- DataTables JavaScript -->
<script src="bower_components/datatables/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="bower_components/datatables-responsive/js/dataTables.responsive.js" type="text/javascript"></script>
<script src="js/bootbox.min.js" type="text/javascript"></script>
</body>
</html>
<%@include file="footer.jsp" %> 